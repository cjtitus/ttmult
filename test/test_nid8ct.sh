#!/bin/bash

SOURCE="$(dirname "$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" )"
echo $SOURCE
SCRIPTS=$SOURCE/scripts

EXAMPLE=nid8ct

$SCRIPTS/runrcg $EXAMPLE
$SCRIPTS/runrac $EXAMPLE
$SCRIPTS/runban $EXAMPLE

