
#include "globals.h"
#include <string.h>

#include "spectra.h"

#define DEFAULT_NPOINTS 1000

/* Exported data */
boolean dynamic_range = true, inform = false, nodegeneracies = false;
integer npoints = 0;
real gaussian = 0.0, approx_zero = 0.0000001, energy_shift = 0.0;
real min_energy = 0.0, max_energy = BIG_REAL, temperature = 0.0, scale = 1.0;
char initial_state[STRING_SIZE] = "", operator_label[STRING_SIZE] = "",
     final_state[STRING_SIZE] = "";
boolean ground_energy_set = false;
state ground_states = NULL;
real ground_energy = 0.0;
/* The list of plots we've created */
plot_data plot_head = NULL, plot_tail = NULL;

/* Local data */
/* This is Boltzmanns constant - converts Kelvin to eV.
   So its k in J/K divided by number of J/eV */
static real k = 0.00008618;
/* Used by the tree routines */
static spectral_line spec_lines = NULL, plot_lines = NULL;
/* input parameters used by the routines that create spectra */
static real energy_step = 0.0;
static char lowest_state[STRING_SIZE];
static state temp_ground = NULL;
/* used by Fano and the gaussian routines */
static real pi;
static real spectrum[MAX_PTS];

/* Function prototypes */
static void empty_tree(spectral_line);
static void empty_states(state);
static void find_ground_energy(spectral_line);
static void find_energy_range(void);
static real search_lines(spectral_line);
static real normalise_temp(void);
static void insert_lines(void);
static void clear_spectrum(void);
static void find_sticks(spectral_line);
static real fano(real gamma, real q, real energy);
static void widen_lines(spectral_line, lorentz lorentzian);
static void convolute(real sigma);
static plot_data spectrum_to_plot(void);
static void sticks_to_plot(plot_data plot);

/* Routines for dealing with plots */

boolean unique_plot_label(char label[])
/* Checks that the label for the plot will be unique. */
{
  plot_data plot = plot_head;
  boolean unique = true;

  while (plot != NULL and unique) {
    unique = (strcmp(label, plot->label) != 0);
    plot = plot->next;
  }
  return(unique);
}

plot_data find_plot(char label[])
/* Find the plot with the given label */
{
  plot_data plot = plot_head;
  boolean found = false;

  while (plot != NULL and !found) {
    if (strcmp(label, plot->label) == 0)
      found = true;
    else
      plot = plot->next;
  }
  return(plot);
}

void add_plot(plot_data plot)
/* Add the plot to the list. */
{
  if (plot_head == NULL)
    plot_head = plot;
  else
    plot_tail->next = plot;
  plot_tail = plot;
}

void remove_plot(plot_data plot)
{
  free_mem(plot->spectrum);
  if (plot->sticks != NULL)
    free_mem(plot->sticks);
  free_mem(plot);
}

spectral_line create_lines(integer n)
{
  spectral_line node;

  node = (spectral_line)mem_alloc(sizeof(struct matrix));
  node->intensities = (ket_vector *) mem_alloc(n * sizeof(ket_vector));
  node->rows = n;
  node->cols = 0;
  node->multiplicity = 1;
  node->ket_energies = NULL;
  node->next = NULL;
  node->initial_state[0] = 0;
  node->transition_label[0] = 0;
  node->op_label[0] = 0;
  node->final_state[0] = 0;
  return(node);
}

void insert_node(spectral_line newnode, boolean all_lines)
/* procedure to insert a new matrix of lines. */
{
  if (all_lines) {
    if (spec_lines != NULL)
      newnode->next = spec_lines;
    spec_lines = newnode;
  } else {
    if (plot_lines != NULL)
      newnode->next = plot_lines;
    plot_lines = newnode;
  }
}

void store_ground()
{
  state ptr = temp_ground;

  while (ptr->next != NULL)
    ptr = ptr->next;
  ptr->next = ground_states;
  ground_states = temp_ground;
  temp_ground = NULL;
}

void insert_ground_state(state newstate)
{
  state ptr = temp_ground;
  boolean found = false;

  while (ptr != NULL and !found) {
    if (strcmp(ptr->label, newstate->label) == 0)
      found = true;
    else
      ptr = ptr->next;
  }
  if (found) {
    free_mem(newstate);
    newstate = NULL;
  } else {
    newstate->next = temp_ground;
    temp_ground = newstate;
  }
}

static void empty_tree(spectral_line head)
/* Delete the whole tree */
{
  spectral_line ptr;
  integer i;

  while (head != NULL) {
    ptr = head;
    head = head->next;
    for (i = 0; i < ptr->rows; i++)
      free_mem(ptr->intensities[i].intensities);
    free_mem(ptr->intensities);
    free_mem(ptr->ket_energies);
    free_mem(ptr);
  }
}

static void empty_states(state head)
/* Delete the whole tree */
{
  state ptr;

  while (head != NULL) {
    ptr = head;
    head = head->next;
    free_mem(ptr);
  }
}

void clear_tree(boolean all_lines)
/* Delete list of lines. */
{
  if (all_lines) {
    empty_tree(spec_lines);
    spec_lines = NULL;
    empty_states(ground_states);
    ground_states = NULL;
  } else {
    empty_tree(plot_lines);
    plot_lines = NULL;
  }
}

/* routines to process to spectral data */

static void find_ground_energy(spectral_line head)
/* Since no ground state energy is set, find out what the lowest state is. */
{
  while (head != NULL) {
    if (head->intensities[0].bra_energy < ground_energy) {
      if (initial_state[0] == 0 or
	  strcmp(head->initial_state, initial_state) == 0) {
	ground_energy = head->intensities[0].bra_energy;
	strcpy(lowest_state, head->initial_state);
      }
    }
    head = head->next;
  }
}

static void find_energy_range()
{
  spectral_line node = plot_lines;
  real min_e = BIG_REAL, max_e = -BIG_REAL;

  while (node != NULL) {
    if (node->ket_energies[0] - node->intensities[node->rows - 1].bra_energy
	< min_e)
      min_e = node->ket_energies[0]
	- node->intensities[node->rows - 1].bra_energy;
    if (node->ket_energies[node->cols - 1] - node->intensities[0].bra_energy
	> max_e)
      max_e = node->ket_energies[node->cols - 1]
	- node->intensities[0].bra_energy;
    node = node->next;
  }
  min_energy = rTrunc(min_e) - 2.0;
  max_energy = rTrunc(max_e) + 3.0;
}

static real search_lines(spectral_line node)
/* this actually scans the lines for insertion */
{
  boolean valid_node;
  integer i, j, l;
  real lscale, tscale, total = 0.0;
  spectral_line new_node;

  while (node != NULL) {
    valid_node = true;
    if (initial_state[0] != 0 and
	strcmp(node->initial_state, initial_state) != 0)
      valid_node = false;
    if (operator_label[0] != 0 and
	(strcmp(node->transition_label, operator_label) != 0 and
	 strcmp(node->op_label, operator_label) != 0))
      valid_node = false;
    if (final_state[0] != 0 and
	strcmp(node->final_state, final_state) != 0)
      valid_node = false;
    if (valid_node) {
      new_node = create_lines(node->rows);
      new_node->multiplicity = node->multiplicity;
      new_node->next = NULL;
      new_node->cols = node->cols;
      strcpy(new_node->initial_state, node->initial_state);
      strcpy(new_node->op_label, node->op_label);
      strcpy(new_node->transition_label, node->transition_label);
      strcpy(new_node->final_state, node->final_state);
      new_node->ket_energies = (real *) mem_alloc(sizeof(real) * node->cols);
      for (i = 0; i < node->cols; i++)
        new_node->ket_energies[i] = node->ket_energies[i] + energy_shift;
      l = 0;
      for (i = 0; i < node->rows; i++) {
	lscale = scale;
        if (temperature > approx_zero) {
	  tscale = rexp((ground_energy - node->intensities[i].bra_energy) /
			(k * temperature));
	  if (tscale < approx_zero)
	    lscale = 0.0;
	  else
	    lscale *= tscale;
	} else if (rabs(ground_energy - node->intensities[i].bra_energy) >=
		   approx_zero)
	  lscale = 0.0;
	if (rabs(ground_energy - node->intensities[i].bra_energy) <
	    approx_zero) {
	  /* Calculate the total intensity of the ground state. */
	  for (j = 0; j < node->cols; j++)
	    total += node->intensities[i].intensities[j];
	}
	if (lscale != 0.0) {
	  new_node->intensities[l].bra_energy =
	    node->intensities[i].bra_energy;
	  new_node->intensities[l].intensities =
	    (real *) mem_alloc(sizeof(real) * node->cols);
	  for (j = 0; j < node->cols; j++)
	    new_node->intensities[l].intensities[j] = lscale *
	      node->intensities[i].intensities[j];
	  l++;
	}
      }
      if (l != 0) {
	new_node->rows = l;
	insert_node(new_node, false);
      }
    }
    node = node->next;
  }
  return(total);
}

static real normalise_temp()
/* This is required since when using a Boltzmann distribution we should
   divide the result by the total weight. e.g 100/110 + 10/110 to get a
   total weight of 100% */
{
  integer i;
  real weight = 0.0, intensity;
  state node = ground_states;

  if (initial_state[0] != 0) {
    /* By calculating the normalisation before we insert the lines, we can
       only do it for all ground states or the ones given by the label
       in initial_state. Hence if we use addlines to sum lines from states
       with different ground state labels, we will divide each by its total
       weight rather than both by the sum of the total weights. Of course
       we have no problem if both have the same ground state label, or we
       don't give a ground state label. */
    fprintf(stderr, "Due to the use of addlines the temperature ");
    fprintf(stderr, "normalisation is probably wrong\n");
    return(0.0);
  }
  while (node != NULL) {
    if (initial_state[0] == 0 or
	strcmp(initial_state, node->label) == 0) {
      for (i = 0; i < node->rows; i++) {
	intensity = (real)(node->dim)
			   * rexp((ground_energy - node->energy[i]) /
				  (k * temperature));
	if (intensity > ZERO)
	  weight += intensity;
      }
    }
    node = node->next;
  }
  return(weight);
}

static void insert_lines()
/* scan the list of spectral lines and make a copy of those we wish to plot */
{
  real total;

  if (temperature > approx_zero and ground_energy_set) {
    fprintf(stderr, "Can't define a ground state energy and a temp\n");
    exit(0);
  }
  if (not ground_energy_set) {
    ground_energy = BIG_REAL;
    find_ground_energy(spec_lines);
    printf("Ground state has symmetry %s, energy = %f, ",
           lowest_state, ground_energy);
    ground_energy_set = true;
  }
  if (temperature > approx_zero)
    scale = scale / normalise_temp();
  total = search_lines(spec_lines);
  printf("total intensity = %f\n", total);
}

static void clear_spectrum()
/* Clear the spectrum */
{
  register integer i;

  for (i = 0; i < npoints; i++)
    spectrum[i] = 0.0;
}

static void find_sticks(spectral_line node)
/* Sum the lines between E - step/2 and E + step/2, to go at E */
{
  real energy, temp;
  integer place, i, j;

  while (node != NULL) {
    for (i = 0; i < node->rows; i++) {
      temp = node->intensities[i].bra_energy + min_energy;
      for (j = 0; j < node->cols; j++) {
	energy = node->ket_energies[j] - temp;
	/* Round off the number of steps represented by energy */
	place = Round(energy / energy_step);
	if (place > 0 and place < npoints)
	  spectrum[place] += node->intensities[i].intensities[j];
      }
    }
    node = node->next;
  }
}

static real fano(real gamma, real q, real energy)
{
  return( ((q + energy / gamma) * (q + energy / gamma)) /
	    (gamma * (q * q - 1.0) * pi *
	    (1.0 + (energy / gamma) * (energy / gamma))) );
}

static void widen_lines(spectral_line node, lorentz lorentzian)
/* Add the Lorentzian width to each line */
{
  integer i, j, k;
  lorentz ptr;
  real delta_e, energy, qgam, gamsq, coeff;

  while (node != NULL) {
    for (i = 0; i < node->rows; i++) {
      ptr = lorentzian;
      for (j = 0; j < node->cols; j++) {
	if (rabs(node->intensities[i].intensities[j]) > ZERO) {
	  delta_e = node->ket_energies[j] - node->intensities[i].bra_energy;
	  while (ptr->max_energy < delta_e) {
	    ptr = ptr->next;
	    if (ptr == NULL) {
	      fprintf(stderr, "No lorentzian given for line at %f eV,",
		      delta_e);
	      fprintf(stderr, " which contributes to the spectrum\n");
	      exit(1);
	    }
	  }
	  coeff = node->intensities[i].intensities[j] /
		    (ptr->gamma * pi * (ptr->q * ptr->q - 1.0));
	  gamsq = ptr->gamma * ptr->gamma;
	  qgam = ptr->q * ptr->gamma;
	  energy = min_energy - delta_e;
	  for (k = 0; k < npoints; k++) {
	    /* This is the intensity * fano(energy) of course. */
	    spectrum[k] += (coeff * (qgam + energy) * (qgam + energy) /
			    (gamsq + energy * energy));
	    energy += energy_step;
	  }
	}
      }
    }
    node = node->next;
  }
}

static void convolute(real sigma)
/* Convolute all the Lorentzian widened lines with the Gaussian.
   Essentially unchanged from Theo's program */
{
  static real spec[MAX_PTS];
  real sqrt_two_pi, iran, sum, f;
  register integer i, j;

  sqrt_two_pi = rsqrt(8.0 * Atan(1.0));
  for (i = 0; i < npoints; i++)
    spec[i] = 0.0;
  iran = min(npoints - 1, (integer)(10.0 * sigma));
  sum = 0.0;
  for (j = -iran; j <= iran; j++) {
    f = rexp(-((real)(j * j))/(2 * sigma * sigma)) / (sqrt_two_pi * sigma);
    sum += f;
    for (i = max(1, 1 - j); i <= min(npoints, npoints - j); i++)
      spec[i - 1] += spectrum[i + j - 1] * f;
  }
  for (i = 0; i < npoints; i++)
    spectrum[i] = spec[i] / sum;
}

static plot_data spectrum_to_plot()
/* Store the new spectrum for use. */
{
  plot_data plot;
  integer i;

  plot = (plot_data) mem_alloc(sizeof(struct plot_info));
  plot->next = NULL;
  plot->npoints = npoints;
  plot->min_energy = min_energy;
  plot->max_energy = max_energy;
  plot->energy_step = energy_step;
  plot->spectrum = (real *) mem_alloc(npoints * sizeof(real));
  for (i = 0; i < npoints; i++)
    plot->spectrum[i] = spectrum[i];
  plot->sticks = NULL;
  return(plot);
}

static void sticks_to_plot(plot_data plot)
/* Add the calculated sticks to the data about the plot */
{
  integer i;

  plot->sticks = (real *) mem_alloc(plot->npoints * sizeof(real));
  for (i = 0; i < plot->npoints; i++)
    plot->sticks[i] = spectrum[i];
}

plot_data create_spectrum(boolean make, lorentz lorentzian)
/* Create the spectrum we will plot */
{
  plot_data plot = NULL;

  if (npoints == 0)
    npoints = DEFAULT_NPOINTS;
  insert_lines();
  if (plot_lines == NULL)
    return(NULL);
  if (make) {
    if (dynamic_range)
      find_energy_range();
    clear_spectrum();
    energy_step = (max_energy - min_energy) / ((real)npoints);
    pi = 4.0 * Atan(1.0);
    widen_lines(plot_lines, lorentzian);
    convolute(gaussian / energy_step);
    plot = spectrum_to_plot();
    clear_spectrum();
    find_sticks(plot_lines);
    sticks_to_plot(plot);
    clear_tree(false);
    return(plot);
  } else
    return((plot_data)1);
}
