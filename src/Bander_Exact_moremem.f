      PROGRAM BANAUG4
c --- implicit real*8 (a-h,o-z)
C
C  BASED ON /THOLE/NEWBAND
C
C                1990.4.13    given by B.T.THOLE
C
C  /BANAUG8      1991.9.20             H.OGASAWARA
C
c  (29/11/1991)  october version obtained from Ogasawara
c  (20/12/1991)  --- alloc  --read  -- diagonalise

C  Dec '92, B.G.Searle and B.T.Thole
C           restructured alloc for dynamic allocation by C and
C           modified Cowan and Ham_times_vec for sparse operations
C  Mar '93, B.G.Searle
C           sparse reading and matrix building implementated
C           slightly slower but band uses a lot less memory.
c  Dec '93  B.T.Thole  Cleaned up sparse things. alloc and illoc. tridiag.

C  2013		R. J. Green - Changed diagonalization from lanczos to exact

C There is a parameter called intsize which must be set for your machine.
C Intsize = sizeof(real) / sizeof(integer).
C *** It is initialized at the start of this routine

c     my changes marked by *****

C---- Input (50) is read from column 1 <<< to not avoid line numbers.>>>
C---- c.f. subroutine NEWLIN.
c--- lheap appears many times in the program. Be sure to change
c--- every occurrence.
      parameter   (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence     (nx,x)
      common    /ACTORS / NADD(2,2,3,3) , XADD(10,2,2,3,3)
      common    /CPRMULT/ PRMULT
      common    /qaz / old_cowan, intsize
      common    /spectr/ erange
      common    /debug / inform_debug
      logical   prmult,   old_cowan
      dimension NCONF(2), NG(3), NF(3)
      character *20 IRG , IRT, IRF, PRMUL, CHAGET
*
      DIMENSION EGRND(3),EFXAS(3),DIP(9),DIPx(9),CI(9),EFAUG(9,3)
      LOGICAL   SwitchAuger
C
      call CPUTIME
      inform_debug  = 0
      intsize       = 1
      i_calculation = 0
      infinite      = 100000


C HP-UX the following lines are needed on HP. Else comment them out.
      open(14, status = 'old',     file = 'FTN14',
     &      access = 'sequential', form = 'formatted')
      open(15, status = 'old',     file = 'FTN15',
     &      access = 'sequential', form = 'formatted')
      open(98, status = 'unknown', file = 'FTN98',
     &      access = 'sequential', form = 'unformatted')
      open(99, status = 'unknown', file = 'FTN99',
     &      access = 'sequential', form='unformatted')
C HP-UX

      do 70 i_calculation = 1, infinite
c---  do for each set of parameters. File 14 is only read for
c---  i_calculation = 1, otherwise file 99 is used. Further
c---  diagonalisation data is stored on file 98 to be used for
c---  next triad or, if requested, for next i_calculation.
      write(44,'(a,i2)') 'BAND',i_calculation

      call NEWFIL(50)
      call getact (NCONF, N2, N3, W, DEL, UCV, UVV,
     &              PRMULT, EGRND, EFXAS, DIPXAS,
     &              SwitchAuger, EFAUG, NOMEGA, ESTART, ESTEP, DIP, CI)
C
      if (SwitchAuger) then
        print'(a,i3)',' Auger calculation number',i_calculation
c--     dipx is used in the calculations. Dip should remain unchanged.
        DO 1 I=1,9
          DIPx(I)=DIP(I)/DIPXAS/SQRT(13.6)
    1   CONTINUE
        PRINT '(A,2X, F10.5)',' DIPXAS',DIPXAS
        PRINT '(A,2X,3F10.3)',' EGRND',(EGRND(I),I=1,3)
        PRINT '(A,2X,3F10.3)',' EFXAS',(EFXAS(I),I=1,3)
        PRINT '(9(A,I2,3F9.3,3(A,F10.5)/))',(' EFAUG',J,
     &   (EFAUG(J,I),I=1,3),': DIP',DIP(J),'(',DIPx(J),
     &                      ')  CI', CI(J),J=1,9)
      else
       print'(a,i3)',' Band calculation number',i_calculation
      endif
C
      do 60 itriad = 1, infinite
      call NEWFIL (50)
      rewind 99
      call STATUS ('program BanAug7')
      IRG = CHAGET()
c--   At end of triads start a new calculation.
      if (irg .eq. 'ENDTRIADS') goto 70
      IRT = CHAGET()
      IRF = CHAGET()
      if (PRMULT)  PRMUL = CHAGET()
      rewind  15
      call NEWFIL (15)
      call izero ( ng, 3 )
      call izero ( nf, 3 )
      IDIMG = 0
      IDIMF = 0

      do 20 ICG = 1, NCONF(1)
      do 20 ICf = 1, NCONF(2)
        if (nadd(1,2,icg,icf) .ne. 0) then
          call GETMUL (NG(ICG), IDIMG, IRG, 'GROUND')
          call GETMUL (NF(ICF), IDIMF, IRF, 'EXCITE')
        endif
   20 continue

      do 30 ICG = 1, NCONF(1) - 1
         call GETMUL (NG(ICG  ),IDIMG, IRG, 'GROUND')
   30    call GETMUL (NG(ICG+1),IDIMG, IRG, 'EXCITE')
      do 40 ICF = 1, NCONF(2) - 1
         call GETMUL (NF(ICF  ),IDIMF, IRF, 'GROUND')
   40    call GETMUL (NF(ICF+1),IDIMF, IRF, 'EXCITE')

      NOG    = NG(1) + N2 * (NG(2) + N3 * NG(3))
      NOF    = NF(1) + N2 * (NF(2) + N3 * NF(3))

C---- Calculate starting adresses of all matrices.
C---- Assume length of an integer =< length of real.
c *******
c alterations are marked by this or are in COWAN (+ alloc etc)
      call alloc('master',0, istart)
      call alloc('master', NOG, IWKG)
      call alloc('master', NOF, IWKF)
      call alloc('master', NOF, IWKF1)
      call alloc('master', NOG, IxEG)
      call alloc('master', NOF, IxEF)
      call alloc('master', NOF, ISPECT)
      call alloc('master', NOG**2, IHAMG)
      call alloc('master', NOG * NOF, ITRAN)
      call illoc('master', NOG, IGCO)
      call illoc('master', NOF, IFCO)

      rewind       14
      REWIND       15
      call NEWFIL (15)

      call master  (X(IHAMG), X(ITRAN),
     &            X(IWKG) ,X(IWKF) , X(IWKF1),
     &            X(IxEG) ,X(IxEF) , X(ISPECT),
     &            NCONF   ,nX(IGCO), nX(IFCO)  , NOG, NOF,
     &            NG  , NF ,
     &            IDIMG, IDIMF,   N2  , N3 , W  , DEL,  UVV, UCV,
     &            EGRND, EFXAS, DIPXAS, itriad, IRG, IRT, IRF, PRMUL,
     &            SwitchAuger,  EFAUG, NOMEGA, ESTART, ESTEP ,DIPx, CI)

      call cut_heap('master', istart)
      call CPUTIME
      old_cowan = .true.

   60 write(6,*) ('<>',I=1,35)
c---  end of loop over triads

   70 write(44,*) ' FINISHED '
c--   end of loop over calculations

c--   The end of the calculations is reached at a STOP in IOERR!
      end

C----------------------------------------------------------------------
      subroutine master
     &           (HAMG, TRAN,
     &            WKG   ,WKF   ,WKF1  ,
     &            EG    ,EF    ,SPECT,
     &            NCONF ,IGCONF,IFCONF,NOG  ,NOF  ,
     &            NG    ,NF ,
     &            IDIMG ,IDIMF ,N2    ,N3   ,W    ,DEL  ,UVV ,UCV,
     &            EGRND ,EFXAS ,DIPXAS,itriad,IRG ,IRT  ,IRF, PRMUL,
     &            SwitchAuger,  EFAUG, NOMEGA, ESTART, ESTEP ,DIPx, CI)
c --- implicit real*8 (a-h,o-z)

C
      dimension HAMG(NOG, NOG), TRAN(NOG, NOF),
     & WKG (NOG)     , WKF (NOF)     ,
     & WKF1(NOF)     , EG  (NOG)     , EF  (NOF)     , SPECT(NOF)    ,
     & IGCONF(NOG)   , IFCONF(NOF)   , NCONF(2)      , NG(3),   NF(3),
     & CHARAC(3)     ,
     & EGRND(3)      , EFXAS(3)

      integer mateg(3), matef(3), mata(3,3),
     &        matvg12,  matvg23,  matvf12,  matvf23,
     &        mathamg,  mathamf,  mattran
      parameter (lheap=999999999)
      common    /ACTORS / NADD(2,2,3,3) , XADD(10,2,2,3,3)
      common /heap     / x(lheap)
C
      character IRG *(*) , IRT *(*) , IRF *(*) , PRMUL *(*) , IR0,PRM
      character INFO*80
      COMMON    /information/INFO
*
      DIMENSION EFAUG(9,3), DIPx(9), CI(9)
      LOGICAL   SwitchAuger
      external  Diag
C
C
C^^^^ INFORMATION
      write(INFO,'(2I1,4F7.3,25X,3A5)')
     &  N2, N3, W, DEL, UVV, UCV, IRG,IRT,IRF

C^^^^ TRANSITION
C^^^^ Transitions from ground configurations 1,2,3 to excited

      call alloc('master', 0, i_cut)
      do 10 icg = 1, nconf(1)
      do 10 icf = 1, nconf(2)
        if (nadd(1,2,icg,icf) .ne. 0) then
          call PAIRIN ( mateg(icg), matef(icf), mata(icg,icf),
     &      NG(ICG), NF(ICF), IRG, IRT, IRF, PRMUL, 1, 2, ICG, ICF)
        endif
   10 continue

C^^^^^ MIXING
      IR0='0'
      PRM='0'
C
C     -- Ground --------------------------------------
      if (NCONF(1) .gt. 1 ) then
        call PAIRIN ( mateg(1), mateg(2), matvg12,
     &                NG(1),NG(2),IRG,IR0,IRG,PRM,1,1,1,2)
      endif
      if (NCONF(1) .gt. 2 ) then
        call PAIRIN ( mateg(2), mateg(3), matvg23,
     &                NG(2),NG(3),IRG,IR0,IRG,PRM,1,1,2,3)
      endif
C     -- FINAL --------------------------------------
      if (NCONF(2) .gt. 1 ) then
        call PAIRIN ( matef(1), matef(2), matvf12,
     &                NF(1),NF(2),IRF,IR0,IRF,PRM,2,2,1,2)
      endif
      if (NCONF(2) .gt. 2 ) then
        call PAIRIN ( matef(2), matef(3), matvf23,
     &                NF(2),NF(3),IRF,IR0,IRF,PRM,2,2,2,3)
      endif
C
      call WARN(0)
      call alloc('master', 0, i_block)
      call alloc('master', 0, i_cut1)
C
      call   make_hamiltonian
     & (mathamg, nog, mateg(1), mateg(2), mateg(3),
     &  ng(1), ng(2), ng(3), n2, n3,
     &  egrnd, idimg, matvg12, matvg23,
     &  igconf, w ,nconf(1))

C      call PRMAT  (HAMG, NOG, NOG, NOG, ' HAMILTON GROUND')

      call alloc('master', 0, iblock1)
      call expand(HAMG, x(mathamg), nog, nog)
      call remove_region('master', iblock1, i_cut1)

      call find_data(1, irg, diag, HAMG, NOG, NOG, EG, WKG)
C
      write(43,'(5(I4, F10.4))') (I, EG(I),  I=1,NOG)

      NDEG = 1
      do 400 I = 2,NOG
  400   if ( EG(I) - EG(1) .le. 1e-6 * (EG(NOG)-EG(1)) )
     &     NDEG = NDEG + 1

C---- Analysis of the ground state in terms of configurations ------
      call ZERO (CHARAC,3)
      do 430 IG = 1,NOG
         if (IGCONF(IG) .ne. 0) then
           ICON   = IGCONF(IG)
         endif
         do 420 IDEG = 1,NDEG
  420       CHARAC(ICON) = CHARAC(ICON) + HAMG(IG, IDEG)**2
  430 continue

      do 450 iu = 6, 44, 38
        write(iu,
     &  '(a,g17.10,a,i4,a /A,3F8.5 /a,t31,5f10.5/(8f10.5))')
     &          ' Ground state energy Eg0=',EG (1),' (',NDEG,' times)',
     &          ' Weight of configurations 1,2,3 in the ground state:',
     &            (CHARAC(ICON)/NDEG, ICON = 1,3),
     &          ' Lowest few energies ',(EG(i), i=1,min(NDEG+10,NOG))
  450 continue

      call zero(wkg,nog)
      do 530 ig1 = 1,nog
      do 530 IG = 1,NOG
         if (IGCONF(IG) .ne. 0) ICON   = IGCONF(IG)
         if (icon.eq.1) wkg(ig1) = wkg(ig1) + HAMG(IG, ig1)**2
  530 continue
C
      write(44,*) 'energies and purities'
      write(44, '( 5(f10.5,f6.3) )' ) (EG(I), wkg(i), I=1,min(NOG,40))

C
      if (SwitchAuger) call alloc('master', 0, i_cut2)

      call   make_hamiltonian
     & (mathamf, nof, matef(1), matef(2), matef(3),
     &  nf(1), nf(2), nf(3), n2, n3, Efxas,
     &  idimf, matvf12, matvf23, ifconf,
     &  w, nconf(2))
C      call PRMAT  (HAMF, NOF, NOF, NOF, ' HAMILTON EXCITE')
C
      if (SwitchAuger) call alloc('master', 0, iblock2)
      call alloc('master', 0, i_cut3)

      call   make_transition
     & (mattran, dipxas, nog, nof, igconf, ifconf,
     &  nadd, mata, ng, nf)
C
C
      call alloc('master', 0, iblock3)
      call remove_region('master', i_block, i_cut)
      call expand(TRAN, x(mattran), nog, nof)
      call remove_region('master', iblock3, i_cut3)

      IF (.NOT. SwitchAuger) THEN
        call timing(' before spectr_new ')
        call spectr_new(HAMG, TRAN, mathamf, nog, nof,
     &                  wkf,  wkf1, spect,   eg,  irg,
     &                  irt,  irf,  idimg,   idimf)

        write(44,'(I5,A,A,A    )')  NOF,'  ''',INFO(1:70),''''
        write(44,'(A)') '%Output of Ander , BANAUG7'
        write(44,'(A,F9.4,A,I3,A,F9.4,A,3F6.3)') '%Eg=',EG(1),
     &     ' NDeg=',NDEG,' : 1stExE=',EG(2),
     &     ' Charc=',(CHARAC(I)/NDEG,I=1,3)

      ELSE
         call alloc('master', nof * nof, ihamf)
         call expand(x(ihamf), x(mathamf), nof, nof)
         call remove_region('master', iblock2, i_cut2)
         call AUGER(NCONF,N2,N3,NG,NOG,HAMG,EG,NDEG,NF,NOF,
     &              X(IHAMF),EF,SPECT,TRAN,CHARAC,WKF,ifconf,irf,
     &              itriad,NOMEGA,ESTART,ESTEP,DIPx,CI,EFAUG,EFXAS)
      ENDIF
C
      end

      function bandshape(x,x1,x2)
c---  Defines the shape of the square of the density of states.
c---  rho**2= sum(i=0,nexpansion) [ a(i)*x**i ]
c---  The shape should be given as a function between e=-1 and e=+1.
c---  This routines scales it between x1 and x2. The vertical scale
c---  is of no importance.
      parameter (maxexpansion=25)
      common /bandexpansion/ nexpansion, ashape(0:maxexpansion)
c---  scale to x1...x2
      x0 = -1 + 2 * (x-x1) / (x2-x1)
      tailor = ashape(0)
      do 10 i = 1,nexpansion
   10   tailor = tailor + ashape(i) * x0**i
      bandshape = tailor
      end


      subroutine gausslegendre ( x1, x2, x, w, n)
c---  section 4.5 of numerical recipes
      parameter (eps=1E-6)
      real    x(n),w(n)
      integer m,j,i
      real    z1,z,xm,xl,pp,p3,p2,p1
      if ( x1.eq.x2 ) then
         if ( n.eq.1 ) then
            x(1) = x2
            w(1) = 1
         else
            print*, 'In gausslegendre x1=x2=',x1,' and n=',n
            print*, 'n>1 is not allowed when x1=x2.'
            call quitprog
     &      ( 'Make W non-zero or make N2 and/or N3 =1 in INPUT.' )
         endif
      else
C here begins the real gauss legendre algorithm
         m= (n+1)/2
         xm = .5 * (x2+x1)
         xl = .5 * (x2-x1)
         do 10 i=1,m
            z = cos( atan(1.)*4* (i-.25) / (n+.5) )
 20         p1 = 1
            p2 = 0
            do 30 j = 1,n
               p3 = p2
               p2 = p1
               p1 = (( 2*j-1) * z * p2 - (j-1) * p3 ) / j
 30         continue
            pp = n * (z*p1-p2) / (z**2-1)
            z1 = z
            z  = z1 - p1/pp
            if (abs(z-z1) .gt. eps) goto 20
            x(i) = xm - xl * z
            x(n+1-i) = xm + xl * z
            w(i) = 2*xl/((1-z**2) * pp**2)
            w(n+1-i) = w(i)
 10      continue

c---  Normalize such that Integral( bandshape**2 ) = 1
         sum = 0
         do 50 i=1,n
            sum = sum + bandshape( x(i), x1, x2 )**2 * w(i)
 50      continue
         do 60 i = 1,n
            w(i) = bandshape( x(i), x1, x2) * sqrt(w(i)/sum)
 60      continue
      endif

      print*,'gausslegendre ',(i,x(i),w(i),i=1,n)
      end

C----------------------------------------------------------------------
      subroutine make_hamiltonian
     & (matham, nos,  mates1, mates2, mates3,
     &  ns1, ns2, ns3,  n2, n3, Ens, idim,
     &  matv12, matv23, iconf, w, nconf)
      integer matham, mates1, mates2, mates3, matv12, matv23
      dimension iconf(nos), Ens(3)
      parameter (maxlevels=100)
      dimension ek(maxlevels,2:3), vk(maxlevels,2:3)
      parameter (max_add=10000)
      PARAMETER (LINDX=1000)
      parameter (lheap=999999999)
      common /INDX/ mtrx(LINDX)
      common /heap/ x(lheap)
      common /ibuild/ iindx(max_add), ibra(max_add), iket(max_add),
     &                nbra(max_add), nket(max_add), fact2(max_add),
     &                shift(max_add)
C

      call izero  ( ICONF , NOs )

      if (nconf.ge.2)  call gausslegendre(-W/2,W/2,ek(1,2),vk(1,2),n2)
      if (nconf.ge.3)  call gausslegendre(-W/2,W/2,ek(1,3),vk(1,3),n3)

      indx  = 1
      IX1   = indx
      indx  = indx + ns1
      ICONF (IX1) = 1
      ELEV1 = Ens(1)
c fill the INDX common block
      mtrx(1) = mates1
      mtrx(2) = mates2
      mtrx(3) = mates3
      mtrx(4) = matv12
      mtrx(5) = matv23
c now fill the common ibuild
      i = 1
      iindx(i) = 1
      ibra(i) = ix1
      iket(i) = ix1
      nbra(i) = Ns1
      nket(i) = Ns1
      fact2(i) = 1 / SQRT(FLOAT(IDIM))
      shift(i) = ELEV1
      if (nconf .ge. 2) then
        do 20 K2 = 1, N2
           i = i + 1
           IX2   = indx
           indx  = indx + ns2
           ICONF (IX2) = 2
           ELEV2 = ELEV1 + Ens(2) - Ens(1) + EK(K2,2)

           iindx(i) = 2
           ibra(i) = ix2
           iket(i) = ix2
           nbra(i) = Ns2
           nket(i) = Ns2
           fact2(i) = 1 / SQRT(FLOAT(IDIM))
           shift(i) = ELEV2

           i = i + 1
           iindx(i) = 4
           ibra(i) = ix1
           iket(i) = ix2
           nbra(i) = Ns1
           nket(i) = Ns2
           fact2(i) = Vk(k2,2) / SQRT(FLOAT(IDIM))
           shift(i) = 0.000

           if (nconf.ge.3) then
             do 10 K3 = 1, N3
               i = i + 1
               IX3   = indx
               indx  = indx + ns3
               ICONF (IX3) = 3
               ELEV3 = ELEV2 + Ens(3) - Ens(2) + EK(K3,3)

               iindx(i) = 3
               ibra(i) = ix3
               iket(i) = ix3
               nbra(i) = Ns3
               nket(i) = Ns3
               fact2(i) = 1 / SQRT(FLOAT(IDIM))
               shift(i) = ELEV3

               i = i + 1
               iindx(i) = 5
               ibra(i) = ix2
               iket(i) = ix3
               nbra(i) = Ns2
               nket(i) = Ns3
               fact2(i) = Vk(k3,3) / SQRT(FLOAT(IDIM))
               shift(i) = 0.000

  10         continue
           endif
  20    continue
      endif
      call sp_build(matham, i, NOs, NOs, .true.)

      end

C----------------------------------------------------------------------
      subroutine make_transition
     & (mattran, dipxas, nog, nof, igconf, ifconf, nadd, mata, ng, nf )

c--   transition matrix is constructed. The whole matrix is multiplied
c--   by dipxas. In Auger the continuum transitions are corrected for
c--   this in rescale.
      integer    mattran, igconf(nog), ifconf(nof), nadd(2,2,3,3),
     &           mata(3,3), ng(3), nf(3)
      dimension  kg(3), kf(3)
      parameter (max_add=10000)
      PARAMETER (LINDX=1000)
      common /INDX/ mtrx(LINDX)
      common /ibuild/ iindx(max_add), ibra(max_add), iket(max_add),
     &                nbra(max_add), nket(max_add), fact2(max_add),
     &                shift(max_add)
C
      call izero(kg,3)
      i = 0
      j = 0
      do 1 idx = 1, 3
         do 2 jdx = 1, 3
            j = j + 1
            mtrx(j) = mata(idx, jdx)
 2       continue
 1    continue
      do 100 IG = 1,NOG
         ICG = IGCONF(IG)
         if (ICG .ne. 0) then
           kg(icg) = kg(icg) + 1
           call izero(kf,3)
           do 90 IF = 1, NOF
             ICF = IFCONF(IF)
             if (ICF .ne. 0) then
               kf(icf) = kf(icf) + 1
               if (NADD(1,2,ICG,ICF) .ne. 0) then
c-               only states with the same parentage may be connected
                 do 80 ic = 1, min(icg,icf)
                    if (kg(ic) .ne. kf(ic)) goto 90
   80            continue
                 i = i + 1
                 iindx(i) = (icg - 1) * 3 + icf
                 ibra(i) = ig
                 iket(i) = if
                 nbra(i) = ng(icg)
                 nket(i) = nf(icf)
                 fact2(i) = dipxas
                 shift(i) = 0.000
               endif
             endif
   90      continue
        endif
  100 continue

      call sp_build( mattran, i, nog, nof, .false.)
      end

C----------------------------------------------------------------------
      subroutine spectr_new(hamg, tran, mathamf, nog, nof,
     &                      a, b, spect, eg, irg,
     &                      irt, irf, idimg, idimf)
      dimension hamg(nog,nog), tran(nog,nof), hamf(nof,nof),
     &          a(nof), b(nof), spect(nof), eg(nog), Ener(nof)

      integer mathamf
      parameter (lheap=999999999)
      common /spectr/ erange
      common /heap     / x(lheap)
      common /heap_info/ last, max_used
      character *20  irg, irt, irf
      dimension nx(lheap)
      integer nogActual
      equivalence (x, nx)
      external tred3

      call expand(HAMF, x(mathamf),nof,nof)

      call alloc('spectr_new work', nof, iWork2)
      call alloc('spectr_new worktemp',nof,iWork2Temp)
      call find_data(2, irf,tred3,HAMF,nof,nof,Ener,x(iWork2))


      do 100 ideg = 1,nog
        if (eg(ideg) - eg(1) .le. erange) then
          call zero (spect, nof)
          do 40 j=1,nof
            a(j) = Ener(j)
            x(iWork2Temp + j) = x(iWork2 + j)
             do 40 k=1,nog
                spect(j) = spect(j) + hamg(k,ideg) * tran(k,j)
   40     continue

          call eigen2(2, irf, HAMF, nof, nof, a, spect, 1, iWork2Temp)
          nlast = nof

C          !call timing(' time before tridiag ')
C          !call tridiag(x(mathamf), spect, nof, a, b, nlast)
c          !total_intensity = b(1)
c          !call zero   (spect, nlast)
c          !spect(1) = total_intensity
c          !call timing(' time before tql3 ')
c          !call tql3 (1, nlast, a, b, spect, ierr)
c          !call timing('time before primat ')
c          !do 60 j=1,nlast
c!   60       spect(j) = spect(j)**2
cgroningen
C           call OUTPUT
C     &        (0,0,nlast,a,spect,1,1,EG,CHARAC,total_intensity)

C mine !!!
C!            call condens
C!     &     ( a, spect, nlast, total_intensity*1e-6, .0001, n_condens )
            call primat (0, eg(ideg), a, spect, nlast, irg,irt,irf,
     &               idimg, idimf)
cjapan
c           call primat_japan ( eg, a, spect, nlast )

          endif
 100    continue

      call timing(' time end of calculation ')
      call dealloc ('spectr_new work', nof, iWork2)
      call dealloc ('spectr_new worktemp', nof, iWork2Temp)
      write(6,* ) 'Maximum heap used:',max_used

      end

C----------------------------------------------------------------------
      subroutine Tridiag(mat, f, n, a, b, nlast )
C..
C.. Calculates a tridiagonal form for the Hamiltonian specified by
C.. the external subroutine
C..            "    ham_times_vec( mat, f0, f, n )   "
C.. where "f0" and "f" are real arrays of dimension "n", and
C.. H(f0) = f . "H" is sparse. "a" and "b" define the resulting diagonal and
C.. off-diagonal matrix elements. "f" is taken as starting state.
C.. "nlast" gives the size of the tridiagonal matrix,
C.. ref. : 'Memory function approaches', eds. Evans, Grigolini and
C..        Pastori Parravicini, page 146-148.
C..
      REAL f(n), a(n), b(n)
      integer mat(3,n)
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      call alloc('tridiag',n,ifn)
      call alloc('tridiag',n,ifnm1)
      call alloc('tridiag',n,ifnp1)
      call tridiag_x( mat, f, n, a, b, nlast,
     &                      x(ifn), x(ifnm1), x(ifnp1) )
      call cut_heap('tridiag', ifn)
      end

C----------------------------------------------------------------------
      subroutine tridiag_x( mat, f, n, a, b, nlast,
     $                      fn, fnm1, fnp1 )

      integer mat(3,n)
      REAL f(n), a(n), b(n)
      REAL fn(n), fnm1(n), fnp1(n)
      common/iter/max_iter

      call zero(a, n)
      call zero(b, n)
      call zero(fn, n)
      call z_is_x(fnp1, f, n)

      DO 100 lus = 1, min(max_iter, n)
        call z_is_x (fnm1, fn,   n)
        call z_is_x (fn,   fnp1, n)
        b(lus) = vecabs(fn, n)
        IF( b(lus).eq.0 ) RETURN
        call z_is_a_y(fn, 1/ b(lus), fn, n)
        call Ham_times_vec (mat, fn, fnp1, n)
        a(lus) = dot(fnp1, fn, n)
        nlast = lus
        do 50 i=1,n
          fnp1(i) = fnp1(i) - a(lus) * fn(i) - b(lus) * fnm1(i)
 50     continue
100   CONTINUE

      END

C----------------------------------------------------------------------
      subroutine Ham_times_vec(mat, fin, fuit, n )
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)
      REAL fin(n), fuit(n)
      integer mat(3,n)

      call zero (fuit, n)
      do 1 i=1,n
        if (fin(i).ne.0.0) call row_times_x
     &      ( x(mat(1,i)), nx(mat(2,i)), mat(3,i), fin(i), fuit, n )
 1    continue
      end

C----------------------------------------------------------------------
      subroutine row_times_x( x, nx, lrow, fini, fuit, n )
      dimension x(lrow), nx(lrow), fuit(n)
      do 1 k = 1, lrow
 1      fuit(nx(k)) = fuit(nx(k)) + x(k) * fini
      end

C----------------------------------------------------------------------
      subroutine z_is_x( z, x, n )
      real z(n), x(n)
      do 1 i = 1, n
    1   z(i) = x(i)
      end

C----------------------------------------------------------------------
      subroutine z_is_a_y( z, a, y, n )
      real z(n), a, y(n)
      do 1 i = 1, n
    1   z(i) = a * y(i)
      end

C----------------------------------------------------------------------
      subroutine fill_vector(row, mat, irow, icol, jrow, fact, shift)
c used by sp_BUILD to build up a matrix row
c by calling this for the correct row of all submatrixes that cut this row.
c The jrow-th row of mat has to be added starting at the icol-th column
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)
      dimension row(irow+1), mat(3,jrow+1)
      call fill_vector_x(
     &   row(icol), x(mat(1,jrow)), nx(mat(2,jrow)), mat(3,jrow), fact)
c this is the shift which should only be nonzero for hamiltonian blocks
      if (shift.ne.0) row(irow) = row(irow) + shift
      end

C----------------------------------------------------------------------
      subroutine fill_vector_x( row, x, nx, lrow, fact)
      dimension row(*), nx(lrow+1), x(lrow+1)
      do 10 i=1,lrow
   10     row(nx(i)) = row(nx(i)) + x(i) * fact
      end

C----------------------------------------------------------------------
      subroutine symmetrise( mat, row, ng, nf, irow )
c     used by sp_BUILD to fill the part of the row in
c     the lower triangle in a Hamiltonian matrix
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)
      dimension row(nf+1), mat(3,ng+1)

      do 10 jcol = 1, irow - 1
         row(jcol) = find_element_of_sparse_row
     &      (x(mat(1,jcol)), nx(mat(2,jcol)), mat(3,jcol), irow)
   10 continue
      end

C----------------------------------------------------------------------
      function find_element_of_sparse_row(x,nx,lrow,i)
      dimension x(i+1), nx(i+1)
      find_element_of_sparse_row = 0
      do 10 j = 1, lrow
        if(nx(j).ge.i) then
          if(nx(j).eq.i) find_element_of_sparse_row = x(j)
          return
        endif
   10 continue
      end

C----------------------------------------------------------------------
      subroutine vec_to_row(mat, row, nf, i)
c used by sp_BUILD to shift the full row vector into a sparse matrix row
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap),    mat(3,i+1), row(nf+1)
      equivalence (nx,x)

      non0 = 0
      do 10 j = 1, nf
         if (row(j).ne.0.0) non0 = non0 + 1
 10   continue
      call alloc('vector to row', non0, mat(1,i))
      call illoc('vector to row', non0, mat(2,i))
      mat(3,i) = non0
      call vec_to_row_x( x(mat(1,i)), nx(mat(2,i)), row, nf)
      end

C----------------------------------------------------------------------
      subroutine vec_to_row_x( x, nx, row, n)
      dimension x(n+1), nx(n+1), row(n+1)
      k = 0
      do 20 j = 1, n
         if (row(j).ne.0.0) then
            k = k + 1
            nx(k) = j
            x (k) = row(j)
         endif
 20   continue
      end

C----------------------------------------------------------------------
      subroutine sp_BUILD(mat, iidx, ng, nf, symm)
c --- implicit real*8 (a-h,o-z)
      parameter (max_add=10000)
      parameter (LINDX=1000)
      common /INDX/ mtrx(LINDX)
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)
      common /ibuild/ iindx(max_add), ibra(max_add), iket(max_add),
     &                nbra(max_add), nket(max_add), fact2(max_add),
     &                shift(max_add)
      logical symm
      call CHECK (iidx, max_add, 'MAX_ADD' , .FALSE.)

c get the space we need
      call illoc('Sparse Build', 3*ng , mat)
c allocate the temporary row used for creation
      call alloc('Sparse Build', nf, iz)
c now build up each row of the matrix

      do 10 irow = 1, ng
         call zero(x(iz), nf)
         do 20 j = 1, iidx
            if ((ibra(j).le.irow).and.(ibra(j)+nbra(j).gt.irow)) then
               jrow = irow - ibra(j) + 1
               call fill_vector(x(iz), nx(mtrx(iindx(j))),
     &           irow, iket(j), jrow, fact2(j), shift(j) )
            endif
 20      continue
         if (symm) call symmetrise( nx(mat), x(iz), ng, nf, irow)
         call vec_to_row( nx(mat), x(iz), nf, irow)
 10   continue

c get rid of the temporary space
      call remove_region('Sparse Build', iz, iz)
      end

C----------------------------------------------------------------------
      subroutine PAIRIN(mateg, matef, mata, NG, NF, IRG, IRT, IRF,
     &                  PRMUL, ITASKG, ITASKF, ICONFG, ICONFF)
c --- implicit real*8 (a-h,o-z)
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)
      common    /ACTORS / NADD(2,2,3,3) , XADD(10,2,2,3,3)
      character IRG *(*) , IRT *(*), IRF *(*), TEXT(2) *(6)
     &           ,PRMUL *(*)
      data TEXT / 'ground','excite' /
      integer mateg, matef, mata

      if (NADD  (ITASKG, ITASKF, ICONFG, ICONFF) .eq. 0) return
      write(6,*)
      write(6,'(3a,i1,3a,i1,a,i6,a,i6)')
     & '=========== PAIR  ',TEXT(ITASKG),' ',ICONFG,' => ',
     & TEXT(ITASKF),' ',ICONFF,'   ',NG,' * ',NF
      call NEWFIL (14)
      call alloc ('pairin',0, i_cut)
      call timing ('begin of cowan')
      call COWAN
      call timing ('end of cowan')
      call alloc ('pairin', 0, iblock)
      call NEWFIL (15)
C---- ------------ transition  matrices --------------------
c      call ZERO ( A  , NG*NF )
      i = 1
      do 10 IADDT = 1, NADD  (ITASKG, ITASKF, ICONFG, ICONFF)
   10    call ETR(NG, NF, IRG, IRT, IRF, PRMUL, 'TRANSI', i,
     &            XADD(IADDT, ITASKG, ITASKF, ICONFG, ICONFF))
      call sp_BUILD(mata, i - 1, ng, nf, .false.)
C---- first  state hamiltonian matrices --------------------
c      call ZERO ( EG , NG*NG )
      i = 1
      do 20 IADDG = 1, NADD  (ITASKG, ITASKG, ICONFG, ICONFG)
   20    call ETR(NG, NG, IRG, '0', IRG, '0', 'GROUND', i,
     &              XADD(IADDG, ITASKG, ITASKG, ICONFG, ICONFG))
      call sp_BUILD(mateg, i - 1, ng, ng, .false.)

C---- second state hamiltonian matrices --------------------
c      call ZERO ( EF , NF*NF )
      i = 1
      do 30 IADDF = 1, NADD  (ITASKF, ITASKF, ICONFF, ICONFF)
   30    call ETR(NF, NF, IRF, '0', IRF, '0', 'EXCITE', i,
     &              XADD(IADDF, ITASKF, ITASKF, ICONFF, ICONFF))
      call sp_BUILD(matef, i - 1, nf, nf, .false.)

      call remove_region ('pairin', iblock, i_cut)
      end

C----------------------------------------------------------------------
      subroutine GETMUL ( MULT , IDIM , IR , TASK )
c --- implicit real*8 (a-h,o-z)
      character TASK *(*) , IR *(*) , CHAGET *20
      write(6,'(A,A8,2X,A6,A)') ' Looking for  irrep ',TASK,IR,'  MULT'
   10 call NEWLIN
      if ( CHAGET() .NE. 'IRREP'     ) GOTO 10
      if ( INDEX(CHAGET(),TASK).eq.0 ) GOTO 10
      if ( CHAGET() .NE.  IR         ) GOTO 10
      call   CHCKCH ('MULT')
      MULT = INTGET ( MULT )
      call   CHCKCH ('DIM' )
      call   CHCKIN ( IDIM )
      write(6,'(A,A8,2X,A6,A,I5,A,I2)')
     &' multiplicity found ',TASK,IR,'  mult=',MULT,'  dim=',IDIM
      RETURN
      end

C----------------------------------------------------------------------
      subroutine ETR(NG, NF, IRG, IRT, IRF, PRMUL, TASK, i, fact)
c --- implicit real*8 (a-h,o-z)
      character *(*)            IRG, IRT, IRF, PRMUL, TASK
      character *20  CHAGET, IRT1, NAME, WORD
      parameter (max_add=10000)
      common    /CPRMULT/ PRMULT
      common /ibuild/ iindx(max_add), ibra(max_add), iket(max_add),
     &                nbra(max_add), nket(max_add), fact2(max_add),
     &                shift(max_add)
      logical         PRMULT

      IRT1 = IRT
      if (IRT1.eq.'0' .AND.
     &   (INDEX(IRG,'+ ').NE.0 .OR. INDEX(IRG,'- ').NE.0) ) IRT1 ='0+'
      write(6,'(A,A8,2X,3A6,2I5,A,F12.7)')
     & ' looking for ',
     &   TASK, IRG, IRT1, IRF, NG, NF, ' Factor =',fact
      call STATUS ('ETR')
   10 call NEWLIN
      if (INDEX(CHAGET(),'REDUCEDMATRIX') .eq.0 ) GOTO 10
      if (INDEX(CHAGET(), TASK          ) .eq.0 ) GOTO 10
      if (      CHAGET() .NE.   IRG             ) GOTO 10
      if (      CHAGET() .NE.   IRT1            ) GOTO 10
      if (      CHAGET() .NE.   IRF             ) GOTO 10
      if (PRMULT) then
         if (CHAGET() .ne. PRMUL) goto 10
      endif
      NAME  =   CHAGET()
      call CHCKIN (NG)
      call CHCKIN (NF)
      call PUTINP
   20 WORD = CHAGET()
      if     ( WORD .eq. 'ADD' ) then
         call CHECK (i, max_add, 'MAX_ADD' , .FALSE.)
         IINDX(i)  = INTGET (j)
         IBRA(i)   = INTGET (j)
         IKET(i)   = INTGET (j)
         NBRA(i)   = INTGET (j)
         NKET(i)   = INTGET (j)
         FACT2(i)  = fact * REAGET (e)
         shift(i) = 0.000
         i = i + 1
      elseif ( WORD .eq. 'END' ) then
         call CHCKCH(NAME)
         return
      else
         write(6,*) 'Expected "ADD" or "END", got: ',word,' in:'
         call WARN(10)
      endif
      goto 20
      end

C----------------------------------------------------------------------

      subroutine expand( z, mat, nbra, nket )
c     dimension z(nbra, nket), mat(3,nbra)
      dimension z(*), mat(3,*)
      parameter   (lheap=999999999)
      common      /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence (nx,x)

      call zero(z, nbra * nket)
      do 1 irow = 1, nbra
         call expand_row( z, nbra, nket, irow,
     &        x(mat(1,irow)), nx(mat(2,irow)), mat(3,irow) )
 1    continue
      end

C----------------------------------------------------------------------
      subroutine expand_row ( z, nbra, nket, irow, x, nx, lrow )
c     dimension  z(nbra,nket), x(lrow), nx(lrow)
      dimension  z(nbra,*), x(*), nx(*)
      do 1 j = 1, lrow
 1      z(irow,nx(j)) = x(j)
      end

C----------------------------------------------------------------------
      subroutine COWAN
c --- implicit real*8 (a-h,o-z)
      character *20  CHAGET, NAME, WORD, TASK, IRG, IRT, IRF
      common    /qaz / old_cowan, intsize
      logical    old_cowan
      parameter   (lheap=999999999)
      common /heap/ x(lheap)
      dimension    nx(lheap)
      equivalence    (x, nx)
      PARAMETER (LINDX=1000)
      common /INDX/ mtrx(LINDX)
      integer size(LINDX), nr(LINDX), icoldx(LINDX), matrix(LINDX)

c---  Supergroup reduced matrix elements are read from unit 14 in
c---  the first passage through the main loop in the master program (BAND)
c---  and are written to unit 99.
c---  At subsequent passages these matrix elements are read from unit 99.
c---  Reason: reading the coded file 14 is slow.
c---          Reading the binary file 99 is very fast.

      if (old_cowan) then
        read(99) lstore, iindx
        write(6,'(a/ a,i12/ a,i5)')
     &  ' Cowan store will be read from file 99.',
     &  ' Total length =',lstore,
     &  ' Number of matrixes  =', iindx
        read(99) (size(i), nr(i), i=1, iindx)
        do 2 i=1,iindx
           call illoc ('old cowan', 3*nr(i), mtrx(i))
           call illoc ('old cowan', size(i), icoldx(i))
           call alloc ('old cowan', size(i), matrix(i))

           call rint99 (nx(mtrx(i)),   3*nr(i))
           call rint99 (nx(icoldx(i)), size(i))
           call read99 ( x(matrix(i)), size(i))

c          the values in the element index and the column index
c          have to be shifted to the present allocation
           call shift_index( nx(mtrx(i)), matrix(i), icoldx(i), nr(i) )
 2      continue
        return
      endif

      call   STATUS ('COWAN near begin')
      do 4 IINDX  = 1,LINDX
    4    matrix(IINDX) = 10000000
      IINDX  = 1
      istore = 0
      isparse = 0
      isize = 0

   10 WORD = CHAGET ()
      call   STATUS ('COWAN at begin loop')
      if (WORD  .eq. 'FINISHED'  ) then
         write(6,'(a,i9,a)') ' In store: ',IINDX-1,' matrixes.'
         print *, 'Total matrix size ', isize, ' with ',
     &            isparse, ' elements'
         write(99)  isparse, iindx-1
         write(99) (size(i), nr(i), i=1,iindx-1)
         do 5 i=1,iindx-1
            call wint99  (nx(mtrx(i)),   3*nr(i))
            call wint99  (nx(icoldx(i)), size(i))
 5          call write99 ( x(matrix(i)), size(i))
         write(6,*) ' Cowan store written to file 99.'
         return

      endif
      if (WORD(1:3) .ne. 'RME') goto 10
      TASK = CHAGET()
      IRG  = CHAGET()
      IRT  = CHAGET()
      IRF  = CHAGET()
      NAME = CHAGET()
      NBRA = INTGET (NBRA)
      NKET = INTGET (NKET)
      nonzero=INTGET(nonzero)
      call CHECK (IINDX, LINDX, 'LINDX' , .FALSE.)
      nr(IINDX) = NBRA
      size(IINDX) = nonzero
      isparse = isparse + nonzero
      isize = isize + nbra * nket
C******** shouldn't be here
      call illoc ('cowan', 3*nr(iindx), mtrx(iindx))
      call illoc ('cowan', size(iindx), icoldx(iindx))
      call alloc ('cowan', size(iindx), matrix(iindx))
      call rncowan( nx(mtrx(iindx)), x(matrix(iindx)),nx(icoldx(iindx)),
     &             nbra, nonzero)
      call shift_index( nx(mtrx(iindx)), matrix(iindx), icoldx(iindx),
     &             nbra)
      call STATUS ('COWAN near end')
      IINDX  = IINDX  + 1
      goto 10
      end

C----------------------------------------------------------------------
      subroutine shift_index(mat, matrix1, icoldx1, nrows )
c     integer mat(3,nrows)
      integer mat(3,*    )
      matrix_shift = matrix1 - mat(1,1)
      icoldx_shift = icoldx1 - mat(2,1)
      do 1 irow = 1, nrows
        mat(1,irow) = mat(1,irow) + matrix_shift
        mat(2,irow) = mat(2,irow) + icoldx_shift
   1  continue
      end

C----------------------------------------------------------------------
      subroutine rncowan(mat, xmatrix, icoldx, nrows, nonzero)
      parameter (lheap=999999999)
      common /heap/ x(lheap)
      dimension    xmatrix(nonzero+1), icoldx(nonzero+1)
      dimension mat(3,nrows+1)
      integer totn0

c     initialise because empty rows are not on file.
c     mat(1,1) has to be 1 because of shift_index. Element 1 corresponds
c     to the start address of xmatrix and icoldx
      do 3 irow = 1, nrows
        mat(1,irow) = 1
        mat(2,irow) = 1
   3    mat(3,irow) = 0
      totn0=0
      do 1 irow = 1, nrows
         if (totn0.eq.nonzero) return
         read (14, *,err=10) ibra, non0,
     &        (icoldx(totn0+j), xmatrix(totn0+j),j=1,non0)
         mat(1,ibra) = totn0 + 1
         mat(2,ibra) = totn0 + 1
         mat(3,ibra) = non0
         totn0 = totn0 + non0
 1    continue
c      call sp_prindex(mat,nrows)
      return
   10 write(6,*) 'A read error occurred on file 14. The numbers are ',
     &'not in the rigth order or wrong characters are present. ',
     &'The error occurred in the matrix after the following position:'
      call warn(10)
      end

C---------------------------------------------------------------------
      subroutine wint99 (m, n)
      dimension m(*)
      write(99) (m(i),i=1,n)
      end

C----------------------------------------------------------------------
      subroutine write99 ( x, n )
      dimension x(*)
      write(99) (x(i),i=1,n)
      end

C---------------------------------------------------------------------
      subroutine rint99 (m, n)
      dimension m(*)
      read(99) (m(i),i=1,n)
      end

C----------------------------------------------------------------------
      subroutine read99 ( x, n )
      dimension x(*)
      read(99) (x(i),i=1,n)
      end

C----------------------------------------------------------------------
      subroutine sp_prmat(mat,nbra,nket)
      integer mat(3,nbra+1)
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      call alloc('sp_prmat',nbra*nket,iz)
      call expand(x(iz),mat,nbra,nket)
      call prmat(x(iz),nbra,nket,nket,'sp_prmat')
      call dealloc('sp_prmat',nbra*nket,iz)
      end

C----------------------------------------------------------------------
      subroutine sp_prindex(mat,nbra)
      integer mat(3,nbra+1)
      print'(a/(3i10))','mat',((mat(i,j),i=1,3),j=1,nbra)
      end

C----------------------------------------------------------------------
c non standard? The following routines are standard fortran.
c--- Five basis memory allocation routines. Comment these out if you
c--- replace them by others.
C----------------------------------------------------------------------
       subroutine mem_alloc ( istart, n )
       parameter (lheap=999999999)
       common /heap_info/ last, max_used
       istart = last + 1
       call check ( istart + n, lheap, 'lheap', .false. )
       end
cC----------------------------------------------------------------------
       subroutine free_mem ( istart, n )
cc---   release one block, with start address istart, from the heap.
cc---   Not necessarily the last allocated one.
       end
ccC----------------------------------------------------------------------
       subroutine cut_blocks ( i_cut, last )
cc---   release blocks from the heap, starting with the one with start
cc---   address i_cut, and all those requested by later alloc calls.
       call dealloc ('cut_heap',last-(i_cut-1), i_cut)
       end
cc
cC----------------------------------------------------------------------
       subroutine cut_region (istart, i_cut, last)
cc---   cut out a section of the heap from i_cut to istart, inclusive.
cc---   i_cut and istart must be start adresses returned by alloc.
cc---   i_cut must have been requested before istart. Care is required.
       end
cc
cC----------------------------------------------------------------------
       subroutine check_heap ()
cc---   check the integrity of the heap. (Dummy here).
       end
c
C----------------------------------------------------------------------
      subroutine debugheap ( name, n, istart, last, text )
      character name*(*),text*(*)
c      print'(1x,a10,3i10,a)', name,n,istart,last,text
      end

C----------------------------------------------------------------------
      subroutine alloc ( text, n, istart)
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      common /heap_info/ last, max_used
      character*(*) text
      call check_heap()
      call mem_alloc(istart, n)
      last   = last + n
      max_used = max (max_used, last)
      call debugheap('alloc',n,istart,last,text)

c--- Initialize to a large number so that if the user does not initialize
c--- correctly himself, he will get bad results.
      do 1 i = istart,istart + n - 1
    1   x(i) = 1e30
      end

C----------------------------------------------------------------------
      subroutine illoc ( text, n, istart)
      common    /qaz / old_cowan, intsize
      character*(*) text
      call alloc ( text, (n+intsize-1)/intsize, istart)
      istart = (istart -1) * intsize+1
      end

C----------------------------------------------------------------------
      subroutine deilloc ( text, n, istart)
      common    /qaz / old_cowan, intsize
      character*(*) text
      call dealloc (text, (n+intsize-1)/intsize, (istart-1)/intsize + 1)
      end

C----------------------------------------------------------------------
      subroutine dealloc ( text, n, istart)
c--- release n numbers from the end of the heap
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      common /heap_info/ last, max_used
      character*(*) text
      do 1 i = istart,istart + n - 1
    1   x(i) = 1e30
      call free_mem(istart, n)
      last = last - n
      call debugheap('dealloc',n,istart,last,text)
      end

C----------------------------------------------------------------------
      subroutine remove_region(text, istart, i_cut)
c--- cut out a section of the heap from i_cut to istart, inclusive.
c--- i_cut and istart must be start adresses returned by alloc.
c--- i_cut must have been requested before istart. Care is required.
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      common /heap_info/ last, max_used
      character*(*) text
      call cut_region(istart, i_cut, last)
      call debugheap('remove_rgn',n,istart,last,text)
      end

C----------------------------------------------------------------------
      subroutine cut_heap ( text, i_cut )
c--- deallocate space starting at i_cut. To be used when after a number
c--- of calls to alloc all allocated space has to be freed.
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      common /heap_info/ last, max_used
      character*(*) text
      call cut_blocks(i_cut, last)
      call debugheap('cut_heap',n,istart,last,text)
      end


C----------------------------------------------------------------------
      subroutine CHCKIN(I)
c --- implicit real*8 (a-h,o-z)
      J = I
      if ( INTGET(I) .NE. J ) THEN
         IF ( J .NE. 0 ) THEN
            write(6,*) 'expected: ', J ,', got:', I ,' in:'
            call WARN(10)
         endif
      endif
      end

C----------------------------------------------------------------------
      subroutine CHCKRE(R)
c --- implicit real*8 (a-h,o-z)
      S = R
      if (abs( REAGET(R) - S ) .gt. 1E-20) then
         if ( abs(S) .gt. 1E-20 ) then
            write(6,*) 'expected: ', S ,', got:', R ,' in:'
            call WARN(10)
         endif
      endif
      end

C----------------------------------------------------------------------
      subroutine CHCKCH ( WORD )
c --- implicit real*8 (a-h,o-z)
      character WORD *(*) , WORD1 *20 , CHAGET *20
      WORD1 = CHAGET()
      if (WORD1 .NE. WORD) then
         write(6,*) 'expected: ', WORD ,', got:', WORD1 ,' in:'
         call WARN(10)
      endif
      end

C----------------------------------------------------------------------
      subroutine NEWLIN
c --- implicit real*8 (a-h,o-z)
      character LINE *136
      common    /CHLINE/ LINE
      common    /INLINE/ IPOS , ILINE , IFILE
      read(IFILE, '(A)',IOSTAT=IO) LINE
      call IOERR (IO , 'NEWLIN')
      ILINE = ILINE + 1
      IPOS  = 1
      if (IFILE .eq. 50) then
cjapan
c-- all characters before IPOS are discarded from all lines from standard
c-- input (50). To supress line numbers, for instance.
        IPOS = 1
        call UPPERCASE (LINE)
        write (44,'(1x,a79)') LINE(1:79)
      endif
      end

C----------------------------------------------------------------------
      function CHAGET()
c --- implicit real*8 (a-h,o-z)
      character LINE *136 , CHAGET *20 , WORD *20
      common    /CHLINE/ LINE
      common    /INLINE/ IPOS , ILINE , IFILE
      WORD = ' '
    5 if (    IPOS.eq.137     ) call NEWLIN
      do 10 IPOS = IPOS,136
   10    if (LINE(IPOS:IPOS) .NE. ' ' ) GOTO 20
      IPOS = 137
      GOTO 5
   20 if (LINE(IPOS:IPOS) .eq. '%') THEN
         call NEWLIN
         GOTO 5
      endif

      JPOS = 0
      do 30 IPOS = IPOS,136
         if (LINE(IPOS:IPOS) .eq. ' ' ) GOTO 50
         JPOS = JPOS + 1
         if (JPOS.gt.20) then
            write(6,*) 'string longer than 20 characters is not allowed'
            call  WARN(10)
         endif
   30    WORD(JPOS:JPOS) = LINE(IPOS:IPOS)
      IPOS = 137
   50 CHAGET = WORD
      end

C----------------------------------------------------------------------
      function INTGET (NUMBER)
c --- implicit real*8 (a-h,o-z)
      character WORD *20 , CHAGET *20
      NUMBER = 0
      WORD = CHAGET()
      read(WORD, '(BN,I20)' , IOSTAT=IO) NUMBER
      call IOERR (IO , 'INTGET')
      INTGET = NUMBER
      end

C----------------------------------------------------------------------
      function REAGET (REANUM)
c --- implicit real*8 (a-h,o-z)
      character WORD *20 , CHAGET *20
      REANUM = 0
      WORD = CHAGET()
      read(WORD, '(BN,E20.0)' , IOSTAT=IO) REANUM
      call IOERR (IO , 'REAGET')
      REAGET = REANUM
      end

C----------------------------------------------------------------------
      subroutine IOERR ( IO , RUTEEN )
c --- implicit real*8 (a-h,o-z)
      character RUTEEN *(*)
      common    /INLINE/ IPOS , ILINE , IFILE
      if (IO .NE. 0) then
         if (IO .LT. 0) then
           if (IFILE.eq.50) THEN
              write(44,*) 'FINISHED'
              STOP 'Normal end because of end of file on input'
           endif
           write(6,*) 'End of file encountered in ', RUTEEN,'  at:'
           call WARN  (10)
           call quitprog ('quitprog because of unwanted end of file')
         else if (IO .gt. 0) then
           write(6,*) 'IO error nr ', IO ,' encountered in '
     &     , RUTEEN ,'  at:'
           call WARN(10)
         endif
      endif
      end

C----------------------------------------------------------------------
      BLOCKDATA DATS
c --- implicit real*8 (a-h,o-z)
      common    /COMWARN/ NWARN
      parameter (MAXIX=100)
      common    /store_index/ nix , ixstate(maxix)
      common /heap_info/ last, max_used
      DATA NWARN /0/ last /0/ max_used/0/ NIX /0/
      end

C----------------------------------------------------------------------
      subroutine WARN(N)
c --- implicit real*8 (a-h,o-z)
      common    /COMWARN/ NWARN
      if (N.NE.0) then
         call PUTINP
         NWARN = NWARN + 1
         write(6,*) '***** number of warnings is ', NWARN,' *****'
      endif
      if (NWARN .gt. N)  call quitprog('TOO MANY WARNINGS')
      end

C----------------------------------------------------------------------
      subroutine NEWFIL (JFILE)
c --- implicit real*8 (a-h,o-z)
      character LINE *136
      common    /CHLINE/ LINE
      common    /INLINE/ IPOS , ILINE , IFILE
      IFILE = JFILE
      IPOS  = 137
      ILINE = 0
      LINE  =' '
      end

C----------------------------------------------------------------------
      subroutine STATUS ( TRACE1 )
c --- implicit real*8 (a-h,o-z)
      character TRACE1 *(*) , TRACE *20
      common    /CTRACE/   TRACE
         TRACE = TRACE1
      end

C----------------------------------------------------------------------
      subroutine PUTINP
c --- implicit real*8 (a-h,o-z)
      character TRACE *20
      common    /CTRACE/   TRACE
      character LINE *136
      common    /CHLINE/ LINE
      common    /INLINE/ IPOS , ILINE , IFILE
      PARAMETER (LEN=78)
      do 1 NPOS = LEN, IPOS, -1
    1    if (LINE(NPOS:NPOS) .NE. ' ') goto 5
    5 write(6,'(A,A,A,I3,A,I6,A,I3,A,/(1X,80A))')
     &  ' routine=',trace,
     &  ' file=',IFILE,  ' line=',ILINE,  ' col=',IPOS,  ' =>',
     &   (LINE(JPOS:JPOS),JPOS=  1 , IPOS-1),   '<','<',
     &   (LINE(JPOS:JPOS),JPOS=IPOS, NPOS  )
      end

C----------------------------------------------------------------------
      subroutine quitprog (TEXT)
c --- implicit real*8 (a-h,o-z)
      character TEXT*(*)
      write(6,*) TEXT
      STOP 'quitprog: see output'
      end

C----------------------------------------------------------------------
      subroutine ZERO ( X , N )
c --- implicit real*8 (a-h,o-z)
      dimension   X(*)
      do 1 I = 1,N
    1    X(I) = 0
      end

C----------------------------------------------------------------------
      subroutine izero ( IX , N )
c --- implicit real*8 (a-h,o-z)
      dimension  IX(*)
      do 1 I = 1,N
    1   IX(I) = 0
      end

C----------------------------------------------------------------------
      function dot ( a, b, n )
      dimension a(*), b(*)
      dot = 0
      do 1 i=1,n
    1    dot = dot + a(i) * b(i)
      end

C----------------------------------------------------------------------
      function vecabs ( v, n )
      dimension v(*)
      vecabs = sqrt ( dot (v, v, n)  )
      end

C----------------------------------------------------------------------
      subroutine CHECK ( IFIELD , LFIELD , CFIELD , PRNT)
c --- implicit real*8 (a-h,o-z)
      LOGICAL PRNT
      character CFIELD *(*)
      if (IFIELD .gt. LFIELD) then
         write(6,*)
     &  'Increase parameter ',CFIELD,' from ',LFIELD,' to ',IFIELD
         call quitprog ('Increase dimension. See output.')
      ELSEIF (PRNT) THEN
         write(6,*) 'PARAMETER ',CFIELD,' :USED=',IFIELD,' :SIZE=',
     &              LFIELD
      endif
      end

C----------------------------------------------------------------------
      subroutine PRMAT (A, N, M, NMAX, TEXT)
c --- implicit real*8 (a-h,o-z)
      dimension  A(N,M)
      character TEXT *(*)
      if (N.EQ.0 .OR. M.EQ.0) then
        write(6,*) 'zero dimension ',text, N,' * ',M
        return
      endif
      write(6,'(/1x,A,I7,A,I5/(T5,10I7))')
     &TEXT,N,' *',M,  (J, J=1,MIN(NMAX,M))
      do 2 I=1,MIN(NMAX,N)
    2   write(6,'(I4,(T5,10f7.4))') I, (A(I,J),J=1,MIN(NMAX,M))
      end

C========================================================================
C     Auger subroutine: this subroutine checks work area size and
C     allocate variables, prepares matricies and calls main.
      subroutine AUGER(NCONF,N2,N3,NG,NOG,HAMG,EG,NDEG,NF,NOF,HAMF,EF,
     &                 SPECT,TRAN,CHARAC,WK,ifconf,irf, itriad,
     &                 NOMEGA,ESTART,ESTEP,DIPx,CI,EFAUG,EFXAS)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION HAMG(NOG,NOG),HAMF(NOF,NOF),TRAN(NOG*NOF),WK(NOF),
     &          EG(NOG),EF(NOF),SPECT(NOF),ifconf(nof),
     &          NCONF(2),CHARAC(3),NF(0:2),NG(3),
     &          DIPx(9),CI(9),EFAUG(3,9),EFXAS(3)
      character *20 irf
      parameter (niwk=5000)
      parameter (lheap=999999999)
      DIMENSION iwk(niwk)
      common /heap     / x(lheap)
C
      PRINT *,'subroutine AUGER callED'
C
      call check(nof, niwk,'NiWK',.false.)

      call rescale(hamf, tran, ifconf, iwk, efaug, efxas, ci, dipx,
     &             noa, nob, nog, nof)


      call HAMSORT(NOG,NOF,TRAN,HAMF,iWK)

      print '(10(a,i6))',
     &' nog=',nog,' nof=',nof,' noa=',noa,' nob=',nob,' ndeg=',ndeg

      call transform_bra ( nog, nog, nof, nog, Hamg, Tran )

      if (Noa.eq.0) then
ctrick VAX
c-- On a VAX calling a routine with an array of dimension zero
c-- is not allowed. Therefore, if Noa=0, i.e. there are no
c-  intermediate states, direct photoemission only is calculated.
c-- On most other machines this is not necessary because the array
c-- with dimension zero is not really used in genmat and agmain..
        call alloc   ('auger degTf' , Ndeg*Nof, idegTf )
        call alloc   ('auger Ene'   , Nof     , iEne )
        call alloc   ('auger rInten', Nof     , irInten )

        call cut_rows( Tran, Nog, Nof, x(idegTf), ndeg )
        call eigen   ( irf, Hamf, Nof, Nof, x(iEne), x(idegTf), Ndeg )
        call auger_spect_real( Ndeg, Nof, x(idegTf),x(irInten),TotInt)
        do 100 iomega = 0, nomega
          call OUTPUT
     &    (51+ITRIAD,iomega,Nof,x(iEne),x(irInten),Nog,Ndeg,EG,
     &     CHARAC,TotInt)
          call TotalYield
     &         (61+ITRIAD,NOMEGA,IOMEGA,ESTART,ESTEP,TotInt, Totint)
  100   continue

        call dealloc ('auger rInten', Nof     , irInten)
        call dealloc ('auger Ene'   , Nof     , iEne   )
        call dealloc ('auger degTf' , Ndeg*Nof, idegTf )
        return
      endif
C ******
      call alloc('auger', 0, istart)
      call alloc('auger', NOa**2, IHAMa)
      call alloc('auger', NOb**2, IHAMb)
      call alloc('auger', NOa*NOb, IaVb)
      call alloc('auger', NOa*NOG, IaMg)
      call alloc('auger', NOb*NOG, IbMg)
      call alloc('auger', NOa*NOa, IaGa)
c It seems that the complex components need to be allocated as a contiguous
c block of memory
      call alloc('auger', 2*NOb*Ndeg, IbMgRe)
      IbMgIm = IbMgRe + NOb * Ndeg
      call alloc('auger', NOb*NOa, IbVa)
      call alloc('auger', NOa*Ndeg, IaVbbMg)
      call alloc('auger', 2*NOa**2, IaGaRe)
      IaGaIm = IaGaRe + NOa**2
      call alloc('auger', 2*NOa*Ndeg, IaMgRe)
      IaMgIm = IaMgRe + NOa*Ndeg
      call alloc('auger', max(NOb, NOa), IWKx)

C
      call GENMAT(NOG,NOF,HAMF,TRAN,NOa,NOb,
     &     X(IHAMa),X(IHAMb),X(IaVb),X(IaMg),X(IbMg) )
C
C
      IEa=1
      IEb=IEa+NOA
C
      call check(noa,niwk,'NiWK',.true.)
C
      call AGmain(irf, ITRIAD,Nomega,ESTART,ESTEP,
     &            NOa,NOb,NOg,Ndeg,SPECT,CHARAC,
     &            EF(IEa),EF(IEb),Eg,X(IHAMa),X(IHAMb),HAMg,
     &            X(IaVb),   X(IbVa),  X(IaMg),  X(IbMg),
     &            X(IaVbbMg),X(IaGa),  X(IaGaRe),X(IaGaIm),
     &            X(IaMgRe), X(IaMgIm),X(IbMgRe),
     &            X(IbMgIm), X(iWKx),  iwk)
C
      call cut_heap ('auger', istart)
      END
C
C----------------------------------------------------------------------
c
      subroutine rescale(hamf, tran, ifconf, icon, efaug, efxas,
     &                   ci, dipx, noa, nob, nog, nof)
c --- implicit real*8 (a-h,o-z)
      dimension hamf(nof,nof), tran(nog,nof), ifconf(nof), icon(nof),
     &          ci(9), dipx(9), efaug(9,3), efxas(3)
c---  ICON  is going to contain the configuration number of the
c---  final states.
c---  IFCONF contains the information about the hybridization stucture.
c
c---  energy shift and calculation of icon
      parameter (step=1000.)
      noa = 0
      do 10 ibra = 1, nof
        if (ifconf(ibra).ne.0) iconfbra = ifconf(ibra)
        icon(ibra) = nint ( hamf(ibra,ibra) / step )
        if (icon(ibra).eq.0) then
          noa = noa + 1
        else
          hamf(ibra,ibra) = hamf(ibra,ibra) - icon(ibra) * step
     &                    + efaug (icon(ibra), iconfbra)
     &                    - efxas(iconfbra)
        endif
   10 continue
      if (noa.eq.0) print*,' No direct transitions to these',
     & ' intermediate states are posible for this symmetry. Noa=0'
      nob = nof - noa

c---- Apply ci factor between intermediate state (ICON=0) and
c---  continuum states (ICON<>0).
      do 30 ibra = 1, nof
        do 20 iket = 1, nof
          if (icon(ibra).eq.0 .and. icon(iket).ne.0) then
            hamf(ibra,iket) = hamf(ibra,iket) * ci(icon(iket))
          endif
          if (icon(ibra).ne.0 .and. icon(iket).eq.0) then
            hamf(ibra,iket) = hamf(ibra,iket) * ci(icon(ibra))
          endif
   20   continue
   30 continue
c
c---  apply dipfactor
c---  We are only interested in the final configuration of a transition.
c---  The ground state from which it is coming is determined by
c---  ITRAN in main. Other transitions are zero.
      do 50 if = 1  , nof
        do 40 ig = 1  , nog
          if (icon(if).ne.0) tran(ig,if) = tran(ig,if) * dipx(icon(if))
   40   continue
   50 continue
      end


C----------------------------------------------------------------------
      subroutine GENMAT(NOG,NOF,HAMF,TRAN,NOa,NOb,
     &                  HAMFa,HAMFb,aVb,aMg,bMg)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION HAMF(NOF,NOF),TRAN(NOG,NOF),HAMFa(NOa,NOa),
     &          HAMFb(NOb,NOb),aVb(NOa,NOb),aMg(NOa,NOG),bMg(NOb,NOG)
*
C
      PRINT *,'subroutine GENMAT callED'
C     generate matricies
C
      call MATCUT(NOF,NOF,NOa,NOa, 1, 1, 1, 1,    noa,noa, hamf,hamfa)
      call MATCUT(NOF,NOF,NOb,NOb,noa+1,noa+1,1,1,nob,nob, hamf,hamfb)
      call MATCUT(NOF,NOF,NOa,NOb, 1, noa+1, 1, 1,noa,nob, HAMF,aVb)
      call MATCUT(NOG,NOF,NOa,NOG, 1, 1, 1, 1,    noa,nog, TRAN,aMg)
      call MATCUT(NOG,NOF,NOb,NOG,noa+1, 1, 1, 1, nob,nog, TRAN,bMg)
      END

C----------------------------------------------------------------------
      subroutine HamSort(NOG,NOF,TRAN,HAMF,iWK)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION TRAN(NOG,NOF),HAMF(NOF,NOF),iWK(NOF)
C
C---  Sort to increasing configuration number. Within the same
c---  configuration keep present order. (iwk is changed)
      DO 5 I = 1, NOF
    5   iWK(I) = NOF * iWK(I) + I

      do 30 k1 = 1, nof-1
        kmin = k1
        do 20 k2 = k1+1, nof
   20     if ( iwk(k2) .lt. iwk(kmin) ) kmin = k2
        if (k1.ne.kmin) then
          iwkk1     = iwk(k1)
          iwk(k1)   = iwk(kmin)
          iwk(kmin) = iwkk1
          call SWAP1D(k1,kmin,NOG,NOF,TRAN)
          call SWAP2D(k1,kmin,NOF,HAMF)
        endif
   30 CONTINUE
      END

C--------------------------------------------------------------------
      subroutine SWAP1D(I1,I2,NOG,NOF,TRAN)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION TRAN(NOG,NOF)
C
      DO 10 I=1,NOG
        WK        =TRAN(I,I2)
        TRAN(I,I2)=TRAN(I,I1)
        TRAN(I,I1)=WK
   10 CONTINUE
      END

C--------------------------------------------------------------------
      subroutine SWAP2D(I1,I2,NOF,HAMF)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION HAMF(NOF,NOF)
C
c --- This looks safer to me than the previous version.
      DO 10 I=1,NOF
        WK        =HAMF(I,I2)
        HAMF(I,I2)=HAMF(I,I1)
        HAMF(I,I1)=WK
   10 CONTINUE
      DO 20 I=1,NOF
        WK        =HAMF(I2,I)
        HAMF(I2,I)=HAMF(I1,I)
        HAMF(I1,I)=WK
   20 CONTINUE
      END

C--------------------------------------------------------------------
      subroutine AGmain(irf, ITRIAD,Nomega,ESTART,ESTEP,
     &           Na,Nb,Ng,Ndeg,rInten,CHARAC,
     &           Ea,Eb,Eg,HAMFa,HAMFb,HAMg,aVb,bVa,aMg,bMg,aVbbMg,
     &           aGa,aGaRe,aGaIm,aMgRe,aMgIm,bMgRe,bMgIm,WK,IPVT)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION rInten(Nb),CHARAC(3)
      DIMENSION Ea(Na),HAMFa(Na,Na),Eb(Nb),HAMFb(Nb,Nb),
     &          Eg(Ng),HAMg(Ng,Ng),aVb(Na,Nb),bVa(Nb,Na),aMg(Na,Ng),
     &          bMg(Nb,Ng),aVbbMg(Na,Ndeg),aGa(Na,Na),aGaRe(Na,Na),
     &          aGaIm(Na,Na),aMgRe(Na,Ndeg),aMgIm(Na,Ndeg),
     &          bMgRe(Nb,Ndeg),bMgIm(Nb,Ndeg),WK(Na),IPVT(Na)
      character *20 irf
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      common    /com_gamma/ gamma
      external  DIAG
C
      PI=4.0*ATAN(1.0)
C
C     bVa is used as work area: required size is Na & Nb
      call find_data ( 2, irf, DIAG, HAMFa,Na,Na,Ea,bVa)
C
      call transform_bra ( Na, Na, Nb, Na, Hamfa, aVb )
      call transform_bra ( Na, Na, Ng, Na, Hamfa, aMg )

      if (nomega.lt.0) then
        print*,'The continuum states are not diagonalized.'
      else
        Nx = Na + Ndeg
        call alloc   ( 'agmain', Nx*Nb, ixTb )
c--     aVb and bMg are put into xTb to be transformed together by eigen
        call gather  ( Na, Ndeg, Nb, aVb, bMg, X(ixTb) )
        call eigen   ( irf,HAMFb, Nb, Nb, Eb, X(ixTb), Nx )
        call degather( Na, Ndeg, Nb, X(ixTb), aVb, bMg )
        call dealloc ( 'agmain', Nx*Nb, ixTb )
      endif
C
      DO 20 I=1,Na
        DO 10 J=1,Nb
          bVa(J,I)=aVb(I,J)
   10   CONTINUE
   20 CONTINUE

      call MMULT(Na,Nb,Ng,Ndeg,aVbbMg,aVb,bMg)
      call MMULT(Na,Nb,Na,Na,aGa,aVb,bVa)

      call MIDOUT(46,45,Ng,Na,Ndeg,Eg,Ea,aMg,aGa,CHARAC)

c---  Putting Nomega < 0 stops calculation here, and so gives only the
c--   list of energies and absorption intensities printed in MIDOUT to file 46

C     (*begin of loop*)
      IF (Nomega.EQ.0) PRINT *,'CAUTION Nomega=0'



      DO 100 Iomega = 0, Nomega
C
      call ZERO  ( aMgRe, Na*Ndeg)
      call ZERO  ( aMgIm, Na*Ndeg)
C     complex-aMg <= aMg-i*PI*aVbbMg
        DO 40 I=1,Na
          DO 30 J=1,Ndeg
            aMgRe(I,J)=aMg(I,J)
            aMgIm(I,J)=-PI*aVbbMg(I,J)
   30     CONTINUE
   40   CONTINUE
C
C     Green**(-1)
c--- thole  gamma is read in getauger and stored in /com_gamma/
      call zero(aGaRe,Na*Na)
      call zero(aGaIm,Na*Na)
      Omega = Estart + Estep*(Iomega-1)
ctrick
c--   for iomega=0 the off-resonance spectrum is calculated
      if (Iomega.eq.0) Omega = 10000
      DO 60 I=1,Na
          aGaRe(I,I)=  Omega+Eg(1)-Ea(I)
          aGaIm(I,I)=  Gamma
          DO 50 J=1,Na
            aGaIm(I,J)=aGaIm(I,J)+PI*aGa(I,J)
   50    CONTINUE
   60 CONTINUE
C
C     solve Green's function
      call SLVGRN (Na,Ng,Ndeg,aGaRe,aGaIm,aMgRe,aMgIm,WK,IPVT)

C
C     intensity calculation
C     bVa * complex-aMg + bMg:
      call zero(bMgRe,Nb*Ndeg)
      call zero(bMgIm,Nb*Ndeg)
      DO 90 I=1,Ndeg
        DO 80 J=1,Nb
          DO 70 K=1,Na
            bMgRe(J,I)=bMgRe(J,I)+bVa(J,K)*aMgRe(K,I)
            bMgIm(J,I)=bMgIm(J,I)+bVa(J,K)*aMgIm(K,I)
   70     CONTINUE
          bMgRe(J,I)=bMg(J,I)+bMgRe(J,I)
   80   CONTINUE
   90 CONTINUE
C

c---  calculate intensity to other continuums, implicit in use of Gamma.
        call alloc       ('auger_spect scratch',na,iIn)
        call auger_spect (Na,Ndeg,aMgRe,aMgIm,X(iIn),TotInt_a)
        call dealloc     ('auger_spect scratch',na,iIn)

c---  calculate intensity to "b" continuums.
        call auger_spect (Nb,Ndeg,bMgRe,bMgIm,rInten,TotInt_b)

	TotInt = TotInt_b + Gamma / pi * TotInt_a


        call OUTPUT(51+ITRIAD,iomega,Nb,Eb,rInten,Ng,Ndeg,EG,
     &              CHARAC,TotInt_b)
        call TotalYield
     &        (61+ITRIAD,NOMEGA,IOMEGA,ESTART,ESTEP,TotInt_b, TotInt)
C
  100 CONTINUE
C     (*end of loop*)
      RETURN
      END

C--------------------------------------------------------------------
      subroutine cut_rows ( Tran, Nog, Nof, degTf, ndeg )
c --- cut out part of matrix without transpose (MATCUT takes transpose)
      dimension  Tran(Nog,Nof), degTf(ndeg,Nof)
      do 1 iof = 1,Nof
      do 1 ideg = 1, Ndeg
    1   degTf(ideg,iof) = Tran(ideg,iof)
      end

C--------------------------------------------------------------------
C
C    cut out certain part of a Matrix and
C                put them into some part of another Matrix
      subroutine MATCUT(NyI,NxI,NyO,NxO,NOFSxI,NOFSyI,NOFSxO,NOFSyO,
     &                  NDX,NDY,rMATIN,rMATOUT)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION rMATIN(NyI,NxI),rMATOUT(NyO,NxO)
C
      IF (NXO.EQ.0 .OR. NYO.EQ.0) THEN
C        PRINT *,'CAUTION:....NXO=0 OR NYO=0    :RETURN MATCUT'
        RETURN
      ENDIF
      IF (NDX.EQ.0 .OR. NDY.EQ.0) THEN
C        PRINT *,'CAUTION:....NDO=0 OR NDO=0    :RETURN MATCUT'
        RETURN
      ENDIF
      IF (NXI.LE.0 .OR. NYI.LE.0 .OR. NXO.LE.0 .OR. NYO.LE.0) THEN
        PRINT *,'These values should be equal to or greater than 1'
        PRINT '(4(A,I4))',' NXI=',NXI,' NYI=',NYI,
     &                    ' NXO=',NXO,' NYO=',NYO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSXI.LT.0 .OR. NOFSYI.LT.0 .OR.
     &    NOFSXO.LT.0 .OR. NOFSYO.LT.0) THEN
        PRINT *,'These values should be equal to or greater than 0'
        PRINT '(4(A,I4))',' NOFSXI=',NOFSXI,' NOFSYI=',NOFSYI,
     &                    'NOFSXO=',NOFSXO,' NOFSYO=',NOFSYO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NDX.LT.0 .OR. NDY.LT.0) THEN
        PRINT *,'These values should be equal to or greater than 0'
        PRINT '(2(A,I4))',' NDX=',NDX,' NDY=',NDY
        call quitprog('Error in subroutine MATCUT')
      ENDIF
*
      IF (NOFSXI.LT.1 .OR. NOFSXI.GT.NXI) THEN
        PRINT '( 2(A,I4) )',' Out of range | NOFSXI(',NOFSXI,
     &                      ') should be 1<= & <=',NXI
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSYI.LT.1 .OR. NOFSYI.GT.NYI) THEN
        PRINT '( 2(A,I4) )',' Out of range | NOFSYI(',NOFSYI,
     &                      ') should be 1<= & <=',NYI
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSXO.LT.1 .OR. NOFSXO.GT.NXO) THEN
        PRINT '( 2(A,I4) )',' Out of range | NOFSXO(',NOFSXO,
     &                      ') should be 1<= & <=',NXO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSYO.LT.1 .OR. NOFSYO.GT.NYO) THEN
        PRINT '( 2(A,I4) )',' Out of range | NOFSYO(',NOFSYO,
     &                      ') should be 1<= & <=',NYO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
*
      IF (NOFSXI+NDX-1.GT.NXI) THEN
        PRINT '( 4(A,I4) )',' Out of range | NOFSXI(',NOFSXI,
     &  ')+NDX(',NDX,')-1=',NOFSXI+NDX-1,' should be less than NXI=',NXI
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSYI+NDY-1.GT.NYI) THEN
        PRINT '( 4(A,I4) )',' Out of range | NOFSYI(',NOFSYI,
     &  ')+NDY(',NDY,')-1=',NOFSYI+NDY-1,' should be less than NYI=',NYI
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSXO+NDY-1.GT.NXO) THEN
        PRINT '( 4(A,I4) )',' Out of range | NOFSXO(',NOFSXO,
     &  ')+NDY(',NDY,')-1=',NOFSXO+NDY-1,' should be less than NXO=',NXO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
      IF (NOFSYO+NDX-1.GT.NYO) THEN
        PRINT '( 4(A,I4) )',' Out of range | NOFSYO(',NOFSYO,
     & ')+NDX(',NDX,')-1=',NOFSYO+NDX-1,' should be less than NYO=',NYO
        call quitprog('Error in subroutine MATCUT')
      ENDIF
C
C     output Matrix is transposed
      DO 20 IX=0,NDX-1
        DO 10 IY=0,NDY-1
          rMATOUT(NOFSyO+Ix,NOFSxO+Iy)=rMATIN(NOFSyI+Iy,NOFSxI+Ix)
   10   CONTINUE
   20 CONTINUE
C
      RETURN
      END
C
C----------------------------------------------------------------------
C     MATRIX TRANSFORMATION
      subroutine TRSFRM(Nb,Ng,Ndeg,HAMFb,bMg,HAMg,WK)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION bMg(Nb,Ng),HAMFb(Nb,Nb),HAMg(Ng,Ng),WK(Nb,Ndeg)
C
      IF (Nb.EQ.0) THEN
C        PRINT *,'CAUTION:...Nb=0 MATRIX IS 0*0 :RETURN TRSFRM'
        RETURN
      ENDIF
      IF (Nb.LE.0 .OR. Ng.LE.0 .OR. Ndeg.LE.0) THEN
        PRINT *,'These values should be equal to or greater than 1'
        PRINT '(3(A,I4))',' Nb=',Nb,' Ng=',Ng,' Ndeg=',Ndeg
        call quitprog('Error in subroutine TRSFRM')
      ENDIF
      IF (Ndeg.GT.Ng) THEN
        PRINT *,'Ndeg=',Ndeg,' should be smaller than Ng=',Ng
        call quitprog('Error in subroutine TRSFRM')
      ENDIF
C
      call ZERO(WK,Nb*Ndeg)
      DO 30 I=1,Ndeg
        DO 20 J=1,Nb
          DO 10 K=1,Ng
            WK(J,I)=WK(J,I)+bMg(J,K)*HAMg(K,I)
   10     CONTINUE
   20   CONTINUE
   30 CONTINUE
C
      call ZERO(bMg,Nb*Ndeg)
      DO 60 I=1,Ndeg
        DO 50 J=1,Nb
          DO 40 K=1,Nb
            bMg(J,I)=bMg(J,I)+HAMFb(K,J)*WK(K,I)
   40     CONTINUE
   50   CONTINUE
   60 CONTINUE
C
      RETURN
      END
C

C--------------------------------------------------------------------
      subroutine eigen ( irf, Ham, ndim, n, Ene, T, nt )
c--- diagonalize Ham and transform T to eigenstates. If Ham is
c--- blocked, diagonalization is done for each block separately.
c--- Eigenvectors are not produced.
c--- In eigen2  irf and istate are used to find tridiagonalising
c--- vectors if they have been calculated previously. Istate for
c--- the continuum configurations takes the values 3,4,5 ... .
      dimension Ham(ndim,n), Ene(n), T(nt,n)
      character*20 irf
      nstart = 1
      nlast  = 1
      istate = 2
      do 100 i = 1, n
c--     find last non-zero in row i
        do 10 j = n, i, -1
   10     if (Ham(i,j) .ne. 0 ) goto 20
c--     nlast is the end of the non-zero off-diagonal block in the
c--     rows up to and including i.
   20   nlast = max ( nlast, j )
        if (nlast.eq.i) then
          print*,' eigen2 called for block: ',
     &   'nstart=',nstart,' nlast=', nlast
          istate = istate + 1
          call eigen2 ( istate, irf, Ham(nstart,nstart), ndim,
     &       nlast-nstart+1, Ene(nstart), T(1,nstart), nt,iWork2 )
          nstart = nlast + 1
        endif
  100 continue
      end

C--------------------------------------------------------------------
      subroutine eigen2 ( istate, irf, Ham, nm, n, Ene, T, nt, iWork2 )
c--   tred3 finds the vectors that tridiagonalize Ham.
c--   T is transformed to those vectors.
c--   tql3 diagonalizes the tridiagonal matrix and transforms
c--   T to the eigenvectors, without producing these eigenvectors
c--   themselves. For this tql3 has been changed slightly.
      dimension Ham(nm,n), Ene(n), T(nt,n)
      character *20 irf
      external  tred3
      parameter (lheap=999999999)
      common /heap     / x(lheap)

c!      call alloc  ( 'eigen2 work', n, iWork )
c!      call find_data ( istate, irf, tred3, Ham, nm, n, Ene, x(iWork))
      call transform_ket ( nt, nt, n, nm, T, Ham )
      call tql3   ( nt, n, Ene, x(iWork2), T, ierr )
c!      call dealloc( 'eigen2 work', n, iWork )
      end


C--------------------------------------------------------------------
      subroutine mmult1 ( n, nm, nt, nmT, Ham, T, HamT )
c--   rigth transformation of HAM. General dimensions.
      dimension  Ham(nm,n), T(nmT,nt), HamT(nmT,nt)
      call zero (HamT, nmT*nt)
      do 1 i  = 1, n
      do 1 k  = 1, n
      do 1 j  = 1, nt
    1   HamT(i,j) = HamT(i,j) + Ham(k,i) * T(k,j)
      end


C--------------------------------------------------------------------
      subroutine mmult2 ( nt, nmT, n, nmH, T, Ham, THam )
c--   left transformation of HAM. General dimensions.
      dimension T(nmT,n), Ham(nmH,n), THam(nmT,n)
      call zero (THam, nmT*n)
      do 1 it = 1, nt
      do 1 k  = 1, n
      do 1 j  = 1, n
    1   THam(it,j) = THam(it,j) + T(it,k) * Ham(k,j)
      end

C--------------------------------------------------------------------
      subroutine gather ( Na, Ndeg, Nb, aVb, bMg, T )
      dimension aVb(Na,Nb), bMg(Nb,Ndeg), T(Na+Ndeg, Nb)
      do 100 Ib = 1, Nb
        do 10 Ia = 1, Na
   10     T(Ia, Ib) = aVb(Ia,Ib)
        do 20 Ideg = 1, Ndeg
   20     T(Na+Ideg,Ib) = bMg(Ib,Ideg)
  100 continue
      end

C--------------------------------------------------------------------
      subroutine degather ( Na, Ndeg, Nb, T, aVb, bMg )
      dimension aVb(Na,Nb), bMg(Nb,Ndeg), T(Na+Ndeg, Nb)
      do 100 Ib = 1, Nb
        do 10 Ia = 1, Na
   10     aVb(Ia,Ib) = T(Ia, Ib)
        do 20 Ideg = 1, Ndeg
   20     bMg(Ib,Ideg) = T(Na+Ideg,Ib)
  100 continue
      end

C----------------------------------------------------------------------
      subroutine transform_bra ( Na, Nma, Ng, NmaM, aHa, aMg )
      dimension  aHa(Nma,Na), aMg(NmaM,Ng)
      parameter (lheap=999999999)
      common /heap     / x(lheap)
        call alloc  ('transform_bra  aHaaMg', NmaM*Ng, iaHaaMg )
        call mmult1 (Na, Nma, Ng, NmaM, aHa, aMg, x(iaHaaMg) )
        call copy   ( x(iaHaaMg), aMg, NmaM*Ng )
        call dealloc('transform_bra  aHaaMg', NmaM*Ng, iaHaaMg )
      end

C----------------------------------------------------------------------
      subroutine transform_ket ( Na, Nma, Ng, Nmg, aMg, gHg )
      dimension  aMg(Nma,Ng), gHg(Nmg,Ng)
      parameter (lheap=999999999)
      common /heap     / x(lheap)
        call alloc  ('transform_ket  aMggHg', Nma*Ng, iaMggHg )
        call mmult2 ( Na, Nma, Ng, Nmg, aMg, gHg, x(iaMggHg) )
        call copy   ( x(iaMggHg), aMg, Nma*Ng )
        call dealloc('transform_ket  aMggHg', Nma*Ng, iaMggHg )
      end

C----------------------------------------------------------------------
C     MATRIX MULTIPLICATION
      subroutine MMULT(Na,Nb,Ng,Ndeg,aVbbMg,aVb,bMg)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION aVbbMg(Na,Ndeg),aVb(Na,Nb),bMg(Nb,Ng)
C
      IF (Na.EQ.0) THEN
C        PRINT *,'CAUTION:...Na=0 MATRIX IS 0*0 :RETURN MMULT '
        RETURN
      ENDIF
      IF (Na.LE.0 .OR. Nb.LE.0 .OR. Ng.LE.0 .OR. Ndeg.LE.0) THEN
        PRINT *,'These values should be equal to or greater than 1'
        PRINT '(4(A,I4))',' Na=',Na,' Nb=',Nb,' Ng=',Ng,' Ndeg=',Ndeg
        call quitprog('Error in subroutine MMULT')
      ENDIF
      IF (Ndeg.GT.Ng) THEN
        PRINT *,'Ndeg=',Ndeg,' should be smaller than Ng=',Ng
        call quitprog('Error in subroutine MMULT')
      ENDIF
C
      call ZERO(aVbbMg,Na*Ndeg)
C
      DO 30 I=1,Ndeg
        DO 20 J=1,Na
          DO 10 K=1,Nb
            aVbbMg(J,I)=aVbbMg(J,I)+aVb(J,K)*bMg(K,I)
   10     CONTINUE
   20   CONTINUE
   30 CONTINUE
C
      RETURN
      END
C
C----------------------------------------------------------------------
      subroutine SLVGRN(Na,Ng,Ndeg,aGaRe,aGaIm,aMgRe,aMgIm,WK,IPVT)
C     SOLVE GREEN'S FUNCTION using COMPLEX routines
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION aGaRe(Na,Na),aGaIm(Na,Na),aMgRe(Na,Ndeg),
     &          aMgIm(Na,Ndeg), WK(na),IPVT(Na)
      parameter (lheap=999999999)
      common /heap     / x(lheap)

      if (na.eq.0) return
C
      call alloc ( 'slvgrn', 0, i_cut )
      call alloc ( 'slvgrn  aMg ', 2*(Na*Ndeg),iaMgCo)
      call alloc ( 'slvgrn  aGa ', 2*(Na*Na  ),iaGaCo)
      call alloc ( 'slvgrn  work', Na, iwork)
      call real_to_complex ( aMgRe, X(iaMgCo),Na*Ndeg )
      call real_to_complex ( aGaRe, X(iaGaCo),Na*Na   )
      call solve_equations (X(iagaCo),Na,X(iaMgCo),Ndeg,X(iwork),ipvt)
      call complex_to_real ( X(iaMgCo), aMgRe, Na*Ndeg )
      call cut_heap ('slvgrn', i_cut)

      END

C----------------------------------------------------------------------
      subroutine solve_equations ( A, n, B, Ndeg, ipvt, work )
      complex a(n,n), b(n,ndeg), work(n)
      integer ipvt(n)
cnonstandard
c NAG routine F04ADF
c     ifail = 0
c      call F04ADF
c     &(a, n, b, n, n, Ndeg, b, n, work, ifail)
c        IF (ifail.eq.1) THEN
c          PRINT *,'matrix was singular ',N
c          call quitprog('Error in subroutine solve_equations')
c        ENDIF
c end NAG

c linpack routines  cGEFA and cGESL
      call cgefa ( a, n, n, ipvt, ier )
      IF (ier.ne.0) THEN
        PRINT *,'matrix was singular ',N
        call quitprog('Error in subroutine solve_equations')
      ENDIF
      do 10 i = 1,ndeg
   10   call cgesl ( a, n, n, ipvt, b(1,i), 0 )
c end linpack
      end

C----------------------------------------------------------------------
      subroutine real_to_complex ( a, b, n )
c --- implicit real*8 (a-h,o-z)
c     real*8 b
      dimension a(2*n), b(2*n)
      do 1 i = 1, n
        b(2*i-1) = a(i)
        b(2*i)   = a(i+n)
    1 continue
      end

C----------------------------------------------------------------------
      subroutine complex_to_real ( b, a, n )
c --- implicit real*8 (a-h,o-z)
      dimension  a(2*n), b(2*n)
c     real*8 b
      do 1 i= 1, n
        a(i)   = b(2*i-1)
    1   a(i+n) = b(2*i)
      end

C----------------------------------------------------------------------
      subroutine copy ( a, b, n)
c --- implicit real*8 (a-h,o-z)
      dimension a(n), b(n)
      do 1 i= 1, n
    1   b(i) = a(i)
      end

C----------------------------------------------------------------------
      subroutine CGEFA(A,LDA,N,IPVT,INFO)
      INTEGER LDA,N,IPVT(*),INFO
      COMPLEX A(LDA,*)
C
C     CGEFA FACTORS A COMPLEX MATRIX BY GAUSSIAN ELIMINATION.
C
C     CGEFA IS USUALLY CALLED BY CGECO, BUT IT CAN BE CALLED
C     DIRECTLY WITH A SAVING IN TIME IF  RCOND  IS NOT NEEDED.
C     (TIME FOR CGECO) = (1 + 9/N)*(TIME FOR CGEFA) .
C
C     ON ENTRY
C        A       COMPLEX(LDA, N)
C                THE MATRIX TO BE FACTORED.
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  A .
C        N       INTEGER
C                THE ORDER OF THE MATRIX  A .
C     ON RETURN
C        A       AN UPPER TRIANGULAR MATRIX AND THE MULTIPLIERS
C                WHICH WERE USED TO OBTAIN IT.
C                THE FACTORIZATION CAN BE WRITTEN  A = L*U  WHERE
C                L  IS A PRODUCT OF PERMUTATION AND UNIT LOWER
C                TRIANGULAR MATRICES AND  U  IS UPPER TRIANGULAR.
C        IPVT    INTEGER(N)
C                AN INTEGER VECTOR OF PIVOT INDICES.
C        INFO    INTEGER
C                = 0  NORMAL VALUE.
C                = K  IF  U(K,K) .EQ. 0.0 .  THIS IS NOT AN ERROR
C                     CONDITION FOR THIS subroutine, BUT IT DOES
C                     INDICATE THAT CGESL OR CGEDI WILL DIVIDE BY ZERO
C                     IF CALLED.  USE  RCOND  IN CGECO FOR A RELIABLE
C                     INDICATION OF SINGULARITY.
C     subroutineS AND FUNCTIONS
C     BLAS CAXPY,CSCAL,ICAMAX
C     FORTRAN ABS,AIMAG,REAL
C
      COMPLEX T
      INTEGER ICAMAX,J,K,L
      COMPLEX ZDUM
      REAL CABS1
      CABS1(ZDUM) = ABS(REAL(ZDUM)) + ABS(AIMAG(ZDUM))
C
C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
C
      INFO = 0
      DO 60 K = 1, N-1
         L = ICAMAX(N-K+1,A(K,K),1) + K - 1
         IPVT(K) = L
C
C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
C
         IF (CABS1(A(L,K)) .EQ. 0) then
            info = k
            goto 60
         endif
C        INTERCHANGE
            T = A(L,K)
            A(L,K) = A(K,K)
            A(K,K) = T
C        COMPUTE MULTIPLIERS
            T = -(1.,0.) / A(K,K)
            CALL CSCAL(N-K,T,A(K+1,K),1)
C        ROW ELIMINATION WITH COLUMN INDEXING
            DO 30 J = K+1, N
               T = A(L,J)
               A(L,J) = A(K,J)
               A(K,J) = T
               CALL CAXPY(N-K,T,A(K+1,K),1,A(K+1,J),1)
   30       CONTINUE
   60 CONTINUE
      END

C----------------------------------------------------------------------
      subroutine CGESL(A,LDA,N,IPVT,B,JOB)
      INTEGER LDA,N,IPVT(1),JOB
      COMPLEX A(LDA,1),B(1)
C
C     CGESL SOLVES THE COMPLEX SYSTEM
C     A * X = B  OR  CTRANS(A) * X = B
C     USING THE FACTORS COMPUTED BY CGECO OR CGEFA.
C
C     ON ENTRY
C        A       COMPLEX(LDA, N)
C                THE OUTPUT FROM CGECO OR CGEFA.
C        LDA     INTEGER
C                THE LEADING DIMENSION OF THE ARRAY  A .
C        N       INTEGER
C                THE ORDER OF THE MATRIX  A .
C        IPVT    INTEGER(N)
C                THE PIVOT VECTOR FROM CGECO OR CGEFA.
C        B       COMPLEX(N)
C                THE RIGHT HAND SIDE VECTOR.
C        JOB     INTEGER
C                = 0         TO SOLVE  A*X = B ,
C                = NONZERO   TO SOLVE  CTRANS(A)*X = B  WHERE
C                            CTRANS(A)  IS THE CONJUGATE TRANSPOSE.
C     ON RETURN
C        B       THE SOLUTION VECTOR  X .
C     ERROR CONDITION
C        A DIVISION BY ZERO WILL OCCUR IF THE INPUT FACTOR CONTAINS A
C        ZERO ON THE DIAGONAL.  TECHNICALLY THIS INDICATES SINGULARITY
C        BUT IT IS OFTEN CAUSED BY IMPROPER ARGUMENTS OR IMPROPER
C        SETTING OF LDA .  IT WILL NOT OCCUR IF THE subroutineS ARE
C        CALLED CORRECTLY AND IF CGECO HAS SET RCOND .GT. 0.0
C        OR CGEFA HAS SET INFO .EQ. 0 .
C     TO COMPUTE  INVERSE(A) * C  WHERE  C  IS A MATRIX
C     WITH  P  COLUMNS
C           CALL CGECO(A,LDA,N,IPVT,RCOND,Z)
C           IF (RCOND IS TOO SMALL) GO TO ...
C           DO 10 J = 1, P
C              CALL CGESL(A,LDA,N,IPVT,C(1,J),0)
C        10 CONTINUE
C     subroutineS AND FUNCTIONS
C
C     BLAS CAXPY,CDOTC
C     FORTRAN CONJG
C
C     INTERNAL VARIABLES
      COMPLEX CDOTC,T
      INTEGER K,L
C
      IF (JOB .eq. 0) then
C        JOB = 0 , SOLVE  A * X = B
C        FIRST SOLVE  L*Y = B
         DO 20 K = 1, N-1
            L = IPVT(K)
            T = B(L)
            B(L) = B(K)
            B(K) = T
            CALL CAXPY(N-K,T,A(K+1,K),1,B(K+1),1)
   20    CONTINUE
C
C        NOW SOLVE  U*X = Y
         DO 40 K = n, 1, -1
            B(K) = B(K)/A(K,K)
            T = -B(K)
            CALL CAXPY(K-1,T,A(1,K),1,B(1),1)
   40    CONTINUE
      else
C        JOB = NONZERO, SOLVE  CTRANS(A) * X = B
C        FIRST SOLVE  CTRANS(U)*Y = B
         DO 60 K = 1, N
            T = CDOTC(K-1,A(1,K),1,B(1),1)
            B(K) = (B(K) - T)/CONJG(A(K,K))
   60    CONTINUE
C
C        NOW SOLVE CTRANS(L)*X = Y
         DO 80 K = n-1, 1, -1
            B(K) = B(K) + CDOTC(N-K,A(K+1,K),1,B(K+1),1)
            L = IPVT(K)
            T = B(L)
            B(L) = B(K)
            B(K) = T
   80    CONTINUE
      endif
      END

C----------------------------------------------------------------------
      subroutine caxpy(n,t,a,ia,b,ib)
      complex a(*),b(*),t
      do 1 i=1,n
    1   b(i) = t * a(i) + b(i)
      end

C----------------------------------------------------------------------
      subroutine cscal(n,a,x,incx)
      complex a, x(n)
      do 1 i=1,n
    1  x(i) = a * x(i)
      end

C----------------------------------------------------------------------
      integer function icamax(n,a,inca)
      complex a(n), cdum
      real cabs1, camax, ctemp
      cabs1(cdum) = abs(real(cdum)) + abs(aimag(cdum))
      icamax=1
      camax = cabs1(a(1))
      do 1 i=2,n
        ctemp = cabs1(a(i))
        if(ctemp .gt. camax) then
          icamax= i
          camax = ctemp
        endif
    1 continue
      end

C----------------------------------------------------------------------
      complex function cdotc ( n, x, incx, y, incy)
      complex x(n), y(n)
      cdotc = 0
      do 1 i=1,n
    1   cdotc = cdotc + conjg(x(i)) * y(i)
      end

C----------------------------------------------------------------------
cJapan
C     SOLVE GREEN'S FUNCTION
      subroutine SLVGRN_removed
     &       (Na,Ng,Ndeg,aGaRe,aGaIm,aMgRe,aMgIm,WK,IPVT)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION aGaRe(Na,Na),aGaIm(Na,Na),aMgRe(Na,Ng),aMgIm(Na,Na),
     &          WK(Na),IPVT(Na)
      DIMENSION DET(3)
      parameter (lheap=999999999)
      common /heap     / x(lheap)
      LOGICAL   INVSWCH
C
      IF (Na.EQ.0) THEN
C        PRINT *,'CAUTION:...Na=0 MATRIX IS 0*0 :RETURN SLVGRN'
        RETURN
      ENDIF
      IF (Na.LE.0 .OR. Ng.LE.0 .OR. Ndeg.LE.0) THEN
        PRINT *,'These values should be equal to or greater than 1'
        PRINT '(3(A,I4))',' Na=',Na,' Ng=',Ng,' Ndeg=',Ndeg
        call quitprog('Error in subroutine SLVGRN')
      ENDIF
      IF (Ndeg.GT.Ng) THEN
        PRINT *,'Ndeg=',Ndeg,' should be smaller than Ng=',Ng
        call quitprog('Error in subroutine SLVGRN')
      ENDIF
C
C     LU
cJapan      call ZBGMLU(aGaRe,aGaIm,Na,Na,IPVT,WK,IER)
C
C      PRINT *,'IER=',IER
c      IF (IERR.EQ.0) PRINT *,'Normal end ZBGMLU  IER=',IER
c      IF (IERR.EQ.1000) PRINT *,'Matrix is 1 by 1, Na=',Na,' IER=',IER
      IF (IERR.EQ.3000) THEN
        PRINT *,'IER=',IER
        PRINT *,'Na<0 or trouble with Matrix size Na, Na=',Na
        call quitprog('Error in subroutine SLVGRN')
      ENDIF
      IF (IERR.GE.4000) THEN
        PRINT *,'IER=',IER
        PRINT *,'PIVOT = 0.0 in col/line ',IERR-4000
        PRINT *,'This Matrix is Singular'
        call quitprog('Error in subroutine SLVGRN')
      ENDIF
C
      DO 10 I=1,Ndeg
cJapan        call ZBGMLS(aGaRe,aGaIm,Na,Na,aMgRe(1,I),aMgIm(1,I),IPVT,IERR)
C
C        PRINT *,'IER=',IERR
C        IF (IERR.EQ.0) PRINT *,'Normal end ZBGMLS'
C        IF (IERR.EQ.1000) PRINT *,'Matrix size is 1 by 1, Na=',Na
        IF (IERR.EQ.3000) THEN
          PRINT *,'Na<0 or trouble with Na, Na=',Na
          call quitprog('Error in subroutine SLVGRN')
        ENDIF
        IF (IERR.GE.4000) THEN
          PRINT *,'L Matrix has 0.0 in dagonal element
     &             col/line ',IERR-4000
          call quitprog('Error in subroutine SLVGRN')
        ENDIF
   10 CONTINUE
C
C     PRINT INVERSION MATRIX  that is aGa
      INVSWCH=.TRUE.
      IF (INVSWCH) then
C       ISW > 0 Determinant only, ISW < 0 inverse only ISW=0 both
        ISW=0
        call alloc   ( 'SLVGRN', Na, iWK2 )
cJapan        call ZBGMDI(aGaRe,aGaIm,Na,Na,IPVT,DET,ISW,x(iWK2),IERR)
        call dealloc ( 'SLVGRN', Na, iWK2 )
C
C        PRINT *,'IER=',IERR
C        IF (IERR.EQ.0) PRINT *,'Normal end ZBGMDI        IER=',IER
cC        IF (IERR.EQ.1000) PRINT *,'Matrix size is 1 by 1, Na=',Na
        IF (IERR.EQ.3000) THEN
          PRINT *,'IER=',IER
          PRINT *,'Na<0 or trouble with Na, Na=',Na
          call quitprog('Error in subroutine SLVGRN')
        ENDIF
        IF (IERR.GE.4000) THEN
          PRINT *,'IER=',IER
          PRINT *,'L Matrix has 0.0 in diagonal element
     &             col/line ',IERR-4000
          call quitprog('Error in subroutine SLVGRN')
        ENDIF
        IF (ISW.GE.0) PRINT '(A,1PG15.6)',' DETERMINANT=',
     &                        (DET(1)+DET(2))*10.0**DET(3)
C
      ENDIF
      END

C----------------------------------------------------------------------
      subroutine auger_spect_real ( Ndeg, Nb, A, rInten, TotInt )
      DIMENSION A(Ndeg, Nb), rInten(Nb)
      TotInt=0.0
      call ZERO(rInten,Nb)
C
      DO 20 ib = 1, Nb
        DO 10 ideg = 1, Ndeg
          rInten(Ib)=rInten(Ib) + A(ideg,ib)**2
   10   CONTINUE
        TotInt=TotInt+rInten(ib)
   20 CONTINUE
C
      END

C----------------------------------------------------------------------
      subroutine auger_spect(Nb,Ndeg,xRe,xIm,rInten,TotInt)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION xRe(Nb,Ndeg),xIm(Nb,Ndeg),rInten(Nb)
C
      TotInt=0.0
      call ZERO(rInten,Nb)
C
      DO 20 I=1,Nb
        DO 10 J=1,Ndeg
          rInten(I)=rInten(I)+xRe(I,J)**2+xIm(I,J)**2
   10   CONTINUE
        TotInt=TotInt+rInten(I)
   20 CONTINUE
      END
C
C----------------------------------------------------------------------
      subroutine find_data ( ISTATE, IR, routine, H, nm, n, E, D )
c--   The values of H, E and D are required. If they have been
c--   calculated before for the same value of istate and ir they are
c--   read from file 98, else "routine" is called to calculate them.
c--   The intention is that routine is either "diag" to get the
c--   eigenvectors and eigenvalues of H, or "tred3" to get the vectors
c--   that tridiagonalise H, and d and e then contain the diagonal and
c--   off-diagonal.
c--   Istate is meant to contain 1 for the ground state, 2 for the
c--   intermediate state, and 3,4,5 ... for the continuum configurations.
c--   Actually instead of the continuum configurations istate numbers
c--   the blocks into which the "b" state hamiltonian divides, which
c--   can be more than one per configuration.

      parameter (MAXIX=100)
      character ixir *20
      common    /store_index/ nix , ixstate(maxix)
      common    /store_ir_index/    ixir   (maxix)

      character * 20  ir
      dimension  h(nm,n), e(n), D(n)
      external   routine
      write(6,'(a,a,a,i3,a,i9)')
     &       ' Data required for symmetry ',ir,' state=',istate,
     &       '   dimension ',n
      rewind 98
      do 1   ix = 1, nix
        if ( ixstate(ix) .eq. istate  .and.  ixir(ix) .eq. ir )  then
          read(98)  (( h(i,j),i=1,n),j=1,n) , e, d
          write(6,'(a,i3)')
     &    ' Data read from file 98 record nr. ',ix
          return
        else
          read(98)
        endif
    1   continue

      call routine ( h, nm, n, e, d )
      if (nix .lt. MAXIX) then
        nix = nix + 1
        ixstate (nix)  = istate
        ixir    (nix)  = ir
        write (98)  (( h(i,j),i=1,n),j=1,n) , e, d
        write(6,'(a,a,i4)')
     &         ' Data calculated and stored on file98 ',
     &          'record nr. ',nix
      else
        write(6,*)
     &  'Maximum number of stored matrixes has been reached. ',
     &  'Calculated matrix will not be stored. ',
     &  'You may increase MAXIX.'
      endif
      end

C----------------------------------------------------------------------
      subroutine delete_from_store ( istate_first, istate_last )
      parameter (MAXIX=100)
      character ixir *20
      common /store_index/ nix , ixstate(maxix)
      common /store_ir_index/    ixir   (maxix)

      if (nix.eq.0) return

      do 1 iix = 1, nix
    1   if (ixstate(iix) .ge. istate_first  .and.
     &      ixstate(iix) .le. istate_last)  ixstate(iix) = 0

c--   discard trailing unused records.
      do 2 iix = nix,1,-1
    2   if (ixstate(nix) .eq. 0 )  nix = nix - 1
c--
      write (6,'(a,i3,a,i3,a/a,i3)')
     &' States ',istate_first,' to ',min(maxix,istate_last),
     &' deleted from store (as far as they were present).',
     &' The number of records still in store is',nix
      end


C----------------------------------------------------------------------
      subroutine DIAG(HAM,Ndim,N,ENE,WK)
C     diagonalize matrix
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION HAM(Ndim,Ndim),ENE(Ndim),WK(Ndim)
C
      IF (NDIM.EQ.0) THEN
C        PRINT *,'CAUTION:...MATRIX IS 0*0      :RETURN DIAG'
        RETURN
      ENDIF
      call timing(' before diagonalization')
      PRINT '(2(A,I4))',' Matrix size=',ndim,'*',ndim
      call DCSMAA(HAM,Ndim,N,ENE,WK,IER)

      call timing(' after diagonalization')
      END
C

      subroutine DCSMAA ( ham, n ,nm , eig , work , ier )
c--   You may replace this routine by an other routine that
c--   calculates all eigenvectors and eigenvalues.
cjapan please do not remove this routine but comment it or rename it
c      to e.g. DCSMAAx.
c --- implicit real*8 (a-h,o-z)
      dimension  ham(nm,n), eig(n), work(n)
c--   find eigenvalues and eigenvectors of ham
cnonstandard
cgroningen
c      call timing(' time before TRED ')
      call tred3 ( ham, nm, n, eig, work)
c      call timing(' time before tql3 ')
      call tql3  (nm, n, EIG, work ,ham, IER)
c      call timing(' time after tql3 ')

c  NAG routine
c      call f02abf(HAM,N,N,eig,ham,n,work,IER)
      END


      subroutine tred3 (Z, NM, N, D, E)
c--   You may replace this routine by an other routine that
c--   tridiagonalises. It is used in Band and Auger.
cjapan leave this version in.
c --- implicit real*8 (a-h,o-z)
      DIMENSION Z(NM,N), D(N), E(N)
C
      IF (N.EQ.1) GO TO 160
C     ********** FOR I=N STEP -1 UNTIL 2 DO -- **********
      DO 150 II=2,N
         I = N + 2 - II
         L = I - 1
         H = 0.0
         SCALE = 0.0
         IF (L.LT.2) GO TO 40
C     ********** SCALE ROW (ALGOL TOL THEN NOT NEEDED) **********
         DO 30 K=1,L
            SCALE = SCALE + ABS(Z(I,K))
30       CONTINUE
C
         IF (SCALE.NE.0.0) GO TO 50
40       E(I) = Z(I,L)
         GO TO 140
C
50       DO 60 K=1,L
            Z(I,K) = Z(I,K)/SCALE
            H = H + Z(I,K)*Z(I,K)
60       CONTINUE
C
         F = Z(I,L)
         G = -SIGN(SQRT(H),F)
         E(I) = SCALE*G
         H = H - F*G
         Z(I,L) = F - G
         F = 0.0
C
         DO 100 J=1,L
            Z(J,I) = Z(I,J)/(SCALE*H)
            G = 0.0
C     ********** FORM ELEMENT OF A*U **********
            DO 70 K=1,J
               G = G + Z(J,K)*Z(I,K)
70          CONTINUE
C
            JP1 = J + 1
            IF (L.LT.JP1) GO TO 90
C
            DO 80 K=JP1,L
               G = G + Z(K,J)*Z(I,K)
80          CONTINUE
C     ********** FORM ELEMENT OF P **********
90          E(J) = G/H
            F = F + E(J)*Z(I,J)
100      CONTINUE
C
         HH = F/(H+H)
C     ********** FORM REDUCED A **********
         DO 120 J=1,L
            F = Z(I,J)
            G = E(J) - HH*F
            E(J) = G
C
            DO 110 K=1,J
               Z(J,K) = Z(J,K) - F*E(K) - G*Z(I,K)
110         CONTINUE
120      CONTINUE
C
         DO 130 K=1,L
            Z(I,K) = SCALE*Z(I,K)
130      CONTINUE
C
140      D(I) = H
150   CONTINUE
C
160   D(1) = 0.0
      E(1) = 0.0
C     ********** ACCUMULATION OF TRANSFORMATION MATRICES **********
      DO 220 I=1,N
         L = I - 1
         IF (D(I).EQ.0.0) GO TO 200
C
         DO 190 J=1,L
            G = 0.0
C
            DO 170 K=1,L
               G = G + Z(I,K)*Z(K,J)
170         CONTINUE
C
            DO 180 K=1,L
               Z(K,J) = Z(K,J) - G*Z(K,I)
180         CONTINUE
190      CONTINUE
C
200      D(I) = Z(I,I)
         Z(I,I) = 1.0
         IF (L.LT.1) GO TO 220
C
         DO 210 J=1,L
            Z(I,J) = 0.0
            Z(J,I) = 0.0
210      CONTINUE
C
220   CONTINUE
C
      RETURN
      END


      subroutine tql3 (NM, N, D, E, Z, IERR)
c-- do not replace this routine (except if you understand the following).
c-- tql3 is a slightly changed version of tql2. Tql2 transforms a
c-- n*n matrix Z, which is normally done to produce the eigenvectors,
c-- Tql3 transforms a NM*N matrix Z such that Z is transformed
c-- to the eigenvectors of the tridiagonal matrix (if, on entry, Z was
c-- given in the basis in which the "hamiltonian" was tridiagonal).
c-- The only change to the routine was replacing N by Nm twice, as
c-- indicated. Speed improvement is about a factor N/Nm.
c --- implicit real*8 (a-h,o-z)
      DIMENSION D(N), E(N), Z(NM,N)
C     IMPLICIT INTEGER*4 (I-N)
      REAL MACHEP
C               **********
      MACHEP = 2.**(-23)
C     ********** MACHEP IS A MACHINE DEPENDENT PARAMETER SPECIFYING
C                THE RELATIVE PRECISION OF FLOATING POINT ARITHMETIC.
C
      IERR = 0
      IF (N.EQ.1) GO TO 160
C
      DO 10 I=2,N
         E(I-1) = E(I)
10    CONTINUE
C
      F = 0.0
      B = 0.0
      E(N) = 0.0
C
      DO 110 L=1,N
         J = 0
         H = MACHEP*(ABS(D(L))+ABS(E(L)))
         IF (B.LT.H) B = H
C     ********** LOOK FOR SMALL SUB-DIAGONAL ELEMENT **********
         DO 20 M=L,N
            IF (ABS(E(M)).LE.B) GO TO 30
C     ********** E(N) IS ALWAYS ZERO, SO THERE IS NO EXIT
C                THROUGH THE BOTTOM OF THE LOOP **********
20       CONTINUE
30       IF (M.EQ.L) GO TO 100
40       IF (J.EQ.30) GO TO 150
         J = J + 1
C     ********** FORM SHIFT **********
         P = (D(L+1)-D(L))/(2.0*E(L))
         R = SQRT(P*P+1.0)
         H = D(L) - E(L)/(P+SIGN(R,P))
C
         DO 50 I=L,N
            D(I) = D(I) - H
50       CONTINUE
C
         F = F + H
C     ********** QL TRANSFORMATION **********
         P = D(M)
         C = 1.0
         S = 0.0
         MML = M - L
C     ********** FOR I=M-1 STEP -1 UNTIL L DO -- **********
         DO 90 II=1,MML
            I = M - II
            G = C*E(I)
            H = C*P
            IF (ABS(P).LT.ABS(E(I))) GO TO 60
            C = E(I)/P
            R = SQRT(C*C+1.0)
            E(I+1) = S*P*R
            S = C/R
            C = 1.0/R
            GO TO 70
60          C = P/E(I)
            R = SQRT(C*C+1.0)
            E(I+1) = S*E(I)*R
            S = 1.0/R
            C = C*S
70          P = C*D(I) - S*G
            D(I+1) = H + S*(C*G+S*D(I))
C     ********** FORM VECTOR **********
c-- transform Z instead of vectors. N was changed into Nm in next line.
            DO 80 K=1,Nm
               H = Z(K,I+1)
               Z(K,I+1) = S*Z(K,I) + C*H
               Z(K,I) = C*Z(K,I) - S*H
80          CONTINUE
C
90       CONTINUE
C
         E(L) = S*P
         D(L) = C*P
         IF (ABS(E(L)).GT.B) GO TO 40
100      D(L) = D(L) + F
110   CONTINUE
C     ********** ORDER EIGENVALUES AND EIGENVECTORS **********
      DO 140 II=2,N
         I = II - 1
         K = I
         P = D(I)
C
         DO 120 J=II,N
            IF (D(J).GE.P) GO TO 120
            K = J
            P = D(J)
120      CONTINUE
C
         IF (K.EQ.I) GO TO 140
         D(K) = D(I)
         D(I) = P
C
c-- N was changed to Nm in next line
         DO 130 J=1,Nm
            P = Z(J,I)
            Z(J,I) = Z(J,K)
            Z(J,K) = P
130      CONTINUE
C
140   CONTINUE
C
      GO TO 160
C     ********** SET ERROR -- NO CONVERGENCE TO AN
C                EIGENVALUE AFTER 30 ITERATIONS **********
150   IERR = L
160   RETURN
C     ********** LAST CARD OF tql3 **********
      END


C----------------------------------------------------------------------
cC     output to file 44
cjapan
      subroutine OUTPUT replaced
     &           (IW,iomega,N,ENE,rInten,NOG,NDEG,EG,CHARAC,TotInt)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION ENE(N),rInten(N),EG(NOG),CHARAC(3)
      character INFO*80
      common    /information/INFO
C
      IF (N.LE.0) THEN
        PRINT *,'This value should be equal to or greater than 1'
        PRINT '(1(A,I4))',' Nf=',N
        call quitprog('Error in subroutine OUTPUT')
      ENDIF
C
      write(IW,'(I5,A,A,A    )')  N,'  ''',INFO(1:70),''''
      write(IW,'( 5(F8.2,I8) )') (ENE(I),INT(1e4*rInten(I)),I=1,N)
      write(IW,'( A,I3,2(a,f8.4) )') 'Ground state is',ndeg,
     &  'fold degenerate. E(grnd)=',EG(1),' E(1st excited)=',eg(ndeg+1)
      write(IW,'( 3(A,I1,A,F8.5,A) )')(' Config ',I,'=',
     &  CHARAC(I)/NDEG,'%',I=1,3)
      write(IW,'(A,F10.4)')'Output Of Auger: Total Intensity=',TotInt
C
      RETURN
      END

C----------------------------------------------------------------------
cgroningen
      subroutine OUTPUT
     &           (IW,iomega,N,ENE,rInten,NOG,NDEG,EG,CHARAC,TotInt)
c --- implicit real*8 (a-h,o-z)
      DIMENSION ENE(n),rInten(n),EG(nog),CHARAC(3)
      character INFO*80
      common    /information/INFO
      parameter (lheap=999999999)
      common /heap     / x(lheap)

      call alloc ( 'output enex   ', n, ienex )
      call alloc ( 'output rIntenx', n, irIntenx )
      call copy  ( ene,     X(ienex),   n )
      call copy  ( rInten,  X(irIntenx), n )
      call order ( X(ienex),X(irIntenx), n )
      call condens
     &     ( X(ienex), X(irIntenx), n, TotInt*1e-6, .0001, nx )
      call primat(iomega, eg, X(ienex), X(irIntenx), nx,
     &            info(56:60),info(61:65),info(66:70),1,1)
c ******
      call cut_heap ( 'output', ienex )
      end

C--------------------------------------------------------------------
C                                                                   |
C     'CONDENS'                                                     |
C                                                                   |
C                                                                   |
C--------------------------------------------------------------------
      subroutine CONDENS ( DE, GF, NSP, Thresh, RESOL, NSPx )
      REAL DE(NSP),GF(NSP)
C
      IF (NSP.EQ.0) then
        nspx = 0
        RETURN
      endif

      NSPX =1
      DENS=DE(1)
      DO 80 I=2,NSP
         IF (DE(I)-DENS.LT.RESOL/2 .or. GF(i).lt.Thresh ) THEN
            GF(NSPX)=GF(NSpx)+GF(I)
         ELSE
            NSPX=NSPX+1
            DENS=MAX(DE(I),DENS+RESOL)
            GF(NSPX)=GF(I)
            DE(NSPX)=DENS
         ENDIF
   80 CONTINUE
      END
C
C
C
C--------------------------------------------------------------------
C                                                                   |
C     'ORDER'                                                       |
C                                                                   |
C                                                                   |
C--------------------------------------------------------------------
      subroutine ORDER(A,B,N)
C
C== Declarations:
      REAL A(N),B(N)
C
      NS=N
    1 NS=NS/2
      DO 5 I=1,NS
         DO 4 J=I+NS,N,NS
            AJ=A(J)
            BJ=B(J)
            DO 2 K=J,I+NS,-NS
               IF (A(K-1).LE.AJ) GOTO 3
               A(K)=A(K-NS)
    2       B(K)=B(K-NS)
    3       A(K)=AJ
    4    B(K)=BJ
    5 CONTINUE
      IF (NS.GT.1) GOTO 1
      END

C----------------------------------------------------------------------
      subroutine PRIMAT
     &        ( iomega, EG, EF, SPECt, NOF,
     &          IRG, IRT, IRF, idimg, idimf)
c --- implicit real*8 (a-h,o-z)
      CHARACTER*(*) IRG, IRT, IRF, irt_x*8
      DIMENSION EG(1), EF(NOF), SPECt(NOF)
      parameter (iw=44)

      if (iomega.eq.0) then
        irt_x = irt
      else
        write(irt_x,'(i3,a5)') iomega, irt
      endif

        write(iw, '(A,a8,1x,a8,1x,a8,1x,A,I7,A,I2,A,I2,A)')
     &   ' TRANSFORMED MATRIX FOR TRIAD ( ',IRG,IRT_x,IRF,
     &   ') (1*',NOF,') DIM : ',IDIMG,' : 0 :', IDIMF,
     &   ' ACTOR MULTIPOLE'
c---   numbers per line is 7 (format allows 12)
        DO 10 ILOW = 1, NOF, 7
          IHIG = MIN (ILOW + 7 -1, NOF)
          write (iw,'(A10,12F10.5)')
     &       ' BRA/KET : ',(  EF(I), I = ILOW, IHIG)
          write(iw, '(F9.5,A1,12F10.6)')
     &          EG(1),':', (SPECt(I), I = ILOW, IHIG)
   10   CONTINUE

      SPTOT = 0
      do 20 IF = 1,NOF
20      SPTOT = SPTOT + SPECt(IF)
      write(iw,'(A,F9.5)') ' Total intensity ',SPTOT
      END


C----------------------------------------------------------------------
      subroutine primat_japan ( eg, ef, spect, n )
      dimension  eg(1), ef(n), spect(n)
      character INFO*80
      COMMON /information/INFO
      write(44,'(i5,3a)') n,'  ''',info(1:70),''''
      write(44,'(5(f8.2,i8))')
     &  (ef(i)-eg(1), int(1e5*spect(i)),i=1,n)
      SPTOT = 0
      do 20 IF = 1,NOF
20      SPTOT = SPTOT + SPECt(IF)
      write( 6,'(A,F9.5)') ' Total intensity ',SPTOT
      write(iw,'(A,F9.5)') '%Total intensity ',SPTOT
      end

C--------------------------------------------------------------------
      subroutine MIDOUT(IW1,IW2,Ng,Na,Ndeg,Eg,Ea,aMg,aGa,CHARAC)
c --- implicit real*8 (a-h,o-z)
C
      PARAMETER (ITMP=3000)
      DIMENSION Eg(Ng),Ea(Na),aMg(Na,Ng),aGa(Na,Na),CHARAC(3),TMP(ITMP)
      CHARACTER INFO*80
      COMMON /INFORMATION/INFO
C
      IF (ITMP.LT.Na) THEN
        PRINT *,'INCREASE ITMP TO ',Na
        STOP 'subroutine MIDOUT'
      ENDIF
C
      write(IW1,'( I5,3A )') Na,'  ''',INFO(1:70),''''
C
      TotalInten=0.0
      DO 20 I=1,Na
        TMP(I)=0.0
        DO 10 J=1,Ndeg
          TMP(I)=TMP(I)+aMg(I,J)**2
   10   CONTINUE
        TotalInten=TotalInten+TMP(I)*1E4
   20 CONTINUE
C
      write(IW1,'( 5(F8.2,I8) )') (Ea(I)-EG(1),INT(1E4*TMP(I)),I=1,Na)
      write(IW1,'(A,F9.4,A,I3,A,F9.4,A,3F6.3)') '%Eg=',EG(1),
     &     ' NDeg=',NDEG,' : 1stExE=',EG(min(ndeg+1,ng)),
     &     ' Charc=',(CHARAC(I)/NDEG,I=1,3)
      write(IW1,'(A,F10.6)') '%TotalIntensity=',TotalInten/1E4
      write(IW1,'(A)') '%OUTPUT OF BANAUG INTERMEDIATE STATE'
C>>>>>
      write(IW2,'(A)') '%OUTOUT FOR NEWDRF'
      write(IW2,'(2A)') '%PARAMETER0 OUTPUT OF BANAUG INTERMEDIATE ',
     &                 'STATE'
      write(IW2,'(A,I5)') '%Number Of Data=',Na
      write(IW2,'(5A)') '%PARAMETER1 ',
     &                       INFO(1:1),' ',INFO(2:30),INFO(55:70)
cjapan
      call GammaOut(IW2,Na,Ea,EG(1),ITMP,TMP,aGa)
cgroningen
      call GammaOut( 44,Na,Ea,EG(1),ITMP,TMP,aGa)
      write(IW2,'(A,F9.4,A,I3,A,F9.4,A,3F6.3)') '%Eg=',EG(1),
     &     ' NDeg=',NDEG,' : 1stExE=',EG(min(ndeg+1,ng)),
     &     ' Charc=',(CHARAC(I)/NDEG,I=1,3)
      write(IW2,'(A,F10.6)') '%TotalIntensity=',TotalInten/1.0E4
      write(IW2,'(A)') '%OUTPUT OF BANAUG INTERMEDIATE STATE'
C>>>>>
      RETURN
      END
C--------------------------------------------------------------------
      subroutine GammaOut(IW,Na,Ea,EG,ITMP,TMP,aGa)
c --- implicit real*8 (a-h,o-z)
C
      DIMENSION Ea(Na),aGa(Na,Na),TMP(ITMP)
C
      PI=4.0*ATAN(1.0)
C
      write(IW,'(A)') '%FORMAT  1(F10.4,F10.6) '
      write(IW,'(2A10,A12,A5)') '%   Energy',' Intensity',' Gamma','I'
      DO 5 I=1,Na
       write(IW,'(F10.4,F10.6,F12.6,I5)')Ea(I)-EG,TMP(I),PI*aGa(I,I),I
    5 CONTINUE
      write(IW,'(A)') '%END'
      END
C
C--------------------------------------------------------------------
      subroutine UPPERCASE(TEXT)
c --- implicit real*8 (a-h,o-z)
C
      CHARACTER TEXT*(*) ,  CUPPER*26,CLOWER*26
      DATA CUPPER/'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      DATA CLOWER/'abcdefghijklmnopqrstuvwxyz'/
C
      DO 10 I=1,LEN(TEXT)
        LOWER=INDEX(CLOWER,TEXT(I:I))
        IF (LOWER.GE.1) THEN
          TEXT(I:I)=CUPPER(LOWER:LOWER)
        ENDIF
   10 continue
c--   And now delete underscores please
      j=0
      do 20 i=1,len(text)
        if(text(i:i).ne. '_') then
          j=j+1
          text(j:j) = text(i:i)
        endif
   20 continue
      END

C--------------------------------------------------------------------
      subroutine TotalYield
     &    ( IW, NOMEGA, IOMEGA, ESTART, ESTEP, Totint_b, TotInt )
c --- implicit real*8 (a-h,o-z)
C
      CHARACTER INFO*80
      COMMON /information/INFO
C
      IF (IOMEGA.EQ.1) THEN
        write(IW,'(A)') '%XYLINE'
        write(IW,'(A)') '%PARAMETER0 OUTPUT OF BANAUG   TOTAL YIELD'
        write(IW,'(5A)')'%PARAMETER1',INFO(1:1),' ',
     &                                INFO(2:30),INFO(55:70)
        write(IW,'(A,F10.4,A,I4)')'%ESTART=',ESTART,' NOMEGA=',NOMEGA
        write(IW,'(A)')'%FORMAT  1(F12.5,F12.5)'
	write(iw,'(a,t7,a,t20,a,t34,a)')
     &       '%', 'energy', 'yield', 'absorption'
	write(6, '(a,t7,a,t20,a,t34,a)')
     &       '%', 'energy', 'yield', 'absorption'
      ENDIF
C
      write(IW,'(2F12.5,i5,f12.5)')
     &      ESTART+ESTEP*(IOMEGA-1), TotInt_b, iOmega, Totint
      write(6 ,'(2F12.5,i5,f12.5)')
     &      ESTART+ESTEP*(IOMEGA-1), TotInt_b, iOmega, Totint
C
      IF (IOMEGA.EQ.NOMEGA) THEN
        write(IW,'(A)')'%END'
      ENDIF
      END

C----------------------------------------------------------------------
      subroutine timing(text)
cnonstandard
c-- put your own timing code here
      character text*(*)
c---VAX
c      character c_date*9, c_time*8

      logical first
      save first
      data first/.true./
      if (first) then
        first = .false.

cVAXextra        call lib$init_timer
      endif
      print*, text
cVAXextra      call lib$show_timer

c      call date (c_date)
c      call time (c_time)
c      print'(1x,a10,a10,f10.1,2x,a)', c_date, c_time, secnds(0.),text
c---  vax
      end
c
C----------------------------------------------------------------------
      subroutine CPUTIME
c --- implicit real*8 (a-h,o-z)
C
       call clok(TIME)
       write(6,'(A,f11.4,A)') ' ****** CPU TIME =',TIME,' SEC.*****'
       RETURN
       END

C----------------------------------------------------------------------
      subroutine clok(time)
c --- implicit real*8 (a-h,o-z)
      time = 0
cnonstandard
c vax
c      time=secnds(0.)
C SunOS
c      dimension tarr(2)
c      time = etime(tarr)
      end

C----------------------------------------------------------------------
      subroutine GetDef(TEXT)
c --- implicit real*8 (a-h,o-z)
      CHARACTER TEXT*(*),LINE*136
      COMMON /CHLINE/ LINE
C
      TEXT=LINE(1:80)
      call NEWLIN
      END

C----------------------------------------------------------------------
      subroutine SPACECUT(TEXT,LEN1)
c --- implicit real*8 (a-h,o-z)
      CHARACTER TEXT*(*)
C
      LEN1=0
      DO 10 I=1,LEN(TEXT)
        IF (TEXT(I:I).NE.' ') THEN
          LEN1=LEN1+1
          TEXT(LEN1:LEN1)=TEXT(I:I)
        ENDIF
   10 CONTINUE
      TEXT(len1+1:) = ' '
      END

C--------------------------------------------------------------------
      subroutine SUBANAx(E,DE,COM,TEXT)
c --- implicit real*8 (a-h,o-z)
C
      CHARACTER COM*(*),TEXT*(*),TEMP*20,LETTER*13
      DATA LETTER/'+-0123456789.'/
C
      IP=INDEX(TEXT,COM)
      IF (IP.EQ.0) THEN
        RETURN
      ELSEIF(IP.EQ.1) THEN
        E=E+DE
        RETURN
      ENDIF
C
      J=0
      DO 10 I=IP-1,1,-1
        J=J+1
        IDX=INDEX(LETTER,TEXT(I:I))
        IF (IDX.EQ.0) THEN
          PRINT *,'ERROR IN DEFINITION: PT=',I
          PRINT *,TEXT
          STOP 'subroutine SUBANAx'
        ELSEIF (IDX.EQ.1 .AND. J.EQ.1) THEN
          E=E+DE
          RETURN
        ELSEIF (IDX.EQ.2 .AND. J.EQ.1) THEN
          E=E-DE
          RETURN
        ELSEIF (IDX.EQ.1 .OR. IDX.EQ.2 .OR. I.EQ.1) THEN
          TEMP='                    '
          TEMP(20+I-IP+1:20)=TEXT(I:IP-1)
          READ(TEMP,'(BN,F20.0)',IOSTAT=IO) FAC
          IF (IO.EQ.0) THEN
            E=E+FAC*DE
            RETURN
          ELSE
            PRINT '(A,I5,A,A)',' IO ERROR NO=',IO,' OCCURED ',
     &              ' ,CHECK DEF LINE IN FILE 50'
            PRINT *,TEXT
            PRINT *,'CHECK THIS FIGURE---->',TEXT(I:IP-1)
            STOP 'subroutine SUBANA'
          ENDIF
        ENDIF
   10 CONTINUE
       END

C--------------------------------------------------------------------
      subroutine subana(cene, cene0, e, text, DEL, UCV, UVV)
      character cene*3, cene0*3, text*(*)
      IF (CENE.EQ.cene0) THEN
        E=0
        call SUBANAx(E,DEL ,'DEL'  , TEXT)
        call SUBANAx(E,UCV ,'UCV'  , TEXT)
        call SUBANAx(E,UVV ,'UVV'  , TEXT)
        call SUBANAx(E,1.0 ,'UNITY', TEXT)
      endif
      end

C--------------------------------------------------------------------
       subroutine ANALYDEF(TEXT,DEL,UCV,UVV,EG1,EG2,EG3,EF1,EF2,EF3)
c --- implicit real*8 (a-h,o-z)
C
       CHARACTER TEXT*(*),CENE*3
C
       call SPACECUT(TEXT,ILEN)
C
       I1=INDEX(TEXT,'DEF')
       I2=INDEX(TEXT,'=')
       IF (I1+6.NE.I2) THEN
         PRINT *,'Equals sign required in definition.'
         PRINT *,TEXT
         STOP 'subroutine ANALYDEF'
       ENDIF
       CENE=TEXT(I1+3:I2-1)
       TEXT=TEXT(I2+1:)
C
       call subana(cene,'EG1',eg1,text,DEL,UCV,UVV)
       call subana(cene,'EG2',eg2,text,DEL,UCV,UVV)
       call subana(cene,'EG3',eg3,text,DEL,UCV,UVV)
       call subana(cene,'EF1',ef1,text,DEL,UCV,UVV)
       call subana(cene,'EF2',ef2,text,DEL,UCV,UVV)
       call subana(cene,'EF3',ef3,text,DEL,UCV,UVV)
       END

C----------------------------------------------------------------------
      subroutine GETAUGER(DEL0,UCV0,UVV0,ELVA,DIP,CI,
     &                                        NOMEGA,ESTART,ESTEP)
c --- implicit real*8 (a-h,o-z)
C
      CHARACTER NAME*20,CHAGET*20,TEXT*80
      DIMENSION ELVA(9,3),DIP(9),CI(9)
      common    /com_gamma/ gamma

      gamma = 0
C
      NO =1
      DEL=DEL0
      UCV=UCV0
      UVV=UVV0
   10 NAME = CHAGET()
      IF (NAME.EQ.'NOMEGA') THEN
        NOMEGA = INTGET(NOMEGA)
      ELSEIF (NAME.EQ.'ESTART') THEN
        ESTART = REAGET(ESTART)
      ELSEIF (NAME.EQ.'ESTEP') THEN
        ESTEP = REAGET(ESTEP)
      ELSEIF (NAME.EQ.'NO') THEN
        ITEMP = INTGET(ITEMP)
        IF (ITEMP.LT.1 .OR. ITEMP.GT.9) THEN
          PRINT *,'ERROR: NO MUST BE 1..9 , NO UNCHANGED , NO=',NO
        ELSE
          NO=ITEMP
        ENDIF
      ELSEIF (NAME(1:3).EQ.'DEL') THEN
        DEL = REAGET(DEL)
      ELSEIF (NAME.EQ.'UCV') THEN
        UCV = REAGET(UCV)
      ELSEIF (NAME.EQ.'UVV') THEN
        UVV = REAGET(UVV)
      ELSEIF (NAME.EQ.'DIP') THEN
        DIP(NO) = REAGET(DIP(NO))
      ELSEIF (NAME.EQ.'CI') THEN
        CI(NO) = REAGET(CI(NO))
      elseif (name.eq.'GAMMA') then
        gamma  = reaget (gamma)
      ELSEIF (NAME.EQ.'DEF') THEN
        call GetDEF(TEXT)
        call ANALYDEF(TEXT,DEL,UCV,UVV,DUMMY1,DUMMY2,DUMMY3,
     &                ELVA(NO,1),ELVA(NO,2),ELVA(NO,3) )
        PRINT '(A,I2,3(A,F10.3))',' No=',NO,' EF1=',ELVA(NO,1),
     &                   ' EF2=',ELVA(NO,2),' EF3=',ELVA(NO,3)
      ELSEIF (NAME.EQ.'ENDAUGER') THEN
        RETURN
      ELSE
        write(6,*)'ERROR: Expected one of the following AUGER commands',
     &            'NOMEGA,ESTART, ESTEP, NO, UCV, UVV, DIP, CI, DEF'
        call warn(10)
      ENDIF
      GOTO 10
      END

c----------------------------------------------------------------------
      subroutine getact (NCONF, N2, N3, W, DEL, UCV, UVV, PRMULT,
     &                    EGRND, EFXAS,  DIPXAS,
     &       SwitchAuger, EFAUG, NOMEGA, ESTART, ESTEP, DIP, CI)
c --- implicit real*8 (a-h,o-z)

      parameter (maxlevels=100)
      common
     & /actors / NADD(2,2,3,3) , XADD(10,2,2,3,3)
c---  XADD(iadd,itask1,itask2,iconf1,iconf2) is the STRENGTH of
c---  the OPER number IADD  from configuration ICONF1 in TASK1
c---  to ICONF2 in TASK2 (TASK = ground or excited state)
c---  NADD is the number of operators in this actor (IADD in
c---  XADD is 1...NADD). Many array elements are not used.
      dimension XHAM(10) , XMIX(10) , X(10), NCONF(2)
      character *(20) NAME , CHAGET, text*80
      common    /qaz / old_cowan, intsize
      logical             old_cowan
      common/iter/max_iter
      common    /spectr/ erange
      parameter (maxexpansion=25)
      common /bandexpansion/ nexpansion, ashape(0:maxexpansion)
*
      DIMENSION EGRND(3), EFXAS(3), EFAUG(9,3),DIP(9),CI(9)
      LOGICAL first, old_ground, old_excite, prmult, SwitchAuger
      save first
      data first /.true./

      if (first) then
        first    = .false.
        prmult   = .false.
        NCONF(1) = 0
        NCONF(2) = 0
        N2       = 0
        N3       = 0
        W        = 0
        DEL      = 0
        UVV      = 0
        UCV      = 0
        old_cowan = .false.
        call ZERO  (XADD, 10*2*2*3*3)
        call izero (NADD,    2*2*3*3)
        call ZERO  (EGRND,3)
        call ZERO  (EFXAS,3)
        call ZERO  (EFAUG,9*3)
        call ZERO  (DIP  ,9)
        DIPXAS=1.0
        DO 3 I=1,9
          CI(I)=1.0
    3   CONTINUE
        switchAuger = .FALSE.
	max_iter    = 100
        erange       = 1e-6
        call zero  (ashape,maxexpansion)
c---    default band shape is SQUARE
        nexpansion = 0
        ashape(0)  = 1
      endif

      call STATUS ('getact')
      old_ground = .false.
      old_excite = .false.
   10 NAME = CHAGET()
           if (name .eq. 'OLDGROUND') then
         old_ground = .true.
      else if (name .eq. 'OLDEXCITE') then
         old_excite = .true.
      else if (NAME .eq. 'NCONF') THEN
         NCONF(1) = INTGET(IDUMMY)
         NCONF(2) = INTGET(IDUMMY)
      else if (NAME .EQ. 'N2'  ) THEN
           N2  =  INTGET (N2)
           call check(n2,100,'maxlevels',.false.)
      else if (NAME .EQ. 'N3'   ) THEN
           N3  =  INTGET (N3)
           call check(n3,100,'maxlevels',.false.)
      else if (NAME .EQ. 'W'    ) THEN
           W   =  REAGET (W )
      else if (NAME .EQ. 'DEL') THEN
           DEL =  REAGET (DEL)
      else if (NAME .EQ. 'UVV'  ) THEN
           UVV =  REAGET (UVV)
      else if (NAME .EQ. 'UCV'  ) THEN
           UCV =  REAGET (UCV)

      else if (NAME .EQ. 'EG1'  ) THEN
           EGrnd(1) =  REAGET (dum)
      else if (NAME .EQ. 'EG2'  ) THEN
           EGrnd(2) =  REAGET (dum)
      else if (NAME .EQ. 'EG3'  ) THEN
           EGrnd(3) =  REAGET (dum)

      else if (NAME .EQ. 'EF1'  ) THEN
           EFxas(1) =  REAGET (dum)
      else if (NAME .EQ. 'EF2'  ) THEN
           EFxas(2) =  REAGET (dum)
      else if (NAME .EQ. 'EF3'  ) THEN
           EFxas(3) =  REAGET (dum)

      else if (NAME .eq. 'XHAM' ) THEN
         NAD  = INTGET(NAD)
         do 20 IAD = 1,NAD
   20       XHAM(IAD) = REAGET(DUMMY)
         NSTATE = INTGET(NSTATE)
         do 30 ISTATE = 1,NSTATE
            ITASK = INTGET(ITASK)
            ICONF = INTGET(ICONF)
            NADD(ITASK,ITASK,ICONF,ICONF) = NAD
            do 40 IAD = 1,NAD
   40          XADD(IAD,ITASK,ITASK,ICONF,ICONF) = XHAM(IAD)
   30    continue
      else if (NAME .eq. 'XMIX') THEN
         NAD  = INTGET(NAD)
         do 50 IAD = 1,NAD
   50       XMIX(IAD) = REAGET(DUMMY)
         NINTER = INTGET(NINTER)
         do 60 IINTER = 1,NINTER
            ITASK = INTGET(ITASK)
            ICONF1= INTGET(ICONF1)
            ICONF2= INTGET(ICONF2)
            NADD(ITASK,ITASK,ICONF1,ICONF2) = NAD
            do 70 IAD = 1,NAD
   70          XADD(IAD,ITASK,ITASK,ICONF1,ICONF2) = XMIX(IAD)
   60    continue
      else if (NAME .eq. 'TRAN') THEN
         NTRANS = INTGET(NTRANS)
         do 80 ITRANS = 1,NTRANS
            ICONF1 = INTGET(ICONF1)
            ICONF2 = INTGET(ICONF2)
            NADD(  1,2,ICONF1,ICONF2) = 1
            XADD(1,1,2,ICONF1,ICONF2) = 1
   80    continue

      else if (NAME .eq. 'XTRAN' ) THEN
         NAD  = INTGET(NAD)
         do 90 IAD = 1,NAD
   90       X(IAD) = REAGET(DUMMY)
         NTRANS = INTGET(NTRANS)
         do 100 ITRANS = 1,NTRANS
            ICONF1 = INTGET(ICONF1)
            ICONF2 = INTGET(ICONF2)
            NADD (1,2, ICONF1, ICONF2) = NAD
            do 110 IAD = 1,NAD
  110         XADD(IAD,1,2, ICONF1, ICONF2) = X(IAD)
  100    continue


      else if (NAME .eq. 'PRMULT') then
         PRMULT = .true.

      else if (NAME .eq. 'OLDCOWAN') then
         old_cowan = .true.

C     This nows allows us to set the number of kets output *****
      else if (name .eq. 'MAXITER' ) then
         max_iter = intget (max_iter)

C     I added this to set range of bra states *****
      else if (name .eq. 'ERANGE') then
         erange = reaget(erange)

c---  Define band shape (Thole 11/mar/93)
      else if (name .eq. 'SQUARE') then
         nexpansion = 0
         ashape(0) = 1
      else if (name .eq. 'CIRCLE' ) then
         nexpansion = 2
         ashape(0) = 1
         ashape(2) =-1
      else if (name .eq. 'SHAPE' ) then
         nexpansion = intget (nexpansion)
         call check(nexpansion,maxexpansion,'maxexpansion',.false.)
         do 120 i = 0, nexpansion
  120      ashape(i) = REAGET(DUMMY)

      ELSE IF (NAME .EQ. 'DEF') THEN
         call GetDef(TEXT)
         call AnalyDef(TEXT,DEL,UCV,UVV,EGRND(1),EGRND(2),EGRND(3),
     &                                  EFXAS(1),EFXAS(2),EFXAS(3))
      ELSE IF (NAME .EQ. 'DIP') THEN
         DIPXAS = REAGET(DIPXAS)

      ELSE IF (NAME .EQ. 'AUGER') THEN
         SwitchAuger=.TRUE.
         call GetAuger(DEL,UCV,UVV,EFAUG,DIP,CI,NOMEGA,ESTART,ESTEP)

      else if (NAME .eq. 'TRIADS') THEN
         if (.not. old_ground) call delete_from_store(1,1)
         if (.not. old_excite) call delete_from_store(2,1000000)
         return

      else
         write(6,*) 'ERROR. Expected one of the following commands: ',
     &  ' NCONF , N2 , N3 , W , DEL , UVV , UCV , XHAM , XMIX , TRAN,'
     & ,' AUGER , DEF , DIP, OLDCOWAN, OLDGROUND, OLDEXCITE'
     & ,' or PRMULT'
         call WARN(10)
      endif
      goto 10
      end
