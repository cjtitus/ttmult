
/* ANSI C version of plotter program to produce plots on various devices,
   see the documentation in plotter.tex 
Edited by Michael L Baker, denoted with -mlb*/

#include "globals.h"
#include <string.h>
#include "input.h"
#include "spectra.h"
#include "devices.h"
#include "common.h"


/* Function prototypes */
static integer read_integer(integer);
static void reset_energy(void);
static void change_output(output_types);
static void interpret_plot(plot_modes);
static void interpret_commands(void);
static void shutdown(void);

/* routines to interpret commands and set up data they use */

static integer read_integer(integer limit)
{
  integer i;

  i = get_integer();
  if (i < 1 or i > limit) {
    fprintf(stderr, "Invalid integer given, using 1\n");
    return(1);
  } else
    return(i);
}

static void reset_energy()
/* Set the energy range back to dynamic selection */
{
  dynamic_range = true;
}

static void change_output(output_types image)
/* Open a new output file */
{
  integer pixels;
  char filename[STRING_SIZE];

  filename[0] = 0;
  get_string();
  if (command_string[0] == 0) {
    fprintf(stderr, "Unable to find name for new output file\n");
    return;
  }
  add_path(filename, command_string); 
  if (open_output_file(filename)) {
    image_type = image;
    pixels = get_integer();
    if (pixels > 100 and pixels < MAX_PTS)
      npoints = pixels;
  } else
    fprintf(stderr, "Output file open failed\n");
}

static void interpret_plot(plot_modes object)
/* Interpret the commands that request a plot be made. */
{
  plot_data plot1 = NULL, plot2 = NULL;
  real shift = 0.0;
  boolean preserve = preserve_scale;

  preserve_scale = false;
  get_string();
  if (command_string[0] == 0) {
    fprintf(stderr, "Label missing from plot command\n");
    return;
  }
  plot1 = find_plot(command_string);
  if (plot1 == NULL) {
    fprintf(stderr, "Unable to find label - %s\n", command_string);
    return;
  }
  if (object != plot_it) {
    get_string();
    if (command_string[0] == 0) {
      fprintf(stderr, "Second label missing from compare/subtract\n");
      return;
    }
    plot2 = find_plot(command_string);
    if (plot2 == NULL) {
      fprintf(stderr, "Unable to find second label - %s\n", command_string);
      return;
    }
    if (object == overlay)
      shift = get_real();
  }
  print_spectrum(plot1, plot2, object, shift, preserve);
}

static void interpret_commands()
/* get the next line from the command file and interpret its leading string */
{
  register boolean quit = false;

  title[0] = 0;
  frame_title[0] = 0;
  image_title[0] = 0;
  while (not (feof(stdin) or quit)) {
    get_buff(stdin);
    get_string();
    if (strcmp(command_string, "title") == 0)
      get_title(title);
    else if (strcmp(command_string, "frame_title") == 0)
      get_title(frame_title);
    else if (strcmp(command_string, "image_title") == 0)
      get_title(image_title);
    else if (strcmp(command_string, "label") == 0)
      get_title(label);
    else if (strcmp(command_string, "inform") == 0)
      inform = true;
    else if (strcmp(command_string, "dont_inform") == 0)
      inform = false;
    else if (strcmp(command_string, "allow_degeneracy") == 0)
      nodegeneracies = false;
    else if (strcmp(command_string, "lorentzian") == 0)
      interpret_lorentzian(&lorentzian, NULL);
    else if (strcmp(command_string, "reset_lorentz") == 0)
      reset_lorentz(&lorentzian);
    else if (strcmp(command_string, "gaussian") == 0)
      set_gaussian();
    else if (strcmp(command_string, "energy_range") == 0)
      set_energy_range();
    else if (strcmp(command_string, "reset_energy") == 0)
      reset_energy();
    else if (strcmp(command_string, "addlines") == 0)
      interpret_spectrum(false, false, false);
    else if (strcmp(command_string, "create") == 0)
      interpret_spectrum(true, false, false);
    else if (strcmp(command_string, "spectrum") == 0)
      interpret_spectrum(true, true, false);
    else if (strcmp(command_string, "peaks") == 0)
      interpret_spectrum(true, false, true);
    else if (strcmp(command_string, "plot") == 0)
      interpret_plot(plot_it);
    else if (strcmp(command_string, "compare") == 0)
      interpret_plot(compare);
    else if (strcmp(command_string, "overlay") == 0)
      interpret_plot(overlay);
    else if (strcmp(command_string, "subtract") == 0)
      interpret_plot(subtract);
    else if (strcmp(command_string, "latex") == 0) {
      fprintf(stderr, "LaTeX option not implemented, using default\n");
      change_output(hpgl);
    } else if (strcmp(command_string, "laserjet") == 0) {
      fprintf(stderr, "LaserJet option not implemented, using default\n");
      change_output(hpgl);
    } else if (strcmp(command_string, "hpgl") == 0)
      change_output(hpgl);
    else if (strcmp(command_string, "postscript") == 0)
      change_output(postscript);
    else if (strcmp(command_string, "xy") == 0)
      change_output(xy);
    else if (strcmp(command_string, "ylist") == 0)
      change_output(ylist);
    else if (strcmp(command_string, "portrait") == 0) {
      image_mode = portrait;
      test_flush();
    } else if (strcmp(command_string, "landscape") == 0) {
      image_mode = landscape;
      test_flush();
    } else if (strcmp(command_string, "rcg9") == 0)
      change_input(rcg9);
    else if (strcmp(command_string, "old_racah") == 0)
      change_input(old_racah);
    else if (strcmp(command_string, "racah") == 0)
      change_input(racah);
    else if (strcmp(command_string, "band") == 0)
      change_input(bander);
    else if (strcmp(command_string, "load") == 0)
      load_data();
    else if (strcmp(command_string, "rows_per_page") == 0) {
      rows_per_page = read_integer(5);
      test_flush();
    } else if (strcmp(command_string, "columns_per_page") == 0) {
      columns_per_page = read_integer(5);
      test_flush();
    } else if (strcmp(command_string, "vertical_order") == 0) {
      horizontal = false;
      test_flush();
    } else if (strcmp(command_string, "horizontal_order") == 0) {
      horizontal = true;
      test_flush();
    } else if (strcmp(command_string, "images_per_frame") == 0) {
      images_per_frame = read_integer(20);
      test_flush();
    } else if (strcmp(command_string, "preserve_scale") == 0)
      preserve_scale = true;
    else if (strcmp(command_string, "adjust_expt") == 0)
      adjust_scale = true;
    else if (strcmp(command_string, "preserve_expt") == 0)
      adjust_scale = false;
    else if (strcmp(command_string, "end") == 0)
      quit = true;
    else if (command_string[0] != 0) {
      fprintf(stderr, "Unable to understand command %s", command_string);
      fprintf(stderr, ", skipping rest of command line\n");
    }
  }
}

static void shutdown()
/* Close all those files that were actually opened */
{
  if (picture_file != NULL) {
    shut_output();
    fclose(picture_file);
  }
}

void main()
{
  interpret_commands();
  shutdown();
}
