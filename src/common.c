/* Edited by Michael L Baker, denoted with -mlb */
#include "globals.h"
#include "devices.h"
#include "input.h"
#include "spectra.h"
#include "common.h"
#include <string.h> /* added -mlb */

lorentz lorentzian = NULL;
char label[STRING_SIZE];
#ifdef ADJUST
boolean adjust_scale = ADJUST;
#else
boolean adjust_scale = true;
#endif

void set_gaussian()
/* Get the value of the gaussian used to convolute the spectra */
{
  real sigma;

  sigma = get_real();
  if (command_string[0] == 0 or sigma == 0.0)
    fprintf(stderr, "Unable to get valid value for gaussian\n");
  else
    gaussian = sigma;
}

void set_energy_range()
{
  real energy;

  energy = get_real();
  if (command_string[0] == 0) {
    fprintf(stderr, "Unable to get lowest energy value\n");
    return;
  }
  min_energy = energy;
  energy = get_real();
  if (command_string[0] == 0 or energy <= min_energy) {
    fprintf(stderr, "Unable to get upper energy value\n");
    return;
  }
  max_energy = energy;
  dynamic_range = false;
}

void load_options(real *shift, real *scale)
{
  if (strcmp(command_string, "shift") == 0)
    *shift = get_real();
  else if (strcmp(command_string, "scale") == 0)
    *scale = get_real();
  else
    fprintf(stderr, "Unknown option for load\n");
  if (*scale == 0.0) {
    fprintf(stderr, "Bad scale for expt data\n");
    *scale = 1.0;
  }
}

void load_data()
{
  plot_data plot = NULL;
  real shift = 0.0, scale = 1.0;
  integer i;
  char filename[STRING_SIZE];

  filename[0] = 0;
  get_string();
  if (command_string[0] == 0) {
    fprintf(stderr, "Unable to get an expt data file name\n");
    return;
  }
  /* add_path(filename, command_string); remove -mlb */
  if (label[0] == 0)
    fprintf(stderr, "No label has been given for this spectrum\n");
  plot = read_spectrum(filename);
  if (plot == NULL)
    fprintf(stderr, "Unable to load expt data\n");
  else {
    get_string();
    if (command_string[0] != 0) {
      load_options(&shift, &scale);
      get_string();
      if (command_string[0] != 0)
	load_options(&shift, &scale);
      plot->min_energy += shift;
      plot->max_energy += shift;
      for (i = 0; i < plot->npoints; i++)
	plot->spectrum[i] *= scale;
    }
    strcpy(plot->label, label);
    add_plot(plot);
  }
}

void change_input(input_type format)
/* Open a new file of spectral data */
{
  boolean overwrite = true;
  char filename[STRING_SIZE];

  filename[0] = 0;
  get_string();
  if (command_string[0] == 0) {
    fprintf(stderr, "Unable to get a new spectral data file name\n");
    return;
  }
 add_path(filename, command_string);
  get_string();
  if (command_string[0] != 0) {
    if (strcmp(command_string, "add") != 0) {
      fprintf(stderr, "Didn't understand input file option\n");
      return;
    } else
      overwrite = false;
  }
  if (!read_datafile(format, filename, overwrite))
    fprintf(stderr, "Data load failed\n");
}

void get_spectrum_parameters()
/* read in any information restricting the spectrum */
{
  temperature = 0.0;
  ground_energy_set = false;
  initial_state[0] = 0;
  final_state[0] = 0;
  operator_label[0] = 0;
  scale = 1.0;
  energy_shift = 0.0;
  get_string();
  while (command_string[0] != 0) {
    if (strcmp(command_string, "temp") == 0) {
      temperature = get_real();
      if (temperature == 0.0 or command_string[0] == 0)
        fprintf(stderr, "No temperature found for spectrum\n");
    } else if (strcmp(command_string, "istate") == 0) {
      get_string();
      if (command_string[0] == 0)
        fprintf(stderr, "No initial state label found\n");
      else
        strcpy(initial_state, command_string);
    } else if (strcmp(command_string, "fstate") == 0) {
      get_string();
      if (command_string[0] == 0)
        fprintf(stderr, "No final state label found\n");
      else
        strcpy(final_state, command_string);
    } else if (strcmp(command_string, "energy") == 0) {
      ground_energy = get_real();
      if (command_string[0] == 0)
        fprintf(stderr, "No ground state energy found for spectrum\n");
      else
        ground_energy_set = true;
    } else if (strcmp(command_string, "shift") == 0) {
      energy_shift = get_real();
      if (command_string[0] == 0) {
        fprintf(stderr, "No value found for energy shift\n");
        energy_shift = 0.0;
      }
    } else if (strcmp(command_string, "scale") == 0) {
      scale = get_real();
      if (command_string[0] == 0) {
        fprintf(stderr, "No scaling value found for spectrum\n");
        scale = 1.0;
      }
    } else if (strcmp(command_string, "operator") == 0) {
      get_string();
      if (command_string[0] == 0)
        fprintf(stderr, "No label found for transition operator\n");
      else
        strcpy(operator_label, command_string);
    } else
      fprintf(stderr, "Unable to interpret spectral command: %s\n",
              command_string);
    get_string();
  }
}

void interpret_spectrum(boolean create, boolean plots, boolean peaks)
/* Interpret the commands and work out just what we are trying to output. */
{
  plot_data plot = NULL;
  boolean preserve = preserve_scale;

  preserve_scale = false;
  get_spectrum_parameters();
  if (create) {
    plot = create_spectrum(true, lorentzian);
    if (plot == NULL) {
      fprintf(stderr, "Failure to create spectrum\n");
      return;
    } else if (plots) {
      print_spectrum(plot, NULL, plot_it, 0.0, preserve);
      remove_plot(plot);
    } else if (peaks) {
      print_peaks(plot);
      remove_plot(plot);
    } else {
      if (label[0] == 0)
        fprintf(stderr, "No label has been given for this spectrum\n");
      strcpy(plot->label, label);
      add_plot(plot);
    }
  } else
    (void) create_spectrum(false, lorentzian);
}
