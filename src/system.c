
/* These are the routines that depend heavily on the machine OS. Of course
   window dependencies are handled in a different manner. */
/* Edited by Michael L Baker, denoted with -mlb */

#include "system.h"
#include <string.h>

#ifdef UNIX 

#include <unistd.h>
#include <pwd.h>

static void strip_dir(char []);

static void strip_dir(char path[])
/* Strips the last directory segment off the path. */
{
  int i = strlen(path);

  if (path[i - 1] == '/')
    i--;
  while (path[i - 1] != '/')
    i--;
  path[i] = 0;
}


void add_path(char name[], char path[])
/* Interprets file and path info from path to alter the current filename
   stored in name. */
{
  int i = 0, j = 0;
  struct passwd *pwd;
  char *home_dir, current_dir[STRING_SIZE];

  if (path[0] == '/')
    /* The filename contains an absolute path so we ignore the path. */
    current_dir[0] = 0;
  else if (path[0] == '~') {
    /* The filename contains a path relative to someones home directory,
       so we again ignore the path. */
    if (path[1] == '/') {        /* The users home directory. */
      home_dir = getenv((const char *)"HOME");
      strcpy(current_dir, home_dir);
      i = 2;
    } else {                       /* Its the home of another user. */
      /* copy the user name. */
      i = 1;
      while (path[i] != '/')
        current_dir[j++] = path[i++];
      current_dir[j] = 0;
      i++;
      /* find the users home directory. */
      pwd = getpwnam(current_dir);
      strcpy(current_dir, pwd->pw_dir);
    }
    /* We don't expect a relative path in this case, should we? */
  } else if (strncmp(path, "../", 3) == 0) {
    /* A relative path. */
    if (name[0] == 0)
      (void) getcwd(current_dir, STRING_SIZE);
    else
      strcpy(current_dir, name);
    i = 0;
    do {
      i += 3;
      strip_dir(current_dir);
    } while (strncmp(&path[i], "../", 3) == 0);
  } else if (strncmp(path, "./", 2) == 0) {
    /* The current directory. Again further relative paths are not expected. */
    strcpy(current_dir, name);
    i = 2;
  } else
    strcpy(current_dir, name);
  if (current_dir[0] != 0)
    if (current_dir[strlen(current_dir) - 1] != '/')
      strcat(current_dir, "/");
  strcat(current_dir, &path[i]);
  strcpy(name, current_dir);
}

 #endif /* UNIX */ 

// -mlb remove the ifdef VMS option compleatly 

/* #ifdef VMS

#include <unixio.h>
#include <unixlib.h>

/* This probably needs work, especially if we want to use UNIX names in VMS */

/* void add_path(char name[], char path[])
/* Interprets file and path info from path to alter the current filename
   stored in name. */
/*{
  int i = 0, j = 0;
  struct passwd *pwd;
  char *home_dir, current_dir[STRING_SIZE];

  if (path[0] == '[' and path[1] != '.')
    /* The filename contains an absolute path so we ignore the path. */
//    current_dir[0] = 0;
/*  else
    strcpy(current_dir, name);
  if (current_dir[0] != 0)
    if (current_dir[strlen(current_dir) - 1] != '/')
      strcat(current_dir, "/");
  strcat(current_dir, &path[i]);
  strcpy(name, current_dir);
}

#endif /* VMS */

void change_dir(char path[])
{
  chdir(path);
}

boolean is_a_terminal(FILE *file)
{
  return(isatty(fileno(file)));
}

#ifdef DEBUG

/* These are trap routines for debugging purposes, to see if I've made a
   mistake with alloc/free. */

typedef struct memory_node {
  struct memory_node *next;
  void *address;
  unsigned long size;
} *mem_block;

static mem_block blocks = NULL, free_nodes = NULL;

void *mem_alloc(unsigned long size)
{
  void *ptr;
  mem_block node;

  ptr = malloc((unsigned)size);
  if (free_nodes == NULL)
    node = (mem_block) malloc(sizeof(struct memory_node));
  else {
    node = free_nodes;
    free_nodes = free_nodes->next;
  }
  node->next = blocks;
  blocks = node;
  node->address = ptr;
  node->size = size;
  return(ptr);
}

void free_mem(void *ptr)
{
  mem_block node = blocks, prev = NULL;

  while (node != NULL) {
    if (node->address == ptr)
      break;
    else {
      prev = node;
      node = node->next;
    }
  }
  if (node == NULL)
    fprintf(stderr, "Attempting to release free block at %x\n", ptr);
  else {
    if (node == blocks)
      blocks = blocks->next;
    else
      prev->next = node->next;
    node->next = free_nodes;
    free_nodes = node;  
    free((char *)ptr);
  }
}

#endif /* DEBUG */
