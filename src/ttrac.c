/* *	Code derived from program runner            *
*****************************************************
* Comment on ttrac written 2016 by Charles Titus,   *
* ctitus@stanford.edu                               *
*                                                   *
* ttrac has a long and complex history, which is    *
* mostly unknown to me, since I am writing at least *
* 32 years after some components were started, and  *
* have had no contact with any of the original      *
* authors.                                          *
*                                                   *
* All that can be said here about this code is that * 
* ttrac, ttrcg, and ttban form a system to compute  *
* spectra for molecules that can be described by    *
* the charge transfer multiplet theory framework.   *
*                                                   *
* This program seems to have been originally run    *
* on machines such as Sun, VAX, SPARC, Mac, etc     *
* As of 2016, the program has been modified to      *
* comply with the modern GCC found on Ubuntu 14.04  *
* It has been tested only by comparison to examples *
* that came with the source code, and by comparison *
* to the windows binaries distributed by Frank      *
* de Groot as the "CTM4XAS" program.                *
*                                                   *
* Two primary modifications to the original code    *
* should be documented.                             *
*                                                   *
* Firstly, numerous printf                          *
* statements have been modified to avoid compiler   *
* warnings. The original authors used the "%d"      *
* format for ints, longs, their own custom types,   *
* etc. Behaviour of "%d" is technically undefined   *
* if given a long, so "%d" has been changed to "%ld"*
* where appropriate, etc.                           *
*                                                   *
* Secondly, there was an insidious IO bug whereby   *
* the original authors defined a boolean type,      *
* and then tried to cast the return value of the    *
* isdigit() builtin function to a boolean. At one   *
* time, and with one compiler, isdigit() must have  *
* returned 1 for success. On my personal system it  *
* happens to return 2048, so I have made minor      *
* modifications to avoid any dependence on the      *
* return value of isdigit(). It is my hope that     *
* should future issues of this type arise, you      *
* the reader will be more prepared to find them     *
*                                                   *
* Compilation:                                      *
* gcc -o ttrac -lm ttrac.c                          *
*                                                   *
* Usage:                                            *
* ttrac file_from_ttrcg log_file_name < rac_input   *
*                                                   *
* Outputs:                                          *
* Logs to log_file_name, and outputs rme_out.dat,   *
* which should be used as an input to ttban         *
****************************************************/

/* *	    Definitions for case-statements         */
#define Line __LINE__
static void Caseerror();

/*	Definitions for non-local gotos */
#include <setjmp.h>
static struct Jb {
  jmp_buf jb;
} J[1];

#ifndef NON_ANSI
#include <stdlib.h>
#endif
#include <stdio.h>
#include <string.h>
#include <ctype.h>
/* NOTE: the system time lookup should probably also be changed if NON_ANSI,
         see clock() and CLOCKS_PER_SEC */
#include <time.h>
#include <math.h>

#ifdef NON_ANSI
#define slprintf(x)  strlen(x)
#else
#define slprintf(x)  x
#endif

#if defined(NON_ANSI) || defined(VMS)
#define const
#define signed
#endif

/* #define CONVEX */ /* use calls to CONVEX VECLIB BLAS and EISPACK routines */
/* The complex diagonalisation routines HTRIDI, HTRIBK are in veclib so we
   should consider making use of them in future */


/*  macros */
#define streq(x, y) 	       !strncmp(x, y, sizeof(x))
#define in_eq(x)		streq(Win->in_word,x)
#define wordcpy(x,y) 		strncpy(x, y, length_word)
#define per(i,ifirst,ilast) 	for( i=ifirst; i<=ilast; i++)
#define arraycpy(x,y,n) 	per(i,0,n-1) x[i] = y[i]
#define putbuf                  bufp += slprintf(sprintf(&buf[bufp],
#define member2(a,b,c)   	(((a)==(b)) || ((a)==(c)))
#define member3(a,b,c,d) 	(((a)==(b)) || ((a)==(c)) ||((a)==(d)))
#define NEW(type)               (type *)MALLOC(sizeof(type))

/* *	Definitions for standard types */
typedef char 		boolean;
typedef long int 	integer;
typedef double 		real;
#ifdef CONVEX
typedef long long int 	biginteger;
#else
typedef real 		biginteger;
#endif

#define false 	(boolean)0
#define true  	(boolean)1
#define maxint	2147483647
#define NIL 	0
#define lline 	130 /* LINELENGTH and FIELDLENGTH for PRINTMATRIX */
#define lnumber 10

/* *	Start of program definitions */
#define length_disk_buff 	72
#define length_term_buff 	79
#define continuation_char 	'%'
#define system_backspace 	0
#define length_printer_buff 	132
#define length_word 		16
#define length_in_buff 		512
#define length_out_buff 	512
#define margin_logfile 		1
#define main_prompt  		'>'
#define racah_prompt 		']'
#define c_heap_mess_step 	1500
#define maxgroups 		15
#define max_reps 		150
#define max_mult 		9
#define maxtriads 		1000
#define max6js 			200
#define increment_3jm 		1000
#define spin_char 		'S'

static char line_title[]   ="LINE";
static char log_title[]    ="LOGFILE";
static char main_title[]   ="INPUT";
static char logfile_head[] ="TT-RAC97";
static char disk_name[]    ="rme_out.dat";
static char def_rme_name[] ="rme.dat";
static char def_log_name[] ="racah.log";
static char line_name[]    ="line_file";
static char prefix06title[] ="F6/";
static char prefix03title[] ="F3/";

static char rme_name[80], log_name[80];

/* A trick to get ANSI type preprocessors to turn the filename into a string */
#define XSTRNG(a)	YSTRNG(a)
#define YSTRNG(a)	#a

#ifdef DISK0
static char data_name[]    = XSTRNG(DISK0);
#else
static char data_name[]    ="/home/jamie/work/irwin_group/ctm_sim/compilation_testing/clean/exec/disk0";
#endif

static char blank[] ="\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";



typedef char word_type[length_word];

typedef enum {  word, number, delimiter, end_of_statement,
                exit_loop, end_of_file
} item_type;
typedef enum {  finished_disk, main_disk, disk_one, disk_two } in_file_type;
typedef enum {  ask_free, ask_word, ask_string, ask_char } in_item_ask_type;
typedef enum {  p_logfile, p_line } p_file;

typedef struct {
  char in_buff[length_in_buff];
  integer 	in_index;
  integer 	in_last;
  item_type 	in_item;
  word_type 	in_word;
  char 		in_char;
  integer 	in_number;
  word_type 	in_title;
  boolean 	in_start;
  boolean 	in_echo;
  char 		in_prompt;
} type_in_record;


typedef enum {  unknown, stop } c_op_type;
typedef enum {  no_heap, temp_heap, safe_heap } c_heapname;

typedef struct c_record_tag *c_number;
typedef struct c_record_tag{
  c_number next;
  c_op_type op;
  real re, im;
} c_record;

typedef integer  grouptype;
typedef integer  ntriadtype;
typedef integer  nsixtype;
typedef integer  nthreetype;
typedef unsigned char  irreptype;
typedef signed   char  multtype;
typedef unsigned char  symtype;
typedef enum {  nothing, origdata, somevalues, allvalues } calctype;
typedef enum {  as_tables, product, so3, d00,  cyclic } group_storage_type;
typedef enum {  notable, primtable, fulltable } tabletype;
typedef enum {  embedded, coupled, reflected, so3so2,
                so3d00, d00so2, cyclic_cyclic } embedtype;

typedef integer 	permtype, coltype;

typedef struct {
  irreptype ir3[3 - 1 + 1];
   multtype mult3;
    symtype sym3;
          } triadtype;

typedef struct{
  irreptype ir6[6];
   multtype mult6[4]; 
    symtype sym6;
   c_number value6;
          } sixjtype;

typedef struct{
  irreptype ir9  [9 - 1 + 1];
   multtype mult9[6 - 1 + 1];
    symtype symcol, symrow;
   c_number value9;
          } ninejtype;

typedef struct{
  irreptype gpir[3 - 1 + 1], subir[3 - 1 + 1];
   multtype gpmult, submult;
   multtype brmult[3 - 1 + 1];
    symtype sym3jm;
    boolean star3;
   c_number value3;
} threejmtype;


typedef  struct {
    word_type ext_name, tag, title, int_name, iso_name, filetime, filedate;
      boolean continuous;
    grouptype timesused, group1, group2;
group_storage_type howstored;
     calctype calc;
    tabletype table6;
      integer last_rep;
     multtype last_trmult;
      integer labelwidth;
    word_type labels[max_reps + 1];
    irreptype conj[max_reps + 1];    
    irreptype power[max_reps + 1];    
    irreptype irpart1[max_reps + 1];    
    irreptype irpart2[max_reps + 1];    
     multtype part1mult[max_mult - -1 + 1];
     multtype part2mult[max_mult - -1 + 1];
      integer dimen[max_reps + 1];
      symtype two[max_reps + 1];
   ntriadtype ntriad;
     nsixtype nsix;
    triadtype tabletriad[maxtriads - 1 + 1];
     nsixtype zero6j;
} gpdata_;


typedef struct{
  word_type title, int_name, filetime, filedate;
   calctype calc3;
  tabletype table3;
   multtype last_brmult;
  embedtype howembed;
    boolean paired;
    boolean bothreflected;
  irreptype tilderep;
   multtype branches [max_reps + 1][max_reps + 1];
    symtype twojms   [max_reps + 1][max_reps + 1];
    integer chosenket[max_reps + 1][max_reps + 1];
 nthreetype nthree, max3jms;
threejmtype *table3jm;
          } branchdata_ ;

typedef boolean   branchpresent_[max_reps + 1][3][2]; 
typedef real **rmatrixtype;
typedef unsigned char linktype;
typedef long dimensiontype;
typedef struct groupx_tag 	*p_groupx;
typedef struct branch_tag 	*p_branch;
typedef struct j3_tag     	*p_j3;
typedef struct oper_tag   	*p_oper;
typedef struct redmat_tag 	*p_redmat;
typedef struct actor_tag      	*p_actor;
typedef struct matrix_tag     	*p_matrix;
typedef struct matel_tag      	*p_matel;
typedef struct complex_tag    	*p_complex;


typedef enum {  ground, excite, transi} tasktype;
typedef tasktype statetype;
typedef enum {  bra, opr, ket } braoprkettype;
typedef enum {  basis, opera  } jobtype;
typedef enum {  printraw, diag, printeig, trans,  printtrans} purptype;

typedef struct {
      long mult[excite + 1];
   boolean present[3][2];
  p_branch branchhead;
} irx_;

typedef struct {
  irreptype ir[3];
    boolean present[3];
   multtype trmult;
       p_j3 j3head;
} triadx_;    

typedef struct groupx_tag {
  grouptype slot;
      short nreps;
       irx_ ir [max_reps + 1];
  irreptype co [max_reps + 1];
    integer dimen [max_reps + 1];
   unsigned short ntriads;
   multtype lasttrmult;
   multtype lastbrmult;
   triadx_  triad [maxtriads];
} groupxtype;

typedef struct branch_tag {
  irreptype 	ir;
  multtype 	brmult;
  p_branch 	parent;
  irreptype 	gpir;
  long 		gpmult[excite + 1];
  long 		start[excite + 1];
  p_branch 	next;
} branchtype;

typedef struct j3_tag {
  p_j3 		next;
  p_branch 	branch[3];
  multtype 	gptrmult;
  c_number 	threej;
} j3type;


/* ACTORS, OPERATORS and the OPERATOR->ACTOR CONNECTIONS ARE READ from INPUT */

typedef struct complex_tag{
  real 	r, i;
} complextype;

typedef struct oper_tag {
  p_branch 	branch;
  word_type 	name;
  complextype   strength;
  p_oper 	next;
} opertype;

typedef struct redmat_tag {
  tasktype 	task;
  word_type 	name;
  multtype 	gptrmult;
  irreptype 	ir[3];
  p_matel 	matelhead;
  integer 	sequence; /* Used by the Racer version */
  p_redmat 	next;
} redmattype;

/* SET of the MATRICES of AN OPERATOR or A SUM of OPERATORS.         */
/* DEFINED BY the OPERATOR POINTGROUP IR, the TASKTYPE  and the NAME */

typedef struct actor_tag {
  word_type 	name;
  irreptype 	pgir;
  p_oper 	operhead;
  tasktype 	task;
  boolean 	purp[5];
  integer	mode[5];
  p_matrix 	matrixhead;
  p_actor 	next;
} actortype;

typedef real 		*realarray;
typedef realarray 	eigvaltype;

typedef struct matel_tag{
  complextype 	complex;
  p_matel 	next;
} mateltype;

typedef complextype **squaretype;

typedef struct matrix_tag{
  ntriadtype 	isgtriad;
  squaretype 	elementhead;
  eigvaltype 	eigval[3];
  p_matrix 	next;
} matrixtype;

struct { integer lfract; real small;} putnum_scales;


word_type tasktext[3] = {"GROUND","EXCITE","TRANSI"};
word_type boktext[3]  = {"BRA","OPR","KET"};
word_type jobtext[2]  = {"BASIS","OPERA"};
word_type purptext[5] = {"PRINTRAW","DIAG","PRINTEIG","TRANS","PRINTTRANS"};

/* global variabels */

linktype  lpoint;
p_actor   actorhead;
p_redmat  redmathead;
p_groupx  chain[maxgroups + 1];

integer 	nr_warn, oldtime, triadcalls;
FILE 		*disk, *disk0, *disk2, *line, *logfile;
in_file_type 	in_file;
type_in_record 	in_record[disk_two+1];
boolean 	debug, interactive, logging, log_is_open, line_is_open;
word_type 	margin_string;
word_type 	out_disk_title, line_out_disk;
integer		c_heapsize, level;
c_number 	c_zero, c_one, c_unknown, c_freeheap, c_top_tempheap, 
                c_bot_tempheap, c_top_safeheap, c_bot_safeheap;

boolean 	internal, debug_butler;
grouptype 	group, subgroup;
nsixtype 	last6j;
branchdata_ 	branchdata, *Wbr;
irreptype 	tildeir[max_reps + 1];
char 		buffer[length_disk_buff ];
sixjtype 	tablesixj[max6js+1];
gpdata_ 	gpdata[maxgroups + 1];

static boolean 		racer = false;
static complextype 	comzero, comlarge;
static realarray 	e, e2, tau1, tau2, eps;
		/* e in real/hemitian diag, e2, tau1, and tau2 in  */
		/* hermitian 	   diag, and eps in treatmatrix    */
static eigvaltype 	eigenval; /* from treatmatrix */
static complextype 	*c;
static rmatrixtype 	z; /* from diagon */
static squaretype 	a, u;
static long 		maxbra = 0, maxdim = 0;

integer pno[172] ={
  8,  4,  9,  2,  3,  5,  7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59,
 61, 67, 71, 73, 79, 83, 89, 97,101,103,107,109,113,127,131,137,139,149,151,157,
163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,
271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,
397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,
521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,
647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,
787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,
929,937,941,947,953,967,971,977,983,991,997,1
};

#ifndef TRAP

static void *MALLOC(size)
int size;
{
  void *ptr;

  if ((ptr = (void *)malloc(size)) == NULL) {
    fprintf(stderr,"Out of memory allocating %d bytes\n", size);
    exit(1);
  }
  return(ptr);
}

#define FREE(ptr)	free(ptr)

static void CHECK_MEM()
{
}

#else /* TRAP */

/* These are trap routines for debugging purposes, to see if I've made a
   mistake with alloc/free. */

typedef struct memory_node {
  struct memory_node *next;
  void *address;
  unsigned long size;
} *mem_block;

static mem_block blocks = NULL, free_nodes = NULL;
static unsigned int data_base = 0;

static void CHECK_MEM()
{
  unsigned long count = 0, size = 0;
  mem_block ptr = blocks;

  while (ptr != NULL) {
    count++;
    size += ptr->size;
    ptr = ptr->next;
  }
  printf("%d blocks not freed, using %d bytes\n", count, size);
}

static void *MALLOC(unsigned size)
{
  void *ptr;
  mem_block node;

  if (data_base == 0) data_base = sbrk(0);
  ptr = (void *)MALLOC(size);
  if (free_nodes == NULL)
    node = (mem_block) malloc(sizeof(struct memory_node));
  else {
    node = free_nodes;
    free_nodes = free_nodes->next;
  }
  if (ptr == NULL || node == NULL) {
    fprintf(stderr,"Out of memory, data segment is %d bytes\n",
	    sbrk(0) - data_base);
    exit(1);
  }
  node->next = blocks;
  blocks = node;
  node->address = ptr;
  node->size = size;
  return(ptr);
}

static void FREE(void *ptr)
{
  mem_block node = blocks, prev = NULL;

  while (node != NULL) {
    if (node->address == ptr)
      break;
    else {
      prev = node;
      node = node->next;
    }
  }
  if (node == NULL)
    fprintf(stderr,"Attempting to release free block at %x\n", ptr);
  else {
    if (node == blocks)
      blocks = blocks->next;
    else
      prev->next = node->next;
    node->next = free_nodes;
    free_nodes = node;  
    free((char *)ptr);
  }
}

#endif /* TRAP */

/*   use fortran column major storage for 2D arrays */

#define A(i,j) (*(a[j]+i))
#define U(i,j) (*(u[j]+i))
#define Z(i,j) (*(z[j]+i))
#define Eigvec(i,j) (*(eigvec[j]+i))



static void allocate_matrices()
{
  long i;

  /* use calloc ??? */
  e        = (realarray) MALLOC(maxdim * sizeof(real));
  e2       = (realarray) MALLOC(maxdim * sizeof(real));
  tau1     = (realarray) MALLOC(maxdim * sizeof(real));
  tau2     = (realarray) MALLOC(maxdim * sizeof(real));
  eps      = (realarray) MALLOC(maxdim * sizeof(real));
  eigenval = (realarray) MALLOC(maxdim * sizeof(real));
  c    = (complextype *) MALLOC(maxdim * sizeof(complextype));
  a    =(complextype **) MALLOC(maxdim * sizeof(complextype *));
  u    =(complextype **) MALLOC(maxdim * sizeof(complextype *));
  z    =        (real**) MALLOC(maxdim * sizeof(real *));
  a[0] = (complextype *) MALLOC(maxbra * maxdim * sizeof(complextype));
  u[0] = (complextype *) MALLOC(maxdim * maxdim * sizeof(complextype));
  z[0] = (real *) MALLOC(maxdim * maxdim * sizeof(real));
  for (i = 1; i < maxdim; i++)
    a[i] = a[i - 1] + maxbra;
  for (i = 1; i < maxdim; i++) {
    u[i] = u[i - 1] + maxdim;
    z[i] = z[i - 1] + maxdim;
  }
}

static void free_matrices()
{
  FREE(c);
  FREE(e);
  FREE(e2);
  FREE(tau1);
  FREE(tau2);
  FREE(eps);
  FREE(eigenval);
  FREE(a[0]);
  FREE(u[0]);
  FREE(z[0]);
  FREE(a);
  FREE(u);
  FREE(z);
}

#define buffer_size 2048
char buf[buffer_size];
short int bufp = 0;


void put_char(a)
char a;
{
  buf[bufp++] = a;
}

void put_name(a)
word_type a;
{
  bufp += slprintf(sprintf(&buf[bufp], "%s ",a));
}

integer qx(i)
integer i;
/* NUMBER of CHARACTERS of integer I */
{
  integer q;

  if (i >= 0) q = 0; else q = 1;
  do { q = q + 1; i = i / 10; } while (i != 0);
  return q;
}

void put_integer(a)
integer a;
{
  bufp += slprintf(sprintf(&buf[bufp], "%ld", a));
}

void put_input()
{
  integer i;

  {
    type_in_record *Win = &in_record[in_file];

    bufp += slprintf(sprintf(&buf[bufp], "  I obtained \""));
    switch (Win->in_item) {
      case number:            put_integer(Win->in_number);	break;
      case exit_loop: 
      case word:              bufp += slprintf(sprintf(&buf[bufp], " %s",Win->in_word)); 	break;
      case delimiter:         put_char(Win->in_char); 		break;
      case end_of_statement:  bufp += slprintf(sprintf(&buf[bufp], " <END of STATEMENT>")); 	break;
      case end_of_file:       bufp += slprintf(sprintf(&buf[bufp], " <END of FILE>")); 	break;
      default:
	Caseerror(Line);
    }
    bufp += slprintf(sprintf(&buf[bufp], " \" on file"));
    switch (in_file) {
      case main_disk: bufp += slprintf(sprintf(&buf[bufp], "  INPUT")); break;
      case disk_one:  bufp += slprintf(sprintf(&buf[bufp], "  DISK0")); break;
      case disk_two:  bufp += slprintf(sprintf(&buf[bufp], "  DISK2")); break;
      default:
	Caseerror(Line);
    }
    bufp += slprintf(sprintf(&buf[bufp], "  at column %ld of ->%.*s", 
      Win->in_index - 1, (int)Win->in_last, Win->in_buff));
  }
}

void send_warn();

void sequence(outfile)
FILE *outfile;
{
  integer index;

  index = 6;
  while ((line_out_disk[index - 1] == '9') && (index > 1)) {
    line_out_disk[index - 1] = '0';
    index = index - 1;
  }
  line_out_disk[index - 1] = ((line_out_disk[index - 1]) + 1);
  fprintf(outfile,"%8.16s", line_out_disk);
}

void send_buf(outfile, margin, length, diskfile)
FILE *outfile;
integer margin, length;
boolean diskfile;
{
  integer last, fold, lines, i;
  word_type error_line;
  boolean error;
  char marg_str[16], outbuff[length_out_buff + 1], chbuf;

  for (i = 0; i < margin; i++) marg_str[i] = margin_string[i];
  marg_str[margin] = 0;
  lines  = 0;
  error = false;
  outbuff[0] = 0;
  do {
    if (margin > 0) fputs(marg_str, outfile);
    last = 0;
    while ((last < length) && lines + last < bufp) {
      outbuff[last] = buf[lines + last];
      last++;
    }
    fold = last;
    if (lines + last < bufp) {
      while (outbuff[fold - 1] != ' ') {
	fold--;
	if (fold == 0) {
	  error = true;
	  fold = last;
	  wordcpy(error_line, line_out_disk);
	  break;
	}
      }
    }
    outbuff[fold] = 0;
    fputs(outbuff, outfile);
    if (diskfile) {
      /* 
      per(i,fold+1,length) putc(' ', outfile);
      */
      sequence(outfile);
    }
    putc('\n', outfile);
    lines += fold;
  } while (lines < bufp);
  if (error) {
    bufp = 0;
    bufp += slprintf(sprintf(&buf[bufp], " Unable to fold line %16s on file %s",
            error_line, out_disk_title));
    send_warn();
  }
}

void send_disk()
{
  send_buf(disk, 0, length_disk_buff, false);
  bufp = 0;
  fflush(disk);
}

void send()
{
  if (interactive) {
     send_buf(stdout, 1, length_term_buff, false);
     fflush(stdout);
  }
  if (logging) {
    send_buf(logfile, margin_logfile, length_printer_buff, false);
    fflush(logfile);
  }
  bufp = 0;
  if(bufp > buffer_size) {
    bufp += slprintf(sprintf(&buf[bufp], "  This line was too long for the buffer."));
    bufp += slprintf(sprintf(&buf[bufp], "  Program run may be damaged."));
    bufp += slprintf(sprintf(&buf[bufp], "  Maybe buffer_size should be increased."));
  }
}

void send_mess()
{
  boolean oldinter;

  oldinter = interactive;
  interactive = true;
  strcpy(margin_string," ===>>");
  bufp += slprintf(sprintf(&buf[bufp], "  <<<<MESSAGE>>>>"));
  send();
  strcpy(margin_string,"");
  interactive = oldinter;
}

void send_warn()
{
  boolean oldinter;

  oldinter = interactive;
  interactive = true;
  strcpy(margin_string," >>>>>");
  bufp += slprintf(sprintf(&buf[bufp], "  <<<<WARNING>>>>"));
  send();
  strcpy(margin_string,"");
  interactive = oldinter;
  nr_warn = nr_warn + 1;
  if (nr_warn > 25) {
    bufp += slprintf(sprintf(&buf[bufp], " STOP BECAUSE NUMBER of WARNINGS > 25"));
    send();
    longjmp(J[0].jb, 9999);
  }
}

void warninput(need)
word_type need;
{
  bufp += slprintf(sprintf(&buf[bufp], " . I needed %s,", need));
  put_input();
  send_warn();
}

void warning(warn)
char *warn;
{
  bufp += slprintf(sprintf(&buf[bufp], " %s", warn)); send_warn();
}

void send_printer(file_name)
p_file file_name;
{
  if (file_name == p_line) {
    send_buf(line, 0, length_printer_buff, false);
    fflush(line);
  } else if (logging) {
    send_buf(logfile, 0, length_printer_buff, false);
    fflush(logfile);
  }
  bufp = 0;
}

void put_super(header)
char header[length_word];
{
  integer row[length_word], col[length_word];
  integer line, lett, i, j;
  char *list =
   {" ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-=>/?:()*'."};

char* a[5][5];
char* a0 =
"\
       AAA  BBBB   CCC  DDDD  EEEEE FFFFF  GGG  H   H IIIII \
      A   A B   B C   C D   D E     F     G   G H   H   I   \
      AAAAA BBBB  C     D   D EEEE  FFFF  G     HHHHH   I   \
      A   A B   B C   C D   D E     F     G  GG H   H   I   \
      A   A BBBB   CCC  DDDD  EEEEE F      GGG  H   H IIIII \
JJJJJ K   K L     M   M N   N  OOO  PPPP   QQQ  RRRR   SSS  \
  J   K K   L     MM MM NN  N O   O P   P Q   Q R   R S     \
  J   KK    L     M M M N N N O   O PPPP  Q   Q RRRR   SSS  \
J J   K K   L     M   M N  NN O   O P     Q QQQ R R       S \
 J    K   K LLLLL M   M N   N  OOO  P      QQQQ R   R  SSS  \
TTTTT U   U V   V W   W X   X Y   Y ZZZZZ   0     1    222  \
  T   U   U V   V W   W  X X   Y Y     Z   0 0   11   2   2 \
  T   U   U  V V  W W W   X     Y    ZZZ  0   0   1      2  \
  T   U   U   V   WW WW  X X    Y   Z      0 0    1    2    \
  T    UUUU   V   W   W X   X   Y   ZZZZZ   0    111  22222 \
 333     4  55555  666  77777  888   999                    \
3   3   44  5     6        7  8   8 9   9   +         ======\
  33   4 4  5555  6666   777   888   9999 +++++ -----       \
    3 44444 5   5 6   6   7   8   8    9    +         ======\
 333     4   555   666  7      888   9                      \
 >        /  ???           (   )           ***              \
===>     /  ?   ?   ::    (     )   *   *    *              \
    >   /      ?         (       )   * *    *               \
===>   /      ?           (     )     *           *         \
 >    /       ?     ::     (   )    *   *         *         "
;
/*
    "       AAA  BBBB   CCC  DDDD  EEEEE FFFFF  GGG  H   H IIIII "
    "      A   A B   B C   C D   D E     F     G   G H   H   I   "
    "      AAAAA BBBB  C     D   D EEEE  FFFF  G     HHHHH   I   "
    "      A   A B   B C   C D   D E     F     G  GG H   H   I   "
    "      A   A BBBB   CCC  DDDD  EEEEE F      GGG  H   H IIIII "
    "JJJJJ K   K L     M   M N   N  OOO  PPPP   QQQ  RRRR   SSS  "
    "  J   K K   L     MM MM NN  N O   O P   P Q   Q R   R S     "
    "  J   KK    L     M M M N N N O   O PPPP  Q   Q RRRR   SSS  "
    "J J   K K   L     M   M N  NN O   O P     Q QQQ R R       S "
    " J    K   K LLLLL M   M N   N  OOO  P      QQQQ R   R  SSS  "
    "TTTTT U   U V   V W   W X   X Y   Y ZZZZZ   0     1    222  "
    "  T   U   U V   V W   W  X X   Y Y     Z   0 0   11   2   2 "
    "  T   U   U  V V  W W W   X     Y    ZZZ  0   0   1      2  "
    "  T   U   U   V   WW WW  X X    Y   Z      0 0    1    2    "
    "  T    UUUU   V   W   W X   X   Y   ZZZZZ   0    111  22222 "
    " 333     4  55555  666  77777  888   999                    "
    "3   3   44  5     6        7  8   8 9   9   +         ======"
    "  33   4 4  5555  6666   777   888   9999 +++++ -----       "
    "    3 44444 5   5 6   6   7   8   8    9    +         ======"
    " 333     4   555   666  7      888   9                      "
    " >        /  ???           (   )           ***              "
    "===>     /  ?   ?   ::    (     )   *   *    *              "
    "    >   /      ?         (       )   * *    *               "
    "===>   /      ?           (     )     *           *         "
    " >    /       ?     ::     (   )    *   *         *         "
;
*/

  per(i,0,4) per(j,0,4) a[i][j] = &a0[60*(5*i+j)];
  
  per(i,0,length_word-1) {
    if (header[i]==0) break;
    per(lett,0,49) 
    if (list[lett] == header[i]) {
      row[i] =  lett / 10;
      col[i] = (lett * 6) % 60;
      break;
    }
  }
  per(line,0,4) {
    per(i,0,length_word-1) {
      if (header[i]==0) break;
      bufp += slprintf(sprintf(&buf[bufp], " %.6s",  &a [row[i]] [line] [col[i]] ));
    }
    send();
  }
}


void put_time()
{
  time_t clock;
  char *cur_time;

  (void) time(&clock);
  cur_time = ctime(&clock);
  cur_time[24] = 0;
  bufp += slprintf(sprintf(&buf[bufp], " %s", cur_time));
}

void close_file(disk)
FILE *disk;
{
  fclose(disk);
}


void open_printer(file_name, header)
p_file file_name;
char* header;
{
  if ((file_name == p_logfile)) {
    logging = true;
    if (!log_is_open) log_is_open = true;
    logfile = fopen(log_name,"w");
    if (logfile == NULL) {
      fprintf(stderr,"Unable to open logfile\n");
      exit(0);
    }
  } else {
    if (line_is_open)
      putc('\f', line);
    else {
      line = fopen(line_name,"w");
      if (line == NULL) {
        fprintf(stderr,"Unable to open line\n");
        exit(0);
      }
    }
    line_is_open = true;
  }
  put_time();
  send_printer(file_name);
  put_char(' ');
  send_printer(file_name);
  put_super(header);
  send_printer(file_name);
}

void new_page(file_name)
p_file file_name;
{
  if (file_name == p_logfile) {
    if (logging)
      putc('\f', logfile);
  } else {
    if (line_is_open)
      putc('\f', line);
    else {
      line_is_open = true;
      line = fopen(line_name,"w");
      if (line == NULL) {
        fprintf(stderr,"Unable to open line\n");
        exit(0);
      }
    }
  }
}

void close_printer(file_name)
p_file file_name;
{
  if (file_name == p_logfile) {
    log_is_open = false;
    close_file(logfile);
  } else {
    line_is_open = false;
    close_file(line);
  }
}

void open_out_disk(disk_title)
char *disk_title;
{
  disk = fopen(disk_name,"w");
  if (disk == NULL) {
    fprintf(stderr,"Unable to open disk\n");
    exit(0);
  }
  wordcpy(line_out_disk,"%0000000");
  wordcpy(out_disk_title, disk_title);
}

void close_out_disk()
{
  close_file(disk);
}

boolean feoln(unit)
FILE *unit;
{
  int ch;

  ch = getc(unit);
  ungetc(ch, unit);
  if (ch == '\n')
    return true;
  else
    return false;
}

boolean eof(file)
FILE *file;
{
  if (feof(file))
    return true;
  else {
    int ch;
    ch = getc(file);
    ungetc(ch, file);
    if (ch == 0)
      return true;
    else
      return false;
  }
}

void get_buff()
{
  integer backspace;
  integer i;
  char chbuf;

  if (in_file != finished_disk) {
    type_in_record *Win = &in_record[in_file];

    if (Win->in_item != end_of_file) {
      while (Win->in_buff[Win->in_index - 1] == ' ')
	Win->in_index = Win->in_index + 1;
      while ((Win->in_item != end_of_file) && 
             (Win->in_buff[Win->in_index - 1] == continuation_char)) {
	Win->in_index = 1;
	Win->in_last = 0;
	switch (in_file) {
	  case main_disk:
	    if (system_backspace != 0)
	      backspace = system_backspace;
	    else if ((unsigned) ('0') == 240)
	      backspace = 22;
	    else
	      backspace = 8;
            if( !Win->in_start && !eof(stdin) )
	      if (interactive && (Win->in_prompt != ' ')) {
	        putc(Win->in_prompt, stdout);
                putc('\n', stdout);
                fflush(stdout);
	    }
	    if (eof(stdin))
	      Win->in_item = end_of_file;
	    else
	      while ((chbuf = getchar()) != '\n') {
		if (chbuf != '\r') {
		  Win->in_buff[Win->in_last] = chbuf;
		  Win->in_last = Win->in_last + 1;
		  if (Win->in_last == length_in_buff - 1) {
		    bufp += slprintf(sprintf(&buf[bufp], " INCREASE LENGTH_IN_BUFF"));
		    send_warn();
		  }
		  if (Win->in_buff[Win->in_last - 1] == backspace) {
		    if (Win->in_last == 1)
		      Win->in_last = 0;
		    else
		      Win->in_last = Win->in_last - 2;
		  }
		}
	      }
	    if (Win->in_last != 0) {
	      Win->in_last = Win->in_last + 1;
	      Win->in_buff[Win->in_last - 1] = ';';
	    }
	    break;
	  case disk_one:
	    if (!eof(disk0))
	      getc(disk0);
	    if (eof(disk0))
	      Win->in_item = end_of_file;
	    else {
	      while (!feof(disk0) && (chbuf = getc(disk0)) != '\n') {
		if (chbuf != '\r') {
		  Win->in_last = Win->in_last + 1;
		  Win->in_buff[Win->in_last - 1] = chbuf;
		  if (Win->in_last == length_in_buff) {
		    bufp += slprintf(sprintf(&buf[bufp], " INCREASE LENGTH_IN_BUFF"));
		    send_warn();
		  }
		}
	      }
              ungetc(chbuf, disk0);
	    }
	    if (Win->in_last > length_disk_buff)
	      Win->in_last = length_disk_buff;
	    break;
	  case disk_two:
	    if (!Win->in_start && !eof(disk2))
	      getc(disk2);
	    if (eof(disk2))
	      Win->in_item = end_of_file;
	    else {
	      while (!feof(disk2) && (chbuf = getc(disk2)) != '\n') {
		if (chbuf != '\r') {
		  Win->in_last = Win->in_last + 1;
		  Win->in_buff[Win->in_last - 1] = chbuf;
		  if (Win->in_last == length_in_buff) {
		    bufp += slprintf(sprintf(&buf[bufp], " INCREASE LENGTH_IN_BUFF"));
		    send_warn();
		  }
		}
	      }
              ungetc(chbuf, disk2);
            }
	    break;
	  default:
	    Caseerror(Line);
	}
	Win->in_start = false;
	if (Win->in_echo || (in_file == main_disk)) {
	  bufp += slprintf(sprintf(&buf[bufp], " ECHO:%.*s",(int)Win->in_last,Win->in_buff));
	  send();
	} 

        per(i,1,Win->in_last) 
            Win->in_buff[i - 1] = toupper(Win->in_buff[i - 1]);

	if ((Win->in_item != end_of_file)) {
	  Win->in_last++;
	  Win->in_buff[Win->in_last - 1] = continuation_char;
	  while (Win->in_buff[Win->in_index - 1] == ' ')
	    Win->in_index++;
	}
      }
      if ((Win->in_item != end_of_file)) {
	if (Win->in_buff[Win->in_index - 1] == ';') {
	  Win->in_item  = end_of_statement;
	  Win->in_index++;
	} else if (Win->in_buff[1 - 1] == '$')
	  Win->in_item  = end_of_file;
	else
	  Win->in_item  = word;
      }
    }
  }
}

integer get_integer()
{
  boolean foundsign, founddigit, finished;
  char first;

  {
    type_in_record *Win = &in_record[in_file];

    get_buff();
    Win->in_number = 0; 
    if ( member2(Win->in_item, end_of_file, end_of_statement) )
      warning("INTEGER wanted");
    else {
      first = Win->in_buff[Win->in_index - 1];
      foundsign  = member2( first, '+','-' );
      founddigit = false;
      if (foundsign)
	do { Win->in_index++;
	} while (!(Win->in_buff[Win->in_index - 1] != ' '));
      while ( isdigit( Win->in_buff[Win->in_index - 1]) ) {
	founddigit = true;
	Win->in_number = 10 * Win->in_number 
                          + (unsigned) (Win->in_buff[Win->in_index - 1])
                          - (unsigned) ('0');
	Win->in_index++;
      }
      if (foundsign && !founddigit)
	Win->in_number = 1;
      if (!(foundsign || founddigit)) {
	Win->in_char = first;
	Win->in_item = delimiter;
	warninput("INTEGER");
      } else
	Win->in_item = number;
      if (first == '-')
	Win->in_number = -Win->in_number;
    }
    return Win->in_number;
  }
}

void get_item(ask_option)
in_item_ask_type ask_option;
{
  integer i;
  int letter, digit;
  char this;

  {
    type_in_record *Win = &in_record[in_file];

    get_buff();
    Win->in_char = ' ';
    memcpy(Win->in_word, blank,length_word);
    Win->in_number = -maxint;
    if ( !(member2( Win->in_item, end_of_file, end_of_statement ))) {
      this = Win->in_buff[Win->in_index - 1];
      if (ask_option == ask_char) {
	Win->in_index = Win->in_index + 1;
	Win->in_char = this;
	Win->in_item = delimiter;
      } else {
	digit = isdigit( this );
	letter =isalpha( this )
              || (digit && (ask_option == ask_word)) 
              || (ask_option == ask_string);
	if (letter) {
	  Win->in_item = word;
	  i = 1;
	  do {
	    if (i <= length_word)
	      Win->in_word[i - 1] = this;
	    else if (i == length_word + 1) {
	      bufp += slprintf(sprintf(&buf[bufp], " IDENTIfIER TOO LONG, char MAXIMUM=%d",length_word));
	      put_input();
	      send_warn();
	    }
	    Win->in_index++;  i++;
	    this = Win->in_buff[Win->in_index - 1];
	    letter = isdigit(this) || isalpha(this)
                  || ((ask_option == ask_string) 
                     && (this != 0)
		     && (this != ' ')
                     && (this != ';') 
                     && (this != continuation_char) );
	  } while (letter);
	} else if (digit || (this == '+') || (this == '-'))
	  Win->in_number = get_integer();
	else {
	  Win->in_index = Win->in_index + 1;
	  Win->in_char = this;
	  Win->in_item = delimiter;
	}
      }
    }
  }
}


void copy(diskx, in_title)
FILE *diskx;
char *in_title;
{
  char first_ch;
  unsigned char i;
  boolean ready;
  char chbuf;
  word_type wordcopy;

  disk0 = fopen(data_name,"r");
  if (disk0 == NULL) {
    fprintf(stderr,"Couldn't find butler data file %s\r",data_name);
    fprintf(stderr,"Please, check the compilation symbol");
    fprintf(stderr," DISK0 in the compile command,"); 
    fprintf(stderr," or the variable data_name in the racer source code.\r");
    exit(0);
  }
  do {
    if (!eof(disk0))
      first_ch = getc(disk0);
    if (first_ch == '$') {
      i = 0;
      if (!eof(disk0))
	do {
	  i = i + 1;
	  chbuf = getc(disk0);
          if (chbuf == ' ') chbuf = 0;
	  wordcopy[i - 1] = chbuf;
	} while (!((i == length_word) || feoln(disk0)));
    } else {
      while (first_ch != '\n') first_ch = getc(disk0);
      memcpy(wordcopy, blank, length_word);
    }
  } while ( !(streq(wordcopy, in_title) || eof(disk0) ) );
  if (eof(disk0)) {
    bufp += slprintf(sprintf(&buf[bufp], " \"%s\"%s", in_title," not found by OPEN_IN_FILE on DISK0"));
    bufp += slprintf(sprintf(&buf[bufp], "  (%s)", data_name));
    send_warn();
  }
}

void open_in_file(file_name)
char *file_name;
{
  if (in_file == disk_two) {
    bufp += slprintf(sprintf(&buf[bufp], " TOO MANY FILES OPENED"));
    send_warn();
  } else {
    in_file++;
    {
      type_in_record *Win = &in_record[in_file];

      Win->in_index = 1;
      Win->in_last = 0;
      Win->in_item = end_of_statement;
      memcpy(Win->in_word, blank, length_word);
      Win->in_char = ' ';
      Win->in_number = 0;
      Win->in_prompt = ' ';
      Win->in_buff[Win->in_index - 1] = continuation_char;
      wordcpy(Win->in_title, file_name);
      Win->in_start = true;
      Win->in_echo = false;
    }
    switch (in_file) {
      case main_disk:
	break;
      case disk_one:
	printf("CTPRINT Open %s as disk0\n",file_name);
	copy(disk0, file_name); /* file_name is searched on disk0 by copy*/
	break;
      case disk_two:
	disk2 = fopen(file_name,"r");
        if (disk2 == NULL) {
          fprintf(stderr,"Unable to open disk2\n");
          exit(0);
        }
	break;
      default:
	Caseerror(Line);
    }
  }
}

void close_in_file()
{
  switch (in_file) {
    case finished_disk:
      bufp += slprintf(sprintf(&buf[bufp], " CLOSED TOO MANY FILES: FATAL"));
      send_warn();
      break;
    case main_disk:
      break;
    case disk_one:
      printf("CTPRINT Close disk0\n");
      close_file(disk0);
      disk0 = NULL;
      break;
    case disk_two:
      close_file(disk2);
      break;
    default:
      Caseerror(Line);
  }
  in_file--;
}

void statistics(title)
char* title;
{
  clock_t time;
  boolean oldinter;
  real cpu_time, diff_time;

  oldinter = interactive;
  interactive = true;
  time = clock();
  cpu_time = (real)time / ((real)(CLOCKS_PER_SEC));
  diff_time = ((real)(time - oldtime))/ ((real)(CLOCKS_PER_SEC));
  printf("statist %8.2f %8.2f, HEAP = %8ld, NTHREE = %6ld %s\n",
          cpu_time, diff_time, c_heapsize, branchdata.nthree, title);
  oldtime = time;
  triadcalls = 0;
  interactive = oldinter;
}

void initialise_runner()
{
  in_file = finished_disk;
  debug = false;
  debug_butler = false;
  strcpy(margin_string,"");
  logging = false;
  line_is_open = false;
  branchdata.nthree = 0;
  Wbr = &branchdata;
  bufp = 0;
  log_is_open = true;
  interactive = false;
  if (interactive) {
    /* Rewrite(output); */
    send();
    send();
  }
  open_in_file( main_title);
  logging = true;
  if (logging)
    open_printer(p_logfile,  logfile_head);
  in_record[main_disk].in_echo = interactive;
  level = 0;
  nr_warn = 0;
  oldtime = 0;
  triadcalls = 0;
}

integer gcd(i, j)
integer i, j;
{
  integer k;

  i = abs(i);
  j = abs(j);
  while (j != 0) {
    k = i % j;
    i = j;
    j = k;
  }
  return i;
}

void concat(a, b, ab)
char *a, *b, *ab;
{
  strcpy(ab,a); strcat(ab,b);
}

integer max(a, b)
integer a, b;
{
  return (a > b) ?  a : b;
}

integer min(a, b)
integer a, b; { return (a < b) ?  a : b; }

static long int Trunc(f)
real f;  {  return f;  }

boolean c_known(a)
c_number a; { return (a == c_zero) ? (true) : (a->op != unknown); }


void put_exact(re)
real re;
{
  integer a, p, q, r, s, t, u;
  real x, y;

  if (re == 0)
    bufp += slprintf(sprintf(&buf[bufp], "  ( 0 )"));
  else if( fabs(re) < 1.0e-4 ) 
    bufp += slprintf(sprintf(&buf[bufp], "  ( small )"));
  else {
    p = 0; q = 1; r = 1; s = 0;
    x = re * re; y = x;
    while (fabs(x * s - r) > fabs(x * s) * 1.0e-12) {
      a = Trunc(y);
      t = a * r + p; p = r; r = t;
      u = a * s + q; q = s; s = u;
      if (y != a)  y = 1 / (y - a);
    }
    bufp += slprintf(sprintf(&buf[bufp], "  ( "));
    if (re < 0) bufp += slprintf(sprintf(&buf[bufp], "-"));
    if (r != 1) bufp += slprintf(sprintf(&buf[bufp], "#")); bufp += slprintf(sprintf(&buf[bufp], "%ld",r));
    if (s != 1) bufp += slprintf(sprintf(&buf[bufp], "/#%ld",s));
    bufp += slprintf(sprintf(&buf[bufp], " )"));
  }
}

void put_c_plain(c_num)
c_number c_num;
{
  if (c_num == c_zero || (c_num->re == 0 && c_num->im == 0) )
    bufp += slprintf(sprintf(&buf[bufp], " +0"));
  else if (!c_known(c_num))
    bufp += slprintf(sprintf(&buf[bufp], "  ?"));
  else {
    if(c_num->re != 0) { bufp += slprintf(sprintf(&buf[bufp], "  %.14f", c_num->re));    put_exact(c_num->re); }
    if(c_num->im != 0) { bufp += slprintf(sprintf(&buf[bufp], "  +%.14f*I", c_num->im)); put_exact(c_num->im); }
  }
}

void put_c_number(c_num)
c_number c_num;
{
  put_c_plain(c_num);
}

void c_shell(a)
c_number *a;
{
  if (c_freeheap != c_zero) {
    (*a) = c_freeheap;
    c_freeheap = (*a)->next;
  } else {
    (*a) = NEW(c_record);
    c_heapsize = c_heapsize + 1;
    if (c_heapsize % c_heap_mess_step == 0) {
      bufp += slprintf(sprintf(&buf[bufp], " HEAPSIZE ="));
      put_integer(c_heapsize);
      send_mess();
    }
  }
}

void c_new(a)
c_number *a;
{
  c_shell(a);
  {

    (*a)->op = stop;
    (*a)->next = c_top_tempheap;
    c_top_tempheap = (*a);
  }
}

void c_initialiseheaps()
{
  c_zero = NIL;
  c_heapsize = 0;
  c_freeheap = c_zero;
  c_shell(&c_one);
  c_one->re = 1;
  c_one->im = 0;
  c_one->op = stop;
  c_one->next = c_zero;
  c_bot_tempheap = c_one;
  c_top_tempheap = c_one;
  c_shell(&c_unknown);
  c_unknown->op = unknown;
  c_bot_safeheap = c_unknown;
  c_top_safeheap = c_unknown;
}

void c_markheap(c_mark)
c_number *c_mark;
{
  c_new(&(*c_mark));
}

void c_cutheap(c_mark)
c_number c_mark;
{
  c_number heap_ptr;

  heap_ptr = c_mark->next;
  c_mark->next = c_freeheap;
  c_freeheap = c_top_tempheap;
  c_top_tempheap = heap_ptr;
}

c_number c_move(a, dest)
c_number a;
c_heapname dest;
{
  c_number c_value;
  c_number a_free, c_ptr;

  if ((a == c_zero) || (a == c_unknown))
    return a;
  else {
    c_shell(&a_free);
    *a_free = *a;
    c_ptr = a_free;
    while ( !member2( a->op, unknown, stop ) ) {
      c_shell(&c_ptr->next);
      a = a->next;
      c_ptr = c_ptr->next;
      *c_ptr = *a;
    }
    switch (dest) {
      case no_heap:
	c_ptr->next = c_zero;
	break;
      case temp_heap:
	c_ptr->next = c_top_tempheap;
	c_top_tempheap = a_free;
	break;
      case safe_heap:
	c_ptr->next = c_top_safeheap;
	c_top_safeheap = a_free;
	break;
      default:
	Caseerror(Line);
    }
    return a_free;
  }
}

c_number c_copy(a)
c_number a;
{
  return c_move(a, temp_heap);
}

void c_save(a)
c_number *a;
{
  *a = c_move(*a, safe_heap);
}

integer delta(i, j)
integer i, j;
{
  return i==j ?  1 : 0;
}

integer phase(isym)
integer isym;
{
  return (isym & 1) ?  -1 : 1;
}

c_number c_conj(c_num)
c_number c_num;
{
  c_number conj;

  if (!c_known(c_num))
    return c_unknown;
  else if (c_num == c_zero)
    return c_zero;
  else {
    c_new(&conj);
    conj->re =  c_num->re;
    conj->im = -c_num->im;
    return conj;
  }
}

c_number c_minus(c_num)
c_number c_num;
{
  c_number minus;

  if (!c_known(c_num))
    return c_unknown;
  else if (c_num == c_zero)
    return c_zero;
  else {
    c_new(&minus);
    minus->re = -c_num->re;
    minus->im = -c_num->im;
    return minus;
  }
}

c_number c_invert(c_num)
c_number c_num;
{
  c_number invert;
  real norm;

  if (!c_known(c_num))
    return c_unknown;
  else if (c_num == c_zero) {
    bufp += slprintf(sprintf(&buf[bufp], " ZERO DIVIDE in C_INVERT"));
    send_warn();
    return c_unknown;
  } else {
    c_new(&invert);
    norm = c_num->re * c_num->re + c_num->im * c_num->im;
    invert->re =  c_num->re / norm;
    invert->im = -c_num->im / norm;
    return invert;
  }
}

c_number c_convert(num_whole, num_surd, den_whole, den_surd)
biginteger num_whole, num_surd, den_whole, den_surd;
{
  real d;
  c_number convert;

  d = den_whole * den_surd;
  if (d == (biginteger)0) {
    bufp += slprintf(sprintf(&buf[bufp], " ZERO DIVIDE in C_CONVERT"));
    send_warn();
    return c_unknown;
  } else if (num_whole * num_surd == (biginteger)0)
    return c_zero;
  else {
    c_new(&convert);
    {
      if (num_surd * den_surd < (biginteger)0) {
	convert->re = 0.0;
	convert->im = num_whole / ((real) den_whole)
                   * sqrt(-num_surd / ((real) den_surd));
      } else {
	convert->im = 0.0;
	convert->re = num_whole / ((real) den_whole)
                   * sqrt((num_surd) / ((real) den_surd));
      }
    }
    return convert;
  }
}

c_number c_sqrt(a)
c_number a;
{
  c_number b;

  if (!c_known(a))
    return c_unknown;
  else if (a == c_zero)
    return c_zero;
  else if (a->im != 0) {
    bufp += slprintf(sprintf(&buf[bufp], " cannot SQRT"));
    put_c_number(a);
    send_warn();
    return c_unknown;
  } else {
    c_new(&b);
    {
      if (a->re > 0) {
	b->re = sqrt(a->re);
	b->im = 0;
      } else {
	b->re = 0;
	b->im = sqrt(-a->re);
      }
    }
    return b;
  }
}

c_number c_mult(a, b)
c_number a, b;
{
  c_number mult;

  if (!(c_known(a) && c_known(b)))
    return c_unknown;
  else if ((a == c_zero) || (b == c_zero))
    return c_zero;
  else {
    c_new(&mult);
    mult->re = a->re * b->re - a->im * b->im;
    mult->im = a->re * b->im + a->im * b->re;
    return mult;
  }
}

c_number c_add(a, b)
c_number a, b;
{
  c_number add;

  if (!(c_known(a) && c_known(b)))
    return c_unknown;
  else if ((a == c_zero))
    return b;
  else if ((b == c_zero))
    return a;
  else {
    c_new(&add);
    add->re = a->re + b->re;
    add->im = a->im + b->im;
    return add;
  }
}


irreptype getirrep(gp)
grouptype gp;
{
  irreptype i, result = 0;
  {
    type_in_record   *Win = &in_record[in_file];
    gpdata_ *Wgp = &gpdata[gp];

    if (Win->in_item != end_of_statement) {
      if (internal) {
	get_item(ask_free);
	if ((Win->in_number > 0) && (Win->in_number < Wgp->last_rep)) {
	  result = Win->in_number;
	} else {
	  bufp += slprintf(sprintf(&buf[bufp], " INTERNAL LABEL not in 0.."));
	  put_integer(Wgp->last_rep);
	  put_input();
	  send_warn();
	}
      } else {
	i = 0;
	get_item(ask_string);
	if (Win->in_item != word)
	  warninput("IRREP");
	else {
	  while ((i < Wgp->last_rep) && (!in_eq( Wgp->labels[i]) ) )
	    i = i + 1;
	  if ( in_eq( Wgp->labels[i]) ) result = i;
	  else {
	    bufp += slprintf(sprintf(&buf[bufp], " %s%s","This is not a valid irrep for ",Wgp->ext_name));
	    warninput("IRREP");
	  }
	}
      }
    }
  }
  return result;
}

multtype getprodmult(gp)
grouptype gp;
{
  multtype result;
  integer i, j;

  result = 0;
  {
    type_in_record   *Win = &in_record[in_file];
    gpdata_ *Wgp = &gpdata[gp];

    if (Win->in_item != end_of_statement) {
      if (internal) {
	get_item(ask_free);
	if ((Win->in_number > 0) && (Win->in_number < max_mult)) {
	  result = Win->in_number;
	} else {
	  bufp += slprintf(sprintf(&buf[bufp], " INTERNAL MULT INDEX EXPECTED,"));
	  put_input();
	  send_warn();
	}
      } else if (Wgp->last_trmult > 0) {
	if (Wgp->howstored == product) {
	  i = getprodmult(Wgp->group1);
	  j = getprodmult(Wgp->group2);
	  result = i * (1 + gpdata[Wgp->group2].last_trmult) + j;
	} else {
	  get_item(ask_free);
	  if ((Win->in_number >= 0) && (Win->in_number <= Wgp->last_trmult))
	    result = Win->in_number;
	  else
	    warninput("PRODUCT MULT");
	}
      }
    }
  }
  return result;
}

multtype getbrmult()
{
  multtype result = 0;
  {
    type_in_record *Win = &in_record[in_file];

    if (Win->in_item != end_of_statement)
      if (internal || (branchdata.last_brmult > 0)) {
	get_item(ask_free);
	if ((Win->in_number >= 0) && (Win->in_number < max_mult))
	  result = Win->in_number;
	else
	  warninput("BRANCHING MULT");
      }
  }
  return result;
}

symtype getsym()
{
  symtype result = 0;
  {
    type_in_record *Win = &in_record[in_file];

    if (Win->in_item != end_of_statement) {
      get_item(ask_char);
      if (Win->in_char == '-')
	result = 1;
      else if (Win->in_char != '+')
	warninput("SYMMETRY + or -");
    }
  }
  return result;
}

void gettriad(tr, gp)
triadtype *tr;
grouptype gp;
{
  irreptype ir;

    per(ir,1,3)	  tr->ir3[ir - 1] = getirrep(gp);
    tr->mult3 = getprodmult(gp);
    tr->sym3 = 0;
}

void get6j(s, gp)
sixjtype *s;
grouptype gp;
{
  irreptype ir;
  integer m;

    per(ir,1,6)	  s->ir6[ir - 1] = getirrep(gp);
    per(m,1,4)	  s->mult6[m - 1] = getprodmult(gp);
    s->sym6 = 0;
    s->value6 = c_unknown;
}

void get9j(n, gp)
ninejtype *n;
grouptype gp;
{
  irreptype ir;
  integer m;

  {
    per(ir,1,9)	  n->ir9[ir - 1] = getirrep(gp);
    per(m,1,6)	  n->mult9[m - 1] = getprodmult(gp);
    n->symcol = 0;
    n->symrow = 0;
    n->value9 = c_unknown;
  }
}

void get3jm(t)
threejmtype *t;
{
  irreptype ir;
    per(ir,1,3)	  t->gpir[ir - 1] = getirrep(group);
    t->gpmult = getprodmult(group);
    per(ir,1,3)	  t->subir[ir - 1] = getirrep(subgroup);
    t->submult = getprodmult(subgroup);
    per(ir,1,3)	  t->brmult[ir - 1] = getbrmult();
    t->value3 = c_unknown;
    t->sym3jm = 0;
    t->star3 = false;
}

void putirrep(a, gp)
irreptype a;
grouptype gp;
{
  integer i, j;
  
  if (internal) { put_char(' '); put_integer(a);
  } else {
    gpdata_ *Wgp = &gpdata[gp];

    if (a > Wgp->last_rep)
      warning("INVALID-LABEL");
    else {
      bufp += slprintf(sprintf(&buf[bufp], "  %*s", (int)Wgp->labelwidth, Wgp->labels[a] ));
    }
  }
}

void putprodmult(a, gp)
multtype a;
grouptype gp;
{
  if (internal)
    bufp += slprintf(sprintf(&buf[bufp], "  %ld",(integer)a ));
  else {
    gpdata_ *Wgp = &gpdata[gp];

    if (Wgp->last_trmult > 0) {
      if (Wgp->howstored == product) {
	putprodmult(Wgp->part1mult[a - -1], Wgp->group1);
	putprodmult(Wgp->part2mult[a - -1], Wgp->group2);
      } else {
	bufp += slprintf(sprintf(&buf[bufp], "  %ld",(integer)a ));
      }
    }
  }
}

void putbrmult(a)
multtype a;
{
  if ((internal || (branchdata.last_brmult > 0)))
    bufp += slprintf(sprintf(&buf[bufp], "  %ld",(integer)a));
}

void putsym(s)
symtype s;
{
  if (s == 0)  bufp += slprintf(sprintf(&buf[bufp], "  +"));  else  bufp += slprintf(sprintf(&buf[bufp], "  -"));
}

void puttriad(tr, gp)
triadtype tr;
grouptype gp;
{
  integer i;

    per(i,1,3)	  putirrep(tr.ir3[i - 1], gp);
    putprodmult(tr.mult3, gp);
    putsym(tr.sym3);
    put_char(' ');
}

void put6j(s, gp)
sixjtype s;
grouptype gp;
{
  integer i, j;
  {
    if (debug) bufp += slprintf(sprintf(&buf[bufp], "  %s %ld    :",gpdata[gp].ext_name, level));
    per(i,1,3) putirrep(s.ir6[i - 1], gp);
    put_char(' ');
    per(i,4,6)	  putirrep(s.ir6[i - 1], gp);
    per(i,1,4)	  putprodmult(s.mult6[i - 1], gp);
    putsym(s.sym6);
    put_char(' ');
    put_c_number(s.value6);
  }
}

void put9j(n, gp)
ninejtype n;
grouptype gp;
{
  integer i, j;
  
  {
    per(i,1,3) putirrep(n.ir9[i - 1], gp);
    put_char(' ');
    per(i,4,6) putirrep(n.ir9[i - 1], gp);
    put_char(' ');
    per(i,7,9) putirrep(n.ir9[i - 1], gp);
    per(i,1,3) putprodmult(n.mult9[i - 1], gp);
    putsym(n.symrow);
    put_char(' ');
    per(i,4,6) putprodmult(n.mult9[i - 1], gp);
    putsym(n.symcol);
    put_char(' ');
    put_c_number(n.value9);
  }
}

void put3jm(t)
threejmtype t;
{
  integer i, j;
  {
    if (debug) bufp += slprintf(sprintf(&buf[bufp], "  %s %ld    :",branchdata.int_name, level));
    per(i,1,3) putirrep(t.gpir[i - 1], group);
    putprodmult(t.gpmult, group);
    put_char(' ');
    per(i,1,3) putirrep(t.subir[i - 1], subgroup);
    putprodmult(t.submult, subgroup);
    if ((gpdata[subgroup].last_trmult > 0) 
     && (branchdata.last_brmult > 0))  put_char(' '); 
    put_char(' ');
    per(i,1,3) putbrmult(t.brmult[i - 1]);
    putsym(t.sym3jm); 
    if (t.star3) put_char('*');
    put_char(' ');
    put_c_number(t.value3);
  }
}

void storetriad(tr, a, b, c, m, s)
triadtype *tr; irreptype a, b, c; multtype m; symtype s;
{
  tr->ir3[1 - 1] = a;
  tr->ir3[2 - 1] = b;
  tr->ir3[3 - 1] = c;
  tr->mult3 = m;
  tr->sym3 = s;
}

void store6j(s, a, b, c, d, e, f, k, l, m, n, p, q)
sixjtype *s;
irreptype a, b, c, d, e, f;
multtype k, l, m, n;
symtype p;
c_number q;
{
    s->ir6[1 - 1] = a;
    s->ir6[2 - 1] = b;
    s->ir6[3 - 1] = c;
    s->ir6[4 - 1] = d;
    s->ir6[5 - 1] = e;
    s->ir6[6 - 1] = f;
    s->mult6[1 - 1] = k;
    s->mult6[2 - 1] = l;
    s->mult6[3 - 1] = m;
    s->mult6[4 - 1] = n;
    s->sym6 = p;
    s->value6 = q;
}

void store9j(nj, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r)
ninejtype *nj;
irreptype a, b, c, d, e, f, g, h, i;
multtype j, k, l, m, n, o;
symtype p, q;
c_number r;
{
    nj->ir9[1 - 1] = a;
    nj->ir9[2 - 1] = b;
    nj->ir9[3 - 1] = c;
    nj->mult9[1 - 1] = j;
    nj->ir9[4 - 1] = d;
    nj->ir9[5 - 1] = e;
    nj->ir9[6 - 1] = f;
    nj->mult9[2 - 1] = k;
    nj->ir9[7 - 1] = g;
    nj->ir9[8 - 1] = h;
    nj->ir9[9 - 1] = i;
    nj->mult9[3 - 1] = l;
    nj->mult9[4 - 1] = m;
    nj->mult9[5 - 1] = n;
    nj->mult9[6 - 1] = o;
    nj->symrow = p;
    nj->symcol = q;
    nj->value9 = r;
}

void store3jm(tjm, a, b, c, d, e, f, g, h, i, j, k, l, m, n)
threejmtype *tjm;
irreptype a, b, c, d, e, f;
multtype g, h, i, j, k;
symtype l;
boolean m;
c_number n;
{
    tjm->gpir[1 - 1] = a;
    tjm->gpir[2 - 1] = b;
    tjm->gpir[3 - 1] = c;
    tjm->gpmult = g;
    tjm->subir[1 - 1] = d;
    tjm->subir[2 - 1] = e;
    tjm->subir[3 - 1] = f;
    tjm->submult = h;
    tjm->brmult[1 - 1] = i;
    tjm->brmult[2 - 1] = j;
    tjm->brmult[3 - 1] = k;
    tjm->sym3jm = l;
    tjm->star3 = m;
    tjm->value3 = n;
}

void findperm(col, perm)
coltype  col[3];
permtype perm[4];
{
  integer i;
  per(i,0,3) perm[i] = i;
  if (col[1 - 1] < col[2 - 1]) {
    perm[0] = 1;
    perm[1] = 2;
    perm[2] = 1;
  }
  if (col[perm[2] - 1] < col[3 - 1]) {
    if (col[perm[1] - 1] < col[3 - 1]) {
      perm[3] = perm[2];
      perm[2] = perm[1];
      perm[1] = 3;
    } else {
      perm[0] = (perm[0] + 1) % 2;
      perm[3] = perm[2];
      perm[2] = 3;
    }
  }
}

boolean findtriad(tr, gp)
triadtype *tr;
grouptype gp;
{
  permtype perm[4];
  coltype s[3], t[3];
  triadtype tr1, tr2;
  integer n, diff, up, low, place, power_sum, i;
  boolean found2, found;

  {
    gpdata_ *Wgp = &gpdata[gp];

   if (Wgp->howstored == product) {
      per(i,1,3) {
        tr1.ir3[i - 1] = Wgp->irpart1[tr->ir3[i - 1]];
        tr2.ir3[i - 1] = Wgp->irpart2[tr->ir3[i - 1]];
      }
      tr1.mult3 = Wgp->part1mult[tr->mult3 - -1];
      tr2.mult3 = Wgp->part2mult[tr->mult3 - -1];
      tr->sym3 = 0;
      found = false;
      if (findtriad(&tr2, Wgp->group2) && findtriad(&tr1, Wgp->group1)) {
	  found = true;
	  tr->sym3 = (tr1.sym3 + tr2.sym3) % 2;
      }
    } else {
      triadcalls = triadcalls + 1;
      per(i,1,3)  t[i - 1] = tr->ir3[i - 1];
      power_sum = Wgp->power[t[1 - 1]] + Wgp->power[t[2 - 1]]
                + Wgp->power[t[3 - 1]];
      found = !(boolean) (power_sum & 1);
      if (found) {
	findperm(t, perm);
	per(i,1,3)  t[i - 1] = tr->ir3[perm[i] - 1];
	found = (boolean) ((Wgp->power[t[1 - 1]] <=
                 (Wgp->power[t[2 - 1]] + Wgp->power[t[3 - 1]]))
                 && (tr->mult3 <= Wgp->last_trmult));
	place = 0;
      }
      if (found) {
	switch (Wgp->howstored) {
	  case as_tables:
	    diff = 0;
            per(i,1,3) {
		  s[i - 1] = Wgp->conj[t[i - 1]];
		  diff = max(diff, abs(s[i - 1] - t[i - 1]));
	    }
	    if (diff != 0) {
	      findperm(s, perm);
              per(i,1,3)  s[i - 1] = Wgp->conj[t[perm[i] - 1]];
	      if ((s[1 - 1] < t[1 - 1]) || ((s[1 - 1] == t[1 - 1])
              && ((s[2 - 1] < t[2 - 1]) || ((s[2 - 1] == t[2 - 1])
              &&  (s[3 - 1] < t[3 - 1])))))
		arraycpy(t,s,3);
	    }
	    up = Wgp->ntriad + 1;
	    low = 1;
	    do {
	      place = (up + low) / 2;
	      i = 0;
	      {
		triadtype *tab_tr = &Wgp->tabletriad[place - 1];
		do {
		  i = i + 1;
		  diff = t[i - 1] - tab_tr->ir3[i - 1];
		} while (!((i == 3) || (diff != 0)));
	      }
	      if (diff == 0)
		diff = tr->mult3 - Wgp->tabletriad[place - 1].mult3;
	      if (diff < 0)
		up = place - 1;
	      else
		low = place + 1;
	    } while (!((diff == 0) || (up < low)));
	    found = (boolean) (diff == 0);
	    tr->sym3 = Wgp->tabletriad[place - 1].sym3;
	    break;
	  case so3:
	    found = (!(boolean) ((power_sum) & 1));
	    tr->sym3 = ((power_sum) / 2) % 2;
	    break;
	  case d00:
	    if (t[3 - 1] > 2)
	      found = (boolean) (t[1 - 1] == t[2 - 1] + t[3 - 1] - 1);
	    else if (t[3 - 1] == 0)
	      found = (boolean) (t[1 - 1] == t[2 - 1]);
	    else if (t[2 - 1] > 2)
	      found = (boolean) (t[1 - 1] == t[2 - 1] - t[3 - 1] + 2);
	    else if (t[2 - 1] == 2)
	      found = false;
	    else
	      found = (boolean) ((t[1 - 1] == 2) || (t[1 - 1] == 3));
	    tr->sym3 = ((power_sum) / 2) % 2;
	    break;
	  case cyclic:
	    n = Wgp->last_rep + 1;
            per(i,1,3) {
		  if ((boolean) ((t[i - 1]) & 1))
		    s[i - 1] = (t[i - 1] + 1);
		  else
		    s[i - 1] = -t[i - 1];
	    }
	    found = ((s[1 - 1] + s[2 - 1] + s[3 - 1]) % (2 * n) == 0);
	    tr->sym3 = 0;
	    break;
	  default:
	    Caseerror(Line);
	}
      }
    }
  }
  return found;
}

void find9j(nine, gp)
ninejtype *nine;
grouptype gp;
{
}


void order(sixj,  star,  swapcols, s, mult, repeating, gp)
           sixjtype *sixj;    boolean *star;     boolean *swapcols; 
           irreptype s[6];   multtype mult[4];   boolean repeating; 
           grouptype gp;
/*
void order(sixjtype *sixj,    boolean *star,     boolean *swapcols, 
           irreptype s[6],   multtype mult[4],   boolean repeating, 
           grouptype gp)
*/
{
  integer i, col, col1;
  permtype perm[4];
  coltype  cols[3];
  irreptype ira, irb, irc;
  boolean star_it;
  {
    gpdata_ *Wgp = &gpdata[gp];

    if (repeating) 
      per(i,1,6) s[i - 1] = sixj->ir6[i - 1];
    else 
      per(i,1,6) s[i - 1] = min(sixj->ir6[i - 1],Wgp->conj[sixj->ir6[i - 1]]);
    per(i,1,3)
	  cols[i - 1] = sixj->mult6[i - 1] + max_mult * (max_reps
                          * max(s[i - 1],s[i + 3 - 1])
                          + min(s[i - 1],s[i + 3 - 1]));
    findperm(cols, perm);  /* NOW KNOW COL ORDER REQD */
    /* SET COLS to TOP LINE-BOTTOM LINE */
    per(i,1,3) cols[i - 1] = s[perm[i] - 1] - s[perm[i] + 3 - 1];
    if (repeating)  col = 0;  /* GOT FLIP CORRECT FIRST TIME */
    else {  /* DECIDE the COL to FLIP */
      if (cols[1 - 1] < 0) {
	col = 2;
	if (cols[2 - 1] < 0)  col = 3;
	else if (cols[2 - 1] == 0) {
	  if ((cols[3 - 1] > 0) || ((cols[3 - 1] == 0)
           && (mult[3 - 1] > mult[2 - 1]))) col = 3;
	}
      } else if (cols[1 - 1] == 0) {
	if (cols[2 - 1] < 0) {
	  if (cols[3 - 1] < 0)  col = 1;
	  else  col = 3;
	} else if ((cols[2 - 1] == 0) && (cols[3 - 1] < 0)) {
	  if (mult[3 - 1] > mult[4 - 1])  col = 1;
	  else  col = 2;
	} else if (cols[3 - 1] < 0)  	  col = 2;
	else  col = 0;
      } else { /* COLS[1]>0 */
	if ((cols[2 - 1] < 0) || ((cols[2 - 1] == 0)
         && (cols[3 - 1] < 0)))
	  col = 1;
	else
	  col = 0;
      }
    }  /* of 'REAL' DECISIONS,NOW SET S AS CORRECT 6J */
    col1 = (col % 3) + 1;
    per(i,1,3) {
      ira = sixj->ir6[perm[i] - 1];
      irb = sixj->ir6[perm[i] + 3 - 1];
      mult[i - 1] = sixj->mult6[perm[i] - 1];
      if ((boolean) ((perm[0]) & 1))  irb = Wgp->conj[irb];
      if (col != 0) {
        if (col != i) {
	  irc = ira;
	  ira = irb;
	  irb = irc;
      }
        if (col1 != i) {
	  ira = Wgp->conj[ira];
	  irb = Wgp->conj[irb];
	}
      }
      s[i - 1] = ira;
      s[i + 3 - 1] = irb;
    }
    mult[4 - 1] = sixj->mult6[4 - 1];
    if (col != 0) {
      multtype m = mult[col1 - 1];
      mult[col1 - 1] = mult[(col1 % 3) + 1 - 1];
      mult[(col1 % 3) + 1 - 1] = m;
      m = mult[col - 1];
      mult[col - 1] = mult[4 - 1];
      mult[4 - 1] = m;
    }
    /* FINALLY WE MUST GET COMPLEX CONJUGATION RIGHT */
    { integer i = 0, diff;
      do {
        i = i + 1;
        diff = s[i - 1] - Wgp->conj[s[i - 1]];
      } while (!((i == 6) || (diff != 0)));
      star_it = (boolean) (diff > 0);
      if ((i % 3) != 0) {
        if (s[i - 1] == Wgp->conj[s[i + 1 - 1]]) {
          star_it = (boolean) (!star_it);
  	  if ((i % 3) == 1)
  	    if (s[i + 1 - 1] == Wgp->conj[s[i + 2 - 1]])
  	      star_it = !star_it;
              /* If I=1 or 4 then GET STAR RIGHT BUT PERM MAY BE WRONG */
        }
      }
    }
    if (star_it) per(i,1,6) s[i - 1] = Wgp->conj[s[i - 1]];
    if (star_it) *star = ! *star;
    if ((perm[0]) & 1) *swapcols = ! *swapcols;
    store6j(sixj, s[1 - 1], s[2 - 1], s[3 - 1], 
                  s[4 - 1], s[5 - 1], s[6 - 1], 
            mult[1 - 1], mult[2 - 1], mult[3 - 1], mult[4 - 1],
            0, c_unknown);
  }
}

void putlowestirrepin3rdposition( sixj,  star, swapcols, s, mult, gp)
	sixjtype *sixj;    boolean *star; boolean *swapcols;  irreptype s[6]; 
	multtype mult[4];  grouptype gp;
/*
void putlowestirrepin3rdposition(
	sixjtype *sixj,    boolean *star, boolean *swapcols,  irreptype s[6], 
	multtype mult[4],  grouptype gp)
*/	
{
  integer i, col, col1;
  permtype perm[4];
  coltype  cols[3];
  irreptype ira, irb, irc;
  {
    gpdata_ *Wgp = &gpdata[gp];

    /* SET COLS to 3 DIGIT NUMBERS */
    per(i,1,3)
     cols[i - 1] = 
        max_reps * min(sixj->ir6[i - 1],sixj->ir6[i + 3 - 1])
                 + max(sixj->ir6[i - 1],sixj->ir6[i + 3 - 1]);
    findperm(cols, perm ); /* NOW KNOW COL ORDER REQD */
    /* PUT LOWEST IRREP in COLUMN 3 in UPPER POSITION */
    per(i,1,3)
      cols[i - 1] = sixj->ir6[perm[i] - 1] - sixj->ir6[perm[i] + 3 - 1];
    if (cols[3 - 1] < 0) {
      if (cols[2 - 1] < 0)
	col = 0;
      else
	col = 3;
    } else {
      if (cols[2 - 1] < 0)
	col = 2;
      else
	col = 1;
    }
    col1 = (col % 3) + 1;
    per(i,1,3){
      ira = sixj->ir6[perm[i] - 1];
      irb = sixj->ir6[perm[i] + 3 - 1];
      mult[i - 1] = sixj->mult6[perm[i] - 1];
      if ((boolean) ((perm[0]) & 1))
        irb = Wgp->conj[irb];
      if (col != 0) {
        if (col != i) {
          irc = ira;
	  ira = irb;
	  irb = irc;
        }
        if (col1 != i) {
	  ira = Wgp->conj[ira];
          irb = Wgp->conj[irb];
        }
      }
      s[i - 1] = ira;
      s[i + 3 - 1] = irb;
    }
    mult[4 - 1] = sixj->mult6[4 - 1];
    if (col != 0) {
      multtype m = mult[col1 - 1];
      mult[col1 - 1] = mult[(col1 % 3) + 1 - 1];
      mult[(col1 % 3) + 1 - 1] = m;
      m = mult[col - 1];
      mult[col - 1] = mult[4 - 1];
      mult[4 - 1] = m;
    }
    store6j(sixj, s[1 - 1], s[2 - 1], s[3 - 1], s[4 - 1], s[5 - 1], s[6 - 1], 
	mult[1 - 1], mult[2 - 1], mult[3 - 1], mult[4 - 1], 0, c_unknown);
    *star = false;
    *swapcols = (boolean) ((perm[0]) & 1);
  }
}

void search(sixj, gp, found, place_found)
sixjtype sixj;  grouptype gp; boolean *found; integer *place_found;
{
  integer i, diff, up, low, place;

  /* PERFORM A BINARY SEARCH in TABLESIXJ] */
  {
    gpdata_ *Wgp = &gpdata[gp];

    up  = Wgp->zero6j + Wgp->nsix;
    low = Wgp->zero6j + 1;
    do {
      place = (up + low) / 2;
      i = 0;
      {
	sixjtype *w6 = &tablesixj[place];

	do {
	  i = i + 1;
	  diff = sixj.ir6[i - 1] - w6->ir6[i - 1];
	} while (!((i == 6) || (diff != 0)));
        i = 0;
        if (diff == 0)
	do {
	  i = i + 1;
	  diff = sixj.mult6[i - 1] - w6->mult6[i - 1];
	} while (!((i == 4) || (diff != 0)));
      }
      if (diff < 0) up = place - 1;
      else  low = place + 1;
    } while (!((diff == 0) || (up < low)));
    *found = (diff == 0);
    *place_found = place;
  }
}


boolean find6j();


c_number be_sum_rhs(prod_of_sym6,
  muu1, muu2, muu3, nuu1, nuu2, nuu3, lam1, lam2, lam3, 
  mr1, mr2, mr3, ms1, ms2, ms3, gp
)
  symtype *prod_of_sym6;
  irreptype lam1, lam2, lam3, muu1, muu2, muu3, nuu1, nuu2, nuu3;
  multtype mr1, mr2, mr3, ms1, ms2, ms3;
  grouptype gp;
{
  irreptype lam;
  multtype mt1, mt2, mt3;
  c_number be_sum, dims;
  triadtype tr2, tr3, tr4;
  sixjtype s2, s3, s4;

  {
    gpdata_ *Wgp = &gpdata[gp];

    be_sum = c_zero;
    mt1 = 0;
    for (lam = 0; lam <= Wgp->last_rep; lam++)
    if (abs(Wgp->power[lam] - Wgp->power[muu1]) == 1) {
      storetriad(&tr4, lam, Wgp->conj[muu1], nuu1, mt1, 0);
      if (findtriad(&tr4, gp))
      for (mt2 = 0; mt2 <= Wgp->last_trmult; mt2++) {
	storetriad(&tr2, lam, Wgp->conj[muu2], nuu2, mt2, 0);
	if (findtriad(&tr2, gp))
	for (mt3 = 0; mt3 <= Wgp->last_trmult; mt3++) {
	  storetriad(&tr3, lam, Wgp->conj[muu3], nuu3, mt3, 0);
	  if (findtriad(&tr3, gp)) {
	    store6j(&s2, nuu2, Wgp->conj[muu2], lam, muu3, nuu3,
                Wgp->conj[lam1], ms1, mr1, mt3, mt2, 0, c_unknown);
	    store6j(&s3, nuu3, Wgp->conj[muu3], lam, muu1, nuu1,
                Wgp->conj[lam2], ms2, mr2, mt1, mt3, 0, c_unknown);
	    store6j(&s4, nuu1, Wgp->conj[muu1], lam, muu2, nuu2,
                Wgp->conj[lam3], ms3, mr3, mt2, mt1, 0, c_unknown);
	    if (find6j(&s3, gp)) if (find6j(&s4, gp)) if (find6j(&s2, gp)) {
	      dims = c_convert((biginteger) (phase(tr2.sym3
                   + tr3.sym3 + tr4.sym3) * Wgp->dimen[lam]),
                   (biginteger)1, (biginteger)1, (biginteger)1);
	      be_sum = c_add(c_mult(c_mult(c_mult(
                   dims, s2.value6), s3.value6), s4.value6), be_sum);
	      *prod_of_sym6 = (s2.sym6 + s3.sym6 + s4.sym6) % 2;
	    }
	  } else goto L1000;
	}
 L1000:;
      }
    }
  }
  if (debug) { bufp += slprintf(sprintf(&buf[bufp], "  BE RHS  SUM")); put_c_number(be_sum); send(); }
  return be_sum;
}

boolean find6j(sixj, gp)
sixjtype *sixj;
grouptype gp;
/* TAKES A 6J SYMBOL,REARRANGES IT INTO ITS STANDARD FORM and
   then TRIES to LOOKUP or CALCULATE the VALUE of the 6J and
   PUTS IT INTO SIXJ.VALUE6. */
{
  sixjtype s0, s1, s2, s3, s4;
  symtype sym_rhs;
  boolean found, star, multiple;
  boolean swapcols, gotvalue;
  integer place, m, d1, d2, d3, sign, i;
  c_number dims, partsum, marker;
  irreptype ira, irb, irc, min_rep, max_rep;
  irreptype lam1, lam2, lam3, muu1, muu2, muu3, nuu1, nuu2, nuu3;
  multtype mr1, mr2, mr3, mr4, ms1, ms2, ms3;
  triadtype tr0, tr1, tr2, tr3, tr4, tra, trb;
  irreptype s[6];
  multtype  mult[4];
  {
    gpdata_ *Wgp = &gpdata[gp];

    c_markheap(&marker);
    level = level + 1; 
    if (Wgp->howstored == product) {
	  per(i,1,6) {
	    s1.ir6[i - 1] = Wgp->irpart1[sixj->ir6[i - 1]];
	    s2.ir6[i - 1] = Wgp->irpart2[sixj->ir6[i - 1]];
	  }
	  per(i,1,4) {
	    s1.mult6[i - 1] = Wgp->part1mult[sixj->mult6[i - 1] - -1];
	    s2.mult6[i - 1] = Wgp->part2mult[sixj->mult6[i - 1] - -1];
	  }
      if (find6j(&s1, Wgp->group1) && find6j(&s2, Wgp->group2))
	sixj->value6 = c_mult(s1.value6, s2.value6);
      else
	sixj->value6 = c_zero;
      sixj->sym6 = (s1.sym6 + s2.sym6) % 2;
    } else {

      s0 = (*sixj);
      s0.value6 = c_unknown;
      min_rep = Wgp->last_rep;
      max_rep = 0;
	  per(i,1,6) {
	    if (min_rep > s0.ir6[i - 1])
	        min_rep = s0.ir6[i - 1];
	    if (max_rep < s0.ir6[i - 1])
	        max_rep = s0.ir6[i - 1];
	  }
      swapcols = false;
      star = false;
      s0.sym6 = 0;
   /* SYM6 is ZERO for SO3 D00 and CYCLIC GROUPS and ALLWAYS for PRIMITIVE 6J */
      found = false;
      if (((Wgp->table6 == primtable) && (Wgp->power[min_rep] == 1))
      ||  ((Wgp->table6 == fulltable) && (Wgp->power[min_rep] >= 1))) {
                      /* BINARY SEARCH */
	order (&s0, &star, &swapcols, s, mult, false, gp);
	search(s0, gp, &found, &place);
	if (!found) {
	  order (&s0, &star, &swapcols, s, mult, true, gp);
	  search(s0, gp, &found, &place);
	}
	if (found) {
	  s0.value6 = tablesixj[place].value6;
	  s0.sym6   = tablesixj[place].sym6;
	  if (debug) bufp += slprintf(sprintf(&buf[bufp], " 6J from TABLE"));
	} else {
	  bufp += slprintf(sprintf(&buf[bufp], " Cannot find 6J")); put6j((*sixj), gp); send_mess();
	  bufp += slprintf(sprintf(&buf[bufp], " looked for"));     put6j(  s0   , gp);
	  bufp += slprintf(sprintf(&buf[bufp], "  in SLOT %ld", gp)); send_warn();
	}
      }
      if (!found) { /* CALCULATE */
	putlowestirrepin3rdposition(&s0, &star, &swapcols, s, mult, gp);
	if (Wgp->power[min_rep] == 0) { /* TRIVIAL */
	  storetriad(&tr0, s0.ir6[1 - 1], Wgp->conj[s0.ir6[5 - 1]], 
	  	       s0.ir6[6 - 1], s0.mult6[1 - 1], 0);
	  if (findtriad(&tr0, gp)) {
	    if (s0.mult6[1 - 1] == s0.mult6[2 - 1])
	      s0.value6 = c_convert((biginteger)(phase(tr0.sym3)),
                       	      (biginteger)1, (biginteger)1,
                              (biginteger)(Wgp->dimen[s0.ir6[1 - 1]]
                              * Wgp->dimen[s0.ir6[4 - 1]]));
	    else {
	      s0.value6 = c_zero;
	      s0.sym6 = tr0.sym3;
	      tr0.mult3 = s0.mult6[2 - 1];
	      if (findtriad(&tr0, gp))
		s0.sym6 = (s0.sym6 + tr0.sym3) % 2;
	    }
          } else s0.value6 = c_zero;
	  if (debug) bufp += slprintf(sprintf(&buf[bufp], " 6J TRIVIAL"));
	}
	if (!c_known(s0.value6)) { /* PRIMITIVE */
	  if (Wgp->power[min_rep] == 1) {
	    switch (Wgp->howstored) { /* FORMULAS */
	      case as_tables:
		break;
	      case so3:
		ira = s0.ir6[6 - 1];
		irb = s0.ir6[5 - 1];
		irc = s0.ir6[1 - 1];
		sign = phase((ira + irb + irc) / 2);
		if (s0.ir6[4 - 1] == irb + 1)
		  s0.value6 = c_convert((biginteger)sign, (biginteger)(
                                  (ira + irb - 
                                  irc + 2) * (ira + irc
                                  - irb) / 4), (biginteger)1,
                                  (biginteger)((irb + 1) *
                                  (irb + 2) * irc
                                  * (irc + 1)));
		else
		  s0.value6 = c_convert((biginteger)sign, (biginteger)
                                  ((ira + irb
                                  + irc + 2) * (irb
                                  + irc - ira) / 4),
                                  (biginteger)1,
                                  (biginteger)(irb * (irb + 1)
                                  * irc * (irc + 1)));
		break;
	      case d00:
		if ((s0.ir6[6 - 1] == 1) &&
                    (s0.ir6[2 - 1] == s0.ir6[5 - 1]) &&
                    (s0.ir6[1 - 1] == s0.ir6[4 - 1]) &&
                    (s0.ir6[1 - 1] > 2))
		  s0.value6 = c_zero;
		else {
		  if (max_rep == 2)
		    m = 1;
		  else {
		    m = Wgp->power[max_rep];
			per(i,1,6) {
			  if (s0.ir6[i - 1] == 2)
			    if (s0.ir6[(i + 2) % 6 + 1 - 1] != max_rep)
			      m = m + 1;
			}
		  }
		  s0.value6 = c_convert((biginteger)(phase(m)),
                         (biginteger)1, (biginteger)2, (biginteger)1);
		}
		break;
	      case cyclic: s0.value6 = c_one; 	break;
	      default: 	Caseerror(Line);
	    }
	    if (debug) bufp += slprintf(sprintf(&buf[bufp], " 6J PRIM FORMULA"));
	  } else if (Wgp->power[min_rep] > 1) {
            /* RECURSE With BIEDENHARN-ELLIOTT SUM RULE */
	    if (debug) { bufp += slprintf(sprintf(&buf[bufp], " 6J RECURSING:")); put6j(s0, gp); send(); }
	    lam1 = s[1 - 1];
	    lam2 = s[2 - 1];
	    lam3 = s[3 - 1];
	    muu1 = s[4 - 1];
	    muu2 = s[5 - 1];
	    muu3 = s[6 - 1];
	    mr1 = mult[1 - 1];
	    mr2 = mult[2 - 1];
	    mr3 = mult[3 - 1];
	    mr4 = mult[4 - 1];
	    if (lam3 > Wgp->conj[lam3])
	      nuu1 = Wgp->conj[1];
	    else
	      nuu1 = 1;
	    ms2 = 0;  ms3 = 0;  nuu2 = lam3;
	    do {
	      nuu2 = nuu2 - 1;
	      storetriad(&tr0, Wgp->conj[nuu1], nuu2, lam3, ms3, 0);
	    } while (!findtriad(&tr0, gp));
	    storetriad(&tr0, lam1, lam2, lam3, 1, 0);
	    multiple = findtriad(&tr0, gp);
	    partsum = c_zero;
	    gotvalue = false;
	    nuu3 = 0;
	    do {
	      nuu3 = nuu3 + 1;
	      if (abs(Wgp->power[nuu3] - Wgp->power[lam2]) == 1) {
		storetriad(&trb, nuu1, lam2, Wgp->conj[nuu3], ms2, 0);
		if (findtriad(&trb, gp)) {
		  ms1 = -1;
		  do {
		    ms1 = ms1 + 1;
		    storetriad(&tra, lam1, Wgp->conj[nuu2], nuu3, ms1, 0);
		    if (findtriad(&tra, gp)) {
		      store6j(&s1, lam1, lam2, lam3, nuu1, nuu2, nuu3,
                              ms1, ms2, ms3, mr4, 0, c_unknown);
		      if (find6j(&s1, gp)) {
			if (debug) { bufp += slprintf(sprintf(&buf[bufp], "  RECURSING With")); put6j(s1, gp);
			  send();
			}
			if (multiple) {
			  (dims) = c_convert((biginteger)
                                          (Wgp->dimen[lam3]
                                          * Wgp->dimen[nuu3]),
                                          (biginteger)1, (biginteger)1,
                                          (biginteger)1);
			  partsum = c_add(partsum, c_mult((dims),
                                     c_mult(s1.value6,
                                            be_sum_rhs(&sym_rhs,
                        muu1, muu2, muu3, nuu1, nuu2, nuu3, lam1, lam2, lam3, 
                        mr1, mr2, mr3, ms1, ms2, ms3, gp ))));
			} else {
			  partsum = c_mult(be_sum_rhs(&sym_rhs,
                        muu1, muu2, muu3, nuu1, nuu2, nuu3, lam1, lam2, lam3, 
                        mr1, mr2, mr3, ms1, ms2, ms3, gp ),
                                           c_invert(c_conj(s1.value6)));
			  gotvalue = true;
			}
		      }
		    }
		  } while (!(gotvalue || (ms1 == Wgp->last_trmult)));
		}
	      }
	    } while (!(gotvalue || (nuu3 == Wgp->last_rep)));
	    storetriad(&tr0, lam1, Wgp->conj[muu2], muu3, mr1, 0);
	    storetriad(&tr1, muu1, lam2, Wgp->conj[muu3], mr2, 0);
	    storetriad(&tr2, Wgp->conj[muu1], muu2, lam3, mr3, 0);
	    if (findtriad(&tr0, gp) && findtriad(&tr1, gp) &&
                findtriad(&tr2, gp)) {
	      if ( (tr0.sym3 + tr1.sym3 + tr2.sym3 +
                       Wgp->two[lam1] + Wgp->two[nuu1]) & 1 ) 
		s0.value6 = c_minus(partsum);
	      else
		s0.value6 = partsum;
	      s0.sym6 = (sym_rhs + s1.sym6) % 2;
	    }
	    if (debug) bufp += slprintf(sprintf(&buf[bufp], " B-E SOLN of S0:"));
	  }
	}
      }
      if (debug) { bufp += slprintf(sprintf(&buf[bufp], "  ")); put6j(s0, gp); send();}
      if (star)  s0.value6 = c_conj(s0.value6);
      if (swapcols && (boolean) (s0.sym6 & 1) ) s0.value6 = c_minus(s0.value6);
      sixj->value6 = s0.value6;
      sixj->sym6 = s0.sym6;
    }
    c_cutheap(marker);
    sixj->value6 = c_copy(sixj->value6);
    level = level - 1;
  }
  return (sixj->value6 != c_zero);
}


multtype branchmult(ir, sub)
irreptype ir, sub;
{
  multtype result;
  integer i;
  boolean ok, goon;
  grouptype sg;

  result = -1;
  {
    gpdata_ *Wgp = &gpdata[group];

    goon = true;
    if (branchdata.bothreflected) {
      goon = (Wgp->irpart2[ir] == gpdata[subgroup].irpart2[sub]);
      ir = Wgp->irpart1[ir];
      sub = gpdata[subgroup].irpart1[sub];
    }
    if (goon)
      switch (branchdata.howembed) {
	case embedded: result = branchdata.branches[ir][sub]; break;
	case coupled:  bufp += slprintf(sprintf(&buf[bufp], " not done AS YET")); send();     break;
	case reflected:
	  if (((Wgp->irpart1[ir] == sub) && (Wgp->irpart2[ir] == 0)) ||
              ((Wgp->irpart1[ir] == tildeir[sub]) &&
               (Wgp->irpart2[ir] == 1)))
	    result = 0;
	  break;
	case so3so2:
	  i = ir - ((sub + 1) / 2);
	  if ((i >= 0) && (!(boolean) ((i) & 1)))
	    result = 0;
	  break;
	case so3d00:
	  ok = false;
	  if (sub == 0)           ok = (ir % 4 == 0);
	  else if (sub == 1)      ok = ((ir) & 1);
	  else if (sub == 2)      ok = (ir % 4 == 2);
	  else if (sub <= ir + 1) ok = ((ir - sub) & 1);
	  if (ok) result = 0;
	  break;
	case d00so2:
	  if (ir > 2) {
	    i = ir - 1;
	    if ((sub == 2 * i) || (sub == 2 * i - 1))
	      result = 0;
	  } else if (((ir == 2) || (ir == 0)) && (sub == 0))
	    result = 0;
	  else if (ir == 1)
	    if ((sub == 1) || (sub == 2))
	      result = 0;
	  break;
	case cyclic_cyclic:
          sg = branchdata.bothreflected ? gpdata[subgroup].group1  : subgroup;
	  i = 2 * gpdata[sg].last_rep + 2;
	  if (2 * sub + 1 == abs((2 * ir + 1 + i) % (2 * i) - i))
	    result = 0;
	  break;
	default:
	  Caseerror(Line);
      }
  }
  return result;
}

symtype find2jm(ir, sub)
irreptype ir, sub;
{
  symtype result;
  integer i, m;

  result = 0;
  {
    if (branchdata.bothreflected) {
      ir  = gpdata[group   ].irpart1[ir];
      sub = gpdata[subgroup].irpart1[sub];
    }
    switch (branchdata.howembed) {
      case embedded:  result = branchdata.twojms[ir][sub]; break;
      case so3so2:
	if ((boolean) ((sub) & 1))
	  i = (sub + 1) / 2;
	else
	  i = -(sub / 2);
	result = ((ir - i) / 2) % 2;
	break;
      case d00so2:
	if ((ir == 2) || (sub % 4 == 2))
	  result = 1;
	break;
      case cyclic_cyclic:
      case reflected:
      case so3d00: break;
      default:     Caseerror(Line);
    }
  }
  return result;
}

void find3jm();


c_number wigner_rhs(
            lam1, lam2, lam3, sig1, sig2, sig3, nuu1, nuu2, nuu3,
            ms1, ms2, ms3, a1, a2, a3, mt,
            tr1, tr2, tr3, agroup, subagroup)
  irreptype lam1, lam2, lam3, sig1, sig2, sig3, nuu1, nuu2, nuu3;
  multtype  ms1, ms2, ms3, a1, a2, a3, mt;
  triadtype tr1, tr2, tr3;
  grouptype agroup, subagroup;
{
  irreptype cnuu1, cnuu2, cnuu3, rho1, rho2, rho3;
  multtype  b1, mt2, mt3, mt1, b2, b3;
  c_number  wigner_term, wigner_sum;
  sixjtype  s2;
  threejmtype t1, t2, t3;

  {
    gpdata_ *Wgp = &gpdata[agroup];
    cnuu1 = Wgp->conj[nuu1];
    cnuu2 = Wgp->conj[nuu2];
    cnuu3 = Wgp->conj[nuu3];
  }
  {
    gpdata_ *Wsg = &gpdata[subagroup];

    wigner_sum = c_zero;   mt2 = 0; mt3 = 0;     b1 = 0;
    for (rho3 = 0; rho3 <= Wsg->last_rep; rho3++) {
      if (abs(Wsg->power[rho3] - Wsg->power[sig2]) == 1) {
        for (b3 = 0; b3 <= branchmult(nuu3, rho3); b3++) {
          for (rho2 = 0; rho2 <= Wsg->last_rep; rho2++) {
	    if (abs(Wsg->power[rho2] - Wsg->power[sig3]) == 1) {
              for (b2 = 0; b2 <= branchmult(nuu2, rho2); b2++) {
		for (mt1 = 0; mt1 <= Wsg->last_trmult; mt1++) {
		  storetriad(&tr1, sig1, Wsg->conj[rho2], rho3, mt1, 0);
		  if (findtriad(&tr1, subagroup)) {
		    store3jm(&t1, lam1, cnuu2, nuu3, 
			     sig1, Wsg->conj[rho2], rho3,
                             ms1, mt1, a1, b2, b3, 0, false,
                             c_unknown);
		    for (rho1 = 0; rho1 <= Wsg->last_rep; rho1++) {
		      if (branchmult(nuu1, rho1) >= 0) {
			storetriad(&tr2, rho1, sig2, Wsg->conj[rho3], mt2, 0);
		        if (findtriad(&tr2, subagroup)) {
			  storetriad(&tr3, Wsg->conj[rho1], rho2, sig3, mt3, 0);
		          if (findtriad(&tr3, subagroup)) {
			    store6j(&s2, sig1, sig2, sig3,
                                    rho1, rho2, rho3, mt1, mt2, mt3,
                                    mt, 0, c_unknown);
			    if (find6j(&s2, subagroup)) {
			      store3jm(&t2, nuu1, lam2, cnuu3,
                                       rho1, sig2, Wsg->conj[rho3],
                                       ms2, mt2, b1, a2, b3, 0,
                                       false, c_unknown);
			      find3jm(&t2, agroup, subagroup);
			      if (t2.value3 != c_zero) {
				store3jm(&t3, cnuu1, nuu2, lam3, 
                                        Wsg->conj[rho1], rho2, sig3, 
                                        ms3, mt3, b1, b2, a3, 0, false,
                                         c_unknown);
				find3jm(&t3, agroup, subagroup);
			        if (t3.value3 != c_zero) {
				  if (t1.value3 == c_unknown)
				    find3jm(&t1, agroup, subagroup);
				  wigner_term = c_mult(t1.value3, c_mult(
                                                 t2.value3, c_mult(t3.value3,
                                                 s2.value6)));
				  if ((boolean) ((find2jm(nuu1, rho1)
                                                + find2jm(nuu2, rho2) 
                                                + find2jm(nuu3, rho3)) & 1))
				    wigner_term = c_minus(wigner_term);
				  wigner_sum = c_add(wigner_term, wigner_sum);
				}
			      }
			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  if (debug) { bufp += slprintf(sprintf(&buf[bufp], "  WIGNER_SUM")); put_c_number(wigner_sum); send(); }
  return wigner_sum;
}

void find3jm(three, agroup, subagroup)
threejmtype *three;
grouptype agroup, subagroup;
/* TAKES A 3JM SYMBOL REARRANGES IT INTO ITS STANDARD FORM and
   EITHER LOOKS UP TABLES or PERFORMS A RECURSIVE CALCULATION
   TO DETERMINE the VALUE of the 3JM SYMBOL. */
{ permtype perm1[4], perm2[4];
  coltype col1[3], col2[3];
  integer s[12], t[12];
  integer e, m, p2, j2, m0, up, low, diff, place, place1, i;
  boolean found, star, gotvalue, multiple, found6j;
  c_number dims, partsum, marker;
  multtype mr4;
  sixjtype s1;
  threejmtype t0, t00;
  triadtype tr0, tra, trb;
  grouptype gp, sg;
  irreptype lam1, lam2, lam3, sig1, sig2, sig3, nuu1, nuu2, nuu3;
  multtype  ms1, ms2, ms3, a1, a2, a3, mt;
  triadtype tr1, tr2, tr3;
  {
    gpdata_ *Wgp = &gpdata[agroup];

    c_markheap(&marker);
    level = level + 1;
    t0 = (*three);
    t0.sym3jm = 0;
    t0.star3 = false;
    gotvalue = false;
    place = 0;
    star = false;
    /* PUT the 3JM INTO STANDARD FORM */
    found = true;
    {
	for (i = 1;i<=3; i++) {
	  col1[i - 1] = max_mult * (max_reps * t0.gpir[i - 1]
                          + t0.subir[i - 1]) + t0.brmult[i - 1];
	  col2[i - 1] = max_mult * (max_reps * Wgp->conj[t0.gpir[i - 1]]
                           + gpdata[subagroup].conj[t0.subir[i - 1]])
                           + t0.brmult[i - 1];
	}
    }
    findperm(col1, perm1);
    findperm(col2, perm2);
    {
	for (i = 1;i<=3; i++) {
	  t[i] = t0.gpir[perm1[i] - 1];
	  s[i] = Wgp->conj[t0.gpir[perm2[i] - 1]];
	  t[i + 4] = t0.subir[perm1[i] - 1];
	  s[i + 4] = gpdata[subagroup].conj[t0.subir[perm2[i] - 1]];
	  t[i + 8] = t0.brmult[perm1[i] - 1];
	  s[i + 8] = t0.brmult[perm2[i] - 1];
	}
    }
    t[4] = t0.gpmult;
    s[4] = t[4];
    t[8] = t0.submult;
    s[8] = t[8];
    i = 1;
    do { diff = t[i] - s[i]; i = i + 1;
    } while (!((diff != 0) || (i > 7)));
    star = (boolean) (diff > 0);
    if (star) {
      for (i=1; i<=12; i++) t[i] = s[i];
      arraycpy( perm1, perm2, 4);
    }
    store3jm(&t0, t[1], t[2], t[3], t[5], t[6], t[7], t[4], t[8], t[9], t[10],
             t[11], 0, false, c_unknown);
    /* CALCULATE VALUE of 3JM SYMBOL */
    if (Wbr->bothreflected) {
      grouptype thegroup=group, thesubgroup= subgroup;
      t00 = t0;
      per(i,1,3) {
	t00.gpir [i - 1] = gpdata[agroup   ].irpart1[t00.gpir [i - 1]];
	t00.subir[i - 1] = gpdata[subagroup].irpart1[t00.subir[i - 1]];
      }
      Wbr->bothreflected = false;
      group = gpdata[agroup].group1;
      subgroup = gpdata[subagroup].group1;
      find3jm(&t00, gpdata[agroup].group1, gpdata[subagroup].group1);
      Wbr->bothreflected = true;
      group = thegroup;
      subgroup = thesubgroup;
      t0.value3 = t00.value3;
      if (debug)     bufp += slprintf(sprintf(&buf[bufp], " BOTHREFLECTED3JM"));
    } else if (Wbr->howembed == reflected) {
      if (Wgp->irpart2[t[1]] == 0) {
	t0.value3 = c_convert((biginteger)(delta(t[4], t[8])),
                                 (biginteger)1, (biginteger)1, (biginteger)1);
      } else {
	gpdata_ *Wsg = &gpdata[subagroup];

	store6j(&s1, t[5], t[6], t[7],
                Wsg->conj[tildeir[t[6]]], tildeir[t[5]], Wbr->tilderep, 
		0, 0, t[4], t[8], 0, c_unknown);
	storetriad(&tr1, tildeir[t[5]], tildeir[t[6]], t[7], t[4], 0);
	storetriad(&tr2, t[6], Wsg->conj[tildeir[t[6]]], Wbr->tilderep, 0, 0);
	if (find6j(&s1, subagroup) && findtriad(&tr1, subagroup)
                                   && findtriad(&tr2, subagroup))
	  t0.value3 = c_mult(c_convert((biginteger)(phase(tr1.sym3
                          + tr2.sym3 + Wsg->two[t[6]])),
                          (biginteger)(Wsg->dimen[t[5]]
                          * Wsg->dimen[t[6]]), (biginteger)1,
                          (biginteger)1), s1.value6);
	if (t0.value3 == c_unknown) {
	  bufp += slprintf(sprintf(&buf[bufp], " Reflected not found"));
	  send_warn();
	}
      }
      if (debug) bufp += slprintf(sprintf(&buf[bufp], " REFLECTED 3JM"));
    } else if (Wgp->power[t[3]] == 0) {
      /* TRIVIAL 3JM = NORMALISED 2JM */
      t0.value3 = c_convert((biginteger)(delta(t[9], t[10])
                      * phase(find2jm(t[1], t[5]))),(biginteger)
                      (gpdata[subagroup].dimen[t[5]]),
                      (biginteger)1, (biginteger)(Wgp->dimen[t[1]]));
      if (debug) bufp += slprintf(sprintf(&buf[bufp], " 3JM TRIVIAL"));
    } else 
      /*if (((Wbr->table3 == primtable)) || (Wbr->table3 == fulltable)) */
      /* PERFORM A BINARY SEARCH */
      {
      up = Wbr->nthree; low = 1; diff = 1; place = up + 1;
      while ( (diff != 0) && (up >= low) ) {
	place = (up + low) / 2;
	i = 0;
	{
	  threejmtype *Wtab3jm = &Wbr->table3jm[place];

	  do {
	    i = i + 1;
	    if (i < 4)
	      diff = t[i] - Wtab3jm->gpir[i - 1];
	    else if (i == 4)
	      diff = t[i] - Wtab3jm->gpmult;
	    else if (i < 8)
	      diff = t[i] - Wtab3jm->subir[i - 4 - 1];
	    else if (i == 8)
	      diff = t[i] - Wtab3jm->submult;
	    else
	      diff = t[i] - Wtab3jm->brmult[i - 8 - 1];
	  } while (!((diff != 0) || (i == 11)));
	}
	if (diff < 0)
	  up = place - 1;
	else
	  low = place + 1;
      };
      found = (diff==0);
      if (found) {
	t0.value3 = Wbr->table3jm[place].value3;
	t0.star3  = Wbr->table3jm[place].star3;
	t0.sym3jm = Wbr->table3jm[place].sym3jm;
	if (debug) bufp += slprintf(sprintf(&buf[bufp], "  3JM from TABLE "));
      } else if (debug) { bufp += slprintf(sprintf(&buf[bufp], " Cannot find 3JM")); send_mess(); }
    }
    if (!c_known(t0.value3)) {
      if (Wgp->power[t[3]] == 1) {
	switch (Wbr->howembed) { /* WE SHOULD HAVE FORMULAS */
	  case embedded:
	    bufp += slprintf(sprintf(&buf[bufp], " PRIMTABLES ARE INCOMPLETE")); send_warn(); break;
	  case coupled:
	    bufp += slprintf(sprintf(&buf[bufp], " COUPLED NOT DONE AS YET")); send_warn(); break;
	  case reflected:
	    bufp += slprintf(sprintf(&buf[bufp], " REFLECTED FORMULA FAILED")); send_warn(); break;
	  case so3so2:
	    if ((boolean) ((t[5]) & 1))
	      m0 = (t[5] + 1) / 2;
	    else
	      m0 = -(t[5] / 2);
	    t0.value3 = c_convert((biginteger)(phase((t[1] - m0 - 2)
                            / 2)),(biginteger)(t[1] - m0 *
                            (3 - 2 * t[7])), (biginteger)1,
                            (biginteger)(2 * t[1] * (t[1] + 1)));
	    break;
	  case so3d00:
	    {
	      gpdata_ *Wsg = &gpdata[subagroup];

	      if (t[6] > 2) {
		e = Wsg->power[t[5]] - Wsg->power[t[6]];
		p2 = Wsg->power[t[6]];
		j2 = t[2];
		t0.value3 = c_convert((biginteger)1, (biginteger)
                                         (j2 + e * p2 + 2), (biginteger)1,
                                         (biginteger)((j2 + 1) * (j2 + 2)));
	      }
	      if (t[6] == 1) {
		j2 = t[2];
		if (t[5] == 3)
		  t0.value3 = c_convert((biginteger)1, (biginteger)(j2 + 3),
                                           (biginteger)1,
                                           (biginteger)((j2 + 1) * (j2 + 2)));
		else
		  t0.value3 = c_convert((biginteger)1, (biginteger)1,
                                           (biginteger)1,
                                           (biginteger)(j2 + 2));
	      }
	      if ((t[6] == 2) || (t[6] == 0)) {
		j2 = t[2];
		t0.value3 = c_convert((biginteger)(phase(j2 / 2)),
                                         (biginteger)1, (biginteger)1,
                                         (biginteger)(j2 + 1));
	      }
	    }
	    break;
	  case d00so2:
	    t0.value3 = c_convert((biginteger)(-phase(delta(t[1], 2)))
                                     , (biginteger)1, (biginteger)1,
                                     (biginteger)2);
	    break;
	  case cyclic_cyclic:
	    t0.value3 = c_one;
	    break;
	  default:
	    Caseerror(Line);
	}
	if (debug) bufp += slprintf(sprintf(&buf[bufp], " 3JM PRIM FORMULA "));
      } else if (Wgp->power[t[3]] > 1) { /* RECURSE With WIGNER EQN */
        /* FIND A 6J to GENERATE the WIGNER EQUATION */
	if (debug) { bufp += slprintf(sprintf(&buf[bufp], " 3JM RECURSING:")); put3jm(t0); send(); }
	lam1 = t[1]; lam2 = t[2];  lam3 = t[3]; mr4 = t[4];
	a1   = t[9]; a2   = t[10]; a3   = t[11];
	sig1 = t[5]; sig2 = t[6];  sig3 = t[7];
	mt = t[8];
	if (lam3 > Wgp->conj[lam3]) nuu1 = Wgp->conj[1];
	else nuu1 = 1;
	ms2 = 0; ms3 = 0; nuu2 = lam3;
	do {
	  nuu2 = nuu2 - 1;
	  storetriad(&tr0, Wgp->conj[nuu1], nuu2, lam3, ms3, 0);
	} while (!(findtriad(&tr0, agroup)));
	storetriad(&tr0, lam1, lam2, lam3, 1, 0);
	multiple = findtriad(&tr0, agroup);
	partsum = c_zero;
	gotvalue = false;
	nuu3 = 0;
	do {
	  nuu3 = nuu3 + 1;
	  if (abs(Wgp->power[nuu3] - Wgp->power[lam2]) == 1) {
	    storetriad(&trb, nuu1, lam2, Wgp->conj[nuu3], ms2, 0);
	    if (findtriad(&trb, agroup)) {
	      ms1 = -1;
	      do {
		ms1 = ms1 + 1;
		storetriad(&tra, lam1, Wgp->conj[nuu2], nuu3, ms1, 0);
		if (findtriad(&tra, agroup)) {
		  store6j(&s1, lam1, lam2, lam3, nuu1, nuu2, nuu3,
                          ms1, ms2, ms3, mr4, 0,  c_unknown);
		  if (find6j(&s1, agroup)) {
		    if (debug) { bufp += slprintf(sprintf(&buf[bufp], "  RECURSING With")); put6j(s1, agroup);
		      send();
		    }
		    switch (multiple) {
		      case true:
			dims = c_convert(
			      (biginteger)(Wgp->dimen[lam3]* Wgp->dimen[nuu3]), 
			      (biginteger)1, (biginteger)1, (biginteger)1);
			partsum = c_add(partsum, c_mult(dims, c_mult(
                             c_conj(s1.value6), wigner_rhs(
                              lam1, lam2, lam3, sig1, sig2, sig3, 
                              nuu1, nuu2, nuu3,
                              ms1, ms2, ms3, a1, a2, a3, mt,
                              tr1, tr2, tr3, agroup,subagroup))));
			break;
		      case false:
			partsum = c_mult(wigner_rhs(      
                              lam1, lam2, lam3, sig1, sig2, sig3, 
                              nuu1, nuu2, nuu3,
                              ms1, ms2, ms3, a1, a2, a3, mt,
                              tr1, tr2, tr3, agroup,subagroup), 
                                         c_invert(s1.value6));
			gotvalue = true;
			break;
		      default:
			Caseerror(Line);
		    }
		  }
		}
	      } while (!(gotvalue || (ms1 == Wgp->last_trmult)));
	    }
	  }
	} while (!(gotvalue || (nuu3 == Wgp->last_rep)));
	t0.value3 = partsum;
	if (debug) bufp += slprintf(sprintf(&buf[bufp], " WIGNER SOLN T0:"));
      }
    }
    storetriad(&tr0, t[1], t[2], t[3], t[4], 0);
    storetriad(&tr1, t[5], t[6], t[7], t[8], 0);
    if (findtriad(&tr0, agroup) && findtriad(&tr1, subagroup))
      t0.sym3jm = (tr0.sym3 + tr1.sym3) % 2;
    t0.star3 = (boolean) ((find2jm(t[1], t[5]) + find2jm(t[2], t[6])
                         + find2jm(t[3], t[7])) & 1);
    if (!found) {
      /* FOUND CAN ONLY BE FALSE when A TABLE has been searched.
         INSERT CALCULATED 3JM INTO TABLE */
       diff = 1;
       while ( (diff >= 0) && (place <= Wbr->nthree) ) {
	i = 0;
	{
	  threejmtype *Wtab3jm = &Wbr->table3jm[place];

	  do {
	    i = i + 1;
	    if (i < 4)
	      diff = t[i] - Wtab3jm->gpir[i - 1];
	    else if (i == 4)
	      diff = t[i] - Wtab3jm->gpmult;
	    else if (i < 8)
	      diff = t[i] - Wtab3jm->subir[i - 4 - 1];
	    else if (i == 8)
	      diff = t[i] - Wtab3jm->submult;
	    else
	      diff = t[i] - Wtab3jm->brmult[i - 8 - 1];
	  }while (!((diff != 0) || (i == 11)));
	}
	if (diff > 0)
	  place = place + 1;
      };
/*
      memmove( &Wbr->table3jm[place+1], &Wbr->table3jm[place],
        (Wbr->nthree-place+1) * sizeof(threejmtype));
*/
      for (place1 = Wbr->nthree; place1 >= place; place1--)
	Wbr->table3jm[place1 + 1] = Wbr->table3jm[place1];

      t0.value3 = c_move(t0.value3, safe_heap);
      Wbr->table3jm[place] = t0;
      Wbr->nthree = Wbr->nthree + 1;
      if (Wbr->nthree == Wbr->max3jms) {
        threejmtype *ptr;

	bufp += slprintf(sprintf(&buf[bufp], " Increasing 3jm table size"));
	send_mess();
        Wbr->max3jms += increment_3jm;
        ptr = (threejmtype *) MALLOC( (Wbr->max3jms+1) * sizeof(threejmtype));
        memcpy(ptr, Wbr->table3jm,( Wbr->nthree+1) * sizeof(threejmtype));
        FREE(Wbr->table3jm);
        Wbr->table3jm = ptr;
      }
      if ((Wbr->nthree % 200 == 0)) {
	bufp += slprintf(sprintf(&buf[bufp], " NTHREE ="));
	put_integer(Wbr->nthree);
	send_mess();
      }
    }
    if (debug) { put3jm(t0); send(); }
    if (star) {
      t0.value3 = c_conj(t0.value3);
      if (t0.star3) t0.value3 = c_minus(t0.value3);
    }
    if ((perm1[0] == 1) && (t0.sym3jm == 1)) t0.value3 = c_minus(t0.value3);
    three->value3 = t0.value3;
    three->star3 = t0.star3;
    three->sym3jm = t0.sym3jm;
    c_cutheap(marker);
    level = level - 1;
    three->value3 = c_copy(three->value3);
  }
}


void reset_racah()
{
  grouptype group;

  per(group,0,maxgroups) gpdata[group].timesused = 0;
  last6j = 0;
}

void put_group_header(gp)
grouptype gp;
{
  gpdata_ *Wgp = &gpdata[gp];

    if(Wgp->howstored == product) {
      put_group_header(Wgp->group1);
      put_group_header(Wgp->group2);
    }
    bufp += slprintf(sprintf(&buf[bufp], " >>> GROUP:     %s ", Wgp->ext_name));
    if(!streq(Wgp->int_name, Wgp->ext_name)) bufp += slprintf(sprintf(&buf[bufp], " (%s) ", Wgp->int_name));
    if (Wgp->timesused > 1) bufp += slprintf(sprintf(&buf[bufp], " REUSING "));
    bufp += slprintf(sprintf(&buf[bufp], " %s %ld, %ld %s", "SLOT",gp, Wgp->last_rep+1,"irreps. "));
    switch (Wgp->howstored) {
      case as_tables:
	bufp += slprintf(sprintf(&buf[bufp], " from FILE "));
	put_name(Wgp->title);    
	put_name(Wgp->filetime); 
	put_name(Wgp->filedate);
	switch (Wgp->table6) {
	  case notable  : bufp += slprintf(sprintf(&buf[bufp], " NO 6J TABLE "));     break;
	  case primtable: bufp += slprintf(sprintf(&buf[bufp], " PRIMITIVE TABLE ")); break;
	  case fulltable: bufp += slprintf(sprintf(&buf[bufp], " FULL 6J TABLE "));   break;
	  default: Caseerror(Line);
	}
	if (Wgp->table6 != notable) {
	  put_integer(Wgp->nsix);
	  bufp += slprintf(sprintf(&buf[bufp], "  6J SYMBOLS "));
	}
	switch (Wgp->calc) {
	  case nothing:
	  case origdata: break;
	  case somevalues: bufp += slprintf(sprintf(&buf[bufp], " SOME VALUES ")); break;
	  case allvalues:  bufp += slprintf(sprintf(&buf[bufp], " ALL VALUES "));  break;
	  default: Caseerror(Line);
	}
	break;
      case product:
	bufp += slprintf(sprintf(&buf[bufp], "  of %ld*%ld ", Wgp->group1, Wgp->group2));
	break;
      case so3:
      case d00:
      case cyclic: bufp += slprintf(sprintf(&buf[bufp], "  USING FORMULAS ")); break;
      default:     Caseerror(Line);
    }
    send();
}

void put_branch_header()
{
  {
    bufp += slprintf(sprintf(&buf[bufp], " >>> BRANCHING: %s ", Wbr->int_name));
    if ( !streq(Wbr->title, blank) ) {
      bufp += slprintf(sprintf(&buf[bufp], " READ from FILE "));
      put_name(Wbr->title);   
      put_name(Wbr->filetime);
      put_name(Wbr->filedate);
      switch (Wbr->table3) {
	case notable:   bufp += slprintf(sprintf(&buf[bufp], " NO 3JM TABLE "));    break;
	case primtable: bufp += slprintf(sprintf(&buf[bufp], " PRIMITIVE TABLE ")); break;
	case fulltable: bufp += slprintf(sprintf(&buf[bufp], " FULL 3JM TABLE "));  break;
	default:  Caseerror(Line);
      }
      if (Wbr->table3 != notable) {
	put_integer(Wbr->nthree);
	bufp += slprintf(sprintf(&buf[bufp], "  3JM SYMBOLS "));
      }
      switch (Wbr->calc3) {
	case nothing:
	case origdata:   break;
	case somevalues: bufp += slprintf(sprintf(&buf[bufp], " SOME VALUES ")); break;
	case allvalues:  bufp += slprintf(sprintf(&buf[bufp], " ALL VALUES ")); break;
	default:    Caseerror(Line);
      }
    }
    switch (Wbr->howembed) {
      case embedded:  break;
      case coupled:   bufp += slprintf(sprintf(&buf[bufp], " COUPLED ")); break;
      case reflected: bufp += slprintf(sprintf(&buf[bufp], " REFLECTED ")); break;
      case so3so2:
      case so3d00:
      case d00so2:
      case cyclic_cyclic: bufp += slprintf(sprintf(&buf[bufp], " USING FORMULAS ")); break;
      default: Caseerror(Line);
    }
    if (Wbr->bothreflected) bufp += slprintf(sprintf(&buf[bufp], " BOTH REFLECTED"));
  }
}

void cancelgp(gp)
grouptype gp;
{
  if (gp > 0) {
    gpdata_ *Wgp = &gpdata[gp];

    if (Wgp->timesused > 0) {
      Wgp->timesused = Wgp->timesused - 1;
      if (Wgp->howstored == product) {
	cancelgp(Wgp->group1);
	cancelgp(Wgp->group2);
      }
    }
  }
}

grouptype find_gp_slot(new_name)
char* new_name;
{
  boolean found;
  grouptype gp;
  gpdata_ *Wgp;

  per(gp,0,maxgroups) {
    Wgp = &gpdata[gp];
    found = streq(Wgp->int_name, new_name) && (Wgp->timesused > 0);
    if (found) break;
  }
  if (!found) 
    per(gp,0,maxgroups) {
      found = (gpdata[gp].timesused == 0);
      if (found) break;
    }
  if (!found) {
    bufp += slprintf(sprintf(&buf[bufp], " TOO MANY GROUPS  FATAL OVERFLOW"));
    send_warn();
  } 
  Wgp = &gpdata[gp];
  Wgp->timesused++;
  if (Wgp->timesused == 1) {
      wordcpy(Wgp->int_name, new_name);
      wordcpy(Wgp->filetime, blank);
      wordcpy(Wgp->filedate, blank);
      wordcpy(Wgp->tag, blank);
      wordcpy(Wgp->title, blank);
      Wgp->howstored = as_tables;
      Wgp->continuous = false;
      Wgp->group1 = 0;
      Wgp->group2 = 0;
      Wgp->calc = nothing;
      Wgp->last_rep = -1;
      Wgp->last_trmult = 0;
      Wgp->labelwidth = 0;
      Wgp->ntriad = 0;
      Wgp->nsix = 0;
      Wgp->table6 = notable;
      Wgp->zero6j = last6j;
  }
  return gp;
}

irreptype findlabel(lable, gp, store)
word_type lable;
grouptype gp;
boolean store;
{
  integer ir;

  {
    gpdata_ *Wgp = &gpdata[gp];

    ir = -1;
    do {
      ir++;
      if (ir > Wgp->last_rep) wordcpy( Wgp->labels[ir], lable);
    } while ( !streq(Wgp->labels[ir], lable) );
    if (ir > Wgp->last_rep) {
      if ((Wgp->howstored != product) || (!store)) {
	bufp += slprintf(sprintf(&buf[bufp], " UNKNOWN LABEL %s for group %s", lable, Wgp->ext_name));
	put_input(); send_warn();
      } else {
	Wgp->last_rep = ir;
        Wgp->labelwidth = max( strlen(lable), Wgp->labelwidth );
      }
    }
  }
  return ir;
}

void makepowers(gp)
grouptype gp;
{
  boolean star, found;
  integer place;
  triadtype s, t;
  irreptype i2, ir, local_power[max_reps + 1];

  {
    gpdata_ *Wgp = &gpdata[gp];

    per(ir,0,Wgp->last_rep) Wgp->power[ir] = 0;
    per(ir,0,Wgp->last_rep){
	  if (ir < 2)
	    local_power[ir] = ir;
	  else if (Wgp->conj[ir] == ir - 1)
	    local_power[ir] = local_power[ir - 1];
	  else {
	    storetriad(&s, ir, 0, 1, 0, 0);
	    storetriad(&t, ir, 0, Wgp->conj[1], 0, 0);
	    i2 = 0;
	    do {
	      s.ir3[2 - 1] = i2;
	      t.ir3[2 - 1] = i2;
	      i2 = i2 + 1;
	      found = findtriad(&s, gp);
	      if (!found)
		found = findtriad(&t, gp);
	    } while (!(found || (i2 == ir)));
	    local_power[ir] = local_power[i2 - 1] + 1;
	    if (!found)
	      if (!(Wgp->continuous && (i2 == Wgp->last_rep))) {
		bufp += slprintf(sprintf(&buf[bufp], " MAKEPOWERS CANNOT FIND A TRIAD for IRREP"));
		put_name(Wgp->labels[ir]);
		bufp += slprintf(sprintf(&buf[bufp], " in GROUP"));
		put_name(Wgp->ext_name);
		send_warn();
	      }
          }
    }
    per(ir,0,Wgp->last_rep) Wgp->power[ir] = local_power[ir];
  }
}

void make_product_ir(gp)
grouptype gp;
{
  irreptype ir, ir1, ir2, irc;
  integer i, j, k;
  word_type label1, label2;

  {
    gpdata_ *Wgp = &gpdata[gp];

    if (Wgp->howstored == product) {
      per(ir,0,Wgp->last_rep){
	    wordcpy(label1, blank);
	    wordcpy(label2, blank);
	    label1[1 - 1] = Wgp->labels[ir][1 - 1];
	    i = 2;
	    while ((Wgp->labels[ir][i - 1] != '.')
                   && (Wgp->labels[ir][i - 1] != '+')
                   && (Wgp->labels[ir][i - 1] != '-')
                   && (i < length_word)) {
	      label1[i - 1] = Wgp->labels[ir][i - 1];
	      i = i + 1;
	    }
	    if ((Wgp->labels[ir][i - 1] == '.'))
	      i = i + 1;
            strcpy(label2, (char*)&Wgp->labels[ir][i - 1]);
	    Wgp->irpart1[ir] = findlabel(label1, Wgp->group1, false);
	    Wgp->irpart2[ir] = findlabel(label2, Wgp->group2, false);
      }
      make_product_ir(Wgp->group2);
      per(ir,0,Wgp->last_rep){
	    ir1 = Wgp->irpart1[ir];
	    ir2 = Wgp->irpart2[ir];
	    Wgp->dimen[ir] = gpdata[Wgp->group1].dimen[ir1]
                           * gpdata[Wgp->group2].dimen[ir2];
	    Wgp->two[ir]  = (gpdata[Wgp->group1].two[ir1]
                           + gpdata[Wgp->group2].two[ir2]) % 2;
	    ir1 = gpdata[Wgp->group1].conj[ir1];
	    ir2 = gpdata[Wgp->group2].conj[ir2];
	    irc = 0;
	    while (((ir1 != Wgp->irpart1[irc])
                 || (ir2 != Wgp->irpart2[irc])) && (irc < Wgp->last_rep))
	      irc = irc + 1;
	    Wgp->conj[ir] = irc;
	    if ((ir1 != Wgp->irpart1[irc]) || (ir2 != Wgp->irpart2[irc])) {
	      bufp += slprintf(sprintf(&buf[bufp], " NO CONJ IRREP"));
	      put_name(Wgp->labels[ir]);
	      bufp += slprintf(sprintf(&buf[bufp], " of GROUP"));
	      put_name(Wgp->ext_name);
	      send_warn();
	    }
      }
      Wgp->last_trmult = 0;
      per(i,0,gpdata[Wgp->group1].last_trmult)
      per(j,0,gpdata[Wgp->group2].last_trmult){
        k = i * (gpdata[Wgp->group2].last_trmult + 1) + j;
	Wgp->part1mult[k - -1] = i;
	Wgp->part2mult[k - -1] = j;
	Wgp->last_trmult = max(Wgp->last_trmult, k);
      }
    }
  }
}

c_number get_c_number();

void read_gp_data(gp)
grouptype gp;
{
  integer i, j, minrep;
  boolean gotone, gotall;
  c_number marker;

  open_in_file(gpdata[gp].title);
  {
    type_in_record   *Win = &in_record[in_file];
    gpdata_ *Wgp = &gpdata[gp];

    do {
      get_item(ask_string);
      if (in_eq("GROUPDATA")) {
	wordcpy( Wgp->int_name, (get_item(ask_string),Win->in_word));
      } else if (in_eq("CREATED")) {
	get_item(ask_string); wordcpy( Wgp->filetime, Win->in_word);
	get_item(ask_string); wordcpy( Wgp->filedate, Win->in_word);
      } else if (in_eq("CONTINUOUS"))
	Wgp->continuous = true;
      else if (in_eq("IRREPS")) {
	get_item(ask_string);
	do {
	  Wgp->last_rep = Wgp->last_rep + 1;
	  wordcpy( Wgp->labels[Wgp->last_rep], Win->in_word);
	  get_item(ask_free);
	  if (Win->in_item == delimiter) {
	    get_item(ask_string);
	    wordcpy( Wgp->labels[Wgp->last_rep + 1],  Win->in_word);
	    i = get_integer();
	    if (i < 0)
	      Wgp->two[Wgp->last_rep] = 1;
	    else
	      Wgp->two[Wgp->last_rep] = 0;
	    Wgp->dimen[Wgp->last_rep] = abs(i);
	    Wgp->last_rep = Wgp->last_rep + 1;
	    Wgp->conj[Wgp->last_rep] = Wgp->last_rep - 1;
	    Wgp->conj[Wgp->last_rep - 1] = Wgp->last_rep;
	  } else {
	    Wgp->conj[Wgp->last_rep] = Wgp->last_rep;
	    i = Win->in_number;
	  }
	  if (i < 0)
	    Wgp->two[Wgp->last_rep] = 1;
	  else
	    Wgp->two[Wgp->last_rep] = 0;
	  Wgp->dimen[Wgp->last_rep] = abs(i);
	  get_item(ask_string);
	} while (!( in_eq("TRIADS") || (Win->in_item == end_of_file)) );
	if (Wgp->continuous) {
	  Wgp->last_rep = Wgp->last_rep + 1;
	  Wgp->dimen[Wgp->last_rep] = 1;
	  Wgp->two[Wgp->last_rep] = 0;
	  Wgp->conj[Wgp->last_rep] = Wgp->last_rep;
	  strcpy(Wgp->labels[Wgp->last_rep],"IR?" );
	}
      }
    } while (!(in_eq("TRIADS") || (Win->in_item == end_of_file)));
    per(i,0,Wgp->last_rep)
        Wgp->labelwidth = max( strlen(Wgp->labels[i]), Wgp->labelwidth );
    if (in_eq("TRIADS")) {
      Wgp->calc = origdata;
      do {
	  triadtype *Wtabtr = &Wgp->tabletriad[Wgp->ntriad + 1 - 1];

	  Wgp->ntriad = Wgp->ntriad + 1;
	  Win->in_item = word;
          per(i,1,3) Wtabtr->ir3[i - 1] = getirrep(gp);
	  Wtabtr->mult3 = get_integer();
	  Wtabtr->sym3 = getsym();
	  Wgp->last_trmult = max(Wgp->last_trmult, Wtabtr->mult3);
	  get_item(ask_free);
	  while ((Win->in_item != end_of_statement) && (!in_eq("ENDTRIADS")) )
	    get_item(ask_free);
      } while (Win->in_item == end_of_statement);
      makepowers(gp);
      get_item(ask_string);
      if      (in_eq("PRIMTABLE")) Wgp->table6 = primtable;
      else if (in_eq("FULLTABLE")) Wgp->table6 = fulltable;
      if (Wgp->table6 != notable) {
	gotone = false;
	gotall = true;
	do {
	    sixjtype *Wtab6j = &tablesixj[Wgp->zero6j + Wgp->nsix +1];

	    c_markheap(&marker);
	    minrep = Wgp->last_rep;
            per(i,1,6) {
		Wtab6j->ir6[i - 1] = get_integer();
		minrep = min( minrep, Wtab6j->ir6[i - 1]);
            }
            per(i,1,4) Wtab6j->mult6[i - 1] = get_integer();
	    Wtab6j->sym6 = get_integer();
	    Wtab6j->value6 = get_c_number();
	    if (c_known(Wtab6j->value6))
	      gotone = true;
	    else
	      gotall = false;
	    if (((Wgp->table6 == primtable) && (minrep == 1))
                || (Wgp->table6 == fulltable)) {
	      Wtab6j->value6 = c_move(Wtab6j->value6, safe_heap);
	      Wgp->nsix = Wgp->nsix + 1;
	    }
	    c_cutheap(marker);
	} while (!((Win->in_item == end_of_file)
                || (Wgp->zero6j + Wgp->nsix == max6js)));
	last6j = Wgp->zero6j + Wgp->nsix;
	if ((Win->in_item != end_of_file)) {
	  bufp += slprintf(sprintf(&buf[bufp], "  6J TABLE is FULL. INCREASE MAX6JS"));
	  send_warn();
	}
	if (gotone) {
	  if (gotall)
	    Wgp->calc = allvalues;
	  else
	    Wgp->calc = somevalues;
	}
      }
    }
    close_in_file();
  }
}

void readbranchdata()
{
  irreptype sub, ir;
  symtype sym;
  multtype mult;
  integer minrep, i;
  boolean gotone, gotall;
  c_number marker;
  printf("CTPRINT File to open: %s\n",branchdata.title);
  open_in_file(branchdata.title);
  {
    type_in_record   *Win = &in_record[in_file];
    gpdata_ *Wgp = &gpdata[group];
    gpdata_ *Wsg = &gpdata[subgroup];

    if (Wgp->howstored == product) {
      Wgp->last_rep   = -1;
      Wgp->labelwidth =  1;
    }
    if (Wsg->howstored == product) {
      Wsg->last_rep   = -1;
      Wsg->labelwidth =  1;
    }
    get_item(ask_string);
    per(ir, 0,max_reps)  per(sub, 0,max_reps) {
      Wbr->branches [ir][sub] = -1;
      Wbr->chosenket[ir][sub] = 0;
      Wbr->twojms   [ir][sub] = 0;
    }
    if (in_eq("BRANCHDATA")) {
      get_item(ask_string);
      wordcpy( Wbr->int_name, Win->in_word);
      get_item(ask_string);
    }
    if (in_eq("CREATED")) { get_item(ask_string);
      wordcpy( Wbr->filetime, Win->in_word); get_item(ask_string);
      wordcpy( Wbr->filedate, Win->in_word); get_item(ask_string);
    }
    if (in_eq("REFLECTED")) {
      Wbr->howembed = reflected; get_item(ask_string);
    }
    if (in_eq("BRANCHINGS")) {
      Wbr->calc3 = origdata; get_item(ask_string);
      do {
	if ( !in_eq("ENDBRANCHINGS") ) {
	  ir = findlabel(Win->in_word, group, true);
	  get_item(ask_char);
	  do {
	    if (Win->in_char == '(') {
	      get_item(ask_free);
	      if (Win->in_item == number) {
		mult = Win->in_number;
		get_item(ask_char);
		if (Win->in_char != ')') {
		  bufp += slprintf(sprintf(&buf[bufp], " INVALID MULT"));
		  put_input();
		  send_warn();
		} else
		  get_item(ask_char);
	      } else {
		bufp += slprintf(sprintf(&buf[bufp], " INVALID MULT"));
		put_input();
		send_warn();
	      }
	    } else
	      mult = 1;
	    if (Win->in_char == '+')
	      sym = 0;
	    else if (Win->in_char == '-')
	      sym = 1;
	    else {
	      bufp += slprintf(sprintf(&buf[bufp], " not MULT or 2JM")); put_input(); send_warn();
            }
	    get_item(ask_string);
	    sub = findlabel(Win->in_word, subgroup, true);
	    Wbr->branches[ir][sub] = Wbr->branches[ir][sub] + mult;
	    Wbr->last_brmult = max(Wbr->last_brmult,Wbr->branches[ir][sub]);
	    Wbr->twojms[ir][sub] = sym;
	    get_item(ask_char);
	  } while (!(Win->in_item != delimiter));
	}
	get_item(ask_string);
      } while (!in_eq("ENDBRANCHINGS"));
      get_item(ask_free);
      make_product_ir(group);
      make_product_ir(subgroup);
      if (gpdata[group].howstored == product) makepowers(group);
      if (gpdata[group].continuous) {
	ir = gpdata[group].last_rep;
        per(sub,0,gpdata[subgroup].last_rep) {
	      Wbr->branches[ir][sub] = -1;
	      Wbr->twojms  [ir][sub] = 0;
	}
      }
      if (in_eq("CHOSENKETS")) {
        per(ir, 0,gpdata[group].last_rep)
        per(sub,0,gpdata[group].last_rep)
	    Wbr->chosenket[ir][sub] = 0;
	get_item(ask_free);
	do { 
	  irreptype ir  = Win->in_number, sub = get_integer();
          integer   ket = get_integer();

	  Wbr->chosenket[ir][sub] = ket;
	  get_item(ask_free);
	} while (! member2( Win->in_item, end_of_file, word ));
      }
      if      (in_eq("PRIMTABLE"))	Wbr->table3 = primtable;
      else if (in_eq("FULLTABLE")) 	Wbr->table3 = fulltable;
      if (Wbr->table3 != notable) {
	gotone = false;
	gotall = true;
	do {
	    threejmtype *Wtab3j = &Wbr->table3jm[Wbr->nthree +1];

	    c_markheap(&marker);
            per(ir,1,3) Wtab3j->gpir[ir - 1] = get_integer();
	    Wtab3j->gpmult = get_integer();
            per(sub,1,3) Wtab3j->subir[sub - 1] = get_integer();
	    Wtab3j->submult = get_integer();
            per(i,1,3) Wtab3j->brmult[i - 1] = get_integer();
	    Wtab3j->sym3jm = get_integer();
	    if (get_integer() == 0)
	      Wtab3j->star3 = false;
	    else
	      Wtab3j->star3 = true;
	    Wtab3j->value3 = get_c_number();
	    if (c_known(Wtab3j->value3))
	      gotone = true;
	    else
	      gotall = false;
	    if (((Wbr->table3 == primtable) && (Wtab3j->gpir[3 - 1] == 1))
                || (Wbr->table3 == fulltable)) {
	      Wtab3j->value3 = c_move(Wtab3j->value3, safe_heap);
	      Wbr->nthree = Wbr->nthree + 1;
	    }
	    c_cutheap(marker);
            if ((Wbr->nthree == Wbr->max3jms-1)) {
              bufp += slprintf(sprintf(&buf[bufp], "  3JM table is too small to contain"));
              bufp += slprintf(sprintf(&buf[bufp], " the primitive 3JM. Increase MAX3JMS"));
              send_warn();
            }
	} while (!((Win->in_item == end_of_file)
                || (Wbr->nthree  == Wbr->max3jms)));
	if (gotone) {
	  if (gotall)
	    Wbr->calc3 = allvalues;
	  else
	    Wbr->calc3 = somevalues;
	}
      }
    }
    printf("CTPRINT In file to close: %d\n", in_file);
    close_in_file();
  }
}


void make_spin_label(label_ir, ir, minus, gp)
word_type label_ir;
integer ir;
boolean minus;
grouptype gp;
{
  integer j;

  wordcpy(label_ir, blank);
  j = 1;
  if (minus) {
    label_ir[1 - 1] = '-';
    j = 2;
  }
  if (ir & 1) {
    label_ir[j - 1] = spin_char;
    j = j + 1;
  }
  ir = ir / 2;
  if (ir > 9) {
    label_ir[j - 1] = (ir / 10) + (unsigned) ('0');
    j = j + 1;
  }
  label_ir[j - 1] = (ir % 10) + (unsigned) ('0');
  label_ir[j] = 0;
  gpdata[gp].labelwidth = max(gpdata[gp].labelwidth, j);
}

void set_up_formulas(gp)
grouptype gp;
{
  irreptype ir;
  integer i, j;
  {
    gpdata_ *Wgp = &gpdata[gp];

    switch (Wgp->howstored) {
      case as_tables:
      case product:
	break;
      case so3:
	Wgp->continuous = true;
	Wgp->last_rep = max_reps;
        per(ir,0,Wgp->last_rep){
	      Wgp->conj [ir] = ir;
	      Wgp->power[ir] = ir;
	      Wgp->dimen[ir] = ir + 1;
	      Wgp->two  [ir] = ir % 2;
	      if (streq(Wgp->ext_name,"SU2"))
		i = ir * 2;
	      else
		i = ir;
	      make_spin_label(Wgp->labels[ir], i, false, gp);
	}
	break;
      case d00:
	Wgp->continuous = true;
	Wgp->last_rep = max_reps;
        per(ir,0,Wgp->last_rep) {
	      if (ir > 2)
		i = ir - 1;
	      else
		i = ir;
	      Wgp->conj[ir] = ir;
	      Wgp->power[ir] = i;
	      Wgp->two[ir] = i % 2;
	      Wgp->dimen[ir] = 2;
	      make_spin_label(Wgp->labels[ir], i, false, gp);
	}
	Wgp->dimen[0] = 1;
	Wgp->dimen[2] = 1;
	wordcpy(Wgp->labels[2],"^0");
	break;
      case cyclic:
	if (Wgp->continuous && (Wgp->last_rep & 1)) Wgp->last_rep--;
        per(ir,0,Wgp->last_rep) {
	      Wgp->power[ir] = (ir + 1) / 2;
	      Wgp->dimen[ir] = 1;
	      Wgp->two[ir] = 0;
	      if ((ir == 0) || (ir == Wgp->last_rep))
		Wgp->conj[ir] = ir;
	      else if ((boolean) ((ir) & 1))
		Wgp->conj[ir] = ir + 1;
	      else
		Wgp->conj[ir] = ir - 1;
	      i = (ir + 1) / 2;
	      if (streq(Wgp->ext_name,"U1"))
		i = i * 2;
	      make_spin_label(Wgp->labels[ir], i,
                              ((ir > 0) && !((ir) & 1)), gp);
	}
	if (streq(Wgp->int_name,"CI")) {
	  wordcpy(Wgp->labels[0],"+");
	  wordcpy(Wgp->labels[1],"-");
	  Wgp->labelwidth = 1;
          Wgp->last_rep   = 1;
	}
	break;
      default:
	Caseerror(Line);
    }
  }
}


grouptype link_groups(g1, g2)
grouptype g1, g2;
{
  grouptype gp;
  word_type tempname, temp2name, separator;
  irreptype ir, ir1, ir2;
  integer ness_reps;

  if (g1 == -1)
    gp = g2;
  else if (g2 == -1)
    gp = g1;
  else {
    concat(gpdata[g1].int_name, ".", temp2name);
    concat(temp2name, gpdata[g2].int_name, tempname);
    gp = find_gp_slot(tempname);
    {
      gpdata_ *Wgp = &gpdata[gp];

      concat(gpdata[g1].ext_name, ".", tempname);
      concat(tempname, gpdata[g2].ext_name, Wgp->ext_name);
      Wgp->howstored = product;
      Wgp->continuous = ((gpdata[g1].continuous) || (gpdata[g2].continuous));
      Wgp->group1 = g1;
      Wgp->group2 = g2;
      ness_reps = (gpdata[g2].last_rep + 1) * (gpdata[g1].last_rep + 1) -1;
      if (ness_reps > max_reps)
	if (Wgp->continuous) {
	  gpdata[g1].last_rep = (max_reps + 1) / (gpdata[g2].last_rep + 1) - 1;
          if (gpdata[g1].conj[gpdata[g1].last_rep] > gpdata[g1].last_rep)
              gpdata[g1].last_rep--;
	} else {
	  bufp += slprintf(sprintf(&buf[bufp], " MAX_REPS MUST BE INCREASED to %ld", ness_reps));
	  send_warn();
	}
      if ((streq(gpdata[g2].int_name,"CI")))
	wordcpy(separator, blank);
      else
	wordcpy(separator,".");
      per(ir2,0,gpdata[g2].last_rep) 
      per(ir1,0,gpdata[g1].last_rep) {
        concat(gpdata[g1].labels[ir1], separator, temp2name);
	concat(temp2name, gpdata[g2].labels[ir2], tempname);
	ir = findlabel(tempname, gp, true);
      }
      make_product_ir(gp);
    }
  }
  return gp;
}

grouptype read_a_group()
{
  word_type ext_name, int_name, iso_name;
  grouptype g1, g2, g3, gp;
  integer j, i;
  boolean split;

  get_item(ask_string);
  g1 = -1; g2 = -1; g3 = -1; gp = -1;
  {
    type_in_record *Win = &in_record[in_file];

    if ((Win->in_item == word)) {
      wordcpy( int_name, Win->in_word);
      wordcpy( iso_name, Win->in_word);
      wordcpy( ext_name, Win->in_word);

      split = false;
      /* unsystematic names */
      if (in_eq("C00")) { wordcpy(int_name,"SO2"); wordcpy(iso_name,"SO2");};
      if (in_eq("U1" )) { wordcpy(int_name,"SO2"); wordcpy(iso_name,"SO2");};
      if (in_eq("SU2")) { wordcpy(int_name,"SO3"); wordcpy(iso_name,"SO3");};

      /* systematic names for groups isomorphic to internal groups */
      if (in_eq("C00V"))wordcpy(int_name,"D00");
      if (in_eq("TD" )) wordcpy(int_name,"O" );
      if (in_eq("D3H")) wordcpy(int_name,"D6");
      if (in_eq("C6V")) wordcpy(int_name,"D6");
      if (in_eq("C5V")) wordcpy(int_name,"D5");
      if (in_eq("D2D")) wordcpy(int_name,"D4");
      if (in_eq("C4V")) wordcpy(int_name,"D4");
      if (in_eq("C3V")) wordcpy(int_name,"D3");
      if (in_eq("C2V")) wordcpy(int_name,"D2");
      if (in_eq("C3H")) wordcpy(int_name,"C6");
      if (in_eq("S4" )) wordcpy(int_name,"C4");
      if (in_eq("CS" )) wordcpy(int_name,"C2");
      /* inversion groups */
      if (in_eq("O3" )) { wordcpy(int_name,"SO3"); split = true;} 
      if (in_eq("O2" )) { wordcpy(int_name,"SO2"); split = true;} 
      if (in_eq("OH" )) { wordcpy(int_name,"O"  ); split = true;}
      if (in_eq("TH" )) { wordcpy(int_name,"T"  ); split = true;}
      if (in_eq("D00H")){ wordcpy(int_name,"D00"); split = true;}
      if (in_eq("D6H")) { wordcpy(int_name,"D6" ); split = true;}
      if (in_eq("D5D")) { wordcpy(int_name,"D5" ); split = true;}
      if (in_eq("D4H")) { wordcpy(int_name,"D4" ); split = true;}
      if (in_eq("D3D")) { wordcpy(int_name,"D3" ); split = true;}
      if (in_eq("D2H")) { wordcpy(int_name,"D2" ); split = true;}
      if (in_eq("C6H")) { wordcpy(int_name,"C6" ); split = true;}
      if (in_eq("C5I")) { wordcpy(int_name,"C5" ); split = true;}
      if (in_eq("C4H")) { wordcpy(int_name,"C4" ); split = true;}
      if (in_eq("C3I")) { wordcpy(int_name,"C3" ); split = true;}
      if (in_eq("S6" )) { wordcpy(int_name,"C3" ); split = true;}
      if (in_eq("C2H")) { wordcpy(int_name,"C2" ); split = true;}
      if (in_eq("CI" )) { wordcpy(int_name,"C1" ); split = true;}
      if (in_eq("S2" )) { wordcpy(int_name,"C1" ); split = true;}

      g1 = find_gp_slot(int_name);
      {
	gpdata_ *Wg1 = &gpdata[g1];

	if (split) {
	  wordcpy( Wg1->ext_name, int_name );
          wordcpy( Wg1->iso_name, int_name );
	}
	else{
	  wordcpy( Wg1->ext_name, ext_name );
          wordcpy( Wg1->iso_name, iso_name );
	}
	get_item(ask_string);
	if ((Win->in_item == word) && (Win->in_word[1 - 1] == '(')) {
	  wordcpy( Wg1->tag, Win->in_word ); get_item(ask_string);
	}
	if (streq(int_name,"SO3")) Wg1->howstored = so3;
	if (streq(int_name,"D00")) Wg1->howstored = d00;
	if (streq(int_name,"SO2")) {
	    Wg1->howstored  = cyclic;
	    Wg1->continuous = true;
	    Wg1->last_rep   = 2*(int)(max_reps/2);
        }
	if (int_name[0] == 'C') {
	    if (streq(int_name,"CI"))
	      i = 1;
	    else
	      i = (int_name[2 - 1]) - ('0');
	    if ((i >= 0) && (i < 10)) {
	      if ( strlen(int_name) <= 2) {
		Wg1->howstored = cyclic;
		Wg1->last_rep = 2 * i - 1;
	      } else {
		j = (unsigned) (int_name[3 - 1]) - (unsigned) ('0');
		if ( (strlen(int_name) <= 3) && (j >= 0) && (j < 10)) {
		  Wg1->howstored = cyclic;
		  i = 20 * i + 2 * j - 1;
		  if ((i > 0) && (i < max_reps))
		    Wg1->last_rep = i;
		  else {
		    Wg1->continuous = true;
		    Wg1->last_rep = 2*(int)(max_reps/2);
		  }
		}
	      }
	    }
	}
	if (in_eq("6FILE") ) {
	  get_item(ask_string);
	  if (Win->in_item == word) {
	    wordcpy( Wg1->title, Win->in_word );
	    Wg1->howstored = as_tables;
	    get_item(ask_string);
	  } else {
	    bufp += slprintf(sprintf(&buf[bufp], " TITLE EXPECTED"));
	    put_input();
	    send_warn();
	  }
	} else if (Wg1->howstored == as_tables)
	  concat( prefix06title, Wg1->int_name, Wg1->title);
	if (Wg1->timesused == 1) {
	  if (!streq(Wg1->title, blank) )
	    read_gp_data(g1);
	  else if (Wg1->howstored != as_tables)
	    set_up_formulas(g1);
	}
	if (split) {
	  g2 = find_gp_slot("CI");
	  {
	    gpdata_ *Wg2 = &gpdata[g2];

	    if (Wg2->timesused == 1) {
	      wordcpy( Wg2->ext_name, Wg2->int_name );
	      Wg2->howstored = cyclic;
	      Wg2->last_rep = 1;
	      set_up_formulas(g2);
	    }
	  }
	}
      }
      if (in_eq("*") )  g3 = read_a_group();
      gp = link_groups(g1, link_groups(g2, g3) );
      wordcpy( gpdata[gp].ext_name, ext_name);
      wordcpy( gpdata[gp].iso_name, iso_name);
      put_group_header(gp);
    }
  }
  return gp;
}

void read_subgroup()
{
  integer   j, i;
  word_type tilde, temp;
  irreptype ir, ir1;
  triadtype tr;
  boolean   found;
  grouptype gp, sg;

  {
    type_in_record *Win = &in_record[in_file];

    wordcpy(Wbr->title, blank);
    wordcpy(tilde, blank);
    wordcpy(Wbr->int_name, blank);
    wordcpy(Wbr->filetime, blank);
    wordcpy(Wbr->filedate, blank);
    Wbr->tilderep = 0;
    Wbr->howembed = embedded;
    Wbr->paired = false;
    Wbr->nthree = 0;
    Wbr->max3jms = increment_3jm;
    Wbr->calc3 = nothing;
    Wbr->table3 = notable;
    Wbr->last_brmult = 0;
    subgroup = -1;
    Wbr->bothreflected = false;
    Wbr->table3jm = 
       (threejmtype *) MALLOC( (Wbr->max3jms+1) * sizeof(threejmtype));
    if ((Win->in_item == word)) {
      if (in_eq("3FILE")) {
	get_item(ask_string);
	if (Win->in_item == word)
	  wordcpy( Wbr->title, Win->in_word );
	else
	  warninput("3FILE TITLE");
      } else if (!in_eq("TO"))
	warninput("3FILE or TO");
      subgroup = read_a_group();
      if (subgroup != -1) {
	if ( (gpdata[group   ].howstored == product)
        &&   (gpdata[subgroup].howstored == product)
	&&   (streq(gpdata[gpdata[   group].group2].int_name,"CI"))
	&&   (streq(gpdata[gpdata[subgroup].group2].int_name,"CI")) )
	    Wbr->bothreflected = true;
	if (Wbr->bothreflected) {
	  gp = group;
	  sg = subgroup;
	  group    = gpdata[group].group1;
	  subgroup = gpdata[subgroup].group1;
	} {
	  gpdata_ *gpdat_sg = &gpdata[subgroup];

	  switch (gpdata[group].howstored) {
	    case as_tables:
	      break;
	    case product:
	      {
		gpdata_ *gpdat_gr = &gpdata[group];

		if (streq(gpdata[gpdat_gr->group1].int_name,
                          gpdata[subgroup].int_name) ) {
		  if (streq(gpdata[gpdat_gr->group1].int_name,
                            gpdata[gpdat_gr->group2].int_name) )
		    Wbr->howembed = coupled;
		  else if (streq(gpdata[gpdat_gr->group2].int_name, "CI"))
		    Wbr->howembed = reflected;
		}
		Wbr->paired = true;
	      }
	      break;
	    case so3:
	      if (gpdat_sg->howstored == d00)
		Wbr->howembed = so3d00;
	      else if ((gpdat_sg->howstored == cyclic) && gpdat_sg->continuous)
		Wbr->howembed = so3so2;
	      break;
	    case d00:
	      if ((gpdat_sg->howstored == cyclic) && gpdat_sg->continuous)
		Wbr->howembed = d00so2;
	      break;
	    case cyclic:
	      if (gpdat_sg->howstored == cyclic)
		Wbr->howembed = cyclic_cyclic;
	      break;
	    default:
	      Caseerror(Line);
	  }
	}
	if ( streq(Wbr->title, blank) )
	  if (Wbr->howembed == embedded) {
	    concat(gpdata[group].int_name, gpdata[subgroup].int_name, temp);
	    concat( prefix03title, temp, Wbr->title);
	  }
	if ( !streq(Wbr->title, blank) )
	  readbranchdata();
	else if (Wbr->howembed == reflected) {
	  gpdata_ *Wsg = &gpdata[subgroup];

	  if (streq(tilde, blank)) {
	    wordcpy(tilde,"^0");
	    if (streq(Wsg->int_name, Wsg->iso_name))
	      wordcpy(tilde,"0");
	    else if (Wbr->tilderep == 0) {
	      char *name = Wsg->int_name;
	      if      (streq(name,"D2D")) wordcpy(tilde,"2");
	      else if (streq(name,"D3H")) wordcpy(tilde,"3");
	      else if (streq(name,"S4" )) wordcpy(tilde,"2");
	      else if (streq(name,"C3H")) wordcpy(tilde,"3");
	      else if (streq(name,"CS" )) wordcpy(tilde,"1");
	    }
	    bufp += slprintf(sprintf(&buf[bufp], "  TILDE = %s", tilde));
	    send();
	    Wbr->tilderep = findlabel(tilde, subgroup, false);
	    per(ir,0,Wsg->last_rep) {
	      storetriad(&tr, 0, Wsg->conj[ir], Wbr->tilderep, 0, 0);
	      ir1 = 0;
	      do {
		tr.ir3[1 - 1] = ir1;
		found = findtriad(&tr, subgroup);
		ir1 = ir1 + 1;
	      } while (!(found || (ir1 > Wsg->last_rep)));
	      if (found)
	   	tildeir[ir] = tr.ir3[1 - 1];
	      else {
		bufp += slprintf(sprintf(&buf[bufp], " no TILDE for")); putirrep(ir, subgroup);
		bufp += slprintf(sprintf(&buf[bufp], "  in group %s", Wsg->ext_name));
		send_warn();
	      }
	    }
	  }
	}
	put_branch_header(); send();
	if (Wbr->bothreflected) { group = gp; subgroup = sg; }
      }
    }
  }
}

void setup_group_and_subgroup()
{
  cancelgp(group);    group = read_a_group();
  cancelgp(subgroup); read_subgroup();
}

void informtriad()
{
  triadtype tr;
  boolean found;

  gettriad(&tr, group);
  found = findtriad(&tr, group);
  puttriad(tr, group);
  if (!found) bufp += slprintf(sprintf(&buf[bufp], "    not FOUND"));
  send();
}

void get_range( first, last, first0, last0)
               integer* first; integer *last; integer first0; integer last0;
/*
void get_range(integer* first, integer *last, integer first0, integer last0)
*/
{
  type_in_record *Win = &in_record[in_file];
  *first = first0; *last = last0; 
  get_item(ask_free); 
  if(Win->in_item == number) {
    *first = Win->in_number; get_item(ask_free);
    if(Win->in_item == number) *last = Win->in_number; else *last = *first;
  }
  *last = min(*last, *first+100);
}


void showtriad()
{
  integer first, last, place;

    gpdata_ *Wgp = &gpdata[group];
    get_range( &first, &last, 1, Wgp->ntriad );
    per(place,first,last) {
      if ((place < 1) || (place > Wgp->ntriad))
        bufp += slprintf(sprintf(&buf[bufp], " %ld not in 1..NTRIAD", place));
      else
        puttriad(Wgp->tabletriad[place - 1], group);
      send();
    }
}

void inform6j()
{
  sixjtype s;
  irreptype ir;
  multtype m;
  boolean found;

  get6j(&s, group);
  found = find6j(&s, group);
  put6j(s, group);
  send();
}

void show6j()
{
  integer first, last, place;

  gpdata_ *Wgp = &gpdata[group];
  get_range( &first, &last, 1, Wgp->nsix );
  per(place, first, last) { 
    if ((place < 1) || (place > Wgp->nsix )) {
      bufp += slprintf(sprintf(&buf[bufp], " %ld not in 1..%ld", place, Wgp->nsix)); send();
    }
    bufp += slprintf(sprintf(&buf[bufp], " %3ld: ",place)); put6j(tablesixj[Wgp->zero6j + place], group);
    send();
  }
}

void inform9j()
{
  ninejtype n;

  get9j (&n, group);
  find9j(&n, group);
  put9j ( n, group);
  send();
}

void inform3jm()
{
  threejmtype t;

  get3jm (&t);
  find3jm(&t, group, subgroup);
  put3jm (t);
  send();
}

void show3jm()
{
  integer first, last, place;

  get_range( &first, &last, 1, branchdata.nthree );
  per(place, first, last) {
    if ((place < 1) || (place > branchdata.nthree)) {
      bufp += slprintf(sprintf(&buf[bufp], " %ld not in 1..%ld", place, branchdata.nthree)); send();
    }
    bufp += slprintf(sprintf(&buf[bufp], " %3ld: ",place)); put3jm(branchdata.table3jm[place]);
    send();
  }
}

void show2jm()
{
  integer first, last;
  irreptype irgp, irsg;
  multtype brm;
  gpdata_ *Wgp = &gpdata[group];
  get_range( &first, &last, 0, Wgp->last_rep );
  per(irgp, first, last) {
    if (irgp > Wgp->last_rep ) {
      bufp += slprintf(sprintf(&buf[bufp], " %d not in 0..%ld", irgp, Wgp->last_rep)); send();
    }
    putirrep(irgp,group); bufp += slprintf(sprintf(&buf[bufp], "  ->"));
    per(irsg,0,gpdata[subgroup].last_rep) {
      brm = branchmult(irgp,irsg);
      if(brm>=0) {
        putsym(find2jm(irgp, irsg));
        if(brm > 0) putbrmult(brm+1);
        putirrep(irsg,subgroup);
        if (bufp > 70) send();
      }
    }
    send();
  }
}

void write_irrep_table()
{
irreptype ir;
  bufp += slprintf(sprintf(&buf[bufp], " irreps for group %s: ", gpdata[group].ext_name));
  per(ir,0,gpdata[group].last_rep) {
    putirrep(ir,group); if (bufp > 70) send();
  }
  send();
}

void writetable(which)
integer which;
{
}

boolean get_command(prompt)
char prompt;
{
  boolean understood;
  c_number marker;

  {
    type_in_record *Win = &in_record[in_file];

    Win->in_prompt = prompt;
    understood = true;
    do {
      statistics("GET COMMAND");
      if ((Win->in_item != end_of_statement) &&
          (Win->in_item != end_of_file))
	get_item(ask_word);
      if ((Win->in_item != end_of_statement) &&
          (Win->in_item != end_of_file)) {
	bufp += slprintf(sprintf(&buf[bufp], " END of STATEMENT EXPECTED);      INPUT SKIPPED:"));
	put_input();
	send_warn();
	do {
	  get_item(ask_word);
	} while (!((Win->in_item == end_of_statement) ||
                   (Win->in_item == end_of_file)));
      }
      get_item(ask_word);
      if (Win->in_item == delimiter) {
	bufp += slprintf(sprintf(&buf[bufp], " COMMAND WORD EXPECTED"));
	put_input();
	send_warn();
      } else if (Win->in_item == word) {
	if ( (in_eq("LOGFILE")) || (in_eq("Y")) )
	  open_printer(p_logfile,  logfile_head);
	else if (in_eq("NOLOGFILE")) 	logging = false;
	else if (in_eq("INTERACTIVE"))	interactive = true;
	else if (in_eq("DEBUG"))	debug = true;
	else if (in_eq("NODEBUG"))	debug = false;
	else if (in_eq("DEBUGBUTLER"))	debug_butler = true;
	else if (in_eq("EXIT"))		Win->in_item = exit_loop;
	else if (in_eq("FINISH"))	Win->in_item = end_of_file;
	else if (in_eq("CNUMBER")) {	c_markheap(&marker);
	  				put_c_number(get_c_number());
	  				send();
	  				c_cutheap(marker);
      } else
	  understood = false;
      }
    } while (!((!understood) || (Win->in_item == end_of_file)));
  }
  return understood;
}


void xint(in_int)
biginteger *in_int;
{
  boolean ready, error;

  {
    type_in_record *Win = &in_record[in_file];

    if (Win->in_item == delimiter)  get_item(ask_free);
    if (Win->in_char != '#')
      do {
	error = (Win->in_item != number);
	if (!error) {
	  (*in_int) = (*in_int) * (biginteger)(Win->in_number);
	  get_item(ask_free);
	  ready = (Win->in_char != '.');
	  if (!ready)  get_item(ask_free);
	}
      } while (!(ready || error));
  }
}

c_number get_c_number()
{
  c_number c_num, c_ptr;
  biginteger a, b, c, d;
  boolean imag, error, starting;

  {
    type_in_record *Win = &in_record[in_file];

    get_item(ask_free);
    error = false;
    imag = false;
    c_num = c_unknown;
    if (in_eq("I") || in_eq("J")) {
      imag = true;
      get_item(ask_free);
    }
    if (Win->in_char == '?')
      c_num = c_unknown;
    else if (in_eq("ADD"))	c_num = c_add(get_c_number(), get_c_number());
    else if (in_eq("MULT"))	c_num = c_mult(get_c_number(), get_c_number());
    else if (in_eq("CONJ"))	c_num = c_conj(get_c_number());
    else if (in_eq("MINUS"))	c_num = c_minus(get_c_number());
    else if (in_eq("SQRT"))	c_num = c_sqrt(get_c_number());
    else if (in_eq("INVERT"))	c_num = c_invert(get_c_number());
    else if ((Win->in_char == '#') || (Win->in_item == number)) {
      starting = true;
      c_num = c_zero;
      do {
	a = (biginteger)1;
	b = (biginteger)1;
	c = (biginteger)1;
	d = (biginteger)1;
	if (Win->in_item == number)
	  xint(&a);
	if ((in_eq("I")) ||
            (in_eq("J"))) {
	  imag = true;
	  get_item(ask_free);
	}
	if (Win->in_char == '#')
	  xint(&b);
	if (Win->in_char == '/') {
	  xint(&c);
	  if (Win->in_char == '#')
	    xint(&d);
	}
	if (a * b != (biginteger)0)
	  if (c * d == (biginteger)0) {
	    bufp += slprintf(sprintf(&buf[bufp], " ZERO DIVIDE in GET_C_NUMBER"));
	    put_input();
	    send_warn();
	  } else {
	    if (imag) {
	      b = -b;
	      imag = false;
	      if (b > (biginteger)0)
		a = -a;
	    }
	    c_num = c_add(c_num, c_convert(a, b, c, d));
	  }
      } while (Win->in_item == number);
    } else {
      error = true;
      c_num = c_unknown;
    }
    if ((Win->in_char == ',') || (Win->in_item == end_of_statement) ||
        (Win->in_item == end_of_file));
    else
      error = true;
    if (error) {
      bufp += slprintf(sprintf(&buf[bufp], " INVALID C_NUMBER"));
      put_input();
      send_warn();
    }
  }
  return c_num;
}

void butler();
/*
   THIS Procedure doES A CRYSTAL FIELD CALCULATION.
   IT is AS MUCH AS POSSIBLE INDEPendENT of the REST of the PROGRAM:
    - GPDATA is COPIED to LOCAL STRUCTURES
    - DETAILED KNOWLEDGE of GROUPDATA INPUT and 3JM FACTOR CALCULATION
      is not NEEDED to UNDERSTAND "BUTLER"
    - SOME KNOWLEDGE of INPUT ROUTINES ("GET_ITEM" and"GET_INTEGER")
      and OUTPUT ROUTINES ("SEND", "SEND_PRINTER", "SEND_WARN", "warninput",
      "PUT_NAME" and the USE of INTERMEDIATE FILE "BUF")
      CAN BE USEFUL
    - NAMES LOCAL to"BUTLER" do not CONTAIN "0" AS A SEPARATOR,
      EXCEPT NAMES of POINTERTYPES, WHICH START With "P0".
      "PUT1BRMULT" and "GET1BRMULT" CONTAIN A "1" to DISTINGUISH THEM
      from THEIR NON LOCAL ANALOGONS With"0".

   LISTS START With A "HEAD" ELEMENT and end With "NIL" and ARE LINKED
   BY "NEXT". in the CONSTRUCTION of A LIST A DUMMY"START" ELEMENT is
   USED WHOSE "NEXT" is MADE the "HEAD".

   COMMANDS ARE READ from INPUT ("MAIN").
   GROUPDATA ARE READ from DISK0 (VIA"DISK_ONE").
   SUPERGROUP INFORMATION (IRREPS and REDUCED MATRIIX ELEMENTS) is READ
   from DISK2 ("DISK_TWO").
*/

void initbutler()
{
  (comzero).r = 0.0;
  (comzero).i = 0.0;
  (comlarge).r = (real)(1e30);
  (comlarge).i = (real)(1e30);
  branchdata.nthree = 0;
  maxdim = 0;
  maxbra = 0;
}

void spaceback()
{
  in_record[in_file].in_index--;
}

void skip()
{
  in_record[in_file].in_index++;
}

char getnext(next)
char *next;
/* GET NEXT CHARACTER */
{
    type_in_record *Win = &in_record[in_file];

    if ( !( member2(Win->in_item, end_of_file, end_of_statement ) ) ) {
      Win->in_index++;
      Win->in_char = Win->in_buff[Win->in_index - 1];
      Win->in_item = delimiter;
      if (Win->in_char == ';') {
	Win->in_item = end_of_statement;
	skip();
      }
    } else
      Win->in_char = ';';
    (*next)   = Win->in_char;
    return *next;
}

char getfollow(next)
char *next;
/* GET NEXT NON BLANK CHARACTER, POSSIBLY from NEXT LINE */
{
    if ( member2( in_record[in_file].in_item, end_of_file, end_of_statement ) )
      warninput("MORE SYMBOLS");
    else {
      get_buff();
      spaceback();
    }
    return getnext(&(*next));
}

real sqr( x )
real x;
{ return x*x; }

real to_power( x, i )
real x; integer i;
/*
real to_power(real x, integer i)
*/
{
  real z, result;

  if (i < 0)
    x = 1/x;
  z = 1.0;
  while (i != 0) {
    if ((boolean) ((i) & 1))
      z = z * x;
    i = i / 2;
    x = sqr(x);
  }
  return z;
}

integer findmode( mode, i)
integer mode, i;
/*
integer findmode(integer mode, integer i)
*/
{
  integer j;

  for (j = 1; j <= i - 1; j++)
    mode = mode / 10;
  return mode % 10;
}

void putcomplex(complex)
complextype complex;
{
       if (complex.r == 0 && complex.i == 0) bufp += slprintf(sprintf(&buf[bufp], " +0"));
  else if (complex.i == 0) bufp += slprintf(sprintf(&buf[bufp], " %.8f",complex.r));
  else 
     bufp += slprintf(sprintf(&buf[bufp], " (%.8f,%.8f)" ,complex.r, complex.i));
}

real getreal()
{
  char first, next;
  real d, x;

  x = 0.0;
  d = 10.0;
  {
    if ( member2(in_record[in_file].in_item, end_of_file, end_of_statement ) )
      warninput("REAL NUMBER");
    else {
      if (member2( getfollow(&first), '+','-' ) ) {
	skip();
	next = getfollow(&next);
      }
      spaceback();
      while ( isdigit(getnext(&next)) )
	x = 10.0 * x + ((unsigned) (next) - (unsigned) ('0'));
      if (next == '.')
	while ( isdigit(getnext(&next)) ) {
	  x = x + ((unsigned) (next) - (unsigned) ('0')) / ((real) d);
	  d = d * 10.0;
	}
      if (next == 'E') {
	skip();
	x = x * to_power(10.0, (get_integer()));
      }
      if (first == '-')
	x = -x;
      if ( !( member3( first,'+','-','.' ) || isdigit(first) ) ) {
	warninput("REAL NUMBER");
	skip();
      }
    }
  }
  return x;
}

void getcomplex(complex)
complextype *complex;
{
  char first, next;

  {
    if ( member2(in_record[in_file].in_item, end_of_file, end_of_statement ) )
      warninput("COMPLEX NUMBER");
    else {
      if (getfollow(&first) == '(') {
	skip();
	complex->r = getreal();
	if (getfollow(&next) == ',')
	  skip();
	else
	  warninput("-> ,");
	complex->i = getreal();
	if (getfollow(&next) == ')')
	  skip();
	else
	  warninput("-> )");
      } else {
	complex->r = getreal();
	complex->i = 0.0;
      }
    }
  }
}

void comvalue(c_num, comc_n)
c_number c_num;
complextype *comc_n;
{
  boolean stopping;
  real absval;

  if (c_num == c_zero)
    (*comc_n) = (comzero);
  else if (c_num->op == unknown) {
    bufp += slprintf(sprintf(&buf[bufp], " UNKNOWN C_NUMBER in \"COMVALUE\""));
    send_warn();
    if (racer)
      bufp += slprintf(sprintf(&buf[bufp], "  ?"));
  } else {
    comc_n->r = c_num->re;
    comc_n->i = c_num->im;
  }
}

void putmixprod(com1, conum)
complextype com1;
c_number conum;
{
  complextype com2, comx;
  c_op_type op;

#define comprod(a,b,c) \
  (c).r = (c).r + (a).r*(b).r - (a).i*(b).i; \
  (c).i = (c).i + (a).r*(b).i + (a).i*(b).r
  comvalue(conum, &com2);

  comx = comzero; 
  comprod(com1, com2, comx);
  putcomplex(comx);
}

void mixprod(com, com1, com2, c_3)
complextype *com, com1, com2;
c_number c_3;
{
  complextype com3, comx;
  comvalue(c_3, &com3);
  comx = comzero;
  comprod(com1,com2,comx);
  comprod(com3,comx,*com);
#undef comprod
}

void put1brmult(br, gp)
multtype br;
p_groupx gp;
{
  if (gp->lastbrmult > 0)
    bufp += slprintf(sprintf(&buf[bufp], " %2d", br));
}

void putbranch(branch, lgp)
p_branch branch;
linktype lgp;
{
  if (lgp > 1) {
    putbranch(branch->parent, lgp - 1);
    bufp += slprintf(sprintf(&buf[bufp], "  >"));
  }
  put1brmult(branch->brmult, chain[lgp]);
  putirrep(branch->ir, chain[lgp]->slot);
}

  void put1triad( tr, lgp)  ntriadtype tr; linktype lgp;
/*void put1triad( ntriadtype tr, linktype lgp)*/
{
  braoprkettype bok;

  {
    groupxtype  *Wgpl  = chain[lgp];
    triadx_ *Wgptr = &Wgpl->triad[tr - 1];

    bufp += slprintf(sprintf(&buf[bufp], "  TRIAD %*ld (", (int)qx(chain[lgp]->ntriads), tr));
    per(bok, bra, ket) putirrep(Wgptr->ir[bok], Wgpl->slot);
    putprodmult(Wgptr->trmult, Wgpl->slot);
    bufp += slprintf(sprintf(&buf[bufp], " )"));
  }
}

void putgroupx(lgp)
linktype lgp;
{
  {
    p_groupx Wgpl = chain[lgp];

    send();
    bufp += slprintf(sprintf(&buf[bufp], "  GROUP %s %s ",gpdata[Wgpl->slot].ext_name,
			  gpdata[Wgpl->slot].int_name));
    bufp += slprintf(sprintf(&buf[bufp], "  SLOT=%ld  NREPS=%d  LASTTRMULT=%d",
             Wgpl->slot, Wgpl->nreps, Wgpl->lasttrmult));
    send();
    statistics(gpdata[Wgpl->slot].ext_name);
  }
}

void putj3s(lgp)
linktype lgp;
{
  braoprkettype bok;
  tasktype task;
  unsigned short tr;
  p_j3 j3;

  {
    p_groupx Wgpl = chain[lgp];

    send();
    bufp += slprintf(sprintf(&buf[bufp], " <TRIADS>     NTRIADS=%d", Wgpl->ntriads));
    send();
    for (tr = 1; tr <= Wgpl->ntriads; tr++) {
      put1triad(tr, lgp);
      per (task, ground, transi) {
	if (Wgpl->triad[tr - 1].present[task])
	  switch (task) {
	    case ground: bufp += slprintf(sprintf(&buf[bufp], "  GROUND")); break;
            case excite: bufp += slprintf(sprintf(&buf[bufp], "  EXCITE")); break;
	    case transi: bufp += slprintf(sprintf(&buf[bufp], "  TRANSI")); break;
	    default:  Caseerror(Line);
	  }
      }
      send();
      /* OUTPUT of J3S to DISK
         J3 := J3HEAD;
         While J3<>NIL do With J3^ do begin
         write(BUF,' ':10);
         for BOK := BRA to KET do begin
           PUTBRANCH(BRANCH[BOK],LGP); PUT_CHAR(';') end;
         PUTPRODMULT(GPTRMULT,CHAIN[1]^.SLOT);
         write(BUF,' = ');
         PUT_C_NUMBER(THREEJ);
         SEND_DISK;
         J3 := J3^.NEXT;
         OUTPUT of J3 to DISK. if NEEDED REMOVE COMMENT BRACKETS */
    }
  }
  statistics("PUTJ3S");
  send();
  send();
}

void putbranches(lgp)
linktype lgp;
{
  irreptype gpir;
  statetype state;
  tasktype task;
  jobtype job;
  p_branch branch;
  integer lmult;

  {
    p_groupx Wgpl = chain[lgp];

    send();
    bufp += slprintf(sprintf(&buf[bufp], " <REPRESENTATIONS>      NREPS=%d", Wgpl->nreps));
    send();
    for (gpir = 0; gpir <= Wgpl->nreps; gpir++) {
      irx_ *Wgpir = &Wgpl->ir[gpir];

      if (Wgpir->branchhead != NIL) {
        send();
        bufp += slprintf(sprintf(&buf[bufp], " IRREP"));
        putirrep(gpir, Wgpl->slot);
        bufp += slprintf(sprintf(&buf[bufp], "    (DIM=%ld)   ", Wgpl->dimen[gpir]));
        per(task, ground, transi) {
	  per (job, basis, opera) {
  	    if (Wgpir->present[task][job]) {
	      switch (task) {
	        case ground: bufp += slprintf(sprintf(&buf[bufp], " GROUND-")); break;
  	        case excite: bufp += slprintf(sprintf(&buf[bufp], " EXCITE-")); break;
	        case transi: bufp += slprintf(sprintf(&buf[bufp], " TRANSI-")); break;
	        default: Caseerror(Line);
	      }
	      switch (job) {
	        case basis:
	          bufp += slprintf(sprintf(&buf[bufp], " BASIS (MULT=%ld) ",Wgpir->mult[task]));
	          break;
	        case opera: bufp += slprintf(sprintf(&buf[bufp], " OPERA  ")); break;
	        default:    Caseerror(Line);
	      }
	    }
          }
        }
        send();
        branch = Wgpir->branchhead;
        while (branch != NIL) {
          bufp += slprintf(sprintf(&buf[bufp], "      BRANCH:"));
          putbranch(branch, lgp);
          per(state, ground, excite) {
	    lmult = qx(Wgpir->mult[state]);
	    if (branch->gpmult[state] == 0)
              bufp += slprintf(sprintf(&buf[bufp], " %*s",16 + 2 * (int)lmult,""));
	    else
	      bufp += slprintf(sprintf(&buf[bufp], "      %-6s %*ld to %*ld", tasktext[state],
                 (int)lmult, branch->start[state],
                 (int)lmult, branch->start[state] + branch->gpmult[state] - 1));
          }
          send();
          branch = branch->next;
        }
      }
    }
    statistics("PUTBRANCHES");
    send();
    send();
  }
}

void putoper(oper)
p_oper oper;
{
  tasktype task;

  {
    bufp += slprintf(sprintf(&buf[bufp], "       <OPER> %-16s", oper->name));
    bufp += slprintf(sprintf(&buf[bufp], "  STRENGTH ")); putcomplex(oper->strength);
    bufp += slprintf(sprintf(&buf[bufp], "  BRANCH:"));
    putbranch(oper->branch, lpoint);
    send();
  }
}

void putactor(actor)
p_actor actor;
{
  p_oper oper;
  purptype purps;

  {
    send();
    bufp += slprintf(sprintf(&buf[bufp], "  %s %-15s  %s","<ACTOR>",actor->name,"IRREP"));
    putirrep(actor->pgir, chain[lpoint]->slot);
    bufp += slprintf(sprintf(&buf[bufp], "  %s ", tasktext[actor->task]));
    per(purps, printraw, printtrans) {
      if (actor->purp[purps]) {
	put_name(purptext[purps]);
	if (actor->mode[purps] != 0) 
	  bufp += slprintf(sprintf(&buf[bufp], "  (MODE=%ld)", actor->mode[purps]));
      }
    }
    send();
    oper = actor->operhead;
    while (oper != NIL) {
      putoper(oper);
      oper = oper->next;
    }
  }
}

void putactors()
{
  p_actor actor;

  send();
  bufp += slprintf(sprintf(&buf[bufp], " ----  ACTORS   ----"));
  send();
  actor = actorhead;
  while (actor != NIL) {
    putactor(actor);
    actor = actor->next;
  }
}

void putredmat()
{
  p_redmat redmat;
  p_matel matel;
  braoprkettype bok;

  redmat = redmathead;
  while (redmat != NIL) {
    put_integer((unsigned) (redmat->task));
    put_name(redmat->name);
    putprodmult(redmat->gptrmult, chain[1]->slot);
    per (bok, bra, ket) putirrep(redmat->ir[bok], chain[1]->slot);
    send_disk();
    matel = redmat->matelhead;
    while (matel != NIL) {
      putcomplex(matel->complex);
      matel = matel->next;
    }
    send_disk();
    redmat = redmat->next;
  }
}

void extendgroup(link, group)
linktype link;
grouptype group;
/* TRANSFER of DATA from"GPDATA" to"GROUPX"
   to MAKE"BUTLER" SOMEWHAT INDEPENDENT of the REST */
{
  p_groupx gpx;
  irreptype gpir;
  tasktype task;
  statetype state;
  jobtype job;

  gpx = NEW(groupxtype);
  chain[link] = gpx;
  {
    gpdata_ *Wgp = &gpdata[group];

    gpx->slot = group;
    gpx->nreps = Wgp->last_rep;
    gpx->lasttrmult = Wgp->last_trmult;
    if (link == 1)
      gpx->lastbrmult = 0;
    else
      gpx->lastbrmult = branchdata.last_brmult;
    for (gpir = 0; gpir <= gpx->nreps; gpir++) {
      gpx->dimen[gpir] = Wgp->dimen[gpir];
      gpx->co[gpir] = Wgp->conj[gpir];
      per(state, ground, excite)
	gpx->ir[gpir].mult[state] = 0;
      gpx->ir[gpir].branchhead = NIL;
    }
  }
}


void readredmat(branchpresent)
branchpresent_ *branchpresent;
{
  statetype state;
  integer gpmult, qnumber = 0;
  irreptype gpir;
  p_groupx gp;
  grouptype slot;
  p_matel matel, startmatel;
  p_redmat redmat, startredmat;
  unsigned short icol, ncol, nrow, dim, jcol, irow, row;
  in_file_type oldinfile;
  complextype dummy;

  gp   = chain[1];
  slot = gp->slot;
  startredmat = NEW(redmattype);
  redmat      = startredmat;
  startmatel  = NEW(mateltype);
  oldinfile   = in_file;
  if (!racer) {
    in_file = disk_one;
    open_in_file( rme_name);
  } else
    in_file = disk_two;
  {
    type_in_record *Win = &in_record[in_file];

    get_item(ask_word);
    do {
      if (in_eq("IRREP")) {
	get_item(ask_word);
	if 	(in_eq("GROUND")) 	state = ground;
	else if (in_eq("EXCITE"))	state = excite;
	else  warninput("GROUND/EXCITED");
	gpir = getirrep(slot);
	get_item(ask_word);
	if (in_eq("MULT")) gpmult = get_integer();
	else warninput("-> MULT");
	(*branchpresent)[gpir][state][basis] = true;
	gp->ir[gpir].mult[state] = gpmult;
	do {
	  skip();
	} while (!(Win->in_buff[Win->in_index - 1] == '%'));
	get_item(ask_word);
      } else if (in_eq("RME")) {
        /* FORMAT:  TASK IRREP IRREP IRREP (PRODMULT) NAME NROW NCOL */
	redmat->next = NEW(redmattype);
	redmat = redmat->next;
        qnumber++; /* Racer */
	{
          redmat->sequence = qnumber; /* Racer */
	  get_item(ask_word);
	  if      (in_eq("GROUND")) redmat->task = ground;
	  else if (in_eq("EXCITE")) redmat->task = excite;
	  else if (strncmp(Win->in_word,"TRANSI",6)==0)
	    			    redmat->task = transi;
	  else
	    warninput("TASK for REDMAT");
	  redmat->ir[bra] = getirrep(slot);
	  redmat->ir[opr] = getirrep(slot);
	  (*branchpresent)[redmat->ir[opr]][redmat->task][opera] =
           true;
	  redmat->ir[ket] = getirrep(slot);
	  redmat->gptrmult = getprodmult(slot);
	  get_item(ask_string);
	  if (Win->in_item == word)
	    wordcpy( redmat->name, Win->in_word );
	  else
	    warninput("NAME for REDMAT");
	  get_item(ask_free);
	  if (Win->in_item == number) nrow = Win->in_number;
	  get_item(ask_free);
	  if (Win->in_item == number) ncol = Win->in_number;
	  /* Throw away the number of elements in the matrix */
	  (void) get_integer(); 
	  matel = startmatel;
          /* FORMAT:  (IROW (ICOL COMPLEX)) */
	  irow = 0;
	  do {
	    get_item(ask_free);
	    if (Win->in_item == number) {
	      row = Win->in_number;
	      if (row < 1 || row > nrow) {
		bufp += slprintf(sprintf(&buf[bufp], " I needed a row number in 1..%d",nrow));
		put_input();
		send_warn();
	      }
	      if (!racer) {
		for (irow++; irow < row; irow++) {
		  for (jcol = 1; jcol <= ncol; jcol++) {
		    matel->next = NEW(mateltype);
		    matel = matel->next;
		    matel->complex = (comzero);
		  }
		}
		/* irow is now equal to the last row read */
	      }
	      /* Throw away the number of elements in the row and the ',' */
	      (void) get_integer();
	      get_item(ask_free);
	      icol = 0;
	      do {
		jcol = icol + 1;
		icol = get_integer();
		if ((icol < jcol) || (icol > ncol)) {
		  bufp += slprintf(sprintf(&buf[bufp], " I expected a column number in the range %d..%d,",
			jcol, ncol));
		  put_input();
		  send_warn();
		}
		if (racer)
		  getcomplex(&dummy);
		else {
		  for (jcol = jcol; jcol <= icol; jcol++) {
		    matel->next = NEW(mateltype);
		    matel = matel->next;
		    matel->complex = (comzero);
		  }
		  getcomplex(&matel->complex);
		}
		if (Win->in_item != end_of_statement)
		  get_item(ask_free);
	      } while (Win->in_item != end_of_statement);
	      if (!racer) {
		if (icol < ncol) {
		  for (jcol = icol + 1; jcol <= ncol; jcol++) {
		    matel->next = NEW(mateltype);
		    matel = matel->next;
		    matel->complex = (comzero);
		  }
		}
	      }
	    }
	  } while (Win->in_item != word);
	  if (!racer) {
	    for (irow++; irow <= nrow; irow++) {
	      for (jcol = 1; jcol <= ncol; jcol++) {
		matel->next = NEW(mateltype);
		matel = matel->next;
		matel->complex = (comzero);
	      }
	    }
	  }
	  matel->next = NIL;
	  redmat->matelhead = startmatel->next;
	}
      } else if (!in_eq("FINISHED")) {
	warninput("IRREP or RME");
	get_item(ask_word);
      }
    } while (  !(member2( Win->in_item, end_of_file, end_of_statement)
              || (in_eq("FINISHED"))));
  }
  redmat->next = NIL;
  redmathead = startredmat->next;
  if (!racer)
    close_in_file();
  in_file = oldinfile;
}

void extendbranches(lgp, lsg)
linktype lgp, lsg;
/* EXTEND BRANCH TREE with LABELS for this LINK (SUBGROUP) */
{
  p_groupx gp, sg;
  statetype state;
  tasktype task;
  jobtype job;
  p_branch startbranch, gpbranch, prevbranch, sgbranch;
  irreptype gpir, sgir;
  multtype br;
  boolean needed;
  branchpresent_ branchpresent;

  gp = chain[lgp];
  sg = chain[lsg];
  per(sgir, 0, sg->nreps)  per(task, ground, transi) per(job, basis, opera)
    branchpresent[sgir][task][job] = false;

  if (lsg == 1) {
    /* A BRANCH is CREATED for EACH IRREP PRESENT in the
       REDUCED MATRIX INPUT from DISK2 */
    readredmat(branchpresent);
    for (sgir = 0; sgir <= sg->nreps; sgir++) {
      irx_ *Wsgir = &sg->ir[sgir];

      needed = false;
      per(task, ground, transi) {
	per(job, basis, opera) {
          if (branchpresent[sgir][task][job])
	    needed = true;
	}
      }
      if (needed) {
	Wsgir->branchhead = NEW(branchtype);
	{
          p_branch br_head = Wsgir->branchhead;

	  br_head->ir = sgir;
	  br_head->brmult = 0;
	  br_head->parent = NIL;
	  br_head->gpir = sgir;
	  per(state, ground, excite) {
	    br_head->start[state] = 1;
	    if (!Wsgir->present[state][basis])
	      sg->ir[sgir].mult[state] = 0;
	    br_head->gpmult[state] = sg->ir[sgir].mult[state];
	    maxdim = max(maxdim, sg->ir[sgir].mult[state]);
            if (state == ground)
              maxbra = max(maxbra, sg->ir[sgir].mult[state]);
	  }
	  br_head->next = NIL;
	}
      }
    }
  }
  if (lsg > 1) {
    startbranch = NEW(branchtype);
    {
      per(state, ground, excite) {
	startbranch->gpmult[state] = 0;
	startbranch->start[state] = 1;
      }
    }
    /* LOOP OVER ALL BRANCHINGS */
    for (sgir = 0; sgir <= sg->nreps; sgir++) {
      sgbranch = startbranch;
      for (gpir = 0; gpir <= gp->nreps; gpir++) {
	for (br = 0; br <= branchmult(gpir, sgir); br++) {
	  per(task, ground, transi) {
	    per(job, basis, opera) {
	      if (gp->ir[gpir].present[task][job])
		branchpresent[sgir][(task)][(job)] = true;
	    }
	  }
	  gpbranch = gp->ir[gpir].branchhead;
	  while (gpbranch != NIL) {
	    prevbranch = sgbranch;
	    sgbranch = NEW(branchtype);
	    prevbranch->next = sgbranch;
	    *sgbranch = *gpbranch;
	    sgbranch->ir = sgir;
	    sgbranch->brmult = br;
	    sgbranch->parent = gpbranch;
	    per(state, ground, excite) {
	      if (!sg->ir[sgir].present[state][basis])
		sgbranch->gpmult[state] = 0;
                /* the IRREP WAS DELETED BY INPUT */
	      sgbranch->start[(state)] = prevbranch->start[(state)]
                                            + prevbranch->gpmult[state];
	      sg->ir[sgir].mult[(state)] = sg->ir[sgir].mult[(state)]
                                                + sgbranch->gpmult[(state)];
	      maxdim = max(maxdim, sg->ir[sgir].mult[state]);
              if (state == ground)
                maxbra = max(maxbra, sg->ir[sgir].mult[state]);
	    }
	    gpbranch = gpbranch->next;
	  }
	}
      }
      sgbranch->next = NIL;
      sg->ir[sgir].branchhead = startbranch->next;
    }
  }
  per(task, ground, transi) {
    per(job, basis, opera) {
      for (sgir = 0; sgir <= sg->nreps; sgir++) {
	{
	  irx_ *Wsgir = &sg->ir[sgir];

	  if ((!Wsgir->present[task][job] &&
               branchpresent[sgir][task][job])) {
	    bufp += slprintf(sprintf(&buf[bufp], " IRREP DELETED BY INPUT:"));
	    bufp += slprintf(sprintf(&buf[bufp], " %7.16s%6.16s", tasktext[task], jobtext[job]));
	    putirrep(sgir, sg->slot);
	    send();
	  } {
	    irx_ *Wsgir = &sg->ir[sgir];

	    Wsgir->present[task][job] = (boolean)
                (Wsgir->present[task][job] &&
                branchpresent[sgir][task][job]);
	  }
	}
      }
    }
  }
  bufp += slprintf(sprintf(&buf[bufp], " For %.16s MAXDIM is now ", gpdata[sg->slot].ext_name));
  maxbra = maxdim; 
  /* there is a bug in transformation of matrices beacuse */
  /* it is asumed that matrix a  is  maxbra * maxdim */
  put_integer(maxdim);
  send_mess();
}

void extendj3s(lgp, lsg)
linktype lgp, lsg;
/* PRODUCE  ALL NECESSARY 3J SYMBOLS (J3), USING  */
/*   the 3J SYMBOLS UP to  the PREVIOUS LINK      */
{
  p_groupx 	gp, sg;
  irreptype 	gpir[3], sgir[3],  gpbra, gpopr, gpket,  sgket, sgopr, sgbra;
  multtype 	brm[3],  brmket, brmopr, brmbra, gptrmult, sgtrmult;
  p_j3 		startj3, gpj3, sgj3, sgj3mult0;
  ntriadtype 	sgtr, gptr;
  p_branch 	branch, branch1[3];
  braoprkettype bok;
  triadtype 	tr0;
  threejmtype 	t0;
  c_number 	factor;
  boolean 	multsum;
  tasktype 	task;

  gp = chain[lgp];
  sg = chain[lsg];
  sg->ntriads = 0;
  startj3 = NEW(j3type);

  for (sgbra = 0; sgbra <= sg->nreps; sgbra++) 
  if( sg->ir[sgbra].present[ground][basis]
  ||  sg->ir[sgbra].present[excite][basis]) {

  for (sgopr = 0; sgopr <= sg->nreps; sgopr++) 
  if( sg->ir[sgopr].present[ground][opera]
  ||  sg->ir[sgopr].present[excite][opera]
  ||  sg->ir[sgopr].present[transi][opera]) {

  for (sgket = 0; sgket <= sg->nreps; sgket++) 
  if( sg->ir[sgket].present[ground][basis]
  ||  sg->ir[sgket].present[excite][basis]) {

	for (sgtrmult = 0; sgtrmult <= sg->lasttrmult; sgtrmult++) {
	  sgir[bra] = sgbra;
	  sgir[opr] = sgopr;
	  sgir[ket] = sgket;
	  storetriad(&tr0, sg->co[sgbra], sgopr, sgket, sgtrmult, 0);
	  {
	    sgtr = sg->ntriads + 1;
	    sgj3 = startj3;
	    {
	      triadx_ *Wsgtr = &sg->triad[sgtr - 1];

	      Wsgtr->ir[bra] = sgbra;
	      Wsgtr->ir[opr] = sgopr;
	      Wsgtr->ir[ket] = sgket;
	      Wsgtr->trmult = sgtrmult;
	      Wsgtr->present[ground] = (boolean) 
                (sg->ir[sgbra].present[ground][basis] &&
                 sg->ir[sgopr].present[ground][opera] &&
                 sg->ir[sgket].present[ground][basis]);
	      Wsgtr->present[excite] = (boolean)
                (sg->ir[sgbra].present[excite][basis] &&
                 sg->ir[sgopr].present[excite][opera] &&
                 sg->ir[sgket].present[excite][basis]);
	      Wsgtr->present[transi] = (boolean)
                (sg->ir[sgbra].present[ground][basis] &&
                 sg->ir[sgopr].present[transi][opera] &&
                 sg->ir[sgket].present[excite][basis]);
	      if (Wsgtr->present[ground] ||
                  Wsgtr->present[excite] || 
                  Wsgtr->present[transi])
		if (findtriad(&tr0, sg->slot))
		  if (sg->ntriads >= maxtriads) {
		    bufp += slprintf(sprintf(&buf[bufp], " MAXTRIADS TOO small"));
		    send_warn();
		  } else if (lsg == 1) { /* THIS SUBGROUP TRIAD is NEEDED */
		    sgj3->next = NEW(j3type);
		    sgj3 = sgj3->next;
		    {
		      per(bok, bra, ket) 
		        sgj3->branch[bok] = sg->ir[sgir[bok]].branchhead;
		      sgj3->gptrmult = sg->triad[sgtr - 1].trmult;
		      sgj3->threej = c_one;
		    }
		  } else if (lsg > 1) {
                    /* JUST AS AN IRREP BRANCHES to SUBGROUP IRREPS, A TRIAD
                       BRANCHES to SUBGROUP TRIADS. EACH TRIAD BRANCHING
                       GIVES A 3JM FACTOR */
		    for (gptr = 1; gptr <= gp->ntriads; gptr++) {
		      {
			triadx_ *Wgptr = &gp->triad[gptr - 1];

		        gpbra = Wgptr->ir[bra];
			gpopr = Wgptr->ir[opr];
			gpket = Wgptr->ir[ket];
			gptrmult = Wgptr->trmult;
		      }
	              multsum = (boolean) ((gptrmult > 0) && (lgp != 1));
		      if (multsum)
			sgj3 = sgj3mult0;
		      else
			sgj3mult0 = sgj3;
		      per (brmbra, 0, branchmult(gpbra, sgbra))
		      per (brmopr, 0, branchmult(gpopr, sgopr))
		      per (brmket, 0, branchmult(gpket, sgket)) {
			store3jm(&t0, gp->co[gpbra], gpopr, gpket,
                                 sg->co[sgbra], sgopr, sgket, gptrmult,
                                 sgtrmult, brmbra, brmopr, brmket, 0,
                                 false, c_unknown);
			find3jm(&t0, group, subgroup);
			if ((boolean) ((find2jm(gpbra, sgbra)) & 1))
			  factor = c_minus(t0.value3);
			else
			  factor = t0.value3;
			brm[bra] = brmbra; brm[opr] = brmopr; brm[ket] = brmket;
			gpir[bra] = gpbra;
			gpir[opr] = gpopr;
			gpir[ket] = gpket;
                        /* NOW LOOK UP BRANCHES in the LIST WHICH
                        CORRESPOND to OUR THREE IRREP BRANCHINGS. ALL
                        BRANCHES CORRESPONDING to ONE IRREP BRANCHING,
                        BUT with DIFFERENT HIGHER GROUP LABELS, ARE
                        CONTIGUOUS in the LIST."BRANCH1" is the START */
		        per(bok, bra, ket) {
			  branch = sg->ir[sgir[bok]].branchhead;
			  while (branch->parent->ir != gpir[bok])
			    branch = branch->next;
			  while (branch->brmult != brm[bok])
			    branch = branch->next;
			  branch1[bok] = branch;
			}
			gpj3 = gp->triad[gptr - 1].j3head;
			while (gpj3 != NIL) {
			  if (multsum) {
			    sgj3 = sgj3->next;
			    sgj3->threej = c_add(sgj3->threej,
                                               c_mult(factor, gpj3->threej));
			  } else {
                            /* FIND the ACTUAL BRANCHES for THIS J3 */
			    sgj3->next = NEW(j3type);
			    sgj3 = sgj3->next;
			    per(bok, bra, ket) {
			      branch = branch1[bok];
			      while (branch->parent != gpj3->branch[bok])
				branch = branch->next;
			      sgj3->branch[bok] = branch;
			    }
			    sgj3->gptrmult = gpj3->gptrmult;
			    sgj3->threej = c_mult(factor, gpj3->threej);
			  }
			  gpj3 = gpj3->next;
			}
		      }
	            }
                  }
	      sgj3->next = NIL;
              sg->triad[sgtr - 1].j3head = startj3->next;
	      if (startj3->next != NIL)
	        sg->ntriads = sgtr;
	    }
	  }
	}
      }
    }
  }
}

void extendpresent(link)
linktype link;
/* READS WHICH IRREPS of the SUBGROUP"LINK" ARE to BE INCLUDED in the
   CALCULATIONS AS BASIS FUNCTIONS or AS OPERATORS in the"GROUND" or"EXCITE"
   STATE or AS A"TRANSI" OPERATOR. if A (TASK,JOB) COMBINATION is not GIVEN
   then ALL IRREPS ARE USED for THAT COMBINATION.
   FORMAT:  (TASK (JOB (IRREP))) */
{
  irreptype ir1;
  tasktype task;
  jobtype job;

  {
    type_in_record *Win = &in_record[in_file];
    p_groupx Wlink = chain[link];

    for (ir1 = 0; ir1 <= Wlink->nreps; ir1++) {
      per(task, ground, transi) {
	per(job, basis, opera)
	  Wlink->ir[ir1].present[task][job] = true;
      }
    }
    while (Win->in_item == word) {
      if ((in_eq("GROUND")))		task = ground;
      else if ((in_eq("EXCITE")))	task = excite;
      else if ((in_eq("TRANSI")))	task = transi;
      else if ((in_eq("BASIS")) || (in_eq("OPERA"))) {
	if (in_eq("BASIS"))
	  job = basis;
	else
	  job = opera;
        for (ir1 = 0; ir1 <= Wlink->nreps; ir1++)
	  Wlink->ir[ir1].present[task][job] = false;
	if ((task == transi) && (job == basis)) {
	  bufp += slprintf(sprintf(&buf[bufp], " TRANSI/BASIS is A SENSELESS COMBINATION."));
	  put_input();
	  send_warn();
	}
      } else
	Wlink->ir[findlabel(Win->in_word,
         Wlink->slot, false)].present[task][job] = true;
      get_item(ask_string);
    }
    get_item(ask_string);
  }
}

p_branch getbranch();

multtype get1brmult(gp)
p_groupx gp;
/* GETS the BRANCHING MULTIPLICITY from the IN_RECORD. */
{
  multtype result;

  result = 0;
  {
    type_in_record *Win = &in_record[in_file];

    if (Win->in_item != end_of_statement) {
      if (internal || (gp->lastbrmult > 0)) {
	get_item(ask_free);
	if ((Win->in_number >= 0) && (Win->in_number < gp->lastbrmult)) {
	  result = Win->in_number;
	} else
	  warninput("BRANCHING MULT");
      }
    }
  }
  return result;
}

p_branch getbranch(lpoint)
linktype lpoint;
/* GETS A BRANCH from INPUT and LOCATES IT in the BRANCHES LIST.
   FORMAT:  IRREP > (BRMULT) IRREP...IRREP */
{
  p_groupx gp;
  linktype lgp;
  irreptype gpir, sgir;
  multtype brm;
  p_branch gpbranch, sgbranch;
  boolean found;

  {
    type_in_record *Win = &in_record[in_file];

    for (lgp = 1; lgp <= lpoint; lgp++) {
      gp = chain[lgp];
      if (lgp == 1) {
	gpir = getirrep(gp->slot);
	gpbranch = gp->ir[gpir].branchhead;
      } else {
	get_item(ask_char);
	if (Win->in_char != '>')
	  warninput("RIGHT BRACKET >");
	brm = get1brmult(gp);
	sgir = getirrep(gp->slot);
	sgbranch = gp->ir[sgir].branchhead;
	found = false;
	if (sgbranch != NIL)
	  do {
	    found = (boolean) ((sgbranch->parent == gpbranch) &&
                               (sgbranch->brmult == brm));
            if (!found)
	      sgbranch = sgbranch->next;
	  } while (!(found || (sgbranch == NIL)));
	gpbranch = sgbranch;
	if (!found) {
	  bufp += slprintf(sprintf(&buf[bufp], " THIS BRANCH COULD NOT BE CONSTRUCTED"));
          bufp += slprintf(sprintf(&buf[bufp], " from the IRREPS ON DISK2. IT WILL BE IGNORED"));
	  put_input();
	  send_mess();
	}
      }
    }
  }
  return gpbranch;
}

void geteos()
{
    type_in_record *Win = &in_record[in_file];

    while (!(member2( Win->in_item, end_of_file, end_of_statement) ) ) {
      get_item(ask_free);
      if (Win->in_item != end_of_statement)
	warninput("END of STATEMENT");
    }
}

void readactorsandopers(lpg)
linktype lpg;
{
  p_actor actor, startactor;
  p_oper oper, startoper;
  purptype purps1, purps;
  word_type actorname, opername;
  boolean found;

  startactor = NEW(actortype);
  actor = startactor;
  actor->next = NIL;
  startoper = NEW(opertype);
  geteos();
  {
    type_in_record *Win = &in_record[in_file];

    do {
      get_item(ask_word);
      if (in_eq("ACTOR")) {
        /* FORMAT:"ACTOR" IRREP NAME TASK (PURP MODE) */
	actor->next = NEW(actortype);
	actor = actor->next;
	actor->next = NIL;
        per(purps, printraw, printtrans) {
	  actor->purp[purps] = false;
	  actor->mode[purps] = 0;
	}
	actor->matrixhead = NIL;
	actor->operhead   = NIL;
	oper = startoper;
	oper->next  = NIL;
	actor->pgir = getirrep(chain[lpg]->slot);
	get_item(ask_string);
	if (Win->in_item == word)
	  wordcpy( actor->name, Win->in_word);
	else
	  warninput("ACTOR NAME");
	get_item(ask_word);
	if (in_eq("GROUND"))		actor->task = ground;
	else if (in_eq("EXCITE"))	actor->task = excite;
	else if (in_eq("TRANSI"))	actor->task = transi;
	else	warninput("TASK for ACTOR");
	if (!chain[lpg]->ir[actor->pgir].present[actor->task][opera]) {
	  bufp += slprintf(sprintf(&buf[bufp], " an ACTOR of this IR and TASK can not "));
          bufp += slprintf(sprintf(&buf[bufp], " be constructed from the OPERS on DISK2."));
	  put_input();
	  send_mess();
	}
	do {
	  get_item(ask_free);
	  if (Win->in_item != end_of_statement) {
	    if (Win->in_item == number)
	      actor->mode[purps] = Win->in_number;
	    else {
	      found = false;
   	      per(purps1, printraw, printtrans) {
		if (in_eq( purptext[purps1]) ) {
		  found = true;
		  purps = purps1;
		}
	      }
	      if (!found) warninput("PURP or MODE");
	      actor->purp[purps] = true;
	      if (purps == printeig  ) actor->purp[diag] = true;
	      if (purps == printtrans) actor->purp[trans] = true;
	    }
	  }
	} while (!(Win->in_item == end_of_statement));
      } else if (in_eq("OPER")) {
        /* FORMAT:"OPER" NAME */
	get_item(ask_string);
	wordcpy( opername,  Win->in_word);
	geteos();
      } else if (in_eq("BRANCH")) {
        /* FORMAT:"BRANCH" IRREP ( > (BRMULT) IRREP) STRENGTH */
	oper->next = NEW(opertype);
	oper = oper->next;
	wordcpy( oper->name, opername );
	oper->next = NIL;
	actor->operhead = startoper->next;
	oper->branch = getbranch(lpg);
	if (oper->branch->ir != actor->pgir)
	  warninput("ACTOR-S  IRREP");
	if (!chain[1]->ir[oper->branch->gpir].
             present[actor->task][opera]) {
	  bufp += slprintf(sprintf(&buf[bufp], " an OPER of the SUPER GROUP SYMMETRY"));
	  putirrep(oper->branch->gpir, chain[1]->slot);
	  bufp += slprintf(sprintf(&buf[bufp], "  and task %s ",tasktext[actor->task]));
	  bufp += slprintf(sprintf(&buf[bufp], " is not present on DISK2."));
	  put_input();
	  send_mess();
	}
	getcomplex(&(oper->strength));
	geteos();
      } else if (Win->in_item != end_of_file && (!in_eq("RUN")) )
	  warninput("\"ACTOR\" \"OPER\" or \"BRANCH\"");
    } while (Win->in_item != end_of_file && (!in_eq("RUN")) );
  }
  actorhead = startactor->next;
}

void clearmatrix(a, nbra, nket, square)
squaretype a;
const dimensiontype nbra, nket;
const boolean square;
{
  dimensiontype ibra, iket, mbra;

  mbra = (square) ? maxdim : maxbra;
  for (ibra = 0; ibra < mbra; ibra++) for (iket = 0; iket < maxdim; iket++)
     A(ibra,iket) = comlarge;
  for (ibra = 0; ibra < nbra; ibra++) for (iket = 0; iket < nket; iket++)
     A(ibra,iket) = comzero;
}

statetype findstate(task, bok)
tasktype task;
braoprkettype bok;
/* SHORTHAND for FINDING WHETHER A SIDE is ASSOCIATED
   With"GROUND" or"EXCITE" */
{
  statetype r_state;

  if ((task==ground) || (task==excite))
    r_state = task;
  if (task == transi)
    if (bok == bra)     
      r_state = ground;
    else if (bok == ket) 
      r_state = excite;
  if (bok == opr)
    warning("Illegal call of function findstate (with bok = opr)");
  return r_state;
}

void dimens(ipgtriad, task, nbra, nket)
ntriadtype ipgtriad;
tasktype task;
dimensiontype *nbra, *nket;
/* SHORTHAND for FINDING the DIMENSIONS of A MATRIX */
{
  {
    p_groupx Wgp = chain[lpoint];

    (*nbra) = Wgp->ir[Wgp->triad[ipgtriad - 1].ir[bra]].
              mult[findstate(task, bra)];
    (*nket) = Wgp->ir[Wgp->triad[ipgtriad - 1].ir[ket]].
              mult[findstate(task, ket)];
  }
}

integer mantisse(scale)
real scale;
{
  if (scale == 0.0)
    return lnumber - 4;
  else
    return max(1, lnumber - 4 - max(0, Trunc(log(fabs(scale)) / log(10.0))) );
}

void   set_putnum_scales(scale)
real scale;
{
    putnum_scales.lfract = mantisse(scale);
    putnum_scales.small  = 0.5 * to_power(10.0, -putnum_scales.lfract);
}

void putnum(x)
real x;
{
  if (x == 0.0) 
    fprintf(logfile,"%*c", lnumber, '0');
  else if (fabs(x) < putnum_scales.small)
    fprintf(logfile,"%*.*s", lnumber, lnumber,"0.0");
  else
    fprintf(logfile,"%*.*f", lnumber, (int)putnum_scales.lfract, x);
}

void printmatrix(a, nbra, nket, task, purp, mode, eigbra, eigket)
squaretype a;
dimensiontype nbra, nket;
tasktype task;
purptype purp;
integer mode;
eigvaltype eigbra, eigket;
{
  boolean complex;
  integer ifirst, ilast, numlin, i, part;
  dimensiontype iket, ibra;
  unsigned char  brafrac, ketfrac;
  real  scale;

  if (!((purp == printeig) && (findmode(mode, 1) == 0))) {
    if (findmode(mode, 1) > 0 && ((task==excite) || (task==ground))) {
      nbra = min(nbra, findmode(mode, 1));
      min(nket, findmode(mode, 1));
    }
    if (findmode(mode, 4) > 0)
       nket = nbra = min(nbra, nket);
    if (findmode(mode, 1) > 0 && (task==transi))
      nbra = min(nbra, findmode(mode, 1));
    if (findmode(mode, 2) == 0) {
      for (ibra = 0; ibra < nbra; ibra++) {
	for (iket = 0; iket < nket; iket++) {
	  complextype *Wbk = &A(ibra,iket);

	  Wbk->r = Wbk->r * Wbk->r + Wbk->i * Wbk->i;
	  Wbk->i = 0.0;
	}
      }
    }
    scale = 0;
    for (ibra = 0; ibra < nbra; ibra++) {
      for (iket = 0; iket < nket; iket++) {
        complextype *Wbk = &A(ibra,iket);

	if (fabs(Wbk->r) > scale) scale = fabs(Wbk->r);
	if (fabs(Wbk->i) > scale) scale = fabs(Wbk->i);
      }
    }
    set_putnum_scales(scale);

    if (eigbra != NIL)
      brafrac = mantisse(fabs(eigbra[1 - 1]) + fabs(eigbra[nbra - 1]));
    if (eigket != NIL)
      ketfrac = mantisse(fabs(eigket[1 - 1]) + fabs(eigket[nket - 1]));
    send();

    bufp += slprintf(sprintf(&buf[bufp], "       ---- MATRIX ----   %s %s",tasktext[task], purptext[purp]));
    send();
    numlin = lline / lnumber - 1;
    for (part = 0; part <= (nket - 1) / numlin; part++) {
      ifirst = part * numlin+1;
      ilast = min(ifirst + numlin - 1, nket);
      send();
      (void) fprintf(logfile,"%*.*s", lnumber, lnumber,"BRA/KET :");
      for (iket = ifirst; iket <= ilast; iket++) {
	if (eigket == NIL)
	  fprintf(logfile," KET:%4ld%*c", iket, lnumber - 9, ' ');
	else
	  fprintf(logfile,"%*.*f", lnumber, ketfrac, eigket[iket - 1]);
      }
      putc('\n', logfile);
      for (i = 1; i <= (ilast - ifirst + 2) * lnumber; i++) putc('-', logfile);
      putc('\n', logfile);


      if (findmode(mode,4) > 0) {
        fprintf(logfile," DIAGVAL %*c", lnumber - 9, ':');
	complex = false;
 	for (ibra = ifirst; ibra <= ilast; ibra++) {
         complextype *Wbkm = &A(ibra-1,ibra-1);

	  putnum(Wbkm->r);
	  if (Wbkm->i != 0.0)
	    complex = true;
	}
	putc('\n', logfile);
	if (complex) {
	  (void) fprintf(logfile,"%*.*s", lnumber, lnumber,"+I*");
	  for (ibra = ifirst; ibra <= ilast; ibra++) {
	    complextype *Wbkm = &A(ibra-1,ibra-1);

	    if (Wbkm->i == 0.0)
	      fprintf(logfile,"%*c", lnumber, ' ');
	    else
	      putnum(Wbkm->i);
	  }
	  putc('\n', logfile);
	}
      }
      else {
        for (ibra = 0; ibra < nbra; ibra++) {
	  complex = false;
	  if (eigbra == NIL)
	    fprintf(logfile," BRA:%4ld%*c", ibra + 1, lnumber - 9, ':');
	  else
	    fprintf(logfile,"%*.*f:", lnumber - 1, brafrac - 1,eigbra[ibra]);
	  for (iket = ifirst; iket <= ilast; iket++) {
	    complextype *Wbkm = &A(ibra,iket - 1);

	    putnum(Wbkm->r);
	    if (Wbkm->i != 0.0)
	      complex = true;
	  }
	  putc('\n', logfile);
	  if (complex) {
	    (void) fprintf(logfile,"%*.*s", lnumber, lnumber,"+I*");
	    for (iket = ifirst; iket <= ilast; iket++) {
	      complextype *Wbkm = &A(ibra,iket - 1);

	      if (Wbkm->i == 0.0)
	        fprintf(logfile,"%*c", lnumber, ' ');
	      else
	        putnum(Wbkm->i);
	    }
	    putc('\n', logfile);
	  }
        }
      }
    }
    send();
  }
}

void printeigenvalues(eps, u, nbra, mode)
realarray eps;
squaretype u;
const dimensiontype nbra;
integer mode;
{
  dimensiontype maxbra, ibra, iket;
  integer ifirst, ilast, numlin, lfract, part;
  real val, maxval;

  send();
  send();
  bufp += slprintf(sprintf(&buf[bufp], "       ---- EIGENVALUES ----"));
  send();
  numlin = lline / lnumber - 1;
  lfract = mantisse(fabs(eps[1 - 1]) + fabs(eps[nbra - 1]));
  for (part = 0; part <= (nbra - 1) / numlin; part++) {
    ifirst = part * numlin + 1;
    ilast = min(ifirst + numlin - 1, nbra);
    send();
    bufp += slprintf(sprintf(&buf[bufp], " %*c", lnumber, ' '));
    for (iket = ifirst; iket <= ilast; iket++)
      bufp += slprintf(sprintf(&buf[bufp], " %*ld   ", lnumber - 3, iket));
    send();
    bufp += slprintf(sprintf(&buf[bufp], " %*.*s", lnumber, lnumber,"KET/PURE"));
    for (iket = ifirst - 1; iket < ilast; iket++) {
      maxval = 0.0;
      for (ibra = 0; ibra < nbra; ibra++) {
	complextype *Wubk = &U(ibra,iket);

        val = Wubk->r * Wubk->r + Wubk->i * Wubk->i;
        if (val > maxval) {
          maxval = val;
          maxbra = ibra + 1;
        }
      }
      bufp += slprintf(sprintf(&buf[bufp], " %*ld%5.2f",lnumber - 5, maxbra, maxval));
    }
    send();
    bufp += slprintf(sprintf(&buf[bufp], " %*.*s", lnumber, lnumber,"EIGVAL"));
    for (iket = ifirst - 1; iket < ilast; iket++)
      bufp += slprintf(sprintf(&buf[bufp], " %*.*f", lnumber, (int)lfract, eps[iket]));
    send();
    send();
  }
  send();
}

void diagon();

void tql(z, d, e, n,ierr)
rmatrixtype z;
realarray d, e;
dimensiontype n;
integer *ierr;
{
#define machep (real)1e-7
#define maxiter 30
  signed short i, j, k, l, m, iter;
  real b, c, f, g, h, p, r, s;

  *ierr = 0;
  if (n > 1) {
    for (i = 1; i < n; i++) e[i - 1] = e[i];
    f = b = e[n - 1] = 0.0;
    for (l = 0; l < n; l++) if ((*ierr) == 0) {
	h = machep * (fabs(d[l]) + fabs(e[l]));
	if (b < h) b = h;
	m = l;
	while (fabs(e[m]) > b) m = m + 1;
	if (m > l) {
	  iter = 0;
	  do {
	    iter = iter + 1;
	    p = (d[l + 1] - d[l]) / 2 / e[l];
	    r = sqrt(p * p + 1.0);
	    if (p < 0.0) r = -r;
	    h = d[l] - e[l] / (p + r);
	    for (i = l; i < n; i++)  d[i] = d[i] - h;
	    f = f + h;
	    p = d[m]; c = 1.0; s = 0.0;
	    for (i = m - 1; i >= l; i--) {
	      g = c * e[i]; h = c * p;
	      if (fabs(p) > fabs(e[i])) {
		c = e[i]/p; r = sqrt(c*c+1); e[i+1] = s*p*r;    s=c/r; c=1/r;
	      } else {
		c = p/e[i]; r = sqrt(c*c+1); e[i+1] = s*e[i]*r; s=1/r; c=c*s;
	      }
	      p = c * d[i] - s * g;
	      d[i + 1] = h + s * (c * g + s * d[i]);
	      for (k = 0; k < n; k++) {
		h = Z(k,i+1); Z(k,i+1)= s*Z(k,i) + c*h; Z(k,i)= c*Z(k,i)-s*h;
	      }
	    }
	    e[l] = s * p;
	    d[l] = c * p;
	  } while (!((fabs(e[l]) < b) || (iter == maxiter)));
	}
	d[l] = d[l] + f;
	if (iter == maxiter) (*ierr) = l;
    }
    /* -------------------- ORDER EIGENVALUES and EIGENVECTORS -------- */
    if ((*ierr) == 0) {
      for (i = 0; i < n - 1; i++) {
	k = i; for (j = i + 1; j < n; j++) { if (d[j] < d[k])  k = j ;}
	if (k != i) {
	  p = d[k]; d[k] = d[i]; d[i] = p;
	  for (j = 0; j < n; j++) {
	    p = Z(j,i); Z(j,i) = Z(j,k); Z(j,k) = p;
	  }
	}
      }
    }
  }
}

void real_diag();

#ifndef CONVEX

void tred(z, d, e, n)
rmatrixtype z;
realarray d, e;
dimensiontype n;
{
  signed short i, j, k;
  real f, g, h, hh, scale;

  if (n != 1) {
    for (i = n - 1; i > 0; i--) {
      h = 0.0; scale = 0.0;
      if (i > 1) { for (k = 0; k < i; k++) scale = scale + fabs(Z(i,k)); }
      if (scale == 0.0)
	e[i] = Z(i,i-1);
      else {
	for (k = 0; k < i; k++) {
	  Z(i,k) /= scale;
	  h += sqr(Z(i,k));
	}
	f = Z(i,i-1);
	if (f < 0) g = sqrt(h); else g = -sqrt(h);
	e[i] = scale * g; h = h - f * g; Z(i,i-1) = f - g; f = 0.0;
	for (j = 0; j < i; j++) {
	  Z(j,i) = Z(i,j) / h / scale;
	  g = 0.0; 
	  for (k = 0; k <= j; k++)    g += Z(j,k) * Z(i,k);
	  for (k = j + 1; k < i; k++) g += Z(k,j) * Z(i,k);
	  e[j] = g / h;
	  f += e[j] * Z(i,j);
	}
	hh = f / h / 2;
	for (j = 0; j < i; j++) {
	  f = Z(i,j); g = e[j] - hh * f; e[j] = g;
	  for (k = 0; k <= j; k++)
	    Z(j,k) -= f * e[k] + g * Z(i,k);
	}
	for (k = 0; k < i; k++)  Z(i,k) *= scale;
      }
      d[i] = h;
    }
  }
  d[0] = e[0] = 0.0;
  for (i = 0; i < n; i++) {
    if (d[i] != 0.0) {
      for (j = 0; j < i; j++) {
	g = 0.0;
	for (k = 0; k < i; k++) g += Z(i,k) * Z(k,j);
	for (k = 0; k < i; k++) Z(k,j) -= g * Z(k,i);
      }
    }
    d[i] = Z(i,i);
    Z(i,i) = 1.0;
    for (j = 0; j < i; j++) { Z(i,j) = Z(j,i) = 0.0; }
  }
}
#endif /* not CONVEX */

void real_diag(u, z, eigval, n)
squaretype u;
rmatrixtype z;
realarray eigval;
dimensiontype n;
{
  integer ierr;
  dimensiontype i, j;
  for (i = 0; i < n; i++)  for (j = 0; j < n; j++)  Z(i,j) = U(i,j).r;
#ifdef CONVEX
  tred2_(&maxdim, &n, z[0], eigval, e, z[0]);
  tql2_ (&maxdim, &n, eigval, e, z[0], &ierr);
#else
  tred(z, eigval, e, n);
  tql (z, eigval, e, n, &ierr);
#endif
  for (i = 0; i < n; i++) for (j = 0; j < n; j++)  {
    U(i,j).r = Z(i,j);
    U(i,j).i = 0.0;
  }
  if (ierr != 0)
    (void) printf("NO CONVERGENCE in DIAGONALIZATION (in TQL)\n");
}

void hermitian_diag();

void htridi(a, d, e, e2, tau1, tau2, n)
squaretype a;
realarray d, e, e2, tau1, tau2;
const dimensiontype n;
/* Reduces a complex hermitian matrix to a real symmetric tridiagonal matrix */
{
  dimensiontype i, j, k;
  signed short l;
  real f, fi, g, gi, h, hh, si, scale;

  tau1[n - 1] = 1.0;
  tau2[n - 1] = 0.0;
  for (i = 0; i < n; i++)  d[i] = A(i,i).r;
  for (i = n - 1; i >= 0; i--) {
    l = i - 1;
    h = scale = 0.0;
    if (l < 0) {
      e[i] = e2[i] = 0.0;
    } else {          /* scale row */
      for (k = 0; k <= l; k++)
        scale += fabs(A(i,k).r) + fabs(A(i,k).i);
      if (scale == 0.0) { tau1[l] = 1.0; tau2[l] = e[i] =  e2[i] = 0.0;
      } else {
	for (k = 0; k <= l; k++) {
	  complextype *Waik = &A(i,k);
	  Waik->r /= scale;
	  Waik->i /= scale;
	  h += sqr(Waik->r) + sqr(Waik->i);
	}
	e2[i] = scale * scale * h;
	g = sqrt(h);
	e[i] = scale * g;
	{
	  complextype *Wail = &A(i,l);
	  f = sqrt(sqr(Wail->r) + sqr(Wail->i));
          /* ... form next diagonal element of T */
          if (f != 0.0) {
 	    tau1[l] = (Wail->i * tau2[i] - Wail->r * tau1[i]) / f;
	    si      = (Wail->r * tau2[i] + Wail->i * tau1[i]) / f;
	    h += f * g;
	    g = 1.0 + g / f;
	    Wail->r *= g; Wail->i *= g;
	  } else {
	    tau1[l] = -tau1[i];
	    si = tau2[i];
	    A(i,l).r = g;
	  }
        }
	if (l != 0) {
	  f = 0.0;
	  for (j = 0; j <= l; j++) {
	    g = gi = 0.0;
            /* ... form element of A*U */
	    for (k = 0; k <= j; k++) {
	      complextype *Wajk = &A(j,k), *Waik = &A(i,k);
	      g +=   Wajk->r * Waik->r + Wajk->i * Waik->i;
	      gi+= - Wajk->r * Waik->i + Wajk->i * Waik->r;
	    }
	    for (k = j + 1; k <= l; k++) {
	      complextype *Wakj = &A(k,j), *Waik = &A(i,k);
	      g +=   Wakj->r * Waik->r - Wakj->i * Waik->i;
	      gi+= - Wakj->r * Waik->i - Wakj->i * Waik->r;
	    }
            /* ... form element of P */
	    e[j]    = g  / h;
	    tau2[j] = gi / h;
	    f += e[j] * A(i,j).r - tau2[j] * A(i,j).i;
	  }
          /* ... form reduced A */
	  hh = f / (h + h);
	  for (j = 0; j <= l; j++) {
	    f  =  A(i,j).r; g  = e[j]   -= hh * f;
	    fi = -A(i,j).i; gi = tau2[j] - hh * fi;
	    tau2[j] = -gi;
	    for (k = 0; k <= j; k++) {
	      A(j,k).r += - f * e[k]    - g * A(i,k).r
                  + fi * tau2[k] + gi * A(i,k).i;
	      A(j,k).i += - f * tau2[k] - g * A(i,k).i
                  - fi * e[k]    - gi * A(i,k).r;
	    }
	  }
	}
	for (k = 0; k <= l; k++) { A(i,k).r *=scale; A(i,k).i *=scale;}
        tau2[l] = -si;
      }
    }
    hh = d[i];
    d[i] = A(i,i).r;
    A(i,i).r = hh;
    A(i,i).i = scale * sqrt(h);
  }
}

void htribk(a, eigvec, tau1, tau2, n)
squaretype a, eigvec;
realarray tau1, tau2;
const dimensiontype n;
/* Transform the eigenvectors of the real symmetric tridiagonal
   matrix to those of the hermitian tridiagonal matrix */
{
  dimensiontype i, j, k, l;
  real h, s, si;

  for (k = 0; k < n; k++) {
  for (j = 0; j < n; j++) {
      Eigvec(k,j).i  = -Eigvec(k,j).r * tau2[k];
      Eigvec(k,j).r *= tau1[k];
    }
  }
  if (n != 1) {        /* ... recover and apply the householder matrices */
    for (i = 1; i < n; i++) {
      h = A(i,i).i;
      if (h != 0.0) {
	for (j = 0; j < n; j++) {
	  s = si = 0.0;
	  for (k = 0; k <i; k++) {
	    s +=  A(i,k).r * Eigvec(k,j).r - A(i,k).i * Eigvec(k,j).i;
	    si += A(i,k).r * Eigvec(k,j).i + A(i,k).i * Eigvec(k,j).r;
	  }
          /* ... double divisions avoid possible underflow */
	  s  = s  / h / h;
	  si = si / h / h;
	  for (k = 0; k <i; k++) {
	    Eigvec(k,j).r += - s  * A(i,k).r - si * A(i,k).i;
	    Eigvec(k,j).i += - si * A(i,k).r + s  * A(i,k).i;
	  }
	}
      }
    }
  }
}

void hermitian_diag(a, z, eigval, n)
squaretype a;
rmatrixtype z;
realarray eigval;
const dimensiontype n;
/* Finds the eigenvalues and eigenvectors of a complex hermitian matrix */
{
  integer ierr;
  dimensiontype i, j;
  static squaretype eigvec = NULL;

  htridi(a, eigval, e, e2, tau1, tau2, n);
  if (eigvec == NULL) {
    eigvec    = (squaretype)    MALLOC(maxdim * sizeof(complextype *));
    eigvec[0] = (complextype *) MALLOC(maxdim * maxdim * sizeof(complextype));
    for (i = 1; i < maxdim; i++) eigvec[i] = eigvec[i - 1] + maxdim;
  }
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++)  Eigvec(i,j).r = 0.0;
    Eigvec(i,i).r = 1.0;
  }
  for (i = 0; i < n; i++) for (j = 0; j < n; j++)  Z(i,j) = Eigvec(i,j).r;

  tql(z, eigval, e, n, &ierr);
  for (i = 0; i < n; i++) for (j = 0; j < n; j++)  Eigvec(i,j).r = Z(i,j);

  if (ierr != 0)
    (void) printf("NO CONVERGENCE in COMPLEX DIAGONALIZATION (in TQL)\n");
  else
    htribk(a, eigvec, tau1, tau2, n);

  for (i = 0; i < n; i++) for (j = 0; j < n; j++)  A(i,j) = Eigvec(i,j);
}

void diagon(u, n, d, irdimen)
squaretype u;
const dimensiontype n;
realarray d;
integer irdimen;
{
  dimensiontype i, j;
  boolean complex;
  integer ierr;
  real dim;

  complex = false;
  dim = sqrt((real) irdimen);
  for (i = 0; i < n; i++)
  for (j = 0; j < n; j++) {
    U(i,j).r /= dim;  
    U(i,j).i /= dim;
    if (U(i,j).i != 0.0)  complex = true;
  }
  if (complex)  hermitian_diag(u, z, d, n); else  real_diag(u, z, d, n);

  statistics("END DIAGON");
}

void findmat(actor, isgtriad, matrix)
p_actor actor;
ntriadtype isgtriad;
p_matrix *matrix;
/* EACH ACTOR HAS A LIST of MATRICES, LABELED BY THEIR"ISGTRIAD"
   THIS Procedure LOCATES A MATRIX in the STORE */
{
  boolean found;
  p_matrix prevmatrix;

  found = false;
  (*matrix) = actor->matrixhead;
  while (((*matrix) != NIL) && (!found)) {
    found = (boolean) ((*matrix)->isgtriad == isgtriad);
    if (!found) {
      prevmatrix = (*matrix);
      (*matrix) = (*matrix)->next;
    }
  }
  if (!found) {
    (*matrix) = NEW(matrixtype);
    {
      if (actor->matrixhead == NIL)
	actor->matrixhead = (*matrix);
      else
	prevmatrix->next = (*matrix);
    }
    (*matrix)->isgtriad = isgtriad;
    (*matrix)->elementhead = NIL;
    (*matrix)->next = NIL;
  }
}

squaretype getmat(a, actor, isgtriad, matrix, nbra, nket)
squaretype a;
p_actor actor;
ntriadtype isgtriad;
p_matrix *matrix;
dimensiontype *nbra, *nket;
/* Finds where A is stored, and copies it */
{
  dimensiontype ibra, iket;
  complextype *element;

  findmat(actor, isgtriad, matrix);
  dimens(isgtriad, actor->task, nbra, nket);
  clearmatrix(a, (*nbra), (*nket), false);
  if ((*matrix)->elementhead != NIL) {
    for (ibra = 0; ibra < (*nket); ibra++) {
      element = (*matrix)->elementhead[ibra];
      memcpy(a[ibra], element, (*nbra) * sizeof(complextype));
    }
    return((*matrix)->elementhead);
  } else
    warning("PROBLEM in GET");
  return(NULL);
}


void storemat(a, actor, isgtriad, matrix)
squaretype a;
p_actor actor;
ntriadtype isgtriad;
p_matrix *matrix;
/* TRANSFERS MATRIX from"A" to STORE */
{
  boolean create;
  complextype *element;
  dimensiontype ibra, iket, nbra, nket;

  findmat(actor, isgtriad, matrix);
  dimens(isgtriad, actor->task, &nbra, &nket);
  create = (boolean) ((*matrix)->elementhead == NIL);
  if (create) {
    (*matrix)->elementhead = (squaretype) MALLOC(nket * sizeof(complextype *));
    element = (complextype *) MALLOC(nbra * nket * sizeof(complextype));
    if (element == NIL)
      warning("PROBLEM in STORE");
    for (ibra = 0; ibra < nket; ibra++) {
      memcpy(element, a[ibra], nbra * sizeof(complextype));
      (*matrix)->elementhead[ibra] = element;
      element += nbra;
    }
  } else {
    for (ibra = 0; ibra < nket; ibra++) {
      element = (*matrix)->elementhead[ibra];
      memcpy(element, a[ibra], nbra * sizeof(complextype));
    }
  }
}

void printtrnsform(a)
squaretype a;
{
  p_actor actor;
  braoprkettype bok;
  p_matrix matrix;
  dimensiontype nbra, nket;

  actor = actorhead;
  {
    p_groupx Wgp = chain[lpoint];

    while (actor != NIL) {
      if (actor->purp[printtrans]) {
	matrix = actor->matrixhead;
	while (matrix != NIL) {
	  getmat(a, actor, matrix->isgtriad, &matrix, &nbra, &nket);
	  send();
	  send();
	  bufp += slprintf(sprintf(&buf[bufp], " TRANSFORMED MATRIX for"));
	  put1triad(matrix->isgtriad, lpoint);
	  bufp += slprintf(sprintf(&buf[bufp], "  (%ld*%ld)", nbra, nket));
	  {
	    triadx_ *Wsgtr = &Wgp->triad[matrix->isgtriad - 1];

 	    bufp += slprintf(sprintf(&buf[bufp], "   DIM"));
  	    per(bok, bra, ket) bufp += slprintf(sprintf(&buf[bufp], " :%ld", Wgp->dimen[Wsgtr->ir[bok]]));
	  }
	  bufp += slprintf(sprintf(&buf[bufp], "    ACTOR %.16s", actor->name));
	  send();
	  printmatrix(a, nbra, nket, actor->task,
		printtrans, actor->mode[printtrans],
                matrix->eigval[bra], matrix->eigval[ket]);
	  matrix = matrix->next;
	}
      }
      actor = actor->next;
    }
  }
  bufp += slprintf(sprintf(&buf[bufp], " TRANSFORMATION FINISHED"));
  send();
  send();
}

void disktrnsform(a)
squaretype a;
{
  p_actor actor;
  braoprkettype bok;
  statetype state;
  p_matrix matrix;
  dimensiontype ibra, iket, nbra, nket;
  irreptype irep;
  integer qi, nol;

  open_out_disk("REDMATAND3JS");
  {
    p_groupx Wgp = chain[lpoint];

    for (state = ground; state <= excite; state++) {
      for (irep = 0; irep <= Wgp->nreps; irep++) {
	irx_ *Wgpir = &Wgp->ir[irep];

	if (Wgpir->present[state][basis]) {
	  bufp += slprintf(sprintf(&buf[bufp], " IRREP %7.16s", tasktext[state]));
	  putirrep(irep, Wgp->slot);
	  bufp += slprintf(sprintf(&buf[bufp], "  MULT %10ld   DIM %5ld",
                          Wgpir->mult[state], Wgp->dimen[irep]));
	  send_disk();
	}
      }
    }
  }
  actor = actorhead;
  {
    p_groupx Wgp = chain[lpoint];

    while (actor != NIL) {
      if (actor->purp[printtrans])
	if (findmode(actor->mode[printtrans], 3) == 1) {
	  matrix = actor->matrixhead;
	  while (matrix != NIL) {
	    getmat(a, actor, matrix->isgtriad, &matrix, &nbra, &nket);
	    send_disk();
	    send_disk();
	    bufp += slprintf(sprintf(&buf[bufp], " REDUCEDMATRIX %.10s",
                            tasktext[actor->task]));
	    {
	      triadx_ *Wsgtr = &Wgp->triad[matrix->isgtriad - 1];

	      for (bok = bra; bok <= ket;
                   bok = (braoprkettype) (bok + 1))
		putirrep(Wsgtr->ir[bok], Wgp->slot);
	      putprodmult(Wsgtr->trmult, Wgp->slot);
	    }
	    qi = max(qx(nbra), qx(nket)) + 1;
	    bufp += slprintf(sprintf(&buf[bufp], " %.16s %*ld%*ld", actor->name, (int)qi, nbra, (int)qi, nket));
	    send_disk();
	    for (ibra = 1; ibra <= nbra; ibra++) {
	      nol = 0;
	      (void) fprintf(disk,"%*ld", (int)qi, ibra);
	      for (iket = 1; iket <= nket; iket++) {
		complextype *Wabmkm = &A(ibra - 1,iket - 1);
		if (Wabmkm->r !=0 || Wabmkm->i !=0 || iket == nket) {
		  fprintf(disk,"%*ld", (int)qi, iket);
		  if (Wabmkm->i == 0) {
		    fprintf(disk," %20e", Wabmkm->r);
		    nol = nol + 1;
		  } else {
		    fprintf(disk," (%20e,%20e)", Wabmkm->r, Wabmkm->i);
		    nol = nol + 2;
		  }
		  if (nol >= 5) {
		  putc('\n', disk);
		  nol = 0;
		}
	      }
	    }
	    putc('\n', disk);
	  }
	  bufp += slprintf(sprintf(&buf[bufp], " END %.16s", actor->name));
	  send_disk();
	  send_disk();
	  matrix = matrix->next;
	}
      }
      actor = actor->next;
    }
  }
  fclose(disk);
}

#ifdef CONVEX

void transform(ap, u, a, side, nbra, nket)
squaretype ap, u, a;
braoprkettype side;
dimensiontype nbra, nket;
{
  complextype alpha = {1.0, 0.0}, beta = {0.0, 0.0};

  if (side == bra) /* U dagger x A */
    zgemm_("ctrans","normal", &nbra, &nket, &nbra, &alpha, u[0], &maxdim,
           ap[0], &nbra, &beta, a[0], &maxbra);
  else /* A x U */
    zgemm_("normal","normal", &nbra, &nket, &nket, &alpha, ap[0], &nbra,
           u[0], &maxdim, &beta, a[0], &maxbra);
}

#else

void transform(ap, u, a, side, nbra, nket)
squaretype ap, a, u;
braoprkettype side;
dimensiontype nbra, nket;
{
  dimensiontype k, iket, ibra;
  real aibrakr, aibraki;

  if (side == bra) {
    /* Use transpose conjugate of A */
    for (ibra = 0; ibra < nket; ibra++) {
      for (iket = 0; iket < nbra; iket++)
        c[iket] = comzero;
      for (k = 0; k < nbra; k++) {
        aibrakr =  A(k,ibra).r;
        aibraki = -A(k,ibra).i;
        for (iket = 0; iket < nbra; iket++) {
          complextype *uiketk = &U(k,iket);

	  c[iket].r += aibrakr * uiketk->r - aibraki * uiketk->i;
	  c[iket].i += aibrakr * uiketk->i + aibraki * uiketk->r;
        }
      }
      for (iket = 0; iket < nbra; iket++) {
         A(iket,ibra).r =  c[iket].r;
         A(iket,ibra).i = -c[iket].i;
      }
    }
  } else {
    for (ibra = 0; ibra < nbra; ibra++) {
      for (iket = 0; iket < nket; iket++)
        c[iket] = comzero;
      for (k = 0; k < nket; k++) {
        aibrakr = A(ibra,k).r;
        aibraki = A(ibra,k).i;
        for (iket = 0; iket < nket; iket++) {
          complextype *uiketk = &U(k,iket);

	  c[iket].r += aibrakr * uiketk->r - aibraki * uiketk->i;
	  c[iket].i += aibrakr * uiketk->i + aibraki * uiketk->r;
        }
      }
      for (iket = 0; iket < nket; iket++) A(ibra,iket) = c[iket];
    }
  }
}

#endif /* CONVEX */

void treatmatrix( a, u, actor, ipgtriad)
 	 squaretype a;
	 squaretype u; 
	 p_actor    actor; 
	 ntriadtype ipgtriad;
/*
void treatmatrix(
 	 squaretype a,
	 squaretype u, 
	 p_actor    actor, 
	 ntriadtype ipgtriad)
*/	 
{
  braoprkettype side;
  dimensiontype nbra, nket, nbra1, nket1, iket, ibra;
  p_groupx pg;
  p_actor actor1;
  p_matrix matrix, matrix1;
  tasktype task;
  boolean complex;
  integer nzero, nreal, ncomplex;
  squaretype transition;
  {
    pg = chain[lpoint];
    task = actor->task;
    dimens(ipgtriad, task, &nbra, &nket);
    nzero = 0;
    nreal = 0;
    ncomplex = 0;
    for (ibra = 0; ibra < nbra; ibra++) {
      for (iket = 0; iket < nket; iket++) {
       	complextype *Wubk = &U(ibra,iket);

        if      (Wubk->i != 0.0) ncomplex = ncomplex + 1;
	else if (Wubk->r != 0.0) nreal = nreal + 1;
	else                         nzero = nzero + 1;
      }
    }
    complex = (boolean) (ncomplex > 0);
    bufp += slprintf(sprintf(&buf[bufp], "       MATRIX HAS %ld ZERO, %ld REAL and %ld COMPLEX ELEMENTS",
           nzero, nreal, ncomplex));
    send();
    if (actor->purp[printraw])
      printmatrix(u, nbra, nket, actor->task, 
           printraw, actor->mode[(printraw)], NIL, NIL);
    if (actor->purp[trans]) {
      /* MATRIX WAITS in STORE. IT WILL BE TRANSFORMED WHEN the
         APROPRIATE MATRICES ARE DIAGONALIZED */
      storemat(u, actor, ipgtriad, &matrix);
      {
	per(side, bra, ket) matrix->eigval[side] = NIL;
      }
    }
    if (actor->purp[diag])
      if (nbra != nket) {
	bufp += slprintf(sprintf(&buf[bufp], " MATRIX is NOT SQUARE and will NOT be DIAGONALIZED"));
	send_warn();
      } else {
	send();
	bufp += slprintf(sprintf(&buf[bufp], " %6cDIAGONALIZATION   %s", ' ',tasktext[actor->task]));
	send();
	diagon(u, nbra, eps,
               pg->dimen[pg->triad[ipgtriad - 1].ir[bra]]);
	memcpy(eigenval, eps, maxdim * sizeof(real));
	if (actor->purp[printeig]) {
	  printeigenvalues(eps, u, nbra, actor->mode[printeig]);
	  printmatrix(u, nbra, nket, actor->task, printeig,
                      actor->mode[printeig], NIL, eigenval);
	}
	actor1 = actorhead;
	while (actor1 != NIL) {
	  matrix1 = actor1->matrixhead;
          /* LOOP THROUGH STORE to FIND MATRICES THAT HAVE to BE TRANSFORMED */
	  while (matrix1 != NIL) {
            /* BOTH SIDES of MATRIX1 HAVE to BE TRANSFORMED if TASK1=TASK. If
               TASK1=TRANSI then ONLY SIDE CORRESPONDING to TASK is TREATED */
	    for (side = bra; side <= ket; side++) {
	      if (side != opr)
		if (findstate(actor1->task, side) == task)
		  if (pg->triad[ipgtriad - 1].ir[side] ==
                      pg->triad[matrix1->isgtriad - 1].ir[side]) {
		    if (debug_butler) {
		      bufp += slprintf(sprintf(&buf[bufp], "   TRANSFORMING the %s SIDE for ",boktext[side]));
		      put1triad(matrix1->isgtriad, lpoint);
		      bufp += slprintf(sprintf(&buf[bufp], "  of ACTOR %.16s", actor1->name));
		      send();
		    }
		    transition =
                      getmat(a,actor1,matrix1->isgtriad,&matrix1, &nbra1, &nket1);
		    transform(transition, u, a, side, nbra1, nket1);
		    storemat(a, actor1, matrix1->isgtriad, &matrix1);
                    if (side == bra) {
                      matrix1->eigval[bra] = (eigvaltype)
                                               MALLOC(nbra * sizeof(real));
		      memcpy(matrix1->eigval[bra], eigenval,
                             nbra * sizeof(real));
                    } else {
                      matrix1->eigval[ket] = (eigvaltype)
                                               MALLOC(nket * sizeof(real));
		      memcpy(matrix1->eigval[ket], eigenval,
                             nket * sizeof(real));
                    }
		  }
	    }
	    matrix1 = matrix1->next;
	  }
	  actor1 = actor1->next;
	}
    }
    statistics("END TREATMATRIX");
  }
  send();
}

void executetask(lpg)
linktype lpg;
{
  p_groupx pg;
  tasktype task;
  statetype stbra, stket;
  p_actor actor;
  p_oper oper;
  ntriadtype ipgtriad;
  p_redmat redmat;
  p_j3 j3;
  dimensiontype nbra, nket, iket, ibra;
  p_matel redmatel;
  boolean diagonalize;

  allocate_matrices();
  send();
  send();
  bufp += slprintf(sprintf(&buf[bufp], " ---- EXECUTE TASK ----"));
  send();
  send();
  pg = chain[lpg];
  {
    per(diagonalize, false, true) {
      /* ALL DIAGONALIZATIONS ARE doNE LAST, WHEN ALL MATRICES THAT 
         HAVE to BE TRANSFORMED ARE ALLREADY in STORE */
      per(task, ground, transi) {
	stbra = findstate(task, bra);
	stket = findstate(task, ket);
	actor = actorhead;
	while (actor != NIL) { 
	  if (actor->task == task && actor->purp[diag] == diagonalize) {
	    send();
	    put_super(actor->name);
	    send();
	    bufp += slprintf(sprintf(&buf[bufp], " CALCULATIONS for ACTOR: %s %s",
                   actor->name, tasktext[task]));
	    send();
	    send();
	    for (ipgtriad = 1; ipgtriad <= pg->ntriads; ipgtriad++) {
	      if (pg->triad[ipgtriad - 1].present[task])
		if (pg->triad[ipgtriad - 1].ir[opr] == actor->pgir) {
		  dimens(ipgtriad, task, &nbra, &nket);
		  bufp += slprintf(sprintf(&buf[bufp], " %6cCALCULATING MATRIX for", ' '));
		  put1triad(ipgtriad, lpg);
		  bufp += slprintf(sprintf(&buf[bufp], "   (%ld*%ld)", nbra, nket));
		  if (debug_butler)
		    bufp += slprintf(sprintf(&buf[bufp], "   ADDING MATRICES of the FOLLOWING OPERATORS"));
		  send();
		  clearmatrix(u, nbra, nket, true);
		  oper = actor->operhead;
		  while (oper != NIL) {
		    if (debug_butler) putoper(oper);
		    redmat = redmathead;
		    while (redmat != NIL) {
		      if ((redmat->task == task) &&
                          (redmat->ir[opr] == oper->branch->gpir) &&
                          (streq(redmat->name, oper->name) )) {
			j3 = pg->triad[ipgtriad - 1].j3head;
			while (j3 != NIL) {
			  if (j3->branch[opr] == oper->branch) 
			  if (j3->branch[bra]->gpir == redmat->ir[bra])
			  if (j3->branch[ket]->gpir == redmat->ir[ket])
			  if (j3->gptrmult == redmat->gptrmult) {
			    redmatel = redmat->matelhead;
			    for (ibra  = j3->branch[bra]->start[stbra]; 
                                 ibra <= j3->branch[bra]->start[stbra] 
                                       + j3->branch[bra]->gpmult[stbra] - 1;
                                 ibra++) {
			      dimensiontype cket;
			      for (iket  = j3->branch[ket]->start[stket], 
				   cket  = j3->branch[ket]->gpmult[stket];
                                   cket>0; iket++, cket--) {
			        mixprod(&U(ibra - 1,iket - 1), oper->strength, 
			              redmatel->complex, j3->threej);
				redmatel = redmatel->next;
			      }
			    }
			  }
  			  j3 = j3->next;
		        }
  		      }
		      redmat = redmat->next;
		    }
		    oper = oper->next;
		  }
	          statistics("MATRIX READY");
                  /* PRINT MATRIX, PUT IT in STORE, DIAGONALIZE IT and
                     TRANSFORM OTHER MATRICES  THAT ARE ALLREADY in STORE */
		  treatmatrix(a, u, actor, ipgtriad);
	        }
	    }
	  }
	  actor = actor->next;
	}
      }
    }
  }
  printtrnsform(a);
  /* disktrnsform(a); */
  statistics("EXECUTETASK");
  free_matrices();
}

void irrepinfotodisk()
{
  statetype state;
  irreptype irep;

  per(state, ground, excite) {
    for (irep = 0; irep <= chain[lpoint]->nreps; irep++) {
      if (chain[lpoint]->ir[irep].present[state][basis]) {
        bufp += slprintf(sprintf(&buf[bufp], " IRREP %.7s", tasktext[state]));
        putirrep(irep, chain[lpoint]->slot);
        bufp += slprintf(sprintf(&buf[bufp], "  MULT %ld   DIM %ld",
                        chain[lpoint]->ir[irep].mult[state],
                        chain[lpoint]->dimen[irep]));
        send_disk();
      }
    }
  }
}

void execute_racer_task(lpg)
linktype lpg;
{
  p_groupx pg;
  tasktype task;
  statetype stbra, stket;
  p_actor actor;
  p_oper oper;
  ntriadtype ipgtriad;
  p_redmat redmat;
  p_j3 j3;
  dimensiontype nbra, nket, iket, ibra;
  p_matel redmatel;
  boolean diagonalize;
  integer qi;
  braoprkettype bok;

  send();
  send();
  bufp += slprintf(sprintf(&buf[bufp], " ---- EXECUTE TASK ----"));
  send();
  send();
  irrepinfotodisk();
  pg = chain[lpg];
  {
    actor = actorhead;
    while (actor != NIL) {
      send();
      put_super(actor->name);
      send();
      bufp += slprintf(sprintf(&buf[bufp], " CALCULATIONS for ACTOR: %s %s",
                      actor->name, tasktext[actor->task]));
      send();
      send();
      stbra = findstate(actor->task, bra);
      stket = findstate(actor->task, ket);
      for (ipgtriad = 1; ipgtriad <= pg->ntriads; ipgtriad++) {
	if (pg->triad[ipgtriad - 1].present[actor->task])
	  if (pg->triad[ipgtriad - 1].ir[opr] == actor->pgir) {
	    dimens(ipgtriad, actor->task, &nbra, &nket);
	    bufp += slprintf(sprintf(&buf[bufp], " %6cCALCULATING MATRIX for", ' '));
	    put1triad(ipgtriad, lpg);
	    bufp += slprintf(sprintf(&buf[bufp], "   (%ld*%ld)", nbra, nket));
	    if (debug_butler)
	      bufp += slprintf(sprintf(&buf[bufp], "   ADDING MATRICES of the FOLLOWING OPERATORS"));
	    send();
            send_disk();
            send_disk();
            bufp += slprintf(sprintf(&buf[bufp], " REDUCEDMATRIX %s ", tasktext[actor->task]));
            per(bok, bra, ket)
              putirrep(pg->triad[ipgtriad - 1].ir[bok], pg->slot);
            putprodmult(pg->triad[ipgtriad - 1].trmult, pg->slot);
            qi = max(qx(nbra), qx(nket)) + 1;
            bufp += slprintf(sprintf(&buf[bufp], "  %.16s %*ld%*ld", actor->name, (int)qi, nbra, (int)qi, nket));
            send_disk();
	    oper = actor->operhead;
	    while (oper != NIL) {
	      if (debug_butler) putoper(oper);
	      redmat = redmathead;
	      while (redmat != NIL) {
		if ((redmat->task == actor->task) &&
                    (redmat->ir[opr] == oper->branch->gpir) &&
                    (streq(redmat->name, oper->name) )) {
		  j3 = pg->triad[ipgtriad - 1].j3head;
		  while (j3 != NIL) {
		    if (j3->branch[opr]       == oper->branch)
		    if (j3->branch[bra]->gpir == redmat->ir[bra])
		    if (j3->branch[ket]->gpir == redmat->ir[ket])
		    if (j3->gptrmult          == redmat->gptrmult) {
                      bufp += slprintf(sprintf(&buf[bufp], "  ADD %6ld  %*ld%*ld  %*ld%*ld ",
                        redmat->sequence,
                        (int)qi, j3->branch[bra]->start[stbra],
                        (int)qi, j3->branch[ket]->start[stket],
                        (int)qi, j3->branch[bra]->gpmult[stbra],
                        (int)qi, j3->branch[ket]->gpmult[stket]));
                        putmixprod(oper->strength, j3->threej);
                        send_disk();
		    }
  		    j3 = j3->next;
		  }
  		}
		redmat = redmat->next;
	      }
	      oper = oper->next;
	    }
	    statistics("MATRIX READY");
            bufp += slprintf(sprintf(&buf[bufp], "  END %.16s", actor->name));
            send_disk();
	  }
      }
      actor = actor->next;
    }
  }
  statistics("EXECUTETASK");
}

void butler()
{
  linktype link;

  initbutler();
  {
    type_in_record *Win = &in_record[in_file];

    /* READ COMMANDS from INPUT */
    link = 0;
    chain[0] = NIL;
    subgroup = -1;
    do {
      /* TREAT SUBSEQUENT LINKS of the SUBGROUP CHAIN from the
         SUPERGROUP to the ACTUAL POINTGROUP */
      link = link + 1;
      group = subgroup;
      if (link == 1)
        /* READ DATA of THIS LINK from"DISK0" INTO"GPDATA" */
	subgroup = read_a_group();
      else
	read_subgroup();
      extendgroup(link, subgroup);
      /* COPY RELEVANT DATA from"GPDATA" to"GROUPX" */
      putgroupx(link);
      extendpresent(link);
      /* READ WHICH IRREPS of THIS SUBGROUP ARE TO BE DISCARDED */
      extendbranches(link - 1, link);
      /* CONSTRUCT ALL BASIS FUNCTIONS BRANCHING from ALL BASIS FUNCTIONS
         in the PREVIOUS LINK EXCEPT THOSE DISCARDED IRREPS. */
      if (debug_butler)
	putbranches(link);
      extendj3s(link - 1, link);
      /* CONSTUCT ALL 3J SYMBOLS UP to THIS LINK BY CONSIDERING ALL 3J SYMBOLS
         UP TO,the PREVIOUS LINK, USING ALL BRANCHINGS of IRREPS, and
         MULTIPLYING BY the CORRESPONDING 3JM and 2JM FACTORS */
      if (debug_butler)
	putj3s(link);
    } while (!(member2( Win->in_item, end_of_file, end_of_statement) ||
               (in_eq("ENDCHAIN"))));
  }
  /* BASISFUNCTION and 3J CALCULATIONS COMPLETE */
  lpoint = link;
  if (!debug_butler)
    putbranches(lpoint);
  readactorsandopers(lpoint);  /* READ OPERATORS */
  putactors();
  if ((nr_warn == 0))
    if (racer)
      execute_racer_task(lpoint);
    else
      executetask(lpoint);
    /* CONSTRUCT MATRICES for ALL ACTORS (=SUM of OPERATORS) DIAGONALIZE IF
       REQUESTED USING EIGENVECTORS to TRANSFORM MATRXES of OTHER ACTORS if
       REQUESTED. MATRICES ARE PRINTED in ANY REQUESTED STAGE, DETERMINED BY
"PURP" and"MODE" */
  else {
    bufp += slprintf(sprintf(&buf[bufp], " EXECUTION STOPPED BECAUSE OF ERRORS"));
    send();
  }
}

boolean racah_command()
{
  boolean result;
  boolean no_subgroup;

  result = true;
  no_subgroup = false;
  {
    type_in_record *Win = &in_record[in_file];

    if (in_eq("RACAH"))
      setup_group_and_subgroup();
    else if (group == -1) {
      bufp += slprintf(sprintf(&buf[bufp], " NO GROUP DATA AVAILABLE,"));
      result = false;
    } else if (in_eq("3J"))
      informtriad();
    else if (in_eq("6J"))	inform6j();
    else if (in_eq("9J"))	inform9j();
    else if (in_eq("IRREPS"))	write_irrep_table();
    else if (in_eq("6JTABLE"))	writetable(6);
    else if (in_eq("9JTABLE"))	writetable(9);
    else if (in_eq("3JM")) {
      if (subgroup == -1)
	no_subgroup = true;
      else
	inform3jm();
    } else if (in_eq("3JMTABLE")) {
      if (subgroup == -1)
	no_subgroup = true;
      else
	writetable(3);
    }
    else if (in_eq("INTERNAL"))	internal = true;
    else if (in_eq("EXTERNAL"))	internal = false;
    else if (in_eq("GROUP"))	group = get_integer();
    else if (in_eq("SUBGROUP"))	subgroup = get_integer();
    else if (in_eq("3JQ"))	showtriad();
    else if (in_eq("6JQ"))	show6j();
    else if (in_eq("2JMQ"))	show2jm();
    else if (in_eq("3JMQ"))	show3jm();
    else			result = false;
  }
  if (no_subgroup) {
    bufp += slprintf(sprintf(&buf[bufp], " NO SUBGROUP DATA AVAILIABLE"));
    send_warn();
  }
  return result;
}

boolean racah()
{
  boolean result;
  in_file_type old_in_file;

  result = true;
  internal = false;
  group    = -1;
  subgroup = -1;
  {
    type_in_record *Win = &in_record[in_file];

    if (in_eq("RACAH")) {
      bufp += slprintf(sprintf(&buf[bufp], " RACAH CALLED"));
      send();
      reset_racah();
      if (racer) {
        old_in_file = in_file;
        in_file = disk_one;
        close_in_file();
        in_file = old_in_file;
        close_out_disk();
      }
      racer = false;
      setup_group_and_subgroup();
      while (!get_command(racah_prompt))
	if (!racah_command()) {
	  bufp += slprintf(sprintf(&buf[bufp], " INVALID RACAH COMMAND:"));
	  put_input();
	  send_warn();
	}
    } else if (in_eq("BUTLER")) {
      reset_racah();
      if (racer) {
        old_in_file = in_file;
        in_file = disk_one;
        close_in_file();
        in_file = old_in_file;
        close_out_disk();
      }
      racer = false;
      butler();
    } else if (in_eq("RACER")) {
      reset_racah();
      if (!racer) {
        old_in_file = in_file;
        in_file = disk_one;
        open_in_file(rme_name);
        in_file = old_in_file;
        open_out_disk("BAND DISK");
      }
      racer = true;
      butler();
    } else
      result = false;
  }
  return result;
}

/* *	Start of program code */
main(argc, argv)
int argc;
char *argv[];
{
    in_file_type old_in_file;
#ifdef CONVEX
  f_init();
#endif
  if (setjmp(J[0].jb))
    goto L9999;
  if (argc > 1)
    strcpy(rme_name, argv[1]);
  else
    strcpy(rme_name, def_rme_name);
  if (argc > 2)
    strcpy(log_name, argv[2]);
  else
    strcpy(log_name, def_log_name);
  initialise_runner();
  c_initialiseheaps();
  {
    type_in_record *Win = &in_record[in_file];

    do {
      if (get_command(main_prompt)) {
	if (Win->in_item == exit_loop) {
	  bufp += slprintf(sprintf(&buf[bufp], " \"FINISH\" is the FINAL COMMAND"));
	  send();
	}
      } else if (!racah()) {
	bufp += slprintf(sprintf(&buf[bufp], " INVALID RUNNER COMMAND."));
	put_input();
	send_warn();
      }
      CHECK_MEM();
    } while (!(Win->in_item == end_of_file));
  }
L9999:
  bufp += slprintf(sprintf(&buf[bufp], " RUNNER FINISHED"));
  send();
  if (line_is_open)
    close_printer(p_line);
  if (racer) {
    old_in_file = in_file;
    in_file = disk_one;
    if (disk0 != NULL){
    close_in_file();
    }
    in_file = old_in_file;
    close_out_disk();
  }
  close_in_file();
  if (log_is_open)
    close_printer(p_logfile);
  exit(0);
}

/* *	End of program code */

static void Caseerror(n)
long int n;
{
  (void) fprintf(stderr,"Missing case limb: line %ld\n", n);
  exit(1);
}





