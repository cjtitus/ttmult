
extern char command_string[STRING_SIZE];

extern void get_buff(FILE *);
extern void get_string(void);
extern integer get_integer(void);
extern real get_real(void);
extern void get_title(char []);
extern boolean read_datafile(input_type, char [], boolean);
extern plot_data read_spectrum(char []);
extern boolean interpret_lorentzian(lorentz *, char *);
extern void reset_lorentz(lorentz *);
