#include <iostream>
#include <cmath>
#include <cstring>
#include <fstream>
#include <sstream>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{

  if (argc != 2 ) //checks whether was there an input argument
    {
        cout<<"Give Filename!!"<<endl;
        return 0;
    }
    
ifstream read;
read.open(argv[1]);
    
if (read.fail()) //checks whether the file exist
    {
        cout<<"Filename "<<argv[1]<<" does not exist"<<endl;
        return 0;
    }
cout<<"Input File: "<<argv[1]<<endl;
    
    
string filename=string(argv[1]); //converts input char array to string (its easier to work with strings)
size_t find_orig = filename.find(".orig");
if (find_orig!=std::string::npos)
    filename=filename.substr(0,find_orig); //stripping out the ".orig" extension
else
    {
        cout<<"This is not an .orig file"<<endl; //checks if this is a .orig file
        return 0;
    }
    
cout<<"Output filename: "<<filename<<endl; //makes the output filename
ofstream save;
save.open(filename.c_str());
   
    char line[500];
    
    while (!read.eof()) //loops the input file
    {
        read.getline(line,500); //read line-by-line as Char array (here of up to 500 characters)
        if (read.eof())
            break;
        
        string mystring = string(line); //convert char to string

        size_t findAdd = mystring.find("ADD"); //find if "ADD" is there
        if (findAdd!=std::string::npos)
            {
            size_t found = mystring.find("("); //fing "(" position, if any
                if (found!=std::string::npos)
                    {
                     size_t found2 = mystring.find(","); //find "," position
                     size_t found3 = mystring.find(")"); //find ")" position
                                             
                     string real_part_str = mystring.substr(found+1,found2-found-1);
                     string imag_part_str = mystring.substr(found2+1,found3-found2-1);
                        
                     double real_part = atof (real_part_str.c_str());
                     double imag_part = atof (imag_part_str.c_str());

                     double number;
                     number = real_part;
                        
                     string new_str = mystring.substr(0,found-1);
                     char newline[500];
                     sprintf(newline,"%s %.8f \n",new_str.c_str(),number);
                     save<<newline;
                    }
                else  //when no "(" in ADD line, just save "as is"
                    {
                     save<<mystring<<endl;
                    }
            }

        else
            save<<mystring<<endl;
        
    }
    

    read.close();
    save.close();
    
    return 0;
}
