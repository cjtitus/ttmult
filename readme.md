Here we aim to bring the suite of multiplet theory programs for the simulation of core-hole  X-ray absorption  and emission spectroscopy to Linux. Windows binaries are distributed with CTM4XAS, however we are not aware of a shared resource for Linux users. This is not a complete software suite, we just provide the bare bones source code and a make file for compilation of the basic programs. No guide for input files is given here, users are directed to  the examples given and CTM4XAS for acquiring further input files. These codes were developed by R. D. Cowan, B. T. Thole and co-workers. We claim no ownership of these codes or guarantee with regards to their functionality. The make file has been tested on Ubuntu 16.04 and 14.04. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

This folder contains:

* src: Source files for a working, bare-bones version of ttmult that can be compiled on Ubuntu 14.04 and 16.04 systems with ifort and gcc
* scripts: scripts that call the compiled executables
* examples: a directory full of examples that came with the original ttmult,
* test: a test directory that contains an example that can be run end-to-end to test all compiled executables

For details on excruciating details of code, see compilation_notes. 

Required compilers: CC and ifort. gfortran can be used instead of ifort to compile ttban (however see compilation_notes for details). The ifort compiler can be obtained as part of Intel Parallel Studio suite. After instalation of Parallel Studio (30 day trial version https://software.intel.com/en-us/fortran-compilers/try-buy), it was necessary to set the compiler variables before use with the command (for a 64 bit system):
source /opt/intel/compilers_and_libraries/linux/bin/compilervars.sh intel64

With compilers setup install at the terminal as follows: 

(1) navigate to the ttmult/src

(2) make all

(3) make install

(4) make clean

(5) navigate to ttmult/scripts

(6) ./gen_cfp

To run the test example, try running test/test_nid8ct.sh, and see notes in the test directory.


Who to contact: 
Charles J. Titus - ctitus@stanford.edu                                   
Michael L. Baker - michael.baker@manchester.ac.uk (or michael.lloyd.baker@gmail.com)

