
extern lorentz lorentzian;
extern boolean adjust_scale;
extern char label[STRING_SIZE];

extern void set_gaussian(void);
extern void set_energy_range(void);
extern void load_options(real *, real *);
extern void load_data(void);
extern void change_input(input_type);
extern void get_spectrum_parameters(void);
extern void interpret_spectrum(boolean, boolean, boolean);
