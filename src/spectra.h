
extern boolean dynamic_range, inform, nodegeneracies;
extern integer npoints;
extern real gaussian, approx_zero, ground_energy;
extern real min_energy, max_energy, temperature, scale, energy_shift;
extern char initial_state[STRING_SIZE], operator_label[STRING_SIZE],
	    final_state[STRING_SIZE];
extern boolean ground_energy_set;
extern state ground_states;
extern plot_data plot_head;

extern boolean unique_plot_label(char []);
extern plot_data find_plot(char []);
extern void add_plot(plot_data);
extern void remove_plot(plot_data);
extern spectral_line create_lines(integer);
extern void insert_node(spectral_line, boolean);
extern void insert_ground_state(state);
extern void store_ground(void);
extern void clear_tree(boolean);
extern plot_data create_spectrum(boolean, lorentz);
