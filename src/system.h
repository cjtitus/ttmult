
/* All the basic types in use occur here and can easily be changed for
   different machines. The macros that define the functions should also
   be appropriately altered with typecasts as needed. */

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>

#define not !
#define and &&
#define or  ||
#define false (boolean)0
#define true  (boolean)1
#define min(a,b) (a < b ? a : b)
#define max(a,b) (a > b ? a : b)

#ifdef CONVEX

#define BIG_REAL 1e100

#else /* !CONVEX */

#define BIG_REAL DBL_MAX

#endif

#define ZERO     DBL_MIN

#define STRING_SIZE 80

typedef long integer;
typedef double real;
typedef short boolean;

#define rabs(x)		fabs(x)
#define rsqrt(x)	sqrt(x)
#define rlog10(x)	log10(x)
#define rpow(x,y)	pow(x, y)
#define rexp(x)		exp(x)
#define Trunc(x)	((integer) floor(x))
#define rTrunc(x)	floor(x)
#define Round(x)	((integer) floor(x + 0.5))
#define Upper(x)	((integer) ceil(x))
#define Atan(x)		atan(x)


extern void add_path(char [], char []);
extern void change_dir(char []);
extern boolean is_a_terminal(FILE *);

#ifdef DEBUG

extern void *mem_alloc(unsigned long);
extern void free_mem(void *);

#else

#define mem_alloc(x)	malloc((unsigned)(x))
#define free_mem(x)	free((char *)(x))

#endif
