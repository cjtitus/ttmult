
#include "globals.h"
#include <string.h>
#include <ctype.h>

#include "input.h"
#include "spectra.h"
#include "common.h"

/* Local data */
static char buf[LINE_SIZE];
static integer bufp = 0;
static FILE *spectra_file;
static integer number_kets = 0;

/* Function prototypes */
static void skip_blank(void);
static void get_next_string(void);
static boolean locate_string(char []);
static integer locate_strings(char [], char []);
static integer get_c_number(void);
static void build_ground_states(spectral_line, integer);
static integer read_number_of_lines(void);
static spectral_line locate_node(char [], char []);
static void read_lines(integer number);
static void read_rcg(void);
static void read_ket_energies(spectral_line, integer);
static void read_bra_and_intensity(spectral_line, integer);
static void read_old_racah(void);
static void read_racah(void);
static void read_band(void);
static real read_xy_data(plot_data, real, FILE *);
static real read_sgp_data(plot_data, FILE *);
static void lorentz_node(lorentz *lorentzian, lorentz node);

/* These are the basic input file routines */

static void skip_blank()
/* skip blanks in the command line */
{
  while (isspace(buf[bufp]))
    bufp++;
}

void get_buff(FILE *file)
/* read the next command line, terminate it correctly and
   position the buffer at the first non-blank character.
   Does not check for an overflow */
{
  register integer ch, bptr = 0;

  if (is_a_terminal(file)) {
    printf("> ");
    fflush(stdout);
  }
  while ((ch = getc(file)) != '\n' and ch != EOF)
    buf[bptr++] = (char)ch;
  buf[bptr] = 0;
  bufp = 0;
  skip_blank();
}

char command_string[STRING_SIZE];

void get_string()
/* get the next string of non-blanks from the command line,
   and position buffer at start of next string.
   Does not check for an overflow */
{
  register integer i = 0;

  if (buf[bufp] == 0) {
    command_string[i] = 0;
    return;
  }
  while (!isspace(buf[bufp]) and buf[bufp] != 0)
    command_string[i++] = buf[bufp++];
  command_string[i] = 0;
  skip_blank();
  return;
}

static void get_next_string()
{
  if (buf[bufp] == 0)
    get_buff(spectra_file);
  get_string();
}

integer get_integer()
/* get an integer from the command line,
   interprets the next string of characters as an integer */
{
  register integer value;

  get_string();
  value = atol(command_string);
  return(value);
}

real get_real()
/* gets a real number in fixed or float format from the command line,
   interprets the next string of characters as a real */
{
  register real value;

  get_string();
  value = atof(command_string);
  return(value);
}

void get_title(char title[])
/* read in the title for the figure */
{
  strcpy(title, &buf[bufp]);
}

/* routines to get information from the data file */

static boolean locate_string(char *str)
/* locates a string which begins at the start of a line */
{
  register boolean found = false;

  while (not (found or feof(spectra_file))) {
    get_buff(spectra_file);
    found = strncmp(buf, str, strlen(str)) == 0;
    bufp = strlen(str);
  }
  return(found);
}

static integer locate_strings(char *stra, char *strb)
/* locates a string which begins at the start of a line */
{
  register integer found = 0;

  while (not (found or feof(spectra_file))) {
    get_buff(spectra_file);
    if (strncmp(buf, stra, strlen(stra)) == 0) {
      found = 1;
      bufp = strlen(stra);
    } else if (strncmp(buf, strb, strlen(strb)) == 0) {
      found = 2;
      bufp = strlen(strb);
    } else
      found = 0;
  }
  return(found);
}

static integer get_c_number()
/* Get an integer written by racahs c_number routine. */
{
  integer value = 0;
  char *ptr;

  get_string();
  value = strtol(command_string, &ptr, 10);
  while (*ptr != 0 and *ptr == '.')
    value *= strtol(ptr + 1, &ptr, 10);
  return(value);
}

static void build_ground_states(spectral_line node, integer dim)
{
  state gs;
  integer i;

  gs = (state) mem_alloc(sizeof(struct gstate));
  gs->energy = (real *) mem_alloc(node->rows * sizeof(real));
  gs->next = NULL;
  gs->rows = node->rows;
  gs->dim = dim;
  strcpy(gs->label, node->initial_state);
  for (i = 0; i < node->rows; i++)
    gs->energy[i] = node->intensities[i].bra_energy;
  insert_ground_state(gs);
}

static spectral_line rcg_nodes = NULL;

static integer read_number_of_lines()
/* scan down the lines to find the total number of lines in this block */
{
  integer number, temp = 0;
  spectral_line node;

  number = temp;
  get_buff(spectra_file);
  temp = atol(&buf[24]);
  while (temp != 0) {
    node = create_lines(1);
    node->next = rcg_nodes;
    rcg_nodes = node;
    get_string();
    /* all this assumes J <= 9.5 */
    node->op_label[0] = 0;
    strcpy(node->transition_label, "1-");
    if (command_string[2] == '5') {
      node->initial_state[0] = 's';
      node->initial_state[1] = command_string[0];
      node->initial_state[2] = 0;
    } else if (command_string[0] == '.' ) {
      node->initial_state[0] = '0';
      node->initial_state[1] = 0;
    } else {
      node->initial_state[0] = command_string[0];
      node->initial_state[1] = 0;
    }
    get_string();
    if (command_string[2] == '5') {
      node->final_state[0] = 's';
      node->final_state[1] = command_string[0];
      node->final_state[2] = 0;
    } else if (command_string[0] == '.' ) {
      node->final_state[0] = '0';
      node->final_state[1] = 0;
    } else {
      node->final_state[0] = command_string[0];
      node->final_state[1] = 0;
    }
    node->multiplicity = 1;
    node->cols = get_integer();
    node->ket_energies = (real *) mem_alloc(node->cols * sizeof(real));
    node->intensities[0].intensities = (real *) mem_alloc(node->cols
							  * sizeof(real));
    number = temp;
    get_buff(spectra_file);
    temp = atol(&buf[24]);
  }
  return(number);
}

static spectral_line locate_node(char label1[], char label2[])
{
  spectral_line node = rcg_nodes;

  while (node != NULL) {
    if (strcmp(node->initial_state, label1) == 0 and
	strcmp(node->final_state, label2) == 0)
      break;
    else
      node = node->next;
  }
  return(node);
}

static void read_lines(integer number)
/* Read the spectral lines from the file */
{
  integer curr_line = 1, offset1 = 0, offset2 = 0, offset3 = 0, dim, fdim;
  spectral_line node;
  real energy;
  char ilabel[8], flabel[8];

  (void) locate_string("0           E");
  do {
    get_buff(spectra_file);
    if (get_integer() == curr_line) {
      /* should we allow for other spectra here??? */
      energy = get_real();
      get_string();
      /* all this assumes J <= 9.5 */
      if (command_string[2] == '5') {
	ilabel[0] = 's';
	ilabel[1] = command_string[0];
	ilabel[2] = 0;
      } else if (command_string[0] == '.' ) {
	ilabel[0] = '0';
	ilabel[1] = 0;
      } else {
	ilabel[0] = command_string[0];
	ilabel[1] = 0;
      }
      dim = (integer)(2.0 * atof(command_string)) + 1;
      get_string();
      get_string();
      get_string();
      get_string();
      get_string();
      if (command_string[2] == '5') {
	flabel[0] = 's';
	flabel[1] = command_string[0];
	flabel[2] = 0;
      } else if (command_string[0] == '.' ) {
	flabel[0] = '0';
	flabel[1] = 0;
      } else {
	flabel[0] = command_string[0];
	flabel[1] = 0;
      }
      fdim = (integer)(2.0 * atof(command_string)) + 1;
      node = locate_node(ilabel, flabel);
      node->intensities[0].bra_energy = energy;
      get_string();
      get_string();
      get_string();
      if (fdim < dim) {
        node->ket_energies[offset1] = get_real() + energy;
        get_string();
        /* S/p**2 */
        node->intensities[0].intensities[offset1] = get_real();
	offset1++;
        if (offset1 == node->cols) {
	  build_ground_states(node, dim);
	  offset1 = 0;
	}
      } else if (fdim == dim) {
        node->ket_energies[offset2] = get_real() + energy;
        get_string();
        /* S/p**2 */
        node->intensities[0].intensities[offset2] = get_real();
	offset2++;
        if (offset2 == node->cols) {
	  build_ground_states(node, dim);
	  offset2 = 0;
	}
      } else {
        node->ket_energies[offset3] = get_real() + energy;
        get_string();
        /* S/p**2 */
        node->intensities[0].intensities[offset3] = get_real();
	offset3++;
        if (offset3 == node->cols) {
	  build_ground_states(node, dim);
	  offset3 = 0;
	}
      }
      curr_line++;
    }
  } while (curr_line <= number);
  while (rcg_nodes != NULL) {
    node = rcg_nodes;
    rcg_nodes = node->next;
    node->next = NULL;
    insert_node(node, true);
  }
  if (inform)
    printf("Read %d lines from RCG9 file\n", number);
}

static void read_rcg()
/* Read input from a file written by RCG9 */
{
  register integer number;

  (void) locate_string("                  NO. OF LINES");
  /* skip over the heading */
  get_buff(spectra_file);
  get_buff(spectra_file);
  do {
    number = read_number_of_lines();
    read_lines(number);
    (void) locate_string(" STAMP");
  } while (not feof(spectra_file));
}

static void read_ket_energies(spectral_line node, integer offset)
/* read the energies of the kets from the rest of the line */
{
  static real ketlist[15]; /* 15 is a safe number */
  register integer number = 0;

  skip_blank();
  while (command_string[0] != 0) {
    ketlist[number] = get_real();
    number++;
  }
  number_kets = number - 1;
  for (number = offset; number < offset + number_kets; number++)
    node->ket_energies[number] = ketlist[number - offset];
}

static void read_bra_and_intensity(spectral_line new_node, integer offset)
/* read the bra energy and then the intensities from the rest of the line */
{
  register integer i, j = 0;

  get_buff(spectra_file);
  while (buf[bufp] != 0) {
    if (offset == 0) {
      new_node->intensities[j].bra_energy = get_real();
      new_node->intensities[j].intensities =
	(real *) mem_alloc(new_node->cols * sizeof(real));
    } else
      (void) get_real();
    for (i = 0; i < number_kets; i++)
      new_node->intensities[j].intensities[offset + i] = get_real();
    j++;
    get_buff(spectra_file);
  }
  if (offset == 0)
    new_node->rows = j;
  if (inform)
    printf("Read %d bra with %d ket for transition (%s %s %s %d)\n", j,
           number_kets, new_node->initial_state, new_node->transition_label,
           new_node->final_state, new_node->multiplicity);
}

static void read_old_racah()
/* Read input from a file written by Racah */
{
  integer string_type = 0, offset, dim;
  spectral_line node;
  char *ptr;
  struct matrix new_node;
  boolean first = true;

  while (not feof(spectra_file)) {
    string_type = locate_strings(" TRANSFORMED MATRIX", " BRA/KET :");
    if (string_type == 1) {
      offset = 0;
      first = false;
      skip_blank();
      get_string();
      get_string();
      get_string();
      get_string();
      /* We have a problem if the label has no space between it and this ( */
      if (command_string[1] != 0)
	strcpy(new_node.initial_state, &command_string[1]);
      else {
	get_string();
	strcpy(new_node.initial_state, command_string);
      }
      get_string();
      new_node.op_label[0] = 0;
      strcpy(new_node.transition_label, command_string);
      get_string();
      strcpy(new_node.final_state, command_string);
      new_node.multiplicity = get_integer();
      get_string();
      new_node.rows = strtol(&command_string[1], &ptr, 10);
      new_node.cols = strtol(ptr + 1, NULL, 10);
      get_string();
      /* We need to avoid the ':' at the start of this string. */
      get_string();
      dim = atol(&command_string[1]);
      node = create_lines(new_node.rows);
      node->cols = new_node.cols;
      node->multiplicity = new_node.multiplicity;
      strcpy(node->initial_state, new_node.initial_state);
      strcpy(node->transition_label, new_node.transition_label);
      strcpy(node->final_state, new_node.final_state);
      node->ket_energies = (real *) mem_alloc(node->cols * sizeof(real));
      if (locate_string(" BRA/KET :"))
	string_type = 2;
      else
	string_type = 0;
    }
    if (string_type == 2 and not first) {
      read_ket_energies(node, offset);
      get_buff(spectra_file);
      read_bra_and_intensity(node, offset);
      offset += number_kets;
      if (offset >= node->cols) {
	insert_node(node, true);
	build_ground_states(node, dim);
      }
    }
  }
}

static void read_racah()
/* Read input from a file written by Racah */
{
  integer string_type = 0, offset, dim;
  spectral_line node;
  struct matrix new_node;
  boolean first = true;

  while (not feof(spectra_file)) {
    string_type = locate_strings("Transformed", "Bra/Ket  :");
    if (string_type == 1) {
      first = false;
      offset = 0;
      skip_blank();
      get_string();
      get_string();
      get_string();
      get_string();
      get_string();
      strcpy(new_node.op_label, command_string);
      get_string();
      get_string();
      strcpy(new_node.initial_state, command_string);
      get_string();
      strcpy(new_node.transition_label, command_string);
      get_string();
      strcpy(new_node.final_state, command_string);
      get_string();
      if (command_string[0] == '+' or command_string[0] == '-')
	new_node.multiplicity = 1;
      else
	new_node.multiplicity = atol(command_string);
      /* no need to get another string since mult/phase are printed together */
      get_next_string();
      new_node.rows = atol(command_string);
      get_next_string();
      new_node.cols = atol(command_string);
      /* Dimbra */
      get_next_string();
      dim = get_c_number();
      node = create_lines(new_node.rows);
      node->cols = new_node.cols;
      node->multiplicity = new_node.multiplicity;
      strcpy(node->initial_state, new_node.initial_state);
      strcpy(node->transition_label, new_node.transition_label);
      strcpy(node->final_state, new_node.final_state);
      strcpy(node->op_label, new_node.op_label);
      node->ket_energies = (real *) mem_alloc(node->cols * sizeof(real));
      if (locate_string("Bra/Ket  :"))
	string_type = 2;
      else
	string_type = 0;
    }
    if (string_type == 2 and not first) {
      read_ket_energies(node, offset);
      get_buff(spectra_file);
      read_bra_and_intensity(node, offset);
      offset += number_kets;
      if (offset >= node->cols) {
	insert_node(node, true);
	build_ground_states(node, dim);
      }
    }
  }
}

static void read_band()
/* Read input from a file written by Bander */
{
  register integer i, string_type = 0, offset, dim;
  spectral_line node;
  char *ptr;
  struct matrix new_node;
  register boolean first = true;

  while (not feof(spectra_file)) {
#ifdef VMS /* This mess is due to the file being a VMS F77 output file */
    string_type = locate_strings("TRANSFORMED MATRIX", "BRA/KET :");
#else
    string_type = locate_strings(" TRANSFORMED MATRIX", " BRA/KET :");
#endif
    if (string_type == 1) {
      offset = 0;
      first = false;
      skip_blank();
      get_string();
      get_string();
      get_string();
      /* We have a problem if the label has no space between it and this ( */
      if (command_string[1] != 0)
	strcpy(new_node.initial_state, &command_string[1]);
      else {
	get_string();
	strcpy(new_node.initial_state, command_string);
      }
      get_string();
      new_node.op_label[0] = 0;
      strcpy(new_node.transition_label, command_string);
      get_string();
      strcpy(new_node.final_state, command_string);
      if (command_string[strlen(command_string) - 1] == ')')
	new_node.multiplicity = 1;
      else
        new_node.multiplicity = get_integer();
      get_string();
      new_node.rows = strtol(&command_string[1], &ptr, 10);
      if (*(ptr + 1) != 0)
	new_node.cols = strtol(ptr + 1, NULL, 10);
      else
	new_node.cols = get_integer();
      /* DIM */
      get_string();
      /* : */
      get_string();
      dim = get_integer();
      node = create_lines(new_node.rows);
      node->cols = new_node.cols;
      node->multiplicity = new_node.multiplicity;
      strcpy(node->initial_state, new_node.initial_state);
      strcpy(node->transition_label, new_node.transition_label);
      strcpy(node->final_state, new_node.final_state);
      node->ket_energies = (real *) mem_alloc(node->cols * sizeof(real));
#ifdef VMS
      if (locate_string("BRA/KET :"))
#else
      if (locate_string(" BRA/KET :"))
#endif
	string_type = 2;
      else
	string_type = 0;
    }
    if (string_type == 2 and not first) {
      read_ket_energies(node, offset);
      get_buff(spectra_file);
      if (offset == 0) {
	node->intensities[0].bra_energy = get_real();
        node->intensities[0].intensities = (real *) mem_alloc(node->cols *
							      sizeof(real));
	node->rows = 1;
      } else
	(void) get_real();
      for (i = 0; i < number_kets; i++)
	node->intensities[0].intensities[offset + i] = get_real();
      offset += number_kets;
      if (inform)
	printf("Read bra with %d ket for transition (%s %s %s %d)\n",
	       number_kets, new_node.initial_state, new_node.transition_label,
	       new_node.final_state, new_node.multiplicity);
      if (offset >= node->cols) {
	insert_node(node, true);
	build_ground_states(node, dim);
      }
    }
  }
}

boolean read_datafile(input_type format, char filename[], boolean overwrite)
/* Read in the spectrum */
{
  if (filename[0] == 0)
    return(false);
  spectra_file = fopen(filename, "r");
  if (spectra_file == NULL)
    return(false);
  if (overwrite)
    clear_tree(true);
  switch (format) {
    case rcg9:
      read_rcg();
      break;
    case old_racah:
      read_old_racah();
      break;
    case racah:
      read_racah();
      break;
    case bander:
      read_band();
      break;
  }
  fclose(spectra_file);
  store_ground();
  return(true);
}

static real read_xy_data(plot_data plot, real spec, FILE *data_file)
/* Reads in an experimental spectrum in xy (coordinate) format. Does assume
   that the data points are regularly spaced though. */
{
  register integer i = 1;
  real min_value = 1e6, energy;
  static real spectrum[2000];

  spectrum[0] = spec;
  get_buff(data_file);
  while (buf[0] != 0) {
    energy = get_real();
    spectrum[i++] = get_real();
    get_buff(data_file);
  }
  plot->max_energy = energy;
  plot->energy_step = (plot->max_energy - plot->min_energy) / ((real)(i - 1));
  plot->npoints = i;
  plot->spectrum = (real *) mem_alloc(plot->npoints * sizeof(real));
  for (i = 0; i < plot->npoints; i++) {
    plot->spectrum[i] = spectrum[i];
    if (spectrum[i] < min_value)
      min_value = spectrum[i];
  }
  return(min_value);
}

static real read_sgp_data(plot_data plot, FILE *data_file)
/* Reads in an experimental spectrum in ylist (SGP) format. */
{
  register integer i;
  real min_value = 1e6;

  get_buff(data_file);
  plot->max_energy = get_real();
  get_buff(data_file);
  plot->energy_step = get_real();
  get_buff(data_file);
  plot->npoints = get_integer();
  /* skip 12 + 4 lines */
  for (i = 0; i < 16; i++)
    get_buff(data_file);
  plot->spectrum = (real *) mem_alloc(plot->npoints * sizeof(real));
  for (i = 0; i < plot->npoints; i++) {
    get_buff(data_file);
    plot->spectrum[i] = get_real();
    if (plot->spectrum[i] < min_value)
      min_value = plot->spectrum[i];
  }
  return(min_value);
}

plot_data read_spectrum(char filename[])
/* Reads in an expt spectrum. */
{
  register FILE *data_file;
  register integer i;
  plot_data plot = NULL;
  real min_value, spec = 0.0;

  if (filename[0] == 0)
    return(NULL);
  data_file = fopen(filename, "r");
  if (data_file == NULL)
    return(NULL);
  plot = (plot_data) mem_alloc(sizeof(struct plot_info));
  plot->sticks = NULL;
  plot->next = NULL;
  get_buff(data_file);
  plot->min_energy = get_real();
  if ((spec = get_real()) != 0.0)
    min_value = read_xy_data(plot, spec, data_file);
  else
    min_value = read_sgp_data(plot, data_file);
  fclose(data_file);
  if (adjust_scale) {
    /* adjustment to shift lowest value to zero */
    if (min_value > 0.0)
      for (i = 0; i < plot->npoints; i++)
	plot->spectrum[i] -= min_value;
  }
  return(plot);
}

static void lorentz_node(lorentz *lorentzian, lorentz node)
/* Insert the new node into the list of lorentzian values */
{
  register lorentz ptra, ptrb, prev;

  if (*lorentzian == NULL) {
    *lorentzian = node;
    return;
  } else {
    ptra = *lorentzian;
    ptrb = ptra;
    while (ptra != NULL and node->min_energy >= ptra->min_energy) {
      ptrb = ptra;
      ptra = ptra->next;
    }
    if (ptra == (*lorentzian)) {
      *lorentzian = node;
      node->next = ptra;
    } else if (node->min_energy >= ptrb->max_energy) {
      ptrb->next = node;
      node->next = ptra;
    } else {
      ptrb->next = node;
      prev = node;
      node->next = (lorentz) mem_alloc(sizeof(struct LORENTZ));
      node = node->next;
      node->gamma = ptrb->gamma;
      node->q = ptrb->q;
      node->max_energy = ptrb->max_energy;
      ptrb->max_energy = prev->min_energy;
      node->min_energy = prev->max_energy;
      node->next = ptra;
    }
  }
}

boolean interpret_lorentzian(lorentz *node, char *string)
/* Get the lorentzian used for the line width,
   the value specifies half width at half height.
   You may specify that the Lorentzian only applies to certain energies */
{
  real gamma, energy;
  register lorentz ptr;
  boolean ok = true;

  if (string != NULL) {
    strcpy(buf, string);
    bufp = 0;
  }
  gamma = get_real();
  if (command_string[0] == 0 or gamma == 0.0)
    return(false);
  else {
    ptr = (lorentz) mem_alloc(sizeof(struct LORENTZ));
    ptr->next = NULL;
    ptr->gamma = gamma;
    ptr->min_energy = -1.0;
    ptr->q = get_real();
    if (ptr->q == 0.0)
      ptr->q = BIG_REAL;
    else
      get_string();
    if (strcmp(command_string, "range") == 0) {
      energy = get_real();
      if (command_string[0] != 0 and energy >= 0.0) {
	ptr->min_energy = energy;
	energy = get_real();
	if (command_string[0] != 0 and energy > ptr->min_energy)
	  ptr->max_energy = energy;
	else
	  ok = false;
      } else
	ok = false;
    }
    if (ptr->min_energy == -1) {
      ptr->min_energy = 0.0;
      ptr->max_energy = BIG_REAL;
    }
    if (ok)
      lorentz_node(node, ptr);
    else
      free_mem(ptr);
    return(ok);
  }
}

void reset_lorentz(lorentz *lorentzian)
/* Reinitialise the lorentzian values */
{
  register lorentz ptra, ptrb;

  ptra = *lorentzian;
  *lorentzian = NULL;
  while (ptra != NULL) {
    ptrb = ptra;
    ptra = ptra->next;
    free_mem(ptrb);
  }
}
