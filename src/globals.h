
#include "system.h"

#define LINE_SIZE 256
#define MAX_PTS 4000

typedef enum {rcg9, old_racah, racah, bander} input_type;
typedef enum {latex, raster_hp, hpgl, postscript, xy, ylist} output_types;
typedef enum {portrait, landscape} output_modes;
typedef enum {plot_it, compare, overlay, subtract} plot_modes;

typedef struct {
  real bra_energy;
  real *intensities;
} ket_vector;

typedef struct matrix {
  struct matrix *next;
  integer rows, cols, multiplicity;
  real *ket_energies;
  ket_vector *intensities;
  char initial_state[8], transition_label[8], final_state[8], op_label[16];
} *spectral_line;

typedef struct plot_info {
  struct plot_info *next;
  integer npoints;
  real min_energy, max_energy, energy_step;
  real *spectrum, *sticks;
  char label[STRING_SIZE];
} *plot_data;

typedef struct gstate {
  struct gstate *next;
  real *energy;
  /* Since we have multiple copies of the energy we don't need to count the
     degeneracy of the state. */
  integer rows, dim;
  char label[8];
} *state;

typedef struct LORENTZ {
  struct LORENTZ *next;
  real min_energy, max_energy, gamma, q;
} *lorentz;
