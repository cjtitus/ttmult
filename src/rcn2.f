C -- CDC
C     program RCN2 ( TAPE10 , TAPE9 )
C -- VAX
      program RCN2

C      PROGRAM RCN231(INPUT=64,TAPE10=INPUT,OUTPUT=64,TAPE9=OUTPUT,
C     1  TAPE2=512,TAPE11=64,TAPE31=512,TAPE32=512)
C
C          THIS PROGRAM READS RADIAL WAVEFUNCTIONS FROM TAPE (OR DISK)
C               UNIT NUMBER 2 PREPARED BY PROGRAM RCN OR HF, AND (USING
C               INPUT OPTION G5INP) CALCULATES CONFIGURATION-INTERACTION
C               PARAMETERS (RK) AND RADIAL ELECTRIC MULTIPOLE INTEGRALS,
C               SCALES THE ENERGY-LEVEL PARAMETER VALUES (FK, GK, ZETA,
C               AND RK), AND PUNCHES COMPLETE INPUT CARD DECKS FOR
C               PROGRAM RCG mod 5, 6, 7, 8, OR 9.
C
      implicit real*8 (a-h,o-z)
      PARAMETER (KBGKS=41,KCASE=50)
      DIMENSION X(1801),RU(1801),PNL(1801,20),NNLZ(25),R(1801),VPAR(45),
     & WWNL(25),EE(25),NKKK(25),P(1801,4),
     & PA(1801),PB(1801),PC(1801),PD(1801),NCFG(2),NLZ(4),NKK(4),L(4)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ,NCFG,MMIN,NCONFG,IZ,Z,NCSPVS,IDB,
     &  NCDES,WWNL,NPAR,VPAR,NNLZ,NKKK,NKK,ION,M,MESH,
     &  C,EE,X,R,RU,KUT,L,EXF,CORRF,CA1,CA0,IEXCH,IZ1,OVERLP,
     &  DUMMY(1801,7)
C
      EQUIVALENCE (P,PA),(P(1,2),PB),(P(1,3),PC),(P(1,4),PD),
     &  (L(1),LA),(L(2),LB),(L(3),LC),(L(4),LD)
      COMMON/C1/FACT(5),IFACT(5),DUMMY2(27)
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/CP/P
      COMMON/LCM1/PNL
      COMMON/GENOSC/IGEN,NBIGKS,IBK,BIGKS(KBGKS)
C
C
c     open out file on unit 11 as CRCC
C VAX
C     open(unit=11,status='new',carriagecontrol='list')
c
C     HP-UX problems
      OPEN(2, STATUS = 'UNKNOWN', FILE = 'FTN02',
     &     ACCESS = 'SEQUENTIAL', FORM = 'UNFORMATTED')
      OPEN(31, STATUS = 'SCRATCH', ACCESS = 'SEQUENTIAL',
     &     FORM = 'UNFORMATTED')
      OPEN(32, STATUS = 'SCRATCH', ACCESS = 'SEQUENTIAL',
     &     FORM = 'UNFORMATTED')
      REWIND 2
      IC=2
      ICN(1)=2
      ICN(2)=2
      NCONFG=99
      NCONFN(1)=99
      NCONFN(2)=99
      SUBR0=' '
      P(1,1)=-99.0
C
  200 READ (10,19) DH
   19 FORMAT (A80)
      SUBR=DH(1:3)
      if (SUBR.ne.'G5I') then
      READ (DH,21) SUBR,IZ1,NCFG(1),NCFG(2),HA,HB,HC,HD,MMIN
   21 FORMAT (A3,4X,I3,2I5, 4(2X,A3), I5)
      if (IZ1.lt.0) then
      WRITE (11,'(A5)') '   -1'
      STOP 'NORMAL END'
      ENDIF
      ENDIF

           if (SUBR.eq.'DIP') then
      CALL DIP
      else if (SUBR.eq.'OVE') then
      CALL OVER
      else if (SUBR.eq.'SLI') then
      CALL SLI2
      else if (SUBR.eq.'ZET') then
      CALL ZETA2
      else if (SUBR.eq.'G5I') then
      CALL G5INP
      ENDIF
      GOTO 200
      END


      SUBROUTINE OVER
C
C          EVALUATE OVERLAP INTEGRALS
C
      implicit real*8 (a-h,o-z)
      DIMENSION X(1801),RU(1801),PNL(1801,20),NNLZ(25),R(1801),VPAR(45),
     & WWNL(25),EE(25),NKKK(25),P(1801,4),
     & PA(1801),PB(1801),PC(1801),PD(1801),NCFG(2),NLZ(4),NKK(4),L(4),
     & WNL(2),NLH(4)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ,NCFG,MMIN,NCONFG,IZ,Z,NCSPVS,IDB,
     &  NCDES,WWNL,NPAR,VPAR,NNLZ,NKKK,NKK,ION,M,MESH,
     &  C,EE,X,R,RU,KUT,L,EXF,CORRF,CA1,CA0,IEXCH,IZ1,OVERLP,
     &  absOVR,FRAC
C
      EQUIVALENCE (P,PA),(P(1,2),PB),(P(1,3),PC),(P(1,4),PD),
     &  (L(1),LA),(L(2),LB),(L(3),LC),(L(4),LD),(NLH,HA)
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/CP/P
      COMMON/LCM1/PNL
C
      K=1
      if (IC.GT.30) K=IC-30
      if (SUBR.eq.'   ') go to 105
      if (SUBR.eq.SUBR0) go to 150
      WRITE (9,10)
   10 FORMAT (1H1)
  105 SUBR0=SUBR
      if (ISLI.GT.0) go to 200
      WRITE (9,11)
   11 FORMAT (1H0//15H  Z  KUT       ,9X,5HCONF1,17X,5HCONF2,15X,
     &  16HOVERLAP INTEGRAL,11X,8HFRACTION/17H --- ---         ,
     &  22H-------------------   ,23H-------------------    ,
     &  24H------------------------,7X,8H--------/)
      go to 200
C
  150 if (NCFG(1).ne.NCONFN(K).or.HA.ne.HA1) go to 200
      N=2
      go to 251
C
C          READ WAVEFUNCTIONS FROM TAPE IC AND SELECT THOSE DESIRED
C
  200 N=1
      IZ0=IZ1
      HA1=HA
      if (ISLI.GT.0) go to 205
      WRITE (9,20)
   20 FORMAT (1H )
  205 if (NCFG(N).eq.NCONFG) go to 220
      if (NCFG(N)-NCONFN(K)) 210,206,211
  206 BACKSPACE IC
      go to 211
  210 REWIND IC
  211 READ (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,
     &  (IA,IA,WWNL(I),NLBCD(I),NNLZ(I),NKKK(I),EE(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS)
      if (NCONFG.ne.NCFG(N)) go to 211
      NCONFN(K)=NCONFG
C
  220 if (N.GT.1) go to 223
      CONF1=CONFG
      go to 225
  223 CONF2=CONFG
  225 DO 226 M=1,NCSPVS
      if (NLBCD(M).eq.NLH(N)) go to 230
  226 CONTINUE
      go to 1000
C
  230 NKK(N)=NKKK(M)
      NLZ(N)=NNLZ(M)
      WNL(N)=WWNL(M)
      L(N)=NNLZ(M)-100*(NNLZ(M)/100)
      DO 231 I=1,MESH
  231 P(I,N)=PNL(I,M)
C
      N=N+1
  251 if (N-2) 200,205,300
C
C          BEGIN INTEGRATION LOOP
C
  300 NF=-1
      OVERLP=0.0
      absOVR=0.0
      IMAX=min(NKK(1),NKK(2))
      DO 399 I=1,IMAX
      if (NF) 340,350,360
  340 ERAS=1.5
      if (I-1) 342,341,342
  341 ERAS=1.0
      go to 344
  342 if (I-IMAX) 344,343,344
  343 ERAS=0.5
  344 ERAS=PA(I)*PB(I)*ERAS
      OVERLP=(OVERLP+ERAS)*0.5
      absOVR=(absOVR+abs(ERAS))*0.5
      NF=0
      NOCYC=40
      if (I.ge.IDB) NOCYC=IMAX-I
      go to 399
C
  350 ERAS=PA(I)*PB(I)
      ERAS=ERAS+ERAS
      OVERLP=OVERLP+ERAS
      absOVR=absOVR+abs(ERAS)
      NF=1
      NOCYC=NOCYC-2
      if (NOCYC) 399,351,399
  351 NF=-1
      go to 399
C
  360 ERAS=PA(I)*PB(I)
      OVERLP=OVERLP+ERAS
      absOVR=absOVR+abs(ERAS)
      NF=0
C
  399 CONTINUE
C
C          COMPLETE CALCULATION OF INTEGRAL
C
      FRAC=OVERLP/absOVR
      OVERLP=OVERLP*(R(IMAX)-R(IMAX-1))/0.75
      if (ISLI.GT.0) go to 450
      WRITE (9,41) IZ,KUT,CONF1,CONF2,HA,HB,OVERLP,FRAC
   41 FORMAT (2I4,6X, 2(3X,A18,1X), 5H    (,A3,1H/,A3,2H)=,F14.9,F15.6)
  450 RETURN
C
 1000 WRITE (9,1001) NLH(N),CONFG
 1001 FORMAT (//9H ORBITAL ,A3, 23H NOT FOUND FOR CONFIG  ,A18)
      go to 450
C
      END


      SUBROUTINE ZETA2
C
C          CALCULATE SPIN-ORBIT CONFIGURATION-INTERACTION INTEGRALS
C
C
C
      implicit real*8 (a-h,o-z)
      DIMENSION DRUDR(1801),DERU(1801)
      DIMENSION X(1801),RU(1801),PNL(1801,20),NNLZ(25),R(1801),VPAR(45),
     & WWNL(25),EE(25),NKKK(25),P(1801,4),
     & PA(1801),                       NCFG(2),NLZ(4),NKK(4),L(4),
     & V(1801),ZEI(1801),NLH(4)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ,NCFG,MMIN,NCONFG,IZ,Z,NCSPVS,IDB,
     &  NCDES,WWNL,NPAR,VPAR,NNLZ,NKKK,NKK,ION,M,MESH,
     &  C,EE,X,R,RU,KUT,L,EXF,CORRF,CA1,CA0,IEXCH,IZ1,OVERLP,
     &  ZETA,ZETAK,ZEI
C
      EQUIVALENCE (P,PA),(P(1,2),DRUDR),(P(1,3),DERU),(P(1,4),V),
     &  (L(1),LA),(L(2),LB),(L(3),LC),(L(4),LD),(NLH,HA)
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/CP/P
      COMMON/LCM1/PNL
C
      if (SUBR.ne.SUBR0) go to 114
      if (NCFG(2)-NCONFG) 114,150,114
  114 SUBR0=SUBR
      WRITE (9,10)
   10 FORMAT (11H1RCN mod 30///)
C
C
C          READ WAVEFUNCTIONS FROM TAPE IC AND SELECT THOSE DESIRED
C
  150 N=1
      K=1
  205 if (IC.GT.30) K=IC-30
      if (NCFG(N).eq.NCONFG) go to 220
      if (NCFG(N)-NCONFN(K)) 210,206,211
  206 BACKSPACE IC
      go to 211
  210 REWIND IC
  211 READ (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,
     &  (IA,IA,WWNL(I),NLBCD(I),NNLZ(I),NKKK(I),EE(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS),(RU(I),I=1,MESH)
      if (NCONFG.ne.NCFG(N)) go to 211
      NCONFN(K)=NCONFG
C
  220 CONF1=CONF2
      CONF2=CONFG
C
      DO 222 M=1,NCSPVS
      if (NLBCD(M).eq.NLH(N)) go to 225
  222 CONTINUE
      go to 1000
  225 NKK(N)=NKKK(M)
      NLZ(N)=NNLZ(M)
      L(N)=NNLZ(M)-100*(NNLZ(M)/100)
C
      N=N+1
      if (N.GT.2) go to 350
      DO 231 I=1,MESH
  231 PA(I)=PNL(I,M)
      go to 205
C
  350 WRITE (9,35) CONF1,CONF2,IZ,KUT
   35 FORMAT ( 1H ,A18,5X,A18,6X,2HZ=,I3,6X,4HKUT=,I2///
     &  6X,32HNL   NL  ZETA(RYD)    ZETA(CM-1) )
C
C          CALCULATE V AND DERIVATIVE OF RU
C
      DR3=3.0*(R(2)-R(1))
      KKK=min(NKK(1),NKK(2))
      NBLOCK=KKK/40
      I=1
      DO 420 NB=1,NBLOCK
      DO 410 J=1,10
      I=I+4
      DRUDR(I-3)=(-5.5*RU(I-3)+9.0*RU(I-2)-4.5*RU(I-1)+RU(I))/DR3
      DRUDR(I-2)=(-RU(I-3)-1.5*RU(I-2)+3.0*RU(I-1)-0.5*RU(I))/DR3
      DRUDR(I-1)=(0.5*RU(I-3)-3.0*RU(I-2)+1.5*RU(I-1)+RU(I))/DR3
  410 DRUDR(I)=(-RU(I-3)+4.5*RU(I-2)-9.0*RU(I-1)+5.5*RU(I))/DR3
      if (I.GT.IDB) go to 420
      DR3=DR3+DR3
  420 CONTINUE
C
      TWOZ=Z+Z
      DO 450 I=2,KKK
      V(I)=RU(I)/R(I)
  450 DERU(I)=(DRUDR(I)-V(I))/(R(I)**2)
C
      DO 520 I=2,KKK
      PSQ=PA(I)*PNL(I,M)
  520 ZEI(I)=PSQ*DERU(I)
C
C          INTEGRATE
C
      ZEI(1)=0.0
      XIF=0.00125*C*5.0/288.0
      J=1
      S1Z=0.0
  530 MM=8
      if (I.GT.IDB) go to 531
      XIF=XIF+XIF
  531 CONTINUE
      S2Z=0.0
  532 S2Z=S2Z+19.0*(ZEI(J)+ZEI(J+5))+75.0*(ZEI(J+1)+ZEI(J+4))
     &       +50.0*(ZEI(J+2)+ZEI(J+3))
      J=J+5
      MM=MM-1
      if (MM) 534,534,532
  534 S1Z=S1Z+XIF*S2Z
      if (J-KKK) 530,535,535
  535 ZETA=2.66246E-5*S1Z
      ZETAK=ZETA*13.6054
C
      WRITE (9,70) HA,HB,ZETA,ZETAK
   70 FORMAT (5X,A3,2X,A3,F11.5,F14.3////)
C
C
  900 RETURN
C
 1000 WRITE (9,1001) NLH(N),CONFG
 1001 FORMAT (//9H ORBITAL ,A3, 23H NOT FOUND FOR CONFIG  ,A18)
       go to 900
      END


      SUBROUTINE DIP
C
C          CALCULATE ELECTRIC DIPOLE OR QUADRUPOLE RADIAL INTEGRALS
C
      implicit real*8 (a-h,o-z)
      PARAMETER (KBGKS=41,KCASE=50)
      DIMENSION X(1801),RU(1801),PNL(1801,20),NNLZ(25),R(1801),VPAR(45),
     & WWNL(25),EE(25),NKKK(25),P(1801,4),
     & PA(1801),PB(1801),PC(1801),PD(1801),NCFG(2),NLZ(4),NKK(4),L(4),
     & WNL(2),NLH(4)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ,NCFG,MMIN,NCONFG,IZ,Z,NCSPVS,IDB,
     &  NCDES,WWNL,NPAR,VPAR,NNLZ,NKKK,NKK,ION,M,MESH,
     &  C,EE,X,R,RU,KUT,L,EXF,CORRF,CA1,CA0,IEXCH,IZ1,OVERLP,
     &  DIPOLE,absDIP,FRAC,REDDIP
      COMMON/C2/NCPD,EX(51),STN(51),WT(51),GAMMA(51),Q(51),
     &  Q2(51),PDIPSC(51),RRSC(51,12),NCD,NFIRST,NCONT1,IPWR
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/CP/P
      COMMON/GENOSC/IGEN,NBIGKS,IBK,BIGKS(KBGKS)
C
      EQUIVALENCE (P,PA),(P(1,2),PB),(P(1,3),PC),(P(1,4),PD),
     &  (L(1),LA),(L(2),LB),(L(3),LC),(L(4),LD),(NLH,HA)
      COMMON/LCM1/PNL
      COMMON/LCM2/RDIPS(KBGKS,KCASE),ISTPWR(KCASE),STFRAC(KCASE),
     &            ISTOVER(KCASE),ISTF1(KCASE)
      CHARACTER STCONF1*18,STCONF2*18,STHA*3,STHB*3,STHXID*2
      COMMON /LCM2CH/ STCONF1(KCASE),STCONF2(KCASE),STHA(KCASE),
     &                   STHB(KCASE),STHXID(KCASE)
C
      ITEST=1
      if (SUBR.eq.SUBR0) go to 150
      SUBR0=SUBR
      if (IGEN.le.8) WRITE (9,10)
   10 FORMAT (1H1//19H  Z  KUT  CA1  CA0 ,7X,5HCONF1,15X,5HCONF2,8X,
     &  10HR INTEGRAL,6X,22HREDUCED MUPOLE ELEMENT,7X,4HFRAC,4X,
     &  8HSIGMA SQ,12H  OVER   FR1/
     &  19H --- --- ---- ---- ,20H------------------- ,
     &  20H------------------- ,10H----------,3X,14H--------------,
     &  14H--------------,3X,6H------,2X, 9H---------,12H ----- -----/)
      go to 200
C
  150 if (NCFG(1).ne.NCFG1.or.HA.ne.HA1) go to 200
      N=2
      go to 251
C
C          READ WAVEFUNCTIONS FROM TAPE IC AND SELECT THOSE DESIRED
C
  200 N=1
      IC=ICN(1)
      NCFG1=NCFG(1)
      HA1=HA
      K=1
      WRITE (9,20)
   20 FORMAT (1H )
  205 if (IC.GT.30) K=IC-30
      if (NCFG(N).eq.NCONFG) go to 220
      if (NCFG(N)-NCONFN(K)) 210,206,211
  206 BACKSPACE IC
      go to 211
  210 REWIND IC
  211 READ (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,
     &  (IA,IA,WWNL(I),NLBCD(I),NNLZ(I),NKKK(I),EE(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS)
      if (NCONFG.ne.NCFG(N)) go to 211
      NCONFN(K)=NCONFG
C
  220 if (N.le.1) then
      CONF1=CONFG
      else
      CONF2=CONFG
      ENDIF
      if (MMIN.lt.0) go to 250
      DO 226 M=1,NCSPVS
      if (NLBCD(M).eq.NLH(N)) go to 230
  226 CONTINUE
      go to 1000
C
  230 NKK(N)=NKKK(M)
      NLZ(N)=NNLZ(M)
      WNL(N)=WWNL(M)
      L(N)=NNLZ(M)-100*(NNLZ(M)/100)
      DO 231 I=1,MESH
  231 P(I,N)=PNL(I,M)
C
  250 N=N+1
  251 IC=ICN(2)
      if (N-2) 200,205,300
C
C          BEGIN INTEGRATION LOOP
C
  300 PWRT=2*IPWR+1
      PWRT=SQRT(PWRT)
      DO 450 IBK=1,NBIGKS
      if (MMIN.lt.0) go to 500
      DIPOLE=0.0
      absDIP=0.0
      RPWR=0.
      NF=-1
      OVERLP=0.0
      absOVR=0.0
      IMAX=min(NKK(1),NKK(2))
      DO 399 I=1,IMAX
      RIP=R(I)
      if (I.eq.1) go to 320
      RPWR=RIP**IPWR
      if (IGEN.lt.5) go to 320
      BIGK=BIGKS(IBK)
      BIGK2=BIGK**2
      CALL SBESS(RIP,ITEST,IPWR,BIGK2,SZ,SZP)
      RPWR=PWRT*SZ/(RIP*BIGK2)
  320 CONTINUE
      if (NF) 340,350,360
  340 ERAS=1.5
      if (I-1) 342,341,342
  341 ERAS=1.0
      go to 344
  342 if (I-IMAX) 344,343,344
  343 ERAS=0.5
  344 ERAS1=PA(I)*PB(I)*ERAS
      ERAS=RPWR*ERAS1
      DIPOLE=(DIPOLE+ERAS)*0.5
      absDIP=(absDIP+abs(ERAS))*0.5
      OVERLP=0.5*(OVERLP+ERAS1)
      absOVR=0.5*(absOVR+abs(ERAS1))
      NF=0
      NOCYC=40
      if (I.ge.IDB) NOCYC=IMAX-I
      go to 399
C
  350 ERAS1=2.0*PA(I)*PB(I)
      ERAS=RPWR*ERAS1
      DIPOLE=DIPOLE+ERAS
      absDIP=absDIP+abs(ERAS)
      OVERLP=OVERLP+ERAS1
      absOVR=absOVR+abs(ERAS1)
      NF=1
      NOCYC=NOCYC-2
      if (NOCYC) 399,351,399
  351 NF=-1
      go to 399
C
  360 ERAS1=PA(I)*PB(I)
      ERAS=RPWR*ERAS1
      DIPOLE=DIPOLE+ERAS
      absDIP=absDIP+abs(ERAS)
      OVERLP=OVERLP+ERAS1
      absOVR=absOVR+abs(ERAS1)
      NF=0
C
  399 CONTINUE
C
C          COMPLETE CALCULATION OF INTEGRAL
C
      FRAC=DIPOLE/absDIP
      FRAC1=OVERLP/absOVR
      if (FRAC1.GT.0) FRAC1=-FRAC1
      DIPOLE=DIPOLE*(R(IMAX)-R(IMAX-1))/0.75
      OVERLP=OVERLP*(R(IMAX)-R(IMAX-1))/0.75
      if (IPWR.eq.0) DIPOLE=DIPOLE-OVERLP/BIGK
      A=LA
      B=LB
      C=IPWR
      Zero = 0
      REDDIP=((-1.0)**LA)*SQRT((2.0*A+1.0)*(2.0*B+1.0))
     &  *S3J ( A,B,C, Zero, Zero) * DIPOLE
      PDIPSC(NCPD)=REDDIP
      LGR=max(LA,LB)
      FLGR=LGR
      SIGSQ=DIPOLE*DIPOLE/(4.0*FLGR*FLGR-1.0)
  410 if (IGEN.lt.5) go to 420
      if ((IGEN.le.5.and.REDDIP.ne.0).or.(IGEN.le.8.and.IBK.eq.1))
     &  go to 420
      go to 430
  420 WRITE (9,41) IZ,KUT,CA1,CA0,CONF1,CONF2,
     &  DIPOLE,  HA,RJ,IPWR,HB, REDDIP , FRAC, SIGSQ,OVERLP,FRAC1
   41 FORMAT (1X,2I3,1X,2F5.2,    2(1X,A18,1X), F11.6, 3X, 1H(,A3,
     &  2H//,A1,I1,2H//,A3, 2H)=, F10.5, 3H A0, F9.4, F11.6,2F6.3)
  430 if (NCFG(2).ge.NCONT1+NC0) REDDIP=0.0
      IOVER=100.4*OVERLP
      IF1=100.4*FRAC1
      if (IGEN.ge.5) go to 440
      if (IDIP.ne.8) go to 432
      FRAC=1.0
      go to 435
  432 if (IDIP.lt.7) go to 435
      I=NCFG(2)-NC0
      REDDIP=DEL(I)*REDDIP
      WRITE (9,42) DEL(I),REDDIP
   42 FORMAT (30X,9H SQRTDEL=,F10.4,38X,F10.5)
  435 WRITE (11,43) CONF1,CONF2,REDDIP,HA,IPWR,HB,FRAC,HXID,IOVER,IF1
   43 FORMAT (A18,2X,A18,F12.5,1H(,A3,3H//R,I1,2H//,A3,1H),F6.3,A2,2I4)
      go to 450
  440 if (IBK.GT.1) go to 449
      ICASE=ICASE+1
      if (ICASE.GT.KCASE) STOP 44
      STCONF1(ICASE)=CONF1
      STCONF2(ICASE)=CONF2
      ISTPWR(ICASE)=IPWR
      STFRAC(ICASE)=FRAC
      STHXID(ICASE)=HXID
      ISTOVER(ICASE)=IOVER
      ISTF1(ICASE)=IF1
      STHA(ICASE)=HA
      STHB(ICASE)=HB
  449 RDIPS(IBK,ICASE)=REDDIP
  450 CONTINUE
      RETURN
C
  500 HA=' '
      HB=' '
      HA1=' '
      DIPOLE=0.0
      REDDIP=0.0
      FRAC=0.0
      SIGSQ=0.0
      OVERLP=0.0
      FRAC1=0.0
      go to 410
C
 1000 WRITE (9,1001) NLH(N),CONFG
 1001 FORMAT (//9H ORBITAL ,A3, 23H NOT FOUND FOR CONFIG  ,A18)
      RETURN
C
      END


      SUBROUTINE SLI2
C
C          CALCULATE CONFIGURATION-INTERACTION COULOMB INTEGRALS RK
C
      implicit real*8 (a-h,o-z)
      DIMENSION X(1801),RU(1801),PNL(1801,20),NNLZ(25),R(1801),VPAR(45),
     & WWNL(25),EE(25),NKKK(25),P(1801,4),
     & PA(1801),PB(1801),PC(1801),PD(1801),NCFG(2),NLZ(4),NKK(4),L(4),
     & WNL(2),NLH(4)
C
      DIMENSION K1(10,2),RRYD(10,2),REV(10,2),FRAC(10,2),EAV(2),EPS(2)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ,NCFG,MMIN,NCONFG,IZ,Z,NCSPVS,IDB,
     &  NCDES,WWNL,NPAR,VPAR,NNLZ,NKKK,NKK,DUMMY(6),ION,M,MESH,
     &  C,EE,X,R,RU,KUT,L,EXF,CORRF,CA1,CA0,IEXCH,IZ1,OVERLP,
     &  XI(1801),XJ(1801),XA(1801),XB(1801),F1(1801),F2(1801),RKP1(1801)
C
      EQUIVALENCE (P,PA),(P(1,2),PB),(P(1,3),PC),(P(1,4),PD),
     &  (L(1),LA),(L(2),LB),(L(3),LC),(L(4),LD),(NLH,HA)
      DIMENSION RR1(10,2,3),RK1(10,2,3),KK(4),WW(4),NCFGO(2)
      EQUIVALENCE (KK(1),KA),(KK(2),KB),(KK(3),KC),(KK(4),KD),
     &            (WW(1),WA),(WW(2),WB),(WW(3),WC),(WW(4),WD)
C
      COMMON/C1/FACT(5),IFACT(5),NRK,REVSC(20)
      COMMON/C2/NCPD,EX(51),STN(51),WT(51),GAMMA(51),Q(51),
     &  Q2(51),PDIPSC(51),RRSC(51,12),NCD,NFIRST,NCONT1,IPWR
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/C4/IRHO,ISIG,IRHOP,ISIGP,N12,T0
      COMMON/CP/P
      COMMON/LCM1/PNL
C
      if (P(1,1).eq.(-99.0)) NCFGO(1)=-7
      if (P(1,1).eq.(-99.0)) NCSO=-7
      if (NCFG(1).ne.NCFGO(1).or.NCFG(2).ne.NCFGO(2)) ITEST=0
      NCS=1000*NCFG(1)+NCFG(2)
      if (ISLI.eq.0) go to 110
      WRITE (9,95)
      go to 120
  110 WRITE (9,10)
   10 FORMAT (11H1RCN mod 30//)
C          DETERMINE WHETHER INTERACTION IS CLASS 1B,7, OR 8
C
  120 ICLASS=0
      LA1=LA
      LB1=LB
C
      DO 125 I=1,4
  125 L(I)=ICHAR(NLH(I)(3:3))
      if (HA.eq.HC) go to 150
      if (HB.eq.HD) go to 130
      if (HB.eq.HC.or.HA.eq.HD) go to 140
      go to 150
  130 if (LA.ne.LC) go to 150
      HA1=HB
      HB=HA
      HA=HA1
      HA1=HD
      HD=HC
      HC=HA1
      ICLASS=8
      if (HA.eq.HB.or.HC.eq.HD) ICLASS=1
      go to 150
  140 if (LA.ne.LD.or.LB.ne.LC) go to 150
      ICLASS=7
      if (HA.eq.HD) go to 145
      HA1=HB
      HB=HA
      HA=HA1
      go to 150
  145 HA1=HD
      HD=HC
      HC=HA1
C
C          READ WAVEFUNCTIONS FROM TAPE IC AND SELECT THOSE DESIRED
C
  150 N=N12
      NPOS=0
      if (N12.eq.2) NPOS=NPOS1
      LA=LA1
      LB=LB1
      EEE=0.0
      CALL CLOKC(T1)
      IREAD=0
      if (NCFG(N12)-NCONFG) 160,220,161
  160 REWIND IC
  161 READ (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,
     &  (IA,IA,WWNL(I),NLBCD(I),NNLZ(I),NKKK(I),EE(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS)
      EAV(N)=VPAR(1)
      EPS(N)=EE(NCSPVS)
      IREAD=IREAD+1
      if (NCONFG.ne.NCFG(N)) go to 161
      K=1
      if (IC.GT.30) K=IC-30
      NCONFN(K)=NCONFG
C
  220 if (N12.eq.1) CONF1=CONF2
      CONF2=CONFG
C
      DO 229 J=1,2
      K=2*N-2+J
      DO 222 M=1,NCSPVS
      if (NLBCD(M).eq.NLH(K)) go to 225
  222 CONTINUE
      go to 1000
  225 NKK(K)=NKKK(M)
      NLZ(K)=NNLZ(M)
      L(K)=NNLZ(M)-100*(NNLZ(M)/100)
      WW(K)=WWNL(M)
      KK(K)=M
      IP=MESH
      DO 226 I=1,IP
  226 P(I,K)=PNL(I,M)
  229 CONTINUE
      M=KK(2*N)
      if (EE(M).ge.0) NPOS=NPOS+1
      if (N.eq.1) NPOS1=NPOS
      EEE=EEE+0.5*EE(M)
      if (ISLI.le.0) WRITE (9,22) EE(M),EEE
   22 FORMAT (8H EE,EEE=,2F10.5)
C
      N=N+1
      if (N.GT.2) go to 250
      if (NCFG(2)-NCONFG) 160,231,161
  231 HC=HA
      HD=HB
      go to 220
C
C          DETERMINE WHETHER INTERACTION IS CLASS 1A,6, OR 11
C
  250 if (ICLASS.ne.0) go to 300
      if (HA.ne.HC) go to 300
      if (LB.ne.LD) go to 300
      if ((WB+WD).eq.2.0) ICLASS=11
      if (ISIG.ne.ISIGP) ICLASS=6
      if (HA.eq.HB.or.HC.eq.HD) ICLASS=1
C
C          SET UP K TABLES FOR DIRECT (D) AND EXCHANGE (E) INTEGRALS
C
  300 KDMIN=max(abs(LA-LC),abs(LB-LD))
      KEMIN=max(abs(LA-LD),abs(LB-LC))
      KDMAX=min(LA+LC, LB+LD)
      KEMAX=min(LA+LD, LB+LC)
      KMIN=min(KDMIN,KEMIN)
      KMAX=max(KDMAX,KEMAX)
      NOKD=(KDMAX-KDMIN)/2+1
      NOKE=(KEMAX-KEMIN)/2+1
      NOKMAX=max(NOKD,NOKE)
      CALL CLOKC(T2)
C
C          CALCULATE INTEGRATION LIMITS
C
      I1MAX=max( min(NKK(1),NKK(3)), min(NKK(1),NKK(4)) )
      I2MAX=max( min(NKK(2),NKK(4)), min(NKK(2),NKK(3)) )
      IMAX=max(I1MAX,I2MAX)
C
C          CALC PNL-PRODUCTS FOR DIRECT OR EXCHANGE CASE
C
  400 DO 700 N1=1,2
      if (N1-1) 420,420,412
  412 DO 415 I=1,IMAX
      F1(I)=PA(I)*PD(I)
  415 F2(I)=PB(I)*PC(I)
      K=KEMIN-2
      go to 500
  420 DO 425 I=1,IMAX
      F1(I)=PA(I)*PC(I)
  425 F2(I)=PB(I)*PD(I)
      K=KDMIN-2
C
C          CALC FOR EACH VALUE OF K
C
  500 DO 700 N=1,NOKMAX
      K1(N,N1)=0
      K=abs(K+2)
      K1(N,N1)=K
C
      if (MMIN.ge.0) go to 522
      A1=0.0
      B1=1.0
      ICLASS=0
      go to 650
  522 RKP1(1)=0.001
      DO 530 I=2,IMAX
      if (N-1) 525,525,529
  525 RKP1(I)=R(I)
      if (K) 530,530,526
  526 DO 527 M=1,K
  527 RKP1(I)=RKP1(I)*R(I)
      go to 530
  529 RKP1(I)=RKP1(I)*R(I)*R(I)
  530 CONTINUE
C
C          CALC R1 INTEGRAL
C
      XI(1)=0.0
      XJ(1)=0.0
      XA(1)=0.0
      XB(1)=0.0
      A0=0.0
      B0=0.0
      HO12=R(2)/12.0
      NI=40
      DO 550 I=3,I1MAX,2
      ERAS=8.0*F1(I-1)
      A1=ERAS*RKP1(I-1)/R(I-1)
      B1=ERAS/RKP1(I-1)
      A2=F1(I)*RKP1(I)/R(I)
      B2=F1(I)/RKP1(I)
      ERAS=HO12*(5.0*A0+A1-A2)
      XI(I-1)=XI(I-2)+ERAS
      XA(I-1)=XA(I-2)+abs(ERAS)
      ERAS=HO12*(5.0*B0+B1-B2)
      XJ(I-1)=XJ(I-2)+ERAS
      XB(I-1)=XB(I-2)+abs(ERAS)
      ERAS=HO12*(-A0+A1+5.0*A2)
      XI(I)=XI(I-1)+ERAS
      XA(I)=XA(I-1)+abs(ERAS)
      ERAS=HO12*(-B0+B1+5.0*B2)
      XJ(I)=XJ(I-1)+ERAS
      XB(I)=XB(I-1)+abs(ERAS)
      A0=A2
      B0=B2
      NI=NI-2
      if (NI) 540,540,550
  540 HO12=HO12+HO12
      NI=40
      if (I.ge.IDB) NI=10000
  550 CONTINUE
C
      DO 560 I=2,I1MAX
      ERAS=RKP1(I)/R(I)
      XI(I)=XI(I)/RKP1(I)+(XJ(I1MAX)-XJ(I))*ERAS
  560 XA(I)=XA(I)/RKP1(I)+(XB(I1MAX)-XB(I))*ERAS
      if (I2MAX-I1MAX) 600,600,569
  569 A0=XI(I1MAX)*RKP1(I1MAX)
      B0=XA(I1MAX)*RKP1(I1MAX)
      DO 570 I=I1MAX,I2MAX
      XI(I)=A0/RKP1(I)
  570 XA(I)=B0/RKP1(I)
C
C          CALC R2 INTEGRAL
C
  600 A0=0.0
      B0=0.0
      A1=0.0
      B1=0.0
      HO3=R(2)/1.5
      NI=40
      DO 620 I=3,I2MAX,2
      A2=F2(I)*XI(I)
      B2=abs(F2(I))*XA(I)
      ERAS=4.0*F2(I-1)
      A1=A1+HO3*(A0+ERAS*XI(I-1)+A2)
      B1=B1+HO3*(B0+abs(ERAS)*XA(I-1)+B2)
      XJ(I)=A1
      A0=A2
      B0=B2
      NI=NI-2
      if (NI) 610,610,620
  610 HO3=HO3+HO3
      NI=40
      if (I.ge.IDB) NI=10000
  620 CONTINUE
      if (NPOS.ne.2) go to 650
      AMIN=1.0E35
      AMAX=-1.0E35
      JN=0
      JX=0
      I=I2MAX+2
      IX=I2MAX/4
      DO 630 J=1,IX
      I=I-2
      if (XJ(I).le.AMAX) go to 622
      AMAX=XJ(I)
      JX=JX+1
  622 if (XJ(I).ge.AMIN) go to 624
      AMIN=XJ(I)
      JN=JN+1
  624 if (XJ(I).lt.AMAX.and.XJ(I).GT.AMIN.and.JN.GT.3.and.JX.GT.3)
     &  go to 640
  630 CONTINUE
  640 A1=0.5*(AMIN+AMAX)
      if (ISLI.GT.0) go to 650
      WRITE (9,8764) AMIN,AMAX,A1
 8764 FORMAT ( 3F15.6)
C
  650 FRAC(N,N1)=A1/B1
      RRYD(N,N1)=A1
  700 REV(N,N1)=A1*13.6054
C
C          OUTPUT
C
      MMINP1=max(MMIN+1,1)
      DO 890 N=1,NOKMAX
      DO 890 N1=1,2
      RR1(N,N1,MMINP1)=RRYD(N,N1)
  890 RK1(N,N1,MMINP1)=REV(N,N1)
      if (ISLI.GT.0) go to 935
      if (MMIN.GT.0) go to 930
      WRITE (9,90) CONF1,NCFG(1),IZ,ION,EXF,IREL,CONF2,NCFG(2),KUT,
     &  CORRF,ICLASS
   90 FORMAT (/23H0 SLATER INTEGRALS FOR   ,A18,6X,6HNCONF=,I2,
     &      14X,2HZ=,I3,3X,4HION=,I2,5X,4HEXF=,F5.3,11X,5HIREL=,I2/
     &    23X,A18,6X,6HNCONF=,I2,22X,4HKUT=,I2,3X,6HCORRF=,F5.3,
     &  10X,6HCLASS ,I2)
      if (NCFG(1)-NCFG(2)) 920,910,920
  910 WRITE (9,91) HA,HB,HA,HB
   91 FORMAT (/1H0,8X,1HK,14X,3HFK(,A3,1H,,A3,1H),21X,4HFRAC,
     &             9X,1HK,14X,3HGK(,A3,1H,,A3,1H),21X,4HFRAC)
      go to 930
  920 WRITE (9,92) HA,HB,HC,HD,HA,HB,HC,HD
   92 FORMAT (
     &  /1H0,8X,1HK,10X,4HRDK(,A3,1H,,A3,1H,,A3,1H,,A3,1H),16X,4HFRAC,
     &       9X,1HK,10X,4HREK(,A3,1H,,A3,1H,,A3,1H,,A3,1H),16X,4HFRAC)
  930 WRITE (9,93)
   93 FORMAT (1H ,8X,43H-     -------------------------------------,
     & 8H   -----,9X,43H-     -------------------------------------,
     & 8H   -----//)
  935 if (ISLI.GT.1) go to 945
      if (ISLI.eq.1.and.ICLASS.ne.0.and.MMIN.ge.0.and.MMIN.lt.3)
     &  go to 945
      DO 940 N=1,NOKMAX
  940 WRITE (9,94) (K1(N,N1),RRYD(N,N1),REV(N,N1),
     &      FRAC(N,N1), N1=1,2)
   94 FORMAT (I10,F16.8,7H RYD  =,F14.4,5H CM-1,F8.3,
     &        I10,F16.8,7H RYD  =,F14.4,5H CM-1,F8.3)
      WRITE (9,95)
   95 FORMAT (1H0)
C
  945 AA=0.0
      A=0.0
      if (ICLASS.eq.0) go to 980
      MMIN=MMIN+1
      go to (950,960,965,970), MMIN
  950 DO 951 I=1,IP
      PNL(I,1)=PC(I)
  951 PC(I)=PA(I)
      go to 400
  960 DO 962 I=1,IP
      PNL(I,2)=PA(I)
      PA(I)=PNL(I,1)
  962 PC(I)=PA(I)
      go to 400
  965 DO 966 N=1,NOKMAX
      DO 966 N1=1,2
      RRYD(N,N1)=(RRYD(N,N1)+RR1(N,N1,2))/2.0
  966 REV(N,N1)=(REV(N,N1)+RK1(N,N1,2))/2.0
      if (ISLI.GT.0) go to 935
      go to 930
  970 HA1=HA
      HA=HB
      HB=HD
      FLA=LA
      FLB=LB
      HB=HA
      HA=HA1
      DO 972 I=1,IP
  972 PA(I)=PNL(I,2)
C
      B=(2.0*FLB+1.0)/(4.0*FLB+1.0)
      if (ITEST.ne.0) go to 975
      ITEST=7
      AA=0.0
      A=AA
  975 FK=KEMIN-2
      DO 979 N=1,NOKE
      FK=FK+2.0
      if (ICLASS.eq.1) go to 976
      D=WW(1)
      C=D*RRYD(N,2)
      if (ICLASS.eq.11) go to 1978
      go to 978
  976 N2=2
      N3=3
      if (HA.eq.HB) go to 977
      N2=3
      N3=2
  977 D=WW(N2)-1.0
      C=0.5*RR1(N,2,N3)
      if (N.GT.1) C=C+B*RR1(N,1,N2)
      C=C*D
  978 A=A+0.5*C*S3J0SQ(FLA,FK,FLB)
 1978 CONTINUE
      if (ISLI.GT.0) go to 979
      WRITE (9,96) D,C,FLA,FK,FLB,A,EEE,OVERLP
   96 FORMAT (/8F10.5)
  979 CONTINUE
      RR1(1,1,1)=A/D
      AA=13.6054*AA/D
C
  980 M=0
      DO 981 N1=1,2
      NCFGO(N1)=NCFG(N1)
      N2=N1
      if (ICLASS.eq.7) N2=3-N1
      DO 981 N=1,NOKE
      M=M+1
  981 REV(M,1)=13.6054*RR1(N,N2,1)
      N3=(2-N2)*NOKE+1
      M=1
      N=2*NOKD
      if (HA.eq.HB.or.HC.eq.HD) N=NOKD
      N1=N-M+1
      N2=5
      if (ISLI.lt.3) WRITE (9,97) CONF1(1:16),
     &             CONF2(7:16),N1, (REV(I,1),N2, I=M,N)
   97 FORMAT (/1H0,A16,1H-,A10,4X,I2,5(F11.4,1X,I1)/7(F11.4,1X,I1))
      if (NCS.ne.NCSO) NO=0
      DO 982 I=M,N
      J=NO+I
      REVSC(J)=FACT(5)*REV(I,1)
      if (NCD.ne.NFIRST) go to 982
      RRSC(NCPD,J)=REVSC(J)/13.6054
  982 CONTINUE
      N=N+NO
      NP1=N+1
      if (IDIPP.eq.6.and.EPS(2).GT.0.and.abs(EAV(1)-EAV(2)).
     &  GT.0.01) NP1=1
      DO 983 I=NP1,20
  983 REVSC(I)=0.0
      NRK=N
  990 NO=N
      NCSO=NCS
C
      CALL CLOKC(T3)
      T4=T2-T1
      T5=T3-T2
      T3=T3-T0
      WRITE (9,9988) NCFG,IREAD,T4,T5,T3
 9988 FORMAT (3I5,2F6.3,F10.3,4H SEC)
      if (ICLASS.ne.0) NCONFG=1000
      RETURN
C
 1000 WRITE (9,1001) NLH(K),CONFG
 1001 FORMAT (//9H ORBITAL ,A3, 23H NOT FOUND FOR CONFIG  ,A18)
      go to 990
C
      END


      SUBROUTINE G5INP
C
C          PUNCH INPUT FOR PROGRAM RCG mod 5, 6, 7, OR 8
C
      implicit real*8 (a-h,o-z)
      PARAMETER (KBGKS=41,KCASE=50)
      DIMENSION NORB(51),N(25,51),L(25,51),IWW(8),
     &  W(25,51),NN(8,50),WW(8,50),LSYMB(22),NLB(8,50),E(25),
     &  LB(8,50),LL(8,50),CF2(51),NLHT(8),NLH(4)
C
      CHARACTER*3 DH*80,SUBR,SUBR0,NLBCD,NLH,HA,HB,HC,HD,HA1,
     &            CONF1*18,CONF2*18,CONFG*18,OPTION*28,HXID*2,RJ*1
     &            ,CF2*6,LSYMB*1,LB*1,DMIN*5,IPRINT*5,NCK*2,IENGYD*1,
     &             ISPECC*1,NLB*3,NLHT*3,DUMCH18*18,DUMCH3*3
      COMMON /CHAR/ DH,SUBR,SUBR0,NLBCD(25),HA,HB,HC,HD,
     &              CONF1,CONF2,CONFG,OPTION,HXID,RJ
      COMMON NLZ(4),NCFG(2),MMIN,
     &  NCONFG,IZ,Z,NCSPVS,IDB,NCDES,WWNL(25),NPAR,VPAR(45),
     &  NPART,IX(8),FK(8,5),ZE(8),FGK(8,8,5),KPAR(45)
      COMMON/C1/FACT(5),IFACT(5),NRK,REVSC(20)
      COMMON/C2/NCPD,EE(51),STN(51),WT(51),GAMMA(51),Q(51),
     &  Q2(51),PDIPSC(51),RRSC(51,12),NCD,NFIRST,NCONT1,IPWR
      COMMON/C3/ICON,ISLI,IDIP,IREL,IC,ICN(2),NCONFN(2),NC0,ICASE,
     &  NSCONF(3,2),DEL(50),IDIPP
      COMMON/C4/IRHO,ISIG,IRHOP,ISIGP,N12,T0
      COMMON/CP/P
      COMMON/GENOSC/IGEN,NBIGKS,IBK,BIGKS(KBGKS)
      COMMON/LCM1/PNL(1801,20)
      COMMON/LCM2/RDIPS(KBGKS,KCASE),ISTPWR(KCASE),STFRAC(KCASE),
     &            ISTOVER(KCASE),ISTF1(KCASE)
      CHARACTER STCONF1*18,STCONF2*18,STHA*3,STHB*3,STHXID*2
      COMMON /LCM2CH/ STCONF1(KCASE),STCONF2(KCASE),STHA(KCASE),
     &                   STHB(KCASE),STHXID(KCASE)
C
      DIMENSION EAV(51),ID(51),NNLZ(25),NKKK(25),R(1801),RU(1801)
      DATA (LSYMB(I),I=1,22)/'S','P','D','F','G','H','I','K','L','M',
     &  'N','O','Q','R','T','U','V','W','X','Y','Z','A'/
C
C
      CALL CLOKC(T0)
      READ (DH,10) NCK,NCMAX,NSCONF(3,1),NSCONF(3,2),EAV11,IABG,
     &  OPTION,IQUAD,IFACT,DMIN,IPRINT,IENGYD,ISPECC,ICON,ISLI,IDIP,ALF
      WRITE(9,*) NCK,NCMAX,NSCONF(3,1),NSCONF(3,2),EAV11,IABG,
     &  OPTION,IQUAD,IFACT,DMIN,IPRINT,IENGYD,ISPECC,ICON,ISLI,IDIP,ALF
      WRITE(9,*) DH
   10 FORMAT (5X,A2,I3,I1,I2,F7.4,I1,A28,I1,5I2,2A5,2A1,3I1,F5.0)
C
C     CHECK FOR GENERALISED OSCILLATOR STRENGTH OPTION
C      AND COMPUTE MOMENTUM TRANSFER VALUES
C
      READ (OPTION,11) NBIGKS,NENRGS,IGEN,IRNQ,IRXQ,IRND,IRXD
   11 FORMAT (1X,I3,I2,7X,5I1)
      if (IGEN.lt.5.and.IQUAD.GT.0) IRNQ=2
      IRXQ=max(IRXQ,IRNQ)
      IRND=max(IRND,1)
      IRXD=max(IRXD,IRND)
      RJ='R'
      if (IGEN.lt.5) NBIGKS=1
      if (IGEN.lt.5) go to 80
      if (NBIGKS.le.10) NBIGKS=KBGKS
      if (NENRGS.le.0) NENRGS=21
      RJ='J'
      OPTION(1:6)=' '
      XMIN=1.01
      if (ICON.eq.1) XMIN=1.1
      if (ICON.GT.2) XMIN=ICON
      if (ICON.eq.9) XMIN=10.0
      ICON=0
      XMAX=100.0
      if (IDIP.eq.1) XMAX=20.0
      if (IDIP.GT.1) XMAX=100*IDIP
      if (IDIP.eq.9) XMAX=1000.0
      IDIP=0
   80 NC0=0
      IDIPP=0
      if (IDIP.ne.6) go to 82
      IDIPP=6
      IDIP=9
   82 if (NCMAX.le.0) NCMAX=50
      IABG=abs(IABG)
      IQUAD=abs(IQUAD)
      DO 101 I=1,5
      if (IFACT(I).eq.0.and.I.eq.2) IFACT(I)=95
      if (IFACT(I).eq.0) IFACT(I)=85
      ERAS=IFACT(I)
      FACT(I)=0.01*ERAS
  101 if (IFACT(I).eq.99) FACT(I)=1.0
      if (ICON.eq.0) ICON=8
      NSCF31=abs(NSCONF(3,1))
      if (NSCONF(3,2).eq.0.and.IDIP.eq.8) NSCONF(3,2)=-8
      NSCF32=abs(NSCONF(3,2))
      IDIP0=IDIP
      NC3=0
      REWIND 2
      if (NC0.eq.0) go to 102
      DO 105 NC=1,NC0
  105 READ (2) CONFG
C
  102 DO 104 I=1,8
      DO 104 NC=1,50
      LB(I,NC)=LSYMB(1)
      LL(I,NC)=0
      NN(I,NC)=0
  104 WW(I,NC)=0.0
      DO 90 I=1,51
      STN(I)=0.0
      WT(I)=0.0
      GAMMA(I)=0.0
      Q(I)=0.0
      Q2(I)=0.0
      PDIPSC(I)=0.0
      EAV(I)=0.0
      DO 90 J=1,12
   90 RRSC(I,J)=0.0
      NCONT1=1000
      NCONT2=1000
      IDELE=1000
      EMAX=0.0
C
C          DETERMINE NUMBER OF CONFIGS OF EACH PARITY
C
C      REWIND 31
c      REWIND 32
      IC=31
      ICN(1)=31
      ICN(2)=31
      NC=0
      NMAX=0
      LMAX=0
      NORBX=0
      NC2=0
  110 NC=NC+1
      if (NC+NC0.GT.NCMAX) go to 130
      PRINT*,'TRY TO READ TAPE2',CONFG
      READ (2,END=112) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,(N(I,NC),
     &  L(I,NC),W(I,NC),NLBCD(I),NNLZ(I),NKKK(I),E(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS), (RU(I),I=1,MESH)
      PRINT*,'READ TAPE2 ',CONFG
      go to 115
  112 NCMAX=NC0+NC-1
      go to 130
  115 CF2(NC)=CONFG(7:12)
      EE(NC)=E(NCSPVS)
      NORB(NC)=NCSPVS
      if (IDIP.lt.9.or.EE(NC).le.0) go to 120
      if (NCONT1.eq.1000) NCONT1=NC
      if (EE(NC).eq.EE(NCONT1)) NCONT2=NC
      IERAS=NCONT2-NCONT1
      if (IERAS.GT.0) IDELE=1000/IERAS
      IERAS=NC-NCONT2-1
      if (IERAS.GT.0) IDELE=min(IDELE,3000/IERAS)
  120 if (NC.eq.1) IONT=ION
      if (NC.eq.1) IZT=IZ
      if (IONT.eq.ION.and.IZT.eq.IZ) go to 132
  130 NC3=NC-1
      if (NC3.eq.0) go to 900
      BACKSPACE 2
      go to 160
  132 if (NCSPVS.GT.NORBX) NORBX=NCSPVS
      PRTY=0.0
      DO 135 I=1,NCSPVS
      if (I.eq.NCSPVS.and.W(I,NC).le.1.0) go to 134
      NMAX=max(NMAX,N(I,NC))
      LMAX=max(LMAX,L(I,NC))
  134 ERAS=L(I,NC)
  135 PRTY=PRTY+W(I,NC)*ERAS
      two  = 2
      PRTY = mod (PRTY, two)
      if (NC.eq.1) PRTY1=PRTY
      if (PRTY.eq.PRTY1) go to 150
      if (IC.eq.32) go to 130
      PRTY1=PRTY
      NC2=NC-1
      IC=32
      ICN(2)=32
  150 WRITE (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID, (N(I,NC),
     &  L(I,NC),W(I,NC),NLBCD(I),NNLZ(I),NKKK(I),E(I),I=1,NCSPVS),
     &  NPAR,VPAR,MESH,C,IDB, (R(I),I=1,MESH), EXF,CORRF,CA1,CA0,
     &  KUT,((PNL(I,M),I=1,MESH),M=1,NCSPVS), (RU(I),I=1,MESH)
      PRINT*,'WRITTEN TO TAPE',IC,CONFG
      go to 110
C
  160 WRITE (9,13)
   13 FORMAT (27H1RCN2---INPUT FOR RCG mod 5///)
      if (NC2.eq.0) NC2=NC3
      NSCONF(2,1)=NC2
      NSCONF(2,2)=NC3-NC2
      if (NSCF31.eq.0) NSCONF(3,1)=NC2
      if (NSCF32.eq.0) NSCONF(3,2)=NSCONF(2,2)
C
C          SET UP PARTIALLY-FILLED OR INTERACTING SHELLS
C
C               FIRST, FIND ORBITALS NOT ALWAYS FULL, BUT WITH MORE
C                    THAN ONE ELECTRON IN SOME CONFIGURATION,
C          OR FIND SINGLY-OCCUPIED ORBITAL WHICH IS NOT THE OUTERMOST OR
      J1=0
      if (NMAX.eq.0) go to 170
      NMIN=1
      DO 169 NNN=NMIN,NMAX
      LMAXP1=min(NNN,LMAX+1)
      DO 169 LP1=1,LMAXP1
      LLL=LP1-1
      WMAX=1.0
      A=4*LLL+2
      WMIN=A
      DO 166 NC=1,NC3
      NCSPVS=NORB(NC)
      DO 165 I=1,NCSPVS
      if (N(I,NC).ne.NNN.or.L(I,NC).ne.LLL) go to 165
      WMIN=min(WMIN,W(I,NC))
      WMAX=max(WMAX,W(I,NC))
      if (W(I,NC).eq.1.0.and.I.ne.NCSPVS) WMAX=2.0
      go to 166
  165 CONTINUE
      WMIN=0.0
  166 CONTINUE
      if (WMIN.eq.A.or.WMAX.eq.1.0) go to 169
      J1=J1+1
      DO 168 NC=1,NC3
      NN(J1,NC)=NNN
  168 LL(J1,NC)=LLL
  169 CONTINUE
C
C               SECOND, FIND REMAINING (SINGLY-OCCUPIED) ORBITALS
C                    (FOR ONE PARITY AT A TIME)
  170 NOSUBC=0
      K=1
      NCN=1
      NCX=NC2
      if (ICON.ne.1) ICON=max(ICON,J1+1)
  171 J3=J1
      DO 180 NC=NCN,NCX
      NC1=max(NC-1,NCN)
      DO 172 J=1,8
  172 NLB(J,NC)=' '
      NCSPVS=NORB(NC)
      DO 179 I=1,NCSPVS
      if (J3.eq.0.and.I.eq.NCSPVS.and.NC.eq.NCX) go to 177
      IF(J3.eq.0) go to 174
      DO 173 J=1,J3
      LLLL=LL(J,NC1)
      LLL2=L(I,NC)
      NNNN=NN(J,NCN)
      NNN2=N(I,NC)
      if (N(I,NC).eq.NN(J,NCN).and.L(I,NC).eq.LL(J,NC1)) go to 178
  173 CONTINUE
  174 if (J3.eq.J1.or.NC.eq.NCN) go to 176
      J=J3
      if (L(I,NC).eq.LL(J,NC1).and.I.eq.NCSPVS) go to 178
  176 A=4*L(I,NC)+2
      if (W(I,NC).eq.A) go to 179
  177 J3=J3+1
      if (ICON.GT.1) J3=min(J3,ICON)
      DO 1177 NC1=NC,NCX
 1177 LL(J3,NC1)=L(I,NC)
      J=J3
  178 WW(J,NC)=W(I,NC)
      NN(J,NC)=N(I,NC)
      if (NN(J,NCN).eq.0) NN(J,NCN)=N(I,NC)
      JJ=LL(J,NC)
      LB(J,NC)=LSYMB(JJ+1)
      WRITE (NLB(J,NC),'(I2,A1)') NN(J,NC),LB(J,NC)
  179 CONTINUE
  180 CONTINUE
      NOSUBC=max(NOSUBC,J3)
      if (NC2.eq.NC3.or.K.eq.2) go to 190
      K=2
      NCN=NC2+1
      NCX=NC3
      go to 171
C                    PUNCH RESCALE AND "10" CARDS
  190 WRITE(11,'(2A)')
     &'   10    1    0   00    4    4    1    1',
     &' SHELL00000000 SPIN00000000 INTER8      ',
     &'    0                         80998080  ',
     &'          8065.47800     0000000        '

C                    PUNCH CONTROL CARD
      NSCONF(1,1)=NOSUBC
      NSCONF(1,2)=NOSUBC
      if (NC2.eq.NC3) NSCONF(1,2)=0
c -- change printflag to "9"
      option(18:18) = '9'
      WRITE (9,18) NCK,NSCONF,IABG,OPTION,IQUAD,DMIN,IPRINT,IENGYD,
     &  ISPECC,ICON,ISLI,IDIP,IREL
   18 FORMAT (4X,'1',A2,3X,I1,2I2,I1,2I2,I1,A28,I1,1X,'8065.4790',
     &  2A5,2A1,3X,3I2,I8)
      if (ICON.ne.1) WRITE(11,18) NCK,NSCONF,IABG,OPTION,IQUAD,DMIN,
     &  IPRINT,IENGYD,ISPECC
C                    PUNCH CONFIGURATION CARDS
      K=1
      NCN=1
      NCX=NC2
  192 if (NCN.eq.NCX) go to 196
      DO 195 I=1,NOSUBC
      DO 194 NC=NCN,NCX
      if (WW(I,NC).eq.0) go to 194
      DO 193 M=NCN,NCX
      if (WW(I,M).ne.0) go to 193
      LL(I,M)=LL(I,NC)
      LB(I,M)=LB(I,NC)
  193 CONTINUE
      go to 195
  194 CONTINUE
  195 CONTINUE
  196 DO 198 NC=NCN,NCX
      DO 197 I=1,NOSUBC
  197 IWW(I)=WW(I,NC)
      WRITE  (9,19) (LB(I,NC),IWW(I), I=1,NOSUBC)
   19 FORMAT (1X,15(A1,I2,4X))
      if (ICON.ne.1) WRITE (11,20) (LB(I,NC),IWW(I), I=1,NOSUBC)
   20 FORMAT (15(A1,I2,2X))
  198 CONTINUE
      if (NC2.eq.NC3.or.K.eq.2) go to 200
      K=2
      NCN=NC2+1
      NCX=NC3
      go to 192
C
C
C          REARRANGE, SCALE, AND PUNCH SINGLE-CONF PARAMETERS
C
  200 REWIND 31
      REWIND 32
      IFLAG=0
      KK=1
      NFIRST=1
      NLAST=NC2
      IC=31
      DO 395 NC=1,NC3
      READ (IC) CONFG,NCONFG,IZ,Z,NCSPVS,ION,IREL,HXID,
     &  (N(I,NC),L(I,NC),W(I,NC),NLBCD(I),IA,IA,E(I), I=1,NCSPVS),NPART,
     &  VPAR
      PRINT*,'READ FROM TAPE',IC,CONFG
      I1=NCSPVS+1
      DO 202 I=I1,25
  202 L(I,NC)=77
      NCFG2=NCONFG
      NCONFG=99
      NCONFN(KK)=99
      if (NC.eq.1) E1=VPAR(1)
      KPAR(1)=0
      VPAR(1)=EAV11+13.6054*(VPAR(1)-E1)
      EAV(NC)=VPAR(1)
C          DETERMINE ENERGY WIDTHS FOR PSEUDO-DISCRETE CONFIGURATIONS
      DEL(NC)=1.0
      if (NC.ge.NCONT1) DEL(NC)=0.0
      if (IDIP.eq.8.and.E(NCSPVS).ge.0) DEL(NC)=0.0
      if (NC.eq.NFIRST.and.E(NCSPVS).lt.0) go to 206
      if (IDIP.ne.7.or.W(NCSPVS,NC).ne.1.0) go to 206
      if (L(NCSPVS,NC).ne.L(NCSPVS,NC-1)) EMAX=EAV(NC)-13.6054*E(NCSPVS)
      ZC=ION+1
      FN=99.0
      if (E(NCSPVS).lt.0) FN=ZC/SQRT(-E(NCSPVS))
      DEL0=13.6054
      if (FN.eq.99.0) go to 203
      if (L(NCSPVS,NC).ne.L(NCSPVS,NC-1)) go to 206
      DEL0=2.0*DEL0*(ZC**2)/(FN**3)
      if (N(NCSPVS,NC).ne.N(NCSPVS,NC-1)+1) go to 203
      EMAX=EAV(NC)+0.5*DEL0
      if (NC.eq.NC2.or.NC.eq.NC3) go to 206
      READ (IC) DUMCH18,DUMCH18,IZ,Z,NCS1,ION,IREL,HXID,
     &  (IA,IA,A,DUMCH3,IA,IA,A, I=1,NCS1), IA, EAVP1
      BACKSPACE IC
      EAVP1=EAV11+13.6054*(EAVP1-E1)
      if (EAVP1.le.EAV(NC)) go to 206
      X=(EAVP1-EAV(NC))/(EAVP1-EAV(NC-1))
      EMAX=min(EMAX,X*EAV(NC)+(1.0-X)*EAVP1)
      go to 206
  203 EMIN=EMAX
      EMAX=2.0*EAV(NC)-EMIN
      if (NC.eq.NC2.or.NC.eq.NC3) go to 205
      READ (IC) DUMCH18,DUMCH18,IZ,Z,NCS1,ION,IREL,HXID,
     &  (IA,IB,A,DUMCH3,IA,IA, I=1,NCS1), IA, EAVP1
      BACKSPACE IC
      EAVP1=EAV11+13.6054*(EAVP1-E1)
      if (NC.GT.NFIRST.and.IB.ne.L(NCSPVS,NC)) go to 205
      if (NC.GT.NFIRST.and.IB.eq.L(NCSPVS,NC).and.IB.eq.L(NCSPVS,NC-1))
     &  go to 204
      EMAX=0.5*(EAV(NC)+EAVP1)
      EMIN=EMAX+EAV(NC)-EAVP1
      go to 205
  204 if (EAVP1.le.EAV(NC)) go to 205
      X=(EAVP1-EAV(NC))/(EAVP1-min(EAV(NC-1),EMIN))
      half = 0.5
      X    = max(X, half)
      EMAX = min(EMAX,X*EAV(NC)+(1.0-X)*EAVP1)
  205 DEL(NC)= SQRT((EMAX-EMIN)/DEL0)
C
  206 DO 210 I=1,8
      IX(I)=0
      NLHT(I)='   '
      ZE(I)=-1.0
      DO 210 K=1,5
      FK(I,K)=-1.0
      DO 210 J=1,8
  210 FGK(I,J,K)=-1.0
C
      NPAR=1
      JX=0
      DO 220 I=1,NCSPVS
      A=4*L(I,NC)+1
      if (W(I,NC).lt.1.0.or.W(I,NC).GT.A) go to 220
      JX=JX+1
      NLHT(JX)=NLBCD(I)
      L(JX,NC)=L(I,NC)
      W(JX,NC)=W(I,NC)
      if (W(I,NC).eq.1.0.or.W(I,NC).eq.A) go to 220
      NK=L(I,NC)
      DO 216 K=1,NK
      NPAR=NPAR+1
  216 FK(JX,K)=VPAR(NPAR)
  220 CONTINUE
C
      if (JX.eq.0) go to 291
      DO 230 I=1,JX
      if (L(I,NC).eq.0) go to 230
      NPAR=NPAR+1
      ZE(I)=VPAR(NPAR)
  230 CONTINUE
C
      if (JX.eq.1) go to 251
      JM1=JX-1
      DO 240 I=1,JM1
      J1=I+1
      DO 240 J=J1,JX
      NK=min(L(I,NC),L(J,NC))
      if (NK.eq.0) go to 240
      DO 235 K=1,NK
      NPAR=NPAR+1
  235 FGK(I,J,K)=VPAR(NPAR)
  240 CONTINUE
C
      DO 250 I=1,JM1
      J1=I+1
      DO 250 J=J1,JX
      NK=min(L(I,NC),L(J,NC))+1
      DO 245 K=1,NK
      NPAR=NPAR+1
  245 FGK(J,I,K)=VPAR(NPAR)
  250 CONTINUE
C
C
  251 NPAR=1
      DO 260 I=1,NOSUBC
      DO 256 J=1,JX
      if (NLHT(J).ne.NLB(I,NC)) go to 256
      IX(I)=J
      DO 252 K=1,5
      if (FK(J,K).lt.0) go to 253
      NPAR=NPAR+1
      KPAR(NPAR)=1
  252 VPAR(NPAR)=FK(J,K)*FACT(1)
  253 if (FK(J,1).lt.0) go to 260
      if (IABG.eq.0) go to 260
      NK=LL(I,NC)
      DO 254 K=1,NK
      NPAR=NPAR+1
      KPAR(NPAR)=1
      VPAR(NPAR)=0.0
  254 if (K.eq.1) VPAR(NPAR)=ALF
      go to 260
  256 CONTINUE
  260 CONTINUE
C
      JJ=0
      DO 270 I=1,NOSUBC
      II=IX(I)
      if (II.eq.0) go to 270
      if (II.le.JJ) IFLAG=1
      JJ=II
      if (ZE(II).lt.0) go to 270
      NPAR=NPAR+1
      KPAR(NPAR)=2
      VPAR(NPAR)=ZE(II)*FACT(2)
  270 CONTINUE
C
      if (JX.eq.1) go to 291
      JM1=NOSUBC-1
      DO 280 I=1,JM1
      if (IX(I).eq.0) go to 280
      J1=I+1
      DO 279 J=J1,NOSUBC
      II=IX(I)
      JJ=IX(J)
      if (JJ.eq.0) go to 279
      if (II.lt.JJ) go to 272
      JJJ=II
      II=JJ
      JJ=JJJ
  272 DO 275 K=1,5
      if (FGK(II,JJ,K).lt.0) go to 279
      NPAR=NPAR+1
      KPAR(NPAR)=3
  275 VPAR(NPAR)=FGK(II,JJ,K)*FACT(3)
  279 CONTINUE
  280 CONTINUE
C
      DO 290 I=1,JM1
      if (IX(I).eq.0) go to 290
      J1=I+1
      DO 289 J=J1,NOSUBC
      II=IX(I)
      JJ=IX(J)
      if (JJ.eq.0) go to 289
      if (II.lt.JJ) go to 282
      JJJ=II
      II=JJ
      JJ=JJJ
  282 DO 285 K=1,5
      if (FGK(JJ,II,K).lt.0) go to 289
      NPAR=NPAR+1
      KPAR(NPAR)=4
  285 VPAR(NPAR)=FGK(JJ,II,K)*FACT(4)
  289 CONTINUE
  290 CONTINUE
C
  291 if (NPAR.ge.5) go to 294
      N1=NPAR+1
      DO 292 I=N1,5
      KPAR(I)=0
  292 VPAR(I)=0.0
  294 WRITE (9,28) CONFG, (VPAR(I), I=1,5), HXID,FACT
   28 FORMAT (//1H0,A18,6X,5F12.5,5X,A2,5X,5HFACT=,5F6.2)
      if (NC.ge.NCONT1) VPAR(1)=-9500+IDELE*(NC-NCONT1)
      if (NC.GT.NCONT2) VPAR(1)=-7500+IDELE*(NC-NCONT2-1)
      WRITE (11,29) CONFG,NPAR, (VPAR(I),KPAR(I), I=1,5), HXID,
     &  (IFACT(I), I=1,4)
   29 FORMAT (A18,I2,5(F9.3,I1),A2,4I2)
      if (NPAR.le.5) go to 300
      WRITE ( 9,30) (VPAR(I), I=6,NPAR)
   30 FORMAT (7F12.5)
      WRITE (11,31) (VPAR(I),KPAR(I), I=6,NPAR)
   31 FORMAT (7(F9.3,I1))
C
C          CALC CONFIGURATION INTERACTION PARAMETERS
C
  300 if (NC.lt.NLAST) go to 395
      if (NFIRST.eq.NLAST) go to 350
      if (ISLI.GT.4) go to 350
      SUBR0='   '
      P=-99.0
      SUBR='SLI'
      NLM1=NLAST-1
      NERAS=NSCONF(3,KK)
      if (NERAS.eq.-8) NERAS=-500
      if (NERAS.lt.0.and.NERAS.ne.-9) NLM1=min(NLM1,NFIRST-1-NERAS)
      DO 329 NCD=NFIRST,NLM1
      NCFG(1)=NCD+NC0
      NCPDF=NCD+1
      NCPDL=min(NCD+abs(NERAS),NLAST)
      IRHO1=-7
      ISIG1=-7
      if (NERAS.ne.-9) go to 301
      if (NCD.eq.NFIRST) NCPDL=NLAST
      if (NCD.ne.NFIRST) NCPDL=NCPDF
  301 DO 329 NCPD=NCPDF,NCPDL
      NCFG(2)=NCPD+NC0
      NRK=0
      DO 328 IRHO=1,NOSUBC
      DO 328 ISIG=IRHO,NOSUBC
      DO 328 IRHOP=1,NOSUBC
      DO 327 ISIGP=IRHOP,NOSUBC
      MMIN=0
      if (IRHO.ne.IRHOP.or.ISIG.ne.ISIGP) go to 303
      WWW=0
      DO 302 I=ISIG,NOSUBC
  302 WWW=WWW+WW(I,NCD)+WW(I,NCPD)
      if (WWW.GT.2.0) go to 327
  303 DO 304 I=1,NOSUBC
      WW1=WW(I,NCD)
      WW2=WW(I,NCPD)
      if (I.eq.IRHO) WW1=WW1-1.0
      if (I.eq.ISIG) WW1=WW1-1.0
      if (I.eq.IRHOP) WW2=WW2-1.0
      if (I.eq.ISIGP) WW2=WW2-1.0
      if (WW1.ne.WW2) go to 327
      if (WW1.lt.0) go to 327
      if (I.le.ISIG.or.I.le.ISIGP.or.WW1.le.0) go to 304
      if (LL(I,NCD).ne.LL(I,NCPD)) go to 327
      if (NN(I,NCD).ne.NN(I,NCPD)) MMIN=-1
  304 CONTINUE
      A=4*LL(IRHO,NCD)+2
      if (WW(IRHO,NCD).eq.A.and.WW(IRHO,NCPD).eq.A) go to 327
      A=4*LL(ISIG,NCD)+2
      if (WW(ISIG,NCD).eq.A.and.WW(ISIG,NCPD).eq.A) go to 327
      KDMIN=max(abs(LL(IRHO,NCD)-LL(IRHOP,NCPD)),abs(LL(ISIG,
     &  NCD)-LL(ISIGP,NCPD)))
      KDMAX=min(LL(IRHO,NCD)+LL(IRHOP,NCPD),LL(ISIG,NCD)
     &  +LL(ISIGP,NCPD))
      IF(KDMIN.GT.KDMAX) go to 327
      HA=NLB(IRHO,NCD)
      HB=NLB(ISIG,NCD)
      HC=NLB(IRHOP,NCPD)
      HD=NLB(ISIGP,NCPD)
      N12=2
      if (IRHO.ne.IRHO1.or.ISIG.ne.ISIG1) N12=1
      CALL SLI2
      IRHO1=IRHO
      ISIG1=ISIG
      NCFG2=NCFG(2)
  327 CONTINUE
  328 CONTINUE
      if (NRK.eq.0) go to 329
      if (IDIP.eq.7.or.IDIP.eq.8) go to 324
      if (NCD.lt.NCONT1) go to 326
  324 DO 325 I=1,NRK
  325 REVSC(I)=DEL(NCD)*DEL(NCPD)*REVSC(I)
  326 WRITE (9,32) DEL(NCD),DEL(NCPD),(REVSC(I),I=1,NRK)
   32 FORMAT (9H SQRTDEL=,2F10.4,5X,5(F11.4,2X)/7(F11.4,2X))
      N2=5
      WRITE (11,33) CONF1(7:15),CONF2(7:15),NRK,
     &  (REVSC(I),N2,I=1,5),HXID,(IFACT(I), I=1,3),IFACT(5)
   33 FORMAT (A9,1H-,A9,I1,5(F9.4,I1),A2,4I2)
      if (NRK.le.5) go to 329
      WRITE (11,34) (REVSC(I),N2, I=6,NRK)
   34 FORMAT (7(F9.4,I1))
  329 CONTINUE
C
  350 if (IQUAD.lt.3.and.IQUAD.ne.KK) go to 390
      N1=NFIRST
      N2=NLAST
      N3=N1
      N4=N2
      IRMN1=IRNQ+1
      IRMX1=IRXQ+1
      ICN(1)=IC
      ICN(2)=IC
      go to 402
  390 if (KK.eq.2) go to 400
      if (NC2.eq.NC3) go to 500
      KK=2
      NFIRST=NC2+1
      NLAST=NC3
      IC=32
      WRITE (9,39)
   39 FORMAT (1H1)
  395 CONTINUE
C
      if (IFLAG.eq.0) go to 400
      DO 397 I=1,5
  397 WRITE (9,40)
   40 FORMAT (11H **********)
      WRITE (9,41)
   41 FORMAT (34H0*WARNING --- ORBITALS REARRANGED*)
C
C          CALC QUADRUPOLE OR DIPOLE REDUCED MATRIX ELEMENTS
C
  400 N1=1
      N2=NC2
      N3=NC2+1
      N4=NC3
      IRMN1=IRND+1
      IRMX1=IRXD+1
      ICN(1)=31
      ICN(2)=32
  402 SUBR0='   '
      SUBR='DIP'
      if (IDIP.eq.1) go to 460
      J1=ICN(1)-30
      J2=ICN(2)-30
      if (IGEN.lt.5) go to 3402
      ELO=EAV(1)
      EHI=ELO
      DO 1402 I=1,NC3
      EA=EAV(I)
      if (EA.lt.ELO) ELO=EA
      if (EA.GT.EHI) EHI=EA
 1402 CONTINUE
      if (EHI.le.ELO) EHI=ELO+VPAR(2)
      ENMAX=(EHI-ELO)/13.6054
      ENMAX=ENMAX*1.1*XMAX
      if (ENMAX.lt.0.001) STOP 140
      EKMAX=2.0*SQRT(ENMAX)
      EKMIN=1.0E-3
      EKMAXL=log(EKMAX)
      EKMINL=log(EKMIN)
      DELEKL=(EKMAXL-EKMINL)/FLOAT(NBIGKS-1)
      WRITE (11,42) NBIGKS,EKMINL,EKMAXL,DELEKL,NENRGS,XMIN,XMAX
   42 FORMAT (I5,3F15.10,I10,2F10.2)
      DO 2402 I=1,NBIGKS
      EKL=EKMINL+DELEKL*(I-1)
 2402 BIGKS(I)=EXP(EKL)
      ICASE=0
      WRITE (9,43) ENMAX,NBIGKS
   43 FORMAT (/7H ENMAX=,F14.7,8H NBIGKS=,I4,//9H K-VALUES/)
      WRITE (9,44) (BIGKS(I),I=1,NBIGKS)
   44 FORMAT (8F14.7)
 3402 CONTINUE
      DO 450 IPWR1=IRMN1,IRMX1,2
      IPWR=IPWR1-1
      DO 450 NCD=N1,N2
      NCFG(1)=NCD+NC0
      DO 450 NCPD=N3,N4
      NCFG(2)=NCPD+NC0
      DO 439 IRHO=1,NOSUBC
      DO 438 IRHOP=1,NOSUBC
      if (abs(LL(IRHO,NCD)-LL(IRHOP,NCPD)).GT.IPWR) go to 438
      LSUM=LL(IRHO,NCD)+LL(IRHOP,NCPD)
      if (LSUM.lt.IPWR) go to 438
      if (mod(LSUM+IPWR,2).ne.0) go to 438
      if (IRHO.ne.IRHOP) go to 410
      if (NCD.ne.NCPD) go to 404
      DO 403 I=IRHO,NOSUBC
      ERAS=4*LL(I,NCD)+2
      WWW=WW(I,NCD)
      if ((WWW.eq.0.or.WWW.eq.ERAS).and.I.eq.IRHO) go to 438
      if (I.eq.IRHO) go to 403
      if (WWW.eq.0.or.WWW.eq.ERAS) go to 403
      if (LL(I,NCD).eq.0) go to 403
      go to 438
  403 CONTINUE
      go to 410
  404 WWW=0
      DO 405 I=IRHO,NOSUBC
  405 WWW=WWW+WW(I,NCD)+WW(I,NCPD)
      if (WWW.ne.2.0) go to 438
  410 DO 420 I=1,NOSUBC
      WW1=WW(I,NCD)
      WW2=WW(I,NCPD)
      if (I.ne.IRHO.and.I.ne.IRHOP) go to 412
      if (I.eq.IRHO) WW1=WW1-1.0
      if (I.eq.IRHOP) WW2=WW2-1.0
      if (WW1.lt.0) go to 438
      go to 414
  412 if (WW1+WW2.eq.0) go to 420
      if (NLB(I,NCD).ne.NLB(I,NCPD)) go to 438
  414 if (WW1.ne.WW2) go to 438
  420 CONTINUE
      HA=NLB(IRHO,NCD)
      HB=NLB(IRHOP,NCPD)
      MMIN=0
      CALL DIP
      go to 450
  438 CONTINUE
  439 CONTINUE
      MMIN=-7
      CALL DIP
  450 CONTINUE
      if (IGEN.lt.5) go to 460
      DO 455 IBK=1,NBIGKS
      DO 454 IC=1,ICASE
      CONF1=STCONF1(IC)
      CONF2=STCONF2(IC)
      IPWR=ISTPWR(IC)
      FRAC=STFRAC(IC)
      HXID=STHXID(IC)
      IOVER=ISTOVER(IC)
      IF1=ISTF1(IC)
      HA=STHA(IC)
      HB=STHB(IC)
      REDDIP=RDIPS(IBK,IC)
      WRITE (11,46) CONF1,CONF2,REDDIP,HA,RJ,IPWR,HB,FRAC,HXID,IOVER,IF1
   46 FORMAT(A18,2X,A18,F12.5,1H(,A3,2H//,A1,I1,2H//,A3,1H),F6.3,A2,2I4)
  454 CONTINUE
  455 CONTINUE
  460 if (mod(IPWR,2).eq.0) go to 390
C
C
C
  500 CALL CLOKC(T)
      T=T-T0
      WRITE (9,50) T
   50 FORMAT (//20X,10H-99999999.,10X,2HT=,F7.3,4H SEC)
      WRITE (11,51)
   51 FORMAT (20X,10H-99999999.)
C
C          FOR CONTINUUM RUNS, CALC FANO GAMMA AND Q
C
      if (NC2.eq.NC3.or.NC2.GT.1) go to 700
      if (NCX.le.NCN+2) go to 700
      if (EE(NCX).lt.0) go to 700
      if (IDIP.eq.8) go to 700
      IN=NCN+1
      DO 610 I=IN,NCX
      J=NOSUBC
  606 if (WW(J,I)-1.0) 608,610,700
  608 J=J-1
      go to 606
  610 CONTINUE
      WRITE (9,61)
   61 FORMAT (////15X,2HEE,7X,2HN*,8X,3HWT=,6X,
     &  10HGAMMA/RKSQ,3X,4HQ*RK,2X,12HPI*(QRKSQ)/2,2X,6HPDIPSC,7X,
     &  13HRKSC(RYD)----/27X,17HRT(0.5*NST3/ZST2) /)
      PI=3.141592654
      ZST=ION+1
      DO 660 I=NCN,NCX
      if (I.eq.NCN) go to 630
      if (EE(I).ge.0) go to 620
      STN(I)=ZST/SQRT(-EE(I))
      WT(I)=SQRT(0.5*STN(I))*STN(I)/ZST
      PDIPSC(I)=WT(I)*PDIPSC(I)
      DO 615 J=1,12
  615 RRSC(I,J)=WT(I)*RRSC(I,J)
  620 RMAX=1E-10
      JX=0
      DO 625 J=1,12
      if (RRSC(I,J).ne.0) JX=J
      if (abs(RRSC(I,J)).GT.abs(RMAX)) RMAX=RRSC(I,J)
  625 CONTINUE
      JX=max(JX,1)
      JX=min(JX,4)
      GAMMA(I)=2.0*PI*(RMAX**2)
      if (PDIPSC(I).ne.0) go to 628
      Q(I)=999.999
      Q2(I)=999.999
      go to 630
  628 Q(I)=PDIPSC(NCN)/(PI*RMAX*PDIPSC(I))
      Q2(I)=0.5*PI*(Q(I)**2)
  630 WRITE (9,63) CF2(I),EE(I),STN(I),WT(I),GAMMA(I),Q(I),Q2(I),
     &  PDIPSC(I), (RRSC(I,J), J=1,JX)
   63 FORMAT (1X,A6,F12.6,F9.3,F11.5,F11.5,F11.3,F11.3,F10.5,3X,4F12.6)
  660 CONTINUE
C
C
  700 NC0=NC0+NC3
      if (NC0.eq.NCMAX) go to 900
      go to 102
C
  900 RETURN
      END


      FUNCTION S3J0SQ(FJ1,FJ2,FJ3)
C
C          CALCULATE SQUARE OF 3-J SYMBOL WITH ZERO MAGNETIC QUANTUM NOS
C
C
      implicit real*8 (a-h,o-z)
      FJ=FJ1+FJ2+FJ3
      A=FJ-FJ1-FJ1
      B=FJ-FJ2-FJ2
      C=FJ-FJ3-FJ3
      S3J0SQ=FCTRL(A)*FCTRL(B)*FCTRL(C)/FCTRL(FJ+1.0)
      A=FCTRL(A/2.0)*FCTRL(B/2.0)*FCTRL(C/2.0)/FCTRL(FJ/2.0)
      S3J0SQ=S3J0SQ/A/A
      RETURN
      END


      FUNCTION S3J (FJ1, FJ2, FJ3, FM1, FM2)
C
      implicit real*8 (a-h,o-z)
      S3J=0.0
      FM3=FM1+FM2
      A=FJ2+FJ3+FM1
      B=FJ1-FM1
      C=-FJ1+FJ2+FJ3
      D=FJ3+FM3
      E=FJ1-FJ2-FM3
      F=FJ1-FJ2+FJ3
      G=FJ1+FJ2-FJ3
      H=FJ1+FJ2+FJ3+1.0
      E2=FCTRL(B)*FCTRL(FJ1+FM1)*FCTRL(FJ2-FM2)*FCTRL(FJ2+FM2)
      E1=(FCTRL(C)*FCTRL(F)/FCTRL(H))*FCTRL(G)*FCTRL(D)*FCTRL(FJ3-FM3)
      if (E1.gt.0) then
        E1=SQRT (E2)/SQRT (E1)
        Zero = 0
        I1= nint ( MAX (Zero, -E))
        I2= nint ( MIN (A, C, D ))
        if (I2.ge.I1) then
          DO 152 I=I1,I2
            FI = I
            E2 = FCTRL(FI)*FCTRL(C-FI)*FCTRL(D-FI)/FCTRL(A-FI)
  152       S3J=S3J+(((-1.0)** mod (I,2))/E2)*FCTRL(B+FI)/FCTRL(E+FI)
          S3J=S3J*((-1.0)** mod (nint (abs(FJ1+FM2+FM3)),2)) / E1
        endif
      endif
      END


      FUNCTION FCTRL(A)
C
C          CALCULATE FACTORIAL OF A
C
      implicit real*8 (a-h,o-z)
      ia = a
      if (ia .lt. 0) then
        FCTRL=0
      else
        FCTRL=1
        DO 14 I=2, ia
   14     FCTRL = FCTRL * I
      ENDIF
      END


      SUBROUTINE SBESS(RB,ITEST,LS,EKSQ,SZ,SZP)
C
C     ROUTINE FOR THE EVALUATION OF SPHERICAL BESSEL FUNCTIONS
C           USING RECURRENCE RELATIONS
C
      implicit real*8 (a-h,o-z)
      COMMON /BLK2/ IBUG1,IBUG2,IBUG3,IBUG4,IBUG5,IBUG6,LSWT,LDEL,LMIN
      COMMON/INFORM/ IREAD,IWRITE,IPUNCH
      DIMENSION SB(200), S(200)
C
      if (ITEST.eq.3) EKSQ=-EKSQ
      AK=SQRT(EKSQ)
      R=AK*RB
      if (LS-R) 10,20,20
   10 go to (30,60,90), ITEST
   20 go to (180,60,90), ITEST
   30 SB(1)=SIN(R)
      SB(2)=SIN(R)/R-COS(R)
      if (LS-1) 40,50,140
   40 SZ=SB(1)
      SZP=AK*COS(R)
      go to 210
   50 SZ=SB(2)
      SZP=AK*(COS(R)/R-SIN(R)/R**2+SIN(R))
      go to 210
   60 SB(1)=-COS(R)
      SB(2)=-COS(R)/R-SIN(R)
      if (LS-1) 70,80,140
   70 SZ=SB(1)
      SZP=AK*SIN(R)
      go to 210
   80 SZ=SB(2)
      SZP=AK*(SIN(R)/R+COS(R)/R**2-COS(R))
      go to 210
   90 SB(1)=1.0
      SB(2)=-(1.0+1.0/R)
      if (LS-1) 100,110,120
  100 SZ=EXP(-R)
      SZP=-AK*EXP(-R)
      go to 210
  110 SZ=-(1.0+1.0/R)*EXP(-R)
      SZP=(1.0+1.0/R+1.0/R**2)*EXP(-R)
      go to 210
  120 LS1=LS+1
      DO 130 L=2,LS1
  130 SB(L+1)=(2*L-1)*(-1)**L*SB(L)/R-SB(L-1)
      SZ=SB(L+1)*EXP(-R)
      SZP=AK*((L+1)*(-1)**L*SB(L+1)/R+SB(L))*EXP(-R)
      go to 210
  140 LS1=LS+1
      DO 150 L=2,LS1
      SB(L+1)=(2*L-1)*SB(L)/R-SB(L-1)
  150 CONTINUE
      if (ITEST-2) 200,160,200
  160 if (LS-LS/2*2) 170,200,170
  170 SZ=-SZ
      SZP=-SZP
      go to 200
  180 M=LS+20
      S(M+1)=0.0
      S(M)=1.0E-20
      MM1=M-1
      DO 190 I=1,MM1
      J=M+1-I
      S(J-1)=(2*J-1)/R*S(J)-S(J+1)
  190 CONTINUE
      SB(M)=SIN(R)/S(1)
      SB(LS)=S(LS)*SB(M)
      SB(LS+1)=S(LS+1)*SB(M)
      SB(LS+2)=S(LS+2)*SB(M)
  200 SZ=SB(LS+1)
      SZP=AK*(SB(LS+1)/R+(LS*SB(LS)-(LS+1)*SB(LS+2))/(2*LS+1))
  210 if (IBUG5-1) 240,220,240
  220 WRITE(IWRITE,230)SZ,SZP
  230 FORMAT (//11H SZ SZP  = ,(2E16.8)//)
  240 CONTINUE
      RETURN
      END


      SUBROUTINE CLOKC(T)
      implicit real*8 (a-h,o-z)
      T=0
      END
