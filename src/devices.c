
#include "globals.h"
#include <string.h>

#include "spectra.h"
#include "devices.h"
#include "common.h"

/* Exported data */
char title[LINE_SIZE], frame_title[LINE_SIZE], image_title[LINE_SIZE];
char picture_name[STRING_SIZE];
FILE *picture_file = NULL;
output_types image_type = hpgl;
output_modes image_mode = landscape;
boolean horizontal = false, preserve_scale = false;
integer rows_per_page = 1, columns_per_page = 1, images_per_frame = 1,
	frames_per_page = 1;

/* Local data */
static boolean output_sent = false, send_page = false;
static integer image_number, plot_number, im_scale;
static real *spectrum = NULL;
/* The value of the largest_peak */
static real largest_peak = 0.0, lowest_peak = 0.0;
/* The vertical height allowed for the X axis title */
static integer xtext;
/* The vertical space allowed for the X axis labels */
static integer xlabel;
/* The horizontal space allowed for the Y axis title */
static integer ytext;
/* The horizontal space allowed for the Y axis labels */
static integer ylabel;
/* The width of the image */
static integer image_width;
/* The height of the image */
static integer image_height;
/* The gap between the top x axis and the top of the image */
static integer tgap;
/* The gap between the bottom x axis and y = 0 */
static integer bgap;
/* The step between marks on the X axis */
static integer xstep;
/* The step between marks on the Y axis */
static integer ystep;
/* Page size and margins */
static integer page_x_width, page_y_height, page_x_margin, page_y_margin;
/* Scaling used by the device, relates device scale to pts */
static integer dev_scale, line_width;
/* Font sizes */
static integer title_size, frame_title_size, subscript_size, fr_subscript_size,
            label_size;
/* The size of the box we put the frame in. */
static integer frame_width, frame_height;
/* The margin on the bottom and left of a frame */
static integer frame_x_margin, frame_y_margin;
/* lower left corner of the frame */
static integer frame_x_edge, frame_y_edge;
/* Min and max ratio of frame axes */
static integer max_ratio, min_ratio;
/* The ratio of the highest stick to the image_width */
static integer stick_scale = 2;

/* Used by inform_peaks */
#define MAX_PEAKS 100
static real peak_energy[MAX_PEAKS], peak_value[MAX_PEAKS];

/* Function prototypes */
static void init_hpgl_params(void);
static void start_hpgl(void);
static void put_hpgl_title(char *title, integer x, integer y, boolean frame);
static void hpgl_image(void);
static void hpgl_border(plot_data);
static void hpgl_spectrum(plot_data, real []);
static void hpgl_sticks(plot_data, real, real);
static void hpgl_comparison(plot_data, plot_data, real, real);
static void hpgl_page(void);
static void end_hpgl_image(void);
static void shut_hpgl(void);

static void init_postscript_params(void);
static void start_postscript(void);
static void put_postscript_title(char *title, integer x, integer y,
				 boolean frame);
static void postscript_image(void);
static void postscript_border(plot_data);
static void postscript_spectrum(plot_data, real []);
static void postscript_sticks(plot_data, real, real);
static void postscript_comparison(plot_data, plot_data, real, real);
static void postscript_page(void);
static void end_postscript_image(void);
static void shut_postscript(void);

static void start_points(void);
static void xy_spectrum(plot_data, real []);
static void xy_sticks(plot_data plot);
static void ylist_image(plot_data);
static void ylist_spectrum(plot_data);
static void ylist_sticks(plot_data);
static void points_page(void);

static void start_output(void);
static void start_image(plot_data);
static void make_border(plot_data);
static void send_spectrum(plot_data);
static void send_comparison(plot_data, plot_data, real, boolean);
static void subtract_spectra(real [], plot_data, plot_data, real *, real *);
static void send_subtraction(plot_data, plot_data);
static void send_stick_image(plot_data);
static void shut_image(void);

/* Output the file as HPGL commands */

static void init_hpgl_params()
{
  frames_per_page = rows_per_page * columns_per_page;
  max_ratio = 2;
  min_ratio = 1;
  /* HPGL seems to work in Decipoints of 1/720 inch */
  dev_scale = 10;
  if (image_mode == landscape) {
    page_x_width = 1080 * dev_scale;
    page_y_height = 756 * dev_scale;
    /* The LaserJet starts its axes at (36,18) in pts, we move to (36,36) */
    page_x_margin = 0;
    page_y_margin = 18 * dev_scale;
  } else {
    page_y_height = 1080 * dev_scale;
    page_x_width = 756 * dev_scale;
    page_y_margin = 0;
    page_x_margin = 18 * dev_scale;
  }
  xtext = 30 * dev_scale;
  xlabel = 10 * dev_scale;
  ytext = 45 * dev_scale;
  ylabel = 45 * dev_scale;
  bgap = 24 * dev_scale;
  tgap = 12 * dev_scale;
  xstep = 36 * dev_scale;
  ystep = 36 * dev_scale;
  title_size = 9 * dev_scale;
  subscript_size = 6 * dev_scale;
  frame_title_size = 6 * dev_scale;
  fr_subscript_size = 4 * dev_scale;
  label_size = 2 * dev_scale;
  frame_width = (page_x_width - page_x_margin) / columns_per_page;
  frame_height = (page_y_height - page_y_margin
		  - 2 * title_size) / rows_per_page;
  if (frames_per_page == 1) {
    frame_x_margin = 0;
    frame_y_margin = 0;
  } else {
    frame_x_margin = 6 * dev_scale;
    frame_y_margin = 6 * dev_scale;
  }
  image_width = frame_width - ytext - ylabel - frame_x_margin;
  image_height = frame_height - xtext - xlabel - bgap - tgap - frame_y_margin;
  if (frames_per_page != 1)
    image_height -= 2 * frame_title_size;
  if (images_per_frame == 1) {
    if (image_width / image_height > max_ratio) {
      page_x_margin += (image_width - image_height * max_ratio) / 2;
      image_width = max_ratio * image_height;
      frame_width = image_width + ytext + ylabel + frame_x_margin;
    }
    if (image_width / image_height < min_ratio) {
      page_y_margin += (image_height - image_width * min_ratio) / 2;
      frame_height += min_ratio * image_width - image_height;
      image_height = min_ratio * image_width;
    }
  }
}

static void start_hpgl()
{
  if (not output_sent) {
    image_number = 0;
    plot_number = 0;
    output_sent = true;
    send_page = false;
    init_hpgl_params();
    fprintf(picture_file, "%c%c%c%c\n", 0x1b, 0x25, 0x30, 0x42);
    fputs("IN;\nPW 0.25 1;\n", picture_file);
    if (image_mode == landscape)
      fputs("RO 90;\n", picture_file);
  } else {
    plot_number++;
    if (plot_number == images_per_frame) {
      plot_number = 0;
      image_number++;
      if (image_number == frames_per_page) {
	send_page = true;
	image_number = 0;
      }
    }
  }
  if (send_page) {
    hpgl_page();
    fprintf(picture_file, "%c%c%c%c\n", 0x1b, 0x25, 0x30, 0x42);
    fputs("IN;\nPW 0.25 1;\n", picture_file);
    if (image_mode == landscape)
      fputs("RO 90;\n", picture_file);
    send_page = false;
  }
}

static void put_hpgl_title(char *title, integer x, integer y, boolean frame)
/* Puts the title string at the given position */
{
  integer i = 0, subscript, superscript;
  real font_x, font_y, font_sub_x, font_sub_y;
  boolean small = false, up;

  if (title[0] == 0)
    return;
  if (frame) {
    subscript = fr_subscript_size / 2;
    superscript = fr_subscript_size;
    font_x = 0.10;
    font_y = 0.16;
    font_sub_x = 0.06;
    font_sub_y = 0.10;
  } else {
    subscript = subscript_size / 2;
    superscript = subscript_size;
    font_x = 0.15;
    font_y = 0.25;
    font_sub_x = 0.10;
    font_sub_y = 0.16;
  }
  fputs("SP 2;\n", picture_file);
  fprintf(picture_file, "DI 1.00 0.00; SI %f %f; PU %d %d\n", font_x, font_y,
	  x, y);
  fputs("LB ", picture_file);
  while (title[i] != 0) {
    if (title[i] == '^') {
      i++;
      fprintf(picture_file, "%c\n", 0x03);
      fprintf(picture_file, "PU; PR %d %d; SI %f %f;\nLB", 0, superscript,
	      font_sub_x, font_sub_y);
      if (title[i] == '{') {
	up = true;
	small = true;
      } else {
	fprintf(picture_file, "%c%c\n", title[i], 0x03);
	fprintf(picture_file, "PU; PR %d %d; SI %f %f;\nLB", 0, -superscript,
		font_x, font_y);
      }
    } else if (title[i] == '_') {
      i++;
      fprintf(picture_file, "%c\n", 0x03);
      fprintf(picture_file, "PU; PR %d %d; %f %f;\nLB", 0, -subscript,
	      font_sub_x, font_sub_y);
      if (title[i] == '{') {
	up = false;
	small = true;
      } else {
	fprintf(picture_file, "%c%c\n", title[i], 0x03);
	fprintf(picture_file, "PU; PR %d %d; SI %f %f;\nLB", 0, subscript,
		font_x, font_y);
      }
    } else if (title[i] == '}' and small) {
      small = false;
      fprintf(picture_file, "%c\nPU; PR ", 0x03);
      if (up)
	fprintf(picture_file, "%d %d;", 0, -superscript);
      else
	fprintf(picture_file, "%d %d;", 0, subscript);
      fprintf(picture_file, " SI %f %f;\nLB", font_x, font_y);
    } else
      putc(title[i], picture_file);
    i++;
  }
  fprintf(picture_file, "%c\n", 0x03);
}

static void hpgl_image()
{
  integer row, col;

  if (image_number == 0) {
    put_hpgl_title(title, page_x_margin + ytext + ylabel,
		   page_y_margin + rows_per_page * frame_height +
		   title_size / 2, false);
    frame_x_edge = page_x_margin + frame_x_margin;
    frame_y_edge = page_y_margin + frame_y_margin;
  } else {
    if (horizontal) {
      col = image_number / columns_per_page;
      row = image_number - col * columns_per_page;
    } else {
      row = image_number / rows_per_page;
      col = image_number - row * rows_per_page;
    }
    frame_x_edge = page_x_margin + row * frame_width + frame_x_margin;
    frame_y_edge = page_y_margin + col * frame_height + frame_y_margin;
  }
  if (frames_per_page != 1) {
    fprintf(picture_file, "PA;\n");
    put_hpgl_title(frame_title, frame_x_edge + ytext + ylabel,
		   frame_y_edge - frame_y_margin + frame_height
		   - 3 * frame_title_size / 2, true);
  }
}

static void hpgl_border(plot_data plot)
{
  integer i, ndigits, magnitude, x_offset, y_offset;
  real energy, step, scale, height, low, plot_start, plot_range;

  fputs("PA; SP 1;\n", picture_file);
  /* The border */
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset = frame_y_edge + xtext + xlabel;
  fprintf(picture_file, "PU %d %d; ", x_offset, y_offset);
  fprintf(picture_file, "PD %d %d %d %d %d %d %d %d;\n",
	  x_offset + image_width, y_offset,
	  x_offset + image_width, y_offset + bgap + image_height + tgap,
	  x_offset, y_offset + bgap + image_height + tgap,
	  x_offset, y_offset);
  /* The labels */
  fputs("SP 2;\n", picture_file);
  fprintf(picture_file, "DI 1.00 0.00; SI 0.30 0.40; PU %d %d;\n",
	  frame_x_edge + ytext + ylabel + image_width / (columns_per_page + 2),
	  frame_y_edge);
  fprintf(picture_file, "LB Energy (eV)%c\n", 0x03);
  fprintf(picture_file, "DI 0.00 1.00; SI 0.30 0.40; PU %d %d;\n",
	  frame_x_edge + ytext, frame_y_edge + xtext + xlabel
	  + image_height / (rows_per_page + 2));
  fprintf(picture_file, "LB Intensity%c\n", 0x03);
  magnitude = Trunc(rlog10(largest_peak - lowest_peak));
  if (magnitude != 0) {
    fprintf(picture_file, "LB (x10%c\nPU; PR %d %d;\n", 0x03,
	    -3 * subscript_size / 2, 0);
    fprintf(picture_file, "SI 0.20 0.25; LB%d%c\n", magnitude, 0x03);
    fprintf(picture_file, "PU; PR %d %d;\n", 3 * subscript_size / 2, 0);
    fprintf(picture_file, "SI 0.30 0.40; LB)%c\nPA;\n", 0x03);
  }
  /* The marks */
  x_offset = frame_x_edge + ytext;
  y_offset = frame_y_edge + xtext;
  plot_start = plot->min_energy;
  plot_range = plot->max_energy - plot_start;
  step = Upper(plot_range / ((real)image_width) * ((real)xstep));
  ndigits = Trunc(rlog10(plot_range)) + 1;
  /* Marks for the X axis */
  scale = ((real)image_width) / (plot_range);
  for (energy = step; energy < plot_range; energy += step) {
    i = x_offset + ylabel + Round(energy * scale);
    fputs("SP 1;\n", picture_file);
    fprintf(picture_file, "PU %d %d; PD %d %d;\n",
	    i, y_offset + xlabel, i, y_offset + xlabel + 30);
    fprintf(picture_file, "PU %d %d; PD %d %d;\n",
	    i, y_offset + xlabel + bgap + image_height + tgap,
	    i, y_offset + xlabel + bgap + image_height + tgap - 15);
    fputs("SP 2;\n", picture_file);
    fprintf(picture_file, "DI 1.00 0.00; SI 0.10 0.15; PU %d %d;\n",
	    i - dev_scale * 3 * ndigits, y_offset);
    fprintf(picture_file, "LB%*.0f%c\n", ndigits, energy + plot_start, 0x03);
  }
  height = largest_peak * rpow(10.0, -(real)magnitude);
  low = lowest_peak * rpow(10.0, -(real)magnitude);
  ndigits = 1;
  step = Upper((height - low) * ((real)ystep) / ((real)image_height));
  if (images_per_frame > 1) {
    if (image_height < image_width)
      scale = ((real)(3 * image_height / 4) / (height - low));
    else
      scale = ((real)(3 * image_width / 4) / (height - low));
  } else
    scale = ((real)image_height / (height - low));
  for (energy = Upper(low); energy < height; energy += step) {
    i = y_offset + xlabel + bgap + Round((energy - low) * scale);
    fputs("SP 1;\n", picture_file);
    fprintf(picture_file, "PU %d %d; PD %d %d;\n",
	    x_offset + ylabel, i, x_offset + ylabel + 30, i);
    fprintf(picture_file, "PU %d %d; PD %d %d;\n",
	    x_offset + ylabel + image_width, i,
	    x_offset + ylabel + image_width - 30, i);
    fputs("SP 2;\n", picture_file);
    fprintf(picture_file, "DI 1.00 0.00; SI 0.10 0.15;PU %d %d;\n",
	    x_offset + ylabel / 2, i - 2 * dev_scale);
    fprintf(picture_file, "LB%.1f%c\n", energy, 0x03);
  }
  fputs("SP 1;\n", picture_file);
}

static void hpgl_spectrum(plot_data plot, real spectrum[])
{
  register integer i, x_offset, y_offset;
  real height, step_size, diff;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  diff = largest_peak - lowest_peak;
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset += frame_y_edge + xtext + xlabel + bgap;
  fprintf(picture_file, "PU %d %d; PD", x_offset, y_offset +
	  Round(height * (spectrum[0] - lowest_peak) / diff));
  step_size = ((real)image_width) / ((real)plot->npoints);
  for (i = 1; i < plot->npoints; i++)
    fprintf(picture_file, " %d %d",
	    x_offset + Round(step_size * (real)i),
	    y_offset + Round(height * (spectrum[i] - lowest_peak) / diff));
  fputs(";\n", picture_file);
  if (lowest_peak < 0.0)
    fprintf(picture_file, "PU %d %d; PD %d %d;\n", x_offset,
	    y_offset - Round(height * lowest_peak / diff),
	    x_offset + image_width,
	    y_offset - Round(height * lowest_peak / diff));
}

static void hpgl_sticks(plot_data plot, real lowest, real largest)
{
  register integer i, x_offset, y_offset, y_value;
  real height, step_size;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset += frame_y_edge + xtext + xlabel + bgap;
  step_size = ((real)image_width) / ((real)plot->npoints);
  height = ((real)(height / stick_scale)) / (largest - lowest);
  /* subtract since most likely to be negative. */
  y_offset -= Round((real)image_height * lowest_peak /
		    (largest_peak - lowest_peak));
  for (i = 1; i < plot->npoints; i++)
    if (rabs(plot->sticks[i]) > approx_zero) {
      y_value = Round(height * plot->sticks[i]);
      if (y_value != 0)
	fprintf(picture_file, "PU %d %d; PD %d %d;\n",
		x_offset + Round(step_size * (real)i), y_offset,
		x_offset + Round(step_size * (real)i), y_offset + y_value);
    }
}

static void hpgl_comparison(plot_data plot, plot_data comp, real shift,
			    real largest)
/* send the comparison image */
{
  register integer i, start, x_offset, y_offset, zero_offset;
  real energy, plot_start, plot_range, scale, escale, height;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset += frame_y_edge + xtext + xlabel + bgap;
  energy = comp->min_energy + shift;
  plot_start = plot->min_energy;
  plot_range = plot->max_energy - plot_start;
  if (energy >= plot_start) {
    start = 0;
    energy -= plot_start;
  } else {
    start = Round((plot_start - energy) / comp->energy_step);
    energy = 0.0;
  }
  zero_offset = -Round((height * lowest_peak) / (largest_peak - lowest_peak));
  escale = ((real)image_width) / plot_range;
  scale = ((height - (real)zero_offset)) / (largest);
  fprintf(picture_file, "PU %d %d; PD", x_offset + Round(energy * escale),
	  y_offset + Round(comp->spectrum[start] * scale));
  for (i = start + 1; i < comp->npoints and energy < plot_range; i++) {
    energy += comp->energy_step;
    fprintf(picture_file, " %d %d", x_offset + Round(energy * escale),
	    y_offset + zero_offset + Round(comp->spectrum[i] * scale));
  }
  fputs(";\n", picture_file);
}

static void hpgl_page()
{
  fprintf(picture_file, "%c%c\n", 0x1b, 0x45);
}

static void end_hpgl_image()
{
  integer y_offset, zero_offset;
  real height;

  if (images_per_frame > 1) {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
    zero_offset = Round(height * lowest_peak / (largest_peak - lowest_peak));
    if (image_title[0] != 0)
      put_hpgl_title(image_title, frame_x_edge + ytext + ylabel + 2,
		     frame_y_edge + xtext + xlabel + bgap + zero_offset
		     + y_offset + dev_scale, true);
  }
  /* tell the device that this image is completed so it may now draw it */
}

static void shut_hpgl()
{
  fprintf(picture_file, "%c%c", 0x1b, 0x45);
}

/* Output Postscript commands */

static void init_postscript_params()
{
  frames_per_page = rows_per_page * columns_per_page;
  max_ratio = 2;
  min_ratio = 1;
  /* To match HPGL we will work in Decipoints of 1/720 inch */
  dev_scale = 10;
  if (image_mode == landscape) {
    page_x_width = 800 * dev_scale;
    page_y_height = 560 * dev_scale;
    page_x_margin = 36 * dev_scale;
    page_y_margin = 36 * dev_scale;
  } else {
    page_y_height = 800 * dev_scale;
    page_x_width = 560 * dev_scale;
    page_y_margin = 36 * dev_scale;
    page_x_margin = 36 * dev_scale;
  }
  xtext = 24 * dev_scale;
  xlabel = 12 * dev_scale;
  ytext = 24 * dev_scale;
  ylabel = 24 * dev_scale;
  bgap = 12 * dev_scale;
  tgap = 6 * dev_scale;
  xstep = 25 * dev_scale;
  ystep = 25 * dev_scale;
  title_size = 12 * dev_scale;
  subscript_size = 9 * dev_scale;
  frame_title_size = 10 * dev_scale;
  fr_subscript_size = 8 * dev_scale;
  label_size = 8 * dev_scale;
  frame_width = (page_x_width - page_x_margin) / columns_per_page;
  frame_height = (page_y_height - page_y_margin
		  - 2 * title_size) / rows_per_page;
  if (frames_per_page == 1) {
    frame_x_margin = 0;
    frame_y_margin = 0;
  } else {
    frame_x_margin = 6 * dev_scale;
    frame_y_margin = 6 * dev_scale;
  }
  image_width = frame_width - ytext - ylabel - frame_x_margin;
  image_height = frame_height - xtext - xlabel - bgap - tgap - frame_y_margin;
  if (frames_per_page != 1)
    image_height -= 2 * frame_title_size;
  if (images_per_frame == 1) {
    if (image_width / image_height > max_ratio) {
      page_x_margin += (image_width - image_height * max_ratio) / 2;
      image_width = max_ratio * image_height;
      frame_width = image_width + ytext + ylabel + frame_x_margin;
    }
    if (image_width / image_height < min_ratio) {
      page_y_margin += (image_height - image_width * min_ratio) / 2;
      frame_height += min_ratio * image_width - image_height;
      image_height = min_ratio * image_width;
    }
  }
  /* Lets play with the line width. */
  line_width = dev_scale / 2;
  line_width /= (images_per_frame * frames_per_page);
  if (line_width < 2)
    line_width = 2;
}

static void start_postscript()
/* Postscript uses units of 1/72 inch */
{
  real scale;

  if (not output_sent) {
    image_number = 0;
    plot_number = 0;
    output_sent = true;
    send_page = false;
    init_postscript_params();
    /* Some systems seem to use this to tell if its a postscript file. */
    fputs("%!PS-Adobe-2.0\n", picture_file);
    if (image_mode == landscape) {
      /* Need to shift to actual top of page! */
      fputs("0 840 translate\n", picture_file);
      fputs("270 rotate\n", picture_file);
    }
    scale = 1.0 / ((real)dev_scale);
    fprintf(picture_file, "%f %f scale\n", scale, scale);
    fprintf(picture_file, "%d setlinewidth\n", dev_scale);
  } else {
    plot_number++;
    if (plot_number == images_per_frame) {
      plot_number = 0;
      image_number++;
      if (image_number == frames_per_page) {
	send_page = true;
	image_number = 0;
      }
    }
  }
  if (send_page) {
    postscript_page();
    if (image_mode == landscape) {
      /* Need to shift to actual top of page! */
      fputs("0 840 translate\n", picture_file);
      fputs("270 rotate\n", picture_file);
    }
    scale = 1.0 / ((real)dev_scale);
    fprintf(picture_file, "%f %f scale\n", scale, scale);
    send_page = false;
  }
}

static void put_postscript_title(char *title, integer x, integer y,
				 boolean frame)
{
  integer i = 0, subscript, superscript, font, font_sub;
  boolean small = false, up;

  if (title[0] == 0)
    return;
  if (frame) {
    subscript = fr_subscript_size / 2;
    superscript = fr_subscript_size;
    font = frame_title_size;
    font_sub = fr_subscript_size;
  } else {
    subscript = subscript_size / 2;
    superscript = subscript_size;
    font = title_size;
    font_sub = subscript_size;
  }
  fprintf(picture_file, "%d %d moveto\n", x, y);
  fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n", font);
  fputs("(", picture_file);
  while (title[i] != 0) {
    if (title[i] == '^') {
      i++;
      fputs(") show\n", picture_file);
      fprintf(picture_file, "  0 %d rmoveto\n", superscript);
      fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	      font_sub);
      fputs("(", picture_file);
      if (title[i] == '{') {
	up = true;
	small = true;
      } else {
	fprintf(picture_file, "%c) show\n", title[i]);
	fprintf(picture_file, "  0 %d rmoveto\n", -superscript);
	fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
		font);
	fputs("(", picture_file);
      }
    } else if (title[i] == '_') {
      i++;
      fputs(") show\n", picture_file);
      fprintf(picture_file, "  0 %d rmoveto\n", -subscript);
      fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	      font_sub);
      fputs("(", picture_file);
      if (title[i] == '{') {
	up = false;
	small = true;
      } else {
	fprintf(picture_file, "%c) show\n", title[i]);
	fprintf(picture_file, "  0 %d rmoveto\n", subscript);
	fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
		font);
	fputs("(", picture_file);
      }
    } else if (title[i] == '}' and small) {
      small = false;
      fputs(") show\n", picture_file);
      if (up)
	fprintf(picture_file, "  0 %d rmoveto\n", -superscript);
      else
	fprintf(picture_file, "  0 %d rmoveto\n", subscript);
      fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	      font);
      fputs("(", picture_file);
    } else
      putc(title[i], picture_file);
    i++;
  }
  fputs(") show\n", picture_file);
}

static void postscript_image()
{
  integer row, col;

  if (image_number == 0) {
    put_postscript_title(title, page_x_margin + ytext + ylabel,
			 page_y_margin + rows_per_page * frame_height +
			 title_size / 2, false);
    frame_x_edge = page_x_margin + frame_x_margin;
    frame_y_edge = page_y_margin + frame_y_margin;
  } else {
    if (horizontal) {
      col = image_number / columns_per_page;
      row = image_number - col * columns_per_page;
    } else {
      row = image_number / rows_per_page;
      col = image_number - row * rows_per_page;
    }
    frame_x_edge = page_x_margin + row * frame_width + frame_x_margin;
    frame_y_edge = page_y_margin + col * frame_height + frame_y_margin;
  }
  if (frames_per_page != 1)
    put_postscript_title(frame_title, frame_x_edge + ytext + ylabel,
			 frame_y_edge - frame_y_margin + frame_height -
			 3 * frame_title_size / 2, true);
}

static void postscript_border(plot_data plot)
{
  integer i, ndigits, magnitude, x_offset, y_offset;
  real energy, step, scale, height, low, plot_start, plot_range;

  fprintf(picture_file, "%d setlinewidth\n", dev_scale);
  /* The border */
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset = frame_y_edge + xtext + xlabel;
  fputs("newpath\n", picture_file);
  /* Bottom X axis */
  fprintf(picture_file, "  %d %d moveto\n", x_offset, y_offset);
  fprintf(picture_file, "  %d 0 rlineto\n", image_width);
  /* Right Y axis */
  fprintf(picture_file, "  0 %d rlineto\n", bgap + image_height + tgap);
  /* Top X axis */
  fprintf(picture_file, "  %d 0 rlineto\n", -image_width);
  /* Left Y axis */
  fputs("  closepath\n",picture_file);
  fputs("stroke\n\n", picture_file);
  /* The labels */
  fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	  title_size);
  fprintf(picture_file, "%d %d moveto\n",
	  frame_x_edge + ytext + ylabel + image_width / (columns_per_page + 2),
	  frame_y_edge);
  fputs("(Energy \\050eV\\051) show\n", picture_file);
  fputs("gsave\n", picture_file);
  fprintf(picture_file, "%d %d moveto\n", frame_x_edge + ytext,
	  frame_y_edge + xtext + xlabel + image_height / (rows_per_page + 2));
  magnitude = Trunc(rlog10(largest_peak));
  fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	  title_size);
  fputs("90 rotate\n", picture_file);
  fputs("(Intensity", picture_file);
  if (magnitude != 0) {
    fputs(" \\050x10) show\n", picture_file);
    fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	    subscript_size);
    fprintf(picture_file, "0 %d rmoveto\n", title_size / 2);
    fprintf(picture_file, "(%d) show\n", magnitude);
    fprintf(picture_file, "0 %d rmoveto\n", -title_size / 2);
    fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	    title_size);
    fputs("(\\051", picture_file);
  }
  fputs(") show\nstroke\n", picture_file);
  fputs("grestore\n", picture_file);
  /* The marks */
  x_offset = frame_x_edge + ytext;
  y_offset = frame_y_edge + xtext;
  plot_start = plot->min_energy;
  plot_range = plot->max_energy - plot_start;
  step = Upper(plot_range / ((real)image_width) * ((real)xstep));
  ndigits = Trunc(rlog10(plot_range)) + 1;
  /* Marks for the X axis */
  scale = ((real)image_width) / (plot_range);
  fprintf(picture_file, "/Times-Roman findfont %d scalefont setfont\n",
	  label_size);
  for (energy = step; energy < plot_range; energy += step) {
    i = x_offset + ylabel + Round(energy * scale);
    fprintf(picture_file, "newpath\n  %d %d moveto\n", i, y_offset + xlabel);
    fprintf(picture_file, "  0 %d rlineto\n", 3 * dev_scale);
    fprintf(picture_file, "  %d %d moveto\n", i,
	    y_offset + xlabel + bgap + image_height + tgap);
    fprintf(picture_file, "  0 %d rlineto\nstroke\n", -dev_scale);
    fprintf(picture_file, "  %d %d moveto\n", i - 3 * dev_scale * ndigits,
	    y_offset);
    fprintf(picture_file, "  (%*.0f) show\n", ndigits, energy + plot_start);
  }
  height = largest_peak * rpow(10.0, -(real)magnitude);
  low = lowest_peak * rpow(10.0, -(real)magnitude);
  ndigits = 1;
  step = Upper((height - low) * ((real)ystep) / ((real)image_height));
  if (images_per_frame > 1) {
    if (image_height < image_width)
      scale = ((real)(3 * image_height / 4) / (height - low));
    else
      scale = ((real)(3 * image_width / 4) / (height - low));
  } else
    scale = ((real)image_height / (height - low));
  for (energy = Upper(low); energy < height; energy += step) {
    i = y_offset + xlabel + bgap + Round((energy - low) * scale);
    fprintf(picture_file, "newpath\n  %d %d moveto\n", x_offset + ylabel, i);
    fprintf(picture_file, "  %d 0 rlineto\n", 2 * dev_scale);
    fprintf(picture_file, "  %d %d moveto\n",
	    x_offset + ylabel + image_width, i);
    fprintf(picture_file, "  %d 0 rlineto\nstroke\n", -2 * dev_scale);
    fprintf(picture_file, "  %d %d moveto\n", x_offset + ylabel / 2,
	    i - 2 * dev_scale);
    fprintf(picture_file, "  (%.1f) show\n", energy);
  }
  fputs("\n", picture_file);
  fprintf(picture_file, "%d setlinewidth\n", line_width);
}

static void postscript_spectrum(plot_data plot, real spectrum[])
{
  register integer i, x_offset, y_offset;
  real height, step_size, diff;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  diff = largest_peak - lowest_peak;
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset += frame_y_edge + xtext + xlabel + bgap;
  fputs("newpath\n", picture_file);
  fprintf(picture_file, "  %d %d moveto\n", x_offset, y_offset +
	  Round(height * (spectrum[0] - lowest_peak) / diff));
  step_size = ((real)image_width) / ((real)plot->npoints);
  for (i = 1; i < plot->npoints; i++)
    fprintf(picture_file, "  %d %d lineto\n",
	    x_offset + Round(step_size * (real)i),
	    y_offset + Round(height * (spectrum[i] - lowest_peak) / diff));
  if (lowest_peak < 0.0)
    fprintf(picture_file, "%d %d moveto\n %d %d rlineto\n", x_offset,
	    y_offset - Round(height * lowest_peak / diff), image_width, 0);
  fputs("stroke\n\n", picture_file);
}

static void postscript_sticks(plot_data plot, real lowest, real largest)
{
  register integer i, x_offset, y_offset, y_value;
  real height, step_size;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset += frame_y_edge + xtext + xlabel + bgap;
  step_size = ((real)image_width) / ((real)plot->npoints);
  height = ((height / stick_scale)) / (largest - lowest);
  y_offset -= Round((real)image_height * lowest_peak /
		    (largest_peak - lowest_peak));
  fputs("newpath\n", picture_file);
  for (i = 1; i < plot->npoints; i++)
    if (rabs(plot->sticks[i]) > approx_zero) {
      y_value = Round(height * plot->sticks[i]);
      if (y_value != 0) {
	fprintf(picture_file, "  %d %d moveto\n",
		x_offset + Round(step_size * (real)i), y_offset);
	fprintf(picture_file, "  0 %d rlineto\n", y_value);
      }
    }
  fputs("stroke\n\n", picture_file);
}

static void postscript_comparison(plot_data plot, plot_data comp, real shift,
				  real largest)
/* send the comparison image */
{
  register integer i, start, x_offset, zero_offset, y_offset;
  real energy, plot_start, plot_range, scale, escale, height;

  if (images_per_frame == 1) {
    y_offset = 0;
    height = (real)image_height;
  } else {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
  }
  x_offset = frame_x_edge + ytext + ylabel;
  y_offset = frame_y_edge + xtext + xlabel + bgap;
  energy = comp->min_energy + shift;
  plot_start = plot->min_energy;
  plot_range = plot->max_energy - plot_start;
  if (energy >= plot_start) {
    start = 0;
    energy -= plot_start;
  } else {
    start = Round((plot_start - energy) / comp->energy_step);
    energy = 0.0;
  }
  zero_offset = -Round(height * lowest_peak / (largest_peak - lowest_peak));
  escale = ((real)image_width) / plot_range;
  scale = ((height - (real)zero_offset)) / (largest);
  fputs("newpath\n", picture_file);
  fprintf(picture_file, "  %d %d moveto\n", x_offset + Round(energy * escale),
	  y_offset + Round(comp->spectrum[start] * scale));
  for (i = start + 1; i < comp->npoints and energy < plot_range; i++) {
    energy += comp->energy_step;
    fprintf(picture_file, "  %d %d lineto\n",
	    x_offset + Round(energy * escale),
	    y_offset + zero_offset + Round(comp->spectrum[i] * scale));
  }
  fputs("stroke\n\n", picture_file);
}

static void postscript_page()
{
  fputs("showpage\n\n", picture_file);
}
  
static void end_postscript_image()
{
  integer y_offset, zero_offset;
  real height;

  if (images_per_frame > 1) {
    if (image_height < image_width)
      height = 3.0 * (real)image_height / 4.0;
    else
      height = 3.0 * (real)image_width /4.0;
    y_offset = plot_number * ((image_height - (integer)height) /
			      (images_per_frame - 1));
    zero_offset = Round(height * lowest_peak / (largest_peak - lowest_peak));
    if (image_title[0] != 0)
      put_postscript_title(image_title, frame_x_edge + ytext + ylabel + 2,
			   frame_y_edge + xtext + xlabel + bgap + zero_offset
			   + y_offset + dev_scale, true);
  }
}

static void shut_postscript()
{
  fputs("showpage\n\n", picture_file);
}

/* The output routines for lists of points for other programs to access */

static void start_points()
/* Start the output for xy or ylist lists of points */
{
  if (not output_sent) {
    output_sent = true;
    image_number = 0;
  } else
    points_page();
}

static void xy_spectrum(plot_data plot, real spectrum[])
/* output the sets of (x,y) coords */
{
  register integer i;
  register real energy;

  energy = plot->min_energy;
  for (i = 0; i < plot->npoints; i++) {
    fprintf(picture_file, "%f %f\n", energy, spectrum[i]);
    energy += plot->energy_step;
  }
}

static void xy_sticks(plot_data plot)
{
  register integer i;
  register real energy = plot->min_energy;

  fputs("\nSticks\n", picture_file);
  for (i = 0; i < plot->npoints; i++) {
    if (rabs(plot->sticks[i]) > approx_zero)
      fprintf(picture_file, "%f %f\n", energy, plot->sticks[i]);
    energy += plot->energy_step;
  }
}

static void ylist_image(plot_data plot)
{
  register integer i;

  fprintf(picture_file, "%f\n", plot->min_energy);
  fprintf(picture_file, "%f\n", plot->max_energy);
  fprintf(picture_file, "%f\n",
	  (plot->max_energy - plot->min_energy) / ((real)plot->npoints));
  fprintf(picture_file, "%d\n", plot->npoints);
  for (i = 0; i < 12; i++)
    fputs("0\n", picture_file);
  if (title[0] != 0)
    fputs(title, picture_file);
  for (i = 0; i < 4; i++)
    fputs("\n", picture_file);
}

static void ylist_spectrum(plot_data plot)
/* Write out the list of y values */
{
  register integer i;

  for (i = 0; i < plot->npoints; i++)
    fprintf(picture_file, "%f\n", 10000.0 * plot->spectrum[i]);
}

static void ylist_sticks(plot_data plot)
{
  register integer i;
  register real energy = plot->min_energy;

  fputs("\nSticks\n", picture_file);
  for (i = 0; i < plot->npoints; i++) {
    if (rabs(plot->sticks[i]) > approx_zero)
      fprintf(picture_file, "%f %f\n", energy, 10000.0 * plot->sticks[i]);
    energy += plot->energy_step;
  }
}

static void points_page()
{
  fputs("\n\nNew Image\n", picture_file);
}

/* The shell routines that are always called */

static void start_output()
/* Send the commands required to start the picture file, or to prepare for
   the next image */
{
  /* Image number can be used to only start a new page after a certain
     number of images */
  im_scale = 10;
  switch (image_type) {
    case hpgl:
      start_hpgl();
      break;
    case postscript:
      start_postscript();
      break;
    case xy:
    case ylist:
      start_points();
      break;
    default:
      break;
  }
}

static void start_image(plot_data plot)
/* Output the commands to start the image in the file */
{
  if (plot_number != 0)
    return;
  switch (image_type) {
    case hpgl:
      hpgl_image();
      break;
    case postscript:
      postscript_image();
      break;
    case xy:
      break;
    case ylist:
      ylist_image(plot);
      break;
    default:
      break;
  }
  title[0] = 0;
  frame_title[0] = 0;
}

void find_peak_range(real spectrum[], integer npoints, real *lowest,
		     real *largest)
/* Find the largest value in the spectrum */
{
  register integer i;

  *largest = spectrum[0];
  *lowest = spectrum[0];
  for (i = 1; i < npoints; i++) {
    if (*largest < spectrum[i])
      *largest = spectrum[i];
    if (*lowest > spectrum[i])
      *lowest = spectrum[i];
  }
  if (adjust_scale)
    if (*lowest > 0.0)
      *lowest = 0.0;
}

static void make_border(plot_data plot)
/* set up the outline of the plot */
{
  if (plot_number != 0)
    return;
  if (!preserve_scale)
    find_peak_range(plot->spectrum, plot->npoints,
		    &lowest_peak, &largest_peak);
  switch (image_type) {
    case hpgl:
      hpgl_border(plot);
      break;
    case postscript:
      postscript_border(plot);
      break;
    default:
      break;
  }
}

static void send_spectrum(plot_data plot)
/* draw the spectra */
{
  real lowest, largest;

  find_peak_range(plot->spectrum, plot->npoints, &lowest, &largest);
  if (preserve_scale) {
    if (largest > largest_peak) {
      fprintf(stderr, "Image is larger than previous scale\n");
      return;
    }
  } else {
    largest_peak = largest;
    lowest_peak = lowest;
  }
  switch (image_type) {
    case hpgl:
      hpgl_spectrum(plot, plot->spectrum);
      break;
    case postscript:
      postscript_spectrum(plot, plot->spectrum);
      break;
    case xy:
      xy_spectrum(plot, plot->spectrum);
      break;
    case ylist:
      ylist_spectrum(plot);
      break;
    default:
      break;
  }
}

static void send_stick_image(plot_data plot)
/* sends the vertical lines representing the actual lines */
{
  real scale = 1.0, largest, lowest;

  if (images_per_frame != 1)
    return;
  if (plot->sticks == NULL)
    return;
  if (preserve_scale) {
    find_peak_range(plot->spectrum, plot->npoints, &lowest, &largest);
    scale = (largest - lowest) / (largest_peak - lowest_peak);
  }
  find_peak_range(plot->sticks, plot->npoints, &lowest, &largest);
  lowest /= scale;
  largest /= scale;
  switch (image_type) {
    case hpgl:
      hpgl_sticks(plot, lowest, largest);
      break;
    case postscript:
      postscript_sticks(plot, lowest, largest);
      break;
    case xy:
      xy_sticks(plot);
      break;
    case ylist:
      ylist_sticks(plot);
    default:
      break;
  }
}

void find_shift(plot_data plot, plot_data comp, real *shift,
		real *largest_peak)
/* Find the energy shift required to align the comparison spectrum with the
   one we have plotted. It also finds the largest value in the comparison
   spectrum */
{
  register integer i, p = 0, cp = 0;
  register real largest;

  largest = 0.0;
  for (i = 1; i < plot->npoints; i++)
    if (largest < plot->spectrum[i]) {
      largest = plot->spectrum[i];
      p = i;
    }
  largest = 0.0;
  for (i = 1; i < comp->npoints; i++)
    if (largest < comp->spectrum[i]) {
      largest = comp->spectrum[i];
      cp = i;
    }
  *largest_peak = largest;
  *shift = plot->energy_step * ((real)p) - comp->energy_step * ((real)cp)
				+ (plot->min_energy - comp->min_energy);
}

static void send_comparison(plot_data plot, plot_data comp, real shift,
			    boolean get_shift)
/* sends the comparison image */
{
  real lowest, largest;

  if (get_shift)
    find_shift(plot, comp, &shift, &largest);
  find_peak_range(comp->spectrum, comp->npoints, &lowest, &largest);
  if (preserve_scale) {
    if (largest > largest_peak) {
      fprintf(stderr, "Image is larger than previous scale\n");
      return;
    }
    largest = largest_peak;
  }
  switch (image_type) {
    case hpgl:
      hpgl_comparison(plot, comp, shift, largest);
      break;
    case postscript:
      postscript_comparison(plot, comp, shift, largest);
      break;
    default:
      break;
  }
}

static void subtract_spectra(real spectrum[], plot_data plot, plot_data sub,
			     real *lowest, real *largest)
{
  integer i;

  *lowest = 10000.0;
  *largest = -10000.0;
  for (i = 0; i < plot->npoints; i++) {
    spectrum[i] = plot->spectrum[i] - sub->spectrum[i];
    if (spectrum[i] < *lowest)
      *lowest = spectrum[i];
    if (spectrum[i] > *largest)
      *largest = spectrum[i];
  }
  if (*lowest > 0.0)
    *lowest = 0.0;
}

static void send_subtraction(plot_data plot, plot_data sub)
/* subtract the comparison spectrum from the current one */
{
  if (plot->energy_step != sub->energy_step and
      sub->min_energy != plot->min_energy and sub->npoints != plot->npoints) {
    fprintf(stderr, "Incompatible points for subtraction\n");
    return;
  }
  if (images_per_frame != 1) {
    fprintf(stderr, "Must only be 1 image per frame for subtraction\n");
    return;
  }
  spectrum = (real *) mem_alloc(plot->npoints * sizeof(real));
  subtract_spectra(spectrum, plot, sub, &lowest_peak, &largest_peak);
  if (largest_peak <= 0.0) {
    fprintf(stderr, "ERROR: No difference in spectra\n");
    return;
  }
  switch (image_type) {
    case hpgl:
      hpgl_spectrum(plot, spectrum);
      break;
    case postscript:
      postscript_spectrum(plot, spectrum);
      break;
    case xy:
      xy_spectrum(plot, spectrum);
    default:
      break;
  }
  free_mem(spectrum);
}  

static void shut_image()
/* Output the commands required to close this image */
{
  switch (image_type) {
    case hpgl:
      end_hpgl_image();
      break;
    case postscript:
      end_postscript_image();
      break;
    default:
      break;
  }
  image_title[0] = 0;
  preserve_scale = false;
}

void shut_output()
/* Send the commands required to end the picture file */
{
  switch (image_type) {
    case hpgl:
      shut_hpgl();
      break;
    case postscript:
      shut_postscript();
      break;
    default:
      break;
  }
  output_sent = false;
}

boolean open_output_file(char filename[])
{
  FILE *new_file;

  if (filename[0] == 0)
    return(false);
  if ((new_file = fopen(filename, "w")) == NULL)
    return(false);
  if (picture_file != NULL) {
    shut_output();
    fclose(picture_file);
  }
  picture_file = new_file;
  strcpy(picture_name, filename);
  return(true);
}

void test_flush()
{
  if (output_sent) {
    send_page = true;
    plot_number = -1;
    image_number = 0;
    switch (image_type) {
      case hpgl:
        init_hpgl_params();
	break;
      case postscript:
	init_postscript_params();
	break;
      default:
	break;
    }
  }
}

void print_spectrum(plot_data plot1, plot_data plot2, plot_modes mode,
		    real shift, boolean preserve)
{
  if (picture_file == NULL) {
    fprintf(stderr, "Aborting print\n");
    exit(1);
  }
  start_output();
  start_image(plot1);
  if (mode == plot_it)
    preserve_scale = preserve;    
  make_border(plot1);
  if (mode == subtract)
    send_subtraction(plot1, plot2);
  else if (mode == plot_it) {
    send_spectrum(plot1);
    send_stick_image(plot1);
  } else {
    send_spectrum(plot1);
    preserve_scale = preserve;
    send_comparison(plot1, plot2, shift, mode == compare);
  }
  shut_image();
  preserve_scale = false;
}

void print_peaks(plot_data plot)
{
  register long i, j, k = 0;
  register boolean direction = true;
  register double energy;

  if (title[0] != 0)
    printf("%s\n", title);
  energy = plot->min_energy;
  for (i = 0; i < plot->npoints - 1; i++) {
    if (direction) {
      if (plot->spectrum[i] > plot->spectrum[i + 1]) {
	peak_energy[k] = energy;
	peak_value[k] = plot->spectrum[i];
	k++;
	direction = false;
	printf("Peak %d at E = %f, height = %f\n", k, energy,
	       plot->spectrum[i]);
      }
    } else if (plot->spectrum[i] < plot->spectrum[i + 1])
      direction = true;
    energy += plot->energy_step;
  }
  for (i = 0; i < k - 1; i++)
    for (j = i + 1; j < k; j++)
      printf("Ratio of peak %d to %d is %f, delta E = %f\n", i + 1, j + 1,
             peak_value[i] / peak_value[j], peak_energy[j] - peak_energy[i]);
}
