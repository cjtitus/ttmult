
extern output_types image_type;
extern output_modes image_mode;
extern FILE *picture_file;
extern char picture_name[STRING_SIZE];
extern char title[LINE_SIZE], frame_title[LINE_SIZE], image_title[LINE_SIZE];
extern boolean horizontal, screen_vertical, preserve_scale;
extern integer rows_per_page, columns_per_page, images_per_frame,
	       frames_per_page;

extern void find_peak_range(real [], integer, real *, real *);
extern void find_shift(plot_data, plot_data, real *, real *);
extern void shut_output(void);
extern boolean open_output_file(char []);
extern void test_flush(void);
extern void print_spectrum(plot_data, plot_data, plot_modes, real, boolean);
extern void print_peaks(plot_data);
