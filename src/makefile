# Makefile for ttmult program suite on Ubuntu. 
# Created by Michael L. Baker and Charles Titus.
#
# Verified compilation of all targets on Linux Ubuntu
# 14.04. and 16.04.
#
# Usage: 
# make all
# make install
# make clean
#
# Install path is automatically set to ../bin, so that
# input files for rcn2 and rac are automatically set to ../inputs
# inputs/ bin/ and src/ are all subfolders of the same directory.
# This makes sense for testing, but can be modified for distribution.
#
# Simply modify INSTALL_PATH and INPUTS_PATH to the desired directory.
#
# 20171030: Have hopefully figured out the correct compiler flags to 
# use only gfortran, and leave ifort behind!


SHELL = /bin/bash

# general variables

CC = gcc
GNUF = gfortran
FC = ifort
# defaults

CFLAGS = -O2
IFFLAGS = -save -zero
GNUFLAGS = -fno-automatic -finit-local-zero -fno-align-commons -O2
IFORTTARGETS = ttban ttban_exact ttban_moremem ttban_exact_moremem ttrcg ttrac rac_converter rcn2 rcn31 ttplo
OUTPUTS = ttban ttban_exact ttban_moremem ttban_exact_moremem ttrcg ttrac rac_converter rcn2 rcn31 ttplo
TARGETS = ttban_gnu ttban_exact_gnu ttrcg_gnu ttrac rac_converter rcn2_gnu rcn31_gnu ttplo
INPUTS = disk0 rcn2.inp rcn2q.inp

SYSTEM = -DUNIX
# $(dir $(CURDIR)) is a very useful bit of magic that gets current directory!
INSTALL_PATH=$(dir $(CURDIR))bin
INPUTS_PATH=$(dir $(CURDIR))inputs


# targets
all: $(TARGETS)

ifort: $(IFORTTARGETS)

ttban_gnu: ttban_memory_allocation_Green.f makefile
	$(GNUF) $(GNUFLAGS) -w -o ttban ttban_memory_allocation_Green.f

ttban: ttban_memory_allocation_Green.f makefile
	$(FC) $(IFFLAGS) -o ttban ttban_memory_allocation_Green.f

ttban_moremem: ttban_memory_allocation_moremem.f makefile
	$(FC) $(IFFLAGS) -mcmodel=medium -o ttban_moremem ttban_memory_allocation_moremem.f

ttban_exact_gnu_old: Bander_Exact_Aug_22_2013.f makefile
	$(GNUF) $(GNUFLAGS) -w -o ttban_exact Bander_Exact_Aug_22_2013.f

ttban_exact_gnu: ttban_exact.f makefile
	$(GNUF) $(GNUFLAGS) -w -o ttban_exact ttban_exact.f

ttban_exact: Bander_Exact_Aug_22_2013.f makefile
	$(FC) $(IFFLAGS) -heap-arrays -o ttban_exact Bander_Exact_Aug_22_2013.f

ttban_exact_moremem: Bander_Exact_moremem.f makefile
	$(FC) $(IFFLAGS) -mcmodel=medium -heap-arrays -o ttban_exact_moremem Bander_Exact_moremem.f

ttrcg_gnu: ttrcg.f makefile
	$(GNUF) $(GNUFLAGS) -o ttrcg ttrcg.f

ttrcg: ttrcg.f makefile
	$(FC) $(IFFLAGS) -o ttrcg ttrcg.f

ttrac: ttrac.o makefile
	$(CC) $(CFLAGS) -g -ansi -DR4000 -o ttrac ttrac.o -lm

ttrac.o: ttrac.c makefile
	$(CC) $(CFLAGS) -g -ansi -DR4000 -DDISK0="$(INPUTS_PATH)/disk0" -c -o ttrac.o ttrac.c

rac_converter: rac_converter.cpp makefile
	g++ -g -ansi -o rac_converter rac_converter.cpp

rcn2_gnu: rcn2.f makefile
	$(GNUF) $(GNUFLAGS) -o rcn2 rcn2.f

rcn31_gnu: rcn31.f
	$(GNUF) $(GNUFLAGS) -o rcn31 rcn31.f

rcn2: rcn2.f makefile
	$(FC) $(IFFLAGS) -o rcn2 rcn2.f

rcn31: rcn31.f
	$(FC) $(IFFLAGS) -o rcn31 rcn31.f

ttplo: ttplo.o spectra.o  input.o  devices.o  system.o  common.o
	$(CC) $(CFLAGS) -o  ttplo ttplo.o spectra.o  input.o  devices.o  system.o  common.o -lm

ttplo.o: ttplo.c globals.h input.h spectra.h devices.h common.h makefile
	$(CC) $(CFLAGS) -o  ttplo.o -c ttplo.c

spectra.o: spectra.c spectra.h globals.h makefile
	$(CC) $(CFLAGS) -o  spectra.o -c spectra.c

input.o: input.c spectra.h input.h globals.h common.h makefile
	$(CC) $(CFLAGS) -o  input.o -c input.c

devices.o: devices.c globals.h spectra.h devices.h common.h makefile
	$(CC) $(CFLAGS) -o  devices.o -c devices.c

common.o: common.c devices.h spectra.h common.h input.h globals.h makefile
	$(CC) $(CFLAGS) -o  common.o -c common.c

system.o: system.c system.h makefile
	$(CC) $(CFLAGS) $(SYSTEM) -o  system.o -c system.c



clean:
	rm *.o 

install:
	if [ ! -d $(INSTALL_PATH) ]; then mkdir $(INSTALL_PATH); fi
	mv $(OUTPUTS) $(INSTALL_PATH)
	if [ ! -d $(INPUTS_PATH) ]; then mkdir $(INPUTS_PATH); fi
	cp $(INPUTS) $(INPUTS_PATH)

install-ttban:
	if [ ! -d $(INSTALL_PATH) ]; then mkdir $(INSTALL_PATH); fi
	mv ttban $(INSTALL_PATH)


