

/* the functions 
   
       mem_alloc free_mem cut_blocks and cut_region
   
   and the variables 
   
       heap and debug
   
   are used in the fortran program.
 In order to be recognised when the fortran- and this c program are linked
 some machine dependent conventions must be obeyed. In this c program an
 underscore has to be prepended or appended or both.
 
                   functions      variables
                   
 On CONVEX           both           both
 On SGI              append         append
 On SUN              append         append
 On HP               none           none
    
 */
 
  

#define heap       _heap_       
#define debug      _debug_      
#define mem_alloc  _mem_alloc_  
#define free_mem   _free_mem_   
#define cut_blocks _cut_blocks_ 
#define cut_region _cut_region_ 

/*

#define heap        heap_       
#define debug       debug_      
#define mem_alloc   mem_alloc_  
#define free_mem    free_mem_   
#define cut_blocks  cut_blocks_ 
#define cut_region  cut_region_ 
*/




#include <stdlib.h>
#include <stdio.h>


typedef long integer;
typedef float real;

typedef struct memory_node {
  struct memory_node *next;
  void *address;
  unsigned long size;
} *mem_block;

static mem_block blocks = NULL, free_nodes = NULL;
static integer num_nodes = 0;


#ifdef __STDC__   /* ANSI C */

void mem_alloc(integer *, integer *);
void free_mem(integer *, integer *);
void cut_blocks(integer *, integer *);
void cut_region(integer *, integer *, integer *);

void check_heap(void);

#endif           /* ANSI C */

extern real heap[10];
extern integer debug;

real *x_address = heap - 1;
integer *deb = &debug;



void mem_alloc(istart, size)
integer *istart, *size;
{
  void *ptr;
  mem_block node;
  unsigned bsize = *size * sizeof(real);

  if (bsize == 0)
    bsize = 4;
  ptr = malloc(bsize);
  if (ptr == NULL) {
    fprintf(stderr, "mem_alloc_ ERROR: Insufficient free space\n");
    exit(0);
  }
  if (free_nodes == NULL)
    node = (mem_block) malloc(sizeof(struct memory_node));
  else {
    node = free_nodes;
    free_nodes = free_nodes->next;
  }
  if (node == NULL) {
    fprintf(stderr, "ERROR: failed to allocate block\n");
    exit(0);
  }
  node->next = blocks;
  blocks = node;
  node->address = ptr;
  node->size = (*size);
#ifdef DEBUG
  if (*deb)
    printf("Block at %d, memory at %d (%d)\n", node, ptr, *size);
#endif
  (*istart) = (integer)((real *)blocks->address - x_address);
  num_nodes++;
}

void free_mem(istart, size)
integer *istart, *size;
{
  mem_block node = blocks, prev;
  void *ptr;

  ptr = (void *)(x_address + (*istart));
  while (node->address != ptr) {
    prev = node;
    node = node->next;
  }
  if ((*size) != node->size) {
    fprintf(stderr, "Size error in dealloc\n");
    exit(0);
  }
#ifdef DEBUG
  if (*deb)
    printf("Freeing block at %d\n", node);
#endif
  if (node == blocks)
    blocks = blocks->next;
  else
    prev->next = node->next;
  node->next = free_nodes;
  free_nodes = node;  
  free((char *)node->address);
  num_nodes--;
}

void cut_blocks(cut_index, last)
integer *cut_index, *last;
{
  mem_block node = blocks;
  integer ptr_index;

  do {
#ifdef DEBUG
    if (*deb)
      printf("Cutting block at %d\n", node);
#endif
    blocks = blocks->next;
    node->next = free_nodes;
    free_nodes = node;
    (*last) -= node->size;
    ptr_index = (real *)node->address - x_address;
    free((char *)node->address);
    node = blocks;
    num_nodes--;
  } while (ptr_index != (*cut_index));
}

void check_heap()
{
  mem_block node = blocks;
  integer count = 0;

  while (node != NULL) {
    node = node->next;
    count++;
  }
  if (count != num_nodes) {
    fprintf(stderr,
	    "ERROR: Heap is probably corrupt, found %d nodes out of %d\n",
	    count, num_nodes);
    exit(0);
  }
}

void cut_region(st_index, last_index, size)
integer *st_index, *last_index, *size;
{
  mem_block node = blocks, prev, temp;
  integer ptr_index;
  void *ptr;

  ptr = (void *)(x_address + (*st_index));
  if (ptr == blocks->address)
    cut_blocks(last_index, size);
  else {
    while (node->address != ptr) {
      prev = node;
      node = node->next;
    }
    node = prev;
    do {
      node = node->next;
#ifdef DEBUG
      if (*deb)
	printf("Removing block at %d\n", node);
#endif
      (*size) -= node->size;
      ptr_index = (real *)node->address - x_address;
      free((char *)node->address);
      num_nodes--;
    } while (ptr_index != (*last_index));
    temp = node->next;
    node->next = free_nodes;
    free_nodes = prev->next;
    prev->next = temp;
  }
}
