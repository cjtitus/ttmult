
CCCCC&-------------9 OUTPUT ; 10 INPUT ---------------------
C -- Thole 23 jan 1991
C --  VAX
      PROGRAM RCG9
C    &     RCG-MOD 9, MAIN CONTROL PROGRAM
C
C
C    &     PROGRAM TO COMPUTE COEFFICIENT MATRICES FOR ATOMIC
C    &          ENERGY-LEVEL PARAMETERS (FK, GK, ZETA, ALPHA,
C    &          BETA, GAMMA, RK), READ PARAMETER VALUES,
C    &          AND COMPUTE AND DIAGONALIZE THE  ENERGY MATRICES
C    &          TO OBTAIN ENERGY LEVELS AND EIGENVECTORS.
C    &     ALSO, if DESIRED, COMPUTE ANGULAR COEFFICIENT MATRICES
C    &          FOR MAGNETIC DIPOLE AND
C    &          FOR ELECTRIC DIPOLE AND QUADRUPOLE TRANSITIONS,
C    &          READ RADIAL MULTIPOLE INTEGRALS, AND CALCULATE
C    &          OSCILLATOR STRENGTHS AND TRANSITION PROBABILITIES.
C
C    &     FIRST VERSION (RCG MOD 0) CODED BY B. FAGAN AND R. D. COWAN,
C    &          LOS ALAMOS SCIENTIFIC LABORATORY, SUMMER AND FALL, 1964.
C
C    &     THIS VERSION (RCG MOD 9, INCLUDING CONFIGURATION INTERACTION)
C    &          CODED FALL,1968 BY R. D. COWAN FOR THE IBM 7030(STRETCH)
C    &          --- MODIFIED FOR THE CDC 6500
C    &          AT PURDUE UNIVERSITY, MARCH, 1971.  CONVERTED JULY-NOV,
C    &          1971 TO LASL CDC 7600, USING LARGE CORE MEMORY.
C    &          DIMENSIONS INCREASED TO 8 SUBSHELLS, FEB, 1975.
C    &          AUTOIONIZATION TRANS PROB CALCS ADDED SPRING 1975
C    &          PLANE-WAVE-BORN COLLISION-STRENGTH OPTIONS ADDED
C    &          JAN-FEB 1980 BY W. D. ROBB AND R. D. COWAN.
C    &          MODIFIED TO ALLOW DIFFERENT L FOR FINAL SUBSHELL, JULY 1
C    &          AND TO ACCEPT RESCALING CARDS, OCTOBER 1981.
C    &          FASTER 3NJ ROUTINES INCORPORATED JULY 21, 1983.
C    &          MODIFIED FOR CRAY 1 COMPUTERS, DECEMBER 1983.
C    &          MODIFIED FOR ZEEMAN-LAB CYBER 170/750, FEBRUARY 1984
C
C              sparse input and output to 20 31 32 40 72 73 74 routines. B.T.Thole
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER,KLAM=3000)

      PARAMETER (KBGKS=1,KTRAN=1)
C  KMX MUST BE 3*KLSI BECAUSE OF EQUIVALENCE OF U1(KLSI,KLSI,9) AND
C    & C(KMX,KMX)
      parameter (maxtensors=14)
      parameter (maxopers=50)
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      common/inpfile/ iInp
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ANAM,COUPL1*6
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  nosubc,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NSCRJ4,SCRJ4,SCRJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      DIMENSION DUMC2(2+KS*100+KMX+KS*KMX),DUM1C2(6*KMX+12*KC)
      EQUIVALENCE(DUMC2,NI),(DUM1C2,NI)
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      DIMENSION DUMC3  (8*KLAM+4*KMX+8*66+3*82)
      DIMENSION DUM1C3 (KC+2*KS+1+110+6*KS*KMX+7*KMX+2*KJP*KS+KLSC)
      DIMENSION C3MUPOL(KC+KS+KMX*KS+KMX+KMX*KS*2*2+KLS1*KLS1)
      EQUIVALENCE (CFP2, DUMC3, DUM1C3, C3MUPOL)

      COMMON  U1(KLSI,KLSI,9)
      COMMON  NALSP(KLSC,KS),PSCRL(KLSC,KS),PSCRS(KLSC,KS),NCFGP(KLSC),
     &        NJK(KJK,KS) ,  NCFGJP(KJP,KS),MULS1(KLSC),   MULS4(KLSC)
      DIMENSION DUMBLNK(KMX,KMX)
      DIMENSION DU2BLNK(KMX*KMX+KPR*2+9*2+6+KC+4*KLAM)
      EQUIVALENCE (DUMBLNK , DU2BLNK , U1)

      COMMON /C3LC2/ CFGP1(KLSI,KLSI,7),PC(KPC)
      DIMENSION DUMLC2(KMX*KMX), CFP1(KLS1,KLS1)
      EQUIVALENCE(DUMLC2,CFP1,CFGP1)
      COMMON /C3LC3/ V1(KLSI,KLSI,9)
      DIMENSION DUMLC3(KMX,KMX),DUM2LC3(KLAM)
      EQUIVALENCE(DUMLC3,DUM2LC3,V1)
      COMMON /C3LC4/ ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      DIMENSION DUMLC4(KMX*KMX+KC*KC),DUM2LC4(KLAM)
      EQUIVALENCE(DUMLC4,DUM2LC4,ISER)
      COMMON /C3LC5/ GOSS(KTRAN,KBGKS)
      DIMENSION DUMLC5(KMX,KMX)
      EQUIVALENCE(DUMLC5,GOSS)
      COMMON /C5/    IGEN,IRNQ,IRND,IRMN,
     &               DELEKEV,DELEAV,UENINP,FACT0(5)
      COMMON /CTKEV/ NPTKEV,NTKEV,TKEV(6),TUNRGY(6),BRNCHL(6),
     &               BRNCHT(6), EIONRY
      DIMENSION SJNK(2),SJXK(2), ISPIN(KS), iorbit(ks) 
     &         ,itensor(ks,2,maxtensors)
     &         ,ioper(20,maxopers)
      COMMON /LEVEL/ NLEVELS,ELEVELS
      COMMON /CBUTLER/ IBUTLERx,JJcouplingx,IRREPdotsx
      CHARACTER LINE*80
      logical  OPTION, shifts
C
      DATA PARNAM(1,1),PARNAM(1,2)/'EAV','EAV'/
      DATA (COUPL(I),I=1,12)/'    LS','    JJ','  JJJK','  LSLK',
     &  '  LSJK','LSJLKS','LSJLSJ','J1-LSJ',' ',' ',' ','    SL'/
      DATA (LSYMB(I),I=1,24)/'S','P','D','F','G','H','I',
     &  'K','L','M','N','O','Q','R','T','U','V','W',
     &  'X','Y','Z','A','B','C'/

      OPTION ( ioption, kk ) = ioption.eq.kk .or. ioption.eq.3
C   CHECK KLSI AND KMX   AND KLAM AND KMX
      if (KMX .ne.3*KLSI) STOP 'KMX MUST BE 3*KLSI'
      if (KLAM.gt.KMX**2) STOP 'KLAM MUST BE .LE. KMX**2'
      if (MOD(KISER,8).ne.0) STOP 'KISER MUST BE A MULTIPLE OF 8'

C     HP-UX Problems
      OPEN(2, STATUS = 'SCRATCH')
      OPEN( 9, STATUS = 'UNKNOWN',  FILE = 'fort.9',
     &     ACCESS = 'SEQUENTIAL', FORM = 'FORMATTED')
      OPEN(10, STATUS = 'UNKNOWN',  FILE = 'fort.10',
     &     ACCESS = 'SEQUENTIAL', FORM = 'FORMATTED')
      OPEN(20, STATUS = 'SCRATCH', FORM = 'UNFORMATTED')
      OPEN(31, STATUS = 'SCRATCH', FORM = 'UNFORMATTED')
      OPEN(32, STATUS = 'SCRATCH', FORM = 'UNFORMATTED')
      OPEN(41, STATUS = 'SCRATCH', FORM = 'UNFORMATTED')
      OPEN(72, STATUS = 'UNKNOWN',  FILE = 'fort.72',
     &     ACCESS = 'SEQUENTIAL', FORM = 'UNFORMATTED')
      OPEN(73, STATUS = 'UNKNOWN', FILE = 'fort.73',
     &     ACCESS = 'SEQUENTIAL', FORM = 'UNFORMATTED')
      OPEN(74, STATUS = 'UNKNOWN', FILE = 'fort.74',
     &     ACCESS = 'SEQUENTIAL', FORM = 'UNFORMATTED')

C     SET INPUT AND OUTPUT DISK UNIT NUMBERS
C
      iInp  = 10
      IW    = 9
      ID2DEF= 72
      ILA   = 31
      ILB   = 32
      IC    = 41
C
C    &     CALCULATE TABLE OF SCALED FACTORIALS
C
      call CALCFCT
C
C    &     READ FIRST CONTROL CARD (KCPL.gt.2)
C    &          (ID2.gt.0--E.G. ID2=42 OR 72-- if CUVFD TO BE called)
C
      NTKEV=0
      NPTKEV=0
      DELEKEV=0.0
      IABG=1
      IV=0
      ID2=0
C
C    &     READ FINAL CONTROL CARD (OR RESCALE CARD--KCPL=0)
C
C    &     READ KIND OF COUPLING (1=LS, OR 2=JJ),
C    &          NUMBER OF SUB-SHELLS IN EACH CONFIGURATION, AND
C    &          NUMBER OF CONFIGURATIONS (FOR EACH PARITY).
C    &     if IABG.GE.1, PARAMETERS ALPHA, BETA, GAMMA ARE INCLUDED
C    &          FOR EQUIVALENT ELECTRONS.  if IABG=2 OR 4, then ILLEGAL-
C    &          PARAMETERS ARE INCLUDED FOR NON-EQUIVALENT ELECTRONS.
C    &          if IABG.GE.3, then SL COUPLING IS USED INSTEAD OF LS.
C    &     if EAV=-55555555., INPUT FOR LEAST-SQUARES ENERGY-LEVEL-FITTI
C    &          PROGRAM RCE IS WRITTEN ON TAPE 2.
C    &     if ICTBCD.ne.0, INPUT FOR ARGONNE LEAST-SQUARES PROGRAM
C    &          IS WRITTEN ON TAPE 19.  (TAPES 2 AND 19 CANNOT
C    &          BOTH BE WRITTEN IN THE SAME RUN.)
C    &     if IGEN=KCPLD(3).GE.5, PLANE-WAVE-BORN COLLISION
C    &          STRENGTHS WILL BE CALCULATED.
C
      COUPL1=COUPL(1)
C     KS=8
C     KC=5

      JJcouplingx = 0
      IRREPdotsx = 0

   96 IRW19=0
      call CLOK (TIME0)
      call STAMP( ' ' )
      rewind 2
      DELEAV  =  0
      UENRGY  =  0
      FACT0(1)=  0
      FACT0(2)=  0
      FACT0(3)=  0
      FACT0(4)=  0
      FACT0(5)=  0
      NLEVELS =  0
      ELEVELS = -1
      IRNQ    =  0
      IRXQ    = -10
      IUMIN   =  0
      IUMAX   =  0
      IXCHANG =  0
      NISUBC  = KS
      IBUTLERx=  0
      call ciinit
      call limitinit

      ntensors=  0
      call izero ( itensor,ks*2*maxtensors )

      nopers  = 0
      do 95 iop = 1,maxopers
      do 95 iplace = 1,20
   95   ioper(iplace,iop) = -1000
   
      do 97 IS = 1,KS
         ISPIN (IS) = 0
         Iorbit(IS) = 0
   97    IQUAD (IS) = 0
  100 call GETLINE (LINE)
      READ(LINE,10) KCPL
      if (KCPL.LT.0) STOP 'NORMAL END:  KCPL<0'

      if (KCPL.eq.0) then
        READ(LINE,'(20X,F10.5,5F2.2,10X,F9.4)') DELEAV,FACT0,UENRGY
        do 102 I=1,5
  102     if (FACT0(I) .eq. 0.99) FACT0(I)=1
        GO TO 100

      else if (KCPL.eq.10) then
C&--   THOLE. CHANGES TO CALCULATE GENERAL MULTIPOLE  SPECTRA
C&--   II=-1,0,1,2,3... CALCULATES MAGN. DIP, ELEC. MONO, DIP,
C&--   QUAD AND OCT ETC. SPECTRA
C&--   NLEVELS AND ELEVELS MAKE  SPECTR PRINT ONLY LINES FROM
C&--   LOWEST LEVELS OF EACH J OF THE GROUND STATE.
      if (LINE(6:15).ne.' ') READ(LINE(6:15),'(I5,E5.0)') NLEVELS,ELEVEL
                             READ(LINE(16:20),  '(I5)'  ) IBUTLERx
      if (LINE(21:30).ne.' ')READ(LINE(21:30), '(2I5)'  ) IUMIN,  IUMAX
      if (LINE(31:40).ne.' ')READ(LINE(31:40), '(2I5)'  ) IRNQ,   IRXQ

  110 IPOS = INDEX (LINE,'SHELL') + 4
      if (IPOS.gt.4) then
          READ(LINE(IPOS+1:IPOS+KS),'(18I1)') IQUAD
      endif

      if (index(line,'JJCOUPLING').ne.0) JJcouplingx = 1
      if (index(line,'IRREPDOTS').ne.0) IRREPdotsx = 1
      if (index(line,'SHIFT').ne.0) shifts = .true.

      ipos  = 1
  105 IPOSx = INDEX (LINE(ipos:),'TENSOR' ) + 5
      if (IPOSx.gt.5) then
          ipos = ipos + iposx-1
          ntensors = ntensors + 1
          call warndim ( ntensors, maxtensors, 'maxtensors')
          READ(LINE(IPOS+1:IPOS+3+KS),'(18I1)')
     &    (itensor(ispace,1,ntensors),ispace=1,3),
     &    (itensor(ishell,2,ntensors),ishell=1,ks)
           print'(1x,i2,a,3I1,1x,20i1)',ntensors,' tensor ',
     &    (itensor(ispace,1,ntensors),ispace=1,3),
     &    (itensor(ishell,2,ntensors),ishell=1,ks)
          goto 105
      endif

      ipos  = 0
  106 IPOSx = INDEX (LINE(ipos+1:),'OPER' ) + 3
      if (IPOSx.gt.3) then
          ipos = ipos + iposx
          nopers = nopers + 1
          call warndim ( nopers, maxopers, 'maxopers')
	  iposy =  index(line(ipos+1:),' ') - 1
	  if(iposy.eq.-1) iposy = 80 - ipos
          ioper(20,nopers) = iposy
          READ(LINE(IPOS+1:IPOS+iposy),'(20I1)')
     &    (ioper(iplace,nopers),iplace=1,iposy)
          print*,ioper(20,nopers)
          print'(1x,i2,a,3(2I1,1x),14i1)',nopers,' oper ',
     &      (ioper(iplace,nopers),iplace=1,ioper(20,nopers))
          ipos = ipos + iposy
          goto 106
      endif
      
      call ciread(line)
      call limitread(line)

      IPOS = INDEX (LINE,'SPIN' ) + 3
      if (IPOS.gt.3) then
          READ(LINE(IPOS+1:IPOS+KS),'(18I1)') ISPIN
      endif
      IPOS = INDEX (LINE,'ORBIT') + 4
      if (IPOS.gt.4) then
          READ(LINE(IPOS+1:IPOS+KS),'(18I1)') Iorbit
      endif
      IPOS = INDEX (LINE,'INTER') + 4
      if (IPOS.gt.4) then
          READ(LINE(IPOS+1:IPOS+1),'(I1)') NISUBC
       endif
      if (index(line,'%') .ne. 0 ) then
c---  This line is continued. Go on checking the next line.
          call getline ( line )
          goto 110
      endif

      WRITE(IW,'(A,I3,A,G15.8,6(A,I3)/3(A,8I2/),(a,3i1,1x,8i2) )')
     &' NLEVELS=',NLEVELS,' ELEVELS=',ELEVELS,
     &' IBUTLER=',IBUTLERx,' IUMIN=',IUMIN,' IUMAX=',IUMAX,
     &' IRNQ=',IRNQ,' IRXQ=',IRXQ,' NISUBC=',NISUBC,' IQUAD=',IQUAD,
     &' ISPIN=',ispin,' IORBIT=',iorbit,
     &(' ITENSOR=',(itensor(ispace,1,itens),ispace=1,3),
     &             (itensor(ishell,2,itens),ishell=1,ks),
     &              itens = 1, ntensors)

      do 107 iopr = 1, nopers
  107   write(iw,'(a,3(2I1,1x),14i1)'),
     &   ' IOPER  = ',(ioper(iplace,iopr),iplace=1,ioper(20,iopr))
     &   , ioper(20,iopr)
      
      call ciprint
      call limitprint


C        SETTING IBUTLER  PREPARES A FILE WITH INFORMATION AND
C        MATRIXELEMENTS FOR THE CRYSTALFIELD PROGRAM  BUTLER.
C        MATRIX ELEMENTS ARE OUTPUT OF  U(K), K=IUMIN,IUMAX,2.
c        FOR EACH SHELL SPECIFIED BY "SHELLxxxxx" crystal field
c        matrixes are calculated
C        Also the INRQ TO IRXQ-POLE TRANSITION MATRIXES ARE OUTPUT.
C

        GO TO 100

      else if (KCPL.gt.2) then
        READ (LINE,'(I5,I5,F10.5,I5,5F5.3,F10.5,5X,5I1,2I1,4I2)')
     &    KCPL,ID2,EIONRY,NPTKEV,(TKEV(I),I=2,6),DELEKEV,
     &    ILNCUV,IPLEV,ICPC,ICFC,IDIP, IENGYD,ISPECC, INS,
     &    IPCT,ICTC,ICTBCD
        do 90 I=1,5
   90     if (TKEV(I+1).gt.0.0) NTKEV=I
        TKEV(1)=9.9E10
        WRITE (IW,'(A,F10.5,3(A,I4),A,5F4.1/5(A,I1),A,I4,A,I2,A,I2,
     &              A,F8.2)')
     &    ' RCG MOD 9         EIONRY=',EIONRY,' IABG=',IABG,
     &    ' IV=',IV,' NPTKEV=',NPTKEV,' TKEV=',(TKEV(I),I=2,6),
     &    ' ILNCUV=',ILNCUV,' IPLEV=',IPLEV,' ICPC=',ICPC,
     &    ' ICFC=',ICFC,'IDIP=',IDIP,'IENGYD=',IENGYD,
     &    ' ISPEC=',ISPECC,' ICTBCD=',ICTBCD,' TIME0=',TIME0

        if (ID2.ne.0) call CUVFD

        GOTO 100
      else

C&--    GENUINE CONTROL CARD
        READ (LINE,10) KCPL,NCK,NOCSET,NSCONF,IABG,IV,NLSMAX,
     &  NLT11,NEVMAX, KCPLD, IELPUN,SJNK,SJXK,IMAG,IQUADX ,
     &  UENINP,DMIN, ILNCUV,IPLEV,ICPC,ICFC,IDIP, IENGYD,ISPECC,
     &  INS, IPCT,ICTC,ICTBCD
   10   FORMAT (I5,2I1,I3,I1,2I2,I1,2I2,2I1,I3,I2,I3,
     &  9I1,I1,4F2.0,2I1,F10.5,E5.1,  5I1,  2I1,4I2)
      endif
      if (ID2   .eq.0)    ID2    = ID2DEF
      ID3 = ID2+1
      ID4 = ID3+1
      if (UENRGY.eq.0)    UENRGY = UENINP
      if (NLT11 .eq.0)    NLT11  = 119
      if (NEVMAX.eq.0)    NEVMAX = 500
      if (NOCSET.ne.0.AND.NLSMAX.ne.0) NLSMAX = 500
      if (SJXK(1).LE.0  ) SJXK(1)= 99
      if (SJXK(2).LE.0  ) SJXK(2)= 99
C&--  INSERTED BY B.T. THOLE 14/4/1986.
C&--  FROM NOW  ON  2J+1  MUST  BE ENTERED FOR SJNK AND
C&--  SJXK
      SJNK(1) = (SJNK(1)-1)/2
      SJNK(2) = (SJNK(2)-1)/2
      SJXK(1) = (SJXK(1)-1)/2
      SJXK(2) = (SJXK(2)-1)/2
      if (IENGYD.eq.0) IENGYD = 500
      if (IENGYD.eq.2) IENGYD = NEVMAX
      IPLOTC = 0
      if (ISPECC.eq.0) IPLOTC = 1
      if (ISPECC.eq.0) ISPECC = 7
      INS=0
      IGEN=KCPLD(3)
      if (IGEN.GE.5) then
        IRNQ =           KCPLD(4)
        IRXQ = MAX (IRNQ,KCPLD(5))
        IRND = MAX (1   ,KCPLD(6))
        IRXD = MAX (IRND,KCPLD(7))
        DMIN = 0
        ISPECC=1
        NTKEV =0
        NPTKEV=0
        if (NCK(1).LE.0) NCK(1)=1
      endif
      if (NCK(1).eq.0) NCK(1)=KC
      if (NCK(2).eq.0) NCK(2)=KC
      COUPL(1)=COUPL1
      if (IABG.GE.3) COUPL(1)=COUPL(12)
C    &  NOTE---THE SL-COUPLING OPTION CONCERNS ONLY (SCRS4,SCRL4)SCRJ4.
C    &     THE JJ, JK, ETC. REPRESENTATIONS STILL USE (LI,SI)JI.
      WRITE (IW,15) COUPL(KCPL),NSCONF,IABG,IV,NLT11,KCPLD,SJXK,
     &  ILNCUV,IPLEV,ICPC,ICFC,IDIP,IENGYD,ISPECC,ICTBCD, TIME0
   15 FORMAT (10H1RCG MOD 9,A6,9H COUPLING,2X,7HNSCONF=,3I2,3X,
     &  3I2,5X,5HIABG=,I1,3X,3HIV=,I1,2X,I4,1X,9I1,2F5.1,2X,
     &  6HPRINT=,5I1, I4,2I2,4X,5HTIME=,F8.2)
      if (ICTBCD.eq.0) GO TO 180
      if (IRW19.ne.0) GO TO 180
      rewind 19
      IRW19=7
  180 if (NOCSET.eq.0) GO TO 200
      ICTBCD=0
      if (NOCSET.LT.0) GO TO 200
  182 READ (2) NOCDES,NSCRJ4,NPAR,J1X
      NPAR=NPAR-J1X+1
      do 183 N=1,NSCRJ4
        READ (2) L
        do 183 M=1,NPAR
  183     READ (2) ANAM
      if (NOCDES.ne.NOCSET) GO TO 182
      GO TO 100
C
C    &     CALC LEVEL DESIGNATIONS AND COEFFICIENT MATRICES
C
  200 call LNCUV
      if (IGEN.LT.5) then
        if ( IRXQ .eq.-10) then
          IRNQ=IIPRTY
          IRXQ=IIPRTY
        else
          IIPRTY = IRNQ
        endif
      endif
      call STAMP  ('FINISHED LNCUV')
C
      rewind IC

C&--  LOOP OVER  GROUND (K=1) AND  EXITED (K=2) STATE
      do 490 K = 1,2
      KK = K
      if (NSCONF(1,K) .eq. 0 ) goto 490
      if ( K .eq. 1 )  IL = ILA
      if ( K .eq. 2 )  IL = ILB
      do 410 I=1,KS
        LL (I) = LLIJK (I,1,K)
  410   FLL(I) = FLLIJK(I,1,K)
      nosubc=NSCONF(1,K)
      SJN=SJNK(K)
      SJX=SJXK(K)
      call PLEV
      call STAMP  ('FINISHED PLEV')
      if (ICTC.eq.0) then
        call PFGD (NISUBC)
        if (NSCONF(2,K).gt.1) then
          call PRK
        endif

        NOPC=25
        NPAR=-7
        J1=1
        WRITE (20) NPAR,KPAR,K,I,J,ANAM,CAVE,J1,NOPC,
     &             (PC(N1), N1=1,NOPC)
        rewind 20
        J1X=NSCONF(2,K)
        NOTSX=0
        do 440 I=1,nosubc
  440     NOTSX = MAX(NOTSJ1(I,J1X+1),NOTSX)
        rewind IL
        do 442 ILC = IL, IC, IC-IL
          WRITE (ILC) NOTSJ1,NSCRJ4,SJ4MN,SJ4MX,NPAR,NPARJ,
     &      NOTSX, ((FL(L,I),S(L,I), L=1,NOTSX), I=1,nosubc),NALSJP,PJ
  442   continue

        call CALCFC
C
        if (OPTION(imag,k))   call MUPOLE
     &      ('MAGNET', K,K, 0,0, 0,0, 1,1)
        if (OPTION(iquadx,k)) call MUPOLE 
     &      ('SHELL' , K,K, 0,0, 0,0, IUMIN,IUMAX )

C
        do 445 ishell = 1,nosubc
          if ( OPTION(IQUAD(ishell),k) )
     &      call MUPOLE
     &      ('SHELL' , K,K, 0,0, ishell,ishell, IUMIN,IUMAX )
  445   continue
c
        do 446 ishell = 1,KS
  446     if ( OPTION(ISPIN (ishell),k) ) call SPIN ( ishell , k )
c
        do 447 ishell = 1,KS
  447     if ( OPTION(Iorbit(ishell),k) ) call orbit( ishell , k )
c
        do 449 itens = 1, ntensors
          do 448 ishell = 1,nosubc
  448       if ( OPTION(itensor(ishell,2,itens),k) )
     &        call tensor ( ishell, itensor(1,1,itens), k, v1 )
  449   continue

        rewind IL
      endif
  490 continue
C

      do 510 iop = 1, nopers
      do 510 ir  = 7, ioper(20,iop)
      call MUPOLE 
     &      ('OPER', ioper(1,iop), ioper(2,iop)
     &      , ioper(3,iop), ioper(4 ,iop)
     &      , ioper(5,iop), ioper(6,iop), ioper(ir,iop), ioper(ir,iop)
     &      )
  510 continue

      if ( shifts ) then
        do 520 istate = 1,2
        do 520 iconf  = 1,nsconf(2,istate)
          call MUPOLE 
     &      ('SHIFT', istate,istate, iconf,iconf, 0,0, 0,0)
  520   continue
      endif
  
      call MUPOLE ('MUPOLE', 1,2, 0,0, 0,0, IRNQ,IRXQ )
      
      
      call ENERGY
      rewind ILA
      rewind ILB
      if (IBUTLER() .ne. 0) write(IBUTLER(),*) 'FINISHED'
      GO TO 96
C
      END

      subroutine myabort(text)
      character text*80
      print* ,' RCG9 stopped because it encountered a problem or error.'
     &       ,' See output.'	
      stop '--- RCG9 stopped because of problem or error ---'
      end

      subroutine cispecification
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      parameter (maxcomb=100)
      logical ciwasgiven
      common /cispec/ ciwasgiven
     & , iconf(2,2,maxcomb), ncomb(2)
      character *80 line, confc1*10, confc2*10
      
      entry ciinit
      do 10 istate = 1, 2
      do 10 icomb = 1, maxcomb
      do 10 jconf = 1, 2
        iconf(istate, jconf, icomb) = 0
   10 continue
      cigiven = 0
      ncomb(1) = 0
      ncomb(2) = 0
      ciwasgiven = .false.
      return
      
      entry ciread(line)
      ipos  = 1
   20 IPOSx = INDEX (LINE(ipos:),'CI' )
      if (IPOSx.eq.0) return
        print*,'ciread ',line
        ciwasgiven = .true.
        ipos = ipos - 1 + iposx + 2
        READ(LINE(IPOS:IPOS),'(I1)') istate
	ipos = ipos+1
   30     if(line(ipos:ipos) .ne. '-' ) goto 20
	  ipos = ipos + 1
	  nextspace=index(line(ipos+1:),' ')
	  nextminus=index(line(ipos+1:),'-')
	  nextunder=index(line(ipos+1:),'_')
	  next = 81-ipos 
	  if(nextspace.gt.0) next = min(next,nextspace)
	  if(nextminus.gt.0) next = min(next,nextminus)
	  if(nextunder.ge.1.and.nextunder.lt.next-1) then
	    confc1 = line(ipos:ipos + nextunder -1)				
	    confc2 = line(ipos + nextunder+1:ipos + next - 1)				
	  elseif (next.eq.2) then
            confc1 = line(ipos:ipos)
            confc2 = line(ipos + 1:ipos + 1)
	  else
   35       write(iw,*) 'Error in ci specification. Each pair has to be'
     &     ,' specified by two digits, or else by two integers'
     &     ,' separated by an underscore _. >>>', line(ipos:)
            call myabort('format error in CI specification')
          endif
	  ncomb(istate) = ncomb(istate) + 1
          call warndim ( ncomb(istate), maxcomb, 'maxcomb')
	  READ(confc1,'(bn,I10)',err=35) iconf(istate,1,ncomb(istate))
          READ(confc2,'(bn,I10)',err=35) iconf(istate,2,ncomb(istate))
          ipos = ipos+next
        goto 30

      entry ciprint
      write(9,*) ciwasgiven
      do 40 istate = 1, 2
        write(9,'(1x,a,i2,(t15,5(i2,a,i2,2x)))')
     &  ' ci state ',istate,
     &  (iconf(istate,1,icomb),'-',iconf(istate,2,icomb)
     &  , icomb=1, ncomb(istate) )
   40 continue
      return
      end
      
      function cigiven()
      parameter (maxcomb=100)
      logical ciwasgiven
      common /cispec/ ciwasgiven
     & , iconf(2,2,maxcomb), ncomb(2)
      if(ciwasgiven) then
        cigiven = 1
      else
        cigiven = 0
      endif
      return
      
      entry cispecified(istate,iconf1,iconf2)
      if(ciwasgiven) then
        do 50 icomb = 1, ncomb(istate)
	  if( 
     &	       (iconf1.eq.iconf(istate,1,icomb) 
     &	  .and. iconf2.eq.iconf(istate,2,icomb))
     &	  .or.
     &	       (iconf1.eq.iconf(istate,2,icomb) 
     &	  .and. iconf2.eq.iconf(istate,1,icomb))
     &    ) then
              specified = 1
	      goto 100
	    endif
   50   continue
        specified = 0
      else
        specified = 1
      endif
c      
  100 cispecified = specified
      print*,'istate,iconf1,iconf2 ',istate,iconf1,iconf2, specified
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
      subroutine limitspecification
      parameter (KC=10)
      logical limitwasgiven
      common /limitspec/ limitwasgiven , conflimitx(2,kc,2)
      character *80 line
      
      entry limitinit
      do 10 istate = 1, 2
      do 10 iconf  = 1, kc
      do 10 ilowup = 1, 2
        conflimitx(istate,iconf,ilowup) = (ilowup-1)*99
   10 continue

      limitwasgiven = .false.
      return
      
      entry limitread(line)
      ipos  = 1
   20 IPOSx = INDEX (LINE(ipos:),'LIMIT' )
      if (IPOSx.eq.0) return
        write(9,*) 'limitread ',line
        limitwasgiven = .true.
        ipos = ipos - 1 + iposx + 5
        READ(LINE(IPOS:),'(2I1,2f2.0)') 
     &    istate,iconf,
     &    conflimitx(istate,iconf,1),conflimitx(istate,iconf,2)
        ipos = ipos + 6
        call warndim ( iconf,kc, 'kc')
      goto 20

      entry limitprint
      write(9,*) 'limitwasgiven ',limitwasgiven
c      if(.not.limitwasgiven) return 
      do 40 istate = 1, 2
        write(9,'(1x,a,i2,(t20,4(a,i2,i3,a,i2,2x)))')
     &  ' limit state ',istate,
     &  (' conf ',iconf
     &           ,INT(conflimitx(istate,iconf,1))
     &       ,'-',INT(conflimitx(istate,iconf,2))
     &  , iconf=1, kc )
   40 continue
      return
      end
      
      function conflimit(istate,iconf,lowup)
      parameter (KC=10)
      common /limitspec/ limitwasgiven , conflimitx(2,kc,2)
      conflimit = (conflimitx(istate,iconf,lowup)-1 )/2
      end
      
C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE MUPOLE( identify,
     &     Istate1, Istate2
     &   , iconf1 , iconf2
     &   , ishell1, ishell2
     &   , IRMN   , IRMX )
C
C    &     COMPUTE EXCHANGE(=spin)
C                  MAGNETIC-DIPOLE 
C                  ELECTRIC-MULTIPOLE
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      common/inpfile/ iInp
      CHARACTER*8 IDLS,IDJJ,CHDUM1*1,CHDUM2*2
      COMMON /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  nosubc,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NSCRJ4,SCRJ4,SCRJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      common /c2/ SJ4K(2),NOTSJP(KS,100),NCFGP(KMX),NALSP(KMX,KS)

      COMMON /C3/ ISUBJ(KC),MULS(KS),
     &           NALS(KMX,KS),NCFG(KMX),SCRL(KMX,KS,2),SCRS(KMX,KS,2),
     &           CFP1(KLS1,KLS1)
      DIMENSION    U1(KLS1,KLS1)
      EQUIVALENCE (CFP1,U1)

      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)

      COMMON/C3LC4/DUMLC4(KMX*KMX)
      DIMENSION    DUMLC4A(2*KISER+1), TMXP(KMX,KMX),NPARJ(KC,KC)
      EQUIVALENCE (DUMLC4,DUMLC4A,TMXP),(DUMLC4A(2*KISER+1),NPARJ) 

      COMMON    C(KMX,KMX),
     &          FL(KMX,KS,2),S(KMX,KS,2),CFP2(17,17)
      CHARACTER line*80 ,MATNAME*11, STATE*11, identify*(*)
      dimension pmup(KC,KC)
C
      WRITE(IW,*) ' MUPOLE called (',identify
     &, Istate1,Istate2, iconf1,iconf2, ishell1,ishell2, IRMN,IRMX,')'
      print*,     ' MUPOLE called (',identify
     &, Istate1,Istate2, iconf1,iconf2, ishell1,ishell2, IRMN,IRMX,')'
      IL1 = ILA + Istate1 - 1
      IL2 = ILA + Istate2 - 1
      I1x = Istate1
      I2x = Istate2
c---  no matrix elements when there are no states
      if (nsconf(2,Istate1).eq.0 .or. nsconf(2,Istate2) .eq. 0) return
C---  if card 'MUPOLE' found then read factors for each dipole transitio
      do 50 n1=1,kc
      do 50 n2=1,kc
          pmup(n1,n2)=1.0
  50  continue
      nc =nsconf(2,1)
      ncp=nsconf(2,2)
      call getline(line)
      if (index(line,'MUPOLE').gt.0 .and. Istate1.ne.Istate2) then
        do 51 i=1,(nc*ncp-1)/7+1
          call getline(line)
   51   continue
        do 52 i=1,(nc*ncp-1)/7+1
          call backSpInp()
   52   continue
        read(iInp,'(7(f9.5,i1))') 
     &      ((pmup(n1,n2),idum,n2=1,ncp), n1=1,nc)
      else
        call backSpInp()
      endif
C---  LOOP OVER MULTIPOLE
      do 990 R=IRMN,IRMX,2
      SUMS20=0.0
      SUMS21=0.0

      rewind IL1
      READ (IL1) NOTSJ1,NSCRJ4,SJ4MN ,SJ4MX ,NPAR,NPARJ,
     &  NOTSX, ((FL(L,I,1),S(L,I,1), L=1,NOTSX), I=1,nosubc)
      SJ4K(Istate1)=-1

      rewind IL2
      READ (IL2) NOTSJP,NSCRJP,SJ4PMN,SJ4PMX,NPAR,NPARJ,
     &  NOTSX, ((FL(L,I,2),S(L,I,2), L=1,NOTSX), I=1,nosubc)
      SJ4K(Istate2)=-1

      J1A=NSCONF(2,Istate1)
      J1B=NSCONF(2,Istate2)
      do 105 I=1,J1A
      do 105 J=1,J1B
  105   SOPI2(I,J)=0.0
C
      do 925 SJD=SJ4MN,SJ4MX
       call getbas( SJD,  1, nls,    TMX,  naLS,  scrL,     scrS,
     &    ncfg,  idLS, idJJ, nosubc, kmx,  ks,    SJ4K(Istate1), IL1)

      do 925 SJPD= MAX(SJ4PMN,ABS(SJD-R)), MIN(SJ4PMX,SJD+R)
       call getbas( SJPD, 2, nlsp,   TMXp, naLSp, scrL,     scrS,
     &    ncfgp, idLS, idJJ, nosubc, kmx,  ks,    SJ4K(Istate2), IL2)
C
C
      NCO=0
      NCPO=0

      do 879 L=1,NLS
      NC=NCFG(L)

      do 878 LP=1,NLSP
      NCP=NCFGP(LP)
      C(L,LP)=0.0

C
C       CALC configuration shift ELEMENT
C
      if (identify.eq.'SHIFT') then
        if (l.ne.lp .or. NC.ne.iconf1 .or. ncp.ne.iconf1) GO TO 878
	tc = sqrt(2*sjd+1)
	goto 700
      endif
        
C
C       CALC MAGNETIC DIPOLE ELEMENT
C
      if (identify.eq.'MAGNET' .or. identify.eq.'EXCHANGE') then
C       delta for configuration and all alpha S and L
        if (NC.ne.NCP) GO TO 878
        do 210 I=1,nosubc
          if (NALS(L,I)  .ne. NALSP(LP,I) ) GO TO 878
          if (SCRL(L,I,1).ne. SCRL(LP,I,2)) GO TO 878
  210     if (SCRS(L,I,1).ne. SCRS(LP,I,2)) GO TO 878

        A1=SCRL(L,nosubc,1)
        A2=SCRS(L,nosubc,1)

        if ( identify.eq.'EXCHANGE' ) then
C--       The next two lines calculate the matrix element of gS
          TC=2.00232*UNCPLB(A1,A2,SJD,1.0,A2,SJPD)
     &    *SQRT(A2*(A2+1.0)*(2.0*A2+1.0))

        else if (identify.eq.'MAGNET') then
C--       The next three lines calculate the matrix element of L+gS
          TC=1.00232*UNCPLB(A1,A2,SJD,1.0,A2,SJPD)
     &    *SQRT(A2*(A2+1.0)*(2.0*A2+1.0))
          if (SJD.eq.SJPD) TC=TC+SQRT(SJD*(SJD+1.0)*(2.0*SJD+1.0))
        endif
        GO TO 700
      endif
C
C    &     CALC ELECTRIC MULTIPOLE ELEMENT
C
  300 if (NC.eq.NCO.AND.NCP.eq.NCPO) GO TO 400
C
C    &     DETERMINE WHETHER A TRANSITION IS POSSIBLE (ITRANS.ne.0)
C
      ITRANT=ITRANS
      IT=I
      JT=J
      ITRANS=0
      if (iconf1.ne.0 .and. iconf2.ne.0 
     &   .and. .not. 
     &      ((nc .eq.iconf1 .and. ncp.eq.iconf2)
     &        .or. 
     &       (nc .eq.iconf2 .and. ncp.eq.iconf1))
     &   ) goto 345
     
      do 340 IRHO = 1, nosubc
      do 340 IRHOP= 1, nosubc
      
c--   anihilation in shell irho and creation in shell irhop
      if (ishell1.ne.0 .and. ishell2.ne.0
     &   .and. .not. 
     &      ((irhop.eq.ishell1 .and. irho.eq.ishell2)
     &        .or. 
     &       (irhop.eq.ishell2 .and. irho.eq.ishell1))
     &   ) goto 340
     
      do 302 I=1,nosubc
      if (I.eq.IRHO.or.I.eq.IRHOP) GO TO 302
      if (NIJK(I,NC,Istate1)+NIJK(I,NCP,Istate2) .eq. 0) GO TO 302
      if (FLLIJK(I,NC,Istate1).ne.FLLIJK(I,NCP,Istate2)) GO TO 340
      if (NIJK  (I,NC,Istate1).ne.NIJK  (I,NCP,Istate2)) GO TO 340
  302 continue

      if (IRHO.eq.IRHOP) then

        if (Istate1.eq.Istate2.and.NC.eq.NCP) then
C       --DIAGONAL CASE. No matrix element for empty or full shell.
          NFULL = 4*FLLIJK(irho,NC,Istate1) + 2
          NN    =   NIJK  (irho,NC,Istate1)
          if   (  NN .eq. 0 .or. NN .eq. NFULL )  GOTO 340
        else
c	--non-diagonal case irho=irhop only allowed for last shell
c         with one electron; only l (EL) may differ.
          NN=0
          do 310 I=IRHO,nosubc
  310       NN=NN+NIJK(I,NC,Istate1)+NIJK(I,NCP,Istate2)
          if (NN.gt.2) GO TO 340
	endif
      else
c       check difference in occupations
          if( NIJK(irho ,NC,Istate1) - 1 .ne. NIJK(irho ,NCP,Istate2)
     &	  .or.NIJK(irhop,NC,Istate1) + 1 .ne. NIJK(irhop,NCP,Istate2) 
     &	    ) goto 340
      endif

c     check triad and parity  (L1 R L2)
      a1=FLLIJK(IRHO ,NC ,Istate1)
      a2=FLLIJK(IRHOP,NCP,Istate2)
      if (MOD(a1+R+a2,2.0).ne.0.0
     &  .or. a1+a2.lt.R
     &	.or. ABS(a1-a2).gt.R) 
     & GO TO 340
     

c     Configurations ok for one electron matrix element: start calculation.
      GO TO 350
  340 continue
  
  
c     No matrix element for these configurations. 
c     Take old values for itrans, i and j
  345 ITRANS=ITRANT
      I=IT
      J=JT
      GO TO 878
C
C    &     CALCULATE THEORETICAL S/P2
C
  350 NCO=NC
      NCPO=NCP
      if (SOPI2(NC,NCP).ne.0.0) GO TO 355
      ERAS=2.0
      do 354 I=1,nosubc
      B =NIJK(I,NC, Istate1)
      B1=NIJK(I,NCP,Istate2)
      if (B.GE.B1) GO TO 351
      A = 4*FLLIJK(I,NCP,Istate2)+1
      GO TO 354
  351 A = 4*FLLIJK(I,NC ,Istate1)+2
      if (I.ne.IRHO) GO TO 354
      if (Istate1.ne.Istate2.or.NC.ne.NCP) GO TO 353
      ERAS=ERAS*B*(A-B)/(A*(A-1.0))
      GO TO 354
  353 A=A-1.0
      B=B-1.0
  354 ERAS=ERAS*FCTRL(A)/(FCTRL(B)*FCTRL(A-B))
      SOPI2(NC,NCP)=ERAS
      if (Istate1.eq.Istate2) SOPI2(NCP,NC)=ERAS
C
C    &     DETERMINE WHETHER CONFIGS ARE TO BE INTERCHANGED (ITRANS=-1) 
C
  355 I=MIN(IRHO,IRHOP)
      J=MAX(IRHO,IRHOP)
      M=NIJK(IRHO ,NC ,Istate1)
      N=NIJK(IRHOP,NCP,Istate2)
      ITRANS=1
      TC0=1.0
      if (Istate1.eq.Istate2.AND.NC.eq.NCP) GO TO 370
      A=M*N
      TC0=SQRT(A)
      if (IRHO.gt.IRHOP) N=M
      M=0
      do 360 K=I,J
      if (K.ne.I.AND.K.ne.J) M=M+NIJK(K,NC,Istate1)
  360 continue
      if (MOD(M+N,2).eq.0) TC0=-TC0
      if (I.ne.IRHO) ITRANS=-1
      if (ITRANS.gt.0) GO TO 370
      if (MOD(ABS(SJPD-SJD+R),2.0).ne.0.0) TC0=-TC0
C
C    &     READ CFP FROM DISK 72, OR U2 FROM DISK 73
C
  370 N  =NIJK  (IRHO,NC,Istate1)
      ALL=FLLIJK(IRHO,NC,Istate1)
      if (I.eq.J) GO TO 390
      III=1
  375 if (N.LE.2) GO TO 385
      call LOCDSK(ID2,ALL,N)
      READ (ID2) (A,CHDUM1,CHDUM2,A,A, M=1,NLT), NLP
      if (III.eq.1.AND.ITRANS.eq.(-1)) GO TO 380
      if (III.eq.2.AND.ITRANS.eq.1) GO TO 380
c     READ (ID2) ((CFP1(K,M), K=1,NLT), M=1,NLP)
      call getsparse ((id2), cfp1, nlt, nlp, kls1,.false.)
      GO TO 385
c 380 READ (ID2) ((CFP2(K,M), K=1,NLT), M=1,NLP)
  380 call getsparse ((id2), cfp2, nlt, nlp, 17,.false.)
  385 if (III.eq.2) GO TO 400
      N  =NIJK  (IRHOP,NCP,Istate2)
      ALL=FLLIJK(IRHOP,NCP,Istate2)
      III=2
      GO TO 375
C
  390 if (N.LT.2) GO TO 400
      call LOCDSK(ID3,ALL,N)
      do 395 R1=0,R-1
  395 READ (ID3)
c     READ (ID3) K73, ((U1(K,M), K=1,NLT), M=1,NLT)
      call  getsparse ((id3), u1, nlt, nlt, kls1,.false.)
C
C
C
  400 if (ITRANS) 410,878,405
  405 L1=L
      L2=LP
      NC1=NC
      NC2=NCP
      J1=Istate1
      J2=Istate2
      K1=1
      K2=2
      SJ1=SJD
      SJ2=SJPD
      GO TO 420
  410 L1=LP
      L2=L
      NC1=NCP
      NC2=NC
      J1=Istate2
      J2=Istate1
      K1=2
      K2=1
      SJ1=SJPD
      SJ2=SJD
C    &                    TEST SELECTION RULES
  420 do 430 M=1,nosubc
      A=ABS(SCRL(L,M,1)-SCRL(LP,M,2))
      B=SCRL(L,M,1)+SCRL(LP,M,2)
      if (M.GE.J) GO TO 426
      if (M.LT.I) GO TO 424
      if (A.gt.FLLIJK(I,NC1,J1)) GO TO 878
      if (B.LT.FLLIJK(I,NC1,J1)) GO TO 878
      if (M.eq.I) GO TO 430
      GO TO 428
  424 if (A.ne.0.0) GO TO 878
      GO TO 427
  426 if (A.gt.R) GO TO 878
      if (B.LT.R) GO TO 878
  427 if (SCRS(L,M,1).ne.SCRS(LP,M,2)) GO TO 878
      if (M.eq.J) GO TO 430
  428 if (NALS(L,M)-NOTSJ1(M,NC).ne.NALSP(LP,M)-NOTSJP(M,NCP)) GO TO 878
  430 continue
C
      TC=TC0
      A1=SCRL(L1,nosubc,K1)
      A2=SCRL(L2,nosubc,K2)
      A3=SCRS(L1,nosubc,K1)
      TC=TC*UNCPLA(A1,A3,SJ1, R,A2,SJ2)
C
      MN=J+1
      if (MN.gt.nosubc) GO TO 460
      do 455 M=MN,nosubc
      A1=SCRL(L1,M-1,K1)
      A2=SCRL(L1,M,K1)
      A3=SCRL(L2,M,K2)
      N1=NALS(L,M)
      A4=FL(N1,M,1)
  455 TC=TC*UNCPLA(A1,A4,A2, R,SCRL(L2,M-1,K2),A3)
C
  460 if (Istate1.eq.Istate2.AND.NC.eq.NCP) GO TO 600
C
C    &     COMPLETE CALCULATION FOR NON-CONF-DIAG CASE
C
      if (NIJK(I,NC1,J1).LE.2) GO TO 510
      N1=NALS(L,I)-NOTSJ1(I,NC)
      N2=NALSP(LP,I)-NOTSJP(I,NCP)
      A=CFP1(N1,N2)
      if (ITRANS.LT.0) A=CFP1(N2,N1)
      TC=TC*A
  510 if (NIJK(J,NC2,J2).LE.2) GO TO 520
      N1=NALS(L,J)-NOTSJ1(J,NC)
      N2=NALSP(LP,J)-NOTSJP(J,NCP)
      A=CFP2(N1,N2)
      if (ITRANS.gt.0) A=CFP2(N2,N1)
      TC=TC*A
C
  520 FLI=FLLIJK(I,NC1,J1)
      if (I.eq.1) GO TO 530
      if (NIJK(I,NC1,J1).eq.1) GO TO 530
      A1=SCRL(L1,I-1,K1)
      A2=SCRL(L1,I,K1)
      A3=SCRL(L2,I,K2)
      N1=NALS(L,I)
      N2=NALSP(LP,I)
      if (ITRANS.gt.0) GO TO 522
      N1=N2
      N2=NALS(L,I)
  522 A4=FL(N1,I,J1)
      A5=FL(N2,I,J2)
      B1=SCRS(L1,I-1,K1)
      B2=SCRS(L1,I,K1)
      B3=SCRS(L2,I,K2)
      B4=S(N1,I,J1)
      B5=S(N2,I,J2)
      TC=TC*RECPSH(A1,A5,A3,FLI,A2,A4)*RECPSH(B1,B5,B3,0.5,B2,B4)
C
  530 MN=I+1
      MX=J-1
      if (MN.gt.MX) GO TO 540
      do 539 M=MN,MX
      A1=SCRL(L1,M-1,K1)
      A2=SCRL(L2,M-1,K2)
      A3=SCRL(L1,M,K1)
      A4=SCRL(L2,M,K2)
      N1=NALS(L,M)
      A5=FL(N1,M,1)
      B1=SCRS(L1,M-1,K1)
      B2=SCRS(L2,M-1,K2)
      B3=SCRS(L1,M,K1)
      B4=SCRS(L2,M,K2)
      B5=S(N1,M,1)
  539 TC=TC*RECPEX(A2,FLI,A1,A5,A3,A4)*RECPEX(B2,0.5,B1,B5,B3,B4)
C
  540 FLJ=FLLIJK(J,NC2,J2)
      N1=NALS(L,J)
      N2=NALSP(LP,J)
      if (ITRANS.gt.0) GO TO 542
      N1=N2
      N2=NALS(L,J)
  542 A1=SCRL(L1,J-1,K1)
      A2=SCRL(L2,J-1,K2)
      A3=SCRL(L1,J,K1)
      A4=SCRL(L2,J,K2)
      if (I.eq.J) GO TO 560
      A5=FL(N1,J,J1)
      A6=FL(N2,J,J2)
      B5=S(N1,J,J1)
      if (B5.eq.0.0) GO TO 550
      B1=SCRS(L1,J-1,K1)
      B2=SCRS(L2,J-1,K2)
      B3=SCRS(L1,J,K1)
      B6=S(N2,J,J2)
      TC=TC*RECPJP(B2,0.5,B1,B5,B3,B6)
  550 if (A5.eq.0.0) GO TO 560
      if (MOD(FLJ+A5-A6,2.0).ne.0.0) TC=-TC
      TC=TC*SQRT((2.0*A1+1.0)*(2.0*A6+1.0)*(2.0*A3+1.0)*(2.0*A4+1.0))
      TC=TC*S9J(A1,A2,FLI, A5,A6,FLJ, A3,A4,R)
      GO TO 700
  560 if (J.eq.1) GO TO 700
      if (A2.eq.0.0) GO TO 700
      TC=TC*UNCPLB(A2,FLI,A3, R,FLJ,A4)
      GO TO 700
C
C    &     COMPLETE CALCULATION FOR CONF-DIAG CASE
C
  600 if (I.eq.1) GO TO 620
      N1=NALS(L,I)
      N2=NALSP(LP,I)
      A1=SCRL(L1,I-1,K1)
      A3=SCRL(L1,I,K1)
      A4=SCRL(L2,I,K2)
      A5=FL(N1,I,1)
      A6=FL(N2,I,2)
      TC=TC*UNCPLB(A1,A5,A3, R,A6,A4)
  620 if (NIJK(I,NC1,J1).eq.1) GO TO 700
      N1=NALS(L,I)-NOTSJ1(I,NC)
      N2=NALSP(LP,I)-NOTSJP(I,NCP)
      TC=TC*U1(N1,N2)
C
  700 A1=TC*TC
      SUMS21=SUMS21+A1
      if (Istate1.ne.Istate2) GO TO 720
      if (SJD.ne.SJPD) GO TO 710
      if (L.LE.LP) GO TO 725
      GO TO 720
  710 SUMS21=SUMS21+A1
  720 SUMS20=SUMS20+A1
  725 if (IABG.LT.3) GO TO 730
      M=nosubc
      IPP=SCRL(L,M,1)+SCRS(L,M,1)-SJD+SCRL(LP,M,2)+SCRS(LP,M,2)-SJPD
      if (MOD(IPP,2).ne.0) TC=-TC
  730 C(L,LP)=TC * pmup(nc,ncp)
C
  878 continue
  879 continue
C
      KPAR=2
      SCRJ4=SJD
      SCRJ4P=SJPD
C---    OUTPUT OF TRANSITION REDUCED MATRIX FOR BUTLER CRYSTAL FIELD
      
      MATNAME = ' '
c	Old, configuration diagonal operators. Keep old names.
               if (identify.eq.'SHIFT'   ) then
            WRITE(MATNAME,'(A,I1)') 'SHIFT', Iconf1
          else if (identify.eq.'EXCHANGE') then
            MATNAME = 'EXCHANGE'
          else if (identify.eq.'MAGNET'  ) then
            MATNAME = 'MAGNET'
          else if (identify.eq.'SHELL'   ) then
            WRITE(MATNAME,'(A,I1)') 'SHELL', ISHELL1
          else if (identify.eq.'OPER'    ) then
c           General operators. New (august 1995 B.T.Thole)
            write(matname,'(a,6i1)')
     &      'OPER',Istate1,Istate2,iconf1,iconf2,ishell1,ishell2
          else if (identify.eq.'MUPOLE'  ) then
            MATNAME = 'MULTIPOLE'
          endif
      
      call BUTLER(MATNAME,Istate1,Istate2,C,SCRJ4, IPRITY(Istate1),R,
     &      IPRITY(Istate1)*IPRITY(Istate2),SCRJ4P,IPRITY(Istate2),
     &      NLS,NLSP,KMX)
C
C    &     OUTPUT (TRANSFORM TO JJ COUPLING if NECESSARY)
C
      call SPRIN
C
  925 continue
C
C    &     WRITE DUMMY RECORD ending dipole matrixes
C
      SCRJ4=-7.0
      NLS=1
      if (IBUTLER().eq.0) then
        WRITE (IC) SCRJ4,SCRJ4,NLS,NLS, 0.0, 0.0, NCFG,NCFGP
      endif
      rewind IL1
      rewind IL2

      SSOPI2=0.0
      do 958 NC =1,NSCONF(2,Istate1)
      do 958 NCP=1,NSCONF(2,Istate2)
  958   SSOPI2 = SSOPI2 + SOPI2(NC,NCP)
  
      WRITE (IW,'(/2(a,F12.5)/8X,a,F12.5//)')
     & ' SUMS21=',SUMS21,' SUMS20=',SUMS20,' SSOPI2=',SSOPI2
      do 960 I=1,NSCONF(2,Istate1)
  960   WRITE (IW,'(45X,9F8.3)') (SOPI2(I,J), J=1,NSCONF(2,Istate2))

      if (R.eq.0) SOPI2(1,1)=SUMS21
      if (IBUTLER().eq.0) then
        WRITE (IC) SOPI2
      endif
  990 continue
      call CLOK (TIME)
 
           if (identify .eq.'SHIFT'    ) then
         PRINT'(A,F8.2,A,A,A,I2)',        ' TIME=',TIME-TIME0
     &,'  SHIFT  ',STATE(Istate1,Istate2),'  Conf ' , iconf1

      else if (identify .eq.'EXCHANGE') then
         PRINT'(A,F8.2,A,A)',        ' TIME=',TIME-TIME0
     &,'  EXCHANGE (= spin)  ',STATE(Istate1,Istate2)

      else if (identify .eq.'MAGNET' ) then
         PRINT'(A,F8.2,A,A)',        ' TIME=',TIME-TIME0
     &,'  MAGNETIC DIPOLE   ',STATE(Istate1,Istate2)

      else if (identify.eq.'SHELL'   ) then
         PRINT'(A,F8.2,A,A,3(A,I1))',' TIME=',TIME-TIME0
     &,'  SHELL  ', STATE(Istate1,Istate2)
     &,'  POLE ' ,IRMN,    ' to ', IRMX
     &,'  Shell ',ISHELL

      else if (identify.eq.'MUPOLE'  ) then
         PRINT'(A,F8.2,A,A,3(A,I1))',' TIME=',TIME-TIME0
     &,'  MULTIPOLE  ', STATE(Istate1,Istate2)
     &,'  POLE ',IRMN,     ' to ', IRMX

      else if (identify .eq.'OPER'   ) then
         PRINT'(A,F8.2,A,A,6(A,I1))',' TIME=',TIME-TIME0
     &,'  OPER  ', STATE(Istate1,Istate2)
     &,'  Conf ' , iconf1, ' to ', iconf2
     &,'  Shell ', ishell1,' to ', ishell2
     &,'  Pole ' , IRMN,   ' to ', IRMX
      endif
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
      BLOCKDATA heapDATS
      parameter (lheap=500000)
      common    /heap   / last, max_used, x(lheap)
      DATA  last /0/ max_used/0/
      end

C--------------------------------------------------------------------
      subroutine UPPERCASE(TEXT)
c --- implicit real*8 (a-h,o-z)
C
      CHARACTER TEXT*(*) ,  CUPPER*26,CLOWER*26
      DATA CUPPER/'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      DATA CLOWER/'abcdefghijklmnopqrstuvwxyz'/
C
      DO 10 I=1,LEN(TEXT)
        LOWER=INDEX(CLOWER,TEXT(I:I))
        IF (LOWER.GE.1) THEN
          TEXT(I:I)=CUPPER(LOWER:LOWER)
        ENDIF
   10 continue
c--   And now delete underscores please
c      j=0
c      do 20 i=1,len(text)
c        if(text(i:i).ne. '_') then
c          j=j+1
c          text(j:j) = text(i:i)
c        endif
c   20 continue
c     text(j+1:) = ' '
      END
 
C----------------------------------------------------------------------
      SUBROUTINE GETLINE(LINE)
      CHARACTER*80 LINE
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      common/inpfile/ iInp
      READ (iInp,'(A80)', END=100) LINE
      call uppercase(line)
      PRINT*,LINE
      WRITE (IW,*) 'ECHO:',LINE
      if (IBUTLER().NE.0) then
         if (irrepdots().eq.0) WRITE(IBUTLER(),'(A,A)') ' %',LINE
      endif
      RETURN
  100 WRITE (IW,*) 'END OF FILE ON "INPUT" '
      print *, 'END OF FILE ON "INPUT" '
      STOP 'END OF FILE ON "INPUT"'
      
      entry BackSpInp()
       backspace iInp
      return
      
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      subroutine izero ( intarray, n )
      dimension  intarray (n)
      do 1 i = 1 , n
    1    intarray(i) = 0
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      subroutine zero ( array, n )
      dimension  array (n)
      do 1 i = 1 , n
    1    array(i) = 0
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
      subroutine zero2dim ( array, n,m,kmx )
      dimension  array (kmx,m)
      do 1 j = 1 , m
        do 1 i = 1 , n
    1     array(i,j) = 0
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      subroutine copy ( arrayfrom, arrayto, n )
      dimension  arrayfrom (n), arrayto(n)
      do 1 i = 1 , n
    1    arrayto(i) = arrayfrom(i)
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      subroutine copy2dim ( arrayfrom, arrayto, n,m,kmx )
c     separate routine to copy only part of array. Saves time in
c     a small calculation when kmx is large.
      dimension  arrayfrom (kmx,m), arrayto(kmx,m)
      do 1 j = 1 , m
        do 1 i = 1 , n
    1     arrayto(i,j) = arrayfrom(i,j)
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#


      subroutinE STAMP ( TEXT )
      character TEXT*(*)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      SAVE   TIME0
      DATA   TIME0 /-1./
      if ( TIME0 .EQ. -1. ) then
        call CLOK (TIME0)
      else
        call CLOK (TIME )
        write(iw,'(/1x,a,a,f6.3,a)') text,' at ',time-time0,' min'
      endif
      end


C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLOK(TIME)
C     IMPLICIT INTEGER*4 (I-N)
C --- NEC
C     DOUBLE PRECISION DTIME
C     CALL CLOCK(DTIME)
C     TIME=REAL(DTIME)
C --- CDC
C     TIME = SECOND() / 60
C --- CONVEX
C      REAL TARRAY(2)
C      TIME = ETIME(TARRAY) / 1000000.0
C     TIME = TARRAY(1) / 1000000.0

C---VAX
C
CC      DATA IFIRST/1/
CC      IF(IFIRST.EQ.1) THEN
CC      IFIRST=0
CC           CALL LIB$INIT_TIMER
CC      endif
c      TIME = SECNDS(0.) / 60
CC           CALL LIB$SHOW_TIMER
C     HPUX
      TIME = SECNDS(0.0)
C SunOS
c      DIMENSION TARR(2)
c      TIME = ETIME(TARR)
      END
C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#


      FUNCTION CIJKF(A,B,C)
C
C     IMPLICIT INTEGER*4 (I-N)
      D=S3J0SQ(A,B,C)
      D=SQRT((2.0*A+1.0)*(2.0*B+1.0)*D)
      if (MOD(0.5*(B+C-A),2.0).NE.0.0) D=-D
      CIJKF=D
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE LOCDSK(IDSK,FLLDES,NIDES)
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
C
      CHARACTER*1 LLD

      REWIND IDSK
  100 READ (IDSK,END=500) LLD,NID,FLLD,NOT,NLT,NORCD
c      print'(i4,a,i4,f5.1,3i6,f5.1,i4)',
c     !       idsk,LLD,NID,FLLD,NOT,NLT,NORCD,flldes,nides
      if (FLLD.EQ.FLLDES.AND.NID.EQ.NIDES) RETURN
      do 200 NN=1,NORCD
  200 READ (IDSK,END=500)
      GO TO 100
  500 WRITE(IW,*) 'FLLDES= ',FLLDES,' NIDES= ',NIDES,
     &    ' NOT FOUND ON FILE ',IDSK
     &   ,'   MAYBE YOU DID NOT USE CAPITALS FOR SPDF.. '
      STOP 'TROUBLE: SEE OUTPUT'
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

        subroutine sparse(a, nrows, ncols, mrows,  lsparse, ixsparse,
     &                    sym)
        parameter (lheap=500000)
        common    /heap   / last, max_used, x(lheap)
        dimension a(mrows, ncols)
        logical sym
c--- compresses matrix a to ixsparse (on the heap)
c        print*,'sparse ',
c     &  nrows, ncols, mrows,((a(i,j),j=1,ncols),i=1,nrows)
        rows = 0
        nonzeros = 0
        do 100 irow = 1, nrows
          cols = 0
          if(sym) then
            icol1 = irow
          else
            icol1 = 1
          endif
          do 90 icol = icol1, ncols
   90       if (abs(a(irow, icol)).gt.1e-6) cols = cols + 1
          if ( cols.ne.0 ) then
            rows = rows + 1
            nonzeros = nonzeros + cols
          endif
  100   continue
        lsparse = 2 + 2*rows + 2*nonzeros
c        print*,'sparse ',ncols*nrows, lsparse
        call alloc ('sparse', lsparse, ixsparse)
        call sparsex (a, nrows, ncols, mrows, X(ixsparse), lsparse, sym)
        end

C----------------------------------------------------------------------
        subroutine sparsex (a, nrows, ncols, mrows, X, lsparse, sym)
        dimension a(mrows, ncols), X(lsparse)
        logical sym
        X(1) = lsparse
        ix = 2
        rows = 0
        ix = ix + 1
        do 100 irow = 1, nrows
          cols = 0
          icols = ix
          ix = ix + 2
          if(sym) then
            icol1 = irow
          else
            icol1 = 1
          endif
          do 90 icol = icol1, ncols
            if (abs(a(irow, icol)).gt.1e-6) then
              X(ix) = icol
              X(ix+1) = a(irow, icol)
              ix = ix + 2
              cols = cols + 1
            endif
   90     continue
          if ( cols.ne.0 ) then
            X(icols) = irow
            X(icols+1) = cols
            rows = rows + 1
          else
            ix = icols
          endif
  100   continue
        X(2) = rows
        end
        
C----------------------------------------------------------------------
        subroutine putsparse (ifile, a, nrows, ncols, mrows, sym)
        parameter (lheap=500000)
        common    /heap   / last, max_used, x(lheap)
        dimension a(mrows, ncols)
        logical sym
c---    writes matrix a in sparse form to file ifile.
c---    a is not changed.
        call sparse ( a, nrows, ncols, mrows, lsparse, ixsparse, sym)
        call putblock (ifile, X(ixsparse),  lsparse)
        call dealloc ('putsparse', lsparse, ixsparse)
        end

C----------------------------------------------------------------------
        subroutine putblock ( ifile,  a,  n )
        dimension a(n)
        write (ifile) a
        end

C----------------------------------------------------------------------
        subroutine getblock ( ifile,  a,  n )
        dimension a(n)
        read (ifile) a
        end

C----------------------------------------------------------------------
        subroutine getsparse (ifile, a, nrows, ncols, mrows, sym)
        parameter (lheap=500000)
        common    /heap   / last, max_used, x(lheap)
        dimension a(mrows, ncols)
        logical sym
        call zero2dim ( a, nrows, ncols, mrows)
c---    reads matrix a from file ifile where it was in compressed form.
        read (ifile) xlsparse
        lsparse = xlsparse
        backspace(ifile)
        call alloc    ('getsparse', lsparse, ixsparse)
        call getblock (ifile, X(ixsparse),  lsparse)
        call spexpand (a, nrows, ncols, mrows, X(ixsparse),lsparse,sym)
        call dealloc  ('getsparse', lsparse, ixsparse)
c        print*,'getsparse ',
c     &  nrows, ncols, mrows,((a(i,j),j=1,ncols),i=1,nrows)
        end
        
C----------------------------------------------------------------------
        subroutine spexpand (a, nrows, ncols, mrows, X, lsparse, sym)
        dimension a(mrows, ncols), X(lsparse)
        logical sym
c       xlsparse, rows, (ri,cols, (rj,a(ri,rj), icols=1,cols), irows=1,rows)
        call zero2dim ( a, nrows, ncols, mrows)
        rows = X(2)
        ix = 3
        do 20 irows = 1, rows
          ri = X(ix)
          ix = ix + 1
          cols = X(ix)
          ix = ix + 1
          do 10 icols = 1, cols
            rj = X(ix)
            ix = ix + 1
            a(ri,rj) = X(ix)
            ix = ix + 1
   10     continue
   20   continue
        if (sym) call symmetrise(a, nrows, mrows)
        end

C----------------------------------------------------------------------
        subroutine symmetrise(a, n, mrows)
        dimension a(mrows, n)
        do 20 irow = 1, n
          do 10 icol = 1,irow-1
            a(irow,icol) = a(icol,irow)
   10     continue
   20   continue
        end

C----------------------------------------------------------------------
      subroutine alloc ( text,  n,  istart)
      parameter (lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      character*(*) text
      istart = last + 1
      last   = last + n
      max_used = max (max_used,  last)
c      print'(2(a11, i10), 3x, a)',  ' alloc   n=', n, ' last=', last, text
      call warndim ( last,  lheap,  'lheap' )

c--- Initialize to a large number so that if the user does not initialize
c--- correctly himself,  he will get bad results.
      do 1 i = istart, last
    1   x(i) = 1e30
      end
 
C----------------------------------------------------------------------
      subroutine dealloc ( text,  n,  istart)
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      character*(*) text
      if (last .ne. istart + n - 1 ) then
        print*, ' Dealloc was called for space that was not at the', 
     &         ' top of the heap. This is not allowed.', 
     &         ' text=', text, ' n=', n, ' istart=', istart, ' last=', last
        call myabort('Dealloc was called for space not at top of heap')
      endif

c--- Set deallocated space to a large value such that use of this space
c--- will give bad results.
      do 1 i = istart, last
    1   x(i) = 1e30
      last = last - n
c      print'(2(a11, i10), 3x, a)',  ' dealloc n=', n, ' last=', last, text
      end

C----------------------------------------------------------------------
      subroutine cut_heap ( text,  i_cut )
c--- deallocate space starting at i_cut. To be used when after a number
c--- of calls to alloc all allocated space has to be freed.
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      character*(*) text
      call dealloc ('cut_heap', last-(i_cut-1),  i_cut)
c      print'(2(a11, i10), 3x, a)', 
c     &     ' cut_heap i_cut=', i_cut, ' last=', last,  text
      end
 
C----------------------------------------------------------------------

      SUBROUTINE SPRIN
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ELEM,IDLS,IDJJ,ELEM1,LDES*8
     &           ,LDESP*8
      COMMON /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/SJ4K(2),NOTSJP(KS,100),NCFGP(KMX),NALSP(KMX,KS)
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON /CHAR/ LDES(KMX),LDESP(KMX),ELEM(3,KC+1/KC,2),ELEM1(3)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/VECT(KMX,KMX)
      COMMON    VPAR(KPR,2),AVP(9,2),AVEIG(2),AVCG(2),
     &  NPARK(2),NPAV(KC),    EIG(KMX),FNT(9,2),I1,I2,
     &  FACT(4),LCSER(KMX)
      DIMENSION TMXP(KMX,KMX)
      EQUIVALENCE (VECT,TMXP)
C
C
C    &     PRINT COEF, MUPOLE, ENERGY, OR EIGENVECTOR MATRIX.
C    &          if NECESSARY, TRANSFORM AND PRINT AGAIN.
C    &          FOR MUPOLE ELEMENTS, ALSO PRINT SQUARES.
C    &     KPAR=-1,0,+1,+2 FOR FK,GK,ZETA,MUPOLE,RESPECTIVELY.
C    &     KPAR=-2 FOR RK
C    &     KPAR=+3,+4 FOR ENERGY OR EIGENVECTOR MATRIX, RESPECTIVELY.
C
C
C     KS=8
      K=KK
      J1X=NSCONF(2,K)
      JAB=MAX(J1B-J1A,1)
      ITEST=0

      if (IBUTLER().NE.0) RETURN

      if (KPAR.EQ.3.AND.KCPLD(9).GE.7) RETURN
      if (KPAR-2) 101,108,108
  101 if (ICFC.EQ.1.AND.KPAR.NE.-2) ITEST=1
      if (ICFC.EQ.2.AND.KPAR.EQ.-2) ITEST=1
      if (ICFC.GT.2) ITEST=1
      if (ITEST.GT.0) WRITE (IW,10)
   10 FORMAT (1H2)
  108 IP=1
      KCPL1=1
      if (KPAR-1) 121,115,170
  115 KCPL1=KCPL
      GO TO 121
C
  120 if (KPAR-1) 121,121,170
  121 NLSP=NLS
      if (ITEST.EQ.0) GO TO 200
      WRITE (IW,14) PARNAM(NPAR,K),CAVE,COUPL(KCPL1),scrJ4,
     &  (JN, (LLIJK(L,JN,K),NIJK(L,JN,K), L=1,KS), JN=J1A,J1B,JAB)
   14 FORMAT (1H0,A8,6X,5HCAVE=,F9.5,10X,A6,9H COUPLING,8X,2HJ=,F4.1,
     &  10X,6HCONFIG,I3,8(2X,A1,I2)/84X,I3,8(2X,A1,I2))
      WRITE (IW,15)
   15 FORMAT (1H0)
      GO TO 200
  170 if (KPAR-3) 171,180,190
  171 IP=2
      I12=1
      JX=MAX(NSCONF(2,I1),NSCONF(2,I2))
      JXM1=MIN(JX-1,8)
      if (IDIP.EQ.0) GO TO 200
      if (IDIP.GT.1) WRITE (IW,10)
  172 do 174 J=1,JX
      if (J.GT.JXM1.AND.J.LT.JX) GO TO 174
      WRITE (IW,16) I12,IX, J,(LLIJK(L,J,I1),NIJK(L,J,I1),L=1,KS),scrJ4,
     &  J,(LLIJK(L,J,I2),NIJK(L,J,I2), L=1,KS),scrJ4P,COUPL(KCPL1)
  174 CONTINUE
   16 FORMAT (7H MUP MX,I3,4H II=,I1,I3,8(2X,A1,I2),3X,2HJ=,
     &  F4.1,4H ---,I3,8(2X,A1,I2),3X,3HJP=,F4.1,2X,A6,4H CPL)
      WRITE (IW,15)
      GO TO 200
C
  180 KCPL1=KCPL
      NLST=NLS
      if (IENGYD.GT.1) NLS=MIN(NLS,IENGYD)
      NLSP=NLS
      JXJ=J1X
      I=5
      if (IENGYD.LT.200) I=1
      if (scrJ4.NE.SJ4MN) JXJ=MIN(JXJ,I)
      WRITE (IW,18)  COUPL(KCPL1),scrJ4,   ((ELEM(I,JN,K), I=1,3),
     &  JN, (LLIJK(L,JN,K),NIJK(L,JN,K), L=1,KS), JN=1,JXJ)
   18 FORMAT (15H1 ENERGY MATRIX,4H   (,A6,9H COUPLING,1H),7X,
     &  2HJ=,F4.1,7X,3A6,5X,6HCONFIG,I3,8(2X,A1,I2)/
     &  (55X,3A6,11X,I3,8(2X,A1,I2)))
      if (JXJ.LT.J1X) WRITE (IW,13)
     &  (ELEM(I,J1X,K),I=1,3), J1X,(LLIJK(L,J1X,K),NIJK(L,J1X,K),L=1,KS)
   13 FORMAT (55X,3A6,11X,I3,8(2X,A1,I2))
      WRITE (IW,15)
      if (IENGYD-2) 500,200,200
C
  190 KCPL1=KCPL
      NLSP=NLS
  191 if (KCPL1-1) 196,192,196
C    &                            CALCULATE G-FACTORS
  192 M=NOSUBC
      if (scrJ4.LE.0.0) GO TO 196
      do 193 L=1,NLS
  193 CT4(L,1)=0.50116*(scrS(L,M)**2+scrS(L,M)-scrL(L,M)**2-scrL(L,M))/
     &     (scrJ4**2+scrJ4)+1.50116
      do 194 N=1,NLS
      CT4(N,2)=0.0
      do 194 L=1,NLS
  194 CT4(N,2)=CT4(N,2)+CT4(L,1)*(C(L,N)**2)
      WRITE  (9,20) (CT4(N,2), N=1,NLS)
   20 FORMAT (//11H   G-VALUES/(15X,11F9.3))
  196 if (KCPLD(KCPL1).GT.0) GO TO 281
      WRITE (IW,19) COUPL(KCPL1)
   19 FORMAT (//15H   EIGENVECTORS,4H   (,A6,9H COUPLING,1H)/)
C
  200 if (KPAR-2) 201,202,205
  201 if (ITEST) 282,282,205
  202 if (IDIP-1) 282,282,205
  205 LX=0
  210 LN=LX+1
      LX=LX+11
      if (LX-NLSP) 212,212,211
  211 LX=NLSP
  212 if (KPAR-4) 213,245,245
  213 if (KPAR.EQ.2) GO TO 215
      WRITE (IW,24) (NCFG(L), L=LN,LX)
      GO TO 216
  215 WRITE (IW,24) (NCFGP(L), L=LN,LX)
  216 if (KCPL1-1) 230,220,230
  220 WRITE (IW,22) (IDLS(L,IP), L=LN,LX)
   22 FORMAT (15X, 13(3H  (,A6))
      GO TO 240
  230 WRITE (IW,23) (IDJJ(L,IP), L=LN,LX)
   23 FORMAT (15X, 13(' (',A7))
  240 WRITE (IW,24) (L, L=LN,LX)
   24 FORMAT (13X,13I9)
      if (KPAR-3) 250,290,245
  245 WRITE (IW,24)
      if (KCPL1.NE.KCPL) GO TO 248
      N=(LN+11)/11
      WRITE (IW,25) N, (ELEM(2,LCSER(L),K),L=LN,LX)
   25 FORMAT (I8,9X,11(A6,3X))
      WRITE (IW,29) (LDES(L),L=LN,LX)
   29 FORMAT (17X,11(1H(,A8))
  248 if (LN.GT.NEVMAX) GO TO 281
C
C    &     PRINT COEF, DIPOLE, OR EIGENVECTOR MATRIX
C
  250 do 280 L=1,NLS
      if (KCPL1-1) 260,260,270
  260 WRITE (IW,26) NCFG(L), IDLS(L,1), L, (C(L,LP), LP=LN,LX)
   26 FORMAT (I3, ' (',A6, I3,1X,13F9.5)
      GO TO 280
  270 WRITE (IW,27) NCFG(L), IDJJ(L,1), L, (C(L,LP), LP=LN,LX)
   27 FORMAT (I3, ' (',A7, I3,13F9.5)
  280 CONTINUE
      if (LX-NLSP) 210,281,281
  281 WRITE (IW,27)
      if (KPAR-4) 282,283,283
  282 if (KCPL1-KCPL) 300,400,300
  283 if (NLS-1) 288,288,1283
 1283 PUR=0.0
      do 286 N=1,NLS
      CT4(N,1)=0.0
      do 285 L=1,NLS
      ERAS=C(L,N)**2
      if (ERAS-CT4(N,1)) 285,285,284
  284 CT4(N,1)=ERAS
  285 CONTINUE
  286 PUR=PUR+CT4(N,1)
      FNLS=NLS
      AVP(KCPL1,K)=AVP(KCPL1,K)+PUR
      FNT(KCPL1,K)=FNT(KCPL1,K)+FNLS
      PUR=PUR/FNLS
      NNN=MIN(NLS,NEVMAX)
      WRITE  (9,30) PUR, (CT4(N,1), N=1,NNN)
   30 FORMAT (8H PURITY=,F5.3,2X,11F9.5/(15X,11F9.5))
  288 if (KCPL1-KCPL) 400,289,400
  289 if (NLS-1) 400,400,300
C
C    &     PRINT ENERGY MATRIX
C
  290 ERAS=C(1,1)
      do 298 L=1,NLS
      if (KCPL1-1) 291,291,295
  291 if (ERAS.GT.9999.0) GO TO 292
      WRITE (IW,31) NCFG(L), IDLS(L,1), L, (C(L,LP), LP=LN,LX)
   31 FORMAT (I3, ' (',A6, I3,1X,13F9.3)
      GO TO 298
  292 WRITE (IW,32) NCFG(L), IDLS(L,1), L, (C(L,LP), LP=LN,LX)
   32 FORMAT (I3, ' (',A6, I3,1X,13F9.2)
      GO TO 298
  295 if (ERAS.GT.9999.0) GO TO 296
      WRITE (IW,33) NCFG(L),IDJJ(L,1), L, (C(L,LP), LP=LN,LX)
   33 FORMAT (I3,  ' (',A7, I3,13F9.3)
      GO TO 298
  296 WRITE (IW,34) NCFG(L),IDJJ(L,1), L, (C(L,LP), LP=LN,LX)
   34 FORMAT (I3,  ' (',A7, I3,13F9.2)
  298 CONTINUE
      if (LX.GE.IENGYD) GO TO 299
      if (LX-NLSP) 210,299,299
  299 WRITE (IW,34)
      NLS=NLST
      GO TO 500
C
C    &     TRANSFORM MATRIX
C
C---  skip the rest because LSJJ coupling is not calculated in CALCFC
  300 if (KCPL1.LT.100) GOTO 500

      if (KCPL1-1) 305,305,355
  305 do 320 L=1,NLS
      do 320 LP=1,NLSP
      CT4(L,LP)=0.0
      do 310 L0=1,NLS
  310 CT4(L,LP)=CT4(L,LP)+TMX(L0,L)*C(L0,LP)
  320 CONTINUE
      if (KPAR-2) 331,321,396
  321 do 330 L=1,NLS
      do 330 LP=1,NLSP
      C(L,LP)=0.0
      do 325 L0=1,NLSP
  325 C(L,LP)=C(L,LP)+CT4(L,L0)*TMXP(L0,LP)
  330 CONTINUE
      GO TO 395
  331 do 340 L=1,NLS
      do 340 LP=1,L
      C(L,LP)=0.0
      do 335 L0=1,NLS
  335 C(L,LP)=C(L,LP)+CT4(L,L0)*TMX(L0,LP)
  340 C(LP,L)=C(L,LP)
      GO TO 395
C
  355 do 370 L=1,NLS
      do 370 LP=1,NLSP
      TEMP=0.0
      if (ABS(KPAR).GT.1) GO TO 356
      if (NCFG(L).NE.NCFG(LP)) GO TO 365
  356 do 360 L0=1,NLS
      ERAS=TMX(L,L0)
      if (ERAS.EQ.0.0) GO TO 360
      TEMP=TEMP+ERAS*C(L0,LP)
  360 CONTINUE
  365 CT4(L,LP)=TEMP
  370 CONTINUE
      if (KPAR-2) 381,371,397
  371 do 380 L=1,NLS
      do 380 LP=1,NLSP
      C(L,LP)=0.0
      do 375 L0=1,NLSP
  375 C(L,LP)=C(L,LP)+CT4(L,L0)*TMXP(LP,L0)
  380 CONTINUE
      GO TO 395
  381 do 390 L=1,NLS
      do 389 LP=1,L
      TEMP=0.0
      if (ABS(KPAR).GT.1) GO TO 383
      if (NCFG(L).NE.NCFG(LP)) GO TO 386
  383 do 385 L0=1,NLS
      ERAS=TMX(LP,L0)
      if (ERAS.EQ.0.0) GO TO 385
      TEMP=TEMP+CT4(L,L0)*ERAS
  385 CONTINUE
  386 C(L,LP)=TEMP
  389 C(LP,L)=TEMP
  390 CONTINUE
C
  395 if (KPAR.LT.2.AND.ITEST.GT.0) WRITE (IW,35)
      if (KPAR.EQ.2.AND.IDIP.GT.1) WRITE (IW,35)
   35 FORMAT (1H0)
      KCPL1=KCPL
      GO TO 120
  396 KCPL1=2
      GO TO 398
  397 KCPL1=1
C    &          STORE EIGENVECTORS IN COUPLING KCPL
  398 do 399 L=1,NLS
      do 399 LP=1,NLS
      VECT(L,LP)=C(L,LP)
  399 C(L,LP)=CT4(L,LP)
      GO TO 191
C
  400 if (KPAR-2) 500,405,500
  405 if (I12-2) 410,500,500
  410 if (IBUTLER().EQ.0) THEN
        call sparse(c,nls,nlsp,kmx,lsparse, ixsparse,.false.)
        WRITE (IC) scrJ4,scrJ4P,NLS,NLSP, 
     &  (X(i), i=ixsparse,ixsparse+lsparse-1),
     &  NCFG,NCFGP
        call dealloc('sprin',lsparse, ixsparse)
      endif
      if (IDIP.EQ.0) GO TO 500
      do 430 L=1,NLS
      do 430 LP=1,NLSP
  430 C(L,LP)=C(L,LP)**2
      WRITE (IW,35)
      I12=2
      GO TO 172
C
  500   if (KPAR.GE.2.OR.ITEST.NE.0) THEN
      CALL STAMP  ('STAMP')
      endif

      if (KPAR.EQ.4) CALL SPRN37
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE SPRN37
C--- NOT USED BECAUSE FO SUPPRESSION OF CALCULATION OF LSJJ
C--- TRANSFORMATION IN CALCFC
C
C    &     PRINT EIGENVECTORS IN REPRESENTATIONS 3-7
C
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,LHS4
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      CHARACTER LH*1,LHS*1,LHQQ*1
      COMMON /CHAR/ LH(KS),LHS(KS),LHQQ(KMX)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/VECT(KMX,KMX)
      COMMON    VPAR(KPR,2),AVP(9,2),AVEIG(2),AVCG(2),
     &  NPARK(2),NPAV(KC),    EIG(KMX),FNT(9,2),I1,I2
     &  ,FACT(4)
C
c---  return always (without compiler message "dead code")
      if(kk.lt.100) return

      K=KK
      if (NOSUBC-1) 940,940,101
  101 if (NLS-1) 940,940,102
  102 if (NLS-NLSMAX) 103,103,940
  103 KCPL1=3
      IQ=NOSUBC
      IQM1=IQ-1
      IQM2=IQ-2
C
  110 if (KCPLD(KCPL1)) 150,150,900
  150 call zero2dim(tmx,nls,nls,kmx)
      READ  (IC)  
     & xl,rows, (ri,cols, (rj,tmx(ri,rj), icols=1,cols), irows=1,rows),
     & ( FK(L),FKJ(L),scrJ(L,IQM1),
     &  scrJ(L,IQM2),scrL6(L),FK6(L),scrS6(L),LHQQ(L),MULTQQ(L),L=1,NLS)
      NOICRD=NOICRD+1
C
C    &     TRANSFORM EIGENVECTORS
C
      do 210 L=1,NLS
      do 210 LP=1,NLS
      C(L,LP)=0.0
      do 205 I=1,NLS
  205 C(L,LP)=C(L,LP)+TMX(I,L)*VECT(I,LP)
  210 CONTINUE
C
C    &     PRINT EIGENVECTORS IN NEW COUPLING
C
      WRITE (IW,22) COUPL(KCPL1)
   22 FORMAT (//15H   EIGENVECTORS,4H   (,A6,9H COUPLING,1H)/)
C
      LX=0
  240 LN=LX+1
      LX=LX+11
      if (LX-NLS) 242,242,241
  241 LX=NLS
  242 do 250 L=1,NLS
      GO TO (243,243,243,244,245,246,247) KCPL1
  243 WRITE (IW,23) NCFG(L),scrJ(L,IQM1),FKJ(L),L,(C(L,LP),LP=LN,LX)
   23 FORMAT(1X,I1,F4.1,1HK,F4.1,I3,1X,13F9.5)
      GO TO 250
  244 LP1=scrL(L,NOSUBC)+1.0
      LHS4=LSYMB(LP1)
      WRITE (IW,24) NCFG(L),LHS4,FK(L),L,(C(L,LP),LP=LN,LX)
   24 FORMAT(I3,2X,A1,F4.1,I4,1X,13F9.5)
      GO TO 250
  245 WRITE (IW,23) NCFG(L),scrJ(L,IQM1),FK(L),L,(C(L,LP),LP=LN,LX)
      GO TO 250
  246 WRITE (IW,26) NCFG(L),scrJ(L,IQM2),LHQQ(L),FK6(L),MULTQQ(L),L,
     &  (C(L,LP), LP=LN,LX)
   26 FORMAT(1X,I1,F4.1,A1,F3.1,I2,I3,13F9.5)
      GO TO 250
  247 WRITE (IW,27) NCFG(L),scrJ(L,IQM2),MULTQQ(L),LHQQ(L),scrJ(L,IQM1),
     &  L, (C(L,LP), LP=LN,LX)
   27 FORMAT(1X,I1,F4.1,I2,A1,F3.1,I3,13F9.5)
  250 CONTINUE
      WRITE (IW,23)
      if (LX.GE.NEVMAX) GO TO 300
      if (LX-NLS) 240,300,300
C
C    &     CALCULATE PURITIES
C
  300 if (NLS.LE.1) GO TO 900
      PUR=0.0
      do 330 N=1,NLS
      CT4(N,1)=0.0
      do 320 L=1,NLS
      ERAS=C(L,N)**2
      if (ERAS-CT4(N,1)) 320,320,310
  310 CT4(N,1)=ERAS
  320 CONTINUE
  330 PUR=PUR+CT4(N,1)
      FNLS=NLS
      AVP(KCPL1,K)=AVP(KCPL1,K)+PUR
      FNT(KCPL1,K)=FNT(KCPL1,K)+FNLS
      PUR=PUR/FNLS
      NNN=MIN(NLS,NEVMAX)
      WRITE  (9,30) PUR, (CT4(N,1), N=1,NNN)
   30 FORMAT (8H PURITY=,F5.3,2X,11F9.5/(15X,11F9.5))
C
C
  900 KCPL1=KCPL1+1
      if (KCPL1-4) 110,110,910
  910 if (KCPL1-6) 920,110,921
  920 if (IQ-3) 930,110,110
  921 if (KCPL1-7) 110,110,930
  930 CALL   STAMP  ('STAMP')
  940 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION UNCPLA(FJ1,FJ2,FJ, FK,FJ1P,FJP)
C
C     IMPLICIT INTEGER*4 (I-N)
      A=SQRT((2.0*FJ+1.0)*(2.0*FJP+1.0))
      if (MOD(FJ1+FJ2+FJP+FK,2.0).GT.0.0) A=-A
      UNCPLA=A*S6J(FJ1,FJ,FJ2, FJP,FJ1P,FK)
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION UNCPLB(FJ1,FJ2,FJ, FK,FJ2P,FJP)
C
C     IMPLICIT INTEGER*4 (I-N)
      A=SQRT((2.0*FJ+1.0)*(2.0*FJP+1.0))
      if (MOD(FJ1+FJ+FJ2P+FK,2.0).GT.0.0) A=-A
      UNCPLB=A*S6J(FJ2,FJ,FJ1, FJP,FJ2P,FK)
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION RECPSH(FJ1,FJ2,FJP,FJ3,FJ,FJPP)
C
C     IMPLICIT INTEGER*4 (I-N)
      A=SQRT((2.0*FJP+1.0)*(2.0*FJPP+1.0))
      if (MOD(FJ1+FJ2+FJ3+FJ,2.0).GT.0.0) A=-A
      RECPSH=A*S6J(FJ1,FJ2,FJP, FJ3,FJ,FJPP)
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION RECPJP(FJ1,FJ2,FJP,FJ3,FJ,FJPP)
C
C     IMPLICIT INTEGER*4 (I-N)
      A=SQRT((2.0*FJP+1.0)*(2.0*FJPP+1.0))
      if (MOD(FJ1+FJ+FJPP,2.0).GT.0.0) A=-A
      RECPJP=A*S6J(FJ1,FJ2,FJP, FJ3,FJ,FJPP)
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION RECPEX(FJ1,FJ2,FJP,FJ3,FJ,FJPP)
C
C     IMPLICIT INTEGER*4 (I-N)
      A=SQRT((2.0*FJP+1.0)*(2.0*FJPP+1.0))
      if (MOD(FJ2+FJ3+FJP+FJPP,2.0).GT.0.0) A=-A
      RECPEX=A*S6J(FJ2,FJ1,FJP, FJ3,FJ,FJPP)
      RETURN
      END


C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CALCFCT
C
C     IMPLICIT INTEGER*4 (I-N)
      COMMON/CFACTRL/SCALE,SCALEM1,FCTN(200),FCT(0:200)
C
      SCALE=7
      SCALEM1=1.0/SCALE
      FCT(0)=1.0
      do 100 I=1,200
      FCTN(I)=0.0
      IF(I.LE.60) FCT(I)=FCT(I-1)*FLOAT(I)/SCALE
  100 IF(I.GT.60) FCT(I)=-1E-36
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION S9J (A1, A2, A3, A4, A5, A6, A7, A8, A9)
C
C     IMPLICIT INTEGER*4 (I-N)
      A9J=0.0
      A=MAX(ABS(A1-A9), ABS(A2-A6), ABS(A4-A8))
      C=MIN(A1+A9, A2+A6, A4+A8)
      FJ=2.0*A+1.0
      J1=FJ
      J2=2.0*C+1.0
      if (J2.LT.J1) GO TO 302
      do 301 J=J1,J2,2
      A9J=A9J-((-1.0)**J)*FJ*S6J(A1,A2,A3,A6,A9,A)
     &  *S6J(A4,A5,A6,A2,A,A8)*S6J(A7,A8,A9,A,A1,A4)
      A=A+1.0
  301 FJ=FJ+2.0
  302 S9J=A9J
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION S6J(FJ1,FJ2,FJ3,FL1,FL2,FL3)
C
C     IMPLICIT INTEGER*4 (I-N)
      COMMON/CFACTRL/SCALE,SCALEM1,FCTN(200),FCT(0:200)
C
      DELSQ(IA,IB,IC,ID)=FCT(ID-IA)*FCT(ID-IB)*FCT(ID-IC)/FCT(ID+1)
      S6J=0.0
      S1=FJ1+FJ2+FJ3
      S2=FJ1+FL2+FL3
      S3=FL1+FJ2+FL3
      IS1=S1
      IS2=S2
      IS3=S3
      if ((S1-FLOAT(IS1)).GT.0.01) GO TO 200
      if ((S2-FLOAT(IS2)).GT.0.01) GO TO 200
      if ((S3-FLOAT(IS3)).GT.0.01) GO TO 200
      IS4=FL1+FL2+FJ3
      IS5=FJ1+FJ2+FL1+FL2
      IS6=FJ2+FJ3+FL2+FL3
      IS7=FJ3+FJ1+FL3+FL1
      KN=MAX(IS1,IS2,IS3,IS4)
      KX=MIN(IS5,IS6,IS7)
      if (KX.LT.KN) GO TO 200
      IT1=2.000001*FJ1
      IT2=2.000001*FJ2
      IT3=2.000001*FJ3
      IT4=2.000001*FL1
      IT5=2.000001*FL2
      IT6=2.000001*FL3
      COEF=SQRT(DELSQ(IT1,IT2,IT3,IS1)*DELSQ(IT1,IT5,IT6,IS2)
     &         *DELSQ(IT4,IT2,IT6,IS3)*DELSQ(IT4,IT5,IT3,IS4))*SCALEM1
      SGN=-1+2*MOD(KN,2)
      SUM=0.0
      do 100 K=KN,KX
      SGN=-SGN
  100 SUM=SUM+SGN*FCT(K+1)/(FCT(K-IS1)*FCT(K-IS2)*FCT(K-IS3)*FCT(K-IS4)
     &  *FCT(IS5-K)*FCT(IS6-K)*FCT(IS7-K))
      S6J=COEF*SUM
  200 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION S3J0SQ(FJ1,FJ2,FJ3)
C
C    &     CALCULATE SQUARE OF 3-J SYMBOL WITH ZERO MAGNETIC QUANTUM NOS
C
C     IMPLICIT INTEGER*4 (I-N)
      COMMON/CFACTRL/SCALE,SCALEM1,FCTN(200),FCT(0:200)
C
      S3J0SQ=0.0
      FJ=FJ1+FJ2+FJ3
      IFJ=FJ
      if (MOD(IFJ,2).NE.0) GO TO 100
      IA=FJ-2.0*FJ1
      IB=FJ-2.0*FJ2
      IC=FJ-2.0*FJ3
      if (IA.LT.0.OR.IB.LT.0.OR.IC.LT.0) GO TO 100
      B=FCT(IA)*FCT(IB)*FCT(IC)/FCT(IFJ+1)
      A=FCT(IFJ/2)/(FCT(IA/2)*FCT(IB/2)*FCT(IC/2))
      S3J0SQ=SCALEM1*B*(A**2)
  100 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      FUNCTION FCTRL(A)
C
C          CALCULATE FACTORIAL OF A
C
      if (A.LT. -0.5) THEN
        FCTRL=0
      else
        FCTRL=1
        do 14 I=2, A+0.5
   14   FCTRL = FCTRL * I
      endif
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE SORT2(N,L,X,T,Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9,Y10,Y11,Y12)
C
C         N NUMBERS IN TABLE X ARE SORTED NUMERICALLY INCREASING IN
C         VALUE. THEN L TABLES OF Y (REAL) ARE REORDERED TO CORRESPOND
C         TO THEIR ASSOCIATED X. T IS A TEMPORARY TABLE.
C
C         Entries with the same value of X retain their mutual order.
C         This is required in PLEV, and also more or less in SPEC.


      DIMENSION X(N),T(N),Y1(N),Y2(N),Y3(N),Y4(N),Y5(N),Y6(N),
     &Y7(N),Y8(N),Y9(N),Y10(N),Y11(N),Y12(N)
C
      do 5 I = 1,N
    5 T(I) = I
      if ( N.LE.1 ) RETURN
      J = 1
    6 if (X(J).LE.X(J+1)) GO TO 8
      M = J
      ERAS   = X(M+1)
    7 X(M+1) = X(M)
      T(M+1) = T(M)
      M = M - 1
      if ( M.EQ.0 ) GO TO 9
      if ( X(M).GT.ERAS   ) GO TO 7
    9 X(M+1) = ERAS
      T(M+1) = J + 1
    8 J = J + 1
      if ( J.LT.N ) GO TO 6
C         COMMENCE ORDERING OF ASSOCIATED TABLES OF Y.
      if (L.EQ.0) GO TO 100
      GO TO (101,102,103,104,105,106,107,108,109,110,111,112), L
  112 CALL ORDER2(N,T,Y12)
  111 CALL ORDER2(N,T,Y11)
  110 CALL ORDER2(N,T,Y10)
  109 CALL ORDER2(N,T,Y9)
  108 CALL ORDER2(N,T,Y8)
  107 CALL ORDER2(N,T,Y7)
  106 CALL ORDER2(N,T,Y6)
  105 CALL ORDER2(N,T,Y5)
  104 CALL ORDER2(N,T,Y4)
  103 CALL ORDER2(N,T,Y3)
  102 CALL ORDER2(N,T,Y2)
  101 CALL ORDER2(N,T,Y1)
  100 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#


      SUBROUTINE ORDER1(N,T,Y)
C
C         A TABLE OF N INTEGERS STARTING AT Y IS ORDERED ACCORDING TO A
C         SEQUENCE TABLE IN THE FIRST N WORDS OF T.
C
      DIMENSION T(N)
      INTEGER YJ1,Y(N)
      do 4 J1=1,N
      if (T(J1).LT.0) GOTO 4
      J=J1
      YJ1=Y(J1)

    2 JN=T(J)
      T(J)=-T(J)
      IF(JN.EQ.J1) GOTO 3
      Y(J)=Y(JN)
      J=JN
      GOTO 2
    3 Y(J)=YJ1
    4 T(J1)=-T(J1)
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE ORDER2(N,T,Y)
C
C         A TABLE OF N NUMBERS STARTING AT Y IS ORDERED ACCORDING TO A
C         SEQUENCE TABLE IN THE FIRST N WORDS OF T.
C
      DIMENSION T(N)
      REAL YJ1,Y(N)
C
      do 4 J1=1,N
      if (T(J1).LT.0) GOTO 4
      J=J1
      YJ1=Y(J1)

    2 JN=T(J)
      T(J)=-T(J)
      IF(JN.EQ.J1) GOTO 3
      Y(J)=Y(JN)
      J=JN
      GOTO 2
    3 Y(J)=YJ1
    4 T(J1)=-T(J1)
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#


      SUBROUTINE ORDER3(N,T,Y)
C
C         A TABLE OF N CHARS*8 STARTING AT Y IS ORDERED ACCORDING TO A
C         SEQUENCE TABLE IN THE FIRST N WORDS OF T.
C
      DIMENSION T(N)
      CHARACTER*8 YJ1,Y(N)
      do 4 J1=1,N
      if (T(J1).LT.0) GOTO 4
      J=J1
      YJ1=Y(J1)

    2 JN=T(J)
      T(J)=-T(J)
      IF(JN.EQ.J1) GOTO 3
      Y(J)=Y(JN)
      J=JN
      GOTO 2
    3 Y(J)=YJ1
    4 T(J1)=-T(J1)
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE WARNDIM(KDIM,MAXDIM,NAMEDIM)
C     IMPLICIT INTEGER*4 (I-N)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER *(*) NAMEDIM
      if (KDIM.GT.MAXDIM) THEN
      WRITE(IW,*) ' ***** WARNING *****  INCREASE ',NAMEDIM,' FROM ',
     &    MAXDIM,' TO AT LEAST ',KDIM
      STOP 'INCREASE DIMENSIONS. See OUTPUT'
      endif
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CUVFD
C
C          READ CFP FROM CARDS OR TAPE,
C               AND SET UP TABLES FOR U, V, FK(I,I), D(I)
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      common/inpfile/ iInp
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ANAM,ZETANM,ICUV*1
      CHARACTER L1*1,A1*2,LLM1*1,LLM2*1,LLL*1
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,ALF1*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/CFP(KLS1,KLS1),
     &  MULTBR(KLSI,2),FLBR(KLSI,2),
     &  SBR(KLSI,2),ITRM(KLSI),IPNT(KLSI),ABG(KLSI,3),CAVEI(3),
     &  NALSJI(KJP),PJI(KJP),NJKI(25)
      COMMON/CHAR3/ LBRBCD(KLSI,2),ALFBR(KLSI,2)
      CHARACTER LBRBCD*1,ALFBR*2
      COMMON       CFPM1(KLS1,KLS1),CFP2(KLS1,KLS1)
      COMMON /C3LC2/ DUMMY(KLSI,KLSI,7),PC(KPC)
C
      CHARACTER*8 ABGNAM,ABGNM1,ABGNM2,SORTYS,SORTNO,ERAS
      DIMENSION CFGP(KLS1,KLS1),UA(KLS1,KLS1),VA(KLS1,KLS1),
     &  ABGNAM(3),UV(KLS1,KLS1),CFGPT(KLS1,KLS1), DUM(1)
      EQUIVALENCE (CFP2,CFGP,UA,VA),(CFPM1,CFGPT,UV)
      DATA ZETANM/'ZETA'/,(ABGNAM(I),I=1,3)/' ALPHA','BETA','GAMMA'/
      DATA ABGNM1,ABGNM2/'ALPHA','1000 A'/
      DATA ALF1/'1'/
      DATA SORTYS,SORTNO/' ','NO SORT'/
C
C
C          SET UNIT NUMBERS ONTO WHICH CFP, UV, AND COEFFS ARE
C               TO BE WRITTEN
C
      WRITE(IW,*)  '--- CUVFD CALLED ---'
      ID3=ID2+1
      ID4=ID3+1
      WRITE (IW,8) ID2,ID3,ID4
    8 FORMAT (/20H0CUVFD---WRITE UNITS,3I5)
      REWIND ID2
      REWIND ID3
      REWIND ID4
      LLM1=' '
      FLLM1=0.0
      NIM1=-7
      NIM2=-7
      NTM1=1
      ABGNAM(1)=ABGNM1
      if (ICTBCD.NE.0) ABGNAM(1)=ABGNM2
      GO TO 120
C
C          SET BACK CFP TABLES
C
  110 LLM2=LLM1
      NIM2=NIM1
      NTM2=NTM1
      NLTM2=NLTM1
      do 112 M=1,NTM1
      MULT(M,3)=MULT(M,2)
      LBCD(M,3)=LBCD(M,2)
      S(M,3)=S(M,2)
      FL(M,3)=FL(M,2)
  112 ALF(M,3)=ALF(M,2)
      LLM1=LL(1)
      FLLM1=FLL(1)
      NIM1=NI(1)
      NTM1=NOT
      NLTM1=NLT
      do 115 M=1,NOT
      MULT(M,2)=MULT(M,1)
      LBCD(M,2)=LBCD(M,1)
      S(M,2)=S(M,1)
      FL(M,2)=FL(M,1)
      ALF(M,2)=ALF(M,1)
      do 115 N=1,NOP
  115 CFPM1(M,N)=CFP(M,N)
C
C          READ SUB-SHELL DATA, AND TERMS TO BE INCLUDED
C
  120 READ (iInp,9) LL(1),NI(1),NOT,NOP,ICUV,KLAST,NLT,
     &  (MULT(M,1),LBCD(M,1),ALF(M,1), M=1,12), NOSORT
    9 FORMAT (3X,A1,3I4,2X,A1,I1,I4,4X,12(I1,A1,A2), I4)
      CALL WARNDIM(NOT,KLS1,'KLS1')
      CALL WARNDIM(NOP,KLS1,'KLS1')
      if (KLAST.EQ.0) KLAST=0
      if (NOT.LE.0) GO TO 900
      ERAS=SORTYS
      if (NOSORT.NE.0) ERAS=SORTNO
      if (ILNCUV+ICPC.EQ.0) GO TO 122
      WRITE (IW,10)
   10 FORMAT (1H1)
  122 if (NLT.LE.12) GO TO 124
      read (iInp,11) (MULT(M,1),LBCD(M,1),ALF(M,1), M=13,NLT)
   11 FORMAT (20(I1,A1,A2))
  124 if (NLT.GT.0) GO TO 126
      WRITE (IW,12) LL(1),NI(1),NOT,NOP,ICUV,KLAST,NLT,ERAS
   12 FORMAT (/1H ,A1,I2,2I5,A1,I1,I5,5X,9HALL TERMS,10X,A7)
      if (ILNCUV.GT.0) WRITE (IW,9)
      GO TO 127
  126 WRITE (IW,13) LL(1),NI(1),NOT,NOP,ICUV,KLAST,NLT,
     &  (MULT(M,1),LBCD(M,1),ALF(M,1), M=1,NLT)
   13 FORMAT (/1H ,A1,I2,2I5,A1,I1,I5,5X,20(I1,A1,A2,1X)/
     &  29X,20(I1,A1,A2,1X))
  127 do 128 J=1,24
      if (LSYMB(J).EQ.LL(1)) GO TO 129
  128 CONTINUE
  129 FLL(1)=J-1
C
C          READ CFP TABLE (TYPE 2) FROM CARDS
C
      read (iInp,16) (MULTBR(N,2),LBRBCD(N,2),ALFBR(N,2), N=1,NOP)
   16 FORMAT (20(I1,A1,A2))
      do 141 N=1,NOP
      do 141 M=1,NOT
  141 CFP2(M,N)=0.0
      M=0
  142 M=M+1
      IT=0
  143 ITO=IT
      read (iInp,17) MULT(M,4),LBCD(M,4),ALF(M,4),IT,
     &  (IPNT(J),PC(J), J=1,4)
   17 FORMAT (I1,A1,A2,I4, 4(I3,F13.10))
      if (ITO.GT.0) GO TO 144
      M1=MULT(M,4)
      L1=LBCD(M,4)
      A1=ALF(M,4)
  144 do 145 J=1,4
      N=IPNT(J)
      if (N.EQ.0) GO TO 145
      CFP2(M,N)=PC(J)
  145 CONTINUE
C                                   CHECK CARD ORDERING
      if (M1.NE.MULT(M,4)) GO TO 146
      if (L1.NE.LBCD(M,4)) GO TO 146
      if (A1.NE.ALF(M,4)) GO TO 146
      if (IABS(IT).EQ.(ITO+1)) GO TO 147
  146 WRITE (IW,18) LL(1),NI(1),M1,L1,A1,ITO,MULT(M,4),LBCD(M,4),
     &  ALF(M,4),IT
   18 FORMAT (//35H0CUVFD 18--CFP CARDS OUT OF ORDER  ,A1,I2,I4,A1,A2,I6
     &  /34X,I4,A1,A2,I6///)
  147 if (IT.GT.0) GO TO 143
      if (M.LT.NOT) GO TO 142
C
C          REORDER PARENTS TO CORRESPOND TO TERMS OF PRECEEDING L(N),
C          AND REORDER TERMS WITH SPECIFIED NLT TERMS FIRST
C
      if (LL(1).EQ.LLM1.AND.NI(1).EQ.NIM1+1) GO TO 155
      do 151 I=1,NOP
  151 IPNT(I)=I
      GO TO 170
  155 do 160 N=1,NOP
      do 159 M=1,NTM1
      if (MULTBR(N,2).NE.MULT(M,2)) GO TO 159
      if (LBRBCD(N,2).NE.LBCD(M,2)) GO TO 159
      if (ALFBR(N,2).EQ.ALF(M,2)) GO TO 160
  159 CONTINUE
      WRITE (IW,19) MULTBR(N,2),LBRBCD(N,2),ALFBR(N,2),LL(1),NI(1),
     &  LLM1,NIM1
   19 FORMAT (//18H0CUVFD 19--PARENT ,I1,A1,A2,4H OF ,A1,I2,
     &  27H  NOT FOUND AMONG TERMS OF ,A1,I2)
  160 IPNT(M)=N
C
  170 if (NLT.GT.0) GO TO 175
      do 171 I=1,NOT
  171 ITRM(I)=I
      NLT=NOT
      GO TO 185
  175 M=NLT
      do 180 I=1,NOT
      do 178 J=1,NLT
      if (MULT(I,4).NE.MULT(J,1)) GO TO 178
      if (LBCD(I,4).NE.LBCD(J,1)) GO TO 178
      if (ALF(I,4).EQ.ALF(J,1)) GO TO 179
  178 CONTINUE
      M=M+1
      ITRM(M)=I
      GO TO 180
  179 ITRM(J)=I
  180 CONTINUE
      if (M.EQ.NOT) GO TO 185
      WRITE (IW,20) M,NOT
   20 FORMAT (//17H0CUVFD 20--M,NOT=,2I5)
C                                   CALC VALUES OF S AND L
  185 do 190 M=1,NOT
      do 188 J=1,24
      if (LSYMB(J).EQ.LBCD(M,4)) GO TO 189
  188 CONTINUE
  189 FL(M,4)=J-1
      A=MULT(M,4)-1
  190 S(M,4)=0.5*A
      do 195 N=1,NOP
      do 192 J=1,24
      if (LSYMB(J).EQ.LBRBCD(N,2)) GO TO 193
  192 CONTINUE
  193 FLBR(N,2)=J-1
      A=MULTBR(N,2)-1
  195 SBR(N,2)=0.5*A
      if (FLL(1)-FLLM1) 198,196,200
  196 if (NI(1).GT.NIM1) GO TO 200
  198 WRITE (IW,21)
   21 FORMAT (//37H0CUVFD 21--CFP DECKS OUT OF ORDER****//)
C
  200 do 202 M=1,NOT
      I=ITRM(M)
  202 PC(M)=-100.0*S(I,4)-FL(I,4)
C
C          if ALL TERMS INCLUDED AND NOSORT=0, SORT BY S AND L
      if (NLT.LT.NOT.OR.NOSORT.NE.0) GO TO 203
      CALL SORT2(NLT,0,PC(1),PC(200),
     &              DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      CALL ORDER1(NLT,PC(200),ITRM)
C
  203 do 205 N=1,NOP
      J=IPNT(N)
      MULTBR(N,1)=MULTBR(J,2)
      LBRBCD(N,1)=LBRBCD(J,2)
      ALFBR(N,1)=ALFBR(J,2)
      SBR(N,1)=SBR(J,2)
      FLBR(N,1)=FLBR(J,2)
      do 205 M=1,NOT
      I=ITRM(M)
  205 CFP(M,N)=CFP2(I,J)
      do 210 M=1,NOT
      I=ITRM(M)
      MULT(M,1)=MULT(I,4)
      LBCD(M,1)=LBCD(I,4)
      ALF(M,1)=ALF(I,4)
      S(M,1)=S(I,4)
  210 FL(M,1)=FL(I,4)
      if (ILNCUV.EQ.0) GO TO 220
      WRITE (IW,23) (MULTBR(N,1),LBRBCD(N,1),ALFBR(N,1), N=1,NOP)
   23 FORMAT (//10H0CFP TABLE//5X,10(I9,A1,A2)/(5X,10(I9,A1,A2)))
      do 218 M=1,NOT
  218 WRITE (IW,24) MULT(M,1),LBCD(M,1),ALF(M,1), (CFP(M,N), N=1,NOP)
   24 FORMAT (1H ,I1,A1,A4,10F12.8/(2X,10F12.8))
C                                   CHECK ORTHONORMALITY OF CFP ROWS
  220 do 230 M=1,NOT
      do 229 M1=M,NOT
      SUM=-1.0
      if (M.EQ.M1) GO TO 222
      SUM=0.0
      if (MULT(M,1).NE.MULT(M1,1)) GO TO 229
      if (LBCD(M,1).NE.LBCD(M1,1)) GO TO 229
  222 do 224 N=1,NOP
  224 SUM=SUM+CFP(M,N)*CFP(M1,N)
      if (ABS(SUM).LT.1.0E-4) GO TO 229
      WRITE (IW,25) MULT(M,1),LBCD(M,1),ALF(M,1),
     &             MULT(M1,1),LBCD(M1,1),ALF(M1,1)
   25 FORMAT (//36H0CUVFD 25--CFP NOT ORTHONORMAL FOR  ,2(I4,A1,A2)//)
  229 CONTINUE
  230 CONTINUE
C                                   CHECK ORTHONORMALITY OF CFP COLUMNS
      if (NI(1).EQ.0) GO TO 250
      A=NI(1)
      B=(4.0*FLL(1)+3.0-A)/A
      do 239 N=1,NOP
      SUM=-B*(2.0*FLBR(N,1)+1.0)*(2.0*SBR(N,1)+1.0)
      do 238 N1=N,NOP
      if (N.EQ.N1) GO TO 232
      SUM=0.0
      if (MULTBR(N,1).NE.MULTBR(N1,1)) GO TO 238
      if (LBRBCD(N,1).NE.LBRBCD(N1,1)) GO TO 238
  232 do 234 M=1,NOT
      A=(2.0*FL(M,1)+1.0)*(2.0*S(M,1)+1.0)
  234 SUM=SUM+A*CFP(M,N)*CFP(M,N1)
      if (ABS(SUM).LT.1.0E-4) GO TO 238
      WRITE (IW,25) MULTBR(N,1),LBRBCD(N,1),ALFBR(N,1),
     &             MULTBR(N1,1),LBRBCD(N1,1),ALFBR(N1,1)
  238 CONTINUE
  239 CONTINUE
C
C          READ OR CALC CFGP (IF NEEDED) AND WRITE DISK ID2
C
  250 NLP=1
      NOCFP=0
      if (LL(1).NE.LLM1.OR.NI(1).NE.(NIM1+1)) GO TO 252
      NLP=NLTM1
      NOCFP=1
  252 NLGP=0
      NOCFGP=0
      if (LL(1).NE.LLM2.OR.NI(1).NE.(NIM2+2)) GO TO 254
      NLGP=NLTM2
      NOCFGP=2.0*FLL(1)+1.0
  254 NORCD=1+NOCFP+NOCFGP
      if (NOCFP.GT.0) NORCD=NORCD+1
      if (NOCFGP.GT.0) NORCD=NORCD+1
      WRITE (ID2) LL(1),NI(1),FLL(1),NOT,NLT,NORCD
      WRITE (ID2) (MULT(M,1),LBCD(M,1),ALF(M,1),FL(M,1),S(M,1),M=1,NLT),
     &  NLP,NLGP,NOCFP,NOCFGP
      if (NOCFP.EQ.0) GO TO 257
c      WRITE (ID2) ((CFP(M,N),M=1,NLT), N=1,NLP)
      call  putsparse ((id2), cfp, nlt, nlp, kls1,.false.)
      WRITE (ID2) (MULT(M,2),LBCD(M,2),ALF(M,2),FL(M,2),S(M,2),M=1,NLP)
  257 if (NOCFGP.EQ.0) GO TO 300
C
      WRITE (ID2) (MULT(M,3),LBCD(M,3),ALF(M,3),FL(M,3),S(M,3),M=1,NTM2)
      FLP=-1.0
      do 285 K=1,NOCFGP
      	FLP=FLP+1.0
      	SP=0.5*(1.0+(-1.0)**K)
      	MULP=2.0*SP+1.0
      	IFLP=FLP
      	LLL=LSYMB(IFLP+1)
      	A=SQRT((2.0*FLP+1.0)*(2.0*SP+1.0))
      	do 273 M=1,NLT
      	  X0=FL(M,1)+S(M,1)+1.0
      	  do 273 N=1,NLGP
      	    SUM=0.0
      	    do 272 N1=1,NOP
      	      B=S6J(FLL(1),FLL(1),FLP, FL(M,1),FL(N,3),FL(N1,2))
              if (B.EQ.0.0) GO TO 272
              B=B*S6J(0.5,0.5,SP, S(M,1),S(N,3),S(N1,2))
              if (B.EQ.0.0) GO TO 272

              B=B*SQRT((2.0*FL(N1,2)+1.0)*(2.0*S(N1,2)+1.0))
              B=B*CFP(M,N1)*CFPM1(N1,N)
              SUM=SUM+B
  272       CONTINUE
           SUM=SUM*A
           X=X0+FL(N,3)+S(N,3)
           if (AMOD(X,2.0).GT.0.0) SUM=-SUM
  273      CFGP(M,N)=SUM
        if (ILNCUV.EQ.0) GO TO 276
        WRITE (IW,29) LL(1),MULP,LLL, (MULT(N,3),LBCD(N,3),ALF(N,3),
     &        N=1,NLGP)
   29   FORMAT (///11H0CFGP FOR  ,A1,2H2(,I1,A1,1H)//5X,10(I9,A1,A2)/
     &       (5X,10(I9,A1,A2)))
        do 275 M=1,NLT
  275    WRITE (IW,24) MULT(M,1),LBCD(M,1),ALF(M,1),(CFGP(M,N),N=1,NLGP)
c 276   WRITE (ID2) ((CFGP(M,N),M=1,NLT), N=1,NLGP)
  276   call  putsparse ((id2), cfgp, nlt, nlgp, kls1,.false.)

C                                   CHECK ORTHONORMALITY OF CFGP ROWS
      if (NLGP.eq.NTM2) then
        L=200
        do 280 M=1,NLT
        do 279 M1=M,NLT
          if (MULT(M,1).NE.MULT(M1,1)) GO TO 279
          if (LBCD(M,1).NE.LBCD(M1,1)) GO TO 279
          L=L+1
          if (K.EQ.1) PC(L)=0.0
          do 278 N=1,NTM2
  278       PC(L)=PC(L)+CFGP(M,N)*CFGP(M1,N)
  279   CONTINUE
  280   CONTINUE
      endif
C
  285 CONTINUE
C
      if (NLGP.eq.NTM2) then 
        L=200
        do 290 M =1,NLT
        do 289 M1=M,NLT
          if (MULT(M,1).NE.MULT(M1,1)) GO TO 289
          if (LBCD(M,1).NE.LBCD(M1,1)) GO TO 289
          L=L+1
          if (M.EQ.M1) PC(L)=PC(L)-1.0
          if (ABS(PC(L)).gt.1.0E-4) then
            WRITE (IW,'(///a  ,3(I4,A1,A2))')
     &        ,' CUVFD --  CFGP NOT ORTHONORMAL FOR  '
     &        , MULT(M ,1),LBCD(M ,1),ALF(M ,1)
     &        , MULT(M1,1),LBCD(M1,1),ALF(M1,1),MULP,LLL
          endif
  289   CONTINUE
  290   CONTINUE
      endif
C
C          CALCULATE NUMBER OF TERMS OF EACH KIND
C
  300 K=0
      KB=K
      N2=-100
      do 310 M=1,NLT
      N1=N2
      N2=PC(M)
      if (N2.NE.N1) GO TO 309
      NTRMK(KB)=NTRMK(KB)+1
      GO TO 310
  309 K=K+1
      KB=MIN(K,KLSC)
      NTRMK(KB)=1
  310 CONTINUE
      NDIFFT=K
      CALL WARNDIM(NDIFFT,KLSC,'KLSC')
C
C          CALCULATE NUMBER OF LEVELS OF EACH J
C
      M=0
      MB=M
      do 315 J=1,NLT
      EJI=ABS(FL(J,1)-S(J,1))-1.0
      NJI=FL(J,1)+S(J,1)-EJI
      do 315 IJ=1,NJI
      M=M+1
      MB=MIN(M,KJP)
      EJI=EJI+1.0
      NALSJI(MB)=J
      A=J
      PC(MB)=-200.0*EJI-25.0*S(J,1)-FL(J,1)+0.001*A
  315 PJI(MB)=EJI
      NTOTJI=M
      CALL WARNDIM(M,KJP,'KJP')
      CALL SORT2(M,1,PC(1),PC(500),PJI,
     &             DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      CALL ORDER1(M,PC(500),NALSJI)
C
      K=1
      NJKI(K)=1
      if (M.LE.1) GO TO 320
      do 319 J=2,M
      if (PJI(J).NE.PJI(J-1)) GO TO 317
      NJKI(K)=NJKI(K)+1
      GO TO 319
  317 K=K+1
      NJKI(K)=1
  319 CONTINUE
  320 NDIFJI=K
C
C        WRITE U,V ON DISK ID3, AND COEFFS FOR L(N) ON DISK ID4
C
c---The cccc comments were removed to have n=1 information be written on
c--- id3.
      FN=NI(1)
      NMXM1=4.0*FLL(1)+1.0
      NPAR=0
      if (NI(1).EQ.0.OR.NI(1).EQ.(NMXM1+1).OR.FLL(1).EQ.0.0) GO TO 410
cccc      NPAR=1
cccc      if (NI(1).EQ.1.OR.NI(1).EQ.NMXM1) GO TO 410
      NPAR=1.0+FLL(1)
      if (IABG.EQ.0) GO TO 410
      NPAR=1.0+FLL(1)+MIN(3.,FLL(1))
  410 NORCD=1+NPAR
      WRITE (ID4) LL(1),NI(1),FLL(1),NOT,NLT,NORCD
      WRITE (ID4) NDIFFT, (NTRMK(K), K=1,NDIFFT), NTOTJI,
     &  (NALSJI(M),PJI(M), M=1,NTOTJI), NDIFJI, (NJKI(M), M=1,NDIFJI)
      if (NPAR.EQ.0) GO TO 800
cccc      if (NI(1).EQ.1) GO TO 500
      NORCD=4.0*FLL(1)+2.0
      WRITE (ID3) LL(1),NI(1),FLL(1),NOT,NLT,NORCD
c     print*,ID3, LL(1),NI(1),FLL(1),NOT,NLT,NORCD
C
C          CALC U MATRICES
C
      NOR=NORCD/2
      do 485 K=0,nor-1
      R=K
      do 435 M=1,NOT
      MPX=MIN0(M,NLT)
      do 435 MP=1,MPX
      A=0.0
      B=0.0
      if (S(M,1).NE.S(MP,1)) GO TO 434
      if (R.GT.(FL(M,1)+FL(MP,1))) GO TO 434
      if (R.LT.ABS(FL(M,1)-FL(MP,1))) GO TO 434
      do 432 N=1,NOP
      B=CFP(M,N)*CFP(MP,N)
      if (B.EQ.0.0) GO TO 432
      B=B*S6J(FLL(1),FLL(1),R, FL(M,1),FL(MP,1),FLBR(N,1))
      X=AMOD(FLBR(N,1),2.0)
      if (X.GT.0.0) B=-B
      A=A+B
  432 CONTINUE
      A=A*SQRT((2.0*FL(M,1)+1.0)*(2.0*FL(MP,1)+1.0))*FN
      X=AMOD(FLL(1)+FL(M,1)+R,2.0)
      if (X.GT.0.0) A=-A
      B=A
      X=AMOD(FL(M,1)-FL(MP,1),2.0)
      if (X.NE.0.0) B=-B
  434 UA(M,MP)=A
  435 UA(MP,M)=B
c      WRITE (ID3) K, ((UA(M,MP),M=1,NLT), MP=1,NLT)
      call  putsparse ((id3), ua, nlt, nlt, kls1,.false.)
C
      if (R.EQ.0.0.OR.NPAR.LE.1) GO TO 485
      X=AMOD(R,2.0)
      if (X.GT.0.0) GO TO 460
C                                   CALC FK
      A=2.0*FLL(1)+1.0
      CIJ=0.5*A*S3J0SQ(FLL(1),R,FLL(1))
      CAVE=CIJ*FN*(FN-1.0)/(A+FLL(1)+FLL(1))
      CAVEP=CAVE-FN*CIJ
      CIJ=CIJ*A
      if (ILNCUV.GT.0) WRITE (IW,41) K,CAVE,LL(1),NI(1)
   41 FORMAT (//2H0F,I1,6X,5HCAVE=,F12.7,10X,A1,I2//)
C
      NOPC=0
      MMAX=0
      do 450 L=1,NDIFFT
      MMIN=MMAX+1
      MMAX=MMAX+NTRMK(L)
      do 450 M=MMIN,MMAX
      CIJ1=CIJ/(2.0*FL(M,1)+1.0)
      do 448 MP=MMIN,M
      NOPC=NOPC+1
      A=0.0
      do 445 MPP=1,NOT
  445 A=A+UA(MPP,M)*UA(MPP,MP)
      PC(NOPC)=CIJ1*A
      if (M.NE.MP) GO TO 448
      PC(NOPC)=PC(NOPC)+CAVEP
  448 CONTINUE
      if (ICPC.EQ.0) GO TO 450
      N2=NOPC-M+MMIN
      WRITE (IW,42) NOPC,MULT(M,1),LBCD(M,1), (PC(N1), N1=N2,NOPC)
   42 FORMAT (2I5,A1,10F11.6/(13X,10F11.6))
  450 CONTINUE
      KPAR=-1
      WRITE(ANAM,'(1HF,I1,1H(,A1,I2,1H))') K,LL(1),NI(1)
      WRITE (ID4) KPAR,K,ANAM,CAVE,NOPC, (PC(N1), N1=1,NOPC)
      CALL CLOK(TIME)
      DELT=TIME-TIME0
      if (ILNCUV.GT.0) WRITE (IW,45) ANAM,NOPC,CAVE,DELT
   45 FORMAT (1H ,A7,7H    PC(,I4,1H),7X,5HCAVE=,F12.7,8X,5HTIME=,F8.2,
     &  4H MIN)
      GO TO 485
C                                   CALC BETA OR GAMMA
  460 if (K.GT.5) GO TO 485
      I=(K+1)/2
      do 480 M=1,NLT
      A=0.0
      do 465 MP=1,NOT
  465 A=A+UA(MP,M)**2
      A=A/(2.0*FL(M,1)+1.0)
      GO TO (471,473,475) I
  471 ABG(M,2)=3.0*A
      ABG(M,3)=0.0
      GO TO 480
  473 ABG(M,3)=ABG(M,2)+7.0*A
      GO TO 480
  475 ABG(M,2)=ABG(M,2)+11.0*A
      ABG(M,3)=ABG(M,3)+11.0*A
  480 CONTINUE
C
  485 CONTINUE
C                               FINISH CALCULATION OF ALPHA, BETA, GAMMA
      if (NPAR.LE.1.OR.IABG.EQ.0) GO TO 500
      A=4.0*FLL(1)+1.0
      B=FN*(FN-A-1.0)
      CAVEI(2)=B/26.0
      B=B*FLL(1)/A
      AFACT=1.0
      if (ICTBCD.NE.0) AFACT=0.001
      CAVEI(1)=B*(FLL(1)+1.0)*AFACT
      A=1.0/(2.0*FLL(1)-1.0)
      CAVEI(3)=A*B
      do 487 M=1,NLT
      ABG(M,1)=FL(M,1)*(FL(M,1)+1.0)*AFACT+CAVEI(1)
      ABG(M,2)=0.25*ABG(M,2)+CAVEI(2)
  487 ABG(M,3)=A*ABG(M,3)+CAVEI(3)
      do 488 I=1,NOPC
  488 PC(I)=0.0
      K=0
      do 492 I=1,MIN(3.,FLL(1))
      J=I
      ANAM=ABGNAM(J)
      if (LL(1).EQ.LSYMB(3).AND.I.EQ.2) J=3
      if (ILNCUV.GT.0) WRITE (IW,48) ANAM,CAVEI(J),LL(1),NI(1)
   48 FORMAT (//1H0,A8,5HCAVE=,F10.7,10X,A1,I2//)
      M=0
      NOPC=0
      do 490 L=1,NDIFFT
      NK=NTRMK(L)
      do 490 N=1,NK
      M=M+1
      N2=NOPC+1
      NOPC=NOPC+N
      PC(NOPC)=ABG(M,J)
      if (ICPC.EQ.0) GO TO 490
      WRITE (IW,42) NOPC, MULT(M,1),LBCD(M,1), (PC(N1), N1=N2,NOPC)
  490 CONTINUE
      WRITE (ID4) KPAR,K,ANAM,CAVEI(J),NOPC, (PC(N1), N1=1,NOPC)
      CALL CLOK(TIME)
      DELT=TIME-TIME0
      if (ILNCUV.GT.0) WRITE (IW,45) ANAM,NOPC,CAVEI(J),DELT
  492 CONTINUE
C
C          CALC V MATRICES
C
  500 NOV=NOR
cccc      if (NI(1).EQ.1) NOV=1
      do 585 K=0,nov-1
cccc      if (NOV.EQ.1) K=1
      R=K
      do 535 M=1,NLT
      do 535 MP=1,NLT
      A=0
      B=0.0
      if (S(M,1)+S(MP,1).LT.1.0) GO TO 534
      if (ABS(S(M,1)-S(MP,1)).GT.1.0) GO TO 534
      if (FL(M,1)+FL(MP,1).LT.R) GO TO 534
      if (ABS(FL(M,1)-FL(MP,1)).GT.R) GO TO 534
      do 532 N=1,NOP
      B=CFP(M,N)*CFP(MP,N)
      if (B.EQ.0.0) GO TO 532
      B=B*S6J(0.5,0.5,1.0, S(M,1),S(MP,1),SBR(N,1))
     &   *S6J(FLL(1),FLL(1),R, FL(M,1),FL(MP,1),FLBR(N,1))
      X=AMOD(S(M,1)+SBR(N,1)+1.5+FLBR(N,1),2.0)
      if (X.GT.0.0) B=-B
      A=A+B
  532 CONTINUE
      A=A*FN*SQRT(1.5*(2.0*S(M,1)+1.0)*(2.0*S(MP,1)+1.0)
     &  *(2.0*FL(M,1)+1.0)*(2.0*FL(MP,1)+1.0))
      X=AMOD(FLL(1)+FL(M,1)+R,2.0)
      if (X.GT.0.0) A=-A
      B=A
      X=AMOD(FL(M,1)-FL(MP,1)+S(M,1)-S(MP,1),2.0)
      if (X.NE.0.0) B=-B
  534 VA(M,MP)=A
  535 VA(MP,M)=B
c      WRITE (ID3) K, ((VA(M,MP),M=1,NLT), MP=1,NLT)
      call  putsparse ((id3), va, nlt, nlt, kls1,.false.)
      if (K.NE.1) GO TO 585
C                                   CALC COEFFS OF ZETA
      if (ILNCUV.GT.0) WRITE (IW,52) LL(1),NI(1)
   52 FORMAT (//5H0ZETA,29X,A1,I2//)
C
      B=SQRT(FLL(1)*(FLL(1)+1.0)*(2.0*FLL(1)+1.0))
      NOPC=0
      NOPCB=0
      MMAX=0
      do 560 L=1,NDIFJI
      MMIN=MMAX+1
      MMAX=MMAX+NJKI(L)
      do 560 M=MMIN,MMAX
      J=NALSJI(M)
      IF(M.GT.KJP) THEN
      WRITE(IW,*) ' KJP MUST BE INCREASED TO ',M
      if (L.EQ.NDIFJI) STOP
      GOTO 560
      endif
      do 550 MP=MMIN,M
      JP=NALSJI(MP)
      NOPC=NOPC+1
      NOPCB=MIN(NOPC,KPC)
      PC(NOPCB)=0.0
      A=VA(J,JP)
      if (A.EQ.0.0) GO TO 550
      X=AMOD(FL(JP,1)+S(J,1)+PJI(M),2.0)
      if (X.GT.0.0) A=-A
      PC(NOPCB)=A*B*S6J(S(J,1),FL(J,1),PJI(M), FL(JP,1),S(JP,1),1.0)
  550 CONTINUE
      if (ICPC.EQ.0) GO TO 560
      N2=NOPCB-M+MMIN
      WRITE (IW,54) NOPCB,MULT(J,1),LBCD(J,1),PJI(M),
     &  (PC(N1), N1=N2,NOPCB)
   54 FORMAT (2I5,A1,F4.1,10F11.6/(17X,10F11.6))
  560 CONTINUE
      CALL WARNDIM(NOPC,KPC,'KPC')
      KPAR=1
      CAVE=0.0
      ANAM=ZETANM
      WRITE (ID4) KPAR,K,ANAM,CAVE,NOPC, (PC(N1), N1=1,NOPC)
      CALL CLOK(TIME)
      DELT=TIME-TIME0
      if (ILNCUV.GT.0) WRITE (IW,45) ANAM,NOPC,CAVE,DELT
C
  585 CONTINUE
C
  800 CALL CLOK(TIME)
      DELT=TIME-TIME0
      WRITE (IW,60) LL(1),NI(1),DELT
   60 FORMAT (26H FINISHED CUVFD CALC FOR  ,A1,I2,11H   AT TIME=,F8.2,
     &  4H MIN)
      GO TO 110
C
C          WRITE DUMMY RECORDS REQUIRED BY SYSTEM BUG ON IBM7030
C
  900 WRITE (ID2) (PC(J), J=1,20)
      WRITE (ID3) (PC(J), J=1,20)
      WRITE (ID4) (PC(J), J=1,20)
      REWIND ID2
      REWIND ID3
      REWIND ID4
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE LNCUV
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,BLANK*2
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1, LINE*80
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
      DATA BLANK/'  '/
C
C
C
C    &     READ AND CHECK INPUT CONFIGURATIONS
C
C     KS=8
      IERROR=0
      WRITE (IW,10)
   10 FORMAT (/8H0  K   J,11X,13HCONFIGURATION,35X,
     &  35HN   PAR   I   TERMS OF LI(NI) -----)
      do 259 K=1,2
      if (NSCONF(1,K).LE.0) GO TO 259
      WRITE (IW,*)
      NOSUBC=NSCONF(1,K)
      JMAX=NSCONF(2,K)
      do 202 I=1,KS
      LL(I)=LSYMB(1)
      FLL(I)=0.0
      NLASTT(I)=1
      MULT(1,I)=1
      LBCD(1,I)=LSYMB(1)
      JJJ=MAX(NSCONF(2,1),NSCONF(2,2))
      do 202 J=1,JJJ
      LLIJK(I,J,K)=LSYMB(1)
      FLLIJK(I,J,K)=0.0
  202 NIJK(I,J,K)=0
      do 249 J=1,JMAX
      N=0
      IPRITY(K)=1
      LSUM=0
      CALL GETLINE (LINE)
      READ(LINE,20) (LL(I),NI(I), I=1,NOSUBC)
      PRINT'(A,8(A1,I2,2X))',
     & ' CONFIGURATION:  ',(LL(I),NI(I), I=1,NOSUBC)
   20 FORMAT (8(A1,I2,2X))
      do 229 I=1,NOSUBC
      NIJK(I,J,K)=NI(I)
      do 212 L=1,24
        if (LSYMB(L).EQ.LL(I)) THEN
           ILL    = L-1
           FLL(I) = L-1
           FLLIJK(I,J,K) = FLL(I)
           GO TO 213
        endif
  212 CONTINUE
      PRINT*,  LINE
      PRINT*, 'ERROR: SHELL ',I,' NOT SPECIFIED ON INPUT'
      STOP    'ERROR: SHELL NOT SPECIFIED ON INPUT'
  213 LLIJK(I,J,K)=LL(I)
      if (LL(I).EQ.LLIJK(I,1,K).OR.I.EQ.NOSUBC) GO TO 220
      do 217 M=1,J
      if (NIJK(I,M,K).NE.0) GO TO 218
  217 CONTINUE
      GO TO 220
  218 if (LL(I).EQ.LLIJK(I,M,K)) GO TO 220
      IERROR=7
  220 N=N+NI(I)
      if (MOD(NI(I)*ILL,2).NE.0) IPRITY(K)=-IPRITY(K)
      LSUM=LSUM+NI(I)*ILL
  229 CONTINUE
C
      NPT=0
      do 232 I=1,NOSUBC
      NN   = NIJK(I,J,K)
      NFULL= 4 * FLL(I) + 2
      if (NN.le.1 .or. NN.ge.NFULL-1) then
        NOT=1
        NLT=1
        ALF (1,I)=' '
        if (NN.eq.0.or.NN.eq.NFULL) then
          MULT(1,I)=1
          LBCD(1,I)=LSYMB(1)
          FL  (1,I)=0.0
          S   (1,I)=0.0
        elseif (NN.EQ.1.OR.NN.EQ.NFULL-1) then
          MULT(1,I)=2
          LBCD(1,I)=LL(I)
          FL  (1,I)=FLL(I)
          S   (1,I)=0.5
        endif
      else
        CALL LOCDSK(ID2,FLL(I),NIJK(I,J,K))
        READ (ID2)
     &  (MULT(M,I),LBCD(M,I),ALF(M,I),FL(M,I),S(M,I), M=1,NLT)
      endif

      if (I.EQ.1.AND.(J.LT.JMAX.OR.JMAX.EQ.1)) NLT=MIN(NLT,NLT11)

      if (I.eq.1) then
                       call chcksh ( 1, nlt, klsi)
      else if (I.eq.2) then
                       call chcksh ( 2, nlt, 17  )
      else
                       call chcksh ( i, nlt, 17  )
      endif

      if (NLT.EQ.NOT) GO TO 232
      NPT=NPT+1
      if (NPT.GT.1) GO TO 231
      WRITE (IW,21) K,J, (LL(IP),NIJK(IP,J,K),IP=1,KS), N,IPRITY(K),I,
     &  (MULT(L,I),LBCD(L,I),ALF(L,I), L=1,NLT)
   21 FORMAT (I4,I4,7X,8(A1,I2,3X),I5,I5,I5,3X,13(I1,A1,A2)/
     &  81X,13(I1,A1,I2))
      GO TO 232
  231 WRITE (IW,22) I, (MULT(L,I),LBCD(L,I),ALF(L,I), L=1,NLT)
   22 FORMAT (73X,I5,3X,13(I1,A1,A2)/81X,13(I1,A1,A2))
  232 CONTINUE
      if (NPT.GT.0) GO TO 240
      WRITE (IW,23) K,J, (LL(I),NIJK(I,J,K),I=1,KS), N,IPRITY(K)
   23 FORMAT (I4,I4,7X,8(A1,I2,3X),I5,I5,8X,3HALL)
  240 IF(K.EQ.1.AND.J.EQ.1) NO=N
      IF(K.EQ.1) LSUMO=LSUM
      IF(J.EQ.1) IPRTYO=IPRITY(K)
      IF(N.NE.NO.OR.IPRITY(K).NE.IPRTYO) IERROR=7
  249 CONTINUE
  259 CONTINUE
      IIPRTY=ABS(LSUMO-LSUM)
C
      if (IERROR.EQ.0) GO TO 281
      WRITE (IW,25)
   25 FORMAT (///27H0LNCUV 25--DECK SETUP ERROR////)
      STOP
C
C    &     CALC (LI//CK//LJ) TABLE
C
  281 WRITE (IW,*)
      N=NOSUBC
      do 299 I1=1,NOSUBC
      do 299 I2=I1,NOSUBC
      if (I2-I1) 283,283,284
  283 I=I1
      GO TO 285
  284 N=N+1
      I=N
  285 do 286 NK=1,6
  286 CIJK(I,NK)=0.0
      FK0=ABS(FLL(I1)-FLL(I2))-2.0
      KMIN=FK0+2.0
      NOK=(FLL(I1)+FLL(I2)-FK0)/2.0
      do 295 NK=1,NOK
      FK0=FK0+2.0
  295 CIJK(I,NK)=CIJKF(FLL(I1),FLL(I2),FK0)
      if (ILNCUV) 299,299,297
  297 WRITE (IW,29) KMIN,I1, LL(I1),I2, LL(I2), (CIJK(I,NK), NK=1,NOK)
   29 FORMAT (6H KMIN=,I2,5X,1H(,I1,A1,6H//CK//,I1,A1,2H)=,
     &  3X,6F15.8)
  299 CONTINUE
C
      RETURN
      END

      SUBROUTINE CHCKSH(I,NLT,KLS)
      if ( NLT.gt.KLS ) then
        print*,' shell ',i,' of this configuration is too complex<;'
        print*,' its number of terms is ',nlt,' which is larger than ',
     &   ' its maximum for this shell, which is ',KLS,'.',
     &   ' Probably the calculation can be done by rearranging',
     &   ' the shells, putting the most complex shell in the',
     &   ' first position.',
     &   ' See also Cowan''s input description page 28.'
      STOP ' Shell too complex. See output for more information.'
      endif
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE PLEV
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,BLANK*2
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      DIMENSION DUMCOM(KMX,KMX)
      EQUIVALENCE (DUMCOM,U1)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
      DIMENSION  DUM(1)
      DATA BLANK/'  '/
C
C     NOTE--IF EQUIV OF CFP2 AND CFGP2 CHANGED,
C    &      MAKE CORRESPONDING CHANGE IN PRK CODE, STATEMENT 540 TO 569
C
C     KS=8
C     kjp=1200
C     KLSC=210
C     KJK=100
      write(9,*)'plev called ',kk
      NTOTTJ(1)=0
      NDIFFT=0
      SUMX=0.0
      SJ4MN=1000.0
      SJ4MX=0.0
      do 50 I=1,KS
      NALSJP(1,I)=0
      NOTSJ1(I,1)=0
      NTOTJJ(I,1)=0
   50 NDIFFJ(I)=0
C
      JX=NSCONF(2,KK)
      do 229 J1=1,JX
      LL(NOSUBC)=LLIJK(NOSUBC,J1,KK)
      FLL(NOSUBC)=FLLIJK(NOSUBC,J1,KK)
      do 79 I=1,KS
      NOTS=NOTSJ1(I,J1)
C    &                         READ TERMS FROM DISK 72
      NI(I)=NIJK(I,J1,KK)
      NN=NI(I)
      NFULL=4.0*FLL(I)+2.0
      if (NN.GT.1.AND.NN.LT.NFULL-1) GO TO 70
      NOT=1
      NLT=1
      NLASTT(I)=NLT
      NOT1=NOTS+1
      NOTS=NOTS+NLT
      MULT(NOT1,I)=1
      LBCD(NOT1,I)=LSYMB(1)
      ALF(NOT1,I)=BLANK
      FL(NOT1,I) = 0
      S (NOT1,I) = 0
      if (NN.EQ.0.OR.NN.EQ.NFULL) GO TO 78
      MULT(NOT1,I)=2
      LBCD(NOT1,I)=LL(I)
      FL(NOT1,I)=FLL(I)
      S (NOT1,I)=0.5
      GO TO 78
   70 CALL LOCDSK(ID2,FLL(I),NI(I))
      if (I.EQ.1.AND.J1.LT.JX) NLT=MIN(NLT,NLT11)
      if (I.EQ.1.AND.JX.EQ.1 ) NLT=MIN(NLT,NLT11)
      NLASTT(I)=NLT
      NOT1=NOTS+1
      NOTS=NOTS+NLT
      READ (ID2) (MULT(M,I),LBCD(M,I),ALF(M,I),FL(M,I),S(M,I),
     &  M=NOT1,NOTS)
   78 NOTSJ1(I,J1+1)=NOTS
   79 CONTINUE
C
C    &     CALC PRELIMINARY LS TERMS
C
      M = NTOTTJ(J1)
      MB= MIN(M,KLSC)
      do 120 I1 = NOTSJ1(1,J1) + 1  , NOTSJ1(1,J1+1)
      do 120 I2 = NOTSJ1(2,J1) + 1  , NOTSJ1(2,J1+1)
      do 120 I3 = NOTSJ1(3,J1) + 1  , NOTSJ1(3,J1+1)
      do 120 I4 = NOTSJ1(4,J1) + 1  , NOTSJ1(4,J1+1)
      do 120 I5 = NOTSJ1(5,J1) + 1  , NOTSJ1(5,J1+1)
      do 120 I6 = NOTSJ1(6,J1) + 1  , NOTSJ1(6,J1+1)
      do 120 I7 = NOTSJ1(7,J1) + 1  , NOTSJ1(7,J1+1)
      do 120 I8 = NOTSJ1(8,J1) + 1  , NOTSJ1(8,J1+1)
      CALL LOOPPL ( I1,I2,I3,I4,I5,I6,I7,I8,
     &  J1,KLSC,KLS1,KS,NOSUBC,
     &  S, FL, NALSP, PscrL, PscrS, NCFGP, PC,  M,MB)
  120 CONTINUE

      NOTOTT = M
      L      = NTOTTJ(J1)+1
      NTOTTJ(J1+1)=M
      CALL WARNDIM(M,KLSC,'KLSC')
      if (IPLEV.gt.0) then
        WRITE (IW,*) 'LS TERMS'
        do 122 M=L,NOTOTT
  122     WRITE (IW,'(1x, 8(I5,I3,F5.1,F3.0))')
     &    (NCFGP(M), NALSP(M,I),PscrS(M,I),PscrL(M,I), I=1,NOSUBC)
      endif
C
C                         SORT ON DECREASING   25*scrS4+scrL4

C    All terms with the same scrS and scrL must be grouped after the
C    ordering. Terms with different aLS(i) but all other quantum
C    numbers equal must be ordered with increasing NaLSP; This
C    order is implicit in the loop structure above, and is retained
C    by SORT2 (implicitly).
C    These orders are required by PFGD.
C
      M1=NTOTTJ(J1+1)-NTOTTJ(J1)
      CALL SORT2(M1,0,PC(L),PCI,
     &           DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      do 127 I=1,NOSUBC
      CALL ORDER2(M1,PCI,PscrL(L,I))
      CALL ORDER2(M1,PCI,PscrS(L,I))
  127 CALL ORDER1(M1,PCI,NALSP(L,I))
C
      if (IPLEV.GT.0) THEN
        WRITE(IW,'(A/)') ' LS TERMS (SORTED)'
        do 131 M = L, NOTOTT
  131     WRITE (IW,'(1x, 8(I5,I3,F5.1,F3.0))')
     &    (NCFGP(M), NALSP(M,I),PscrS(M,I),PscrL(M,I), I=1,NOSUBC)
      endif
C
C    &                    CALC NO. OF TERMS OF EACH KIND
C
      K=NDIFFT
      N2=-100
      do 143 M=L,NOTOTT
      N1=N2
      N2=25.0*PscrS(M,NOSUBC)+PscrL(M,NOSUBC)
      if (N2-N1) 142,141,142
  141 NTRMK(K)=NTRMK(K)+1
      GO TO 143
  142 K=K+1
      NTRMK(K)=1
      SJ4MN = MIN(SJ4MN,
     &  max(conflimit(kk,j1,1),ABS(PscrS(M,NOSUBC)-PscrL(M,NOSUBC))) )
      SJ4MX = MAX(SJ4MX,
     &  min(conflimit(kk,j1,2),    PscrS(M,NOSUBC)+PscrL(M,NOSUBC) ) )
c      write(9,*) sj4mn,sj4mx,j1,kk,
c     &conflimit(kk,j1,1),conflimit(kk,j1,2),
c     &PscrS(M,NOSUBC),PscrL(M,NOSUBC)
  143 CONTINUE
c      write(9,*) sj4mn,sj4mx,sjn,sjx
      NDIFFT=K
      SJ4MX1=SJ4MX
  150 if (SJ4MN.EQ.SJ4MX) GO TO 170
      if (SJ4MN.GE.SJN) GO TO 160
      SJ4MN=SJ4MN+1.0
      GO TO 150
  160 if (SJ4MX.LE.SJX) GO TO 170
      SJ4MX=SJ4MX-1.0
      GO TO 160
  170 NscrJ4=SJ4MX-SJ4MN+1.0
c      write(9,*) sj4mn,sj4mx,sjn,sjx,nscrj4
C
C    &     CALC PRELIMINARY LEVELS FOR EACH SUBSHELL
C
      SUM=0.0
      do 225 I=1,NOSUBC
        M=NTOTJJ(I,J1)
        do 205 J = NOTSJ1(I,J1)+1 , NOTSJ1(I,J1+1)
        do 205 EJI = ABS( FL(J,I) - S(J,I) )  ,  FL(J,I) + S(J,I)
          M  = M + 1
          MB = MIN(M,KJP)
          NALSJP(MB,I) = J
          NCFGJP(MB,I) = J1
          PC(MB)   = -200 * EJI - 25 * S(J,I) - FL(J,I)
  205     PJ(MB,I) = EJI
      NOTOTJ(I)=M
      CALL WARNDIM(M,KJP,'KJP')
      NTOTJJ(I,J1+1)=M
      L=NTOTJJ(I,J1)+1
      if (IPLEV.gt.0) then
        WRITE (IW,'(/a,i1)') ' LEVELS OF SUBSHELL ', I
        do 211 N=L,M
          J=NALSJP(N,I)
  211     WRITE (IW,'(I13,I4,I5,A1,F5.1)')
     &    NCFGJP(N,I),NALSJP(N,I),MULT(J,I),LBCD(J,I),PJ(N,I)
      endif
C
C    &                    SORT ON DECREASING PJ
C
      M1=NTOTJJ(I,J1+1)-NTOTJJ(I,J1)
      CALL SORT2(M1,1,PC(L),PCI,PJ(L,I),
     &              DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      CALL ORDER1(M1,PCI,NALSJP(L,I))
C
      if (IPLEV.gt.0 ) then
        WRITE (IW,'(/a,i1,A)') ' LEVELS OF SUBSHELL ',I , ' (SORTED)'
        do 217 N=L,M
          J=NALSJP(N,I)
  217     WRITE (IW,'(I13,I4,I5,A1,F5.1)')
     &    NCFGJP(N,I),NALSJP(N,I),MULT(J,I),LBCD(J,I),PJ(N,I)
      endif
C
C    &                    CALC NO. OF LEVELS OF EACH PJ
C
      K  = NDIFFJ(I)+1
      KB = MIN(K,KJK)
      NJK(KB,I)=1
      L=L+1
      do 223 J=L,M
        if (PJ(J,I).eq.PJ(J-1,I)) then
          NJK(KB,I)=NJK(KB,I)+1
        else
          K=K+1
          KB=MIN(K,KJK)
          NJK(KB,I)=1
        endif
  223 continue
      NDIFFJ(I)=K
      CALL WARNDIM(K,KJK,'KJK')
  225 SUM=SUM+PJ(L-1,I)
      if (SUM.GT.SUMX) SUMX=SUM
  229 CONTINUE
C
      if (SUMX-SJ4MX1) 230,250,230
  230 WRITE (IW,23) SJ4MX1,SUMX
   23 FORMAT (///15H0MAXIMUM scrJ4=,F5.1,10H  FROM LS ,
     &  10HCALC, AND=,F5.1, 14H  FROM JJ CALC)
  250 if (NOSUBC-KS) 255,300,300
  255 N1=NOSUBC+1
      do 260 I=N1,KS
      do 260 J=1,JX
      NOTSJ1(I,J+1)=J
      NTOTJJ(I,J)=J
      NOTOTJ(I)=J
      NDIFFJ(I)=J
      NJK(J,I)=1
      NALSJP(J,I)=J
  260 PJ(J,I)=0.0
C
  300 IERROR=0
      MAXM=0
      MAXK=0
      do 310 I=1,KS
      MAXM=MAX(MAXM,NOTOTJ(I))
  310 MAXK=MAX(MAXK,NDIFFJ(I))
      if (NOTOTT.GT.KLSC.OR.NDIFFT.GT.KLSC) IERROR=7
      if (MAXM.GT.KJP.OR.MAXK.GT.KJK) IERROR=7
      WRITE(IW,*) ' PARAMETERS NEEDED: KLSC=',MAX(NOTOTT,NDIFFT)
     &           ,' KJP=',MAXM,' KJK=',MAXK
      WRITE (IW,30) NOTOTT,NDIFFT, (NOTOTJ(I),I=1,KS),(NDIFFJ(I),I=1,KS)
   30 FORMAT (///25H MAXIMUM SUBSCRIPT VALUES//
     &  39H NALSP(M,I), PscrL(M,I),   M=NOTOTT=   ,I5/
     &  17H NTRMK(K), LF(K),, 10X,12HK=NDIFFT=   ,I5/
     &  39H NALSJP(M,I), PJ(M,I),     M=NOTOTJ(I)=,8I5/
     &  10H NJK(K,I),,17X,12HK=NDIFFJ(I)=,8I5)
      JX=JX+1
      WRITE (IW,31) (NTOTTJ(J), J=1,JX)
   31 FORMAT (/8H0NTOTTJ=,20I6/(8X,20I6))
      WRITE (IW,32) (NTRMK(K), K=1,NDIFFT)
   32 FORMAT (/8H0 NTRMK=,20I6/(8X,20I6))
      if (IERROR.EQ.0) GO TO 400
      WRITE (IW,33)
   33 FORMAT (//25H***** DIMENSIONS EXCEEDED)
      STOP
  400 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE LOOPPL( I1,I2,I3,I4,I5,I6,I7,I8,
     &  J1,KLSC,KLS1,KS,NOSUBC,
     &  S, FL, NALSP, PscrL, PscrS, NCFGP, PC,  M,MB)
      DIMENSION S(KLS1,KS), FL(KLS1,KS), NALSP(KLSC,KS),
     &  PscrL(KLSC,KS), PscrS(KLSC,KS), NCFGP(KLSC), PC(KLSC)
      do 110 ES2 = ABS(S (I1,1)-S (I2,2)) , S (I1,1)+S (I2,2)
      do 110 EL2 = ABS(FL(I1,1)-FL(I2,2)) , FL(I1,1)+FL(I2,2)
      do 110 ES3 = ABS(ES2-S (I3,3)) , ES2+S (I3,3)
      do 110 EL3 = ABS(EL2-FL(I3,3)) , EL2+FL(I3,3)
      do 110 ES4 = ABS(ES3-S (I4,4)) , ES3+S (I4,4)
      do 110 EL4 = ABS(EL3-FL(I4,4)) , EL3+FL(I4,4)
      do 110 ES5 = ABS(ES4-S (I5,5)) , ES4+S (I5,5)
      do 110 EL5 = ABS(EL4-FL(I5,5)) , EL4+FL(I5,5)
      do 110 ES6 = ABS(ES5-S (I6,6)) , ES5+S (I6,6)
      do 110 EL6 = ABS(EL5-FL(I6,6)) , EL5+FL(I6,6)
      do 110 ES7 = ABS(ES6-S (I7,7)) , ES6+S (I7,7)
      do 110 EL7 = ABS(EL6-FL(I7,7)) , EL6+FL(I7,7)
      do 110 ES8 = ABS(ES7-S (I8,8)) , ES7+S (I8,8)
      do 110 EL8 = ABS(EL7-FL(I8,8)) , EL7+FL(I8,8)
      M  = M+1
      MB = MIN(M,KLSC)
      GO TO (108,107,106,105,104,103,102,101) NOSUBC
  101 NALSP(MB,8)=I8
      PscrS(MB,8)=ES8
      PscrL(MB,8)=EL8
  102 NALSP(MB,7)=I7
      PscrS(MB,7)=ES7
      PscrL(MB,7)=EL7
  103 NALSP(MB,6)=I6
      PscrS(MB,6)=ES6
      PscrL(MB,6)=EL6
  104 NALSP(MB,5)=I5
      PscrS(MB,5)=ES5
      PscrL(MB,5)=EL5
  105 NALSP(MB,4)=I4
      PscrS(MB,4)=ES4
      PscrL(MB,4)=EL4
  106 NALSP(MB,3)=I3
      PscrS(MB,3)=ES3
      PscrL(MB,3)=EL3
  107 NALSP(MB,2)=I2
      PscrS(MB,2)=ES2
      PscrL(MB,2)=EL2
  108 NALSP(MB,1)=I1
      PscrS(MB,1)=S (I1,1)
      PscrL(MB,1)=FL(I1,1)
      NCFGP(MB)  =J1
      PC(MB) = - ( 25 * PscrS(MB,NOSUBC) + PscrL(MB,NOSUBC) )
  110 CONTINUE
      END


C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE PFGD (NISUBC)
C
C   CALC PRELIMINARY SINGLE-CONFIGURATION MATRIX ELEMENTS
C
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ANAM,ALFDUM*2
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER*1 LL,LBCD,ALF*2,LLIJK,LHS1,LHS4,LBCDI
      COMMON /CHAR/ LBCDI(KLSI),LHS1(KLSC),LHS4(KLSC)
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      CHARACTER*8 ZE(8)  , BUGSTR*10
      DATA (ZE(I),I=1,8) /'ZETA 1','ZETA 2','ZETA 3','ZETA 4',
     &  'ZETA 5','ZETA 6','ZETA 7','ZETA 8'/
C
C
      J1X=NSCONF(2,KK)
      LX=NTOTTJ(J1X+1)
      do  97 L=1,LX
      MULS1(L)=2.0*PscrS(L,1)+1.0
      MULS4(L)=2.0*PscrS(L,NOSUBC)+1.0
      I=PscrL(L,1)+1.0
      LHS1(L)=LSYMB(I)
      I=PscrL(L,NOSUBC)+1.0
   97 LHS4(L)=LSYMB(I)
C
      NPAR=0
      NPARJO=0
      REWIND 20
C
      do 499 J1=1,J1X
      NPAR=NPAR+1
      PARNAM(NPAR,KK)=PARNAM(1,1)
      do 99 I=1,KS
      LL(I)=LLIJK(I,J1,KK)
      FLL(I)=FLLIJK(I,J1,KK)
   99 NI(I)=NIJK(I,J1,KK)
C
C    &     OBTAIN FK(I,I) FROM DISK 74 AND TERMS OF LI(NI) FROM DISK 72
C
      do 199 I=1,NOSUBC
C--------------------------------------------------------------------
      if (NI(I).LE.1.OR.I.GT.NISUBC) GO TO 199
C--------------------------------------------------------------------
      N=4.0*FLL(I)
      if (NI(I).GT.N) GO TO 199
      CALL LOCDSK(ID2,FLL(I),NI(I))
      READ (ID2) (MULSI(M),LBCDI(M),ALFDUM,A,A, M=1,NLT)
      CALL LOCDSK(ID4,FLL(I),NI(I))
      if (NSCONF(1,KK).GT.1) GO TO 115
      READ (ID4)
      GO TO 170
C
  115 READ (ID4) NDIFFI,(NTI(K),K=1,NDIFFI)
C
C    &     FIND CORRELATION BETWEEN TERMS OF CONF J1 AND TERMS OF LI(NI)
C
      NOPC=0
      NOPCB=MIN(NOPC,KPC)
      MX=0
      do 169 K=1,NDIFFT
      MN=MX+1
      MX=MX+NTRMK(K)
      if (MX.LE.NTOTTJ(J1)) GO TO 169
      if (MX.GT.NTOTTJ(J1+1)) GO TO 170
      do 159 M=MN,MX
      J=NALSP(M,I)
      NPI0=0
      MXI=0
      do 125 K1=1,NDIFFI
      MNI=MXI+1
      MXI=MXI+NTI(K1)
      if (MULT(J,I).NE.MULSI(MNI)) GO TO 125
      if (LBCD(J,I).EQ.LBCDI(MNI)) GO TO 130
  125 NPI0=NPI0+(NTI(K1)*(NTI(K1)+1))/2
  130 do 149 MP=MN,M
      JP=NALSP(MP,I)
      NOPC=NOPC+1
      NOPCB=MIN(NOPC,KPC)
      PC(NOPCB)=0.0
      ISER(NOPCB)=-7
      if (MULT(J,I).NE.MULT(JP,I)) GO TO 149
      if (LBCD(J,I).NE.LBCD(JP,I)) GO TO 149
      do 133 L=1,NOSUBC
      if (PscrL(M,L).NE.PscrL(MP,L)) GO TO 149
      if (PscrS(M,L).NE.PscrS(MP,L)) GO TO 149
      if (L.EQ.I) GO TO 133
      if (NALSP(M,L).NE.NALSP(MP,L)) GO TO 149
  133 CONTINUE
      NPI=NPI0
      do 139 MI=MNI,MXI
      do 135 MPI=MNI,MI
      NPI=NPI+1
      if (J-NOTSJ1(I,J1).NE.MI) GO TO 135
      if (JP-NOTSJ1(I,J1).NE.MPI) GO TO 135
      ISER(NOPCB)=NPI
      GO TO 149
  135 CONTINUE
  139 CONTINUE
  149 CONTINUE
  159 CONTINUE
  169 CONTINUE
      CALL WARNDIM(NOPC,KPC,'KPC')
C
C    &     WRITE PC ON DISK 20
C
  170 NPARI=NORCD-2
      if (IABG.EQ.0) NPARI=FLL(I)
      do 198 N=1,NPARI
      if (NSCONF(1,KK).GT.1) GO TO 175
      READ (ID4) KPAR,K,ANAM,CAVE,NOPC, (PC(J),J=1,NOPC)
      if (KPAR.EQ.1) GO TO 198
      GO TO 185
  175 READ (ID4) KPAR,K,ANAM,CAVE,NPI, (PCI(J),J=1,NPI)
      if (KPAR.EQ.1) GO TO 198
      do 180 J=1,NOPC
      if (ISER(J).LE.0) GO TO 180
      L=ISER(J)
      PC(J)=PCI(L)
  180 CONTINUE
C
  185      NPAR=NPAR+1
        if (K.NE.0) GO TO 186
        if (IABG) 198,198,188
  186   WRITE(ANAM,'(1HF,I1,1H(,2I1,1H))') K,I,I
  188   PARNAM(NPAR,KK)=ANAM
        WRITE (20) NPAR,KPAR,K,I,I,ANAM,CAVE,J1,NOPC,(PC(N1), N1=1,NOPC)
        if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 197
        WRITE (IW,16) ANAM,CAVE
   16   FORMAT (1H2,A8,5X,5HCAVE=,F12.7)
c        print*,anam
	print '(1x,a,3I1,a)', 'F',K,I,I,' = {'
        NOPC=0
        MX=0
        do 191 K=1,NDIFFT
        MN=MX+1
        MX=MX+NTRMK(K)
        if (MX.LE.NTOTTJ(J1)) GO TO 191
        if (MX.GT.NTOTTJ(J1+1)) GO TO 197
        do 190 M=MN,MX
        J=NALSP(M,I)
        N2=NOPC+1
        NOPC=N2+M-MN

c        print '(1x,i4,a1,f12.6)', MULS4(M), LHS4(M), PC(NOPC)
        if(nopc.eq.1) then 
          print '(3x,f12.6)',  PC(NOPC)
	else
          print '(2x,a1,f12.6)', ',',  PC(NOPC)
	endif

  190   WRITE (IW,17) NCFGP(J),NOPC,MULT(J,I),LBCD(J,I),MULS4(M),
     &  LHS4(M), (PC(N1), N1=N2,NOPC)
   17   FORMAT (2I5,5H    (,I1,A1,1H),I4,A1, 10F10.5/(25X, 10F10.5))


  191   CONTINUE
        print'(1x,a/)', '};'
  197   CALL CLOK(TIME)
        DELT=TIME-TIME0
        WRITE (IW,18) ANAM,NOPC,CAVE,DELT, (LL(L),NI(L), L=1,NOSUBC)
   18   FORMAT (1X,A8,5X,3HPC(,I7,1H),5X
     &  ,5HCAVE=,F12.7,18X,5HDELT=,F8.2,
     &  4H MIN,7X, 8(A1,I2,3X))
C      endif
  198 CONTINUE
  199 CONTINUE
C
C    &     OBTAIN ZETA(I) FROM DISK 74
C
      do 299 I=1,NOSUBC
C----------------------------------------------------------------------
      if (NI(I).LE.0.OR.I.GT.NISUBC) GO TO 299
C----------------------------------------------------------------------
      if (FLL(I).LE.0.0) GO TO 299
      NN=4.0*FLL(I)+2.0
      if (NI(I).GE.NN) GO TO 299
      if (NI(I).GT.1.AND.NI(I).LT.NN-1) GO TO 205
      NORCD=2
      M=1
      NTI(1)=1
      N1=2
      NALSJI(1)=1
      NALSJI(2)=1
      PJI(1)=FLL(I)+0.5
      PJI(2)=FLL(I)-0.5
      NDIFJI=2
      NTI(1)=1
      NTI(2)=1
      GO TO 220
  205 CALL LOCDSK(ID4,FLL(I),NI(I))
      if (NSCONF(1,KK).GT.1) GO TO 215
      READ (ID4)
      GO TO 270
C
  215 READ (ID4) M,(NTI(K),K=1,M), N1,(NALSJI(K),PJI(K),K=1,N1),
     &  NDIFJI,(NTI(K),K=1,NDIFJI)
C
C    &     FIND CORREL. BETWEEN LEVELS IN CONF J1 AND LEVELS OF LI(NI)
C
  220 NOPC=0
      MX=0
      KX=NDIFFJ(I)
C            if (I.GT.2) GOTO 9873
      do 269 K=1,KX
      MN=MX+1
      MX=MX+NJK(K,I)
      if (MX.LE.NTOTJJ(I,J1)) GO TO 269
      if (MX.GT.NTOTJJ(I,J1+1)) GO TO 270
      do 259 M=MN,MX
      J=NALSJP(M,I)-NOTSJ1(I,J1)
      NPI0=0
      MXI=0
      do 225 KI=1,NDIFJI
      MNI=MXI+1
      MXI=MXI+NTI(KI)
      if (PJ(M,I).EQ.PJI(MNI)) GO TO 230
  225 NPI0=NPI0+(NTI(KI)*(NTI(KI)+1))/2
  230 do 249 MP=MN,M
      JP=NALSJP(MP,I)-NOTSJ1(I,J1)
      NOPC=NOPC+1
      NOPCB=MIN(NOPC,KPC)
      PC(NOPCB)=0.0
      ISER(NOPCB)=-7
      if (PJ(M,I).NE.PJ(MP,I)) GO TO 249
      NPI=NPI0
      do 239 MI=MNI,MXI
      do 235 MPI=MNI,MI
      NPI=NPI+1
      if (J.NE.NALSJI(MI)) GO TO 235
      if (JP.NE.NALSJI(MPI)) GO TO 235
      ISER(NOPCB)=NPI
      GO TO 249
  235 CONTINUE
  239 CONTINUE
  249 CONTINUE
  259 CONTINUE
  269 CONTINUE
C 9873         CONTINUE
      CALL WARNDIM(NOPC,KPC,'KPC')
C
C    &     WRITE PC ON DISK 20
C
  270 NPARI=NORCD-1
      do 289 N=1,NPARI
      if (NI(I).GT.1.AND.NI(I).LT.NN-1) GO TO 271
      A=-0.5
      if (NI(I).EQ.1) A=0.5
      ERAS1=A*FLL(I)
      ERAS2=-A*(FLL(I)+1.0)
      KPAR=1
      ANAM='ZETA'
      K=0
      CAVE=0.0
      if (NSCONF(1,KK).GT.1) GO TO 1270
      PC(1)=ERAS1
      PC(2)=ERAS2
      GO TO 285
 1270 PCI(1)=ERAS1
      PCI(2)=ERAS2
      GO TO 277
  271 if (N.EQ.NPARI) GO TO 272
      READ (ID4)
      GO TO 289
  272 if (NSCONF(1,KK).GT.1) GO TO 275
      READ (ID4) KPAR,K,ANAM,CAVE,NOPC, (PC(J),J=1,NOPC)
      GO TO 285
  275 READ (ID4) KPAR,K,ANAM,CAVE,NPI, (PCI(J),J=1,NPI)
  277 do 280 J=1,NOPC
      if (ISER(J).LE.0) GO TO 280
      L=ISER(J)
      PC(J)=PCI(L)
  280 CONTINUE
C  285 if (I.LE.2) THEN
  285      NPAR=NPAR+1
        ANAM=ZE(I)
        PARNAM(NPAR,KK)=ANAM
        WRITE (20) NPAR,KPAR,K,I,I,ANAM,CAVE,J1,NOPC,(PC(N1), N1=1,NOPC)
        if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 288
        WRITE (IW,16) ANAM,CAVE
        NOPC=0
        MX=0
        KX=NDIFFJ(I)
        do 287 K=1,KX
        MN=MX+1
        MX=MX+NJK(K,I)
        if (MX.LE.NTOTJJ(I,J1)) GO TO 287
        if (MX.GT.NTOTJJ(I,J1+1)) GO TO 288
        do 286 M=MN,MX
        J=NALSJP(M,I)
        N2=NOPC+1
        NOPC=N2+M-MN
        CALL WARNDIM(NOPC,KPC,'KPC')
  286   WRITE (IW,28) NCFGJP(M,I),NOPC,MULT(J,I),LBCD(J,I),PJ(M,I),
     &  (PC(N1),N1=N2,NOPC)
   28   FORMAT (3I5,A1,F4.1,10F11.6/(22X,10F11.6))
  287   CONTINUE
  288   CALL CLOK(TIME)
        DELT=TIME-TIME0
        WRITE (IW,18) ANAM,NOPC,CAVE,DELT, (LL(L),NI(L), L=1,NOSUBC)
C      endif
  289 CONTINUE
  299 CONTINUE
C
C    &     CALC FK(I,J)
C
      if (NOSUBC.LE.1) GO TO 498
      IRHO=0
C    &                             READ U,V FROM DISK 73
      do 7304 I=1,NOSUBC
C-----------------------------------------------------------------
      if (NI(I).LE.1.OR.I.GT.NISUBC) GO TO 7304
C-----------------------------------------------------------------
      N=4.0*FLL(I)+2.0
      if (NI(I).EQ.N) GO TO 7304
      CALL LOCDSK(ID3,FLL(I),NI(I))
      NOR=NORCD/2
      do 303 K=1,NOR
      GO TO (1303,2303,3303,4303,5303,6303), I
c1303 READ (ID3) N, ((U1(II,JJ,K),II=1,NLT),JJ=1,NLT)
 1303 call  getsparse ((id3), u1(1,1,k), nlt, nlt, klsi,.false.)
      GO TO 303
c2303 READ (ID3) N, ((U2(II,JJ,K),II=1,NLT),JJ=1,NLT)
 2303 call  getsparse ((id3), u2(1,1,k), nlt, nlt, 17,.false.)
      GO TO 303
c3303 READ (ID3) N, ((U3(II,JJ,K),II=1,NLT),JJ=1,NLT)
 3303 call  getsparse ((id3), u3(1,1,k), nlt, nlt, 8,.false.)
      GO TO 303
c4303 READ (ID3) N, ((U4(II,JJ,K),II=1,NLT),JJ=1,NLT)
 4303 call  getsparse ((id3), u4(1,1,k), nlt, nlt, 8,.false.)
      GO TO 303
c5303 READ (ID3) N, ((U5(II,JJ,K),II=1,NLT),JJ=1,NLT)
 5303 call  getsparse ((id3), u5(1,1,k), nlt, nlt, 8,.false.)
      GO TO 303
c6303 READ (ID3) N, ((U6(II,JJ,K),II=1,NLT),JJ=1,NLT)
 6303 call  getsparse ((id3), u6(1,1,k), nlt, nlt, 8,.false.)
  303 CONTINUE
      do 304 K=1,NOR
      GO TO (1304,2304,3304,4304,5304,6304), I
c1304 READ (ID3) N, ((V1(II,JJ,K),II=1,NLT),JJ=1,NLT)
 1304 call  getsparse ((id3), v1(1,1,k), nlt, nlt, klsi,.false.)
      GO TO 304
c2304 READ (ID3) N, ((V2(II,JJ,K),II=1,NLT),JJ=1,NLT)
 2304 call  getsparse ((id3), v2(1,1,k), nlt, nlt, 17,.false.)
      GO TO 304
c3304 READ (ID3) N, ((V3(II,JJ,K),II=1,NLT),JJ=1,NLT)
 3304 call  getsparse ((id3), v3(1,1,k), nlt, nlt, 8,.false.)
      GO TO 304
c4304 READ (ID3) N, ((V4(II,JJ,K),II=1,NLT),JJ=1,NLT)
 4304 call  getsparse ((id3), v4(1,1,k), nlt, nlt, 8,.false.)
      GO TO 304
c5304 READ (ID3) N, ((V5(II,JJ,K),II=1,NLT),JJ=1,NLT)
 5304 call  getsparse ((id3), v5(1,1,k), nlt, nlt, 8,.false.)
      GO TO 304
c6304 READ (ID3) N, ((V6(II,JJ,K),II=1,NLT),JJ=1,NLT)
 6304 call  getsparse ((id3), v6(1,1,k), nlt, nlt, 8,.false.)
  304 CONTINUE
 7304 CONTINUE
C
      do 399 I=1,NOSUBC-1
      NII=NI(I)
C--------------------------------------------------------------------
      if (NI(I).LE.0.OR.I.GT.NISUBC) GOTO 399
C--------------------------------------------------------------------
      if (FLL(I)) 399,399,307
  307 N=4.0*FLL(I)+2.0
      if (NI(I)-N) 308,399,399
  308 I1=I+1
      do 389 J=I1,NOSUBC
      NIJ=NI(J)
C-------------------------------------------------------------------
      if (NI(J).LE.0.OR.J.GT.NISUBC) GOTO 389
C-------------------------------------------------------------------
      if (FLL(J)) 389,389,311
  311 N=4.0*FLL(J)+2.0
      if (NI(J)-N) 312,389,389
  312 if (FLL(I)-FLL(J)) 313,314,314
  313 KMAX=2.0*FLL(I)
      GO TO 315
  314 KMAX=2.0*FLL(J)
  315 do 385 K=1,KMAX
      if (IABG.NE.2.AND.IABG.NE.4.AND.MOD(K,2).NE.0) GO TO 385
      if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 320
      WRITE (IW,31)
   31 FORMAT (1H2)
      WRITE (IW,32) K,I,J
   32 FORMAT (2H F,I1,1H(,I1,1H,,I1,1H)//)
  320 FK=K
      CIJ=CIJKF(FLL(I),FLL(I),FK)*CIJKF(FLL(J),FLL(J),FK)
      if (MOD(K,2).NE.0) CIJ=1.0
C
      NOPC=0
      NOPCB=0
      MMAX=0
C      if (J.GT.2.OR.I.GT.1) GOTO 9876
      do 383 L=1,NDIFFT
      MMIN=MMAX+1
      MMAX=MMAX+NTRMK(L)
      if (NCFGP(MMAX).NE.J1) GO TO 383
      NOTSI=NOTSJ1(I,J1)
      NOTSJ=NOTSJ1(J,J1)
      do 382 M=MMIN,MMAX
      JI=NALSP(M,I)
      JJ=NALSP(M,J)
      KI=JI-NOTSI
      KJ=JJ-NOTSJ
      do 381 MP=MMIN,M
      JPI=NALSP(MP,I)
      JPJ=NALSP(MP,J)
      KPI=JPI-NOTSI
      KPJ=JPJ-NOTSJ
      NOPC=NOPC+1
      NOPCB=MIN(NOPC,KPC)
      PC(NOPCB)=0.0
      do 341 IP=1,NOSUBC
      if (ip.gt.nosubc) print*,'compiler bug on CDC. See PFGD',
     &' loop 341'
      if (PscrS(M,IP)-PscrS(MP,IP)) 381,331,381
  331 if (IP-I) 339,332,335
  332 if (S(JI,I)-S(JPI,I)) 381,341,381
  335 if (IP-J) 340,336,339
  336 if (PscrL(M,IP)-PscrL(MP,IP)) 381,337,381
  337 if (S(JJ,J)-S(JPJ,J)) 381,341,381
  339 if (PscrL(M,IP)-PscrL(MP,IP)) 381,340,381
  340 if (NALSP(M,IP)-NALSP(MP,IP)) 381,341,381
C-- THE NEXT LINE IS NEEDED ON CYBER962 BECAUSE OF COMPILER BUG
  341 WRITE(BUGSTR,'(I2)') IP
C 341 CONTINUE
      do 350 III=1,2
      MM=M
      if (III.EQ.2) MM=MP
      do 350 IP=1,NOSUBC
      II=NALSP(MM,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(MM,IP)
  350 TSCS(III,IP)=PscrS(MM,IP)
      TK=FK
      CALL RDIJ(I,J)
C
      PC(NOPCB)=A*CIJ
  381 CONTINUE
      if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 382
      N2=NOPCB-M+MMIN
      WRITE (IW,17) NCFGP(M),NOPCB,MULS1(M),LHS1(M),MULS4(M),
     &  LHS4(M), (PC(N1), N1=N2,NOPCB)
c      print '(1x,i4,a1,f12.6)', MULS4(M), LHS4(M), PC(NOPC)

        if(nopc.eq.1) then 
	  print '(1x,a,3I1,a)', 'F',K,I,j,' = {'
          print '(3x,f12.6)',  PC(NOPC)
	else
          print '(2x,a1,f12.6)', ',',  PC(NOPC)
	endif

  382 CONTINUE
  383 CONTINUE

      if (.not.(ICPC.EQ.0.OR.ICPC.EQ.2)) print'(1x,a/)', '};'

C 9876 CONTINUE
C      if (I.LE.2.AND.J.LE.2) THEN
        NPAR=NPAR+1
        KPAR=-1
        CAVE=0.0
        WRITE(ANAM,'(1HF,I1,1H(,2I1,1H))') K,I,J
        PARNAM(NPAR,KK)=ANAM
        CALL WARNDIM(NOPC,KPC,'KPC')
        WRITE (20) NPAR,KPAR,K,I,J,ANAM,CAVE,J1,NOPC,(PC(N1), N1=1,NOPC)
        CALL CLOK(TIME)
        DELT=TIME-TIME0
        WRITE (IW,18) ANAM,NOPC,CAVE,DELT, (LL(L),NI(L), L=1,NOSUBC)
C      endif
  385 CONTINUE
  389 CONTINUE
  399 CONTINUE
C
C    &     CALC GK(I,J)
C
      do 497 I=1,NOSUBC-1
      NII=NI(I)
C-----------------------------------------------------------------
      if (NI(I).LE.0.OR.I.GT.NISUBC) GOTO 497
C-----------------------------------------------------------------
      N=4.0*FLL(I)+2.0
      if (NI(I)-N) 403,497,497
  403 I1=I+1
      do 489 J=I1,NOSUBC
      NIJ=NI(J)
C-----------------------------------------------------------------
      if (NI(J).LE.0.OR.J.GT.NISUBC) GOTO 489
C-----------------------------------------------------------------
      N=4.0*FLL(J)+2.0
      if (NI(J)-N) 406,489,489
  406 KMAX=FLL(I)+FLL(J)
      KMIN=ABS(FLL(I)-FLL(J))
      KDMIN=0
      IRP1MX=KMAX-KMIN+1
      MN=I+1
      MX=J-1
      do 485 K=KMIN,KMAX
      if (IABG.NE.2.AND.IABG.NE.4.AND.MOD(K-KMIN,2).NE.0) GO TO 485
      FK=K
      CIJ=(CIJKF(FLL(I),FLL(J),FK))**2
      A=NI(I)*NI(J)
      CAVE=A*S3J0SQ(FLL(I),FK,FLL(J))/2.0
      LPL=FLL(I)+FLL(J)+FK
      if (MOD(LPL,2).EQ.0) GO TO 407
      CIJ=0.5*((-1.0)**K)
      CAVE=-CIJ*A/((2.0*FLL(I)+1.0)*(2.0*FLL(J)+1.0))
      CIJ=2.0*CIJ
  407 if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 410
      WRITE (IW,31)
      WRITE (IW,40) K,I,J,CAVE
   40 FORMAT (2H G,I1,1H(,I1,1H,,I1,1H),6X,5HCAVE=,F12.7//)
C
  410 NOPC=0
      MMAX=0
C      if (J.GT.2.OR.I.GT.1) GOTO 9875
      do 483 L=1,NDIFFT
      MMIN=MMAX+1
      MMAX=MMAX+NTRMK(L)
      if (NCFGP(MMAX).NE.J1) GO TO 483
      NOTSI=NOTSJ1(I,J1)
      NOTSJ=NOTSJ1(J,J1)
      do 482 M=MMIN,MMAX
      JI=NALSP(M,I)
      JJ=NALSP(M,J)
      KI=JI-NOTSI
      KJ=JJ-NOTSJ
      do 481 MP=MMIN,M
      JPI=NALSP(MP,I)
      JPJ=NALSP(MP,J)
      KPI=JPI-NOTSI
      KPJ=JPJ-NOTSJ
      NOPC=NOPC+1
      PC(NOPC)=0.0
      do 417 IP=1,NOSUBC
      if (IP-I) 414,417,413
  412 if (NALSP(M,IP)-NALSP(MP,IP)) 481,417,481
  413 if (IP-J) 412,415,414
  414 if (NALSP(M,IP)-NALSP(MP,IP)) 481,415,481
  415 if (PscrL(M,IP)-PscrL(MP,IP)) 481,416,481
  416 if (PscrS(M,IP)-PscrS(MP,IP)) 481,417,481
  417 CONTINUE
C
      do 450 III=1,2
      MM=M
      if (III.EQ.2) MM=MP
      do 450 IP=1,NOSUBC
      II=NALSP(MM,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(MM,IP)
  450 TSCS(III,IP)=PscrS(MM,IP)
      FLLRHO=FLL(I)
      FLLRHOP=FLL(I)
      FLLSIG=FLL(J)
      FLLSIGP=FLL(J)
      TK=FK
      CALL REIJ(I,J)
C
      PC(NOPC)=CIJ*RSUM
      if (M.NE.MP) GO TO 481
      PC(NOPC)=PC(NOPC)+CAVE
  481 CONTINUE
C
      if (ICPC.EQ.0.OR.ICPC.EQ.2) GO TO 482
      N2=NOPC-M+MMIN
      WRITE (IW,17) NCFGP(M),NOPC,MULS1(M),LHS1(M),MULS4(M),
     &  LHS4(M), (PC(N1), N1=N2,NOPC)
c       print '(1x,i4,a1,f12.6)', MULS4(M), LHS4(M), PC(NOPC)

        if(nopc.eq.1) then 
	  print '(1x,a,3I1,a)', 'G',K,I,j,' = {'
          print '(3x,f12.6)',  PC(NOPC)
	else
          print '(2x,a1,f12.6)', ',',  PC(NOPC)
	endif

  482 CONTINUE
  483 CONTINUE
       if (.not.(ICPC.EQ.0.OR.ICPC.EQ.2)) print'(1x,a/)', '};'

C 9875 CONTINUE
C      if (I.LE.2.AND.J.LE.2) THEN
        NPAR=NPAR+1
        KPAR=0
        WRITE(ANAM,'(A1,I1,A1,2I1,A1)') 'G',K,'(',I,J,')'
        PARNAM(NPAR,KK)=ANAM
        WRITE (20) NPAR,KPAR,K,I,J,ANAM,CAVE,J1,NOPC,(PC(N1), N1=1,NOPC)
        CALL CLOK(TIME)
        DELT=TIME-TIME0
        WRITE (IW,18) ANAM,NOPC,CAVE,DELT, (LL(L),NI(L), L=1,NOSUBC)
C      endif
  485 CONTINUE
  489 CONTINUE
  497 CONTINUE
C
  498 NPARJ(J1,J1)=NPAR-NPARJO
      NPARJO=NPAR
  499 CONTINUE
C
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CHCKCFP(NLT,maxnlt,nlp,maxnlp,k,maxk,name)
      character*(*) name
      if ( NLT.gt.maxnlt.or.nlp.gt.maxnlp.or.k.gt.maxk) then
        print*,'Maximum dimensions of array ',name,' are ',
     &  maxnlt,maxnlp,maxk, '. Requested were ',
     &     nlt,   nlp,   k
        print*,' These configurations are too complex<;',
     &   ' for the calculation of the configuration interactions.',
     &   ' Probably the calculation can be done by rearranging',
     &   ' the shells, putting the most complex shell in the',
     &   ' first position.',
     &   ' See also Cowan''s input description page 28.'
      STOP ' Shell too complex. See output for more information.'
      endif
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#
      subroutine printprk(
     &  pc, NTOTTJ,j1a,j1b, PscrL, PscrS, NOSUBC, name
     &)

      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER,KLAM=3000)

      parameter (kgamma=1000)
      dimension pc(*), NTOTTJ(*), PscrL(KLSC,KS), PscrS(KLSC,KS)
      dimension gamma(0:kgamma)
      character*(*) name
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      data itimes/0/
      itimes = itimes+1

      lmin  = NTOTTJ(J1A)+1
      lmax  = NTOTTJ(J1A+1)
      lpmin = NTOTTJ(J1B)+1
      lpmax = NTOTTJ(J1B+1)
      CALL WARNDIM(lmax-lmin,kgamma,'kgamma in printprk')

c      print '(a,a,i3,a,i3)'
c     &  , name,' from conf ',j1b,' summed over conf ',j1a
        do 10 L= lmin, lmax
   10   gamma(l - lmin) = 0
	 
      NOPC=0
      do 20 LP = lpmin, lpmax
        gamlp = 0
        do 30 L = lmin, lmax
          if ( (PscrL(L,NOSUBC).eq.PscrL(LP,NOSUBC))
     &    .and.(PscrS(L,NOSUBC).eq.PscrS(LP,NOSUBC)) )
     &    then
            NOPC= NOPC+1
            gamlp = gamlp + pc(nopc)**2
            gamma(l-lmin) = 
     &      gamma(l-lmin) + pc(nopc)**2
          endif
   30   continue
c        print '(1x,i4,i3,a1,f12.6)'
c     &   ,lp,int(2*PscrS(Lp,NOSUBC)+1), lsymb(PscrL(Lp,NOSUBC)+1)
c     &   ,gamlp
   20 continue
c      print*

      print '(1x,a,i1,a /3x,f12.6)'
     & ,'R',itimes, ' = {',gamma(0)
      do 40 L= lmin+1, lmax
        print '(2x,a,f12.6)'
     &   ,',', gamma(l-lmin)
   40 continue
      print'(1x,a/)','};'

c      print '(a,a,i3,a,i3)'
c     &  , name,' from conf ',j1a,' summed over conf ',j1b
c      do 40 L= lmin, lmax
c        print '(i4,i3,a1,f12.6)'
c     &   ,l ,int(2*PscrS(L,NOSUBC)+1), lsymb(PscrL(L,NOSUBC)+1)
c     &   ,gamma(l-lmin)
c   40 continue
c      print*

      end


C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE PRK
C
C          CALC PRELIMINARY CONFIGURATION-INTERACTION MATRIX ELEMENTS
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER,KLAM=3000)
      PARAMETER (KCC=KISER/4, KCCC=100000)
      DIMENSION NOPCCC(KCCC) , DUM(1)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ANAM
      CHARACTER   CHDUM1*1, CHDUM2*2, TYPE*1
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1, line*80
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON/C3LC2/ CFGP1(KLSI,KLSI,7), PC(KPC)
      COMMON/C3LC3/ V1(KLSI,KLSI,9)
      COMMON/C3LC4/ CC(KCC,8), NPARJ(KC,KC)

      CHARACTER*1   LHS1,LHS4,LBCDI
      COMMON /CHAR/ LBCDI(KLSI),LHS1(KLSC),LHS4(KLSC)
      COMMON        U1(KLSI,KLSI,9)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
      DIMENSION INTEST(80)
C
C
C     NKTOTX = LOCF(CCC) - LOCF(CIJR)   ( = DIMENSION OF CIJR )
      NKTOTX=8
C
      NPARJO=NPAR
      J1X=NSCONF(2,KK)
      if (J1X.LE.1) GO TO 900
      if(cigiven().eq.0) then
        do 400 I=1,80
  400     INTEST(I)=0
        if (NSCONF(3,KK).eq.0) then
          call getline(line)
          read (line,'(80I1)') (INTEST(I),I=1,80)
        endif
      endif
C
C
C
C
C          CALCULATE RK(I,J)
C
      NOCCX=0
      JTEST=0
      do 799 J1A=1,J1X-1
      do 799 J1B=J1A+1,J1X
      JTEST=JTEST+1
      NPARJ(J1A,J1B)=0
      
      if(cigiven().eq.1) then
        if( cispecified(kk,j1a,j1b) .eq. 0 ) goto 799
      else
        if (NSCONF(3,KK).EQ.0.AND.INTEST(JTEST).EQ.0) GO TO 799
        if (NSCONF(3,KK).GT.0.AND.J1B.GT.J1A+NSCONF(3,KK)-1) GO TO 799
        if (NSCONF(3,KK).EQ.-9.AND.(J1A.EQ.1.OR.J1B.EQ.J1A+1)) GO TO 501
        if (NSCONF(3,KK).EQ.-9) GO TO 799
        if (NSCONF(3,KK).EQ.-8) GO TO 501
        if (NSCONF(3,KK).LT.0.AND.J1A.GT.-NSCONF(3,KK)) GO TO 799
      endif
      
  501 do 798 IRHO=1,NOSUBC
      IRS(1)=IRHO
      do 798 ISIG=IRHO,NOSUBC
      IRS(2)=ISIG
      do 798 IRHOP=1,NOSUBC
      IRS(3)=IRHOP
      do 797 ISIGP=IRHOP,NOSUBC
      IRS(4)=ISIGP
      FLLSIG=FLLIJK(ISIG,J1A,KK)
      FLLSIGP=FLLIJK(ISIGP,J1B,KK)
C              CHECK WHETHER IRHO...ISIGP IS A POSSIBLE SET OF ELECTRONS
      if (ISIG.EQ.NOSUBC.OR.ISIGP.EQ.NOSUBC) GO TO 1501
      if (NIJK(NOSUBC,J1A,KK).EQ.1.AND.NIJK(NOSUBC,J1B,KK).EQ.1.AND.
     &  FLLIJK(NOSUBC,J1A,KK).NE.FLLIJK(NOSUBC,J1B,KK)) GO TO 797
 1501 if (IRHO.NE.IRHOP.OR.ISIG.NE.ISIGP) GO TO 503
      NN=0
      do 502 I=ISIG,NOSUBC
  502 NN=NN+NIJK(I,J1A,KK)+NIJK(I,J1B,KK)
      if (NN.GT.2) GO TO 797
  503 do 504 I=1,NOSUBC
      NIJKP(I,1)=NIJK(I,J1A,KK)
  504 NIJKP(I,2)=NIJK(I,J1B,KK)
      NIJKP(IRHO,1)=NIJKP(IRHO,1)-1
      NIJKP(ISIG,1)=NIJKP(ISIG,1)-1
      NIJKP(IRHOP,2)=NIJKP(IRHOP,2)-1
      NIJKP(ISIGP,2)=NIJKP(ISIGP,2)-1
      do 505 I=1,NOSUBC
      if (NIJKP(I,1).NE.NIJKP(I,2)) GO TO 797
  505 if (NIJKP(I,1).LT.0) GO TO 797
C
      KDMIN=MAX1(ABS(FLL(IRHO)-FLL(IRHOP)),ABS(FLLSIG-FLLSIGP))
      KDMAX=MIN1(ABS(FLL(IRHO)+FLL(IRHOP)),ABS(FLLSIG+FLLSIGP))
      NKD=(KDMAX-KDMIN)/2+1
      KEMIN=MAX1(ABS(FLL(IRHO)-FLLSIGP),ABS(FLLSIG-FLL(IRHOP)))
      NKE=NKD
      if (IRHO.EQ.ISIG.OR.IRHOP.EQ.ISIGP) NKE=0
      if (NKD.LE.0) GO TO 797
      IRP1MX=KDMAX-KDMIN+1
      NKTOT=NKD+NKE
      CALL WARNDIM (NKTOT, NKTOTX, 'NKTOTX')
      FK=KDMIN-2
      do 507 NN=1,NKD
      FK=FK+2.0
  507 CIJR(NN)=CIJKF(FLL(IRHO),FLL(IRHOP),FK)
     &  *CIJKF(FLLSIG,FLLSIGP,FK)
      if (NKE.LE.0) GO TO 509
      FK=KEMIN-2
      N1=NKD+1
      do 508 NN=N1,NKTOT
      FK=FK+2.0
  508 CIJR(NN)=CIJKF(FLL(IRHO),FLLSIGP,FK)
     &  *CIJKF(FLL(IRHOP),FLLSIG,FK)
  509 CONTINUE
C
      do 511 I=1,4
  511 PC(I)=IRS(I)
      CALL SORT2(4,0,PC,PC(5),
     &         DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      IN=PC(1)
      IX=PC(2)
      JN=PC(3)
      JX=PC(4)
C
C          DETERMINE CLASS OF CONFIGURATION INTERACTION
C
      I=1
      J=3
      if (IRHO.LE.IRHOP) GO TO 521
      I=3
      J=1
  521 NEQP1=1
      do 522 K=1,3
      KP1=K+1
      do 522 L=KP1,4
  522 if (IRS(K).EQ.IRS(L)) NEQP1=NEQP1+1
      ICLASS=0
      GO TO (526,525,524,523), NEQP1
  523 ICLASS=1
      GO TO 527
  524 ICLASS=2
      if (IRHO.EQ.IRHOP) ICLASS=11
      if (ICLASS.EQ.11.AND.FLLSIG.NE.FLLSIGP) ICLASS=6
      GO TO 527
  525 if (IRS(I).EQ.IRS(I+1).AND.IRS(I).LT.IRS(J)) ICLASS=3
      if (IRS(J).EQ.IRS(J+1).AND.IRS(J).LT.IRS(I+1)) ICLASS=4
      if (IRS(J).EQ.IRS(J+1).AND.IRS(J).GT.IRS(I+1)) ICLASS=5
      if (IRS(I).EQ.IRS(J)) ICLASS=6
      if (IRS(I+1).EQ.IRS(J)) ICLASS=7
      if (IRS(I+1).EQ.IRS(J+1)) ICLASS=8
      if (ICLASS.EQ.8.AND.FLLSIG.NE.FLLSIGP) ICLASS=10
      GO TO 527
  526 if (IRS(I+1).LT.IRS(J)) ICLASS=9
      if (IRS(J).LT.IRS(I+1)) ICLASS=10
C
C          DETERMINE WHETHER CONFIGS ARE TO BE TRANSPOSED (ITRNSP=2)
C
  527 ITRNSP=1
      if (ICLASS.EQ.2.AND.IRHO.LT.IRHOP) GO TO 530
      if (IRHOP.EQ.ISIGP) GO TO 528
      if (IRHO.EQ.ISIG) GO TO 530
      if (IRHO.LT.IRHOP) GO TO 530
      if (IRHO.EQ.IRHOP.AND.ISIG.LE.ISIGP) GO TO 530
  528 ITRNSP=2
C
C          READ U,V FROM DISK 73
C
  530 J=0
      if (ISIG.EQ.IRHOP.OR.ISIG.EQ.ISIGP) J=ISIG
      if (IRHO.EQ.IRHOP.OR.IRHO.EQ.ISIGP) J=IRHO
      if (J.EQ.0) GO TO 540
      NJ=MIN0(NIJK(J,J1A,KK),NIJK(J,J1B,KK))
      NII=NJ
      NIJ=NJ
      if (NJ.LE.1) GO TO 540
      NFULL=4.0*FLL(J)+2.0
      if (NJ.EQ.NFULL) GO TO 797
      CALL LOCDSK(ID3,FLL(J),NJ)
      NOR=NORCD/2
      do 533 K=1,NOR
c 533   READ (ID3) N, ((U1(II,JJ,K),II=1,NLT), JJ=1,NLT)
  533   call  getsparse ((id3), u1(1,1,k), nlt, nlt, klsi,.false.)
      do 534 K=1,NOR
c 534   READ (ID3) N, ((V1(II,JJ,K),II=1,NLT), JJ=1,NLT)
  534   call  getsparse ((id3), v1(1,1,k), nlt, nlt, klsi,.false.)

C
C          READ CFP OR CFGP FROM DISK 72
C
  540 do 565 I=1,NOSUBC
      NDEL(I)=IABS(NIJK(I,J1A,KK)-NIJK(I,J1B,KK))
      if (NDEL(I).EQ.0.OR.NDEL(I).GT.2) GO TO 565
      IFL(I)=MAX0(NIJK(I,J1A,KK),NIJK(I,J1B,KK))
      if (NDEL(I).EQ.1.AND.IFL(I).LE.2) GO TO 565
      CALL LOCDSK(ID2,FLL(I),IFL(I))
      READ (ID2) (A,CHDUM1,CHDUM2,A,A, M=1,NLT), NLP,NLGP,NOCFP,NOCFGP
      if (NOCFP.EQ.0) GO TO 555
      if (IFL(I).GT.2.AND.NDEL(I).EQ.1) GO TO 545
      READ (ID2)
      GO TO 554
  545 if (I.eq.1) then
        call chckcfp(nlt,klsi,nlp,klsi,1,7,'CFP1')
c       READ (ID2) ((CFP1(M,N), M=1,NLT), N=1,NLP)
        call getsparse ((id2), cfp1, nlt, nlp, klsi,.false.)
      else
        call chckcfp( nlt,17,nlp,17,i-1,7,'CFP2')
c        READ (ID2) ((CFP2(M,N,I-1), M=1,NLT), N=1,NLP)
         call getsparse ((id2), cfp2(1,1,I-1), nlt, nlp, 17,.false.)
      endif
  554 READ (ID2)
  555 if (NDEL(I).NE.2) GO TO 565
      READ (ID2)
      N=1.0+AMIN1(FLL(IRHO)+FLLSIG, FLL(IRHOP)+FLLSIGP)
      if (I.le.IN) then
        call chckcfp(nlt,klsi,nlgp,klsi,n,7,'CFGP1')
        do 560 K=1,N
c         READ (ID2) ((CFGP1(M,L,K), M=1,NLT), L=1,NLGP)
          call getsparse ((id2), cfgp1(1,1,k), nlt, nlgp, klsi,.false.)
  560   continue
      else
        call chckcfp(nlt,17,nlgp,8,n,7,'CFGP2')
        do 562 K=1,N
c         READ (ID2) ((CFGP2(M,L,K), M=1,NLT), L=1,NLGP)
          call getsparse ((id2), cfgp2(1,1,k), nlt, nlgp, 17,.false.)
  562   continue
      endif
  565 CONTINUE
C

      TC0=1.0
      IXX=0
      do 570 I=IRHO,ISIG
  570 if (I.GT.IRHO) IXX=IXX+NIJK(I,J1A,KK)
      do 571 I=IRHOP,ISIGP
  571 if (I.GT.IRHOP) IXX=IXX+NIJK(I,J1B,KK)
      if (IRHO.EQ.ISIG) IXX=IXX+1
      if (IRHOP.EQ.ISIGP) IXX=IXX+1
      IXX=MOD(IXX,2)
      if (IXX.GT.0) TC0=-TC0
      N=NIJK(IRHO,J1A,KK)
      N1=N*NIJK(ISIG,J1A,KK)
      if (IRHO.EQ.ISIG) N1=(N1*(N-1))/N
      N=NIJK(IRHOP,J1B,KK)
      N1=N1*N*NIJK(ISIGP,J1B,KK)
      if (IRHOP.EQ.ISIGP) N1=(N1*(N-1))/N
      if (J.GT.0) N1=N1/(NJ*NJ)
      ERAS=N1
      TC0=TC0*SQRT(ERAS)
      if (IRHO.EQ.ISIG.AND.IRHOP.EQ.ISIGP) TC0=0.5*TC0
C
C          CALC MATRIX ELEMENTS
C
      LN=NTOTTJ(J1A)+1
      LX=NTOTTJ(J1A+1)
      LPN=NTOTTJ(J1B)+1
      LPX=NTOTTJ(J1B+1)
      NOPC=0
      do 700 LP=LPN,LPX
      do 700 L=LN,LX
      if (PscrL(L,NOSUBC).NE.PscrL(LP,NOSUBC)) GO TO 700
      if (PscrS(L,NOSUBC).NE.PscrS(LP,NOSUBC)) GO TO 700
      NOPC=NOPC+1
      TC=0.0
      do 610 I=1,NOSUBC
      if (I.LT.IN) GO TO 605
      if (I.LT.JX) GO TO 606
  605 if (PscrL(L,I).NE.PscrL(LP,I)) GO TO 690
      if (PscrS(L,I).NE.PscrS(LP,I)) GO TO 690
  606 do 607 J=1,4
  607 if (I.EQ.IRS(J)) GO TO 610
      if (NALSP(L,I)-NOTSJ1(I,J1A).NE.NALSP(LP,I)-NOTSJ1(I,J1B))GOTO 690
  610 CONTINUE
C                         MULT BY CFP
      TC=TC0
      do 630 I=1,NOSUBC
      if (NDEL(I).NE.1) GO TO 630
      if (IFL(I).LE.2) GO TO 630
      if (ICLASS.EQ.1.AND.I.EQ.IX) GO TO 630
      L1=L
      L2=LP
      J11=J1A
      J12=J1B
      if (NIJK(I,J1A,KK).GT.NIJK(I,J1B,KK)) GO TO 622
      L1=LP
      L2=L
      J11=J1B
      J12=J1A
  622 N1=NALSP(L1,I)-NOTSJ1(I,J11)
      N2=NALSP(L2,I)-NOTSJ1(I,J12)
      if (I.GT.1) GO TO 627
      TC=TC*CFP1(N1,N2)
      GO TO 630
  627 TC=TC*CFP2(N1,N2,I-1)
  630 CONTINUE
C
      if (TC.EQ.0.0) GO TO 690
C
      L1=L
      L2=LP
      J11=J1A
      J12=J1B
      if (IRHO.LT.IRHOP) GO TO 635
      L1=LP
      L2=L
      J11=J1B
      J12=J1A
  635 if (IN.EQ.1.OR.IN.EQ.IX) GO TO 640
      if (NIJK(IN,J11,KK).LE.1) GO TO 640
      M=IN
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      TC=TC*RECPSH(PscrL(L1,M-1),FL(N2,M),PscrL(L2,M),FLL(M),
     &    PscrL(L1,M),FL(N1,M)) *RECPSH(PscrS(L1,M-1),S(N2,M),
     &  PscrS(L2,M),0.5,PscrS(L1,M),S(N1,M))
      if (TC.EQ.0.0) GO TO 690
C
  640 MN=IN+1
      MX=IX-1
      if (MN.GT.MX) GO TO 650
      do 645 M=MN,MX
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      TC=TC*RECPEX(PscrL(L2,M-1),FLL(IN),PscrL(L1,M-1),FL(N1,M),
     &  PscrL(L1,M),PscrL(L2,M)) *RECPEX(PscrS(L2,M-1),0.5,
     &  PscrS(L1,M-1),S(N1,M),PscrS(L1,M),PscrS(L2,M))
  645 CONTINUE
      if (TC.EQ.0.0) GO TO 690
C
  650 L1=L
      L2=LP
      J11=J1A
      J12=J1B
      FLLJX=FLLSIG
      if (ISIG.GT.ISIGP) GO TO 655
      L1=LP
      L2=L
      J11=J1B
      J12=J1A
      FLLJX=FLLSIGP
  655 if (JN.GE.JX) GO TO 660
      if (NIJK(JX,J11,KK).LE.1) GO TO 660
      M=JX
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      TC=TC*RECPJP(PscrL(L1,M-1),FLLJX,PscrL(L2,M-1),FL(N2,M),
     &  PscrL(L1,M),FL(N1,M)) *RECPJP(PscrS(L1,M-1),0.5,
     &  PscrS(L2,M-1),S(N2,M),PscrS(L1,M),S(N1,M))
      if (TC.EQ.0.0) GO TO 690
C
  660 MN=JN+1
      MX=JX-1
      if (MN.GT.MX) GO TO 670
      FLLJX=FLLIJK(JX,J11,KK)
      do 665 M=MN,MX
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      TC=TC*RECPEX(PscrL(L1,M-1),FL(N1,M),PscrL(L1,M),FLLJX,
     &  PscrL(L2,M),PscrL(L2,M-1)) *RECPEX(PscrS(L1,M-1),S(N1,M),
     &  PscrS(L1,M),0.5,PscrS(L2,M),PscrS(L2,M-1))
  665 CONTINUE
  670 CONTINUE
C
  690 PC(NOPC)=TC
  700 CONTINUE
      CALL WARNDIM ( NOPC, KPC, 'KPC' )

      FLLRHO=FLL(IRHO)
      FLLRHOP=FLL(IRHOP)
      if (ITRNSP.NE.2) GO TO 702
      ERAS=FLLRHO
      FLLRHO=FLLRHOP
      FLLRHOP=ERAS
      ERAS=FLLSIG
      FLLSIG=FLLSIGP
      FLLSIGP=ERAS
  702 if (ICLASS.NE.7) GO TO 705
      KKK=KDMIN
      KDMIN=KEMIN
      KEMIN=KKK
      ERAS=FLLRHO
      FLLRHO=FLLSIG
      FLLSIG=ERAS
      if (AMOD(FLL(IRHOP)-FLL(IRHO),2.0).EQ.0.0) GO TO 705
      do 703 NN=1,NKD
  703 CIJR(NN)=-CIJR(NN)
  705 NOPC=0
      NOCC=0
      do 780 LP=LPN,LPX
      do 780 L=LN,LX
      if (PscrL(L,NOSUBC).NE.PscrL(LP,NOSUBC)) GO TO 780
      if (PscrS(L,NOSUBC).NE.PscrS(LP,NOSUBC)) GO TO 780
      NOPC=NOPC+1
      if (PC(NOPC).EQ.0.0) GO TO 780
      do 710 NN=1,NKTOT
  710 CCC(NN)=0.0
      LCI=L
      LCIP=LP
      if (ITRNSP.EQ.2) GO TO 722
      L1=L
      L2=LP
      J11=J1A
      J12=J1B
      GO TO 730
  722 L1=LP
      L2=L
      J11=J1B
      J12=J1A
  730 GO TO (731,732,733,734,735,736,737,738,739,740,741), ICLASS
  731 CALL CLASS1
      GO TO 750
  732 CALL CLASS2
      GO TO 750
  733 CALL CLASS3
      GO TO 750
  734 CALL CLASS4
      GO TO 750
  735 CALL CLASS5
      GO TO 750
  736 CALL CLASS6
      GO TO 750
  737 CALL CLASS7
      GO TO 750
  738 CALL CLASS8
      GO TO 750
  739 CALL CLASS9
      GO TO 750
  740 CALL CLAS10
      GO TO 750
  741 CALL CLAS11
  750 CONTINUE
C
      NOCC=NOCC+1
      if (NOCC.GT.KCCC) THEN
        WRITE (IW,'(///,A,10X,2I1,I2,3I1,5X,3I5,5X,3I5///)')
     &          ' ******* WARNING ***** ',
     &  J1A,J1B,IRHO,ISIG,IRHOP,ISIGP,LN,L,LX,LPN,LP,LPX
      endif
      CALL WARNDIM ( NOCC, KCCC, 'KCCC')
      NOPCCC(NOCC)=NOPC
      do 760 NN=1,NKTOT
  760 CC(NOCC,NN)=PC(NOPC)*CIJR(NN)*CCC(NN)
  780 CONTINUE
      NOCCX = MAX (NOCCX,NOCC)
C
      if (ICLASS.NE.7) GO TO 781
      KKK   = KDMIN
      KDMIN = KEMIN
      KEMIN = KKK
  781 do 790 NN = 1,NKTOT
      CAVE=0.0
      if (ICLASS.NE.11) GO TO 782
      if (NN.LE.NKD) GO TO 782
      A=NIJK(IRHO,J1A,KK)
      CAVE=0.5*A*CIJR(NN)/((2.0*FLL(IRHO)+1.0)*(2.0*FLLSIG+1.0))
  782 NOPC=0
      M=1
      do 784 LP=LPN,LPX
      do 783 L=LN,LX
      if (PscrL(L,NOSUBC).NE.PscrL(LP,NOSUBC)) GO TO 783
      if (PscrS(L,NOSUBC).NE.PscrS(LP,NOSUBC)) GO TO 783
      NOPC=NOPC+1
      PC(NOPC)=0.0
      if (M.GT.NOCC) GO TO 783
      if (NOPCCC(M).NE.NOPC) GO TO 783
      PC(NOPC)=CC(M,NN)
      if ((L-LN).EQ.(LP-LPN)) PC(NOPC)=PC(NOPC)+CAVE
      M=M+1
  783 CONTINUE
  784 CONTINUE
      NPAR=NPAR+1
      if (NN.LE.NKD) THEN
        K = KDMIN+2*(NN-1)
        TYPE = 'D'
      else
        K = KEMIN+2*(NN-NKD-1)
        TYPE = 'E'
      endif
      WRITE( ANAM, '(3I1,A1,4I1)')
     &  J1A,J1B,K, TYPE, IRHO,ISIG,IRHOP,ISIGP
      PARNAM(NPAR,KK) = ANAM
      if (ICPC.LE.1) GO TO 789
      WRITE (IW,80) ANAM,CAVE,ICLASS
   80 FORMAT (1H0,A8,5X,5HCAVE=,F12.7,15X,16HCONF INTER CLASS,I3/)
      WRITE (IW,81) (MULS1(LP),LHS1(LP),MULS4(LP),LHS4(LP),
     &  LP=LPN,LPX)
   81 FORMAT (10X,12('   (',I1,A1,')',I2,A1)/
     &       (12X,12('   (',I1,A1,')',I2,A1)))
      NT=0
      K1=1
      do 1788 K=1,NDIFFT
      NT=NT+NTRMK(K)
      if (NT.EQ.LN-1) K1=K+1
      if (NT.EQ.LX) K2=K
      if (NT.EQ.LPN-1) K1P=K+1
      if (NT.EQ.LPX) K2P=K
 1788 CONTINUE
      NPC2=0
      L2=LN-1
      do 4788 K=K1,K2
      LXN=NTRMK(K)
      L1=L2+1
      L2=L2+LXN
      L2P=LPN-1
      do 2788 KP=K1P,K2P
      L2P=L2P+NTRMK(KP)
      if (PscrL(L2,NOSUBC).NE.PscrL(L2P,NOSUBC)) GO TO 2788
      if (PscrS(L2,NOSUBC).NE.PscrS(L2P,NOSUBC)) GO TO 2788
      GO TO 3788
 2788 CONTINUE
      GO TO 4788
 3788 NPC1=NPC2
      NPC2=NPC2+NTRMK(K)*NTRMK(KP)
      do 788 L=L1,L2
      NPC1=NPC1+1
  788 WRITE (IW,82) MULS1(L),LHS1(L),MULS4(L),LHS4(L),
     &  (PC(I), I=NPC1,NPC2,LXN)
   82 FORMAT (2H (,I1,A1,1H),I2,A1,2X,12F10.5/(12X,12F10.5))
 4788 CONTINUE
      if (NPC2.NE.NOPC) WRITE (IW,83) NPC2,NOPC
   83 FORMAT (///16H0*****NPC2,NOPC=,2I8,6H *****///)
  789 CALL CLOK(TIME)
      WRITE(IW,84)
     & ANAM,NOPC,CAVE,ICLASS,TIME,
     & (LLIJK(L,J1A,KK),NIJK(L,J1A,KK),L=1,KS),
     & (LLIJK(L,J1B,KK),NIJK(L,J1B,KK),L=1,KS)
   84 FORMAT (/1H ,A8,5X,3HPC(,I5,1H),5X,5HCAVE=,F12.7,5X,7HICLASS=,I2,
     &  6X,5HDELT=,F8.2,4H MIN,7X, /20X,2(8(A1,I2,3X),5X))
      KPAR=-2
      WRITE (20) NPAR,KPAR,K,J1A,J1B,ANAM,CAVE,J,NOPC, (PC(I), I=1,NOPC)
      if (ICPC.ne.0.and.ICPC.ne.2)
     &   call printprk( pc, NTOTTJ,j1a,j1b, PscrL, PscrS, NOSUBC, anam)
  790 CONTINUE
C
  797 CONTINUE
  798 CONTINUE
      NPARJ(J1A,J1B)=NPAR-NPARJO
      NPARJO=NPAR
  799 CONTINUE
C
      WRITE (IW,86) NOCCX
   86 FORMAT (//7H0NOCCX=,I5)
      WRITE (IW,87) NPAR
   87 FORMAT (/9X,5HNPAR=,I4//9X,16HNPARJ-----------/)
      do 890 I=1,J1X
  890 WRITE (IW,89) I, (NPARJ(I,J), J=I,J1X)
   89 FORMAT (4H J1=,I2,2X,40I3/(8X,40I3))
C
  900 CONTINUE
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS1
C
C          CALC FOR IRHO=ISIG=IRHOP.LT.ISIGP
C            OR FOR IRHOP.LT.IRHO=ISIG=ISIGP
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      N1=NALSP(L1,IX)
      N2=NALSP(L2,IX)
      A1=PscrL(L1,IX-1)
      A2=PscrL(L2,IX-1)
      A3=PscrS(L1,IX-1)
      A4=PscrS(L2,IX-1)
      A5=PscrL(L1,IX)
      A6=PscrL(L2,IX)
      A7=PscrS(L1,IX)
      A8=PscrS(L2,IX)
      B1=FL(N1,IX)
      B2=FL(N2,IX)
      B3=S(N1,IX)
      B4=S(N2,IX)
C
      E1=1.0
      if (IN.LT.IX) GO TO 130
      FLLI=FLLSIGP
      if (IRHO.EQ.1) GO TO 141
      E1=RECPSH(A1,B2,A6,FLLI,A5,B1)*RECPSH(A3,B4,A8,0.5,A7,B3)
      GO TO 140
  130 FLLI=FLL(IN)
      E1=-RECPJP(A1,FLLI,A2,B2,A5,B1)*RECPJP(A3,0.5,A4,B4,A7,B3)
  140 if (E1.EQ.0.0) GO TO 900
C
  141 I=1
      J=2
      NII=MIN0(NIJK(IX,J11,KK),NIJK(IX,J12,KK))
      NIJ=1
      TL(2,1)=B2
      TS(2,1)=B4
      TSCL(2,1)=B2
      TSCS(2,1)=B4
      TL(1,2)=FLL(IX)
      TS(1,2)=0.5
      TL(2,2)=FLLI
      TS(2,2)=0.5
      TSCL(1,2)=B1
      TSCL(2,2)=B1
      TSCS(1,2)=B3
      TSCS(2,2)=B3
C
      if (NII-1) 900,144,143
  143 NLP=NOTSJ1(IX,J12+1)-NOTSJ1(IX,J12)
      KPI=N2-NOTSJ1(IX,J12)
      GO TO 145
  144 NLP=1
      TL(1,1)=FLL(IX)
      TS(1,1)=0.5
      TSCL(1,1)=FLL(IX)
      TSCS(1,1)=0.5
      E2=E1
  145 do 200 M=1,NLP
      if (NII.LE.1) GO TO 152
      KI=M
      N=N1-NOTSJ1(IX,J11)
      if (IX.GT.1) GO TO 150
      E2=E1*CFP1(N,M)
      GO TO 151
  150 E2=E1*CFP2(N,M,IX-1)
  151 if (E2.EQ.0.0) GO TO 200
      N=M+NOTSJ1(IX,J12)
      TL(1,1)=FL(N,IX)
      TS(1,1)=S(N,IX)
      TSCL(1,1)=FL(N,IX)
      TSCS(1,1)=S(N,IX)
  152 TK=KDMIN-2
      do 160 NN=1,NKD
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=CCC(NN)+E2*A
  160 CONTINUE
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS2
C
C          CALC FOR IRHO=ISIG.LT.IRHOP=ISIGP
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      N1=NALSP(L1,JX)
      N2=NALSP(L2,JX)
      N3=NALSP(L1,IN)
      N4=NALSP(L2,IN)
      A1=PscrL(L1,JX)
      A2=PscrS(L1,JX)
      A3=PscrL(L1,JX-1)
      A4=PscrL(L2,JX-1)
      A5=PscrS(L1,JX-1)
      A6=PscrS(L2,JX-1)
      B1=FL(N1,JX)
      B2=FL(N2,JX)
      B3=S(N1,JX)
      B4=S(N2,JX)
      B5=FL(N3,IN)
      B6=FL(N4,IN)
      B7=S(N3,IN)
      B8=S(N4,IN)
C
      TFL=ABS(A3-A4)-1.0
      NMIN=TFL+2.0
      NMAX=AMIN1(2.0*FLL(IN),2.0*FLLSIGP,A3+A4)+1.0
      if (NMIN.GT.NMAX) GO TO 900
      TFSN=ABS(A5-A6)
      TFSX=A5+A6
C
      E1=SQRT((2.0*B2+1.0)*(2.0*B4+1.0)*(2.0*A3+1.0)*(2.0*A5+1.0))
      XX=A4+A6+B2+B4+A1+A2+FLL(IN)+FLLSIGP
      if (AMOD(XX,2.0).GT.0.0) E1=-E1
      MN=IN+1
      MX=JX-1
      if (MN.GT.MX) GO TO 170
      do 150 M=MN,MX
      E1=E1*SQRT((2.0*PscrL(L2,M)+1.0)*(2.0*PscrS(L2,M)+1.0)
     &  *(2.0*PscrL(L1,M-1)+1.0)*(2.0*PscrS(L1,M-1)+1.0))
      I=NALSP(L2,M)
      XX=FL(I,M)+S(I,M)+PscrL(L2,M)+PscrS(L2,M)+PscrL(L1,M-1)
     &  +PscrS(L1,M-1)
      if (AMOD(XX,2.0).GT.0.0) E1=-E1
  150 CONTINUE
  170 if (IN.EQ.1) GO TO 200
      A7=PscrL(L1,IN)
      A8=PscrL(L2,IN)
      A9=PscrS(L1,IN)
      A10=PscrS(L2,IN)
      A11=PscrL(L1,IN-1)
      A12=PscrS(L1,IN-1)
      E1=E1*SQRT((2.0*A8+1.0)*(2.0*A10+1.0)*(2.0*B5+1.0)*(2.0*B7+1.0))
      XX=A11+A12+B6+B8+A7+A9
      if (AMOD(XX,2.0).GT.0.0) E1=-E1
C
  200 I1=N1-NOTSJ1(JX,J11)
      I2=N2-NOTSJ1(JX,J12)
      I3=N3-NOTSJ1(IN,J11)
      I4=N4-NOTSJ1(IN,J12)
      do 300 N=NMIN,NMAX
      TFL=TFL+1.0
      TFS=(1+(-1)**N)/2
      if (TFS.LT.TFSN.OR.TFS.GT.TFSX) GO TO 300
      E2=CFGP1(I3,I4,N)*CFGP2(I2,I1,N)
      if (E2.EQ.0.0) GO TO 300
      if (TFS.EQ.1.0) E2=-E2
      E2=E1*E2*S6J(B1,TFL,B2, A4,A1,A3)*S6J(B3,TFS,B4, A6,A2,A5)
      if (MN.GT.MX) GO TO 270
      do 250 M=MN,MX
      I=NALSP(L2,M)
      E2=E2*S6J(FL(I,M),PscrL(L2,M-1),PscrL(L2,M),
     &          TFL,PscrL(L1,M),PscrL(L1,M-1))
  250 E2=E2*S6J(S(I,M),PscrS(L2,M-1),PscrS(L2,M),
     &          TFS,PscrS(L1,M),PscrS(L1,M-1))
  270 if (IN.EQ.1) GO TO 280
      E2=E2*S6J(A11,B6,A8, TFL,A7,B5)
      E2=E2*S6J(A12,B8,A10, TFS,A9,B7)
  280 FK=KDMIN-2
      do 290 NN=1,NKD
      FK=FK+2.0
  290 CCC(NN)=CCC(NN)
     &  +E2*S6J(FLL(IN),FLL(IN),TFL, FLLSIGP,FLLSIGP,FK)
  300 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS3
C
C          CALC FOR IRHO=ISIG.LT.IRHOP.LT.ISIGP
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      I=IN
      J=JN
      N1=NALSP(L1,I)
      N2=NALSP(L2,I)
      TFLN=AMAX1(ABS(FLL(J)-FLLSIGP),ABS(FL(N1,I)-FL(N2,I)))
      TFSN=ABS(S(N1,I)-S(N2,I))
      TFLX=AMIN1(FLL(J)+FLLSIGP,2.0*FLL(I),FL(N1,I)+FL(N2,I))
      TFSX=AMIN1(S(N1,I)+S(N2,I),1.0)
      JM1=J-1
      do 120 M=I,JM1
      TFLN=AMAX1(ABS(PscrL(L1,M)-PscrL(L2,M)),TFLN)
      TFSN=AMAX1(ABS(PscrS(L1,M)-PscrS(L2,M)),TFSN)
      TFLX=AMIN1(PscrL(L1,M)+PscrL(L2,M),TFLX)
  120 TFSX=AMIN1(PscrS(L1,M)+PscrS(L2,M),TFSX)
      if (TFLN.GT.TFLX) GO TO 900
      if (TFSN.GT.TFSX) GO TO 900
C
      N3=NALSP(L1,J)
      N4=NALSP(L2,J)
      A1=PscrL(L1,J-1)
      A2=PscrL(L2,J-1)
      A3=PscrS(L1,J-1)
      A4=PscrS(L2,J-1)
      A5=PscrL(L1,J)
      A6=PscrL(L2,J)
      A7=PscrS(L1,J)
      A8=PscrS(L2,J)
      B1=FL(N3,J)
      B2=FL(N4,J)
      B3=S(N3,J)
      B4=S(N4,J)
C
      E1=SQRT((2.0*B2+1.0)*(2.0*B4+1.0)*(2.0*A1+1.0)*(2.0*A3+1.0)
     &  *(2.0*A6+1.0)*(2.0*A8+1.0))
      XX=FLL(I)+FLL(J)+FLLSIGP+0.5+A1+A3-A2-A4+A5+A7-A6-A8
      if (AMOD(XX,2.0).NE.0.0) E1=-E1
      TFL=TFLN-1.0
      NMIN=TFLN+1.0
      NMAX=TFLX+1.0
      I1=N1-NOTSJ1(I,J11)
      I2=N2-NOTSJ1(I,J12)
      do 200 N=NMIN,NMAX
      TFL=TFL+1.0
      TFS=(1+(-1)**N)/2
      if (TFS.LT.TFSN.OR.TFS.GT.TFSX) GO TO 200
      E2=E1
      if (NIJK(I,J12,KK).EQ.0) GO TO 130
      E2=E1*CFGP1(I1,I2,N)
      if (E2.EQ.0.0) GO TO 200
  130 if (I.EQ.1) GO TO 140
      A9=PscrL(L1,I-1)
      A10=PscrS(L1,I-1)
      A11=PscrL(L1,I)
      A12=PscrL(L2,I)
      A13=PscrS(L1,I)
      A14=PscrS(L2,I)
      B5=FL(N1,I)
      B6=FL(N2,I)
      B7=S(N1,I)
      B8=S(N2,I)
      XX=A9+A10+B6+B8+A11+A13
      if (AMOD(XX,2.0).GT.0.0) E2=-E2
      E2=E2*SQRT((2.0*A12+1.0)*(2.0*A14+1.0)*(2.0*B5+1.0)*(2.0*B7+1.0))
      E2=E2*S6J(A9,B6,A12, TFL,A11,B5)*S6J(A10,B8,A14, TFS,A13,B7)
  140 MN=I+1
      MX=J-1
      if (MN.GT.MX) GO TO 160
      do 150 M=MN,MX
      N5=NALSP(L1,M)
      A9=PscrL(L1,M-1)
      A10=PscrS(L1,M-1)
      A11=PscrL(L2,M)
      A12=PscrS(L2,M)
      B5=FL(N5,M)
      B6=S(N5,M)
      XX=B5+B6+A9+A10+A11+A12
      if (AMOD(XX,2.0).GT.0.0) E2=-E2
      E2=E2*SQRT((2.0*A9+1.0)*(2.0*A10+1.0)*(2.0*A11+1.0)*(2.0*A12+1.0))
  150 E2=E2*S6J(TFL,PscrL(L2,M-1),A9, B5,PscrL(L1,M),A11)
     &     *S6J(TFS,PscrS(L2,M-1),A10, B6,PscrS(L1,M),A12)
  160 E3=E2*SQRT((2.0*TFL+1.0)*(2.0*TFS+1.0))
      if (TFS.EQ.1.0) E3=-E3
      E3=E3*S9J(TFL,FLL(J),FLLSIGP, A2,B2,A6, A1,B1,A5)
     &     *S9J(TFS,0.5,0.5, A4,B4,A8, A3,B3,A7)
      FK=KDMIN-2
      do 170 NN=1,NKD
      FK=FK+2.0
  170 CCC(NN)=CCC(NN)+E3*S6J(FLL(I),FLL(I),TFL, FLLSIGP,FLL(J),FK)
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS4
C
C          CALC FOR IRHOP.LT.IRHO=ISIG.LT.ISIGP
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      M=JN
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      A1=PscrL(L1,M-1)
      A2=PscrL(L2,M-1)
      A3=PscrS(L1,M-1)
      A4=PscrS(L2,M-1)
      A5=PscrL(L1,M)
      A6=PscrL(L2,M)
      A7=PscrS(L1,M)
      A8=PscrS(L2,M)
      B1=FL(N1,M)
      B2=FL(N2,M)
      B3=S(N1,M)
      B4=S(N2,M)
      if (ABS(A1-A2).GT.FLL(IN)) GO TO 900
      if (ABS(A5-A6).GT.FLLSIGP) GO TO 900
      if (A1+A2.LT.FLL(IN)) GO TO 900
      if (A5+A6.LT.FLLSIGP) GO TO 900
      if (ABS(A3-A4).GT.0.5) GO TO 900
      if (ABS(A7-A8).GT.0.5) GO TO 900
C
      E1=SQRT((2.0*B1+1.0)*(2.0*B3+1.0)*(2.0*A2+1.0)*(2.0*A4+1.0)
     &  *(2.0*A6+1.0)*(2.0*A8+1.0))
      E2=FLL(IN)+FLLSIGP
      FXX=AMOD(E2+FLL(M)+1.5+A2+A4+A1-A3,2.0)
      if (FXX.NE.0.0) E1=-E1
C
      TFL=AMAX1(ABS(FLL(IN)-FLLSIGP),ABS(B1-B2))-1.0
      NMIN=TFL+2.0
      NMAX=AMIN1(2.0*FLL(M),E2,B1+B2)+1.0
      if (NMIN.GT.NMAX) GO TO 900
      TFSX=B3+B4
      I1=N1-NOTSJ1(M,J11)
      I2=N2-NOTSJ1(M,J12)
      do 200 N=NMIN,NMAX
      TFL=TFL+1.0
      E2=CFGP2(I1,I2,N)
      if (E2.EQ.0.0) GO TO 200
      TFS=(1+(-1)**N)/2
      if (TFS.GT.TFSX) GO TO 200
      if (TFS.EQ.1.0) E2=-E2
      E2=E1*E2*SQRT((2.0*TFL+1.0)*(2.0*TFS+1.0))
      E2=E2*S9J(A1,FLL(IN),A2, A5,FLLSIGP,A6, B1,TFL,B2)
      E2=E2*S9J(A3,0.5,A4, A7,0.5,A8, B3,TFS,B4)
      FK=KDMIN-2
      do 150 NN=1,NKD
      FK=FK+2.0
  150 CCC(NN)=CCC(NN)
     &  +E2*S6J(FLL(M),FLL(M),TFL, FLL(IN),FLLSIGP,FK)
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS5
C
C          CALC FOR IRHOP.LT.ISIGP.LT.IRHO=ISIG
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      I=IX
      J=JX
      N1=NALSP(L1,J)
      N2=NALSP(L2,J)
      B1=FL(N1,J)
      B2=FL(N2,J)
      B3=S(N1,J)
      B4=S(N2,J)
      TFLN=AMAX1(ABS(FLL(I)-FLL(IN)),ABS(B1-B2))
      TFSN=ABS(B3-B4)
      TFLX=AMIN1(FLL(I)+FLL(IN),2.0*FLLSIG,B1+B2)
      TFSX=AMIN1(B3+B4,1.0)
      do 120 M=I,J
      A1=PscrL(L1,M)
      A2=PscrL(L2,M)
      A3=PscrS(L1,M)
      A4=PscrS(L2,M)
      if (M.EQ.J) GO TO 120
      TFLN=AMAX1(ABS(A1-A2),TFLN)
      TFSN=AMAX1(ABS(A3-A4),TFSN)
      TFLX=AMIN1(A1+A2,TFLX)
      TFSX=AMIN1(A3+A4,TFSX)
  120 CONTINUE
      if (TFLN.GT.TFLX) GO TO 900
      if (TFSN.GT.TFSX) GO TO 900
C
      N3=NALSP(L1,I)
      N4=NALSP(L2,I)
      A5=PscrL(L1,J-1)
      A6=PscrL(L2,J-1)
      A7=PscrS(L1,J-1)
      A8=PscrS(L2,J-1)
      A9=PscrL(L1,I-1)
      A10=PscrL(L2,I-1)
      A11=PscrS(L1,I-1)
      A12=PscrS(L2,I-1)
      A13=PscrL(L1,I)
      A14=PscrL(L2,I)
      A15=PscrS(L1,I)
      A16=PscrS(L2,I)
      B5=FL(N3,I)
      B6=FL(N4,I)
      B7=S(N3,I)
      B8=S(N4,I)
C
      E1=SQRT((2.0*B6+1.0)*(2.0*B8+1.0)*(2.0*A10+1.0)*(2.0*A12+1.0)
     &  *(2.0*A13+1.0)*(2.0*A15+1.0)*(2.0*B1+1.0)*(2.0*B3+1.0)
     &  *(2.0*A6+1.0)*(2.0*A8+1.0))
      XX=FLL(IN)+FLLSIG+A5+A7+B1+B3+A1+A3
      if (AMOD(XX,2.0).GT.0.0) E1=-E1
      TFL=TFLN-1.0
      NMIN=TFLN+1.0
      NMAX=TFLX+1.0
      I1=N1-NOTSJ1(J,J11)
      I2=N2-NOTSJ1(J,J12)
      do 200 N=NMIN,NMAX
      TFL=TFL+1.0
      TFS=(1+(-1)**N)/2
      if (TFS.LT.TFSN.OR.TFS.GT.TFSX) GO TO 200
      E2=E1
      if (NIJK(J,J12,KK).EQ.0) GO TO 130
      E2=E1*CFGP2(I1,I2,N)
      if (E2.EQ.0.0) GO TO 200
  130 E3=E2*S6J(B2,TFL,B1, A5,A1,A6)*S6J(B4,TFS,B3, A7,A3,A8)
      MN=I+1
      MX=J-1
      if (MN.GT.MX) GO TO 160
      do 150 M=MN,MX
      N5=NALSP(L1,M)
      A17=PscrL(L2,M-1)
      A18=PscrS(L2,M-1)
      A19=PscrL(L1,M)
      A20=PscrL(L2,M)
      A21=PscrS(L1,M)
      A22=PscrS(L2,M)
      B9=FL(N5,M)
      B10=S(N5,M)
      XX=B9+B10+A19+A21+A17+A18
      if (AMOD(XX,2.0).GT.0.0) E3=-E3
      E3=E3*SQRT((2.0*A19+1.0)*(2.0*A21+1.0)*(2.0*A17+1.0)
     &  *(2.0*A18+1.0))
  150 E3=E3*S6J(B9,PscrL(L1,M-1),A19, TFL,A20,A17)
     &    *S6J(B10,PscrS(L1,M-1),A21, TFS,A22,A18)
  160 E4=E3*SQRT((2.0*TFL+1.0)*(2.0*TFS+1.0))
      if (TFS.EQ.1.0) E4=-E4
      E4=E4*S9J(A9,FLL(IN),A10, B5,FLL(I),B6, A13,TFL,A14)
     &     *S9J(A11,0.5,A12, B7,0.5,B8, A15,TFS,A16)
      FK=KDMIN-2
      do 170 NN=1,NKD
      FK=FK+2.0
  170 CCC(NN)=CCC(NN)+E4*S6J(FLLSIG,FLLSIG,TFL, FLL(I),FLL(IN),FK)
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS6
C
C          CALC FOR IRHO=IRHOP.LT.ISIG.LT.ISIGP
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      M=JN
      N1=NALSP(L1,M)
      N2=NALSP(L2,M)
      A1=PscrL(L1,M-1)
      A2=PscrL(L2,M-1)
      A3=PscrS(L1,M-1)
      A4=PscrS(L2,M-1)
      A5=PscrL(L1,M)
      A6=PscrL(L2,M)
      A7=PscrS(L1,M)
      A8=PscrS(L2,M)
      B1=FL(N1,M)
      B2=FL(N2,M)
      B3=S(N1,M)
      B4=S(N2,M)
      if (JN.LT.NOSUBC.OR.JX.LT.NOSUBC) GO TO 115
      A6=A2
      A8=A4
      B1=FLLSIG
      B2=0.0
      B3=0.5
      B4=0.0
  115 TFL=AMAX1(ABS(A1-FLLSIG), ABS(A2-FLLSIGP), ABS(B2-A5))-1.0
      NFL=AMIN1(A1+FLLSIG, A2+FLLSIGP, B2+A5)-TFL
      if (NFL.LE.0) GO TO 300
      TFS=AMAX1(ABS(A3-0.5), ABS(A4-0.5), ABS(B4-A7))-1.0
      NFS=AMIN1(A3+0.5, A4+0.5, B4+A7)-TFS
      TFSN=TFS
      if (NFS.LE.0) GO TO 300
      XX=A1+B1+A5+B2+FLLSIGP+A6+A3+B3+A7+B4+0.5+A8
      E1=SQRT((2.0*B1+1.0)*(2.0*A6+1.0)*(2.0*B3+1.0)*(2.0*A8+1.0))
C
      do 200 NL=1,NFL
      TFL=TFL+1.0
      E2=E1*(2.0*TFL+1.0)*S6J(B2,FLLSIG,B1, A1,A5,TFL)
     &  *S6J(B2,A2,A6, FLLSIGP,A5,TFL)
      TFS=TFSN
      do 200 NS=1,NFS
      TFS=TFS+1.0
      E3=E2*(2.0*TFS+1.0)*S6J(B4,0.5,B3, A3,A7,TFS)
     &  *S6J(B4,A4,A8, 0.5,A7,TFS)
      if (AMOD(XX+TFL+TFS,2.0).GT.0.0) E3=-E3
      do 130 III=1,2
      L3=L1
      if (III.EQ.2) L3=L2
      MM1=M-1
      do 130 IP=1,MM1
      II=NALSP(L3,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(L3,IP)
  130 TSCS(III,IP)=PscrS(L3,IP)
      TL(1,M)=FLLSIG
      TS(1,M)=0.5
      TL(2,M)=FLLSIGP
      TS(2,M)=0.5
      TSCL(1,M)=TFL
      TSCS(1,M)=TFS
      TSCL(2,M)=TFL
      TSCS(2,M)=TFS
      I=IRHO
      J=M
      NIJ=1
      KI=NALSP(L1,I)-NOTSJ1(I,J11)
      KPI=NALSP(L2,I)-NOTSJ1(I,J12)
      TK=KDMIN-2
      do 150 NN=1,NKD
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=CCC(NN)+E3*A
  150 CONTINUE
      N1=NKD+1
      TK=KEMIN-2
      do 160 NN=N1,NKTOT
      TK=TK+2.0
      CALL REIJ(I,J)
      CCC(NN)=CCC(NN)+E3*RSUM
  160 CONTINUE
  200 CONTINUE
C
  300 CONTINUE
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS7
C
C          CALC FOR IRHO.LT.ISIG=IRHOP.LT.ISIGP
C
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      I=IX
      N1=NALSP(L1,I)
      A1=PscrL(L1,I-1)
      A2=PscrL(L2,I-1)
      A3=PscrS(L1,I-1)
      A4=PscrS(L2,I-1)
      A5=PscrL(L1,I)
      A6=PscrL(L2,I)
      A7=PscrS(L1,I)
      A8=PscrS(L2,I)
      B1=FL(N1,I)
      B2=S(N1,I)
      TFLN=AMAX1(ABS(FLL(IN)-A5),ABS(B1-A2))
      TFSN=AMAX1(ABS(0.5-A7),ABS(B2-A4))
      TFLX=AMIN1(FLL(IN)+A5,B1+A2)
      TFSX=AMIN1(0.5+A7,B2+A4)
      if (TFLN.GT.TFLX) GO TO 900
      if (TFSN.GT.TFSX) GO TO 900
C
      J=I+1
      NIJ=1
      N2=NALSP(L2,I)
      KI=N1-NOTSJ1(I,J11)
      KPI=N2-NOTSJ1(I,J12)
      TL(1,I)=B1
      TL(2,I)=FL(N2,I)
      TS(1,I)=B2
      TS(2,I)=S(N2,I)
      TL(1,J)=FLL(IN)
      TL(2,J)=FLLSIGP
      TS(1,J)=0.5
      TS(2,J)=0.5
      TSCL(1,I-1)=A2
      TSCL(2,I-1)=A2
      TSCS(1,I-1)=A4
      TSCS(2,I-1)=A4
      TSCL(2,I)=A6
      TSCS(2,I)=A8
      TSCL(1,J)=A5
      TSCL(2,J)=A5
      TSCS(1,J)=A7
      TSCS(2,J)=A7
C
      E1=SQRT((2.0*A1+1.0)*(2.0*A3+1.0))
      XX1=FLL(IN)+0.5+B1+B2+A1+A3
      TFL=TFLN-1.0
      NLX=TFLX-TFL
      NSX=TFSX-TFSN+1.0
      do 200 NL=1,NLX
      TFL=TFL+1.0
      TSCL(1,I)=TFL
      E2=E1*S6J(FLL(IN),A2,A1, B1,A5,TFL)
      TFS=TFSN-1.0
      do 200 NS=1,NSX
      TFS=TFS+1.0
      TSCS(1,I)=TFS
      E3=E2*SQRT((2.0*TFL+1.0)*(2.0*TFS+1.0))
     &  *S6J(0.5,A4,A3, B2,A7,TFS)
      if (AMOD(XX1+TFL+TFS,2.0).GT.0.0) E3=-E3
      TK=KEMIN-2
      do 150 NN=1,NKD
      TK=TK+2.0
      CALL REIJ(I,J)
      CCC(NN)=CCC(NN)-E3*RSUM
  150 CONTINUE
      N1=NKD+1
      TK=KDMIN-2
      do 160 NN=N1,NKTOT
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=CCC(NN)-E3*A
  160 CONTINUE
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS8
C
C          CALC FOR IRHO.LT.IRHOP.LT.ISIG=ISIGP
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
C
      I=IX
      J=JX
      N1=NALSP(L1,I)
      N2=NALSP(L2,I)
      A1=PscrL(L1,I-1)
      A2=PscrL(L2,I-1)
      A3=PscrS(L1,I-1)
      A4=PscrS(L2,I-1)
      A5=PscrL(L1,I)
      A6=PscrL(L2,I)
      A7=PscrS(L1,I)
      A8=PscrS(L2,I)
      B1=FL(N1,I)
      B2=FL(N2,I)
      B3=S(N1,I)
      B4=S(N2,I)
      TFLN=AMAX1(ABS(FLL(IN)-A5),ABS(A2-B1),ABS(FLL(I)-A6))
      TFSN=AMAX1(ABS(0.5-A7),ABS(A4-B3),ABS(0.5-A8))
      TFLX=AMIN1(FLL(IN)+A5,A2+B1,FLL(I)+A6)
      TFSX=AMIN1(0.5+A7,A4+B3,0.5+A8)
      if (TFLN.GT.TFLX) GO TO 900
      if (TFSN.GT.TFSX) GO TO 900
C
      NII=1
      KJ=NALSP(L1,J)-NOTSJ1(J,J11)
      KPJ=NALSP(L2,J)-NOTSJ1(J,J12)
      do 120 III=1,2
      L3=L1
      if (III.EQ.2) L3=L2
      do 120 IP=I,J
      II=NALSP(L3,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(L3,IP)
  120 TSCS(III,IP)=PscrS(L3,IP)
      TL(1,I)=FLL(IN)
      TL(2,I)=FLL(I)
      TS(1,I)=0.5
      TS(2,I)=0.5
C
      E1=SQRT((2.0*A1+1.0)*(2.0*A3+1.0)*(2.0*B2+1.0)*(2.0*B4+1.0))
      XX1=FLL(I)-FLL(IN)+A2+A4-A1-A3+A6+A8
      TFL=TFLN-1.0
      NLX=TFLX-TFL
      NSX=TFSX-TFSN+1.0
      do 200 NL=1,NLX
      TFL=TFL+1.0
      TSCL(1,I-1)=TFL
      TSCL(2,I-1)=TFL
      E2=E1*(2.0*TFL+1.0)*S6J(FLL(IN),A2,A1, B1,A5,TFL)
     &  *S6J(A2,B1,TFL, FLL(I),A6,B2)
      TFS=TFSN-1.0
      do 200 NS=1,NSX
      TFS=TFS+1.0
      TSCS(1,I-1)=TFS
      TSCS(2,I-1)=TFS
      E3=E2*(2.0*TFS+1.0)*S6J(0.5,A4,A3, B3,A7,TFS)
     &  *S6J(A4,B3,TFS, 0.5,A8,B4)
      if (AMOD(XX1-TFL-TFS,2.0).NE.0.0) E3=-E3
      TK=KDMIN-2
      do 150 NN=1,NKD
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=CCC(NN)+E3*A
  150 CONTINUE
      N1=NKD+1
      TK=KEMIN-2
      do 160 NN=N1,NKTOT
      TK=TK+2.0
      CALL REIJ(I,J)
      CCC(NN)=CCC(NN)+E3*RSUM
  160 CONTINUE
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLASS9
C
C          CALC FOR IRHO.LT.ISIG.LT.IRHOP.LT.ISIGP
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
C
      A1=PscrL(L1,IX-1)
      A2=PscrL(L2,IX-1)
      A3=PscrS(L1,IX-1)
      A4=PscrS(L2,IX-1)
      A5=PscrL(L1,IX)
      A6=PscrL(L2,IX)
      A7=PscrS(L1,IX)
      A8=PscrS(L2,IX)
      A9=PscrL(L1,JN-1)
      A10=PscrL(L2,JN-1)
      A11=PscrS(L1,JN-1)
      A12=PscrS(L2,JN-1)
      A13=PscrL(L1,JN)
      A14=PscrL(L2,JN)
      A15=PscrS(L1,JN)
      A16=PscrS(L2,JN)
      TFLN=AMAX1(ABS(FLL(IN)-FLL(IX)),ABS(FLL(JN)-FLLSIGP),ABS(A5-A6),
     &  ABS(A9-A10))
      TFLX=AMIN1(FLL(IN)+FLL(IX),FLL(JN)+FLLSIGP,A5+A6,A9+A10)
      if (TFLN.GT.TFLX) GO TO 900
      TFSN=AMAX1(ABS(A7-A8),ABS(A11-A12))
      TFSX=AMIN1(1.0,A7+A8,A11+A12)
      if (TFSN.GT.TFSX) GO TO 900
      N1=NALSP(L1,IX)
      N2=NALSP(L2,IX)
      N3=NALSP(L1,JN)
      N4=NALSP(L2,JN)
      B1=FL(N1,IX)
      B2=FL(N2,IX)
      B3=S(N1,IX)
      B4=S(N2,IX)
      B5=FL(N3,JN)
      B6=FL(N4,JN)
      B7=S(N3,JN)
      B8=S(N4,JN)
      E6=(2.0*A1+1.0)*(2.0*B1+1.0)*(2.0*A6+1.0)*(2.0*A9+1.0)
     &  *(2.0*B6+1.0)*(2.0*A14+1.0)*(2.0*A3+1.0)*(2.0*B3+1.0)
     &  *(2.0*A8+1.0)*(2.0*A11+1.0)*(2.0*B8+1.0)*(2.0*A16+1.0)
      E6=SQRT(E6)
      XX=FLLSIGP+A9+A13-A10-A14+0.5+A11+A15-A12-A16
      if (AMOD(XX,2.0).NE.0.0) E6=-E6
      MN=IX+1
      MX=JN-1
C
      TFL=TFLN-1.0
      NLX=TFLX-TFL
      NSX=TFSX-TFSN+1.0
      do 300 NL=1,NLX
      TFL=TFL+1.0
      E1=E6*S9J(A2,B2,A6,FLL(IN),FLL(IX),TFL,A1,B1,A5)
     &     *S9J(TFL,A10,A9,FLL(JN),B6,B5,FLLSIGP,A14,A13)*(2.0*TFL+1.0)
      if (E1.EQ.0.0) GO TO 300
      if (TFL.EQ.0.0) GO TO 140
      if (AMOD(TFL,2.0).NE.0.0) E1=-E1
      if (MN.GT.MX) GO TO 140
      do 130 M=MN,MX
      N5=NALSP(L1,M)
      if (FL(N5,M).EQ.0.0) GO TO 130
      E1=E1*RECPEX(PscrL(L2,M-1),TFL,PscrL(L1,M-1),FL(N5,M),
     &  PscrL(L1,M),PscrL(L2,M))
  130 CONTINUE
      if (E1.EQ.0.0) GO TO 300
  140 TFS=TFSN-1.0
      do 200 NS=1,NSX
      TFS=TFS+1.0
      E2=E1*S9J(A4,B4,A8,0.5,0.5,TFS,A3,B3,A7)
     &     *S9J(TFS,A12,A11,0.5,B8,B7,0.5,A16,A15)*(2.0*TFS+1.0)
      if (E2.EQ.0.0) GO TO 200
      if (TFS.EQ.0.0) GO TO 155
      if (AMOD(TFS,2.0).NE.0.0) E2=-E2
      if (MN.GT.MX) GO TO 155
      do 150 M=MN,MX
      N5=NALSP(L1,M)
      if (S(N5,M).EQ.0.0) GO TO 150
      E2=E2*RECPEX(PscrS(L2,M-1),TFS,PscrS(L1,M-1),S(N5,M),
     &  PscrS(L1,M),PscrS(L2,M))
  150 CONTINUE
      if (E2.EQ.0.0) GO TO 200
  155 TK=KDMIN-2
      do 160 NN=1,NKD
      TK=TK+2.0
      E3=E2*S6J(FLL(IN),FLL(IX),TFL,FLLSIGP,FLL(JN),TK)
      if (AMOD(FLL(IX)+FLL(JN)+TFL,2.0).NE.0.0) E3=-E3
  160 CCC(NN)=CCC(NN)+E3
      N1=NKD+1
      TK=KEMIN-2
      do 170 NN=N1,NKTOT
      TK=TK+2.0
      E3=E2*S6J(FLL(IN),FLL(IX),TFL,FLL(JN),FLLSIGP,TK)
      if (AMOD(TFS,2.0).NE.0.0) E3=-E3
  170 CCC(NN)=CCC(NN)+E3
  200 CONTINUE
  300 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLAS10
C
C          CALC FOR IRHO.LT.IRHOP.LT.ISIG.LT.ISIGP
C            OR FOR IRHO.LT.IRHOP.LT.ISIGP.LT.ISIG
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
C
      FLLJN=FLLSIG
      FLLJX=FLLSIGP
      if (IRHO.LT.IRHOP.AND.ISIG.LE.ISIGP) GO TO 110
      if (IRHO.GE.IRHOP.AND.ISIG.GE.ISIGP) GO TO 110
      FLLJN=FLLSIGP
      FLLJX=FLLSIG
  110 LA=L1
      LB=L2
      JA=J11
      JB=J12
      TL(1,JN)=FLLJN
      TL(2,JN)=FLLJX
      TS(1,JN)=0.5
      TS(2,JN)=0.5
      if (L1.EQ.LCI.AND.ISIG.LE.ISIGP) GO TO 120
      if (L1.EQ.LCIP.AND.ISIGP.LE.ISIG) GO TO 120
      LA=L2
      LB=L1
      JA=J12
      JB=J11
      TL(1,JN)=FLLJX
      TL(2,JN)=FLLJN
C
  120 I=IX
      J=JN
      NII=1
      NIJ=1
      N1=NALSP(L1,I)
      N2=NALSP(L2,I)
      N3=NALSP(LA,J)
      N4=NALSP(LB,J)
      A1=PscrL(L1,I-1)
      A2=PscrL(L2,I-1)
      A3=PscrS(L1,I-1)
      A4=PscrS(L2,I-1)
      A5=PscrL(L1,I)
      A6=PscrL(L2,I)
      A7=PscrS(L1,I)
      A8=PscrS(L2,I)
      A9=PscrL(LA,J-1)
      A10=PscrL(LB,J-1)
      A11=PscrS(LA,J-1)
      A12=PscrS(LB,J-1)
      A13=PscrL(LA,J)
      A14=PscrL(LB,J)
      A15=PscrS(LA,J)
      A16=PscrS(LB,J)
      B1=FL(N1,I)
      B2=FL(N2,I)
      B3=S(N1,I)
      B4=S(N2,I)
      B5=FL(N3,J)
      B6=FL(N4,J)
      B7=S(N3,J)
      B8=S(N4,J)
      if (JN.LT.NOSUBC.OR.JX.LT.NOSUBC) GO TO 130
      A14=A10
      A16=A12
      B5=FLLJN
      B6=0.0
      B7=0.5
      B8=0.0
C
  130 TLI=AMAX1(ABS(FLL(IN)-A5),ABS(A2-B1),ABS(FLL(I)-A6))-1.0
      NTLI=AMIN1(FLL(IN)+A5,A2+B1,FLL(I)+A6)-TLI
      if (NTLI.LE.0) GO TO 900
      TLJN=AMAX1(ABS(FLLJN-A9),ABS(A13-B6),ABS(FLLJX-A10))-1.0
      NTLJ=AMIN1(FLLJN+A9,A13+B6,FLLJX+A10)-TLJN
      if (NTLJ.LE.0) GO TO 900
      TSIN=AMAX1(ABS(0.5-A7),ABS(A4-B3),ABS(0.5-A8))-1.0
      NTSI=AMIN1(0.5+A7,A4+B3,0.5+A8)-TSIN
      if (NTSI.LE.0) GO TO 900
      TSJN=AMAX1(ABS(0.5-A11),ABS(A15-B8),ABS(0.5-A12))-1.0
      NTSJ=AMIN1(0.5+A11,A15+B8,0.5+A12)-TSJN
      if (NTSJ.LE.0) GO TO 900
C
      JM1=J-1
      do 150 III=1,2
      M=L1
      if (III.EQ.2) M=L2
      do 150 IP=I,JM1
      II=NALSP(M,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(M,IP)
  150 TSCS(III,IP)=PscrS(M,IP)
      TL(1,I)=FLL(IN)
      TL(2,I)=FLL(IX)
      TS(1,I)=0.5
      TS(2,I)=0.5
C
      E1=SQRT((2.0*A1+1.0)*(2.0*A3+1.0)*(2.0*B2+1.0)*(2.0*B4+1.0)
     &  *(2.0*B5+1.0)*(2.0*B7+1.0)*(2.0*A14+1.0)*(2.0*A16+1.0))
      XX1=FLL(IN)+FLL(I)+FLLJX+B1+B1+A1+A2+A6+B5+B6+A9+A13+A14
     &  +1.5+B3+B3+A3+A4+A8+B7+B8+A11+A15+A16
      do 200 NLI=1,NTLI
      TLI=TLI+1.0
      E2=E1*(2.0*TLI+1.0)*S6J(FLL(IN),A2,A1, B1,A5,TLI)
      E2=E2*S6J(A2,B1,TLI, FLL(I),A6,B2)
      TSCL(1,I-1)=TLI
      TSCL(2,I-1)=TLI
      TLJ=TLJN
      do 200 NLJ=1,NTLJ
      TLJ=TLJ+1.0
      E3=E2*(2.0*TLJ+1.0)*S6J(B6,FLLJN,B5, A9,A13,TLJ)
      E3=E3*S6J(B6,A10,A14, FLLJX,A13,TLJ)
      TSCL(1,J)=TLJ
      TSCL(2,J)=TLJ
      TSI=TSIN
      do 200 NSI=1,NTSI
      TSI=TSI+1.0
      E4=E3*(2.0*TSI+1.0)*S6J(0.5,A4,A3, B3,A7,TSI)
      E4=E4*S6J(A4,B3,TSI, 0.5,A8,B4)
      TSCS(1,I-1)=TSI
      TSCS(2,I-1)=TSI
      TSJ=TSJN
      do 200 NSJ=1,NTSJ
      TSJ=TSJ+1.0
      E5=E4*(2.0*TSJ+1.0)*S6J(B8,0.5,B7, A11,A15,TSJ)
      E5=E5*S6J(B8,A12,A16, 0.5,A15,TSJ)
      XX=XX1+TLI+TLJ+TSI+TSJ
      if (AMOD(XX,2.0).GT.0.0) E5=-E5
      TSCS(1,J)=TSJ
      TSCS(2,J)=TSJ
      TK=KDMIN-2
      do 160 NN=1,NKD
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=CCC(NN)+E5*A
  160 CONTINUE
      N1=NKD+1
      TK=KEMIN-2
      do 170 NN=N1,NKTOT
      TK=TK+2.0
      CALL REIJ(I,J)
      CCC(NN)=CCC(NN)+E5*RSUM
  170 CONTINUE
  200 CONTINUE
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CLAS11
C
C          CALC FOR IRHO=IRHOP, ISIG=ISIGP=NOSUBC, NI(ISIG)=1
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      I=IRHO
      J=ISIG
      NIJ=1
      KI=NALSP(LCI,I)-NOTSJ1(I,J1A)
      KPI=NALSP(LCIP,I)-NOTSJ1(I,J1B)
      do 110 III=1,2
      MM=LCI
      if (III.EQ.2) MM=LCIP
      do 110 IP=1,NOSUBC
      II=NALSP(MM,IP)
      TL(III,IP)=FL(II,IP)
      TS(III,IP)=S(II,IP)
      TSCL(III,IP)=PscrL(MM,IP)
  110 TSCS(III,IP)=PscrS(MM,IP)
      TK=KDMIN-2
      do 150 NN=1,NKD
      TK=TK+2.0
      CALL RDIJ(I,J)
      CCC(NN)=A
  150 CONTINUE
      N1=NKD+1
      TK=KEMIN-2
      do 160 NN=N1,NKTOT
      TK=TK+2.0
      CALL REIJ(I,J)
      CCC(NN)=RSUM
  160 CONTINUE
C
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE RDIJ(I,J)
C
C    &     CALCULATE RD(I,J)
C
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      N=TK+1.0
      A=0.0
      do 200 II=I,J
      if (TS(1,II).NE.TS(2,II)) GO TO 399
      if (TSCS(1,II).NE.TSCS(2,II)) GO TO 399
  200 CONTINUE
      IXX=TL(1,J)+TSCL(2,J-1)+TSCL(1,J)
      A=(-1.0)**IXX
      A=A*S6J(TSCL(1,J-1),TSCL(2,J-1),TK, TL(2,J),TL(1,J),TSCL(1,J))
      MN=I+1
      MX=J-1
      if (MN-MX) 350,350,352
  350 do 351 M1=MN,MX
  351 A=A*UNCPLA(TSCL(1,M1-1),TL(1,M1),TSCL(1,M1),
     &    TK,TSCL(2,M1-1),TSCL(2,M1))
  352 if (I-1) 366,366,355
  355 A=A*UNCPLB(TSCL(1,I-1),TL(1,I),TSCL(1,I), TK,TL(2,I),TSCL(2,I))
  366 if (NII.EQ.1) GO TO 380
      if (IRHO.NE.0) GO TO 371
      GO  TO (371,372,373,374,375,376) I
  371 A=A*U1(KI,KPI,N)
      GO TO 380
  372 A=A*U2(KI,KPI,N)
      GO TO 380
  373 A=A*U3(KI,KPI,N)
      GO TO 380
  374 A=A*U4(KI,KPI,N)
      GO TO 380
  375 A=A*U5(KI,KPI,N)
      GO TO 380
  376 A=A*U6(KI,KPI,N)
  380 if (NIJ.EQ.1) GO TO 399
      if (IRHO.NE.0) GO TO 381
      GO TO (381,382,383,384,385,386) J
  381 A=A*U1(KJ,KPJ,N)
      GO TO 399
  382 A=A*U2(KJ,KPJ,N)
      GO TO 399
  383 A=A*U3(KJ,KPJ,N)
      GO TO 399
  384 A=A*U4(KJ,KPJ,N)
      GO TO 399
  385 A=A*U5(KJ,KPJ,N)
      GO TO 399
  386 A=A*U6(KJ,KPJ,N)
  399 CONTINUE
C
      RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE REIJ(I,J)
C
C    &     CALCULATE RE(I,J)
C
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/    CFP2(17,17,7),CIJK(10,6),
     &    U2(17,17,7),U3(17,17,9),U4(17,17,9),U5(17,17,7),U6(17,17,7),
     &    V2(17,17,7),V3(17,17,7),V4(17,17,7),V5(17,17,5),V6(17,17,5),
     &    NTI(31),NALSJI(KJP),TFLN,TFLX,TFSN,TFSX,
     &  PJI(KJP),MULSI(KLSI),IRHO,IRHOP,ISIG,ISIGP,FLLRHO,
     &  FLLSIG,IN,JN,JX,NII,NIJ,KI,KJ,KPI,KPJ,KDMIN,KEMIN,NKD,NKE,NKTOT,
     &  TR,TK,TC,RSUM,IRP1MX,        J11,J12,L1,L2,TL(2,KS),TS(2,KS),
     &  TSCL(2,KS),TSCS(2,KS),NIJKP(KS,2),IRS(KS),IFL(KS),NDEL(KS),
     &    CIJR(8),CCC(8),N1,N2,N3,N4,E1,E2,E3,
     &  E4,E5,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,B1,B2,B3,B4,B5,B6,
     &  CFGP2(17,8,7),NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       U1(KLSI,KLSI,9)
      COMMON/C3LC2/CFGP1(KLSI,KLSI,7),PC(KPC)
      COMMON/C3LC3/V1(KLSI,KLSI,9)
      COMMON/C3LC4/ISER(KISER),PCI(KISER),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS),MULS1(KLSC),MULS4(KLSC)
      DIMENSION CFP1(KLS1,KLS1)
      EQUIVALENCE (CFP1,CFGP1)
C
      DATA SQRT3H/1.224744871/
C
      IXX=TL(1,J)+TSCL(2,J-1)+TSCL(1,J)
      AR=(-1.0)**IXX
      IXX=TS(1,J)+TSCS(2,J-1)+TSCS(1,J)
      A1R=(-1.0)**IXX
      A1R=A1R*S6J(TSCS(1,J-1),TS(1,J),TSCS(1,J),TS(2,J),TSCS(2,J-1),1.0)
      MN=I+1
      MX=J-1
      if (MN-MX) 420,420,422
  420 do 421 M1=MN,MX
      IXX=TSCL(1,M1-1)+TL(1,M1)+TSCL(2,M1)
      if (MOD(IXX,2).GT.0) AR=-AR
      AR=AR*SQRT((2.0*TSCL(1,M1)+1.0)*(2.0*TSCL(2,M1)+1.0))
  421 A1R=A1R*UNCPLA(TSCS(1,M1-1),TS(1,M1),TSCS(1,M1),
     &      1.0,TSCS(2,M1-1),TSCS(2,M1))
  422 if (I-1) 430,430,425
  425 IXX=TSCL(1,I-1)+TSCL(1,I)+TL(2,I)
      if (MOD(IXX,2).GT.0) AR=-AR
      AR=AR*SQRT((2.0*TSCL(1,I)+1.0)*(2.0*TSCL(2,I)+1.0))
      A1R=A1R*UNCPLB(TSCS(1,I-1),TS(1,I),TSCS(1,I),
     &      1.0,TS(2,I),TSCS(2,I))
  430 A1R=A1R*AR
      do 431 M1=I,MX
      if (TSCS(1,M1)-TSCS(2,M1)) 433,431,433
  431 CONTINUE
      if (TS(1,I)-TS(2,I)) 433,432,433
  432 if (TS(1,J)-TS(2,J)) 433,440,433
  433 AR=0.0
C
  440 RSUM=0.0
      R=KDMIN-1
      do 469 IRP1=1,IRP1MX
      R=R+1.0
      N=R+1.0
      PR=S6J(TSCL(1,J-1),TSCL(2,J-1),R, TL(2,J),TL(1,J),TSCL(1,J))
      P1R=4.0
      if (MN-MX) 443,443,445
  443 do 444 M1=MN,MX
      if (MOD(R,2.0).GT.0.0) PR=-PR
  444 PR=PR*S6J(TSCL(1,M1-1),TSCL(2,M1-1),R,
     &    TSCL(2,M1),TSCL(1,M1),TL(1,M1))
  445 if (I.LE.1) GO TO 447
      if (MOD(R,2.0).GT.0.0) PR=-PR
      PR=PR*S6J(TL(1,I),TL(2,I),R,
     &          TSCL(2,I),TSCL(1,I),TSCL(1,I-1))
  447 P1R=P1R*PR*A1R
      PR=PR*AR
      if (NII.GT.1) GO TO 450
      P1R=P1R*SQRT3H
      GO TO 459
  450 if (IRHO.NE.0) GO TO 451
      GO TO(451,452,453,454,455,456) I
  451 PR=PR*U1(KI,KPI,N)
      P1R=P1R*V1(KI,KPI,N)
      GO TO 459
  452 PR=PR*U2(KI,KPI,N)
      P1R=P1R*V2(KI,KPI,N)
      GO TO 459
  453 PR=PR*U3(KI,KPI,N)
      P1R=P1R*V3(KI,KPI,N)
      GO TO 459
  454 PR=PR*U4(KI,KPI,N)
      P1R=P1R*V4(KI,KPI,N)
      GO TO 459
  455 PR=PR*U5(KI,KPI,N)
      P1R=P1R*V5(KI,KPI,N)
      GO TO 459
  456 PR=PR*U6(KI,KPI,N)
      P1R=P1R*V6(KI,KPI,N)
  459 if (NIJ.GT.1) GO TO 460
      P1R=P1R*SQRT3H
      GO TO 468
  460 if (IRHO.NE.0) GO TO 461
      GO TO (461,462,463,464,465,466) J
  461 PR=PR*U1(KJ,KPJ,N)
      P1R=P1R*V1(KJ,KPJ,N)
      GO TO 468
  462 PR=PR*U2(KJ,KPJ,N)
      P1R=P1R*V2(KJ,KPJ,N)
      GO TO 468
  463 PR=PR*U3(KJ,KPJ,N)
      P1R=P1R*V3(KJ,KPJ,N)
      GO TO 468
  464 PR=PR*U4(KJ,KPJ,N)
      P1R=P1R*V4(KJ,KPJ,N)
      GO TO 468
  465 PR=PR*U5(KJ,KPJ,N)
      P1R=P1R*V5(KJ,KPJ,N)
      GO TO 468
  466 PR=PR*U6(KJ,KPJ,N)
      P1R=P1R*V6(KJ,KPJ,N)
  468 B=(2.0*R+1.0)*S6J(FLLRHO,FLLRHOP,R, FLLSIG,FLLSIGP,
     &  TK)
      if (MOD(R,2.0).GT.0.0) B=-B
      RSUM=RSUM+B*(PR+P1R)
  469 CONTINUE
      RSUM=-0.5*RSUM
C
      RETURN
      END


      SUBROUTINE LOOPFC(MN1,MN2,MN3,MN4,MN5,MN6,MN7,MN8,
     &                  MX1,MX2,MX3,MX4,MX5,MX6,MX7,MX8,
     &                      SJ2,SJ3,SJ4,SJ5,SJ6,SJ7,SJ8,
     &         NOSUBC,L,K, KJP,KMX,KS,  NALSJP, PJ,
     &         NSJK, scrJ, FJ, NALSJ )
      DIMENSION NALSJP(KJP,KS), PJ(KJP,KS),
     &         NSJK(110), scrJ(KMX,KS), FJ(KMX,KS), NALSJ(KMX,KS)

      NSJK(K)=0
      do 325 M1=MN1,MX1
      do 325 M2=MN2,MX2
      do 325 M3=MN3,MX3
      do 325 M4=MN4,MX4
      do 325 M5=MN5,MX5
      do 325 M6=MN6,MX6
      do 325 M7=MN7,MX7
      do 325 M8=MN8,MX8
      L=L+1
      NSJK(K)=NSJK(K)+1
      GO TO (324,323,322,321,318,317,316,315) NOSUBC
  315 scrJ(L,8) =SJ8
      FJ(L,8)   =PJ(M8,8)
      NALSJ(L,8)=NALSJP(M8,8)
  316 scrJ(L,7) =SJ7
      FJ(L,7)   =PJ(M7,7)
      NALSJ(L,7)=NALSJP(M7,7)
  317 scrJ(L,6) =SJ6
      FJ(L,6)   =PJ(M6,6)
      NALSJ(L,6)=NALSJP(M6,6)
  318 scrJ(L,5) =SJ5
      FJ(L,5)   =PJ(M5,5)
      NALSJ(L,5)=NALSJP(M5,5)
  321 scrJ(L,4) =SJ4
      FJ(L,4)   =PJ(M4,4)
      NALSJ(L,4)=NALSJP(M4,4)
  322 scrJ(L,3) =SJ3
      FJ(L,3)   =PJ(M3,3)
      NALSJ(L,3)=NALSJP(M3,3)
  323 scrJ(L,2) =SJ2
      FJ(L,2)   =PJ(M2,2)
      NALSJ(L,2)=NALSJP(M2,2)
  324 scrJ(L,1) =PJ(M1,1)
      FJ(L,1)   =PJ(M1,1)
      NALSJ(L,1)=NALSJP(M1,1)
  325 CONTINUE
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CALCFC
C
C    &     SET UP FINAL COEF MATRICES, AND CALC LS-JJ TRANSF MATRIX
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER STATE*11,IRREP*5,IIDAMII*5
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ANAM,IDLS,IDJJ,LI*1
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1,LH*1,LHS*1,LHQQ*1
      COMMON /CHAR/ LH(KS),LHS(KS),LHQQ(KMX)
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/CC(KISER/4,8),NPARJ(KC,KC)
      COMMON  NALSP(KLSC,KS),PscrL(KLSC,KS),PscrS(KLSC,KS),NCFGP(KLSC),
     &  NJK(KJK,KS),NCFGJP(KJP,KS)
      DIMENSION PC(KISER),V1(KLSI,KLSI,9)
      EQUIVALENCE (CT4,PC,V1)
      character line*130
C
C
C    &     CALC FINAL LEVELS AND COEF MATRICES
C
C     KS=8
C     KC=5
      J1X=NSCONF(2,KK)
      REWIND IL
      READ (IL) NOTSJ1,NscrJ4,SJ4MN,SJ4MX,NPAR,NPARJ,
     &  NOTSX, ((FL(L,I),S(L,I), L=1,NOTSX), I=1,NOSUBC),NALSJP,PJ
      if (ICTBCD.NE.0) WRITE (19,14)
   14 FORMAT (5X,1H )
C
C
      MAXNLS=0
      scrJ4=SJ4MN-1.0
      do 900 N=1,NscrJ4
      scrJ4=scrJ4+1.0
      REWIND 20
C
C    &     SET UP LS LEVELS
C
      L=0
      LB=0
      MX=0
      do 229 K=1,NDIFFT
        LF(K)=0
        MN=MX+1
        MX=MX+NTRMK(K)
        SJMN=ABS(PscrL(MN,NOSUBC)-PscrS(MN,NOSUBC))
        SJMX=PscrL(MN,NOSUBC)+PscrS(MN,NOSUBC)
        if (  scrJ4.LT.SJMN) GO TO 229
        if (  scrJ4.GT.SJMX) GO TO 229
        if (  scrj4.gt.conflimit(kk,ncfgp(mn),2) 
     &   .or. scrj4.lt.conflimit(kk,ncfgp(mn),1) ) goto 229
        do 219 M=MN,MX
          L=L+1
          LB=MIN(L,KMX)
          NCFG(LB)=NCFGP(M)
          do 215 I=1,NOSUBC
            NALS(LB,I)=NALSP(M,I)
            scrL(LB,I)=PscrL(M,I)
  215       scrS(LB,I)=PscrS(M,I)
  219   CONTINUE
        LF(K)=1
  229 CONTINUE
      NLS=L
      MAXNLS=MAX(MAXNLS,NLS)
      if (MAXNLS.GT.KMX) GOTO 900
C
C    &     SET UP JJ LEVELS
C
      L=0
      K=0
      J1=1
      MX1=0
      K1MX=NDIFFJ(1)
      do 349 K1=1,K1MX
      MN1=MX1+1
      MX1=MX1+NJK(K1,1)
      if (NALSJP(MX1,1).GT.NOTSJ1(1,J1+1)) J1=J1+1
      if (  scrj4.gt.conflimit(kk,j1,2)  
     & .or. scrj4.lt.conflimit(kk,j1,1) ) goto 349
      MX2=0
      K2MX=NDIFFJ(2)
      do 348 K2=1,K2MX
      MN2=MX2+1
      MX2=MX2+NJK(K2,2)
      J=NALSJP(MX2,2)
      if (J.LE.NOTSJ1(2,J1).OR.J.GT.NOTSJ1(2,J1+1)) GO TO 348
      MX3=0
      K3MX=NDIFFJ(3)
      do 347 K3=1,K3MX
      MN3=MX3+1
      MX3=MX3+NJK(K3,3)
      J=NALSJP(MX3,3)
      if (J.LE.NOTSJ1(3,J1).OR.J.GT.NOTSJ1(3,J1+1)) GO TO 347
      MX4=0
      K4MX=NDIFFJ(4)
      do 346 K4=1,K4MX
      MN4=MX4+1
      MX4=MX4+NJK(K4,4)
      J=NALSJP(MX4,4)
      if (J.LE.NOTSJ1(4,J1).OR.J.GT.NOTSJ1(4,J1+1)) GO TO 346
      MX5=0
      K5MX=NDIFFJ(5)
      do 345 K5=1,K5MX
      MN5=MX5+1
      MX5=MX5+NJK(K5,5)
      J=NALSJP(MX5,5)
      if (J.LE.NOTSJ1(5,J1).OR.J.GT.NOTSJ1(5,J1+1)) GO TO 345
      MX6=0
      K6MX=NDIFFJ(6)
      do 344 K6=1,K6MX
      MN6=MX6+1
      MX6=MX6+NJK(K6,6)
      J=NALSJP(MX6,6)
      if (J.LE.NOTSJ1(6,J1).OR.J.GT.NOTSJ1(6,J1+1)) GO TO 344
      MX7=0
      K7MX=NDIFFJ(7)
      do 343 K7=1,K7MX
      MN7=MX7+1
      MX7=MX7+NJK(K7,7)
      J=NALSJP(MX7,7)
      if (J.LE.NOTSJ1(7,J1).OR.J.GT.NOTSJ1(7,J1+1)) GO TO 343
      MX8=0
      K8MX=NDIFFJ(8)
      do 342 K8=1,K8MX
      MN8=MX8+1
      MX8=MX8+NJK(K8,8)
      J=NALSJP(MX8,8)
      if (J.LE.NOTSJ1(8,J1).OR.J.GT.NOTSJ1(8,J1+1)) GO TO 342
      SJ2=ABS(PJ(MN1,1)-PJ(MN2,2))-1.0
      NJ2=PJ(MN1,1)+PJ(MN2,2)-SJ2
      do 339 IJ2=1,NJ2
      SJ2=SJ2+1.0
      SJ3=ABS(SJ2-PJ(MN3,3))-1.0
      NJ3=SJ2+PJ(MN3,3)-SJ3
      do 339 IJ3=1,NJ3
      SJ3=SJ3+1.0
      SJ4=ABS(SJ3-PJ(MN4,4))-1.0
      NJ4=SJ3+PJ(MN4,4)-SJ4
      do 339 IJ4=1,NJ4
      SJ4=SJ4+1.0
      SJ5=ABS(SJ4-PJ(MN5,5))-1.0
      NJ5=SJ4+PJ(MN5,5)-SJ5
      do 339 IJ5=1,NJ5
      SJ5=SJ5+1.0
      SJ6=ABS(SJ5-PJ(MN6,6))-1.0
      NJ6=SJ5+PJ(MN6,6)-SJ6
      do 339 IJ6=1,NJ6
      SJ6=SJ6+1.0
      SJ7=ABS(SJ6-PJ(MN7,7))-1.0
      NJ7=SJ6+PJ(MN7,7)-SJ7
      do 339 IJ7=1,NJ7
      SJ7=SJ7+1.0
      SJ8=ABS(SJ7-PJ(MN8,8))-1.0
      NJ8=SJ7+PJ(MN8,8)-SJ8
      do 339 IJ8=1,NJ8
      SJ8=SJ8+1.0
      if (SJ8.NE.scrJ4) GO TO 339      
      K=K+1

      CALL       LOOPFC(MN1,MN2,MN3,MN4,MN5,MN6,MN7,MN8,
     &                  MX1,MX2,MX3,MX4,MX5,MX6,MX7,MX8,
     &                      SJ2,SJ3,SJ4,SJ5,SJ6,SJ7,SJ8,
     &         NOSUBC,L,K, KJP,KMX,KS,  NALSJP, PJ,
     &         NSJK, scrJ, FJ, NALSJ )

  339 CONTINUE
      CONTINUE
  342 CONTINUE
  343 CONTINUE
  344 CONTINUE
  345 CONTINUE
  346 CONTINUE
  347 CONTINUE
  348 CONTINUE
  349 CONTINUE
      NDIFSJ=K
      NJJ=L
      if (NJJ.NE.NLS) WRITE (IW,'(///A,I5,A,I5,A,F4.1)')
     &' NLS=',NLS,'   NJJ=',NJJ,'  FOR scrJ4=',scrJ4

      PRINT'(1X,A,A,F5.1,A,I6)' ,
     & STATE(kk,kk),' J =',scrJ4,'   DIMENSION=', NJJ

      if (IBUTLER().GT.0) THEN
C---   OUTPUT OF BASIS FUNCTIONS FOR BUTLER CRYSTAL FIELD CALCULATIONS
        IIDAMII=IRREP(scrJ4,IPRITY(KK))
        WRITE (IBUTLER(),'(5A,I6,A,I4)') ' IRREP ',STATE(kk,kk),' ',
     &  IIDAMII,                '  MULT ',NJJ,'   DIM ', INT(2*scrJ4+1)
      endif

C
C    &     CALC SUBSHELL TO BE USED IN BASIS-STATE LABELS
C
      do 390 J=1,J1X
      ISUBJ(J)=0
      ISUB=0
      do 380 I=1,NOSUBC
      I1=NOSUBC-I+1
      W=NIJK(I1,J,KK)
      WFM1=4.0*FLLIJK(I1,J,KK)+1.0
      if (W.EQ.0.0.OR.W.GT.WFM1) GO TO 380
      if (W.EQ.1.0.OR.W.EQ.WFM1) GO TO 370
      ISUBJ(J)=-I1
      GO TO 380
  370 if (ISUBJ(J).EQ.0.AND.I1.LT.ISUB) ISUBJ(J)=I1
      ISUB=I1
  380 CONTINUE
      if (ISUBJ(J).EQ.0) ISUBJ(J)=1
  390 CONTINUE
C
C    &     WRITE COMPLETE QUANTUM NUMBERS
C    &          AND FORM BASIS-STATE LABELS
C
      IPT=1
      if (KCPLD(1)+KCPLD(2).GT.1) IPT=0
      if (KCPLD(9).GE.9.OR.ISPECC.GE.9) IPT=0
      JJJ=MIN(J1X,9)
      if (IPT.EQ.0) GO TO 400
      if (JJJ+NLS*((KS+3)/4).GT.55.OR.ICFC.GT.0) WRITE (IW,37)
   37 FORMAT (1H1)
  400 WRITE(IW,38) scrJ4,(J,(LLIJK(I,J,KK),NIJK(I,J,KK),I=1,KS),J=1,JJJ)
   38 FORMAT (' J=',F4.1,10X,'CONFIGURATION',I10,4X,8(A1,I2,3X)/
     &  (30X,I10,4X,8(A1,I2,3X)))
      if (J1X.GT.9) WRITE (IW,39)  J1X, (LLIJK(I,J1X,KK),NIJK(I,J1X,KK),
     &  I=1,KS)
   39 FORMAT (30X,I10,4X,8(A1,I2,3X))
      WRITE (IW,40)
   40 FORMAT (7H ------/)
      do 470 L=1,NLS
        do 410 I=1,NOSUBC
          M=NALS(L,I)
          MUL(I)=MULT(M,I)
          LH(I)=LBCD(M,I)
          MULS(I)=2.0*scrS(L,I)+1.0
          LP1=scrL(L,I)+1.0
  410     LHS(I)=LSYMB(LP1)
        J=NCFG(L)
        I1=ABS(ISUBJ(J))
        MI=MUL(I1)
        LI=LH(I1)
        if (ISUBJ(J).ge.0) then
          MI=MULS(I1)
          LI=LHS(I1)
        endif
        WRITE(IDLS(L,1), '(I1,A1,A,I2,A1,A)')
     &      MI,LI,')',MULS(NOSUBC),LHS(NOSUBC),'  '
        if (IPT.ne.0) then
          if (NOSUBC.gt.3) then
            WRITE (IW,'(1x,I3,I3,8(2X,a,I3,I2,A1,a,I2,A1))')
     &      L, NCFG(L),('(',NALS(L,I),MUL(I),LH(I),')', MULS(I),LHS(I),
     &      I=1,NOSUBC)
          else
            WRITE (line,'(1x,I2,4X,3(a,I3,I3,A1,a,I3,A1,4X))')
     &         NCFG(L),('(',NALS(L,I),MUL(I),LH(I),')', MULS(I),LHS(I),
     &      I=1,NOSUBC)
          endif
        endif

        do 445 i=1,nosubc
          m = NALSJ(l,i)
          MUL(i) = MULT(m,i)
  445     LH (i) = LBCD(m,i)

        FJI=FJ(L,I1)
        if (ISUBJ(J).GT.0) FJI=scrJ(L,I1)
        WRITE(IDJJ(L,1),'(I1,A1,F4.1,A)')   MUL(I1), LH(I1), FJI,'  '
        if (IPT.ne.0 .and. NOSUBC.le.3) then
          WRITE (line(55:),'(i5,4X,3(2x,a,I3,I3,A1,F4.1,a,F4.1))')
     &    L,('(',NALSJ(L,I),MUL(I),LH(I),FJ(L,I),')',scrJ(L,I),
     &    I=1,NOSUBC)
          write(iw,'(a)') line
        endif
  470 CONTINUE

      if (IPT.ne.0 .and. nosubc.ge.4) then
        WRITE (IW,*)
        do 472 L=1,NLS
          do 471 I=1,NOSUBC
            M=NALSJ(L,I)
            MUL(I)=MULT(M,I)
  471       LH(I)=LBCD(M,I)
          WRITE (IW,'(I5,1X,8(a,I2,I2,A1,F4.1,a,F4.1))')
     &    L,(' (',NALSJ(L,I),MUL(I),LH(I),FJ(L,I),')',scrJ(L,I),
     &    I=1,NOSUBC)
  472   CONTINUE
        WRITE (IW,*)
      endif
C
C    &     CALC TRANSFORMATION MATRIX
C

      if (jjcoupling().ne.0) then
        do 530 LS=1,NLS
          do 530 JJ=1,NJJ
            TMX(LS,JJ)=0.0
            do 510 I=1,NOSUBC
              if (NALS(LS,I).NE.NALSJ(JJ,I)) GO TO 530
  510       CONTINUE
            TMX(LS,JJ) = 1
            do 521 I=2,NOSUBC
              ML=NALS(LS,I)
              if (  FL(ML,I)  .EQ.0.0.AND.   S(ML,I)  .EQ.0.0) GO TO 521
              if (scrL(LS,I-1).EQ.0.0.AND.scrS(LS,I-1).EQ.0.0) GO TO 521
              TMX(LS,JJ) = TMX(LS,JJ)
     &         * SQRT (  (2*scrL(LS,I)+1) * (2*scrS(LS,I)  +1)
     &                 * (2*  FJ(JJ,I)+1) * (2*scrJ(JJ,I-1)+1))
     &         * S9J
     &                ( scrL(LS,I-1), FL(ML,I), scrL(LS,I),
     &                  scrS(LS,I-1),  S(ML,I), scrS(LS,I),
     &                  scrJ(JJ,I-1), FJ(JJ,I), scrJ(JJ,I) )
  521       CONTINUE
  530   CONTINUE
      endif

      ILC=IL
      call sparse(tmx,nls,nls,kmx,lsparse, ixsparse,.false.)
c     ((TMX(L,LP),L=1,NLS), LP=1,NLS),
      do 601 NN=1,2
        WRITE (ILC) scrJ4,NLS,
     &  (X(i), i=ixsparse,ixsparse+lsparse-1),
     &  ((NALS(L,I),scrL(L,I),scrS(L,I),
     &  I=1,NOSUBC), NCFG(L),IDLS(L,1),IDJJ(L,1), L=1,NLS)
  601   ILC=IC
        call dealloc('calcfc',lsparse, ixsparse)
      NPAR=0
      if (ICTBCD.GT.0) THEN
        WRITE (19,61) N,NLS,scrJ4,  (LL(I),NI(I), I=1,4), COUPL(KCPL)
   61   FORMAT (2I6,6H     1,6X,F4.1, 12X,4(A1,I2,2X),5X,A6,9H COUPLING)
        NC=0
      endif
  604 NPARO=NPAR
      READ (20) NPAR,KPAR,K20,I,J,ANAM,CAVE,J1,NOPC, (PC(N1), N1=1,NOPC)
      if (NPAR.EQ.-7) GO TO 890
      if (ICTBCD.EQ.0) GO TO 609
      if (NPAR.EQ.NPARO+1) GO TO 609
      NN=-N
      NPAR20=NPAR
      NPAR=NPARO+1
  605 NC=NC+1
      PARNAM(NPAR,KK)='EAV'
      do 607 LP=1,NLS
        do 606 L=LP,NLS
  606     C(L,LP)=0.0
        C(LP,LP)=0.0
        if (NCFG(LP).EQ.NC) C(LP,LP)=1.0
  607 CONTINUE
      GO TO 870
  609 PARNAM(NPAR,KK)=ANAM
      J1A=J1
      J1B=J1
      do 611 L=1,NLS
      do 611 LP=1,L
  611   C(L,LP)=0.0
      if (KPAR.GT.0) GO TO 630
      if (KPAR.EQ.-2) GO TO 680
C
C    &     FK,GK MATRIX (LOWER TRIANGLE)
C
      LX=0
      LMX=0
      M=0
      do 629 K=1,NDIFFT
      LMX=LMX+NTRMK(K)
      if (NCFGP(LMX).EQ.J1
     &  .and. scrj4.ge.conflimit(kk,ncfgp(lmx),1) 
     &  .and. scrj4.le.conflimit(kk,ncfgp(lmx),2) ) goto 621
      if (LF(K).GT.0) LX=LX+NTRMK(K)
      GO TO 629
  621 if (LF(K).GT.0) GO TO 622
      M=M+(NTRMK(K)*(NTRMK(K)+1))/2
      GO TO 629
  622 LN=LX+1
      LX=LX+NTRMK(K)
      do 625 L=LN,LX
      do 625 LP=LN,L
      M=M+1
  625 C(L,LP)=PC(M)
  629 CONTINUE
      GO TO 690
C
C    &     ZETA MATRIX (LOWER TRIANGLE)
C
  630 if (ibutler().eq.0) then
        if (KCPL.EQ.2) GO TO 640
      endif
C    &     LS COUPLING
      J1V=0
      do 639 L=1,NLS
      J=NCFG(L)
      if (J.NE.J1) GO TO 639
      if (J1V.EQ.J) GO TO 632
      J1V=J
      LIJ=NOTSJ1(I,J)
      V1(1,1,2)=SQRT(1.5)
      EL=FLLIJK(I,J,KK)
      if (NIJK(I,J,KK).EQ.1) GO TO 632
      CALL LOCDSK(ID3,EL,NIJK(I,J,KK))
      NOR=(NORCD/2)+2
      do 631 M=1,NOR
c 631 READ (ID3) K, ((V1(II,JJ,2),II=1,NLT), JJ=1,NLT)
  631 call  getsparse ((id3), v1(1,1,2), nlt, nlt, klsi,.false.)
  632 do 638 LP=1,L
      if (NCFG(LP).NE.J) GO TO 638
      K=NOSUBC
      do 634 M=1,K
      if (M.EQ.I) GO TO 633
      if (NALS(L,M).NE.NALS(LP,M)) GO TO 638
      if (M.GT.I) GO TO 633
      if (scrL(L,M).NE.scrL(LP,M)) GO TO 638
      if (scrS(L,M).NE.scrS(LP,M)) GO TO 638
      GO TO 634
  633 if (ABS(scrL(L,M)-scrL(LP,M)).GT.1.0) GO TO 638
      if (ABS(scrS(L,M)-scrS(LP,M)).GT.1.0) GO TO 638
  634 CONTINUE
      ERAS=1.0
      II=scrL(LP,K)+scrS(L,K)+scrJ4
      if (IABG.GE.3) II=scrL(L,K)+scrS(LP,K)+scrJ4
      if (MOD(II,2).NE.0) ERAS=-1.0
      ERAS=ERAS*S6J(scrL(L,K),scrS(L,K),scrJ4,scrS(LP,K),scrL(LP,K),1.0)
      if (I.EQ.K) GO TO 636
      IP1=I+1
      do 635 M=IP1,K
      LT=NALS(L,M)
  635 ERAS=ERAS*UNCPLA(scrL(L,M-1),FL(LT,M),scrL(L,M),1.0,scrL(LP,M-1),
     &  scrL(LP,M))*UNCPLA(scrS(L,M-1),S(LT,M),scrS(L,M),1.0,
     &  scrS(LP,M-1),scrS(LP,M))
  636 LT=NALS(L,I)
      LR=NALS(LP,I)
      if (I.GT.1) ERAS=ERAS*UNCPLB(scrL(L,I-1),FL(LT,I),scrL(L,I),
     &  1.0,FL(LR,I),scrL(LP,I))*UNCPLB(scrS(L,I-1),S(LT,I),scrS(L,I),
     &  1.0,S(LR,I),scrS(LP,I))
      C(L,LP)=ERAS*SQRT(EL*(EL+1.0)*(2.0*EL+1.0))*V1(LT-LIJ,LR-LIJ,2)
  638 CONTINUE
  639 CONTINUE
      GO TO 690
C    &     JJ COUPLING
  640 KIX=NDIFFJ(I)
      LX=0
      do 679 K=1,NDIFSJ
      LN=LX+1
      LX=LX+NSJK(K)
      J=NALSJ(LX,I)
      if (NCFG(LN).NE.J1) GO TO 679
      ML=1
      MC=1
      do 643 KI=1,KIX
      if (NCFGJP(ML,I)-J1) 643,641,644
  641 if (PJ(ML,I)-FJ(LN,I)) 644,645,642
  642 MC=MC+(NJK(KI,I)*(NJK(KI,I)+1))/2
  643 ML=ML+NJK(KI,I)
  644 WRITE (IW,64) LN,I,FJ(LN,I),ML,MC
   64 FORMAT (//19H0CANNOT FIND PJ=FJ(,I2,1H,,I1,2H)=,F4.1,
     &  5X,3HML=,I4,5X,3HMC=,I4//)
  645 M1=NJK(KI,I)-1
      if (M1.GT.0) GO TO 655
C    &                                         (1 X 1  SUBMATRIX)
      do 653 LP=LN,LX
      C(LP,LP)=PC(MC)
  653 CONTINUE
      GO TO 679
C    &                                         (N X N  SUBMATRIX)
  655 do 656 L=LN,LX
      if (NALSJ(L,I).EQ.NALSJP(ML+1,I)) GO TO 657
  656 CONTINUE
  657 LDEL=L-LN
      do 669 L0=LN,LX
      if (NALSJ(L0,I).NE.NALSJP(ML,I)) GO TO 669
      M=MC
      L1=L0+LDEL*M1
      do 665 L=L0,L1,LDEL
      do 665 LP=L0,L,LDEL
      C(L,LP)=PC(M)
      M=M+1
  665 CONTINUE
  669 CONTINUE
  679 CONTINUE
      GO TO 690
C
C    &     RK MATRIX (LOWER TRIANGLE)
C
  680 J1A=I
      J1B=J
      LAMN=0
      LAMX=0
      LBMX=0
      do 682 L=1,NLS
      if (NCFG(L).LT.J1A) LAMN=L
      if (NCFG(L).EQ.J1A) LAMX=L
      if (NCFG(L).LT.J1B) LBMN=L
  682 if (NCFG(L).EQ.J1B) LBMX=L
      if (LAMX.EQ.0.OR.LBMX.EQ.0) GO TO 690
      LJ=LAMN
      LAMN=LAMN+1
      LBMN=LBMN+1
      LPJ=LBMN
C
      LN=NTOTTJ(J1A)+1
      LX=NTOTTJ(J1A+1)
      LPN=NTOTTJ(J1B)+1
      LPX=NTOTTJ(J1B+1)
      NOPC=0
      do 688 LP=LPN,LPX
      do 688 L=LN,LX
      ITEST=0
      if (PscrL(L,NOSUBC).NE.PscrL(LP,NOSUBC)) ITEST=1
      if (PscrS(L,NOSUBC).NE.PscrS(LP,NOSUBC)) ITEST=1
      if (ITEST.EQ.0) NOPC=NOPC+1
      if (ABS(PscrL(L,NOSUBC)-PscrS(L,NOSUBC)).GT.scrJ4) GO TO 688
      if (ABS(PscrL(LP,NOSUBC)-PscrS(LP,NOSUBC)).GT.scrJ4) GO TO 688
      if (PscrL(L,NOSUBC)+PscrS(L,NOSUBC).LT.scrJ4) GO TO 688
      if (PscrL(LP,NOSUBC)+PscrS(LP,NOSUBC).LT.scrJ4) GO TO 688
      LJ=LJ+1
      if (LJ.LE.LAMX) GO TO 686
      LJ=LAMN
      LPJ=LPJ+1
      if (LPJ.LE.LBMX) GO TO 686
      WRITE (IW,68) J1A,J1B,L,LP,LJ,LPJ,LAMN,LAMX,LBMN,LBMX,NOPC,
     &  PC(NOPC),LN,LX,LPN,LPX
   68 FORMAT (//17H0***LPJ.GT.LBMX  ,2I5,3X,4I5,3X,4I5,I10,F15.7,5X,4I5)
  686 if (ITEST.EQ.0) C(LPJ,LJ)=PC(NOPC)
  688 CONTINUE
C
C
C    &     COMPLETE MATRIX (UPPER TRIANGLE)
C
  690 do 691 L=1,NLS
      do 691 LP=1,L
  691 C(LP,L)=C(L,LP)
C
C    &     OUTPUT
C
      K=KC
      NNN=N
      CALL SPRIN
C
      CALL CLOK (TIME)
      PRINT'(F8.2,A,A)', TIME-TIME0,' MIN  ',ANAM
C
      call  putsparse ((ic), c, nls, nls, kmx,.true.)
C      WRITE (IC) ((C(L,LP),L=LP,NLS), LP=1,NLS)
      if (ICTBCD.EQ.0) GO TO 604
      NN=N
  870 do 880 LP=1,NLS
      do 879 L=LP,NLS
      if (ABS(C(L,LP)).LE.1.0E-9) GO TO 879
      if (KCPL.LE.1) GO TO 875
      WRITE (19,87) NN,LP,L,NPAR,C(L,LP), NALSJ(L,1),IDJJ(L,1),
     &  PARNAM(NPAR,1)
   87 FORMAT (4I6,F12.7, ' (',I3,2X,A7, 2X,A8,3X,3(A1,I2,2X) )
      GO TO 879
  875 WRITE(19,88) NN,LP,L,NPAR,C(L,LP), NALS(L,1),IDLS(L,1),
     &  PARNAM(NPAR,1)
   88 FORMAT (4I6,F12.7, ' (',I3,2X,A6,3X,A8,3X,3(A1,I2,2X) )
  879 CONTINUE
  880 CONTINUE
      if (NN.GT.0) GO TO 604
      NPARO=NPAR
      NPAR=NPAR+1
      if (NPAR.EQ.NPAR20) GO TO 609
      GO TO 605
C
  890 if (ICTBCD.NE.0)  WRITE (19,14)
      CALL CPL37
C
C
  900 CONTINUE
C    &      WRITE DUMMY RECORD, REQUIRED BY SYSTEM BUG (UPON BACKSPACE)
      WRITE (IL) (PC(I), I=1,20)
C
      REWIND 20
      REWIND IL
      CALL WARNDIM(MAXNLS,KMX,'KMX')
      if (ICTBCD.EQ.0) GO TO 950
      END FILE 19
      WRITE (IW,92) COUPL(KCPL)
   92 FORMAT (/44H0COEFFICIENT MATRICES WRITTEN ON TAPE 19 IN ,
     &  A6,9H COUPLING)
  950 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE CPL37
C
C    &     CALC TRANSF MATRICES FROM LS TO REPRESENTATIONS 3-7
C
C     IMPLICIT INTEGER*4 (I-N)
C
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      parameter(lheap=500000)
      common    /heap   / last,  max_used,  x(lheap)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,IDLS,IDJJ
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK,LH*1,LHS*1,LHQQ*1,LHS4*1
      COMMON /CHAR/ LH(KS),LHS(KS),LHQQ(KMX)
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/CC(KISER/4,8),NPARJ(KC,KC)
      DIMENSION IQL(KMX),IQM1L(KMX),IQM2L(KMX),scrJM1(KMX),scrJM2(KMX)
C
C
C    &   CALC TRANSFORMATION MATRICES FROM LS (KCPL=1) OR JJ (KCPL=2) TO
C    &          FIVE ADDITIONAL COUPLINGS (KCPL1=3,4,5,6,7)
C
  100 if (NOSUBC.LE.1.OR.NLS.LE.1) GO TO 980
      if (NLS.GT.NLSMAX) GO TO 980
      KCPL1=3
      if (KCPL.EQ.1) GO TO 101
      REWIND 2
      WRITE (2) ((TMX(L,LP),L=1,NLS), LP=1,NLS)
  101 NMAX=NSCONF(2,KK)
      do 107 NC=1,NMAX
      IQ=0
      IQM1=0
      IQM2=0
      do 104 I=1,NOSUBC
      J=NOSUBC-I+1
      IERAS=4.0*FLLIJK(J,NC,KK)+2.0
      if (NIJK(J,NC,KK).EQ.0.OR.NIJK(J,NC,KK).EQ.IERAS) GO TO 104
      if (IQ.NE.0) GO TO 102
      IQ=J
      GO TO 104
  102 if (IQM1.NE.0) GO TO 103
      IQM1=J
      GO TO 104
  103 if (IQM2.NE.0) GO TO 105
      IQM2=J
      GO TO 105
  104 CONTINUE
  105 IQM1=MAX(1,IQM1)
      IQM2=MAX(1,IQM2)
      do 106 L=1,NLS
      if (NCFG(L).NE.NC) GO TO 106
      IQL(L)=IQ
      IQM1L(L)=IQM1
      IQM2L(L)=IQM2
  106 CONTINUE
  107 CONTINUE
      if (IABG.LT.3) GO TO 110
      do 109 L=1,NLS
      M=NOSUBC
      I=scrL(L,M)+scrS(L,M)-scrJ4
      if (MOD(I,2).NE.0) GO TO 109
      do 108 LP=1,NLS
  108 TMX(L,LP)=-TMX(L,LP)
  109 CONTINUE
C
  110 do 111 L=1,NLS
  111 LF(L)=0
      if (KCPLD(KCPL1).LE.0) GO TO 200
      if (KCPL1.NE.4) GO TO 120
      if (KCPLD(5)) 200,200,970
  120 if (KCPL1.NE.6) GO TO 970
  121 if (KCPLD(7).GT.0) GO TO 970
  200 GO TO (100,100,300,400,500,600,700) KCPL1
C
C    &     JJJK COUPLING (KCPL1=3)
C
  300 do 349 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      scrJM1(L)=scrJ(L,IQM1)
      if (LF(L).NE.0) GO TO 349
      LF(L)=L
      NQ=NALSJ(L,IQ)
      FKK=MIN(scrJ(L,IQM1)+FL(NQ,IQ), scrJ4+S(NQ,IQ))
      FKJ(L)=FKK
      if (L.GE.NLS) GO TO 349
      L1=L+1
      do 339 LP=L1,NLS
      do 325 I=1,IQ
      if (NALSJ(L,I).NE.NALSJ(LP,I)) GO TO 339
  325 CONTINUE
      do 330 I=1,IQM1
      if (FJ(L,I).NE.FJ(LP,I)) GO TO 339
      if (scrJ(L,I).NE.scrJ(LP,I)) GO TO 339
  330 CONTINUE
      LF(LP)=L
      FKK=FKK-1.0
      FKJ(LP)=FKK
  339 CONTINUE
  349 CONTINUE
C
      do 369 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      NQ=NALSJ(L,IQ)
      do 368 LP=1,NLS
      CT4(L,LP)=TMX(L,LP)
      C(L,LP)=0.0
      IF(NCFG(L).NE.NCFG(LP)) GO TO 368
      if (LF(L).NE.LF(LP)) GO TO 368
      I=scrJ(L,IQM1)+FL(NQ,IQ)+S(NQ,IQ)+scrJ4+0.001
      A=SQRT((2.0*FKJ(LP)+1.0)*(2.0*FJ(L,IQ)+1.0))
      if (MOD(I,2).NE.0) A=-A
      B=S6J(FKJ(LP),S(NQ,IQ),scrJ4, FJ(L,IQ),scrJ(L,IQM1),FL(NQ,IQ))
      C(L,LP)=A*B
  368 CONTINUE
  369 CONTINUE
      GO TO 900
C
C    &     LSLK COUPLING (KCPL1=4)
C
  400 do 449 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      if (LF(L).NE.0) GO TO 449
      LF(L)=L
      NQ=NALS(L,IQ)
      FKK=MIN(scrL(L,IQ)+scrS(L,IQM1), scrJ4+S(NQ,IQ))
      FK(L)=FKK
      if (L.GE.NLS) GO TO 449
      L1=L+1
      do 439 LP=L1,NLS
      do 425 I=1,IQ
      if (NALS(L,I).NE.NALS(LP,I)) GO TO 439
  425 CONTINUE
      if (scrL(L,IQ).NE.scrL(LP,IQ)) GO TO 439
      do 430 I=1,IQM1
      if (scrL(L,I).NE.scrL(LP,I)) GO TO 439
      if (scrS(L,I).NE.scrS(LP,I)) GO TO 439
  430 CONTINUE
      LF(LP)=L
      FKK=FKK-1.0
      FK(LP)=FKK
  439 CONTINUE
  449 CONTINUE
C
      do 469 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      do 468 LP=1,NLS
      TMX(L,LP)=0.0
      IF(NCFG(L).NE.NCFG(LP)) GO TO 468
      if (LF(L).NE.LF(LP)) GO TO 468
      NQ=NALS(L,IQ)
      I=scrL(L,IQ)+scrS(L,IQM1)+S(NQ,IQ)+scrJ4+0.001
      A=SQRT((2.0*FK(LP)+1.0)*(2.0*scrS(L,IQ)+1.0))
      if (MOD(I,2).NE.0) A=-A
      B=S6J(FK(LP),S(NQ,IQ),scrJ4, scrS(L,IQ),scrL(L,IQ),scrS(L,IQM1))
      TMX(L,LP)=A*B
  468 CONTINUE
  469 CONTINUE
      GO TO 930
C
C    &     LSJK COUPLING (KCPL1=5)
C
  500 do 549 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      if (LF(L).NE.0) GO TO 549
      LF(L)=L
      NQ=NALS(L,IQ)
      FJJ=MIN(scrL(L,IQM1)+scrS(L,IQM1), FL(NQ,IQ)+FK(L))
      scrJ(L,IQM1)=FJJ
      scrJM1(L)=FJJ
      if (L.GE.NLS) GO TO 549
      L1=L+1
      do 539 LP=L1,NLS
      do 525 I=1,IQ
      if (NALS(L,I).NE.NALS(LP,I)) GO TO 539
  525 CONTINUE
      if (FK(L).NE.FK(LP)) GO TO 539
      do 530 I=1,IQM1
      if (scrL(L,I).NE.scrL(LP,I)) GO TO 539
      if (scrS(L,I).NE.scrS(LP,I)) GO TO 539
  530 CONTINUE
      LF(LP)=L
      FJJ=FJJ-1.0
      scrJ(LP,IQM1)=FJJ
      scrJM1(LP)=FJJ
  539 CONTINUE
  549 CONTINUE
C
      do 569 L=1,NLS
      IQM1=IQM1L(L)
      IQ=MAX(IQL(L),IQM1+1)
      do 568 LP=1,NLS
      CT4(L,LP)=TMX(L,LP)
      C(L,LP)=0.0
      IF(NCFG(L).NE.NCFG(LP)) GO TO 568
      if (LF(L).NE.LF(LP)) GO TO 568
      NQ=NALS(LP,IQ)
      I=scrL(L,IQ)+scrJ(LP,IQM1)+FL(NQ,IQ)+scrS(L,IQM1)+0.001
      A=SQRT((2.0*scrL(L,IQ)+1.0)*(2.0*scrJ(LP,IQM1)+1.0))
      if (MOD(I,2).NE.0) A=-A
      B=S6J(scrL(L,IQ),scrS(L,IQM1),FK(L), scrJ(LP,IQM1),FL(NQ,IQ),
     &  scrL(L,IQM1))
      C(L,LP)=A*B
  568 CONTINUE
  569 CONTINUE
      GO TO 900
C
C    &     LSJLKS COUPLING (KCPL1=6)
C
C    &                  CALC QU. NOS. FOR KCPL1=6 AND 7 BOTH
  600 do 646 L=1,NLS
      IQM2=IQM2L(L)
      IQM1=MAX(IQM1L(L),IQM2+1)
      IQ=MAX(IQL(L),IQM1+1)
      if (LF(L).NE.0) GO TO 646
      L1=L
      NQM1=NALS(L,IQM1)
      NQ=NALS(L,IQ)
      NJQ=2.0*MIN(scrL(L,IQM2),scrS(L,IQM2))+1.0
      NLQ=2.0*MIN(FL(NQM1,IQM1),FL(NQ,IQ))+1.0
      NSQ=2.0*MIN(S(NQM1,IQM1),S(NQ,IQ))+1.0
      SJQ=scrL(L,IQM2)+scrS(L,IQM2)
      do 645 IJQ=1,NJQ
      SLQ=FL(NQM1,IQM1)+FL(NQ,IQ)
      do 644 ILQ=1,NLQ
      SSQ=S(NQM1,IQM1)+S(NQ,IQ)
      do 643 ISQ=1,NSQ
      FKK=SJQ+SLQ+1.0
      FKMN=ABS(SJQ-SLQ)
      FJJ=SLQ+SSQ+1.0
      FJMN=ABS(SLQ-SSQ)
  602 FKK=FKK-1.0
      if (FKK.LT.FKMN) GO TO 643
      if (FKK+SSQ-scrJ4) 643,608,606
  606 if (ABS(FKK-SSQ).GT.scrJ4) GO TO 602
  608 FJJ=FJJ-1.0
      if (FJJ.LT.FJMN) GO TO 643
      if (FJJ+SJQ-scrJ4) 643,615,612
  612 if (ABS(FJJ-SJQ).GT.scrJ4) GO TO 608
  615 LF(L1)=L
      scrJ(L1,IQM2)=SJQ
      scrJM2(L1)=SJQ
      scrL6(L1)=SLQ
      FK6(L1)=FKK
      scrS6(L1)=SSQ
      scrJ(L1,IQM1)=FJJ
      scrJM1(L1)=FJJ
      I=SLQ+1.0
      LHQQ(L1)=LSYMB(I)
      MULTQQ(L1)=2.0*SSQ+1.0
      if (L1.GE.NLS) GO TO 646
      L11=L1+1
      do 630 LP=L11,NLS
      do 622 I=1,IQ
      if (NALS(LP,I).NE.NALS(L,I)) GO TO 630
  622 CONTINUE
      do 625 I=1,IQM2
      if (scrL(LP,I).NE.scrL(L,I)) GO TO 630
      if (scrS(LP,I).NE.scrS(L,I)) GO TO 630
  625 CONTINUE
      L1=LP
      GO TO 602
  630 CONTINUE
      GO TO 646
  643 SSQ=SSQ-1.0
  644 SLQ=SLQ-1.0
  645 SJQ=SJQ-1.0
  646 CONTINUE
C
      do 669 L=1,NLS
      IQM2=IQM2L(L)
      IQM1=MAX(IQM1L(L),IQM2+1)
      IQ=MAX(IQL(L),IQM1+1)
      NQM1=NALS(L,IQM1)
      NQ=NALS(L,IQ)
      A1=scrL(L,IQM2)
      A2=scrS(L,IQM2)
      A4=scrL(L,IQM1)
      A5=scrS(L,IQM1)
      A6=scrL(L,IQ)
      A7=scrS(L,IQ)
      A8=FL(NQM1,IQM1)
      A9= S(NQM1,IQM1)
      A10=FL(NQ,IQ)
      A11= S(NQ,IQ)
      do 668 LP=1,NLS
      TMX(L,LP)=0.0
      IF(NCFG(L).NE.NCFG(LP)) GO TO 668
      if (LF(L).NE.LF(LP)) GO TO 668
      A3=scrJ(LP,IQM2)
      A12=FK6(LP)
      A13=scrL6(LP)
      A14=scrS6(LP)
      I=A1+A2+A3+A6+A7-scrJ4+A8+A9+A10+A11+A13-A14
      A=SQRT((2.0*A4+1.0)*(2.0*A5+1.0)*(2.0*A6+1.0)*(2.0*A7+1.0)
     &      *(2.0*A13+1.0)*(2.0*A14+1.0)*(2.0*A12+1.0)*(2.0*A3+1.0))
      if (MOD(I,2).NE.0) A=-A
      B=S6J(A1,A8,A4, A10,A6,A13) * S6J(A2,A9,A5, A11,A7,A14)
     &  *S6J(A12,A14,scrJ4, A7,A6,A2) * S6J(A6,A2,A12, A3,A13,A1)
      TMX(L,LP)=A*B
  668 CONTINUE
  669 CONTINUE
      GO TO 930
C
C    &     LSJ(LSJ) COUPLING (KCPL1=7)
C
  700 do 769 L=1,NLS
      IQM2=IQM2L(L)
      IQM1=MAX(IQM1L(L),IQM2+1)
      IQ=MAX(IQL(L),IQM1+1)
      A1=scrJ(L,IQM2)
      A2=scrL6(L)
      A3=FK6(L)
      A4=scrS6(L)
      do 768 LP=1,NLS
      CT4(L,LP)=TMX(L,LP)
      C(L,LP)=0.0
      IF(NCFG(L).NE.NCFG(LP)) GO TO 768
      if (LF(L).NE.LF(LP)) GO TO 768
      if (A1.NE.scrJ(LP,IQM2)) GO TO 768
      if (A2.NE.scrL6(LP)) GO TO 768
      if (A4.NE.scrS6(LP)) GO TO 768
      A6=scrJ(LP,IQM1)
      I=A1+A2+A4+scrJ4
      A=SQRT((2.0*A3+1.0)*(2.0*A6+1.0))
      if (MOD(I,2).NE.0) A=-A
      B=S6J(A1,A2,A3, A4,scrJ4,A6)
      C(L,LP)=A*B
  768 CONTINUE
  769 CONTINUE
      GO TO 900
C
C    &     OUTPUT
C
  900 do 910 L=1,NLS
      do 910 LP=1,NLS
      TMX(L,LP)=0.0
      do 905 K=1,NLS
  905 TMX(L,LP)=TMX(L,LP)+CT4(L,K)*C(K,LP)
  910 CONTINUE
C
  930 if (IABG.LT.3) GO TO 935
      do 934 L=1,NLS
      M=NOSUBC
      I=scrL(L,M)+scrS(L,M)-scrJ4
      if (MOD(I,2).NE.0) GO TO 934
      do 933 LP=1,NLS
  933 TMX(L,LP)=-TMX(L,LP)
  934 CONTINUE
  935 if (KCPL.EQ.1) GO TO 939
      if (KCPL1.EQ.5.OR.KCPL1.EQ.7) GO TO 939
C    &     TRANSFORM FROM LS TO JJ REPRESENTATION
      REWIND 2
      READ (2) ((C(L,LP),L=1,NLS), LP=1,NLS)
      do 936 L=1,NLS
      do 936 LP=1,NLS
      CT4(L,LP)=TMX(L,LP)
  936 CONTINUE
      do 938 L=1,NLS
      do 938 LP=1,NLS
      TMX(L,LP)=0.0
      do 937 LPP=1,NLS
  937 TMX(L,LP)=TMX(L,LP)+C(LPP,L)*CT4(LPP,LP)
  938 CONTINUE
  939 if (KCPLD(KCPL1).NE.0) GO TO 970

        call sparse(tmx,nls,nls,kmx,lsparse, ixsparse,.false.)
        WRITE (IC) 
     & (X(i), i=ixsparse,ixsparse+lsparse-1),
     & ( FK(L),FKJ(L),scrJM1(L),
     &  scrJM2(L),scrL6(L),FK6(L),scrS6(L),LHQQ(L),MULTQQ(L),L=1,NLS)
        call dealloc('cpl37',lsparse, ixsparse)

C
      if (ICFC.EQ.0) GO TO 970
      WRITE (IW,91) COUPL(KCPL),COUPL(KCPL1)
   91 FORMAT (9H0TMX FROM, A6, 5H  TO ,A6,9H COUPLING//)
      LX=0
  940 LN=LX+1
      LX=LX+11
      if (LX.GT.NLS) LX=NLS
      GO TO (943,943,943,944,945,946,947) KCPL1
  943 WRITE (IW,93) (scrJM1(L),FKJ(L), L=LN,LX)
   93 FORMAT (15X,13(F4.1,1HK,F4.1))
      GO TO 950
  944 LP1=scrL(L,NOSUBC)+1.0
      LHS4=LSYMB(LP1)
      WRITE (IW,94) (LHS4,FK(L), L=LN,LX)
   94 FORMAT (15X,13(2X,A1,1H(,F4.1,1H)))
      GO TO 950
  945 WRITE (IW,93) (scrJM1(L),FK(L), L=LN,LX)
      GO TO 950
  946 WRITE (IW,96) (scrJM2(L),LHQQ(L),FK6(L),MULTQQ(L), L=LN,LX)
   96 FORMAT (15X,13(F4.1,A1,F3.1,I1))
      GO TO 950
  947 WRITE (IW,97) (scrJM2(L),MULTQQ(L),LHQQ(L),scrJM1(L),
     &    L=LN,LX)
   97 FORMAT (15X,13(F4.1,I1,A1,F3.1))
  950 do 955 L=1,NLS
      if (KCPL.EQ.1) GO TO 953
      WRITE (IW,95) NCFG(L),IDJJ(L,1), L, (TMX(L,LP), LP=LN,LX)
   95 FORMAT (I3,' (',A7,I3,13F9.5)
      GO TO 955
  953 WRITE (IW,99) NCFG(L),IDLS(L,1), L, (TMX(L,LP), LP=LN,LX)
   99 FORMAT (I3,' (',A6,I3,1X,13F9.5)
  955 CONTINUE
      if (LX.LT.NLS) GO TO 940
C
  970 KCPL1=KCPL1+1
      if (KCPL1.LE.4) GO TO 110
      if (KCPL1-6) 972,110,973
  972 if (NOSUBC-3) 980,110,110
  973 if (KCPL1-7) 110,121,980
  980 CALL CLOK(TIME)
      DELT=TIME-TIME0
      WRITE (IW,98) DELT
   98 FORMAT (11X,5HTIME=,F8.2,4H MIN)
      RETURN
      END



C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      subroutinE SPIN ( ishell , kpari )
C
C
C -----   Compute matrix of S working on shell "ishell" only
C -----   in state kpari (1=first, 2=second parity)
C
C     implicit integer*4 (I-N)
      parameter (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      common/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,Il,IC,IE
      character*8 IDLS,IDJJ
      common /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      common/C1/KCPl,NOCSET,NSCONF(3,2),IABG,IV,NLSmax,NLT11,NEVmax,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  nosubc,kk,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,sJ4min,sJ4max,NscrJ4,scrJ4,scrJ4P,NLS,NLSp,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      common /C2/ NOTSJP(KS,100),NCFGP(KMX),naLSp(KMX,KS)

      common /C3/ ISUBJ(KC),MULS(KS),
     &            naLS(KMX,KS),NCFG(KMX),scrL(KMX,KS,2),scrS(KMX,KS,2)

      common/C3LC2/TMX (KMX,KMX)

      common/C3LC4/TMXp(KMX,KMX), NPARJ(KC,KC)

      common    C(KMX,KMX),
     &          VPAR(KPR,2), AVP(9,2), AVEIG(2), AVCG(2),
     &          NPARK(2), NPAV(KC), EIG(KMX), FNT(9,2), I1, I2,
     &          FACT(4),
     &          FL(KMX,KS,2),S(KMX,KS,2),CFP2(17,17)
      character matname*5 , state*11
      logical forbid
      forbid (p1,p2,p3) = ( p1+p2 .lt. p3 ) .or. ( abs(p1-p2) .gt. p3 )
C
      write(IW,*) ' SPIN   called (', ishell, ')'
      ifile = kpari + ILA-1
c---  no matrix elements when there are no states
      if (nsconf(2,kpari).eq.0) return
C
      R = 1
      rewind ifile
      read (ifile) NOTSJ1,NscrJ4,sJ4min,sJ4max,NPAR,NPARJ,
     &  NOTSX, ((FL(l,I,1),S(l,I,1), L=1,NOTSX), I=1,nosubc)
      sJ4pos = -1

      do 600 scrJ4 = sJ4min, sJ4max

      call getbas( scrJ4, 1, nls, TMX, naLS, scrL, scrS,
     &   ncfg, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)

      do 500 scrJ4p = sJ4min, sJ4max
      if ( forbid ( scrJ4, 1., scrJ4p ) ) goto 500
      call getbas( scrJ4p, 2, nlsp, TMXp, naLSp, scrL, scrS,
     &   ncfgp, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)
C
C
      do 400 l  = 1 , NLS
      do 300 lp = 1 , NLSp
        C(l,LP)=0

C                        TEST SELECTION RULES
        if (NCFG(l).ne.NCFGp(lp)) go to 300
        if (forbid(S(naLS(l,ishell),ishell,1), 1.,
     &                      S(naLSp(lp,ishell),ishell,1)) ) go to 300
        do 100 isubc = 1, nosubc
          if (scrL(l,isubc,1) .ne. scrL (lp,isubc,2)) go to 300
          if (naLS(l,isubc)   .ne. naLSp(lp,isubc)  ) go to 300
          if (isubc .ge. ishell) then
            if (forbid(scrS(l,isubc,1),1.,scrS(lp,isubc,2))) go to 300
          else
            if ( scrS(l,isubc,1) .ne. scrS(lp,isubc,2)) go to 300
          endif
  100   continue

c     Calculate matrix element
        Tc = Uncplb
     &      ( scrL(l,nosubc,1), scrS(l,nosubc,1), scrJ4,
     &                      R, scrS(lp,nosubc,2), scrJ4p)

        do 200 m = ishell+1, nosubc
          Tc = Tc *
     &     Uncpla
     &      ( scrS(l,m-1,1), S(naLS(l,m),m,1), scrS(l,m,1),
     &                      R, scrS(lp,m-1,2), scrS(lp,m,2) )
  200   continue

        naLSli = naLS( l, ishell)
        naLSlpi= naLSp(lp,ishell)
        if (ishell .gt. 1) then
          Tc = Tc *
     &     Uncplb
     &      ( scrS(l,ishell-1,1), S(naLSli,ishell,1), scrS(l,ishell,1),
     &                        R, S(naLSlpi,ishell,1), scrS(lp,ishell,2))

        endif
c
        Si = S(naLSli ,ishell,1)
        Tc = Tc * sqrt( Si*(Si+1)*(2*Si+1) )
        C(l,lp) = Tc
C
  300 continue
  400 continue
C
      write(matname,'(a,i1)') 'SPIN',ishell
      call BUTLER(matname,kpari,kpari,C, scrJ4,IPRITY(kpari), R,1,
     &            scrJ4p,IPRITY(kpari), NLS,NLSp,KMX)
  500 continue
  600 continue

      call CLOK (TIME)
      print'(A,F8.2,3X,A,I1,3X,A)', ' time=',TIME-TIME0,
     &'  spin  shell',ishell, STATE(kpari,kpari)
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      subroutinE orbit ( ishell , kpari )
C
C
C -----   Compute matrix of L working on shell "ishell" only
C -----   in state kpari (1=first, 2=second parity)
C
C     implicit integer*4 (I-N)
      parameter (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      common/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,Il,IC,IE
      character*8 IDLS,IDJJ
      common /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      common/C1/KCPl,NOCSET,NSCONF(3,2),IABG,IV,NLSmax,NLT11,NEVmax,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  nosubc,kk,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,sJ4min,sJ4max,NscrJ4,scrJ4,scrJ4P,NLS,NLSp,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      common /C2/ NOTSJP(KS,100),NCFGP(KMX),naLSp(KMX,KS)

      common /C3/ ISUBJ(KC),MULS(KS),
     &            naLS(KMX,KS),NCFG(KMX),scrL(KMX,KS,2),scrS(KMX,KS,2)

      common/C3LC2/TMX (KMX,KMX)

      common/C3LC4/TMXp(KMX,KMX), NPARJ(KC,KC)

      common    C(KMX,KMX),
     &          VPAR(KPR,2), AVP(9,2), AVEIG(2), AVCG(2),
     &          NPARK(2), NPAV(KC), EIG(KMX), FNT(9,2), I1, I2,
     &          FACT(4),
     &          FL(KMX,KS,2),S(KMX,KS,2),CFP2(17,17)
      character matname*6 , state*11
      logical forbid
      forbid (p1,p2,p3) = ( p1+p2 .lt. p3 ) .or. ( abs(p1-p2) .gt. p3 )
C
      write(IW,*) ' orbit   called (', ishell, ')'
      ifile = kpari + ILA-1
c---  no matrix elements when there are no states
      if (nsconf(2,kpari).eq.0) return
C
      R  = 1
      rewind ifile
      read (ifile) NOTSJ1,NscrJ4,sJ4min,sJ4max,NPAR,NPARJ,
     &  NOTSX, ((FL(l,I,1),S(l,I,1), L=1,NOTSX), I=1,nosubc)
      sJ4pos = -1

      do 600 scrJ4 = sJ4min, sJ4max

      call getbas( scrJ4, 1, nls, TMX, naLS, scrL, scrS,
     &   ncfg, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)

      do 500 scrJ4p = sJ4min, sJ4max
      if ( forbid ( scrJ4, 1., scrJ4p ) ) goto 500
      call getbas( scrJ4p, 2, nlsp, TMXp, naLSp, scrL, scrS,
     &   ncfgp, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)
C
C
      do 400 l  = 1 , NLS
      do 300 lp = 1 , NLSp
        C(l,LP)=0.0

C                        TEST SELECTION RULES
        if (NCFG(l).ne.NCFGp(lp)) go to 300
        if (forbid(fL(naLS(l,ishell),ishell,1), R,
     &                      fL(naLSp(lp,ishell),ishell,1)) ) go to 300
        do 100 isubc = 1, nosubc
          if (scrS(l,isubc,1) .ne. scrS (lp,isubc,2)) go to 300
          if (naLS(l,isubc)   .ne. naLSp(lp,isubc)  ) go to 300
          if (isubc .ge. ishell) then
            if (forbid(scrL(l,isubc,1),R ,scrL(lp,isubc,2))) go to 300
          else
            if ( scrL(l,isubc,1) .ne. scrL(lp,isubc,2)) go to 300
          endif
  100   continue

c     Calculate matrix element
        Tc = Uncpla
     &      ( scrL(l,nosubc,1), scrS(l,nosubc,1), scrJ4,
     &                      R, scrL(lp,nosubc,2), scrJ4p)

        do 200 m = ishell+1, nosubc
          Tc = Tc *
     &     Uncpla
     &      ( scrL(l,m-1,1), fL(naLS(l,m),m,1), scrL(l,m,1),
     &                      R, scrL(lp,m-1,2), scrL(lp,m,2) )
  200   continue

        naLSli = naLS( l, ishell)
        naLSlpi= naLSp(lp,ishell)
        if (ishell .gt. 1) then
          Tc = Tc *
     &     Uncplb
     &     ( scrL(l,ishell-1,1), fL(naLSli,ishell,1), scrL(l,ishell,1),
     &                       R, fL(naLSlpi,ishell,1), scrL(lp,ishell,2))
        endif
c
        fLi = fL(naLSli ,ishell,1)
        Tc = Tc * sqrt( fLi*(fLi+1)*(2*fLi+1) )
        C(l,lp) = Tc
C
  300 continue
  400 continue
C
      write(matname,'(a,i1)') 'ORBIT',ishell
      call BUTLER(matname,kpari,kpari,C, scrJ4,IPRITY(kpari), R,1,
     &            scrJ4p,IPRITY(kpari), NLS,NLSp,KMX)
  500 continue
  600 continue

      call CLOK (TIME)
      print'(A,F8.2,3X,A,I1,3X,A)', ' time=',TIME-TIME0,
     &' orbit  shell',ishell, STATE(kpari,kpari)
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*
 
      subroutinE tensor ( ishell ,ixyz, kpari ,v )
C
C
C -----   Compute matrix of Ux or Vx1z working on shell "ishell" only
C -----   in state kpari (1=first, 2=second parity)
C
C     implicit integer*4 (I-N)
      parameter (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)

      dimension ixyz(3), v(klsi,klsi)

      common/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,Il,IC,IE
      character*8 IDLS,IDJJ
      common /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      common/C1/KCPl,NOCSET,NSCONF(3,2),IABG,IV,NLSmax,NLT11,NEVmax,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  nosubc,kk,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,sJ4min,sJ4max,NscrJ4,scrJ4,scrJ4P,NLS,NLSp,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      common /C2/ NOTSJP(KS,100),NCFGP(KMX),naLSp(KMX,KS)
 
      common /C3/ ISUBJ(KC),MULS(KS),
     &            naLS(KMX,KS),NCFG(KMX),scrL(KMX,KS,2),scrS(KMX,KS,2)
 
      common/C3LC2/TMX (KMX,KMX)
 
      common/C3LC4/TMXp(KMX,KMX), NPARJ(KC,KC)
 
      common    C(KMX,KMX),
     &          VPAR(KPR,2), AVP(9,2), AVEIG(2), AVCG(2),
     &          NPARK(2), NPAV(KC), EIG(KMX), FNT(9,2), I1, I2,
     &          FACT(4),
     &          FL(KMX,KS,2),S(KMX,KS,2),CFP2(17,17)
      character matname*10 , state*11
      logical forbid
      forbid (p1,p2,p3) = ( p1+p2 .lt. p3 ) .or. ( abs(p1-p2) .gt. p3 )
C
      write(matname,'(a,4i1)') 'TENSOR',ixyz,ishell
      write(IW,*) 'tensor called for ',matname
      ifile = kpari + ILA-1
c---  no matrix elements when there are no states
      if (nsconf(2,kpari).eq.0) return
C
      rx = ixyz(1)
      ry = ixyz(2)
      rz = ixyz(3)

      if ( ry.eq.0 ) then
        factorUVtoW = sqrt (  2*rx+1 )
      else
        factorUVtoW = sqrt ( (2*rx+1) *2 )
      endif

      rewind ifile
      read (ifile) NOTSJ1,NscrJ4,sJ4min,sJ4max,NPAR,NPARJ,
     &  NOTSX, ((FL(l,I,1),S(l,I,1), L=1,NOTSX), I=1,nosubc)
      sJ4pos = -1
      iconf = 0
  
      do 600 scrJ4 = sJ4min, sJ4max
 
      call getbas( scrJ4, 1, nls, TMX, naLS, scrL, scrS,
     &   ncfg, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)
 
      do 500 scrJ4p = sJ4min, sJ4max
      if ( forbid ( scrJ4, rz, scrJ4p ) ) goto 500
      call getbas( scrJ4p, 2, nlsp, TMXp, naLSp, scrL, scrS,
     &   ncfgp, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)
C
C
      do 400 l  = 1 , NLS
       if ( ncfg(l) .ne. iconf) then
c        load reduced matrix elements for this configuration from id3.
         iconf = ncfg(l)
         n00LS = notsj1(ishell,iconf)
         el = fllijk(ishell,iconf,kpari)
         nn = nijk  (ishell,iconf,kpari)
         if (nn.ne.0 .and. nn.ne.nint(4*el+2) ) then
           call locdsk( id3, el, nijk(ishell,iconf,kpari) )
           do 50 iskip = 1, rx + ry * (2*el+1)
  50         read(id3)
c          read (id3) idum, ((v(i,j), i=1,nlt),j=1,nlt)
           call  getsparse ((id3), v, nlt, nlt, klsi,.false.)
         else if (rx+ry+rz.eq.0) then
           v(1,1) = nn / sqrt( 2*el+1 )
         else 
           v(1,1) = 0
         endif 
       endif

       do 300 lp = 1 , NLSp
        C(l,lp)=0.0
 
C                        TEST SELECTION RULES
        if (NCFG(l).ne.NCFGp(lp)) go to 300
        if (forbid(fL(naLS (l, ishell),ishell,1), rx,
     &             fL(naLSp(lp,ishell),ishell,1)) ) go to 300
        if (forbid( S(naLS (l, ishell),ishell,1), ry,
     &              S(naLSp(lp,ishell),ishell,1)) ) go to 300
        do 100 isubc = 1, nosubc
          if (isubc .ge. ishell) then
             if (forbid(scrL(l,isubc,1),rx,scrL(lp,isubc,2))) go to 300
             if (forbid(scrS(l,isubc,1),ry,scrS(lp,isubc,2))) go to 300
          else
             if ( scrL(l,isubc,1) .ne. scrL(lp,isubc,2)) go to 300
             if ( scrS(l,isubc,1) .ne. scrS(lp,isubc,2)) go to 300
          endif
  100   continue
 
c     Calculate matrix element
        Tc = sqrt ( (2*scrJ4+1)*(2*rz+1)*(2*scrJ4p+1) ) * s9j
     &      ( scrL(l ,nosubc,1), scrS(l ,nosubc,1), scrJ4 ,
     &        scrL(lp,nosubc,2), scrS(lp,nosubc,2), scrJ4p,
     &                 rx      ,       ry         ,  rz   )

        do 200 m = ishell+1, nosubc
          Tc = Tc *
     &     Uncpla
     &      ( scrL(l,m-1,1),fL(naLS(l,m),m,1), scrL(l ,m,1),
     &                     rx, scrL(lp,m-1,2), scrL(lp,m,2) )  
     &   * Uncpla
     &      ( scrS(l,m-1,1), S(naLS(l,m),m,1), scrS(l ,m,1),
     &                     ry, scrS(lp,m-1,2), scrS(lp,m,2) )
  
  200   continue
 
        naLSli = naLS( l, ishell)
        naLSlpi= naLSp(lp,ishell)
        if (ishell .gt. 1) then
          Tc = Tc *
     &     Uncplb
     &     ( scrL(l,ishell-1,1),fL(naLSli ,ishell,1), scrL(l ,ishell,1),
     &                      rx ,fL(naLSlpi,ishell,1), scrL(lp,ishell,2))
     &   * Uncplb
     &     ( scrS(l,ishell-1,1), S(naLSli ,ishell,1), scrS(l,ishell,1),
     &                      ry , S(naLSlpi,ishell,1), scrS(lp,ishell,2))
 
        endif
c
        Tc = Tc * v(naLSli-n00LS, naLSlpi-n00LS)
c       Change Uk to Uk0. I suppose Cowan's note 14 on p321 that
c       this factor should be sqrt(2S+1) is wrong. It should be
c       sqrt((2S+1)/(2s+1)).
          if (ry .eq. 0) tc = tc * sqrt ( S(naLSli,ishell,1)+.5)
        Tc = Tc * factorUVtoW
        C(l,lp) = Tc
C
  300 continue
  400 continue
C
      call BUTLER(matname,kpari,kpari,C, scrJ4,IPRITY(kpari), rz,1,
     &            scrJ4p,IPRITY(kpari), NLS,NLSp,KMX)
  500 continue
  600 continue
 
      call CLOK (TIME)
      print'(A,F8.2,3X,A,3X,A)', 
     & ' time=',TIME-TIME0, matname, STATE(kpari,kpari)
      end 

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      subroutine getbas( scrJ4, ik, nls, TMX, naLS, scrL, scrS,
     &   ncfg, idLS , idJJ, nosubc, kmx, ks, sJ4pos, ifile)

      dimension TMX(kmx,kmx) , naLS(kmx,ks), scrL(kmx,ks,2),
     &          scrS(kmx,ks,2), NCFG(kmx), idLS(kmx,2), idJJ(kmx,2)
      character idLS*8, idJJ*8

   20 do  30 ipos = sJ4pos , scrJ4, -1
   30   backspace ifile

      read (ifile) sJ4pos, nls
      if (sj4pos.ne.scrJ4) GO TO  20

      backspace ifile
      call zero2dim(tmx,nls,nls,kmx)
      read (ifile) sJ4pos, nls, 
     & xl,rows, (ri,cols, (rj,tmx(ri,rj), icols=1,cols), irows=1,rows),
     &  ((naLS(l,isub),scrL(l,isub,ik),scrS(l,isub,ik),isub=1,nosubc),
     &  NCFG(l),idLS(l,ik),idJJ(l,ik), l=1,nls)

      end


C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#

      SUBROUTINE ENERGY
C
C    &     READ PARAMETER VALUES, CALC ENERGY MATRICES, AND DIAGONALIZE
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      PARAMETER (KEXC=20)
      PARAMETER (KBGKS=1,KENRGS=21,KCASE=50,KTRAN=1,KLAM=3000)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM,ELEM,ELEM1,IDLS,IDJJ,CFLAG*1
     &  ,BLNK*1,STAR*1,LDES,LDESP, LINE*80
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON /CHAR3/ IDLS(KMX,2),IDJJ(KMX,2)
      COMMON /CHAR/  LDES(KMX),LDESP(KMX),ELEM(3,KC+1/KC,2),ELEM1(3)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/DUMLC4(KMX*KMX)
      COMMON/C3LC5/CI(KMX,KMX)
      COMMON/C3LC6/GAAXC(KMX,KEXC),BRNCHX(KEXC,KEXC)
      COMMON    VPAR(KPR,2),AVP(9,2),AVEIG(2),AVCG(2),
     &  NPARK(2),NPAV(KC),    EIG(KMX),FNT(9,2),I1,I2,
     &  FACT(4),LCSER(KMX),X(KMX)
      COMMON/C5/IGEN,IRNQ,IRND,IRMN,
     &  DELEKEV,DELEAV,UENINP,FACT0(5)
      COMMON/C6/NLEV,EMIN,EMAX,SUMWT,NLINES,FLBDN,FLBDX,FNUNP,FNUXP
      DIMENSION NPARJ(KC,KC),DELE(KMX),CFLAG(KC,2)
      DIMENSION DUMLC4A(2*KISER+1), tmxp(kmx,kmx)
      EQUIVALENCE (DUMLC4,DUMLC4A),(DUMLC4A(2*KISER+1),NPARJ),
     &            (DELE,CT4),      (dumlc4,tmxp)
      DIMENSION CPURTY(KMX,KC),NLCFGJ(KC),NSCFGJ(KC)
      DIMENSION AA(KMX,KMX),CII(KMX,KMX),JPAR(KPR)
      EQUIVALENCE (CT4,AA,CPURTY),(DUMLC4,CII,JPAR)
      EQUIVALENCE (DUMLC4,NLCFGJ),(DUMLC4(500),NSCFGJ)
      COMMON/C7/G1,SUMGAA,SUMGAR,SUMGAAT
      COMMON/C8/EXC(KEXC),JEXC(KEXC),MEXC(KEXC)
      COMMON/C9/L1,L2,IMAX,CGENGY(KC,2),SWT(KC,2)
      COMMON/PWBORN/NBIGKS,NENRGS,EKMINL,EKMAXL,DELEKL,IBK,BIGKS(KBGKS),
     &  XMIN,XMAX,IPWR,TEV(25),I11,CORR(KENRGS)

      DATA TEV/1.0,1.5,2.0,3.0,5.0,7.0,10.0,15.0,20.0,30.0,
     &  50.0,70.0,100.0,150.0,200.0,300.0,500.0,700.0,1000.0,
     &  1500.0,2000.0,3000.0,5000.0,7000.0,10000.0/
      DATA BLNK,STAR/' ','*'/
C
      WRITE(IW,*) '--- ENERGY CALLED ---'

      IKKK=KCPLD(8)
      if (NLSMAX.NE.0) IKKK=0
      IPRINT=KCPLD(9)
      do 99 I=1,3
   99 ELEM(I,1,1)=BLNK
C
C    &     READ QUANTUM NUMBERS
C
  100 IL=ILA
      IE=ILA
      NLSX=0
      K=1
      G1AA=0.0
      SUMGAA=0.0
      SUMGAAT=0.0
      REWIND IC
  101 REWIND IE
      READ (IC) NOTSJ1,NscrJ4,SJ4MN,SJ4MX,NPAR,NPARJ
      ICRD=1
      KK=K
C
C    &     READ PARAMETER VALUES
      if (IPRINT.GE.7) GO TO 102
      WRITE (IW,12)
   12 FORMAT (1H1)
  102 IXC=0
      IXC1=1
      NLST=0
      do 103 I=1,KEXC
      JEXC(I)=KEXC+1
  103 EXC(I)=0.0
C
      J1X=NSCONF(2,K)
      NPAR0=1
      do 131 I=1,J1X
      NPAV(I)=NPAR0
      NPAR1=NPAR0+1
      NPAR4=NPAR0+4
      NPARSN=NPARJ(I,I)+NPAR0-1
  105 CALL GETLINE(LINE)
      if (LINE(1:5).EQ.'    0') THEN
C       INTERPRET AS A RESCALE CARD
        READ(LINE,'(20X,F10.5,5F2.2,10X,F9.4)') DELEAV,FACT0,UENRGY
        if (UENRGY.EQ.0) UENRGY=UENINP
        GOTO 105
      endif

c --- "EMPTY" on the input line means that this state has all zero interactions.
c --- In particular it has zero conf. interactions. Configuration
c --- diagonal interactions can also be switched off by setting INTER0.
c --- This is meant as an easy way to avoid having to type the right number
c --- of zero parameters. The input is also made more transparent.
c --- The zero matrix is further treated as normal. This means that a
c --- zero matrix is printed in butler mode. So there is always a 
c --- hamiltonian present on file ibutler. BANDER needs this, because
c --- racah does not produce anything when there is no oper of symmetry 0+
c --- "present" on file ibutler.
      if ( INDEX(LINE,'EMPTY').ne.0 ) then
        call zero ( vpar(1,k), kpr )
        goto 139
      endif

      READ (LINE,10) (ELEM(N,I,K),N=1,3), VPAR(NPAR0,K),
     &  (VPAR(N,K),JPAR(N),N=NPAR1,NPAR4),TYPE,FACT
   10 FORMAT (2A6,A7,1X,F10.5,4(F9.4,I1),A2,4F2.2)
C
      if (VPAR(NPAR0,K).LT.-90000000.0) GO TO 900
      if (VPAR(NPAR0,K).LT.-50000000.0) GO TO 800
      if (VPAR(NPAR0,K).LT.-30000000.0) GO TO 100
      if (VPAR(NPAR0,K).GT.-9000.0) GO TO 110
      READ(ELEM(3,I,K),9) EMINA
    9 FORMAT (1X,F5.0)
  110 if (NPARJ(I,I).LE.5) GO TO 120
      NPAR5=NPAR4+1
      do 115 N1 = NPAR5,NPARSN,7
         CALL GETLINE(LINE)
         READ (LINE,11) (VPAR(N,K),JPAR(N), N=N1,MIN(N1+6,NPARSN) )
  115 CONTINUE
   11 FORMAT (7(F9.4,I1))
  120 do 121 J=1,4
      if (FACT (J).EQ..99 .OR. FACT(J).EQ.0) FACT(J) = 1
  121 if (FACT0(J).EQ..99) FACT0(J) = 1
C          SHIFT EAV AND/OR RESCALE SINGLE-CONFIGURATION PARAMETERS
      N1=NPAR1
      ERAS=VPAR(NPAR0,K)
      if (ERAS.LE.-4000.0) GO TO 123
      VPAR(NPAR0,K)=ERAS+DELEAV
      N1=NPAR0
  123 do 125 N=N1,NPARSN
  125 VPAR(N,K)=UENINP/UENRGY*VPAR(N,K)
      if (NPAR1.GT.NPARSN) GO TO 127
      do 126 N=NPAR1,NPARSN
      J=JPAR(N)
      if (J.LT.1.OR.J.GT.4) GO TO 126
      if (FACT0(J).EQ.0.0) GO TO 126
      VPAR(N,K)=VPAR(N,K)*FACT0(J)/FACT(J)
  126 CONTINUE
  127 do 128 J=1,4
      if (FACT0(J).GT.0.0) FACT(J)=FACT0(J)
  128 CONTINUE
C
      WRITE (IW,13) (ELEM(N,I,K),N=1,3),UENRGY,TYPE,FACT,
     &  I, (LLIJK(L,I,K),NIJK(L,I,K), L=1,NOSUBC)
   13 FORMAT (//1X,3A6,4X,20HPARAMETER VALUES IN ,F7.1,7H CM-1 (,
     &  A2,6H TIMES,4F5.2,1H), 3X,I3,8(2X,A1,I2))
      WRITE (IW,14) (PARNAM(N,K),N=NPAR0,NPARSN)
   14 FORMAT (/39X, A8, 4(7X,A8)/(2X,7(7X,A8)))
      WRITE(IW,15) (VPAR(N,K),N=NPAR0,NPARSN)
   15 FORMAT (/ 30X,5F15.3/(7F15.3))
      PRINT'((1X,A8,A,F11.3))',
     &  (PARNAM(N,K),'=',VPAR(N,K), N=NPAR0,NPARSN)
      NPAR0=NPARSN+1
      CFLAG(I,K)=BLNK
      CGENGY(I,K)=0.0
      SWT(I,K)=0.0
  131 CONTINUE
      NOT=NPAR0
      NJJ=NPARJ(J1X,J1X)
      if (J1X.LE.1) GO TO 139
      J1XM1=J1X-1
      do 138 J1A=1,J1XM1
      J1AP1=J1A+1
      do 138 J1B=J1AP1,J1X
      JAB=MAX(J1B-J1A,1)
      if (NPARJ(J1A,J1B).EQ.0) GO TO 138
      NPAR4=NPAR0+4
      CALL GETLINE (LINE)
      READ(LINE,16) (ELEM1(N),N=1,3),(VPAR(N,K),JPAR(N),N=NPAR0,NPAR4),
     &  TYPE,FACT
   16 FORMAT (2A6,A7,1X,5(F9.4,I1),A2,4F2.2)
      if (FACT(4).EQ..99.OR.FACT(4).EQ.0) FACT(4)=1
      FACT5=FACT(4)
      NPARSN=NPARJ(J1A,J1B)+NPAR0-1
      if (NPARJ(J1A,J1B).LE.5) GO TO 132
      NPAR5=NPAR4+1
      CALL GETLINE (LINE)
      READ (LINE,11) (VPAR(N,K),JPAR(N),N=NPAR5,NPARSN)
C          RESCALE CONFIGURATION-INTERACTION PARAMETERS
  132 SCALE1=1
      J=JPAR(NPAR0)
      if (J.LT.1.OR.J.GT.5) GO TO 133
      if (FACT0(J).EQ.0.0) GO TO 133
      SCALE1=FACT0(J)/FACT5
      FACT5=FACT0(J)
  133 do 134 N=NPAR0,NPARSN
  134 VPAR(N,K)=UENRGY/UENINP*VPAR(N,K)*SCALE1
C
      WRITE (IW,17) (ELEM1(N),N=1,3),UENRGY,TYPE,FACT5,
     &  (JN, (LLIJK(L,JN,K),NIJK(L,JN,K), L=1,KS), JN=J1A,J1B,JAB)
   17 FORMAT (//1X,2A6,A7,3X,20HPARAMETER VALUES IN ,F7.1,7H CM-1 (,
     &  A2,6H TIMES, F5.2,1H),18X,I3,8(2X,A1,I2)/89X,I3,8(2X,A1,I2))
      WRITE (IW,14) (PARNAM(N,K),N=NPAR0,NPARSN)
      WRITE (IW,15) (VPAR(N,K),N=NPAR0,NPARSN)
      PRINT'((1X,A8,A,F11.3))',
     &  (PARNAM(N,K),'=',VPAR(N,K), N=NPAR0,NPARSN)
      NPAR0=NPARSN+1
  138 CONTINUE
  139 do 140 I=1,9
      AVP(I,K)=0.0
  140 FNT(I,K)=0.0
      AVCG(K)=0.0
      AVEIG(K)=0.0
      SUMWT=0.0
      EMIN=1.0E20
      EMAX=-1.0E20
      NLEV=0
      NPAR=NPARSN
      NPARK(K)=NPAR
      do 150 L=1,KMX
  150 LCSER(L)=0
C
C    &     CALCULATE ENERGY MATRICES
C
      do 399 NN=1,NscrJ4

c     ((TMX(L,LP),L=1,NLS), LP=1,NLS),
      READ (IC) scrJ4,NLS
      backspace ic
      call zero2dim(tmx,nls,nls,kmx)
      READ (IC) scrJ4,NLS, 
     & lx,rows, (ri,cols, (rj,tmx(ri,rj), icols=1,cols), irows=1,rows),
     &  ( (NALS(L,I),scrL(L,I),scrS(L,I),
     &  I=1,NOSUBC), NCFG(L),IDLS(L,1),IDJJ(L,1), L=1,NLS)
c        print*,'sparse tmx in energy',
c     &  nls, nls, kmx,((tmx(i,j),j=1,nls),i=1,nls)


      call copy2dim (tmx,tmxp,nls,nls,kmx)
      ICRD=ICRD+1
      NLSX=MAX(NLSX,NLS)
      NOICRD=0
      WT=2.0*scrJ4+1.0
      ERAS=0.0
      if (NLS.LE.0) NLS=1
      call zero2dim (c , nls,nls,kmx)
      do 212 L=1,NLS
        J=NCFG(L)
        I=NPAV(J)
        ERAS=ERAS+VPAR(I,K)
  212   C(L,L)=VPAR(I,K)
      AVCG(K)=AVCG(K)+ERAS*WT
      if (NPAR.LE.1) GO TO 225
      do 222 IPAR=2,NPAR
      do 218 J=1,J1X
      if (IPAR.EQ.NPAV(J)) GO TO 222
  218 CONTINUE
      call getsparse ((ic), ct4, nls, nls, kmx,.true.)
c      READ (IC) ((CT4(L,LP),L=LP,NLS), LP=1,NLS)
      ICRD=ICRD+1
      NOICRD=NOICRD+1
      do 220 L=1,NLS
      do 220 LP=1,L
  220 C(L,LP)=C(L,LP)+VPAR(IPAR,K)*CT4(L,LP)
  222 CONTINUE
  225 do 230 L=1,NLS
      do 230 LP=1,L
  230 C(LP,L)=C(L,LP)
      KPAR=3
C---    OUTPUT OF ENERGY MATRICES FOR BUTLER CRYSTAL FIELD CALCULATIONS
        CALL BUTLER('HAMILTONIAN',KK,KK,C,scrJ4,IPRITY(KK),0.,1,
     &             scrJ4,IPRITY(KK),NLS,NLS,KMX)
      CALL SPRIN
      if (IBUTLER().NE.0) GOTO 399
      LX=0
      do 250 L=1,NLS
      J=NCFG(L)
      I=NPAV(J)
      if (VPAR(I,K).GT.-4000.0) LX=L
  250 CONTINUE
      LXP1=LX+1
      JX=NLS-LX+1
      JXP1=JX+1
      NLSP1=NLS+1
      L=NOSUBC
      J=1
      KKK=1
      if (LX.EQ.0.OR.LX.EQ.NLS) GO TO 271
      I=0
      do 260 LP=1,NLS
      if (LP.LE.LX) GO TO 260
      I=I+1
      do 255 L=1,LX
      CI(L,I)=C(L,LP)
      C(L,LP)=0.0
  255 C(LP,L)=0.0
  260 CONTINUE
C    &     CALC STATISTICAL WEIGHT OF FIRST CONFIGURATION OR
C    &          (IF CONTINUUM CONFIGURATIONS PRESENT) OF ION CORE
      do 265 L=LXP1,NLS
      J=NCFG(L)
      I=NPAV(J)
      if (VPAR(I,K).LT.-9000.0) GO TO 266
  265 CONTINUE
  266 KKK=K
      M=0
      do 270 I=1,NOSUBC
      L=NOSUBC-I+1
      if (NIJK(L,J,KKK).GT.0) M=M+1
      if (M.EQ.2) GO TO 271
  270 CONTINUE
  271 G1=1.0
      do 280 I=1,L
      E1=4.0*FLLIJK(I,J,KKK)+2.0
      E2=NIJK(I,J,KKK)
  280 G1=G1*FCTRL(E1)/(FCTRL(E2)*FCTRL(E1-E2))
      if (LX.GT.0.AND.LX.LT.NLS) G1AA=G1
      if (G1AA.GT.0.0) G1=G1AA

C
C    &     CALC EIGENVALUES AND VECTORS
C
C    &     C CONTAINS MATRIX TO BE DIAGONALIZED.
C    &     MLEW RETURNS EIGENVALUES AND VECTORS IN EIG AND C, RESPECTIVE
      CALL MLEW(KMX,NLS,C,EIG,FK,C,M)
      ERAS=0.0
      do 320 LP=1,NLS
  320 ERAS=ERAS+EIG(LP)
      AVEIG(K)=AVEIG(K)+ERAS*WT
      ERAS=NLS
      SUMWT=SUMWT+ERAS*WT
C
C
      EMIN=MIN(EMIN,EIG(1))
      EMAX=MAX(EMAX,EIG(NLS))
      NLEV=NLEV+NLS
C
C    & CALCULATE CONFIGURATION SERIAL NUMBER FOR LEVEL LP
C
      do 330 J=1,J1X
      NLCFGJ(J)=0
      NSCFGJ(J)=0
      do 330 LP=1,NLS
  330 CPURTY(LP,J)=0.0
      do 331 L=1,NLS
      LCSER(L)=0
      J=NCFG(L)
      NSCFGJ(J)=NSCFGJ(J)+1
      do 331 LP=1,NLS
  331 CPURTY(LP,J)=CPURTY(LP,J)+C(L,LP)**2
C
      do 333 LP=1,NLS
      ERAS=0.0
      do 332 J=1,J1X
      if (CPURTY(LP,J).LT.ERAS) GO TO 332
      ERAS=CPURTY(LP,J)
      LCSER(LP)=J
      M=J
  332 CONTINUE
  333 NLCFGJ(M)=NLCFGJ(M)+1
C
      do 334 J=1,J1X
  334 if (NLCFGJ(J).NE.NSCFGJ(J)) CFLAG(J,K)=STAR
      ICOUNT=0
  335 IERAS=0
      do 337 J=1,J1X
      if (NLCFGJ(J).GE.NSCFGJ(J)) GO TO 337
      IERAS=7
      ERAS=0.0
      do 336 LP=1,NLS
      J1=LCSER(LP)
      if (J1.EQ.J.OR.NLCFGJ(J1).EQ.NSCFGJ(J1)) GO TO 336
      if (CPURTY(LP,J).LT.ERAS) GO TO 336
      ERAS=CPURTY(LP,J)
      M=LP
  336 CONTINUE
      J1=LCSER(M)
      LCSER(M)=J
      NLCFGJ(J1)=NLCFGJ(J1)-1
      NLCFGJ(J)=NLCFGJ(J)+1
  337 CONTINUE
      ICOUNT=ICOUNT+1
      if (IERAS.NE.0.AND.ICOUNT.LE.NLS) GO TO 335
      ERAS=2.0*scrJ4+1.0
      do 338 LP=1,NLS
      J=LCSER(LP)
      CGENGY(J,K)=CGENGY(J,K)+ERAS*EIG(LP)
  338 SWT(J,K)=SWT(J,K)+ERAS
C    &     CALCULATE LEVEL DESIGNATIONS
      do 340 LP=1,NLS
      C2X=0.0
      do 339 L=1,NLS
      if (NCFG(L).NE.LCSER(LP)) GO TO 339
      C2=C(L,LP)**2
      if (C2.LT.C2X) GO TO 339
      LD=L
      C2X=C2
  339 CONTINUE
      LDES(LP)=IDLS(LD,1)
      if (KCPL.EQ.2) LDES(LP)=IDJJ(LD,1)
  340 CONTINUE
C
C    &     PRINT EIGENVALUES AND VECTORS
C
      if (IPRINT.GE.9) GO TO 350
      if (IPRINT.GE.7) GO TO 342
      if (IENGYD.LT.2) GO TO 342
      if (NLS.LE.11) GO TO 342
      if (EIG(NLS).GT.9999.0) GO TO 341
      WRITE (IW,30) scrJ4,(EIG(L),L=1,NLS)
   30 FORMAT (15H1  EIGENVALUES ,5X,3H(J=,F4.1,1H)/(/15X,11F9.3))
      GO TO 345
  341 WRITE (IW,31) scrJ4,(EIG(L),L=1,NLS)
   31 FORMAT (15H1  EIGENVALUES ,5X,3H(J=,F4.1,1H)/(/15X,11F9.2))
      GO TO 345
  342 if (EIG(NLS).GT.9999.0) GO TO 343
      WRITE (IW,32) scrJ4,(EIG(L),L=1,NLS)
   32 FORMAT (15H0  EIGENVALUES ,5X,3H(J=,F4.1,1H)/(/15X,11F9.3))
      GO TO 345
  343 WRITE (IW,33) scrJ4,(EIG(L),L=1,NLS)
   33 FORMAT (15H0  EIGENVALUES ,5X,3H(J=,F4.1,1H)/(/15X,11F9.2))
C  34& WRITE (IW,34) (LCSER(L), L=1,NLS)
  345 WRITE (IW,34)
   34 FORMAT (15H   CONFIG. NO. /(12X,11I9))
  350 KPAR=4
      if (LX.GT.0.AND.LX.LT.NLS) GO TO 355
      if (ISPECC.GT.7) GO TO 355
      if ((NSCONF(1,2)+IMAG).EQ.0) GO TO 358
  355 WRITE (IE) scrJ4,NLS, (EIG(LP), (C(L,LP),L=1,NLS), LP=1,NLS),
     &  LCSER,LDES
  358 if (IPRINT.GE.8) GO TO 359
      CALL SPRIN
  359 if (IELPUN.NE.0) WRITE (11,35) (ELEM(L,1,K), L=1,3),
     &  scrJ4,NLS, (EIG(L), L=1,NLS)
   35 FORMAT (3A6,2X,F5.1,I5/(6F12.3))
C
C    &     CALCULATE AUTOIONIZATION TRANSITION PROBABILITIES
C
      do 360 LP=1,NLS
      AA(LP,JX)=0.0
      AA(LP,JXP1)=0.0
      do 360 I=1,KEXC
  360 GAAXC(LP,I)=0.0
      if (LX.EQ.0.OR.LX.EQ.NLS) GO TO 380
      if (NLS.GT.KMX) WRITE (IW,36) NLS,KMX
   36 FORMAT (//10H *****NLS=,I3,29H .GT. MAX ALLOWABLE VALUE OF ,
     &  I3,18H FOR AA CALCN*****//)
      BACKSPACE IE
      READ (IE) scrJ4,NLS, (EIG(LP), (C(L,LP),L=1,NLS), LP=1,NLS)
      AAMX=0.0
      do 362 LP=1,NLS
      do 362 J=LXP1,NLS
      I=J-LX
      SUM=0.0
      do 361 L=1,LX
  361 SUM=SUM+C(L,LP)*CI(L,I)
  362 CII(LP,J)=SUM
      do 365 LP=1,NLS
      do 364 I=1,NLS
      if (EIG(I).GT.-4000.0) GO TO 364
      AA(LP,I)=0.0
      if (EIG(I).LT.-8000.0.OR.EMINA.EQ.0.0) GO TO 2362
      if (EIG(LP).LT.EMINA) GO TO 364
      J=LCSER(I)
      JJ=NPAV(J)
      ETEMP=EIG(I)-VPAR(JJ,K)
      if (ETEMP.GT.0.0) GO TO 364
 2362 SUM=0.0
      do 363 L=LXP1,NLS
  363 SUM=SUM+CII(LP,L)*C(L,I)
      AA(LP,I)=((SUM*UENRGY/109735.3)**2)*6.28318*2066.0
      if (EIG(I).LT.-8000.0) AA(LP,JX)=AA(LP,JX)+AA(LP,I)
      AA(LP,JXP1)=AA(LP,JXP1)+AA(LP,I)
  364 CONTINUE
  365 AAMX=MAX(AA(LP,JXP1),AAMX)
      ERAS=1.0
      L=13
      if (AAMX.LT.0.1) L=10
      if (AAMX.GT.999.9) L=16
      L1=L
      if (L.EQ.13) GO TO 367
      ERAS=10.0**(13-L)
      do 366 LP=1,NLS
      do 366 I=1,JXP1
  366 AA(LP,I)=ERAS*AA(LP,I)
  367 if (IPRINT.GE.9) GO TO 371
      WRITE (IW,37) L
   37 FORMAT (17H AA(UNITS OF 10**,I2,5H/SEC))
      I=0
      do 370 J=LX,NLSP1
      I=I+1
      if (NLS.EQ.LXP1.AND.I.GE.2) GO TO 370
      if (KCPLD(1)+KCPLD(2).LT.3) GO TO 368
      if (I.LT.JX.AND.(NLS-LX).GT.2.AND.NLS.GT.11) GO TO 370
  368 if (J.GE.NLS) GO TO 369
      N1=LCSER(I)
      WRITE (IW,38) (ELEM(I1,N1,K),I1=2,3), LDES(I), (AA(LP,I),LP=1,NLS)
   38 FORMAT (1X,A6,A2,1H(,A7,F7.4,10F9.4/(16X,11F9.4))
      GO TO 370
  369 ITEMP=-8000
      if (J.EQ.NLSP1) ITEMP=-4000
      WRITE (IW,39) ITEMP, (AA(LP,I),LP=1,NLS)
   39 FORMAT (' TOT, E.LT.',I5,F8.4,10F9.4/(16X,11F9.4))
  370 CONTINUE
  371 ERAS=1.0E13/ERAS
      GG=2.0*scrJ4+1.0
      do 372 LP=1,NLS
      AA(LP,JX)=ERAS*AA(LP,JX)
      AA(LP,JXP1)=ERAS*AA(LP,JXP1)
      SUMGAA=SUMGAA+GG*AA(LP,JX)
  372 SUMGAAT=SUMGAAT+GG*AA(LP,JXP1)
      ERAS=GG*ERAS
      JXM1=JX-1
      do 378 L=1,JXM1
      if (EIG(L).GT.-4000.0) GO TO 378
      J=LCSER(L)
      JJ=NPAV(J)
      ETEMP=EIG(L)-VPAR(JJ,K)
      if (IXC.EQ.0) GO TO 375
      do 374 I=1,IXC
      if (ABS(ETEMP-EXC(I)).LT.0.01.AND.J.EQ.JEXC(I)) GO TO 376
  374 CONTINUE
  375 IXC=IXC+1
      I=IXC
      EXC(I)=ETEMP
      JEXC(I)=J
  376 do 377 LP=1,NLS
  377 GAAXC(LP,I)=GAAXC(LP,I)+ERAS*AA(LP,L)
  378 CONTINUE
      IXC1=MAX(IXC,1)
      if (IPRINT.GE.9) GO TO 380
      WRITE (IW,40) SUMGAA,SUMGAAT
   40 FORMAT (/20X,7HSUMGAA=,1PE13.5,10X,8HSUMGAAT=,E13.5//
     &  10X,11HEXC   GAAXC/)
  380 WRITE (IE) LX, (AA(LP,JX),AA(LP,JXP1), LP=1,NLS)
     &  ,NLST,IXC,IXC1, (EXC(I), (GAAXC(LP,I),LP=JX,NLS), I=1,IXC1)
      NLST=NLST+LX
      if (IPRINT.GE.9) GO TO 399
      if (NLS.LE.1) GO TO 397
      if (IXC.EQ.0) GO TO 388
      if (LX.EQ.0.OR.LX.EQ.NLS) GO TO 388
      ERAS=10.0**(-L1)
      do 386 I=1,IXC
      do 384 LP=1,NLS
  384 GAAXC(LP,I)=ERAS*GAAXC(LP,I)
  386 WRITE (IW,41) JEXC(I),EXC(I), (GAAXC(LP,I), LP=1,NLS)
   41 FORMAT (I3,F12.4,11F9.4/(16X,11F9.4))
  388 CONTINUE
C
C    &     CALCULATE LEVEL DISTRIBUTION
C
      FNLS=NLS
      SUM=0.0
      do 391 L=1,NLS
  391 SUM=SUM+EIG(L)
      AVEEIG=SUM/(FNLS+1.0E-10)
      SUM=0.0
      SUM3=0.0
      do 392 L=1,NLS
      DELTA=EIG(L)-AVEEIG
      DELTA2=DELTA**2
      SUM=SUM+DELTA2
  392 SUM3=SUM3+DELTA2*DELTA
      RMS1=SQRT(SUM/FNLS)
      SUM3=SUM3/FNLS
      ALF3=SUM3/(RMS1**3)
      NLSM1=NLS-1
      FNLSM1=NLSM1
      do 393 L=1,NLSM1
  393 DELE(L)=EIG(L+1)-EIG(L)
      SUM=0.0
      do 394 L=1,NLSM1
  394 SUM=SUM+DELE(L)
      AVEDEL=SUM/FNLSM1
      SUM=0.0
      do 395 L=1,NLSM1
  395 SUM=SUM+(DELE(L)-AVEDEL)**2
      RMS2=SQRT(SUM/FNLSM1)
      WRITE (IW,42) AVEEIG,RMS1,ALF3,AVEDEL,RMS2
   42 FORMAT (//17H0AVEEIG,RMS,ALF3=,2F12.3,F9.4,5X,11HAVEDEL,RMS=,
     &  2F12.3)
C
  397 if (IV.NE.0) CALL CALCV
C
  399 CONTINUE
C    &      WRITE DUMMY RECORD, REQUIRED BY SYSTEM BUG
      WRITE (IE) (EIG(L), L=1,20)
C
      do 400 J=1,J1X
  400 CGENGY(J,K)=CGENGY(J,K)*UENRGY/((SWT(J,K)+0.00000001)*109737.31)
      IF(SUMWT.NE.0) THEN
        AVCG(K)=AVCG(K)/SUMWT
        AVEIG(K)=AVEIG(K)/SUMWT
      endif
      do 410 I=1,9
      if (FNT(I,K).EQ.0.0) GO TO 410
      AVP(I,K)=AVP(I,K)/FNT(I,K)
  410 CONTINUE
      if (AVP(1,K).GT.0) GO TO 415
      do 412 I=1,9
  412 AVP(I,K)=1.0

  415 IF(IPRINT.LT.7.AND.(K.EQ.2.OR.(K.EQ.1.AND.NSCONF(1,2).EQ.0))) THEN
      WRITE  (9,43) (COUPL(I), I=1,7)
   43 FORMAT (///13H AVE PURITIES,29X,9(A6,2X)/)
      do 420 K1=1,K
      J1X=NSCONF(2,K1)
  420 WRITE (IW,44) ((ELEM(I,1+JN*(J1X-1),K1), I=1,3), JN=0,1),
     &  (AVP(I,K1), I=1,7), AVCG(K1),AVEIG(K1)
   44 FORMAT (1X,3A6,3H---,3A6,7F8.3,5X,6H  EAV=,F10.4/101X,6HAVEIG=,
     &  F10.4)
      endif
C
C 430 if (ISPECC.GE.8) CALL LVDIST
      CONTINUE
C
      REWIND IE
      PRINT'(A,10I5)',' END OF ENERGY: K,IMAG,IQUAD ',K,IMAG,IQUAD

      if (IBUTLER().EQ.0) THEN
      if (IMAG .EQ.K.OR.IMAG .GT.2) CALL SPECTR(-1,IE,IE,1,IGEN)
      do 460 IS = 1,KS
  460    if (IQUAD(IS).EQ.K .OR. IQUAD(IS).GT.2)
     &   CALL SPECTR(2,IE,IE,IRNQ,IGEN)
      endif

C     if (IGEN.GE.5) CALL BORN
      if (NSCONF(1,2).LE.0) GO TO 100
      if (IE.EQ.ILB) GO TO 500
  480 IL=ILB
      IE=ILB
      K=2
      GO TO 101
C
  500 CALL SPECTR(IIPRTY,ILA,ILB,IRNQ,IGEN)
C     if (IGEN.GE.5) CALL BORN
      if (IKKK.LE.8) GO TO 100
      do 520 I=1,ICRD
  520 BACKSPACE IC
      GO TO 480
C
C 800 CALL RCEINP
  800 CONTINUE
      GO TO 100
C
  900 RETURN
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      SUBROUTINE SPECTR(II,IE1,IE2,IRMN,IGEN)
C
C
C    &   CALC SPECTRUM FOR MAGNETIC DIPOLE(II=0), ELECTRIC DIPOLE(II=1),
C    &        OR ELECTRIC QUADRUPOLE(II=2) TRANSITIONS
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      PARAMETER (KLAM=3000,KEXC=20)
      PARAMETER (KBGKS=1,KENRGS=21,KCASE=50,KTRAN=1)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LDES,LDESP,LSYMB*1,COUPL*6,PARNAM,ELEMJ,ELEM,ELEM1,
     &            LLDES,LLDESP, LINE*80, SIGNUM*1
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      COMMON /CHAR/ LDES(KMX),LDESP(KMX),ELEM(3,KC+1/KC,2),ELEM1(3)
      COMMON/C2/NCFG(KMX),NCFGP(KMX),PMUP(12,KC),EIG(KMX),EIGP(KMX),
     &  LCSER(KMX),LCSERP(KMX)
      COMMON/C9/L1,L2,IMAX,CGENGY(KC,2),SWT(KC,2)
      COMMON/C3/T(KLAM),TP(KLAM),S2(KLAM),SOABSS(KLAM),
     &  AA(KMX),AAP(KMX),GAA(KLAM),GAAP(KLAM),BRNCH(KLAM),
     &  ICUT(66),SGFCUT(66),SFCUT(66),DIDGF(66),DSGFDGF(66),
     &  FICUT(66),FNUCUT(66),FNUPLT(82),SGF(66),FIPLT(82),SGFPLT(82),
     &  NCSER(KLAM),AAT(KMX),AAPT(KMX)
      COMMON/C6/NLEV,EMIN,EMAX,SUMWT,NLINES,FLBDN,FLBDX,FNUNP,FNUXP
      COMMON       D(KMX,KMX)
      COMMON/C3LC2/V(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/CTA(KMX,KMX)
      COMMON/C3LC5/GOSS(KTRAN,KBGKS)
      COMMON/C3LC6/GAAXC(KMX,KEXC),BRNCHX(KEXC,KEXC)
      COMMON    VPAR(KPR,2),AVP(9,2),AVEIG(2),AVCG(2),
     &  NPARK(2),NPAV(KC),FJT(KLAM),FJTP(KLAM),NCSERP(KLAM),
     &  GAAT(KLAM)
      DIMENSION ORD(KLAM)
      EQUIVALENCE (D,GAAPT)
      COMMON/CTKEV/NPTKEV,NTKEV,TKEV(6),TUNRGY(6),BRNCHL(6),BRNCHT(6),
     &  EIONRY
      DIMENSION GFCUT(66)
      DIMENSION FLAM(KLAM),SUMFJ(KC),GAAPT(KLAM)
      EQUIVALENCE (CTA,ORD),(CT4,FLAM)
      COMMON/C7/G1,SUMGAA,SUMGAR,SUMGAAT
      COMMON/C8/EXC(KEXC),JEXC(KEXC),MEXC(KEXC)
      COMMON/PWBORN/NBIGKS,NENRGS,EKMINL,EKMAXL,DELEKL,IBK,BIGKS(KBGKS),
     &  XMIN,XMAX,IPWR,TEV(25),I1,CORR(KENRGS)
      DIMENSION ISER(KLAM),EXC1(KEXC),JEXC1(KEXC),ELEMJ(KEXC)
      DIMENSION LLDES(KLAM),LLDESP(KLAM)  , DUM(1)
      COMMON /LEVEL  / NLEVELS, ELEVELS
      CHARACTER TEXT*50
      CHARACTER NAM*18,NAMP*18,NAMINT*14,NAMOLD*14,CERAS*1,SPID(-1:3)*8
     &         ,PID*2
      DATA (SPID(I),I=-1,1) /' MAG DIP','ELEC MON','ELEC DIP'/
      DATA (SPID(I),I= 2,3) /'ELEC QUD','ELEC OCT'/
C---   FUNCTION FNU  ----------------
      FNU(I)=ABS(T(I)-TP(I))
C
C
C     KMX=3*KLSI
C     KEXC=20
C     KLAM=3000
C     KTRAN=1
      WRITE(IW,*) 'SPECTR CALLED(',II,IE1,IE2,IRMN,IGEN,')'
      if (II.EQ.-1.OR.IGEN.LT.5) GO TO 92
      CALL GETLINE(LINE)
      READ (LINE,8) NBIGKS,EKMINL,EKMAXL,DELEKL,NENRGS,XMIN,XMAX
    8 FORMAT (I5,3F15.10,I10,2F10.2)
      WRITE (IW,9) NBIGKS,EKMINL,EKMAXL,DELEKL,NENRGS,XMIN,XMAX
    9 FORMAT (//I5,3F15.10,I10,2F10.2)
      do 90 I=1,KTRAN
      do 90 J=1,NBIGKS
   90 GOSS(I,J)=0.0
      TUNRGY(1)=9.9E10
   92 if (NTKEV.NE.0) THEN
       do 94 I=2,6
   94  TUNRGY(I)=TKEV(I)*8065479/UENRGY
      endif
      UENREV=UENRGY/8065.479
      NAMOLD=' '
      IPWR=1
      IBK=0
C
C    &     READ REDUCED MUPOLE MATRIX ELEMENTS
C
      IREAD=0
      FLBDN=0.001
      FLBDX=500000.0
      if (II.EQ.-1) GO TO 104
C--- test next line to see if a spectr calculation is requested
  100 CALL GETLINE (LINE)
      call BACKSpInp()
      READ (LINE,'(A50,A1,6X,I1)') TEXT,CERAS,N
      if ((IGEN.LT.5.AND.N.NE.II) .OR. CERAS.NE.'(') THEN
        WRITE(IW,*) 'IGEN=',IGEN,' N=',N,' II=',II,' CERAS=',CERAS,
     &' THIS WAS NOT A SPECTR CARD: END OF SPECTR.'
        RETURN
      endif

C---- SKIP SPECTR LINES ON INPUT WHEN BUTLER IS ACTIVE
      if (IBUTLER() .NE. 0) THEN
        CALL GETLINE (LINE)
        GOTO 100
      endif

      IPWR=N
      IPTG=0
      if (IBK.EQ.0.OR.(IPWR.GT.IRMN.AND.IBK.EQ.1)) IPTG=1
      if (II.NE.1) IPTG=0
      if (IPTG.EQ.1) WRITE (IW,13) SPID(II)
   13 FORMAT (1H1,59X,A8,9H SPECTRUM////)
      IBACK=IREAD
      if (IGEN.GE.5.AND.N.NE.IRMN) IBACK=0
      if (IBACK.EQ.0) GO TO 104
      do 102 I=1,IREAD
  102 BACKSPACE IC
      IREAD=0
  104 L1=IE1-ILA+1
      L2=IE2-ILA+1
      JNX1=NSCONF(2,L1)
      JNX2=NSCONF(2,L2)
      JNX=MAX(JNX1,JNX2)
      do 110 I=1,JNX1
      do 110 J=1,JNX2
      PMUP(I,J)=1.0
      if (II.EQ.-1) GO TO 110
      CALL GETLINE (LINE)
      if ((I+J).NE.2) GO TO 105
      READ (LINE,14) NAM,NAMP, PMUP(I,J),NAMINT, FRAC, PID,FLBDN,FLBDX
   14 FORMAT (A18,2X,A18,2X, F10.5, A14, F6.4, A2,2E4.1)
      if (FLBDN.GE.FLBDX) FLBDN=0.001
      if (FLBDN.LE.0.0) FLBDN=0.001
      if (FLBDX.LE.0.1) FLBDX=500000.0
      GO TO 106
  105 READ (LINE,14) NAM,NAMP, PMUP(I,J), NAMINT, FRAC, PID
  106 if (IPTG.EQ.1) WRITE(IW,15) I,NAM,J,NAMP,NAMINT,PMUP(I,J),FRAC,
     &                            PID
   15 FORMAT (I5,5X,A18,I10,5X,A18,10X,A14,'=',F15.5,6X,5HFRAC=,F7.4,
     &  5X,A2)
      CSIGMA=0.0
      if (NAMINT.EQ.NAMOLD) CSIGMA=1.0
      NAMOLD=NAMINT
  110 CONTINUE
C    &          CALC COEF FOR PHOTOIONIZATION CROSS-SECTION (SIGMA)
      if (NSCONF(3,2).GE.0.OR.L1.EQ.L2) CSIGMA=0.0
      if (CSIGMA.EQ.0.0) GO TO 120
      J=NOT-NJJ
      I=J-NJJ
      if (FRAC.LE.0.0) FRAC=(VPAR(J,2)-VPAR(I,2))*UENRGY/109735.0
      CSIGMA=8.06662E-18/FRAC
      WRITE (IW,16) FRAC,CSIGMA,VPAR(I,2),VPAR(J,2)
   16 FORMAT (/10X,5HFRAC=,F8.6,10X,7HCSIGMA=,E12.5,20X,2F12.4)
C
  120 GFCUT(1)=0.0
      ERAS=-14.0
      DEL=0.25
      do 130 J=2,65
      ERAS=ERAS+DEL
  130 GFCUT(J)=EXP(2.302585093*ERAS)
      GFCUT(66)=1.0E10
      FNUX=1.0E8/(FLBDN*UENRGY)
      FNUN=1.0E8/(FLBDX*UENRGY)
C
C
C
      REWIND IE1
      REWIND IE2
      REWIND 20
      NLINES=0
      FNUNP=1.0E20
      FNUXP=0.0
      SJ31=100.0
      SJ32=100.0
      PMAX=0
      do 160 I=1,JNX1
      do 160 J=1,JNX2
      if (ABS(PMUP(I,J)).GT.ABS(PMAX)) PMAX=PMUP(I,J)
  160 CONTINUE
      A=1.0/PMAX
      PSQ=3.0375E-6*(PMAX**2)
      if (II.EQ.-1) PSQ=PSQ*(0.5/137.04)**2
      if (IGEN.GE.5) GO TO 162
      if (II.EQ.2) PSQ=PSQ*((3.14159*0.529)**2)/5.0
      GO TO 163
  162 PSQ=PSQ*3.0
  163 SUMS2=0.0
      SUMS2M=0.0
      SUMGF=0.0
      SUMF=0.0
      SUMGAR=0.0
      JCUTX=0
      do 170 J=1,66
      ICUT(J)=0
      SGFCUT(J)=0.0
  170 SFCUT(J)=0.0
      I=0
      I0=0
      I0N=0
      I0X=0
      IJJP=0
      ILOSTT=0
      ICDLT=0
      IBRNCH=0
      D0MAX=0.0
      do 175 L=1,KMX
      do 175 J=1,KEXC
  175 GAAXC(L,J)=0.0
  180 if (IPTG.EQ.1) WRITE (IW,18)
   18 FORMAT (////18X,12HNO. OF LINES/
     &  5X,1HJ,5X,2HJP,5X,4HJ-JP,3X,5HTOTAL,3X,4HKLAM,4X,5HILOST/)
C
C    &     READ DIPOLE MATRIX FOR BASIS VECTORS
C
c     ((D(L,LP),L=1,NLS), LP=1,NLSP),
  200 READ (IC) scrJ4,scrJ4P,NLS,NLSP 
      backspace ic
      call zero2dim(D,nls,nlsp,kmx)
      READ (IC) scrJ4,scrJ4P,NLS,NLSP, 
     & xl,rows, (ri,cols, (rj,D(ri,rj), icols=1,cols), irows=1,rows),
     &  NCFG,NCFGP
      IREAD=IREAD+1
      if (scrJ4.LT.0.0) GO TO 560
      do 220 L=1,NLS
      do 220 LP=1,NLSP
      IN=NCFG(L)
      JN=NCFGP(LP)
  220 D(L,LP)=D(L,LP)*A*PMUP(IN,JN)
      ITRUNC=I
      I00=I0
      I0N0=I0N
      I0X0=I0X
      IJJP0=IJJP
      SUMS20=SUMS2
      SUMSM0=SUMS2M
      ILOST=0
C
C    &     READ EIGENVALUES AND VECTORS
C    &     AND CALC MUPOLE MATRIX FOR ACTUAL EIGENVECTORS
C
      if (scrJ4-SJ31) 301,302,305
  301 BACKSPACE IE1
      BACKSPACE IE1
  302 BACKSPACE IE1
      BACKSPACE IE1
  305 READ (IE1) SJ31,NLS, (EIG(LP), (V(L,LP),L=1,NLS), LP=1,NLS),
     &  LCSER,LDES
      READ (IE1) LX, (AA(LP),AAT(LP), LP=1,NLS)
      if (scrJ4-SJ31) 301,308,305
  308 if (IE1.EQ.IE2) SJ32=SJ31
      if (LX.NE.0.AND.LX.NE.NLS) IBRNCH=7
C     do 310 L=1,NLS
C 310 EIG(L)=EIG(L)*UENRGY
      do 360 L=1,NLS
      do 360 LP=1,NLSP
      CTA(L,LP)=0.0
      CT4(L,LP)=0.0
      do 355 K=1,NLS
      ERAS=V(K,L)*D(K,LP)
      CTA(L,LP)=CTA(L,LP)+ABS(ERAS)
  355 CT4(L,LP)=CT4(L,LP)+ERAS
  360 CONTINUE
      if (scrJ4P-SJ32) 401,402,405
  401 BACKSPACE IE2
      BACKSPACE IE2
  402 BACKSPACE IE2
      BACKSPACE IE2
  405 READ (IE2) SJ32,NLSP, (EIGP(LP), (V(L,LP),L=1,NLSP), LP=1,NLSP),
     &  LCSERP,LDESP
      READ (IE2) LXP, (AAP(LP),AAPT(LP), LP=1,NLSP)
     &  ,NLST,IXC,IXC1, (EXC(J), (GAAXC(LP+NLST,J),LP=1,LXP), J=1,IXC1)
      if (scrJ4P-SJ32) 401,408,405
  408 if (IE1.EQ.IE2) SJ31=SJ32
      if (LXP.NE.0.AND.LXP.NE.NLSP) IBRNCH=7
C     do 410 L=1,NLSP
C 410 EIGP(L)=EIGP(L)*UENRGY
C
C    &     CALC SPECTRUM
C
      IJJP=0
      do 530 L=1,NLS
      IF(NLEVELS.GT.0.AND.L.GT.NLEVELS) GOTO 530
      IF(ELEVELS.GT.-1.AND.
     &  (EIG(L)-EIG(1)).GT.(EIG(NLS)-EIG(1))*ELEVELS) GOTO 530
C ---   CHANGE BY THOLE 28/08/86 (PREVIOUS 2 LINES INSERTED)
C ---   PRINT ONLY LINES FROM LOWEST LEVELS OF EACH J IN THE
C ---   GROUND CONFIGURATION. THESE LEVELS MAY BE DEGENERATE
C ---   , ESPECIALLY IN XPS SPECTRA WITH A CONTINUUM ORBITAL
C ---   IN THE GROUND CONFIGURATION.
      do 520 LP=1,NLSP
      if (EIG(L).LT.-4000.0.OR.EIGP(LP).LT.-4000.0) GO TO 520
      if (IE1.EQ.IE2.AND.scrJ4.EQ.scrJ4P.AND.L.GT.LP) GO TO 520
      ERASA=0.0
      ERAS=0.0
      do 455 K=1,NLSP
      ERASA=ERASA+CTA(L,K)*ABS(V(K,LP))
  455 ERAS=ERAS+CT4(L,K)*V(K,LP)
      ERASS=ERAS*ABS(ERAS)
      ERAS=ERAS*ERAS
      SUMS2=SUMS2+ERAS
      if (IE1.NE.IE2) GO TO 490
      if (scrJ4.NE.scrJ4P) GO TO 480
      if (L.EQ.LP) GO TO 520
  480 SUMS2=SUMS2+ERAS
  490 CONTINUE
      If (ERAS.GE.DMIN) GO TO 504
      I0=I0+1
      if (ERAS.GT.D0MAX) D0MAX=ERAS
      GO TO 520
  504 if (LCSER(L).LE.NCK(1).AND.LCSERP(LP).LE.NCK(2)) GO TO 510
      if (L1.NE.L2) GO TO 509
      if (LCSERP(LP).LE.NCK(1).AND.LCSER(L).LE.NCK(2)) GO TO 510
  509 ICDLT=ICDLT+1
      GO TO 520
  510 if (I.LT.KLAM) GO TO 515
      if (ITRUNC.GT.0) GO TO 511
      if (ITRUNC.EQ.0) GO TO 513
  511 I=ITRUNC
      I0=I00
      I0N=I0N0
      I0X=I0X0
      SUMS2=SUMS20
      SUMS2M=SUMSM0
C     if (DMIN.LE.0.0.AND.NTKEV.EQ.0) GO TO 560
C     do 512 I=1,IREAD
C 512 BACKSPACE IC
C     IREAD=0
C     DMIN=DMIN+MIN(DMIN,0.04)
C     GO TO 150
      GO TO 560
  513 ILOST=ILOST+1
      GO TO 520
  515 I=I+1
      IJJP=IJJP+1
      ERAS1=1.E-6*(scrJ4+0.01*FLOAT(L))
      T(I)=EIG(L)+ERAS1
      FJT(I)=scrJ4
      ERAS2=1.E-6*(scrJ4P+0.01*FLOAT(LP))
      TP(I)=EIGP(LP)+ERAS2
      FJTP(I)=scrJ4P
      GAA(I)=(2.0*scrJ4+1.0)*AA(L)
      GAAP(I)=(2.0*scrJ4P+1.0)*AAP(LP)
      GAAT(I)=(2.0*scrJ4+1.0)*AAT(L)
      GAAPT(I)=(2.0*scrJ4P+1.0)*AAPT(LP)
      S2(I)=ERAS
      SUMS2M=SUMS2M+ERAS
      SOABSS(I)=(ERASS+1.0E-30)/(ERASA*ERASA+1.0E-30)
      ISER(I)=LP-(NLSP-LXP)+NLST
      NCSER(I)=LCSER(L)
      NCSERP(I)=LCSERP(LP)
      LLDES(I)=LDES(L)
      LLDESP(I)=LDESP(LP)
      if (IE1.NE.IE2.OR.T(I).LT.TP(I)) GO TO 520
      T(I)=EIGP(LP)+ERAS2
      FJT(I)=scrJ4P
      TP(I)=EIG(L)+ERAS1
      FJTP(I)=scrJ4
      ERAS=GAA(I)
      GAA(I)=GAAP(I)
      GAAP(I)=ERAS
      ERAS=GAAT(I)
      GAAT(I)=GAAPT(I)
      GAAPT(I)=ERAS
      NCSER(I)=LCSERP(LP)
      NCSERP(I)=LCSER(L)
      LLDES(I)=LDESP(LP)
      LLDESP(I)=LDES(L)
  520 CONTINUE
  530 CONTINUE
      if (IPTG.EQ.1) WRITE (IW,52) scrJ4,scrJ4P,IJJP,I,KLAM,ILOST
   52 FORMAT (' ',2F6.1,4I8)
      if (I.EQ.KLAM.AND.ILOST.GT.0) GO TO 560
      GO TO 200
C
C    &     SORT LINES BY LOWER ENERGY LEVEL
C
  560 IMAX=I
      if (IMAX.EQ.0) GO TO 980
      ILOSTT=ILOSTT+ILOST
      IPT=0
      if (MOD(ISPECC,2).NE.0) IPT=1
      if (ISPECC.EQ.9) IPT=0
      if (IGEN.GE.5) IPT=IPTG
      if (IPT.EQ.0.AND.IGEN.LT.5) GO TO 567
      CALL SORT2(IMAX, 9,TP,ORD,T,FJT,FJTP,S2,SOABSS,GAA,GAAP,GAAT,
     &  GAAPT,DUM,DUM,DUM)
      CALL ORDER1(IMAX,ORD,ISER)
      CALL ORDER1(IMAX,ORD,NCSER)
      CALL ORDER1(IMAX,ORD,NCSERP)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      CALL SORT2(IMAX, 9,T,ORD,TP,FJT,FJTP,S2,SOABSS,GAA,GAAP,GAAT,
     &  GAAPT,DUM,DUM,DUM)
      CALL ORDER1(IMAX,ORD,ISER)
      CALL ORDER1(IMAX,ORD,NCSER)
      CALL ORDER1(IMAX,ORD,NCSERP)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      if (IGEN.LT.5) GO TO 567
      if (IMAX.LE.KTRAN) GO TO 565
      WRITE (IW,54) KTRAN,IMAX
   54 FORMAT (//26H0*****WARNING-- ONLY FIRST,I5,3H OF,I6,
     &  42H LINES RETAINED, FOR LACK OF STORAGE SPACE///)
      IMAX=KTRAN
  565 if (IPWR.NE.IRMN) GO TO 567
      EK=EKMINL+DELEKL*FLOAT(IBK)
      IBK=IBK+1
      if (IBK.GT.KBGKS) STOP 57
      BIGKS(IBK)=EK
  567 SUMFIU=0.0
      SUMFID=-0.0
      JCX=MIN(NSCONF(2,2),NCK(2))
      do 570 JC=1,JCX
  570 SUMFJ(JC)=0.0
      SUMGA=0.0
      S2MAX=0.0
      GFMAX=0.0
      I1=1
      J=0
      JNXX=JNX
      do 599 I=1,IMAX
      if (J.EQ.0 .AND. IPT.NE.0) THEN
        WRITE (IW,55) SPID(II),UENRGY,UENREV,
     &  (JN, (ELEM(L,JN,L1),L=1,3), (ELEM(L,JN,L2),L=1,3), JN=1,JNXX)
   55   FORMAT (1H1,60X,A8,9H SPECTRUM,5X,21H(ENERGIES IN UNITS OF,
     &  F8.1,7H CM-1 =,F8.2,4H EV)/(I5,6X,3A6,12H   ---      ,3A6))
        if (CSIGMA.EQ.0.0) WRITE (IW,56)
   56   FORMAT (/1H0,8X,7H   E   , 3X,8HJ   CONF,11X,8H   EP   , 2X,
     &  4H JP ,6H CONFP,10X,
     &  8H DELTA E,3X,9HLAMBDA(A),1X,9HS/PMAX**2,3X,2HGF,4X,6HLOG GF,1X,
     &  9HGA(SEC-1),9H CF,BRNCH/)
        if (CSIGMA.GT.0.0) WRITE (IW,57)
   57   FORMAT (/1H0,8X,7H   E   , 3X,8HJ   CONF,11X,8H   EP   , 2X,
     &  4H JP ,6H CONFP,10X,
     &  8H DELTA E,3X,9HLAMBDA(A),1X,9HS/PMAX**2,3X,2HGF,4X,6HLOG GF,1X,
     &  10HSIGMA(CM2),5H   CF/)
        J=52-JNXX
        JNXX=MIN(5,JNX)
      endif

      ERAS1=FNU(I)*UENRGY
      FLAM(I)=1.0E8/ERAS1
      if (S2(I).GT.S2MAX) S2MAX=S2(I)
      GF=PSQ*S2(I)*ERAS1
      if (IGEN.GE.5) GOSS(I,IBK)=GOSS(I,IBK)+GF
      if (IGEN.LT.5.AND.II.EQ.2) GF=GF/(FLAM(I)**2)
      ALGGF=-99.999
      if (GF.GT.0.0) ALGGF=ALOG10(GF)
      GA=0.66698*GF*ERAS1*ERAS1
      if (CSIGMA.GT.0.0) GA=CSIGMA*GF/(2.0*FJT(I)+1.0)
      if (GF.GT.GFMAX) GFMAX=GF
      SUMGF=SUMGF+GF
      SUMF=SUMF+GF/(2.0*FJT(I)+1.0)
      if (IPT.EQ.0) GO TO 599
      if (SOABSS(I).GT.0) THEN
        SIGNUM='+'
       else
        SIGNUM='-'
       endif
      WRITE (IW,58) I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),SIGNUM,GF,ALGGF,GA,
     &  SOABSS(I)
   58 FORMAT (' ',I4,F11.4,F5.1,I3,' (',A7,F13.4,F5.1,I3,' (',A7,F13.4,
     &  F12.4,F8.5,A1,F8.4,F8.3,1PE10.3,0PF8.4)
      J=J-1
      if (T(I).GT.TP(I)) GO TO 580
      SUMFIU=SUMFIU+GF
      JC=NCSERP(I)
      SUMFJ(JC)=SUMFJ(JC)+GF
      GO TO 584
  580 SUMGA=SUMGA+GA
      SUMFID=SUMFID-GF
  584 CONTINUE
      if (I.EQ.IMAX) GO TO 586
      if (T(I+1).EQ.T(I)) GO TO 599
  586 ERAS=2.0*FJT(I)+1.0
      FLIFE=9.9946E37
      if (SUMGA.GT.0.0) FLIFE=ERAS/SUMGA
      ERAS=1.0/ERAS
      SUMFIU=SUMFIU*ERAS
      SUMFID=SUMFID*ERAS
      SUMFI=SUMFIU+SUMFID
      WRITE (IW,59) T(I),FJT(I),SUMFIU,SUMFID,SUMFI
     &,FLIFE,SUMGA
   59 FORMAT (5X,F13.4,F5.1,17X,6HSUMFI=,F7.4,F8.4,2H =,F8.4,5X,
     &  9HLIFETIME=,1PE10.3,4H SEC,5X,6HSUMGA=,E10.3)
      do 590 JC=1,JCX
  590 SUMFJ(JC)=SUMFJ(JC)*ERAS
      if (JCX.GT.1) WRITE (IW,60) (SUMFJ(JC),JC, JC=1,JCX)
   60 FORMAT (6X,13HSUMF BY CONF=,10(F8.4,I3)/19X,10(F8.4,I3))
      do 592 L=I1,I
      BRNCH(L)=0.0
      if (T(L).LT.TP(L)) GO TO 592
      BRNCH(L)=GAAT(L)+SUMGA
      if (BRNCH(L).LE.0.0) GO TO 592
      ERAS1=FNU(L)*UENRGY
      BRNCH(L)=GAA(L)*0.66698*PSQ*S2(L)*(ERAS1**3)/BRNCH(L)
  592 CONTINUE
      I1=I+1
      SUMFIU=0.0
      SUMFID=-0.0
      do 595 JC=1,JCX
  595 SUMFJ(JC)=0.0
      SUMGA=0.0
      WRITE(IW,*)
      J=J-2
      if (JCX.GT.1) J=J-(JCX+9)/10
  599 CONTINUE
C
C    &     SORT LINES BY UPPER ENERGY LEVEL
C
      IPT=0
      if (ISPECC.GE.8) IPT=1
      if (MOD(ISPECC/2,2).NE.0) IPT=2
      if (IPT.EQ.0) GO TO 700
      BRNCHR=0.0
      do 602 I=1,6
  602 BRNCHT(I)=0.0
      BRNCHA=0.0
      do 605 J=1,KEXC
      do 604 I=1,KEXC
  604 BRNCHX(I,J)=0.0
      MEXC(J)=J
      ERAS=10000*JEXC(J)
  605 V(J,23)=ERAS+EXC(J)
      CALL SORT2(IXC1,1,V(1,23),ORD,EXC,
     &             DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM,DUM)
      CALL ORDER1(IXC1,ORD,MEXC)
      CALL ORDER1(IXC1,ORD,JEXC)
      CALL SORT2(IMAX,10,T,ORD,TP,FJT,FJTP,S2,SOABSS,
     &  GAA,GAAP,GAAT,GAAPT,BRNCH,DUM,DUM)
      CALL ORDER1(IMAX,ORD,ISER)
      CALL ORDER1(IMAX,ORD,NCSER)
      CALL ORDER1(IMAX,ORD,NCSERP)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      CALL SORT2(IMAX,10,TP,ORD,T,FJT,FJTP,S2,SOABSS,
     &  GAA,GAAP,GAAT,GAAPT,BRNCH,DUM,DUM)
      CALL ORDER1(IMAX,ORD,ISER)
      CALL ORDER1(IMAX,ORD,NCSER)
      CALL ORDER1(IMAX,ORD,NCSERP)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      SUMFIU=0.0
      SUMFID=-0.0
      SUMGA=0.0
      I1=1
      J=0
      JNXX=JNX
      do 640 I=1,IMAX
      ERAS1=FNU(I)*UENRGY
      FLAM(I)=1.0E8/ERAS1
      if (J.GT.0) GO TO 610
      if (IPT.LT.2) GO TO 610
      WRITE (IW,55) SPID(II),UENRGY,UENREV,
     &  (JN, (ELEM(L,JN,L1),L=1,3), (ELEM(L,JN,L2),L=1,3), JN=1,JNXX)
      WRITE (IW,56)
      J=52-JNXX
      JNXX=MIN(5,JNX)
  610 GF=PSQ*S2(I)*ERAS1
      if (IGEN.LT.5.AND.II.EQ.2) GF=GF/(FLAM(I)**2)
      ALGGF=-99.999
      if (GF.GT.0.0) ALGGF=ALOG10(GF)
      GA=0.66698*GF*ERAS1*ERAS1
      if (IPT.LT.2) GO TO 612
      WRITE (IW,58) I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),GF,ALGGF,GA,SOABSS(I)
      J=J-1
  612 if (T(I).LT.TP(I)) GO TO 615
      SUMFIU=SUMFIU+GF
      GO TO 617
  615 SUMGA=SUMGA+GA
      SUMGAR=SUMGAR+GA
      SUMFID=SUMFID-GF
  617 CONTINUE
      if (I.EQ.IMAX) GO TO 620
      if (TP(I+1).EQ.TP(I)) GO TO 640
  620 ERAS=2.0*FJTP(I)+1.0
      FLIFE=9.9946E37
      if (SUMGA.GT.0.0) FLIFE=ERAS/SUMGA
      ERAS=1.0/ERAS
      SUMFIU=SUMFIU*ERAS
      SUMFID=SUMFID*ERAS
      SUMFI=SUMFIU+SUMFID
      if (IPT.GE.1)
     &  WRITE (IW,62) TP(I),FJTP(I),SUMFIU,SUMFID,SUMFI,FLIFE,SUMGA
   62 FORMAT (12X,F13.3,F6.1,9X,6HSUMFI=,F7.4,F8.4,2H =,F8.4,5X,
     &  9HLIFETIME=,1PE10.3,4H SEC,5X,6HSUMGA=,E10.3)
      BRNCH(I)=GAAPT(I)+SUMGA
      if (BRNCH(I).LE.0.0) GO TO 630
      BRNCHA=BRNCHA+GAAP(I)*(GAAPT(I)-GAAP(I))/BRNCH(I)
      BRION=GAAPT(I)/BRNCH(I)
      if (IPT.LT.1.OR.BRION.LE.0.0) GO TO 622
      WRITE (IW,63) GAAPT(I),BRION
   63 FORMAT (82X,7HGAATOT=,1PE10.3,5X,6HBRION=,E10.3)
      J=J-1
  622 if (IXC.EQ.0) GO TO 625
      J2=ISER(I)
      do 624 JJ=1,IXC
      M1=MEXC(JJ)
      JEXC1(JJ)=JEXC(JJ)
      EXC1(JJ)=EXC(JJ)
      do 624 JK=1,IXC
      M2=MEXC(JK)
      BRNCHX(JJ,JK)=BRNCHX(JJ,JK)+GAAXC(J2,M1)*GAAXC(J2,M2)/BRNCH(I)
  624 CONTINUE
  625 ERAS=0.0
      do 627 L=I1,I
      if (T(L).GT.TP(L)) GO TO 627
      GA=0.66698*PSQ*S2(L)*((FNU(L)*UENRGY)**3)
      BRNCH(L)=GAAP(I)*GA/BRNCH(I)
      BRNCHR=BRNCHR+BRNCH(L)
      ERAS=ERAS+BRNCH(L)
  627 CONTINUE
C    &     PRINT T-DEPENDENCE OF BRNCH, SUMMED OVER UPPER LEVELS
      if (NTKEV.EQ.0) GO TO 630
      NC=NCSERP(I)
      LL=NPAV(NC)
      ERAS1=VPAR(LL,L2)-TP(I)
      NP1=NTKEV+1
      do 629 L=1,NP1
      BRNCHL(L)=ERAS*EXP(ERAS1/TUNRGY(L))
  629 BRNCHT(L)=BRNCHT(L)+BRNCHL(L)
      if (NPTKEV.GT.1) WRITE (IW,64) (TKEV(L),BRNCHL(L), L=1,NP1)
   64 FORMAT (18H T(KEV), BRNCH=   ,6(0PF6.1,1PE11.4))
      J=J-1
  630 CONTINUE
      I1=I+1
      SUMFIU=0.0
      SUMFID=-0.0
      SUMGA=0.0
      WRITE (IW,*)
      J=J-2
  640 CONTINUE
      JNXX=JNX
      if (NPTKEV.GT.0.AND.BRNCHR.GT.0.0)
     &  WRITE (IW,55) SPID(II),UENRGY,UENREV,
     &  (JN, (ELEM(L,JN,L1),L=1,3), (ELEM(L,JN,L2),L=1,3), JN=1,JNXX)
C
      WRITE (IW,65) BRNCHR,BRNCHA
   65 FORMAT (//14X,10H GM*FRBAR=,1PE11.4,10X,9HGM*FABAR=,E11.4)
      NP1=NTKEV+1
      if (NTKEV.GT.0) WRITE (IW,64) (TKEV(L),BRNCHT(L), L=1,NP1)
      if (NPTKEV.EQ.0.OR.NPTKEV.EQ.2) GO TO 670
      JNXX=MIN(JNXX,3)
      WRITE (13,71) (ELEM(L,1,1),L=1,3),((ELEM(L,JN,2),L=1,3),JN=1,JNXX)
   71 FORMAT (4(3A6,2X))
      WRITE (13,72) (TKEV(L), L=2,NP1)
   72 FORMAT (37X,11HT(KEV)=INF.,2X,5F6.2)
      GM=1.0
      NN=NSCONF(2,2)
      LL=0
      do 642 I=1,NOSUBC
      MM=NOSUBC-I+1
      ERAS=NIJK(MM,NN,2)
      if (ERAS.GT.0.0) LL=LL+1
      if (LL.EQ.1) GO TO 642
      ERAS1=4.0*FLLIJK(MM,NN,2)+2.0
      GM=GM*FCTRL(ERAS1)/(FCTRL(ERAS)*FCTRL(ERAS1-ERAS))
  642 CONTINUE
      READ(ELEM(2,NN,2),'(F6.2)') EKIN
      EKIN=EKIN*0.0136058
      do 644 L=2,NP1
  644 BRNCHT(L)=BRNCHT(L)/BRNCHT(1)
      BRNCHT(1)=BRNCHT(1)/GM
      NC=NCSERP(IMAX)
      LL=NPAV(NC)
      EKEV=VPAR(LL,L2)*UENRGY/8065479.0
      DEKEV=0.0
      if (EIONRY.NE.0.0) DEKEV=EIONRY*0.0136058+EKIN-EKEV
      EKEV=EKEV+DEKEV
      if (BRNCHT(NP1).GE.1.0) WRITE (13,77) (ELEM(L,1,1),L=1,2),
     &  EKEV,GM,(ELEM(L,1,2),L=2,3),EKIN,(BRNCHT(L),L=1,NP1)
   77 FORMAT (2A6,F8.4,F4.1,1X,A6,A3,F7.3,E9.2,5F6.3)
      if (BRNCHT(NP1).LT.1.0) WRITE (13,78) (ELEM(L,1,1),L=1,2),
     &  EKEV,GM,(ELEM(L,1,2),L=2,3),EKIN,(BRNCHT(L),L=1,NP1)
   78 FORMAT (2A6,F8.4,F4.1,1X,A6,A3,F7.3,E9.2,5F6.4)
C    &     SUM BRNCH OVER UPPER LEVELS (FOR VARIOUS T)
      CALL SORT2(IMAX,8,T,ORD,TP,FJT,FJTP,S2,SOABSS,
     &  GAA,GAAP,BRNCH,DUM,DUM,DUM,DUM)
      CALL ORDER1(IMAX,ORD,NCSER)
      CALL ORDER1(IMAX,ORD,NCSERP)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      WRITE (IW,66) (TKEV(L),L=2,6)
   66 FORMAT (/8X,11HLOWER LEVEL, 13H  T(KEV)=INF.,5F17.2)
      do 650 L=1,6
  650 BRNCHL(L)=1.0E-20
      do 665 I=1,IMAX
      NC=NCSERP(I)
      LL=NPAV(NC)
      ERAS1=VPAR(LL,L2)-TP(I)
      if (T(I).GE.TP(I)) GO TO 656
      do 654 L=1,NP1
  654 BRNCHL(L)=BRNCHL(L)+BRNCH(I)*EXP(ERAS1/TUNRGY(L))
  656 if (I.EQ.IMAX) GO TO 658
      if (T(I+1).EQ.T(I)) GO TO 665
  658 WRITE (IW,67) NCSER(I),T(I),FJT(I), (BRNCHL(L), L=1,NP1)
   67 FORMAT (I5,F13.4,F5.1,1PE12.4,5E17.4)
      do 660 L=2,NP1
  660 BRNCHL(L)=BRNCHL(L)/BRNCHL(1)
      BRNCHL(1)=BRNCHL(1)/GM
      EKEV=T(I)*UENRGY/8065479.0+DEKEV
      NC=NCSER(I)
      if (BRNCHL(NP1).GE.1.0) WRITE (13,77) (ELEM(L,NC,1),L=1,2),
     &  EKEV,FJT(I),(ELEM(L,1,2),L=2,3),EKIN,(BRNCHL(L),L=1,NP1)
      if (BRNCHL(NP1).LT.1.0) WRITE (13,78) (ELEM(L,NC,1),L=1,2),
     &  EKEV,FJT(I),(ELEM(L,1,2),L=2,3),EKIN,(BRNCHL(L),L=1,NP1)
      do 663 L=1,6
  663 BRNCHL(L)=1.0E-20
  665 CONTINUE
      ERAS=-1.0
      WRITE (13,'(F20.4)') ERAS
C    &     WRITE BRANCHING FACTORS FOR AUTOIONIZATION
C    &          CONTRIBUTIONS TO COLLISIONAL EXCITATION
  670 if (IXC.EQ.0) GO TO 700
      WRITE (IW,81) (JEXC(J),EXC(J), J=1,IXC)
   81 FORMAT (//25X,3HEXC,6X,6HBRNCHX//32X,10(I3,F7.2)/32X,10(I3,F7.2))
      WRITE (IW,82)
      do 672 I=1,IXC
      J=JEXC(I)
  672 WRITE (IW,82) J, (ELEM(M,J,2),M=1,2), EXC(I),(BRNCHX(I,L),L=1,IXC)
   82 FORMAT (I3,2X,2A6,F12.4,3X,1P10E10.2/(32X,1P10E10.2))
      if (IXC.LE.1) GO TO 700
      IXC2=0
      if (NSCONF(2,2).LE.2) GO TO 690
C    &     SUM OVER L OF CONTINUUM ELECTRON
      do 673 J=1,IXC
      N=JEXC(J)
  673 READ(ELEM(2,N,2),'(A4)') ELEMJ(J)
      NETOT=0
      do 675 N=1,NOSUBC
  675 NETOT=NETOT+NIJK(N,1,2)
      NC=JEXC(IXC)-JEXC(1)
      if (NC.LE.0) GO TO 690
      IXC1=IXC
      MIXC=JEXC(IXC)
      I=0
  676 I=I+1
      if (I.GE.IXC1) GO TO 684
      MI=JEXC1(I)
      if (MI.EQ.MIXC) GO TO 684
      J=I
  678 J=J+1
      if (J.GT.IXC1) GO TO 676
      MJ=JEXC1(J)
      if (MJ.EQ.MI) GO TO 678
      if (ABS(EXC1(J)-EXC1(I)).GT.0.01) GO TO 678
      NE=0
      do 679 N=1,NOSUBC
      NE=NE+MAX(NIJK(N,MI,2),NIJK(N,MJ,2))
      if (NE.GE.NETOT) GO TO 681
      if (NIJK(N,MI,2).NE.NIJK(N,MJ,2)) GO TO 678
  679 CONTINUE
  681 do 682 N1=1,IXC
  682 BRNCHX(N1,I)=BRNCHX(N1,I)+BRNCHX(N1,J)
      IXC1=IXC1-1
      do 683 N=J,IXC1
      JEXC1(N)=JEXC1(N+1)
      EXC1(N)=EXC1(N+1)
      ELEMJ(N)=ELEMJ(N+1)
      do 683 N1=1,IXC
  683 BRNCHX(N1,N)=BRNCHX(N1,N+1)
      J=J-1
      GO TO 678
  684 if (IXC1.EQ.IXC) GO TO 690
      WRITE (IW,81) (JEXC1(J),EXC1(J), J=1,IXC1)
  686 WRITE (IW,82)
      do 687 I=1,IXC
      J=JEXC(I)
  687 WRITE (IW,82) J,(ELEM(M,J,2),M=1,2), EXC(I),(BRNCHX(I,L),L=1,IXC1)
      if (IXC2.GT.0) GO TO 700
C    &     SUM OVER LEVELS WITHIN THE CORE
  690 I=0
      IXC2=IXC1
  691 I=I+1
      if (I.EQ.IXC2) GO TO 695
      MI=JEXC1(I)
      J=I+1
  692 MJ=JEXC1(J)
      if (MJ.NE.MI) GO TO 691
      do 693 N=1,IXC
  693 BRNCHX(N,I)=BRNCHX(N,I)+BRNCHX(N,J)
      IXC2=IXC2-1
      if (I.EQ.IXC2) GO TO 695
      do 694 J1=J,IXC2
      JEXC1(J1)=JEXC1(J1+1)
      EXC1(J1)=EXC1(J1+1)
      do 694 N=1,IXC
  694 BRNCHX(N,J1)=BRNCHX(N,J1+1)
      GO TO 692
  695 if (IXC2.EQ.IXC1) GO TO 700
      do 697 J=1,IXC2
      N=JEXC1(J)
  697 ELEMJ(J)=ELEM(2,N,2)
      WRITE (IW,87) (JEXC1(J),ELEMJ(J), J=1,IXC2)
   87 FORMAT(//25X,3HEXC,6X,6HBRNCHX//32X,10(I3,2X,A5)/32X,10(I3,2X,A5))
      IXC1=IXC2
      GO TO 686
C
C    &     SORT LINES BY WAVENUMBER
C
  700 IPT=0
      if (ISPECC.GE.4) IPT=2
      if (ISPECC.EQ.9) IPT=1
      if (IPT.EQ.0) GO TO 980
      do 699 I=1,IMAX
  699 GAAPT(I)=FNU(I)
C----   GAAPT IS USED AS TEMPORARY ARRAY FOR SORTING
      CALL SORT2(IMAX,9,GAAPT,ORD,T,TP,FJT,FJTP,S2,SOABSS,
     &  GAA,GAA,BRNCH,DUM,DUM,DUM)
      CALL ORDER3(IMAX,ORD,LLDES)
      CALL ORDER3(IMAX,ORD,LLDESP)
      IN=1
      IX=IMAX
      do 701 I=1,IMAX
      if (FNU(I).LT.FNUN) IN=I+1
      if (FNU(I).LE.FNUX) GO TO 701
      IX=I-1
      GO TO 702
  701 CONTINUE
  702 if (IN.EQ.1) GO TO 704
      I0N=IN-1
      I0X=IMAX-IX
      J=0
      do 703 I=IN,IX
      J=J+1
      D(J,1)=D(I,1)
      T(J)=T(I)
      FJT(J)=FJT(I)
      NCSER(J)=NCSER(I)
      LLDES(J)=LLDES(I)
      TP(J)=TP(I)
      FJTP(J)=FJTP(I)
      NCSERP(J)=NCSERP(I)
      LLDESP(J)=LLDESP(I)
      S2(J)=S2(I)
      SOABSS(J)=SOABSS(I)
      GAA(J)=GAA(I)
      GAAP(J)=GAAP(I)
  703 BRNCH(J)=BRNCH(I)
  704 IMAX=IX-IN+1
      NLINES=NLINES+IMAX
      FNUNP=MIN(FNUNP,FNU(1))
      FNUXP=MAX(FNUXP,FNU(IMAX))
      if (ISPECC.GE.8) WRITE (20) IMAX,PSQ, (FNU(I),S2(I),
     &  BRNCH(I), I=1,IMAX)
C
      J=0
      JNXX=JNX
      do 720 I=1,IMAX
      ERAS1=FNU(I)*UENRGY
      FLAM(I)=1.0E8/ERAS1
      GF=PSQ*S2(I)*ERAS1
      if (IGEN.LT.5.AND.II.EQ.2) GF=GF/(FLAM(I)**2)
      ALGGF=-99.999
      if (GF.GT.0.0) ALGGF=ALOG10(GF)
      GA=0.66698*GF*ERAS1*ERAS1
      if (J.GT.0) GO TO 710
      if (IPT.EQ.1) GO TO 709
      WRITE (IW,55) SPID(II+1),UENRGY,UENREV,
     &  (JN, (ELEM(L,JN,L1),L=1,3), (ELEM(L,JN,L2),L=1,3), JN=1,JNXX)
      WRITE (IW,56)
  709 J=52-JNXX
      JNXX=MIN(5,JNX)
  710 J=J-1
      if (IPT.LT.2) GO TO 720
      if (IBRNCH.EQ.0) GO TO 715
      WRITE (IW,88) I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),GF,ALGGF,GA,BRNCH(I)
   88 FORMAT (' ',I4,F11.4,F5.1,I3,' (',A7,F13.4,F5.1,I3,' (',A7,F13.4,
     &  F12.4,F8.3,F9.4,F8.3,1PE10.3,E9.2)
      GO TO 720
  715 WRITE (IW,58) I,T(I),FJT(I),NCSER(I),LLDES(I),TP(I),FJTP(I),
     &  NCSERP(I),LLDESP(I),FNU(I),FLAM(I),S2(I),GF,ALGGF,GA,SOABSS(I)
  720 CONTINUE
C
C
  980 if (IPTG.EQ.1) CALL STAMP  ('STAMP')
      if (scrJ4.LT.0.0) GO TO 985
      I=0
      if (IMAX.NE.ITRUNC) GO TO 180
      IREAD=IREAD-1
      BACKSPACE IC
      GO TO 200
C
  985 READ (IC) SOPI2
      IREAD=IREAD+1
      SSOPX2=0.0
      do 986 I=1,JNX1
      do 986 J=1,JNX2
  986 SSOPX2=SSOPX2+SOPI2(I,J)*(PMUP(I,J)**2)
      SSOPX2=SSOPX2*(A**2)
      if (ISPECC.LT.8) GO TO 988
      if (IMAX.EQ.0) GO TO 988
      IRAS=5
      ERAS=-1.0
      WRITE (20) IRAS,ERAS, (ERAS,ERAS,ERAS, I=1,IRAS)
C     CALL WNDIST(II)
  988 CONTINUE
C
      if (IPTG.EQ.0) GO TO 991
      CALL CLOK(TIME)
      DELT=TIME-TIME0
      WRITE (IW,96) DELT,TIME,PMAX,SSOPX2,SUMS2,SUMS2M,SUMGF,
     &  SUMF,SUMGAR,DMIN,I0,D0MAX,I0N,FLBDX,I0X,FLBDN,ILOSTT,ICDLT,NCK
   96 FORMAT (//6H TIME=,F8.2,4H MIN,3X,10H(ABS TIME=,F8.2,1H),
     &   5X,9HFOR PMAX=,F9.5,1H,,F17.4/57X,12HSUMS2,SUMGF=,F10.4,F11.5,
     &  F9.4/64X,5HSUMF=,F30.4,9X,7HSUMGAR=,1PE11.4/57X,6HS2MIN=,0PF27.5
     &  //29X,I5,28H LINES OMITTED, WITH MAX S2=,F28.5//29X,I5,
     &    ' LINES OMITTED, WITH LAMBDA.GT.',F14.4/29X,I5,
     &    ' LINES OMITTED, WITH LAMBDA.LT.',F14.4/29X,I5,
     &    ' LINES OMITTED, INSUFF STORAGE '/29X,I5,
     &    ' LINES OMITTED, CONF SERIAL NOS .GT.',I4,1H,,I4)
      WRITE (IW,97) (COUPL(I), I=1,7)
   97 FORMAT (///' AVE PURITIES',29X,9(A6,2X)/)
      do 990 K1=L1,L2
      J1X=NSCONF(2,K1)
      WRITE(IW,98) ((ELEM(I,1+JN*(J1X-1),K1), I=1,3), JN=0,1),
     &  (AVP(I,K1), I=1,7), AVCG(K1),AVEIG(K1)
   98 FORMAT (1X,3A6,3H---,3A6,7F8.3,5X,6H  EAV=,F10.4/101X,6HAVEIG=,
     &  F10.4)
  990 CONTINUE
C
  991 if (II.EQ.-1) RETURN
      if (IGEN.LT.5) GO TO 100
      if (IGEN.GT.7.OR.MOD(IBK-1,5).NE.0) GO TO 100
      IMAX=MIN(IMAX,30)
      WRITE (IW,99) IBK,BIGKS(IBK),IPWR,IMAX, (GOSS(I,IBK), I=1,IMAX)
   99 FORMAT (/'0WEIGHTED GENERALIZED OSC. STRENGTHS AT BIGKS(',I3,
     &  2H)=,F12.7,8H   IPWR=,I1,12H   FOR FIRST,I3,6H LINES//(10F12.8))
      GO TO 100
C
      END


      SUBROUTINE CALCV
C
C    &     CALC V MATRIX (DERIVATIVES OF EIGENVALUES WRT PARAMETERS)
C
C     IMPLICIT INTEGER*4 (I-N)
      PARAMETER (KS=8,KC=10,klsi=400,KLS1=KLSI+1,klsc=10000,
     &  KJK=100,kjp=1200,KMX=3*KLSI,
     &  kiser=605248,KPR=100,KPC=KISER)
      COMMON/IDSKNO/IW,ID2,ID3,ID4,ILA,ILB,IL,IC,IE
      CHARACTER*8 LSYMB*1,COUPL*6,PARNAM
      COMMON /CHAR1/ LSYMB(24),COUPL(12),PARNAM(KPR,2)
      COMMON/C1/KCPL,NOCSET,NSCONF(3,2),IABG,IV,NLSMAX,NLT11,NEVMAX,
     &  KCPLD(9),IELPUN,SJN,SJX,IMAG,IQUAD(KS),UENRGY,DMIN,ILNCUV,IPLEV,
     &  ICPC,ICFC,IDIP,IENGYD,ISPECC,IPCT,ICTC,ICTBCD,KCPL1,
     &  NOSUBC,KK,INS,TIME0,TIME,DELT,
     &  NDIFFT,NOTOTT,SJ4MN,SJ4MX,NscrJ4,scrJ4,scrJ4P,NLS,NLSP,NJJ,
     &  NNN,NDIFSJ,KPAR,NPAR,CAVE,IX,SUMS20,NOPC,J1A,J1B,
     &  NIJK(KS,KC,2),NOTSJ1(KS,100),NOT,NLT,NORCD,A,LCI,LCIP,J1X,
     &  ICNVRT(2),NLASTT(KS),NDIFFJ(KS),NOTOTJ(KS),NTOTTJ(100),
     &  NTOTJJ(KS,100),FLLIJK(KS,KC,2),
     &  IIPRTY,IPRITY(2),SSOPI2,SOPI2(12,KC),IPLOTC,NCK(2)
      CHARACTER LL*1,LBCD*1,ALF*2,LLIJK*1
      COMMON /CHAR2/ LL(KS),LBCD(KLS1,KS),ALF(KLS1,KS),LLIJK(KS,KC,2)
      COMMON/C2/NI(KS),FLL(KS),MULT(KLS1,KS),
     &  FL(KLS1,KS),S(KLS1,KS),NTRMK(KLSC),FLLRHOP,FLLSIGP
      COMMON/C3/ISUBJ(KC),MULS(KS),
     &  NALS(KMX,KS),NCFG(KMX),NALSJ(KMX,KS),scrL(KMX,KS),scrS(KMX,KS),
     &  FJ(KMX,KS),scrJ(KMX,KS), MUL(KS),LF(KLSC),NSJK(110),
     &  FK(KMX),FKJ(KMX),scrL6(KMX),scrS6(KMX),FK6(KMX),
     &  MULTQQ(KMX),NOICRD,NALSJP(KJP,KS),PJ(KJP,KS)
      COMMON       C(KMX,KMX)
      COMMON/C3LC2/TMX(KMX,KMX)
      COMMON/C3LC3/CT4(KMX,KMX)
      COMMON/C3LC4/VECT(KMX,KMX)
      COMMON    VPAR(KPR,2),AVP(9,2),AVEIG(2),AVCG(2),
     &  NPARK(2),NPAV(KC),    EIG(KMX),FNT(9,2),I1,I2
     &  ,FACT(4)
C
      DIMENSION V(KPR,KMX)
      EQUIVALENCE (C,V)
C    &     NOTE---V(KPR,KMX) SPILLS OVER INTO /C3LC2/
C
      M=NOICRD
      if (M.EQ.0) GO TO 900
      K=KK
      do 100 JJ=1,M
  100 BACKSPACE IC
      NLST=MIN(NLS,NEVMAX)
C
C    &     CALC V MATRIX
C
      do 229 IPAR=1,NPAR
      do 205 J=1,J1X
      if (IPAR.NE.NPAV(J)) GO TO 205
      do 202 L=1,NLS
      do 201 LP=1,NLS
  201 CT4(L,LP)=0.0
      if (NCFG(L).EQ.J) CT4(L,L)=1.0
  202 CONTINUE
      GO TO 212
  205 CONTINUE
C
      call  getsparse ((ic), ct4, nls, nls, kmx,.true.)
c      READ (IC) ((CT4(L,LP),L=LP,NLS), LP=1,NLS)

c      do 210 L=1,NLS
c      do 210 LP=1,L
c  210 CT4(LP,L)=CT4(L,LP)
C
  212 do 220 M=1,NLST
      V(IPAR,M)=0.0
      do 220 L=1,NLS
      A=0.0
      do 215 LP=1,NLS
  215 A=A+CT4(L,LP)*VECT(LP,M)
  220 V(IPAR,M)=V(IPAR,M)+VECT(L,M)*A
  229 CONTINUE
C
C
C
      WRITE (IW,35)
   35 FORMAT (////9H V MATRIX)
      LX=0
  310 WRITE (IW,36)
   36 FORMAT (' ')
      LN=LX+1
      LX=LX+11
      if (LX.GT.NLST) LX=NLST
      do 320 I=1,NPAR
  320 WRITE (IW,37) PARNAM(I,K), (V(I,L), L=LN,LX)
   37 FORMAT (2X,A6,7X,11F9.5)
      if (LX.LT.NLST) GO TO 310
C
      M=NOICRD-NPAR+J1X
      if (M.LE.0) GO TO 900
      do 400 I=1,M
  400 READ (IC) A
C
  900 CALL STAMP  ('STAMP')
      RETURN
      END


      SUBROUTINE MLEW(KMX,NLS,VECT,EIG,FK,C,M)
C
C
C     IMPLICIT INTEGER*4 (I-N)
      DIMENSION VECT(KMX,KMX),EIG(KMX),FK(KMX),C(KMX,KMX)
C      CALL RCSMAA (VECT,KMX,NLS,EIG,FK,M)
C      PRINT *,' IER=',M
      CALL TRED2 (KMX,NLS,VECT,EIG,FK,C)
      CALL TQL2  (KMX,NLS,EIG,FK,C,M)
      RETURN
      END

      SUBROUTINE TRED2(NM, N, A, D, E, Z)
      DIMENSION A(NM,N), D(N), E(N), Z(NM,N)
      do 20 I=1,N
C
         do 10 J=1,I
            Z(I,J) = A(I,J)
10       CONTINUE
20    CONTINUE
C
      if (N.EQ.1) GO TO 160
C     ********** FOR I=N STEP -1 UNTIL 2 do -- **********
      do 150 II=2,N
         I = N + 2 - II
         L = I - 1
         H = 0.0
         SCALE = 0.0
         if (L.LT.2) GO TO 40
C     ********** SCALE ROW (ALGOL TOL THEN NOT NEEDED) **********
         do 30 K=1,L
            SCALE = SCALE + ABS(Z(I,K))
30       CONTINUE
C
         if (SCALE.NE.0.0) GO TO 50
40       E(I) = Z(I,L)
         GO TO 140
C
50       do 60 K=1,L
            Z(I,K) = Z(I,K)/SCALE
            H = H + Z(I,K)*Z(I,K)
60       CONTINUE
C
         F = Z(I,L)
         G = -SIGN(SQRT(H),F)
         E(I) = SCALE*G
         H = H - F*G
         Z(I,L) = F - G
         F = 0.0
C
         do 100 J=1,L
            Z(J,I) = Z(I,J)/(SCALE*H)
            G = 0.0
C     ********** FORM ELEMENT OF A*U **********
            do 70 K=1,J
               G = G + Z(J,K)*Z(I,K)
70          CONTINUE
C
            JP1 = J + 1
            if (L.LT.JP1) GO TO 90
C
            do 80 K=JP1,L
               G = G + Z(K,J)*Z(I,K)
80          CONTINUE
C     ********** FORM ELEMENT OF P **********
90          E(J) = G/H
            F = F + E(J)*Z(I,J)
100      CONTINUE
C
         HH = F/(H+H)
C     ********** FORM REDUCED A **********
         do 120 J=1,L
            F = Z(I,J)
            G = E(J) - HH*F
            E(J) = G
C
            do 110 K=1,J
               Z(J,K) = Z(J,K) - F*E(K) - G*Z(I,K)
110         CONTINUE
120      CONTINUE
C
         do 130 K=1,L
            Z(I,K) = SCALE*Z(I,K)
130      CONTINUE
C
140      D(I) = H
150   CONTINUE
C
160   D(1) = 0.0
      E(1) = 0.0
C     ********** ACCUMULATION OF TRANSFORMATION MATRICES **********
      do 220 I=1,N
         L = I - 1
         if (D(I).EQ.0.0) GO TO 200
C
         do 190 J=1,L
            G = 0.0
C
            do 170 K=1,L
               G = G + Z(I,K)*Z(K,J)
170         CONTINUE
C
            do 180 K=1,L
               Z(K,J) = Z(K,J) - G*Z(K,I)
180         CONTINUE
190      CONTINUE
C
200      D(I) = Z(I,I)
         Z(I,I) = 1.0
         if (L.LT.1) GO TO 220
C
         do 210 J=1,L
            Z(I,J) = 0.0
            Z(J,I) = 0.0
210      CONTINUE
C
220   CONTINUE
C
      RETURN
      END


      SUBROUTINE TQL2(NM, N, D, E, Z, IERR)
      DIMENSION D(N), E(N), Z(NM,N)
C     IMPLICIT INTEGER*4 (I-N)
      REAL MACHEP
C               **********
      MACHEP = 2.**(-23)
C     ********** MACHEP IS A MACHINE DEPENDENT PARAMETER SPECIFYING
C                THE RELATIVE PRECISION OF FLOATING POINT ARITHMETIC.
C
      IERR = 0
      if (N.EQ.1) GO TO 160
C
      do 10 I=2,N
         E(I-1) = E(I)
10    CONTINUE
C
      F = 0.0
      B = 0.0
      E(N) = 0.0
C
      do 110 L=1,N
         J = 0
         H = MACHEP*(ABS(D(L))+ABS(E(L)))
         if (B.LT.H) B = H
C     ********** LOOK FOR SMALL SUB-DIAGONAL ELEMENT **********
         do 20 M=L,N
            if (ABS(E(M)).LE.B) GO TO 30
C     ********** E(N) IS ALWAYS ZERO, SO THERE IS NO EXIT
C                THROUGH THE BOTTOM OF THE LOOP **********
20       CONTINUE
30       if (M.EQ.L) GO TO 100
40       if (J.EQ.30) GO TO 150
         J = J + 1
C     ********** FORM SHIFT **********
         P = (D(L+1)-D(L))/(2.0*E(L))
         R = SQRT(P*P+1.0)
         H = D(L) - E(L)/(P+SIGN(R,P))
C
         do 50 I=L,N
            D(I) = D(I) - H
50       CONTINUE
C
         F = F + H
C     ********** QL TRANSFORMATION **********
         P = D(M)
         C = 1.0
         S = 0.0
         MML = M - L
C     ********** FOR I=M-1 STEP -1 UNTIL L do -- **********
         do 90 II=1,MML
            I = M - II
            G = C*E(I)
            H = C*P
            if (ABS(P).LT.ABS(E(I))) GO TO 60
            C = E(I)/P
            R = SQRT(C*C+1.0)
            E(I+1) = S*P*R
            S = C/R
            C = 1.0/R
            GO TO 70
60          C = P/E(I)
            R = SQRT(C*C+1.0)
            E(I+1) = S*E(I)*R
            S = 1.0/R
            C = C*S
70          P = C*D(I) - S*G
            D(I+1) = H + S*(C*G+S*D(I))
C     ********** FORM VECTOR **********
            do 80 K=1,N
               H = Z(K,I+1)
               Z(K,I+1) = S*Z(K,I) + C*H
               Z(K,I) = C*Z(K,I) - S*H
80          CONTINUE
C
90       CONTINUE
C
         E(L) = S*P
         D(L) = C*P
         if (ABS(E(L)).GT.B) GO TO 40
100      D(L) = D(L) + F
110   CONTINUE
C     ********** ORDER EIGENVALUES AND EIGENVECTORS **********
      do 140 II=2,N
         I = II - 1
         K = I
         P = D(I)
C
         do 120 J=II,N
            if (D(J).GE.P) GO TO 120
            K = J
            P = D(J)
120      CONTINUE
C
         if (K.EQ.I) GO TO 140
         D(K) = D(I)
         D(I) = P
C
         do 130 J=1,N
            P = Z(J,I)
            Z(J,I) = Z(J,K)
            Z(J,K) = P
130      CONTINUE
C
140   CONTINUE
C
      GO TO 160
C     ********** SET ERROR -- NO CONVERGENCE TO AN
C                EIGENVALUE AFTER 30 ITERATIONS **********
150   IERR = L
160   RETURN
C     ********** LAST CARD OF TQL2 **********
      END

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      subroutine send
      common /butlerout/ outline, ipos
      character*80 outline

      if (ipos.gt.1) write(ibutler(),'(A)') outline(1:ipos-1)
      ipos=2
      end

      subroutine writebutler
      common /butlerout/ outline, ipos
      character*80 outline
      character*1 c

      entry putinit()
      outline(1:1)=' '
      ipos=2
      return

      entry putinteger(i)
      if (ipos.gt.76) call send
      write (outline(ipos:),'(I5)') i
      ipos=ipos+5
      return

      entry putintreal(i,x)
      if (ipos.gt.58) call send
      write (outline(ipos:),'(A2,I5,G16.8)') ', ', i, x
      ipos=ipos+23
      do 1 j = ipos-1, ipos-5, -1
         if (outline(j:j).ne.' ') then
            ipos=j+1
            return
         endif
 1    continue
      return

      entry putchar(c)
      if (ipos.gt.80) call send
      write (outline(ipos:),'(A)') c
      ipos=ipos+1
      return

      end

      subroutine testzeros(a,n,m,kmx,nonzeros)
      dimension a(kmx,m)

      xnorm2=0
      do 5 i = 1,n
         do 5 j = 1,m
 5          xnorm2=xnorm2+a(i,j)**2
      small=sqrt(xnorm2/(n*m))*1e-10
      nonzeros=0
      do 10 i = 1,n
         do 10 j = 1,m
            if (abs(a(i,j)).le.small) then
               a(i,j)=0
            else
               nonzeros=nonzeros+1
            endif
 10   continue
      end

      SUBROUTINE BUTLER(NAME,K1,K2,A,REPBRA,IPARBRA,REPOPR,IPAROPR,
     &  REPKET,IPARKET,N,M,DKMXD)
      PARAMETER (klsi=400,KMX=3*KLSI)
C     IMPLICIT INTEGER*4 (I-N)
      DIMENSION  A(KMX,M)
      dimension aa(kmx),jj(kmx)
      CHARACTER  NAME*(*),IRREP*5,STATE*11
      CHARACTER  IRBRA*5,IROPR*5,IRKET*5

      ifile=ibutler()
      if (ifile.NE.0) THEN
      call transform ( a, n, m )
      IRBRA = IRREP(REPBRA,IPARBRA)
      IROPR = IRREP(REPOPR,IPAROPR)
      IRKET = IRREP(REPKET,IPARKET)
      call testzeros(a,n,m,kmx,nonzeros)
      WRITE (ifile,*)
      WRITE (ifile,'(11A,2I6,I10)')
     &      ' RME ',STATE(K1,K2),' ',
     &        IRBRA,' ',IROPR,' ',IRKET,' ',NAME,' ',N,M, nonzeros

      if (NAME.NE.'HAMILTONIAN') THEN
        FACTOR=1.0
      else
        FACTOR=SQRT(2.*REPBRA+1)
      endif
      call putinit
      do 105 i = 1,n
        none=0
        do 110 j = 1,m
          if (a(i,j).ne.0.0) then
             none=none+1
             aa(none) =a(i,j)
             jj(none) =j
          endif
  110   continue
      
        if (none.ne.0) then
          write(ifile,
     &     '(1x,i4,1x,i4,8x,a,3(i4,f14.6,a):/4(i4,f14.6,a))')
     &      i,none,(',',jj(j),factor * aa(j),j=1,none),' ;'

c            call putinteger(i)
c            call putinteger(none)
c            do 120 j = 1,m
c               if (a(i,j).ne.0.0) then
c                  call putintreal(j,factor * a(i,j))
c               endif
c 120        continue
c            call putchar(';')
c            call send
        endif
 105  continue
c      call send
      write (ifile,*)
      endif
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      CHARACTER*5 FUNCTION IRREP(S,IPAR)
      CHARACTER*5 IRR
      if (S.EQ.INT(S)) THEN
         WRITE (IRR(1:3),'(I3)') INT(S)
      else if (S.LT.10) THEN
         WRITE (IRR(1:3),'(A2,I1)') ' s',INT(S)
      else
         WRITE (IRR(1:3),'(A1,I2)') 's',INT(S)
      endif
      if (irrepdots().ne.0) then
         if (ipar.eq.1) then
            irr(4:5)='.+'
         else
            irr(4:5)='.-'
         endif
      else
         if (IPAR.EQ.1) THEN
            IRR(4:5)='+ '
         else
            IRR(4:5)='- '
         endif
      endif
      IRREP=IRR
      END

      character*11 functioN STATE ( k1 , k2 )
      if ( k1 .ne. k2 ) then
        STATE = 'TRANSITION'
      elseif ( k1 .eq. 1 ) then
        STATE = 'GROUND'
      elseif ( k1 .eq. 2 ) then
        STATE = 'EXCITE'
      endif
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      subroutine transform ( c, nls, nlsp )
c--- transform c to JJ coupling. The transformation matrices are always in
c--- TMX and TMXp when butler is called.
      parameter (klsi=400, KMX=3*KLSI)
      dimension      c   (KMX,KMX)
      COMMON/C3LC2/  TMX (KMX,KMX)
      COMMON/C3LC3/  CT4 (KMX,KMX)
      COMMON/C3LC4/  TMXp(KMX,KMX)

      if (JJcoupling().eq.0) return

      call zero2dim (ct4, nls,nlsp,kmx)
      do 10 JJ = 1, nLS
      do 10 LS = 1, nLS
        if (tmx(LS,JJ).ne.0) then
          do 20 LSp = 1, nLSp
   20       ct4(JJ,LSp) = ct4(JJ,LSp) + tmx(LS,JJ) * c(LS,LSp)
        endif
   10 continue

      call zero2dim (c  , nls,nlsp,kmx)
      do 30 JJp = 1, nLSp
      do 30 LSp = 1, nLSp
        if (tmxp(LSp,JJp).ne.0) then
          do 40 JJ = 1, nLS
   40       c(JJ,JJp) = c(JJ,JJp) + ct4(JJ,LSp) * tmxp(LSp,JJp)      
        endif
   30 continue
      end

C#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*

      function jjcoupling()
      COMMON /CBUTLER/ IBUTLERx,JJcouplingx,IRREPdotsx
      jjcoupling = JJcouplingx
      return

      entry    ibutler()
      ibutler    = IBUTLERx
      return

      entry irrepdots()
      irrepdots = IRREPdotsx
      return
      end
